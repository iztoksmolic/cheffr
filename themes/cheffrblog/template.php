<?php

/**
 * Preprocess and Process Functions SEE: http://drupal.org/node/254940#variables-processor
 * 1. Rename each function and instance of "cheffrblog" to match
 *    your subthemes name, e.g. if your theme name is "footheme" then the function
 *    name will be "footheme_preprocess_hook". Tip - you can search/replace
 *    on "cheffrblog".
 * 2. Uncomment the required function to use.
 * 3. Read carefully, especially within cheffrblog_preprocess_html(), there
 *    are extra goodies you might want to leverage such as a very simple way of adding
 *    stylesheets for Internet Explorer and a browser detection script to add body classes.
 */

/**
 * Override or insert variables into the html templates.
 */
function cheffrblog_preprocess_html(&$vars) {
  
  // Add class for the active theme name
  /* -- Delete this line to add a class for the active theme name.
  $vars['classes_array'][] = drupal_html_class($theme_key);
  // */

  // Browser/platform sniff - adds body classes such as ipad, webkit, chrome etc.
  /* -- Delete this line to add a classes for the browser and platform.
  $vars['classes_array'][] = css_browser_selector();
  // */

}

/* -- Delete this line if you want to use this function
function cheffrblog_process_html(&$vars) {
}
// */

/**
 * Override or insert variables into the page templates.
 */
function cheffrblog_preprocess_page(&$vars) {
  //dpm($vars);
  if($node = menu_get_object('post_node',3)) {
    $vars['title'] = FALSE;
  }
}

function cheffrblog_process_page(&$vars) {
}


/**
 * Override or insert variables into the node templates.
 */
function cheffrblog_preprocess_node(&$vars) {
  //template foo teaser tpls
  $vars['theme_hook_suggestions'][] = 'node__' . $vars['node']->type . '__' . $vars['view_mode'];
}

function cheffrblog_process_node(&$vars) {
}

/**
 * Override or insert variables into the comment templates.
 */
/* -- Delete this line if you want to use these functions
function cheffrblog_preprocess_comment(&$vars) {
}

function cheffrblog_process_comment(&$vars) {
}
// */

/**
 * Override or insert variables into the block templates.
 */
/* -- Delete this line if you want to use these functions
function cheffrblog_preprocess_block(&$vars) {
}

function cheffrblog_process_block(&$vars) {
}
// */

/**
 * Add the Style Schemes if enabled.
 * NOTE: You MUST make changes in your subthemes theme-settings.php file
 * also to enable Style Schemes.
 */
/* -- Delete this line if you want to enable style schemes.
// DONT TOUCH THIS STUFF...
function get_at_styles() {
  $scheme = theme_get_setting('style_schemes');
  if (!$scheme) {
    $scheme = 'style-default.css';
  }
  if (isset($_COOKIE["atstyles"])) {
    $scheme = $_COOKIE["atstyles"];
  }
  return $scheme;
}
if (theme_get_setting('style_enable_schemes') == 'on') {
  $style = get_at_styles();
  if ($style != 'none') {
    drupal_add_css(path_to_theme() . '/css/schemes/' . $style, array(
      'group' => CSS_THEME,
      'preprocess' => TRUE,
      )
    );
  }
}
// */

/**
 * Themes field collection items printed using the field_collection_view formatter.
 */
function cheffrblog_field_collection_view($variables) {
  $element = $variables['element'];
  return $element['#children'];
}