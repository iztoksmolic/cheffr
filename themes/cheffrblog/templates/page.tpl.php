<div id="page-wrapper">
<div id="page" class="container"><div class="container-inner">

  <div id="columns"><div class="columns-inner clearfix">
  
    <?php print render($page['sidebar_first']); ?>
  
    <div id="content-column"><div class="content-inner">

      <?php $tag = $title ? 'section' : 'div'; ?>
      <<?php print $tag; ?> id="main-content" role="main">

        <?php print $messages; ?>

        <div id="content">

          <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <header>
              <?php if (!$is_front && $is_post_node && $title): ?>
                <h1 id="page-title"><?php print $title; ?></h1>
              <?php endif; ?>
            </header>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
  
          <?php print render($page['content']); ?>
        
        </div>

        <?php print $feed_icons; ?>

      </<?php print $tag; ?>>

      <?php print render($page['content_aside']); ?>

    </div></div>

    <?php print render($page['sidebar_second']); ?>

  </div></div>

  <?php if ($page['footer']): ?>
    <footer role="contentinfo"><?php print render($page['footer']); ?></footer>
  <?php endif; ?>

</div></div>
<footer id="page-footer">
  Powered by Cheffr.com
</footer>
</div>