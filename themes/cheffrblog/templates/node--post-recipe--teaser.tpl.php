<?php
//dpm($node);
?>
<article id="article-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="two-33-66 gpanel clearfix">
    <div class="region region-two-33-66-first">
      <?php print render($content['field_post_recipe_photo']); ?>
    </div>
    <div class="region region-two-33-66-second">
      <?php print render($title_prefix); ?>
      <header>
        <h1<?php print $title_attributes; ?>>
          <a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a>
        </h1>
      </header>
      <?php print render($title_suffix); ?>
      <?php print render($content['field_post_recipe_summary']); ?>
    </div>
  </div>
</article>
