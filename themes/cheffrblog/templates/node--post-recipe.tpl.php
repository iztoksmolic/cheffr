<?php
dpm($node);
?>
<article id="article-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix hrecipe"<?php print $attributes; ?>>

    <figure class="recipe-photo">
      <?php
      dpm($content['field_post_recipe_photo']);
      print render($content['field_post_recipe_photo']);
      ?>
    </figure>
  
    <?php print $unpublished; ?>
    
    <header>
      <?php
        print format_date($node->created);
      ?>
      <?php if ($title): ?>
        <h1 class="fn">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
    </header>
    
    <div<?php print $content_attributes; ?>>
      <div class="two-66-33 gpanel clearfix">
        <div class="region region-two-66-33-first">
          <?php print render($content['field_post_recipe_summary']); ?>       
        </div>
        <div class="region region-two-66-33-second">
          <?php if($foodblog_widget_iframe_code): print $foodblog_widget_iframe_code; endif; ?>
        </div>
      </div>
      
      <div class="two-66-33 gpanel clearfix">
        <div class="region region-two-66-33-first">
          <?php
          print render($content['field_post_recipe_ingredients']);
          ?>        
        </div>
        <div class="region region-two-66-33-second">
          <?php print render($content['field_post_recipe_course']); ?>
          <?php print render($content['field_post_recipe_preptime']); ?>
          <?php print render($content['field_post_recipe_cooktime']); ?> 
        </div>
      </div>
    </div>
  
    <?php if ($links = render($content['links'])): ?>
      <nav class="clearfix"><?php print $links; ?></nav>
    <?php endif; ?>
  
    <?php print render($content['comments']); ?>
    
</article>
