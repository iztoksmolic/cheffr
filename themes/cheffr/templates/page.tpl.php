<?php print render($page['toolbar']); ?>

<?php if ($page['header']): ?>
  <header id="page-header-anoni" class="clearfix" role="banner"><div class="page-header-anoni-inner">
    <hgroup>
      <h1 id="site-name"<?php if ($hide_site_name): ?> class="<?php print $visibility; ?>"<?php endif; ?>><?php print $site_name; ?></h1>
    </hgroup>
    <?php print render($page['header']); ?>
  </div></header>
<?php endif; ?>

<div id="page" class="container">

  <div class="clearfix page-title-tabs">
      <header id="page-header" class="clearfix" role="banner">
        <hgroup>
          <?php if ($title): ?>
            <h1 id="page-title" class="element-hidden"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php if (!empty($section_title)): ?>
            <h2 id="section-title"><?php print $section_title; ?></h2>
          <?php endif; ?>
        </hgroup>
      </header>
    
    <?php if ($primary_local_tasks): ?>
      <nav class="page-tabs">
        <?php if ($primary_local_tasks || $action_links): ?>
          <?php if ($primary_local_tasks): ?>
            <ul class="tabs primary clearfix"><?php print render($primary_local_tasks); ?></ul>
          <?php endif; ?>
        <?php endif; ?>
      </nav>
    <?php endif; ?>
  </div>
  
  <?php
    if ($secondary_local_tasks || $page['sidebar']) {
      $sidebar = TRUE;
    } else {
      $sidebar = FALSE;
    }
  ?>

  <div id="columns" class="<?php if($sidebar){print 'with-sidebar';} ?>"><div class="columns-inner clearfix">

    <?php if($sidebar): ?>
      <div id="sidebar">
        <?php if ($secondary_local_tasks): ?>
          <ul class="tabs secondary clearfix"><?php print render($secondary_local_tasks); ?></ul>
        <?php endif; ?>
        <?php print render($page['sidebar']); ?>
      </div>
    <?php endif; ?>

    <div id="content-column"><div class="content-inner">

      <?php print $messages; ?>
      <?php print render($page['help']); ?>
      <?php print render($page['highlighted']); ?>

      <?php $tag = $title ? 'section' : 'div'; ?>
      <<?php print $tag; ?> id="main-content" role="main">
        
        <?php print render($title_prefix); ?>
        <?php if ($action_links = render($action_links)): ?>
          <ul class="action-links clearfix"><?php print $action_links; ?></ul>
        <?php endif; ?>
        <?php print render($title_suffix); ?>

        <div id="content"><?php print render($page['content']); ?></div>

        <?php print $feed_icons; ?>

      </<?php print $tag; ?>>
      
    </div></div>
    
  </div></div>

  <?php print render($page['tertiary_content']); ?>

  <?php if ($page['footer']): ?>
    <footer role="contentinfo"><?php print render($page['footer']); ?></footer>
  <?php endif; ?>

</div>
