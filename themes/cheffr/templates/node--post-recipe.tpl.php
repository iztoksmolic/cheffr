<article id="article-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<?php
//dpm($content);
?>
  <?php if($teaser): ?>
    <div class="two-33-66 gpanel clearfix">
      <div class="region region-two-33-66-first">
        <?php print render($content['field_post_recipe_photo']); ?>
      </div>
      <div class="region region-two-33-66-second">
        <?php print render($title_prefix); ?>
        <header>
          <h1<?php print $title_attributes; ?>>
            <a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a>
          </h1>
        </header>
        <?php print render($title_suffix); ?>
        <?php print render($content['field_post_recipe_summary']); ?>
      </div>
    </div>
  <?php else: ?>
    <?php print $unpublished; ?>
  
    <?php print render($title_prefix); ?>
    <?php if ($title && !$page): ?>
      <header>
        <?php if ($title): ?>
          <h1<?php print $title_attributes; ?>>
            <a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a>
          </h1>
        <?php endif; ?>
      </header>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
  
    <?php if ($display_submitted): ?>
      <footer class="submitted<?php $user_picture ? print ' with-user-picture' : ''; ?>">
        <?php print $user_picture; ?>
        <p class="author-datetime"><?php print $submitted; ?></p>
      </footer>
    <?php endif; ?>
  
    <div<?php print $content_attributes; ?>>
    <?php
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
    </div>
  
    <?php if ($links = render($content['links'])): ?>
      <nav class="clearfix"><?php print $links; ?></nav>
    <?php endif; ?>
  
    <?php print render($content['comments']); ?>
  <?php endif; ?>

</article>
