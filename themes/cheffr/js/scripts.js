/**
 * @file
 * Adds JS to front page
 */

(function ($) {
 Drupal.behaviors.cheffr = {
  attach: function (context, settings) {
    if($("body.page-signup").length > 0) {
      if($('#edit-mail').val() == "") {
        $('#edit-mail').val('Email').focus(function(){
          if($(this).val() == "Email") {
            $(this).val('').addClass('has-input');
          }
        }).blur(function(){
          if($(this).val() == '') {
            $(this).val('Email');
            $(this).removeClass('has-input');
          } 
        });
      } else {
        $('#edit-mail').addClass('has-input');
      }
      
      $('#edit-pass-pass1').hide().addClass("has-input");
      $('#edit-pass-pass1').after('<input class="form-text" id="edit-pass-pass1-fake" value="Password" />');
      
      $('#edit-pass-pass1-fake').focus(function(){
        $(this).hide();
        $('#edit-pass-pass1').show().focus().addClass('has-input');;
      });
      $('#edit-pass-pass1').blur(function(){
        if($(this).val() == '') {
          $(this).hide();
          $('#edit-pass-pass1-fake').show();
        } 
      });
      
      $('#edit-pass-pass2').hide().addClass("has-input");
      $('#edit-pass-pass2').after('<input class="form-text" id="edit-pass-pass2-fake" value="Repeat password" />');
      
      $('#edit-pass-pass2-fake').focus(function(){
        $(this).hide();
        $('#edit-pass-pass2').show().focus().addClass('has-input');;
      });
      $('#edit-pass-pass2').blur(function(){
        if($(this).val() == '') {
          $(this).hide();
          $('#edit-pass-pass2-fake').show();
        } 
      });
  
      if($('#edit-field-blog-domain-prefix').val() == "") {
        $('#edit-field-blog-domain-prefix').val('URL').focus(function(){
          if($(this).val() == "URL") {
            $(this).val('').addClass('has-input');
          }
          $(this).parent(".form-item-field-blog-domain-prefix").addClass("focus");
        }).blur(function(){
          if($(this).val() == '') {
            $(this).val('URL');
            $(this).removeClass('has-input');
          }
          $(this).parent(".form-item-field-blog-domain-prefix").removeClass("focus");
        });
      } else {
        $('#edit-field-blog-domain-prefix').addClass('has-input');
      }
      
      // ghost input
      $(".form-item-field-blog-domain-prefix .field-suffix").hide().prepend("<span class=\"ghost-text\"></span>")
      $(".form-item-field-blog-domain-prefix input.form-text").keyup(function(){
        $(".form-item-field-blog-domain-prefix .field-suffix").show();
        $(".form-item-field-blog-domain-prefix .field-suffix .ghost-text").text($(this).val());
      });
      $(".form-item-field-blog-domain-prefix input.form-text").blur(function(){
        if($(this).val() == '') {
          $(".form-item-field-blog-domain-prefix .field-suffix").hide();
        } 
      });
    }
    // end of signup code

    console.log(hhmmss_to_milliseconds("48h 30min"));
    
    //$("input.form-submit[id$=upload-button").hide();
    $('input.form-submit[name$=upload_button]').hide();
    //console.log();
    
    $("input[type=file]").change(function() {
      $(this).parents("div.image-widget-data").find("input[type=submit]").mousedown();
      console.log($(this).parents("div.image-widget-data").find("input[type=submit]").val());
    });
    
  }
 }
 
})(jQuery);



function TimeParser() {
  function a(a, c) {
    this.match = function (c) {
      if (!c) return false;
      var e = c.match(a);
      if (!e) return e;
      e[0] = c;
      return e
    };
    this.interpret = function (a) {
      a = this.match(a);
      return !a ? false : c(a)
    }
  }
  var e = [new a(/^\s*0*(\d{1,10})\s*(:|h)\s*0*(\d{1,10})\s*(:|m)\s*0*(\d{1,10})\s*s?\s*$/, function (a) {
    return 1E3 * a[5] + 6E4 * a[3] + 36E5 * a[1]
  }), new a(/^\s*0*(\d{1,10})\s*(m|mins?|minutes?)?$/, function (a) {
    return 6E4 * a[1]
  }), new a(/^\s*0*(\d{1,10})\s*(h|hrs?|hours?)\s*$/, function (a) {
    return 36E5 * a[1]
  }), new a(/^\s*0*(\d{1,10})\s*(s|secs?|seconds?)\s*$/, function (a) {
    return 1E3 * a[1]
  }), new a(/^\s*0*(\d{1,10})?\s*[.,]\s*(\d{0,9})\s*(h|hrs?|hours?)?$/, function (a) {
    var c = a[2];
    return parseFloat(a[1] || 0) * 36E5 + parseFloat("0." + c) * 36E5
  }), new a(/^\s*0*(\d{1,10})\s*[.,]\s*(\d{0,9})\s*(m|mins?|minutes?)?\s*$/, function (a) {
    var c = a[2];
    return parseFloat(a[1]) * 6E4 + parseFloat("0." + c) * 6E4
  }), new a(/^\s*0*(\d{1,10})\s*[.,]\s*(\d{0,9})\s*(s|secs?|seconds?)?\s*$/, function (a) {
    return parseFloat(a[1] + "." + a[2]) * 1E3
  }), new a(/^\s*0*(\d{1,10})\s*:\s*0*(\d{1,10})\s*mins?\s*$/, function (a) {
    return 6E4 * a[1] + 1E3 * a[2]
  }), new a(/^\s*0*(\d{1,10})\s*(:|h|hrs?|hours?)\s*0*(\d{1,10})\s*(m|mins?|minutes?)?\s*$/, function (a) {
    return 6E4 * a[3] + 36E5 * a[1]
  })];
  this.is_valid = function (a) {
    for (var c = 0; c < e.length; ++c) if (e[c].match(a)) return true;
    return false
  };
  this.interpret = function (a) {
    for (var a = a.toLowerCase(), c = 0; c < e.length; ++c) if (e[c].match(a)) return e[c].interpret(a);
    return null
  }
}

var TIME_PARSERS = new TimeParser,
  hhmmss_to_milliseconds = window.TIME_PARSERS.interpret;

