var Translation = {};

(function() { var _ = arguments[0];_["Expand"] = "Expand";
_["Collapse"] = "Collapse";
_["Autosync - daily at"] = "Autosync - daily at";
_["import and time entries export"] = "import and time entries export";
_["import only"] = "import only";
_["Sorry, something went wrong."] = "Sorry, something went wrong.";
_["Failed to delete project."] = "Failed to delete project.";
_["Server error"] = "Server error";
_["Time entry update failed. Please try again."] = "Time entry update failed. Please try again.";
_["This workspace is suspended! Time entry is not saved!"] = "This workspace is suspended! Time entry is not saved!";
_["Integration is configured"] = "Integration is configured";
_["Continue"] = "Continue";
_["Continue with this time entry"] = "Continue with this time entry";
_["(no project)"] = "(no project)";
_["(all projects)"] = "(all projects)";
_["Loading..."] = "Loading...";
_["Are you sure you want clear all the settings of this integration?"] = "Are you sure you want clear all the settings of this integration?";
_["Integration settings have been cleared!"] = "Integration settings have been cleared!";
_["today"] = "today";
_["yesterday"] = "yesterday";
_["this week"] = "this week";
_["last week"] = "last week";
_["this month"] = "this month";
_["last month"] = "last month";
_["Save"] = "Save";
_["Start"] = "Start";
_["What are you working on?"] = "What are you working on?";
_["What have you done?"] = "What have you done?";
_["1 time entry ready for export."] = "1 time entry ready for export.";
_["time entries ready for export."] = "time entries ready for export.";
_["No time entries ready for export."] = "No time entries ready for export.";
_["Are you sure you want to change your plan?"] = "Are you sure you want to change your plan?";
}(Translation))



/*
: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/. 
 @website: http://www.datejs.com/
*/
(function (a) {
  function e(a, b) {
    for (var d = a.length; d--;) if (a[d] === b) return d;
    return -1
  }
  function b(a, b, d) {
    var c, e, g, h;
    d === void 0 && (d = b, b = "all");
    a = a.replace(/\s/g, "");
    c = a.split(",");
    c[c.length - 1] == "" && (c[c.length - 2] += ",");
    for (g = 0; g < c.length; g++) {
      e = [];
      a = c[g].split("+");
      if (a.length > 1) {
        e = a.slice(0, a.length - 1);
        for (h = 0; h < e.length; h++) e[h] = j[e[h]];
        a = [a[a.length - 1]]
      }
      a = a[0];
      a = l[a] || a.toUpperCase().charCodeAt(0);
      a in f || (f[a] = []);
      f[a].push({
        shortcut: c[g],
        scope: b,
        method: d,
        key: c[g],
        mods: e
      })
    }
  }
  function c(a, b, d) {
    a.addEventListener ? a.addEventListener(b, d, false) : a.attachEvent && a.attachEvent("on" + b, function () {
      d(window.event)
    })
  }
  var d, f = {},
    g = {
      16: false,
      18: false,
      17: false,
      91: false
    },
    h = "all",
    j = {
      "\u21e7": 16,
      shift: 16,
      "\u2325": 18,
      alt: 18,
      option: 18,
      "\u2303": 17,
      ctrl: 17,
      control: 17,
      "\u2318": 91,
      command: 91
    },
    l = {
      backspace: 8,
      tab: 9,
      clear: 12,
      enter: 13,
      "return": 13,
      esc: 27,
      escape: 27,
      space: 32,
      left: 37,
      up: 38,
      right: 39,
      down: 40,
      del: 46,
      "delete": 46,
      home: 36,
      end: 35,
      pageup: 33,
      pagedown: 34,
      ",": 188,
      ".": 190,
      "/": 191,
      "`": 192,
      "-": 189,
      "=": 187,
      ";": 186,
      "'": 222,
      "[": 219,
      "]": 221,
      "\\": 220
    };
  for (d = 1; d < 20; d++) j["f" + d] = 111 + d;
  for (d in j) b[d] = false;
  c(document, "keydown", function (a) {
    var d, c, l, u, w;
    c = (a.target || a.srcElement).tagName;
    d = a.keyCode;
    if (d == 93 || d == 224) d = 91;
    if (d in g) for (l in g[d] = true, j) j[l] == d && (b[l] = true);
    else if (!(c == "INPUT" || c == "SELECT" || c == "TEXTAREA") && d in f) for (u = 0; u < f[d].length; u++) if (c = f[d][u], c.scope == h || c.scope == "all") {
      w = c.mods.length > 0;
      for (l in g) if (!g[l] && e(c.mods, +l) > -1 || g[l] && e(c.mods, +l) == -1) w = false;
      if ((c.mods.length == 0 && !g[16] && !g[18] && !g[17] && !g[91] || w) && c.method(a, c) === false) if (a.preventDefault ? a.preventDefault() : a.returnValue = false, a.stopPropagation && a.stopPropagation(), a.cancelBubble) a.cancelBubble = true
    }
  });
  c(document, "keyup", function (a) {
    var a = a.keyCode,
      d;
    if (a == 93 || a == 224) a = 91;
    if (a in g) for (d in g[a] = false, j) j[d] == a && (b[d] = false)
  });
  c(window, "focus", function () {
    for (d in g) g[d] = false;
    for (d in j) b[d] = false
  });
  a.key = b;
  a.key.setScope = function (a) {
    h = a || "all"
  };
  a.key.getScope = function () {
    return h || "all"
  };
  if (typeof module !== "undefined") module.exports = key
})(this);
(function (a, e) {
  function b(a) {
    return k.isWindow(a) ? a : a.nodeType === 9 ? a.defaultView || a.parentWindow : false
  }
  function c(a) {
    if (!zb[a]) {
      var b = H.body,
        d = k("<" + a + ">").appendTo(b),
        c = d.css("display");
      d.remove();
      if (c === "none" || c === "") {
        jb || (jb = H.createElement("iframe"), jb.frameBorder = jb.width = jb.height = 0);
        b.appendChild(jb);
        if (!Db || !jb.createElement) Db = (jb.contentWindow || jb.contentDocument).document, Db.write((H.compatMode === "CSS1Compat" ? "<!doctype html>" : "") + "<html><body>"), Db.close();
        d = Db.createElement(a);
        Db.body.appendChild(d);
        c = k.css(d, "display");
        b.removeChild(jb)
      }
      zb[a] = c
    }
    return zb[a]
  }
  function d(a, b) {
    var d = {};
    k.each(qc.concat.apply([], qc.slice(0, b)), function () {
      d[this] = a
    });
    return d
  }
  function f() {
    kc = e
  }
  function g() {
    setTimeout(f, 0);
    return kc = k.now()
  }
  function h() {
    try {
      return new a.XMLHttpRequest
    } catch (b) {}
  }
  function j(a, b, d, c) {
    if (k.isArray(b)) k.each(b, function (b, f) {
      d || Tb.test(a) ? c(a, f) : j(a + "[" + (typeof f == "object" || k.isArray(f) ? b : "") + "]", f, d, c)
    });
    else if (!d && b != null && typeof b == "object") for (var f in b) j(a + "[" + f + "]", b[f], d, c);
    else c(a, b)
  }
  function l(a, b) {
    var d, c, f = k.ajaxSettings.flatOptions || {};
    for (d in b) b[d] !== e && ((f[d] ? a : c || (c = {}))[d] = b[d]);
    c && k.extend(true, a, c)
  }
  function n(a, b, d, c, f, g) {
    f = f || b.dataTypes[0];
    g = g || {};
    g[f] = true;
    for (var f = a[f], h = 0, v = f ? f.length : 0, j = a === hc, G; h < v && (j || !G); h++) G = f[h](b, d, c), typeof G == "string" && (!j || g[G] ? G = e : (b.dataTypes.unshift(G), G = n(a, b, d, c, G, g)));
    (j || !G) && !g["*"] && (G = n(a, b, d, c, "*", g));
    return G
  }
  function m(a) {
    return function (b, d) {
      typeof b != "string" && (d = b, b = "*");
      if (k.isFunction(d)) for (var c = b.toLowerCase().split(T), f = 0, e = c.length, g, v, h; f < e; f++) g = c[f], h = /^\+/.test(g), h && (g = g.substr(1) || "*"), v = a[g] = a[g] || [], v[h ? "unshift" : "push"](d)
    }
  }
  function p(a, b, d) {
    var c = b === "width" ? a.offsetWidth : a.offsetHeight,
      f = b === "width" ? Va : Eb,
      e = 0,
      g = f.length;
    if (c > 0) {
      if (d !== "border") for (; e < g; e++) d || (c -= parseFloat(k.css(a, "padding" + f[e])) || 0), d === "margin" ? c += parseFloat(k.css(a, d + f[e])) || 0 : c -= parseFloat(k.css(a, "border" + f[e] + "Width")) || 0;
      return c + "px"
    }
    c = na(a, b, b);
    if (c < 0 || c == null) c = a.style[b] || 0;
    c = parseFloat(c) || 0;
    if (d) for (; e < g; e++) c += parseFloat(k.css(a, "padding" + f[e])) || 0, d !== "padding" && (c += parseFloat(k.css(a, "border" + f[e] + "Width")) || 0), d === "margin" && (c += parseFloat(k.css(a, d + f[e])) || 0);
    return c + "px"
  }
  function o(a, b) {
    b.src ? k.ajax({
      url: b.src,
      async: false,
      dataType: "script"
    }) : k.globalEval((b.text || b.textContent || b.innerHTML || "").replace(ob, "/*$0*/"));
    b.parentNode && b.parentNode.removeChild(b)
  }
  function u(a) {
    var b = (a.nodeName || "").toLowerCase();
    b === "input" ? w(a) : b !== "script" && typeof a.getElementsByTagName != "undefined" && k.grep(a.getElementsByTagName("input"), w)
  }
  function w(a) {
    if (a.type === "checkbox" || a.type === "radio") a.defaultChecked = a.checked
  }
  function s(a) {
    return typeof a.getElementsByTagName != "undefined" ? a.getElementsByTagName("*") : typeof a.querySelectorAll != "undefined" ? a.querySelectorAll("*") : []
  }
  function D(a, b) {
    var d;
    if (b.nodeType === 1) {
      b.clearAttributes && b.clearAttributes();
      b.mergeAttributes && b.mergeAttributes(a);
      d = b.nodeName.toLowerCase();
      if (d === "object") b.outerHTML = a.outerHTML;
      else if (d !== "input" || a.type !== "checkbox" && a.type !== "radio") if (d === "option") b.selected = a.defaultSelected;
      else {
        if (d === "input" || d === "textarea") b.defaultValue = a.defaultValue
      } else a.checked && (b.defaultChecked = b.checked = a.checked), b.value !== a.value && (b.value = a.value);
      b.removeAttribute(k.expando)
    }
  }
  function C(a, b) {
    if (b.nodeType === 1 && k.hasData(a)) {
      var d, c, f;
      c = k._data(a);
      var e = k._data(b, c),
        g = c.events;
      if (g) for (d in delete e.handle, e.events = {}, g) for (c = 0, f = g[d].length; c < f; c++) k.event.add(b, d + (g[d][c].namespace ? "." : "") + g[d][c].namespace, g[d][c], g[d][c].data);
      e.data && (e.data = k.extend({}, e.data))
    }
  }

  function z(a) {
    var b = Ba.split("|"),
      a = a.createDocumentFragment();
    if (a.createElement) for (; b.length;) a.createElement(b.pop());
    return a
  }
  function F(a, b, d) {
    b = b || 0;
    if (k.isFunction(b)) return k.grep(a, function (a, c) {
      return !!b.call(a, c, a) === d
    });
    if (b.nodeType) return k.grep(a, function (a) {
      return a === b === d
    });
    if (typeof b == "string") {
      var c = k.grep(a, function (a) {
        return a.nodeType === 1
      });
      if (ra.test(b)) return k.filter(b, c, !d);
      b = k.filter(b, c)
    }
    return k.grep(a, function (a) {
      return k.inArray(a, b) >= 0 === d
    })
  }
  function q() {
    return true
  }

  function K() {
    return false
  }
  function O(a, b, d) {
    var c = b + "defer",
      f = b + "queue",
      e = b + "mark",
      g = k._data(a, c);
    g && (d === "queue" || !k._data(a, f)) && (d === "mark" || !k._data(a, e)) && setTimeout(function () {
      !k._data(a, f) && !k._data(a, e) && (k.removeData(a, c, true), g.fire())
    }, 0)
  }
  function V(a) {
    for (var b in a) if (!(b === "data" && k.isEmptyObject(a[b])) && b !== "toJSON") return false;
    return true
  }
  function cb(a, b, d) {
    if (d === e && a.nodeType === 1) if (d = "data-" + b.replace(Ea, "-$1").toLowerCase(), d = a.getAttribute(d), typeof d == "string") {
      try {
        d = d === "true" ? true : d === "false" ? false : d === "null" ? null : k.isNumeric(d) ? parseFloat(d) : fa.test(d) ? k.parseJSON(d) : d
      } catch (c) {}
      k.data(a, b, d)
    } else d = e;
    return d
  }
  function Ha(a) {
    var b = va[a] = {},
      d, c, a = a.split(/\s+/);
    for (d = 0, c = a.length; d < c; d++) b[a[d]] = true;
    return b
  }
  var H = a.document,
    sa = a.navigator,
    gb = a.location,
    k = function () {
      function b() {
        if (!d.isReady) {
          try {
            H.documentElement.doScroll("left")
          } catch (a) {
            setTimeout(b, 1);
            return
          }
          d.ready()
        }
      }
      var d = function (a, b) {
          return new d.fn.init(a, b, g)
        },
        c = a.jQuery,
        f = a.$,
        g, h = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,
        j = /\S/,
        v = /^\s+/,
        N = /\s+$/,
        G = /^<(\w+)\s*\/?>(?:<\/\1>)?$/,
        n = /^[\],:{}\s]*$/,
        l = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,
        k = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
        m = /(?:^|:|,)(?:\s*\[)+/g,
        q = /(webkit)[ \/]([\w.]+)/,
        o = /(opera)(?:.*version)?[ \/]([\w.]+)/,
        F = /(msie) ([\w.]+)/,
        z = /(mozilla)(?:.*? rv:([\w.]+))?/,
        p = /-([a-z]|[0-9])/ig,
        s = /^-ms-/,
        Ia = function (a, d) {
          return (d + "").toUpperCase()
        },
        va = sa.userAgent,
        mb, oa, w, C = Object.prototype.toString,
        pa = Object.prototype.hasOwnProperty,
        u = Array.prototype.push,
        K = Array.prototype.slice,
        bb = String.prototype.trim,
        ra = Array.prototype.indexOf,
        Z = {};
      d.fn = d.prototype = {
        constructor: d,
        init: function (a, b, c) {
          var f, v, g;
          if (!a) return this;
          if (a.nodeType) return this.context = this[0] = a, this.length = 1, this;
          if (a === "body" && !b && H.body) return this.context = H, this[0] = H.body, this.selector = a, this.length = 1, this;
          if (typeof a == "string") {
            a.charAt(0) !== "<" || a.charAt(a.length - 1) !== ">" || a.length < 3 ? f = h.exec(a) : f = [null, a, null];
            if (f && (f[1] || !b)) {
              if (f[1]) return b = b instanceof d ? b[0] : b, g = b ? b.ownerDocument || b : H, v = G.exec(a), v ? d.isPlainObject(b) ? (a = [H.createElement(v[1])], d.fn.attr.call(a, b, true)) : a = [g.createElement(v[1])] : (v = d.buildFragment([f[1]], [g]), a = (v.cacheable ? d.clone(v.fragment) : v.fragment).childNodes), d.merge(this, a);
              if ((b = H.getElementById(f[2])) && b.parentNode) {
                if (b.id !== f[2]) return c.find(a);
                this.length = 1;
                this[0] = b
              }
              this.context = H;
              this.selector = a;
              return this
            }
            return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a)
          }
          if (d.isFunction(a)) return c.ready(a);
          a.selector !== e && (this.selector = a.selector, this.context = a.context);
          return d.makeArray(a, this)
        },
        selector: "",
        jquery: "1.7.1",
        length: 0,
        size: function () {
          return this.length
        },
        toArray: function () {
          return K.call(this, 0)
        },
        get: function (a) {
          return a == null ? this.toArray() : a < 0 ? this[this.length + a] : this[a]
        },
        pushStack: function (a, b, c) {
          var f = this.constructor();
          d.isArray(a) ? u.apply(f, a) : d.merge(f, a);
          f.prevObject = this;
          f.context = this.context;
          b === "find" ? f.selector = this.selector + (this.selector ? " " : "") + c : b && (f.selector = this.selector + "." + b + "(" + c + ")");
          return f
        },
        each: function (a, b) {
          return d.each(this, a, b)
        },
        ready: function (a) {
          d.bindReady();
          oa.add(a);
          return this
        },
        eq: function (a) {
          a = +a;
          return a === -1 ? this.slice(a) : this.slice(a, a + 1)
        },
        first: function () {
          return this.eq(0)
        },
        last: function () {
          return this.eq(-1)
        },
        slice: function () {
          return this.pushStack(K.apply(this, arguments), "slice", K.call(arguments).join(","))
        },
        map: function (a) {
          return this.pushStack(d.map(this, function (d, b) {
            return a.call(d, b, d)
          }))
        },
        end: function () {
          return this.prevObject || this.constructor(null)
        },
        push: u,
        sort: [].sort,
        splice: [].splice
      };
      d.fn.init.prototype = d.fn;
      d.extend = d.fn.extend = function () {
        var a, b, c, f, v, g, h = arguments[0] || {},
          ba = 1,
          j = arguments.length,
          N = false;
        for (typeof h == "boolean" && (N = h, h = arguments[1] || {}, ba = 2), typeof h != "object" && !d.isFunction(h) && (h = {}), j === ba && (h = this, --ba); ba < j; ba++) if ((a = arguments[ba]) != null) for (b in a) c = h[b], f = a[b], h !== f && (N && f && (d.isPlainObject(f) || (v = d.isArray(f))) ? (v ? (v = false, g = c && d.isArray(c) ? c : []) : g = c && d.isPlainObject(c) ? c : {}, h[b] = d.extend(N, g, f)) : f !== e && (h[b] = f));
        return h
      };
      d.extend({
        noConflict: function (b) {
          a.$ === d && (a.$ = f);
          b && a.jQuery === d && (a.jQuery = c);
          return d
        },
        isReady: false,
        readyWait: 1,
        holdReady: function (a) {
          a ? d.readyWait++ : d.ready(true)
        },
        ready: function (a) {
          if (a === true && !--d.readyWait || a !== true && !d.isReady) {
            if (!H.body) return setTimeout(d.ready, 1);
            d.isReady = true;
            a !== true && --d.readyWait > 0 || (oa.fireWith(H, [d]), d.fn.trigger && d(H).trigger("ready").off("ready"))
          }
        },
        bindReady: function () {
          if (!oa) {
            oa = d.Callbacks("once memory");
            if (H.readyState === "complete") return setTimeout(d.ready, 1);
            if (H.addEventListener) H.addEventListener("DOMContentLoaded", w, false), a.addEventListener("load", d.ready, false);
            else if (H.attachEvent) {
              H.attachEvent("onreadystatechange", w);
              a.attachEvent("onload", d.ready);
              var c = false;
              try {
                c = a.frameElement == null
              } catch (f) {}
              H.documentElement.doScroll && c && b()
            }
          }
        },
        isFunction: function (a) {
          return d.type(a) === "function"
        },
        isArray: Array.isArray ||
        function (a) {
          return d.type(a) === "array"
        },
        isWindow: function (a) {
          return a && typeof a == "object" && "setInterval" in a
        },
        isNumeric: function (a) {
          return !isNaN(parseFloat(a)) && isFinite(a)
        },
        type: function (a) {
          return a == null ? String(a) : Z[C.call(a)] || "object"
        },
        isPlainObject: function (a) {
          if (!a || d.type(a) !== "object" || a.nodeType || d.isWindow(a)) return false;
          try {
            if (a.constructor && !pa.call(a, "constructor") && !pa.call(a.constructor.prototype, "isPrototypeOf")) return false
          } catch (b) {
            return false
          }
          for (var c in a);
          return c === e || pa.call(a, c)
        },
        isEmptyObject: function (a) {
          for (var d in a) return false;
          return true
        },
        error: function (a) {
          throw Error(a);
        },
        parseJSON: function (b) {
          if (typeof b != "string" || !b) return null;
          b = d.trim(b);
          if (a.JSON && a.JSON.parse) return a.JSON.parse(b);
          if (n.test(b.replace(l, "@").replace(k, "]").replace(m, ""))) return (new Function("return " + b))();
          d.error("Invalid JSON: " + b)
        },
        parseXML: function (b) {
          var c, f;
          try {
            a.DOMParser ? (f = new DOMParser, c = f.parseFromString(b, "text/xml")) : (c = new ActiveXObject("Microsoft.XMLDOM"), c.async = "false", c.loadXML(b))
          } catch (v) {
            c = e
          }(!c || !c.documentElement || c.getElementsByTagName("parsererror").length) && d.error("Invalid XML: " + b);
          return c
        },
        noop: function () {},
        globalEval: function (d) {
          d && j.test(d) && (a.execScript ||
          function (d) {
            a.eval.call(a, d)
          })(d)
        },
        camelCase: function (a) {
          return a.replace(s, "ms-").replace(p, Ia)
        },
        nodeName: function (a, d) {
          return a.nodeName && a.nodeName.toUpperCase() === d.toUpperCase()
        },
        each: function (a, b, c) {
          var f, v = 0,
            g = a.length,
            h = g === e || d.isFunction(a);
          if (c) if (h) for (f in a) {
            if (b.apply(a[f], c) === false) break
          } else for (; v < g;) {
            if (b.apply(a[v++], c) === false) break
          } else if (h) for (f in a) {
            if (b.call(a[f], f, a[f]) === false) break
          } else for (; v < g;) if (b.call(a[v], v, a[v++]) === false) break;
          return a
        },
        trim: bb ?
        function (a) {
          return a == null ? "" : bb.call(a)
        } : function (a) {
          return a == null ? "" : (a + "").replace(v, "").replace(N, "")
        },
        makeArray: function (a, b) {
          var c = b || [];
          if (a != null) {
            var f = d.type(a);
            a.length == null || f === "string" || f === "function" || f === "regexp" || d.isWindow(a) ? u.call(c, a) : d.merge(c, a)
          }
          return c
        },
        inArray: function (a, d, b) {
          var c;
          if (d) {
            if (ra) return ra.call(d, a, b);
            for (c = d.length, b = b ? b < 0 ? Math.max(0, c + b) : b : 0; b < c; b++) if (b in d && d[b] === a) return b
          }
          return -1
        },
        merge: function (a, b) {
          var d = a.length,
            c = 0;
          if (typeof b.length == "number") for (var f = b.length; c < f; c++) a[d++] = b[c];
          else for (; b[c] !== e;) a[d++] = b[c++];
          a.length = d;
          return a
        },
        grep: function (a, b, d) {
          for (var c = [], f, d = !! d, e = 0, v = a.length; e < v; e++) f = !! b(a[e], e), d !== f && c.push(a[e]);
          return c
        },
        map: function (a, b, c) {
          var f, v, g = [],
            h = 0,
            ba = a.length;
          if (a instanceof d || ba !== e && typeof ba == "number" && (ba > 0 && a[0] && a[ba - 1] || ba === 0 || d.isArray(a))) for (; h < ba; h++) f = b(a[h], h, c), f != null && (g[g.length] = f);
          else for (v in a) f = b(a[v], v, c), f != null && (g[g.length] = f);
          return g.concat.apply([], g)
        },
        guid: 1,
        proxy: function (a, b) {
          if (typeof b == "string") {
            var c = a[b];
            b = a;
            a = c
          }
          if (!d.isFunction(a)) return e;
          var f = K.call(arguments, 2),
            c = function () {
              return a.apply(b, f.concat(K.call(arguments)))
            };
          c.guid = a.guid = a.guid || c.guid || d.guid++;
          return c
        },
        access: function (a, b, c, f, v, g) {
          var h = a.length;
          if (typeof b == "object") {
            for (var ba in b) d.access(a, ba, b[ba], f, v, c);
            return a
          }
          if (c !== e) {
            f = !g && f && d.isFunction(c);
            for (ba = 0; ba < h; ba++) v(a[ba], b, f ? c.call(a[ba], ba, v(a[ba], b)) : c, g);
            return a
          }
          return h ? v(a[0], b) : e
        },
        now: function () {
          return (new Date).getTime()
        },
        uaMatch: function (a) {
          a = a.toLowerCase();
          a = q.exec(a) || o.exec(a) || F.exec(a) || a.indexOf("compatible") < 0 && z.exec(a) || [];
          return {
            browser: a[1] || "",
            version: a[2] || "0"
          }
        },
        sub: function () {
          function a(b, d) {
            return new a.fn.init(b, d)
          }
          d.extend(true, a, this);
          a.superclass = this;
          a.fn = a.prototype = this();
          a.fn.constructor = a;
          a.sub = this.sub;
          a.fn.init = function (c, f) {
            f && f instanceof d && !(f instanceof a) && (f = a(f));
            return d.fn.init.call(this, c, f, b)
          };
          a.fn.init.prototype = a.fn;
          var b = a(H);
          return a
        },
        browser: {}
      });
      d.each("Boolean Number String Function Array Date RegExp Object".split(" "), function (a, b) {
        Z["[object " + b + "]"] = b.toLowerCase()
      });
      mb = d.uaMatch(va);
      mb.browser && (d.browser[mb.browser] = true, d.browser.version = mb.version);
      d.browser.webkit && (d.browser.safari = true);
      j.test("\u00a0") && (v = /^[\s\xA0]+/, N = /[\s\xA0]+$/);
      g = d(H);
      H.addEventListener ? w = function () {
        H.removeEventListener("DOMContentLoaded", w, false);
        d.ready()
      } : H.attachEvent && (w = function () {
        H.readyState === "complete" && (H.detachEvent("onreadystatechange", w), d.ready())
      });
      return d
    }(),
    va = {};
  k.Callbacks = function (a) {
    var a = a ? va[a] || Ha(a) : {},
      b = [],
      d = [],
      c, f, g, h, v, j = function (d) {
        var c, f, e, v;
        for (c = 0, f = d.length; c < f; c++) e = d[c], v = k.type(e), v === "array" ? j(e) : v === "function" && (!a.unique || !n.has(e)) && b.push(e)
      },
      G = function (e, j) {
        for (j = j || [], c = !a.memory || [e, j], f = true, v = g || 0, g = 0, h = b.length; b && v < h; v++) if (b[v].apply(e, j) === false && a.stopOnFalse) {
          c = true;
          break
        }
        f = false;
        b && (a.once ? c === true ? n.disable() : b = [] : d && d.length && (c = d.shift(), n.fireWith(c[0], c[1])))
      },
      n = {
        add: function () {
          if (b) {
            var a = b.length;
            j(arguments);
            f ? h = b.length : c && c !== true && (g = a, G(c[0], c[1]))
          }
          return this
        },
        remove: function () {
          if (b) for (var d = arguments, c = 0, e = d.length; c < e; c++) for (var g = 0; g < b.length; g++) if (d[c] === b[g] && (f && g <= h && (h--, g <= v && v--), b.splice(g--, 1), a.unique)) break;
          return this
        },
        has: function (a) {
          if (b) for (var d = 0, c = b.length; d < c; d++) if (a === b[d]) return true;
          return false
        },
        empty: function () {
          b = [];
          return this
        },
        disable: function () {
          b = d = c = e;
          return this
        },
        disabled: function () {
          return !b
        },
        lock: function () {
          d = e;
          (!c || c === true) && n.disable();
          return this
        },
        locked: function () {
          return !d
        },
        fireWith: function (b, e) {
          d && (f ? a.once || d.push([b, e]) : (!a.once || !c) && G(b, e));
          return this
        },
        fire: function () {
          n.fireWith(this, arguments);
          return this
        },
        fired: function () {
          return !!c
        }
      };
    return n
  };
  var Z = [].slice;
  k.extend({
    Deferred: function (a) {
      var b = k.Callbacks("once memory"),
        d = k.Callbacks("once memory"),
        c = k.Callbacks("memory"),
        f = "pending",
        e = {
          resolve: b,
          reject: d,
          notify: c
        },
        g = {
          done: b.add,
          fail: d.add,
          progress: c.add,
          state: function () {
            return f
          },
          isResolved: b.fired,
          isRejected: d.fired,
          then: function (a, b, d) {
            v.done(a).fail(b).progress(d);
            return this
          },
          always: function () {
            v.done.apply(v, arguments).fail.apply(v, arguments);
            return this
          },
          pipe: function (a, b, d) {
            return k.Deferred(function (c) {
              k.each({
                done: [a, "resolve"],
                fail: [b, "reject"],
                progress: [d, "notify"]
              }, function (a, b) {
                var d = b[0],
                  f = b[1],
                  e;
                k.isFunction(d) ? v[a](function () {
                  e = d.apply(this, arguments);
                  e && k.isFunction(e.promise) ? e.promise().then(c.resolve, c.reject, c.notify) : c[f + "With"](this === v ? c : this, [e])
                }) : v[a](c[f])
              })
            }).promise()
          },
          promise: function (a) {
            if (a == null) a = g;
            else for (var b in g) a[b] = g[b];
            return a
          }
        },
        v = g.promise({}),
        h;
      for (h in e) v[h] = e[h].fire, v[h + "With"] = e[h].fireWith;
      v.done(function () {
        f = "resolved"
      }, d.disable, c.lock).fail(function () {
        f = "rejected"
      }, b.disable, c.lock);
      a && a.call(v, v);
      return v
    },
    when: function (a) {
      function b(a) {
        return function (b) {
          g[a] = arguments.length > 1 ? Z.call(arguments, 0) : b;
          h.notifyWith(j, g)
        }
      }
      function d(a) {
        return function (b) {
          c[a] = arguments.length > 1 ? Z.call(arguments, 0) : b;
          --v || h.resolveWith(h, c)
        }
      }
      var c = Z.call(arguments, 0),
        f = 0,
        e = c.length,
        g = Array(e),
        v = e,
        h = e <= 1 && a && k.isFunction(a.promise) ? a : k.Deferred(),
        j = h.promise();
      if (e > 1) {
        for (; f < e; f++) c[f] && c[f].promise && k.isFunction(c[f].promise) ? c[f].promise().then(d(f), h.reject, b(f)) : --v;
        v || h.resolveWith(h, c)
      } else h !== a && h.resolveWith(h, e ? [a] : []);
      return j
    }
  });
  k.support = function () {
    var b, d, c, f, e, g, h, v, j, n, l, qa, m = H.createElement("div");
    m.setAttribute("className", "t");
    m.innerHTML = "   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>";
    d = m.getElementsByTagName("*");
    c = m.getElementsByTagName("a")[0];
    if (!d || !d.length || !c) return {};
    f = H.createElement("select");
    e = f.appendChild(H.createElement("option"));
    g = m.getElementsByTagName("input")[0];
    b = {
      leadingWhitespace: m.firstChild.nodeType === 3,
      tbody: !m.getElementsByTagName("tbody").length,
      htmlSerialize: !! m.getElementsByTagName("link").length,
      style: /top/.test(c.getAttribute("style")),
      hrefNormalized: c.getAttribute("href") === "/a",
      opacity: /^0.55/.test(c.style.opacity),
      cssFloat: !! c.style.cssFloat,
      checkOn: g.value === "on",
      optSelected: e.selected,
      getSetAttribute: m.className !== "t",
      enctype: !! H.createElement("form").enctype,
      html5Clone: H.createElement("nav").cloneNode(true).outerHTML !== "<:nav></:nav>",
      submitBubbles: true,
      changeBubbles: true,
      focusinBubbles: false,
      deleteExpando: true,
      noCloneEvent: true,
      inlineBlockNeedsLayout: false,
      shrinkWrapBlocks: false,
      reliableMarginRight: true
    };
    g.checked = true;
    b.noCloneChecked = g.cloneNode(true).checked;
    f.disabled = true;
    b.optDisabled = !e.disabled;
    try {
      delete m.test
    } catch (q) {
      b.deleteExpando = false
    }!m.addEventListener && m.attachEvent && m.fireEvent && (m.attachEvent("onclick", function () {
      b.noCloneEvent = false
    }), m.cloneNode(true).fireEvent("onclick"));
    g = H.createElement("input");
    g.value = "t";
    g.setAttribute("type", "radio");
    b.radioValue = g.value === "t";
    g.setAttribute("checked", "checked");
    m.appendChild(g);
    v = H.createDocumentFragment();
    v.appendChild(m.lastChild);
    b.checkClone = v.cloneNode(true).cloneNode(true).lastChild.checked;
    b.appendChecked = g.checked;
    v.removeChild(g);
    v.appendChild(m);
    m.innerHTML = "";
    a.getComputedStyle && (h = H.createElement("div"), h.style.width = "0", h.style.marginRight = "0", m.style.width = "2px", m.appendChild(h), b.reliableMarginRight = (parseInt((a.getComputedStyle(h, null) || {
      marginRight: 0
    }).marginRight, 10) || 0) === 0);
    if (m.attachEvent) for (l in {
      submit: 1,
      change: 1,
      focusin: 1
    }) n = "on" + l, qa = n in m, qa || (m.setAttribute(n, "return;"), qa = typeof m[n] == "function"), b[l + "Bubbles"] = qa;
    v.removeChild(m);
    v = f = e = h = m = g = null;
    k(function () {
      var a, d, c, f, e, v = H.getElementsByTagName("body")[0];
      !v || (a = H.createElement("div"), a.style.cssText = "visibility:hidden;border:0;width:0;height:0;position:static;top:0;margin-top:1px", v.insertBefore(a, v.firstChild), m = H.createElement("div"), a.appendChild(m), m.innerHTML = "<table><tr><td style='padding:0;border:0;display:none'></td><td>t</td></tr></table>", j = m.getElementsByTagName("td"), qa = j[0].offsetHeight === 0, j[0].style.display = "", j[1].style.display = "none", b.reliableHiddenOffsets = qa && j[0].offsetHeight === 0, m.innerHTML = "", m.style.width = m.style.paddingLeft = "1px", k.boxModel = b.boxModel = m.offsetWidth === 2, typeof m.style.zoom != "undefined" && (m.style.display = "inline", m.style.zoom = 1, b.inlineBlockNeedsLayout = m.offsetWidth === 2, m.style.display = "", m.innerHTML = "<div style='width:4px;'></div>", b.shrinkWrapBlocks = m.offsetWidth !== 2), m.style.cssText = "position:absolute;top:0;left:0;width:1px;height:1px;margin:0;visibility:hidden;border:0;", m.innerHTML = "<div style='position:absolute;top:0;left:0;width:1px;height:1px;margin:0;border:5px solid #000;padding:0;'><div></div></div><table style='position:absolute;top:0;left:0;width:1px;height:1px;margin:0;border:5px solid #000;padding:0;' cellpadding='0' cellspacing='0'><tr><td></td></tr></table>", d = m.firstChild, c = d.firstChild, f = d.nextSibling.firstChild.firstChild, e = {
        doesNotAddBorder: c.offsetTop !== 5,
        doesAddBorderForTableAndCells: f.offsetTop === 5
      }, c.style.position = "fixed", c.style.top = "20px", e.fixedPosition = c.offsetTop === 20 || c.offsetTop === 15, c.style.position = c.style.top = "", d.style.overflow = "hidden", d.style.position = "relative", e.subtractsBorderForOverflowNotVisible = c.offsetTop === -5, e.doesNotIncludeMarginInBodyOffset = v.offsetTop !== 1, v.removeChild(a), m = null, k.extend(b, e))
    });
    return b
  }();
  var fa = /^(?:\{.*\}|\[.*\])$/,
    Ea = /([A-Z])/g;
  k.extend({
    cache: {},
    uuid: 0,
    expando: "jQuery" + (k.fn.jquery + Math.random()).replace(/\D/g, ""),
    noData: {
      embed: true,
      object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
      applet: true
    },
    hasData: function (a) {
      a = a.nodeType ? k.cache[a[k.expando]] : a[k.expando];
      return !!a && !V(a)
    },
    data: function (a, b, d, c) {
      if (k.acceptData(a)) {
        var f, g, h = k.expando,
          v = typeof b == "string",
          j = a.nodeType,
          n = j ? k.cache : a,
          l = j ? a[h] : a[h] && h,
          m = b === "events";
        if (l && n[l] && (m || c || n[l].data) || !(v && d === e)) {
          l || (j ? a[h] = l = ++k.uuid : l = h);
          n[l] || (n[l] = {}, j || (n[l].toJSON = k.noop));
          if (typeof b == "object" || typeof b == "function") c ? n[l] = k.extend(n[l], b) : n[l].data = k.extend(n[l].data, b);
          a = f = n[l];
          c || (f.data || (f.data = {}), f = f.data);
          d !== e && (f[k.camelCase(b)] = d);
          if (m && !f[b]) return a.events;
          v ? (g = f[b], g == null && (g = f[k.camelCase(b)])) : g = f;
          return g
        }
      }
    },
    removeData: function (a, b, d) {
      if (k.acceptData(a)) {
        var c, f, e, g = k.expando,
          v = a.nodeType,
          h = v ? k.cache : a,
          j = v ? a[g] : g;
        if (h[j]) {
          if (b && (c = d ? h[j] : h[j].data)) {
            k.isArray(b) || (b in c ? b = [b] : (b = k.camelCase(b), b in c ? b = [b] : b = b.split(" ")));
            for (f = 0, e = b.length; f < e; f++) delete c[b[f]];
            if (!(d ? V : k.isEmptyObject)(c)) return
          }
          if (!d && (delete h[j].data, !V(h[j]))) return;
          k.support.deleteExpando || !h.setInterval ? delete h[j] : h[j] = null;
          v && (k.support.deleteExpando ? delete a[g] : a.removeAttribute ? a.removeAttribute(g) : a[g] = null)
        }
      }
    },
    _data: function (a, b, d) {
      return k.data(a, b, d, true)
    },
    acceptData: function (a) {
      if (a.nodeName) {
        var b = k.noData[a.nodeName.toLowerCase()];
        if (b) return b !== true && a.getAttribute("classid") === b
      }
      return true
    }
  });
  k.fn.extend({
    data: function (a, b) {
      var d, c, f, g = null;
      if (typeof a == "undefined") {
        if (this.length && (g = k.data(this[0]), this[0].nodeType === 1 && !k._data(this[0], "parsedAttrs"))) {
          c = this[0].attributes;
          for (var h = 0, v = c.length; h < v; h++) f = c[h].name, f.indexOf("data-") === 0 && (f = k.camelCase(f.substring(5)), cb(this[0], f, g[f]));
          k._data(this[0], "parsedAttrs", true)
        }
        return g
      }
      if (typeof a == "object") return this.each(function () {
        k.data(this, a)
      });
      d = a.split(".");
      d[1] = d[1] ? "." + d[1] : "";
      return b === e ? (g = this.triggerHandler("getData" + d[1] + "!", [d[0]]), g === e && this.length && (g = k.data(this[0], a), g = cb(this[0], a, g)), g === e && d[1] ? this.data(d[0]) : g) : this.each(function () {
        var c = k(this),
          f = [d[0], b];
        c.triggerHandler("setData" + d[1] + "!", f);
        k.data(this, a, b);
        c.triggerHandler("changeData" + d[1] + "!", f)
      })
    },
    removeData: function (a) {
      return this.each(function () {
        k.removeData(this, a)
      })
    }
  });
  k.extend({
    _mark: function (a, b) {
      a && (b = (b || "fx") + "mark", k._data(a, b, (k._data(a, b) || 0) + 1))
    },
    _unmark: function (a, b, d) {
      a !== true && (d = b, b = a, a = false);
      if (b) {
        var d = d || "fx",
          c = d + "mark";
        (a = a ? 0 : (k._data(b, c) || 1) - 1) ? k._data(b, c, a) : (k.removeData(b, c, true), O(b, d, "mark"))
      }
    },
    queue: function (a, b, d) {
      var c;
      if (a) return b = (b || "fx") + "queue", c = k._data(a, b), d && (!c || k.isArray(d) ? c = k._data(a, b, k.makeArray(d)) : c.push(d)), c || []
    },
    dequeue: function (a, b) {
      var b = b || "fx",
        d = k.queue(a, b),
        c = d.shift(),
        f = {};
      c === "inprogress" && (c = d.shift());
      c && (b === "fx" && d.unshift("inprogress"), k._data(a, b + ".run", f), c.call(a, function () {
        k.dequeue(a, b)
      }, f));
      d.length || (k.removeData(a, b + "queue " + b + ".run", true), O(a, b, "queue"))
    }
  });
  k.fn.extend({
    queue: function (a, b) {
      typeof a != "string" && (b = a, a = "fx");
      return b === e ? k.queue(this[0], a) : this.each(function () {
        var d = k.queue(this, a, b);
        a === "fx" && d[0] !== "inprogress" && k.dequeue(this, a)
      })
    },
    dequeue: function (a) {
      return this.each(function () {
        k.dequeue(this, a)
      })
    },
    delay: function (a, b) {
      a = k.fx ? k.fx.speeds[a] || a : a;
      b = b || "fx";
      return this.queue(b, function (b, d) {
        var c = setTimeout(b, a);
        d.stop = function () {
          clearTimeout(c)
        }
      })
    },
    clearQueue: function (a) {
      return this.queue(a || "fx", [])
    },
    promise: function (a) {
      function b() {
        --g || d.resolveWith(c, [c])
      }
      typeof a != "string" && (a = e);
      a = a || "fx";
      var d = k.Deferred(),
        c = this,
        f = c.length,
        g = 1,
        h = a + "defer",
        v = a + "queue";
      a += "mark";
      for (var j; f--;) if (j = k.data(c[f], h, e, true) || (k.data(c[f], v, e, true) || k.data(c[f], a, e, true)) && k.data(c[f], h, k.Callbacks("once memory"), true)) g++, j.add(b);
      b();
      return d.promise()
    }
  });
  var P = /[\n\t\r]/g,
    Y = /\s+/,
    M = /\r/g,
    A = /^(?:button|input)$/i,
    S = /^(?:button|input|object|select|textarea)$/i,
    La = /^a(?:rea)?$/i,
    kb = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
    E = k.support.getSetAttribute,
    ea, W, X;
  k.fn.extend({
    attr: function (a, b) {
      return k.access(this, a, b, true, k.attr)
    },
    removeAttr: function (a) {
      return this.each(function () {
        k.removeAttr(this, a)
      })
    },
    prop: function (a, b) {
      return k.access(this, a, b, true, k.prop)
    },
    removeProp: function (a) {
      a = k.propFix[a] || a;
      return this.each(function () {
        try {
          this[a] = e, delete this[a]
        } catch (b) {}
      })
    },
    addClass: function (a) {
      var b, d, c, f, e, g, v;
      if (k.isFunction(a)) return this.each(function (b) {
        k(this).addClass(a.call(this, b, this.className))
      });
      if (a && typeof a == "string") {
        b = a.split(Y);
        for (d = 0, c = this.length; d < c; d++) if (f = this[d], f.nodeType === 1) if (!f.className && b.length === 1) f.className = a;
        else {
          e = " " + f.className + " ";
          for (g = 0, v = b.length; g < v; g++)~e.indexOf(" " + b[g] + " ") || (e += b[g] + " ");
          f.className = k.trim(e)
        }
      }
      return this
    },
    removeClass: function (a) {
      var b, d, c, f, g, h, v;
      if (k.isFunction(a)) return this.each(function (b) {
        k(this).removeClass(a.call(this, b, this.className))
      });
      if (a && typeof a == "string" || a === e) {
        b = (a || "").split(Y);
        for (d = 0, c = this.length; d < c; d++) if (f = this[d], f.nodeType === 1 && f.className) if (a) {
          g = (" " + f.className + " ").replace(P, " ");
          for (h = 0, v = b.length; h < v; h++) g = g.replace(" " + b[h] + " ", " ");
          f.className = k.trim(g)
        } else f.className = ""
      }
      return this
    },
    toggleClass: function (a, b) {
      var d = typeof a,
        c = typeof b == "boolean";
      return k.isFunction(a) ? this.each(function (d) {
        k(this).toggleClass(a.call(this, d, this.className, b), b)
      }) : this.each(function () {
        if (d === "string") for (var f, e = 0, g = k(this), v = b, h = a.split(Y); f = h[e++];) v = c ? v : !g.hasClass(f), g[v ? "addClass" : "removeClass"](f);
        else if (d === "undefined" || d === "boolean") this.className && k._data(this, "__className__", this.className), this.className = this.className || a === false ? "" : k._data(this, "__className__") || ""
      })
    },
    hasClass: function (a) {
      for (var a = " " + a + " ", b = 0, d = this.length; b < d; b++) if (this[b].nodeType === 1 && (" " + this[b].className + " ").replace(P, " ").indexOf(a) > -1) return true;
      return false
    },
    val: function (a) {
      var b, d, c, f = this[0];
      if (arguments.length) return c = k.isFunction(a), this.each(function (d) {
        var f = k(this),
          v;
        if (this.nodeType === 1 && (c ? v = a.call(this, d, f.val()) : v = a, v == null ? v = "" : typeof v == "number" ? v += "" : k.isArray(v) && (v = k.map(v, function (a) {
          return a == null ? "" : a + ""
        })), b = k.valHooks[this.nodeName.toLowerCase()] || k.valHooks[this.type], !b || !("set" in b) || b.set(this, v, "value") === e)) this.value = v
      });
      if (f) {
        if ((b = k.valHooks[f.nodeName.toLowerCase()] || k.valHooks[f.type]) && "get" in b && (d = b.get(f, "value")) !== e) return d;
        d = f.value;
        return typeof d == "string" ? d.replace(M, "") : d == null ? "" : d
      }
    }
  });
  k.extend({
    valHooks: {
      option: {
        get: function (a) {
          var b = a.attributes.value;
          return !b || b.specified ? a.value : a.text
        }
      },
      select: {
        get: function (a) {
          var b, d, c = a.selectedIndex,
            f = [],
            e = a.options,
            g = a.type === "select-one";
          if (c < 0) return null;
          for (a = g ? c : 0, d = g ? c + 1 : e.length; a < d; a++) if (b = e[a], b.selected && (k.support.optDisabled ? !b.disabled : b.getAttribute("disabled") === null) && (!b.parentNode.disabled || !k.nodeName(b.parentNode, "optgroup"))) {
            b = k(b).val();
            if (g) return b;
            f.push(b)
          }
          return g && !f.length && e.length ? k(e[c]).val() : f
        },
        set: function (a, b) {
          var d = k.makeArray(b);
          k(a).find("option").each(function () {
            this.selected = k.inArray(k(this).val(), d) >= 0
          });
          d.length || (a.selectedIndex = -1);
          return d
        }
      }
    },
    attrFn: {
      val: true,
      css: true,
      html: true,
      text: true,
      data: true,
      width: true,
      height: true,
      offset: true
    },
    attr: function (a, b, d, c) {
      var f, g, h, v = a.nodeType;
      if (a && v !== 3 && v !== 8 && v !== 2) {
        if (c && b in k.attrFn) return k(a)[b](d);
        if (typeof a.getAttribute == "undefined") return k.prop(a, b, d);
        h = v !== 1 || !k.isXMLDoc(a);
        h && (b = b.toLowerCase(), g = k.attrHooks[b] || (kb.test(b) ? W : ea));
        if (d !== e) {
          if (d === null) {
            k.removeAttr(a, b);
            return
          }
          if (g && "set" in g && h && (f = g.set(a, d, b)) !== e) return f;
          a.setAttribute(b, "" + d);
          return d
        }
        if (g && "get" in g && h && (f = g.get(a, b)) !== null) return f;
        f = a.getAttribute(b);
        return f === null ? e : f
      }
    },
    removeAttr: function (a, b) {
      var d, c, f, e, g = 0;
      if (b && a.nodeType === 1) for (c = b.toLowerCase().split(Y), e = c.length; g < e; g++) f = c[g], f && (d = k.propFix[f] || f, k.attr(a, f, ""), a.removeAttribute(E ? f : d), kb.test(f) && d in a && (a[d] = false))
    },
    attrHooks: {
      type: {
        set: function (a, b) {
          if (A.test(a.nodeName) && a.parentNode) k.error("type property can't be changed");
          else if (!k.support.radioValue && b === "radio" && k.nodeName(a, "input")) {
            var d = a.value;
            a.setAttribute("type", b);
            d && (a.value = d);
            return b
          }
        }
      },
      value: {
        get: function (a, b) {
          return ea && k.nodeName(a, "button") ? ea.get(a, b) : b in a ? a.value : null
        },
        set: function (a, b, d) {
          if (ea && k.nodeName(a, "button")) return ea.set(a, b, d);
          a.value = b
        }
      }
    },
    propFix: {
      tabindex: "tabIndex",
      readonly: "readOnly",
      "for": "htmlFor",
      "class": "className",
      maxlength: "maxLength",
      cellspacing: "cellSpacing",
      cellpadding: "cellPadding",
      rowspan: "rowSpan",
      colspan: "colSpan",
      usemap: "useMap",
      frameborder: "frameBorder",
      contenteditable: "contentEditable"
    },
    prop: function (a, b, d) {
      var c, f, g, h = a.nodeType;
      if (a && h !== 3 && h !== 8 && h !== 2) return g = h !== 1 || !k.isXMLDoc(a), g && (b = k.propFix[b] || b, f = k.propHooks[b]), d !== e ? f && "set" in f && (c = f.set(a, d, b)) !== e ? c : a[b] = d : f && "get" in f && (c = f.get(a, b)) !== null ? c : a[b]
    },
    propHooks: {
      tabIndex: {
        get: function (a) {
          var b = a.getAttributeNode("tabindex");
          return b && b.specified ? parseInt(b.value, 10) : S.test(a.nodeName) || La.test(a.nodeName) && a.href ? 0 : e
        }
      }
    }
  });
  k.attrHooks.tabindex = k.propHooks.tabIndex;
  W = {
    get: function (a, b) {
      var d, c = k.prop(a, b);
      return c === true || typeof c != "boolean" && (d = a.getAttributeNode(b)) && d.nodeValue !== false ? b.toLowerCase() : e
    },
    set: function (a, b, d) {
      var c;
      b === false ? k.removeAttr(a, d) : (c = k.propFix[d] || d, c in a && (a[c] = true), a.setAttribute(d, d.toLowerCase()));
      return d
    }
  };
  E || (X = {
    name: true,
    id: true
  }, ea = k.valHooks.button = {
    get: function (a, b) {
      var d;
      return (d = a.getAttributeNode(b)) && (X[b] ? d.nodeValue !== "" : d.specified) ? d.nodeValue : e
    },
    set: function (a, b, d) {
      var c = a.getAttributeNode(d);
      c || (c = H.createAttribute(d), a.setAttributeNode(c));
      return c.nodeValue = b + ""
    }
  }, k.attrHooks.tabindex.set = ea.set, k.each(["width", "height"], function (a, b) {
    k.attrHooks[b] = k.extend(k.attrHooks[b], {
      set: function (a, d) {
        if (d === "") return a.setAttribute(b, "auto"), d
      }
    })
  }), k.attrHooks.contenteditable = {
    get: ea.get,
    set: function (a, b, d) {
      b === "" && (b = "false");
      ea.set(a, b, d)
    }
  });
  k.support.hrefNormalized || k.each(["href", "src", "width", "height"], function (a, b) {
    k.attrHooks[b] = k.extend(k.attrHooks[b], {
      get: function (a) {
        a = a.getAttribute(b, 2);
        return a === null ? e : a
      }
    })
  });
  k.support.style || (k.attrHooks.style = {
    get: function (a) {
      return a.style.cssText.toLowerCase() || e
    },
    set: function (a, b) {
      return a.style.cssText = "" + b
    }
  });
  k.support.optSelected || (k.propHooks.selected = k.extend(k.propHooks.selected, {
    get: function () {
      return null
    }
  }));
  k.support.enctype || (k.propFix.enctype = "encoding");
  k.support.checkOn || k.each(["radio", "checkbox"], function () {
    k.valHooks[this] = {
      get: function (a) {
        return a.getAttribute("value") === null ? "on" : a.value
      }
    }
  });
  k.each(["radio", "checkbox"], function () {
    k.valHooks[this] = k.extend(k.valHooks[this], {
      set: function (a, b) {
        if (k.isArray(b)) return a.checked = k.inArray(k(a).val(), b) >= 0
      }
    })
  });
  var I = /^(?:textarea|input|select)$/i,
    ua = /^([^\.]*)?(?:\.(.+))?$/,
    ya = /\bhover(\.\S+)?\b/,
    Fa = /^key/,
    za = /^(?:mouse|contextmenu)|click/,
    la = /^(?:focusinfocus|focusoutblur)$/,
    aa = /^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/,
    Ra = function (a) {
      (a = aa.exec(a)) && (a[1] = (a[1] || "").toLowerCase(), a[3] = a[3] && RegExp("(?:^|\\s)" + a[3] + "(?:\\s|$)"));
      return a
    },
    hb = function (a) {
      return k.event.special.hover ? a : a.replace(ya, "mouseenter$1 mouseleave$1")
    };
  k.event = {
    add: function (a, b, d, c, f) {
      var g, h, v, j, n, l, m, q, o, F;
      if (!(a.nodeType === 3 || a.nodeType === 8 || !b || !d || !(g = k._data(a)))) {
        d.handler && (q = d, d = q.handler);
        d.guid || (d.guid = k.guid++);
        v = g.events;
        v || (g.events = v = {});
        h = g.handle;
        h || (g.handle = h = function (a) {
          return typeof k != "undefined" && (!a || k.event.triggered !== a.type) ? k.event.dispatch.apply(h.elem, arguments) : e
        }, h.elem = a);
        b = k.trim(hb(b)).split(" ");
        for (g = 0; g < b.length; g++) {
          j = ua.exec(b[g]) || [];
          n = j[1];
          l = (j[2] || "").split(".").sort();
          F = k.event.special[n] || {};
          n = (f ? F.delegateType : F.bindType) || n;
          F = k.event.special[n] || {};
          m = k.extend({
            type: n,
            origType: j[1],
            data: c,
            handler: d,
            guid: d.guid,
            selector: f,
            quick: Ra(f),
            namespace: l.join(".")
          }, q);
          o = v[n];
          if (!o && (o = v[n] = [], o.delegateCount = 0, !F.setup || F.setup.call(a, c, l, h) === false)) a.addEventListener ? a.addEventListener(n, h, false) : a.attachEvent && a.attachEvent("on" + n, h);
          F.add && (F.add.call(a, m), m.handler.guid || (m.handler.guid = d.guid));
          f ? o.splice(o.delegateCount++, 0, m) : o.push(m);
          k.event.global[n] = true
        }
        a = null
      }
    },
    global: {},
    remove: function (a, b, d, c, f) {
      var e = k.hasData(a) && k._data(a),
        g, v, h, j, n, l, m, o, q, F, z, p;
      if (e && (o = e.events)) {
        b = k.trim(hb(b || "")).split(" ");
        for (g = 0; g < b.length; g++) if (v = ua.exec(b[g]) || [], h = j = v[1], n = v[2], h) {
          q = k.event.special[h] || {};
          h = (c ? q.delegateType : q.bindType) || h;
          z = o[h] || [];
          l = z.length;
          n = n ? RegExp("(^|\\.)" + n.split(".").sort().join("\\.(?:.*\\.)?") + "(\\.|$)") : null;
          for (m = 0; m < z.length; m++) p = z[m], (f || j === p.origType) && (!d || d.guid === p.guid) && (!n || n.test(p.namespace)) && (!c || c === p.selector || c === "**" && p.selector) && (z.splice(m--, 1), p.selector && z.delegateCount--, q.remove && q.remove.call(a, p));
          z.length === 0 && l !== z.length && ((!q.teardown || q.teardown.call(a, n) === false) && k.removeEvent(a, h, e.handle), delete o[h])
        } else for (h in o) k.event.remove(a, h + b[g], d, c, true);
        k.isEmptyObject(o) && (F = e.handle, F && (F.elem = null), k.removeData(a, ["events", "handle"], true))
      }
    },
    customEvent: {
      getData: true,
      setData: true,
      changeData: true
    },
    trigger: function (b, d, c, f) {
      if (!c || c.nodeType !== 3 && c.nodeType !== 8) {
        var g = b.type || b,
          h = [],
          j, v, n, l, R, m, o;
        if (!la.test(g + k.event.triggered) && (g.indexOf("!") >= 0 && (g = g.slice(0, -1), j = true), g.indexOf(".") >= 0 && (h = g.split("."), g = h.shift(), h.sort()), c && !k.event.customEvent[g] || k.event.global[g])) if (b = typeof b == "object" ? b[k.expando] ? b : new k.Event(g, b) : new k.Event(g), b.type = g, b.isTrigger = true, b.exclusive = j, b.namespace = h.join("."), b.namespace_re = b.namespace ? RegExp("(^|\\.)" + h.join("\\.(?:.*\\.)?") + "(\\.|$)") : null, h = g.indexOf(":") < 0 ? "on" + g : "", c) {
          if (b.result = e, b.target || (b.target = c), d = d != null ? k.makeArray(d) : [], d.unshift(b), j = k.event.special[g] || {}, !(j.trigger && j.trigger.apply(c, d) === false)) {
            m = [
              [c, j.bindType || g]
            ];
            if (!f && !j.noBubble && !k.isWindow(c)) {
              for (o = j.delegateType || g, n = la.test(o + g) ? c : c.parentNode, l = null; n; n = n.parentNode) m.push([n, o]), l = n;
              l && l === c.ownerDocument && m.push([l.defaultView || l.parentWindow || a, o])
            }
            for (v = 0; v < m.length && !b.isPropagationStopped(); v++) n = m[v][0], b.type = m[v][1], R = (k._data(n, "events") || {})[b.type] && k._data(n, "handle"), R && R.apply(n, d), R = h && n[h], R && k.acceptData(n) && R.apply(n, d) === false && b.preventDefault();
            b.type = g;
            !f && !b.isDefaultPrevented() && (!j._default || j._default.apply(c.ownerDocument, d) === false) && (g !== "click" || !k.nodeName(c, "a")) && k.acceptData(c) && h && c[g] && (g !== "focus" && g !== "blur" || b.target.offsetWidth !== 0) && !k.isWindow(c) && (l = c[h], l && (c[h] = null), k.event.triggered = g, c[g](), k.event.triggered = e, l && (c[h] = l));
            return b.result
          }
        } else for (v in c = k.cache, c) c[v].events && c[v].events[g] && k.event.trigger(b, d, c[v].handle.elem, true)
      }
    },
    dispatch: function (b) {
      var b = k.event.fix(b || a.event),
        d = (k._data(this, "events") || {})[b.type] || [],
        c = d.delegateCount,
        f = [].slice.call(arguments, 0),
        g = !b.exclusive && !b.namespace,
        h = [],
        j, v, n, l, R, m, o, q, F;
      f[0] = b;
      b.delegateTarget = this;
      if (c && !b.target.disabled && (!b.button || b.type !== "click")) {
        n = k(this);
        n.context = this.ownerDocument || this;
        for (v = b.target; v != this; v = v.parentNode || this) {
          R = {};
          o = [];
          n[0] = v;
          for (j = 0; j < c; j++) {
            q = d[j];
            F = q.selector;
            if (R[F] === e) {
              var z = R,
                p = F,
                s;
              if (q.quick) {
                s = q.quick;
                var Ia = v.attributes || {};
                s = (!s[1] || v.nodeName.toLowerCase() === s[1]) && (!s[2] || (Ia.id || {}).value === s[2]) && (!s[3] || s[3].test((Ia["class"] || {}).value))
              } else s = n.is(F);
              z[p] = s
            }
            R[F] && o.push(q)
          }
          o.length && h.push({
            elem: v,
            matches: o
          })
        }
      }
      d.length > c && h.push({
        elem: this,
        matches: d.slice(c)
      });
      for (j = 0; j < h.length && !b.isPropagationStopped(); j++) {
        m = h[j];
        b.currentTarget = m.elem;
        for (d = 0; d < m.matches.length && !b.isImmediatePropagationStopped(); d++) if (q = m.matches[d], g || !b.namespace && !q.namespace || b.namespace_re && b.namespace_re.test(q.namespace)) b.data = q.data, b.handleObj = q, l = ((k.event.special[q.origType] || {}).handle || q.handler).apply(m.elem, f), l !== e && (b.result = l, l === false && (b.preventDefault(), b.stopPropagation()))
      }
      return b.result
    },
    props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
    fixHooks: {},
    keyHooks: {
      props: "char charCode key keyCode".split(" "),
      filter: function (a, b) {
        a.which == null && (a.which = b.charCode != null ? b.charCode : b.keyCode);
        return a
      }
    },
    mouseHooks: {
      props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
      filter: function (a, b) {
        var d, c, f, g = b.button,
          h = b.fromElement;
        a.pageX == null && b.clientX != null && (d = a.target.ownerDocument || H, c = d.documentElement, f = d.body, a.pageX = b.clientX + (c && c.scrollLeft || f && f.scrollLeft || 0) - (c && c.clientLeft || f && f.clientLeft || 0), a.pageY = b.clientY + (c && c.scrollTop || f && f.scrollTop || 0) - (c && c.clientTop || f && f.clientTop || 0));
        !a.relatedTarget && h && (a.relatedTarget = h === a.target ? b.toElement : h);
        !a.which && g !== e && (a.which = g & 1 ? 1 : g & 2 ? 3 : g & 4 ? 2 : 0);
        return a
      }
    },
    fix: function (a) {
      if (a[k.expando]) return a;
      var b, d, c = a,
        f = k.event.fixHooks[a.type] || {},
        g = f.props ? this.props.concat(f.props) : this.props,
        a = k.Event(c);
      for (b = g.length; b;) d = g[--b], a[d] = c[d];
      a.target || (a.target = c.srcElement || H);
      a.target.nodeType === 3 && (a.target = a.target.parentNode);
      a.metaKey === e && (a.metaKey = a.ctrlKey);
      return f.filter ? f.filter(a, c) : a
    },
    special: {
      ready: {
        setup: k.bindReady
      },
      load: {
        noBubble: true
      },
      focus: {
        delegateType: "focusin"
      },
      blur: {
        delegateType: "focusout"
      },
      beforeunload: {
        setup: function (a, b, d) {
          k.isWindow(this) && (this.onbeforeunload = d)
        },
        teardown: function (a, b) {
          this.onbeforeunload === b && (this.onbeforeunload = null)
        }
      }
    },
    simulate: function (a, b, d, c) {
      a = k.extend(new k.Event, d, {
        type: a,
        isSimulated: true,
        originalEvent: {}
      });
      c ? k.event.trigger(a, null, b) : k.event.dispatch.call(b, a);
      a.isDefaultPrevented() && d.preventDefault()
    }
  };
  k.event.handle = k.event.dispatch;
  k.removeEvent = H.removeEventListener ?
  function (a, b, d) {
    a.removeEventListener && a.removeEventListener(b, d, false)
  } : function (a, b, d) {
    a.detachEvent && a.detachEvent("on" + b, d)
  };
  k.Event = function (a, b) {
    if (!(this instanceof k.Event)) return new k.Event(a, b);
    a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || a.returnValue === false || a.getPreventDefault && a.getPreventDefault() ? q : K) : this.type = a;
    b && k.extend(this, b);
    this.timeStamp = a && a.timeStamp || k.now();
    this[k.expando] = true
  };
  k.Event.prototype = {
    preventDefault: function () {
      this.isDefaultPrevented = q;
      var a = this.originalEvent;
      !a || (a.preventDefault ? a.preventDefault() : a.returnValue = false)
    },
    stopPropagation: function () {
      this.isPropagationStopped = q;
      var a = this.originalEvent;
      !a || (a.stopPropagation && a.stopPropagation(), a.cancelBubble = true)
    },
    stopImmediatePropagation: function () {
      this.isImmediatePropagationStopped = q;
      this.stopPropagation()
    },
    isDefaultPrevented: K,
    isPropagationStopped: K,
    isImmediatePropagationStopped: K
  };
  k.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout"
  }, function (a, b) {
    k.event.special[a] = {
      delegateType: b,
      bindType: b,
      handle: function (a) {
        var d = a.relatedTarget,
          c = a.handleObj,
          f;
        if (!d || d !== this && !k.contains(this, d)) a.type = c.origType, f = c.handler.apply(this, arguments), a.type = b;
        return f
      }
    }
  });
  k.support.submitBubbles || (k.event.special.submit = {
    setup: function () {
      if (k.nodeName(this, "form")) return false;
      k.event.add(this, "click._submit keypress._submit", function (a) {
        a = a.target;
        (a = k.nodeName(a, "input") || k.nodeName(a, "button") ? a.form : e) && !a._submit_attached && (k.event.add(a, "submit._submit", function (a) {
          this.parentNode && !a.isTrigger && k.event.simulate("submit", this.parentNode, a, true)
        }), a._submit_attached = true)
      })
    },
    teardown: function () {
      if (k.nodeName(this, "form")) return false;
      k.event.remove(this, "._submit")
    }
  });
  k.support.changeBubbles || (k.event.special.change = {
    setup: function () {
      if (I.test(this.nodeName)) {
        if (this.type === "checkbox" || this.type === "radio") k.event.add(this, "propertychange._change", function (a) {
          a.originalEvent.propertyName === "checked" && (this._just_changed = true)
        }), k.event.add(this, "click._change", function (a) {
          this._just_changed && !a.isTrigger && (this._just_changed = false, k.event.simulate("change", this, a, true))
        });
        return false
      }
      k.event.add(this, "beforeactivate._change", function (a) {
        a = a.target;
        I.test(a.nodeName) && !a._change_attached && (k.event.add(a, "change._change", function (a) {
          this.parentNode && !a.isSimulated && !a.isTrigger && k.event.simulate("change", this.parentNode, a, true)
        }), a._change_attached = true)
      })
    },
    handle: function (a) {
      var b = a.target;
      if (this !== b || a.isSimulated || a.isTrigger || b.type !== "radio" && b.type !== "checkbox") return a.handleObj.handler.apply(this, arguments)
    },
    teardown: function () {
      k.event.remove(this, "._change");
      return I.test(this.nodeName)
    }
  });
  k.support.focusinBubbles || k.each({
    focus: "focusin",
    blur: "focusout"
  }, function (a, b) {
    var d = 0,
      c = function (a) {
        k.event.simulate(b, a.target, k.event.fix(a), true)
      };
    k.event.special[b] = {
      setup: function () {
        d++ === 0 && H.addEventListener(a, c, true)
      },
      teardown: function () {
        --d === 0 && H.removeEventListener(a, c, true)
      }
    }
  });
  k.fn.extend({
    on: function (a, b, d, c, f) {
      var g, h;
      if (typeof a == "object") {
        typeof b != "string" && (d = b, b = e);
        for (h in a) this.on(h, b, d, a[h], f);
        return this
      }
      d == null && c == null ? (c = b, d = b = e) : c == null && (typeof b == "string" ? (c = d, d = e) : (c = d, d = b, b = e));
      if (c === false) c = K;
      else if (!c) return this;
      f === 1 && (g = c, c = function (a) {
        k().off(a);
        return g.apply(this, arguments)
      }, c.guid = g.guid || (g.guid = k.guid++));
      return this.each(function () {
        k.event.add(this, a, c, d, b)
      })
    },
    one: function (a, b, d, c) {
      return this.on.call(this, a, b, d, c, 1)
    },
    off: function (a, b, d) {
      if (a && a.preventDefault && a.handleObj) {
        var c = a.handleObj;
        k(a.delegateTarget).off(c.namespace ? c.type + "." + c.namespace : c.type, c.selector, c.handler);
        return this
      }
      if (typeof a == "object") {
        for (c in a) this.off(c, b, a[c]);
        return this
      }
      if (b === false || typeof b == "function") d = b, b = e;
      d === false && (d = K);
      return this.each(function () {
        k.event.remove(this, a, d, b)
      })
    },
    bind: function (a, b, d) {
      return this.on(a, null, b, d)
    },
    unbind: function (a, b) {
      return this.off(a, null, b)
    },
    live: function (a, b, d) {
      k(this.context).on(a, this.selector, b, d);
      return this
    },
    die: function (a, b) {
      k(this.context).off(a, this.selector || "**", b);
      return this
    },
    delegate: function (a, b, d, c) {
      return this.on(b, a, d, c)
    },
    undelegate: function (a, b, d) {
      return arguments.length == 1 ? this.off(a, "**") : this.off(b, a, d)
    },
    trigger: function (a, b) {
      return this.each(function () {
        k.event.trigger(a, b, this)
      })
    },
    triggerHandler: function (a, b) {
      if (this[0]) return k.event.trigger(a, b, this[0], true)
    },
    toggle: function (a) {
      var b = arguments,
        d = a.guid || k.guid++,
        c = 0,
        f = function (d) {
          var f = (k._data(this, "lastToggle" + a.guid) || 0) % c;
          k._data(this, "lastToggle" + a.guid, f + 1);
          d.preventDefault();
          return b[f].apply(this, arguments) || false
        };
      for (f.guid = d; c < b.length;) b[c++].guid = d;
      return this.click(f)
    },
    hover: function (a, b) {
      return this.mouseenter(a).mouseleave(b || a)
    }
  });
  k.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (a, b) {
    k.fn[b] = function (a, d) {
      d == null && (d = a, a = null);
      return arguments.length > 0 ? this.on(b, null, a, d) : this.trigger(b)
    };
    k.attrFn && (k.attrFn[b] = true);
    Fa.test(b) && (k.event.fixHooks[b] = k.event.keyHooks);
    za.test(b) && (k.event.fixHooks[b] = k.event.mouseHooks)
  });
  (function () {
    function a(b, d, f, e, g, v) {
      for (var g = 0, h = e.length; g < h; g++) {
        var j = e[g];
        if (j) {
          for (var n = false, j = j[b]; j;) {
            if (j[c] === f) {
              n = e[j.sizset];
              break
            }
            if (j.nodeType === 1) if (v || (j[c] = f, j.sizset = g), typeof d != "string") {
              if (j === d) {
                n = true;
                break
              }
            } else if (m.filter(d, [j]).length > 0) {
              n = j;
              break
            }
            j = j[b]
          }
          e[g] = n
        }
      }
    }
    function b(a, d, f, e, g, v) {
      for (var g = 0, h = e.length; g < h; g++) {
        var j = e[g];
        if (j) {
          for (var n = false, j = j[a]; j;) {
            if (j[c] === f) {
              n = e[j.sizset];
              break
            }
            j.nodeType === 1 && !v && (j[c] = f, j.sizset = g);
            if (j.nodeName.toLowerCase() === d) {
              n = j;
              break
            }
            j = j[a]
          }
          e[g] = n
        }
      }
    }
    var d = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
      c = "sizcache" + (Math.random() + "").replace(".", ""),
      f = 0,
      g = Object.prototype.toString,
      h = false,
      v = true,
      j = /\\/g,
      n = /\r\n/g,
      l = /\W/;
    [0, 0].sort(function () {
      v = false;
      return 0
    });
    var m = function (a, b, c, f) {
        c = c || [];
        b = b || H;
        var e = b;
        if (b.nodeType !== 1 && b.nodeType !== 9) return [];
        if (!a || typeof a != "string") return c;
        var v, h, j, n, l, N, G, k = true,
          R = m.isXML(b),
          q = [],
          ba = a;
        do if (d.exec(""), v = d.exec(ba), v && (ba = v[3], q.push(v[1]), v[2])) {
          n = v[3];
          break
        }
        while (v);
        if (q.length > 1 && F.exec(a)) if (q.length === 2 && o.relative[q[0]]) h = w(q[0] + q[1], b, f);
        else for (h = o.relative[q[0]] ? [b] : m(q.shift(), b); q.length;) a = q.shift(), o.relative[a] && (a += q.shift()), h = w(a, h, f);
        else if (!f && q.length > 1 && b.nodeType === 9 && !R && o.match.ID.test(q[0]) && !o.match.ID.test(q[q.length - 1]) && (l = m.find(q.shift(), b, R), b = l.expr ? m.filter(l.expr, l.set)[0] : l.set[0]), b) for (l = f ? {
          expr: q.pop(),
          set: s(f)
        } : m.find(q.pop(), q.length === 1 && (q[0] === "~" || q[0] === "+") && b.parentNode ? b.parentNode : b, R), h = l.expr ? m.filter(l.expr, l.set) : l.set, q.length > 0 ? j = s(h) : k = false; q.length;) N = q.pop(), G = N, o.relative[N] ? G = q.pop() : N = "", G == null && (G = b), o.relative[N](j, G, R);
        else j = [];
        j || (j = h);
        j || m.error(N || a);
        if (g.call(j) === "[object Array]") if (k) if (b && b.nodeType === 1) for (a = 0; j[a] != null; a++) j[a] && (j[a] === true || j[a].nodeType === 1 && m.contains(b, j[a])) && c.push(h[a]);
        else for (a = 0; j[a] != null; a++) j[a] && j[a].nodeType === 1 && c.push(h[a]);
        else c.push.apply(c, j);
        else s(j, c);
        n && (m(n, e, c, f), m.uniqueSort(c));
        return c
      };
    m.uniqueSort = function (a) {
      if (va && (h = v, a.sort(va), h)) for (var b = 1; b < a.length; b++) a[b] === a[b - 1] && a.splice(b--, 1);
      return a
    };
    m.matches = function (a, b) {
      return m(a, null, null, b)
    };
    m.matchesSelector = function (a, b) {
      return m(b, null, null, [a]).length > 0
    };
    m.find = function (a, b, d) {
      var c, f, e, g, v, h;
      if (!a) return [];
      for (f = 0, e = o.order.length; f < e; f++) if (v = o.order[f], g = o.leftMatch[v].exec(a)) if (h = g[1], g.splice(1, 1), h.substr(h.length - 1) !== "\\" && (g[1] = (g[1] || "").replace(j, ""), c = o.find[v](g, b, d), c != null)) {
        a = a.replace(o.match[v], "");
        break
      }
      c || (c = typeof b.getElementsByTagName != "undefined" ? b.getElementsByTagName("*") : []);
      return {
        set: c,
        expr: a
      }
    };
    m.filter = function (a, b, d, c) {
      for (var f, g, v, h, j, n, l, N, G = a, k = [], R = b, q = b && b[0] && m.isXML(b[0]); a && b.length;) {
        for (v in o.filter) if ((f = o.leftMatch[v].exec(a)) != null && f[2]) if (n = o.filter[v], j = f[1], g = false, f.splice(1, 1), j.substr(j.length - 1) !== "\\") {
          R === k && (k = []);
          if (o.preFilter[v]) if (f = o.preFilter[v](f, R, d, k, c, q)) {
            if (f === true) continue
          } else g = h = true;
          if (f) for (l = 0;
          (j = R[l]) != null; l++) j && (h = n(j, f, l, R), N = c ^ h, d && h != null ? N ? g = true : R[l] = false : N && (k.push(j), g = true));
          if (h !== e) {
            d || (R = k);
            a = a.replace(o.match[v], "");
            if (!g) return [];
            break
          }
        }
        if (a === G) if (g == null) m.error(a);
        else break;
        G = a
      }
      return R
    };
    m.error = function (a) {
      throw Error("Syntax error, unrecognized expression: " + a);
    };
    var q = m.getText = function (a) {
        var b, d;
        b = a.nodeType;
        var c = "";
        if (b) if (b === 1 || b === 9) {
          if (typeof a.textContent == "string") return a.textContent;
          if (typeof a.innerText == "string") return a.innerText.replace(n, "");
          for (a = a.firstChild; a; a = a.nextSibling) c += q(a)
        } else {
          if (b === 3 || b === 4) return a.nodeValue
        } else for (b = 0; d = a[b]; b++) d.nodeType !== 8 && (c += q(d));
        return c
      },
      o = m.selectors = {
        order: ["ID", "NAME", "TAG"],
        match: {
          ID: /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
          CLASS: /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
          NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,
          ATTR: /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,
          TAG: /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,
          CHILD: /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,
          POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,
          PSEUDO: /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/
        },
        leftMatch: {},
        attrMap: {
          "class": "className",
          "for": "htmlFor"
        },
        attrHandle: {
          href: function (a) {
            return a.getAttribute("href")
          },
          type: function (a) {
            return a.getAttribute("type")
          }
        },
        relative: {
          "+": function (a, b) {
            var d = typeof b == "string",
              c = d && !l.test(b),
              d = d && !c;
            c && (b = b.toLowerCase());
            for (var c = 0, f = a.length, e; c < f; c++) if (e = a[c]) {
              for (;
              (e = e.previousSibling) && e.nodeType !== 1;);
              a[c] = d || e && e.nodeName.toLowerCase() === b ? e || false : e === b
            }
            d && m.filter(b, a, true)
          },
          ">": function (a, b) {
            var d, c = typeof b == "string",
              f = 0,
              e = a.length;
            if (c && !l.test(b)) for (b = b.toLowerCase(); f < e; f++) {
              if (d = a[f]) d = d.parentNode, a[f] = d.nodeName.toLowerCase() === b ? d : false
            } else {
              for (; f < e; f++) d = a[f], d && (a[f] = c ? d.parentNode : d.parentNode === b);
              c && m.filter(b, a, true)
            }
          },
          "": function (d, c, e) {
            var g, v = f++,
              h = a;
            typeof c == "string" && !l.test(c) && (c = c.toLowerCase(), g = c, h = b);
            h("parentNode", c, v, d, g, e)
          },
          "~": function (d, c, e) {
            var g, v = f++,
              h = a;
            typeof c == "string" && !l.test(c) && (c = c.toLowerCase(), g = c, h = b);
            h("previousSibling", c, v, d, g, e)
          }
        },
        find: {
          ID: function (a, b, d) {
            if (typeof b.getElementById != "undefined" && !d) return (a = b.getElementById(a[1])) && a.parentNode ? [a] : []
          },
          NAME: function (a, b) {
            if (typeof b.getElementsByName != "undefined") {
              for (var d = [], c = b.getElementsByName(a[1]), f = 0, e = c.length; f < e; f++) c[f].getAttribute("name") === a[1] && d.push(c[f]);
              return d.length === 0 ? null : d
            }
          },
          TAG: function (a, b) {
            if (typeof b.getElementsByTagName != "undefined") return b.getElementsByTagName(a[1])
          }
        },
        preFilter: {
          CLASS: function (a, b, d, c, f, e) {
            a = " " + a[1].replace(j, "") + " ";
            if (e) return a;
            for (var e = 0, g;
            (g = b[e]) != null; e++) g && (f ^ (g.className && (" " + g.className + " ").replace(/[\t\n\r]/g, " ").indexOf(a) >= 0) ? d || c.push(g) : d && (b[e] = false));
            return false
          },
          ID: function (a) {
            return a[1].replace(j, "")
          },
          TAG: function (a) {
            return a[1].replace(j, "").toLowerCase()
          },
          CHILD: function (a) {
            if (a[1] === "nth") {
              a[2] || m.error(a[0]);
              a[2] = a[2].replace(/^\+|\s*/g, "");
              var b = /(-?)(\d*)(?:n([+\-]?\d*))?/.exec(a[2] === "even" && "2n" || a[2] === "odd" && "2n+1" || !/\D/.test(a[2]) && "0n+" + a[2] || a[2]);
              a[2] = b[1] + (b[2] || 1) - 0;
              a[3] = b[3] - 0
            } else a[2] && m.error(a[0]);
            a[0] = f++;
            return a
          },
          ATTR: function (a, b, d, c, f, e) {
            b = a[1] = a[1].replace(j, "");
            !e && o.attrMap[b] && (a[1] = o.attrMap[b]);
            a[4] = (a[4] || a[5] || "").replace(j, "");
            a[2] === "~=" && (a[4] = " " + a[4] + " ");
            return a
          },
          PSEUDO: function (a, b, c, f, e) {
            if (a[1] === "not") if ((d.exec(a[3]) || "").length > 1 || /^\w/.test(a[3])) a[3] = m(a[3], null, null, b);
            else return a = m.filter(a[3], b, c, 1 ^ e), c || f.push.apply(f, a), false;
            else if (o.match.POS.test(a[0]) || o.match.CHILD.test(a[0])) return true;
            return a
          },
          POS: function (a) {
            a.unshift(true);
            return a
          }
        },
        filters: {
          enabled: function (a) {
            return a.disabled === false && a.type !== "hidden"
          },
          disabled: function (a) {
            return a.disabled === true
          },
          checked: function (a) {
            return a.checked === true
          },
          selected: function (a) {
            return a.selected === true
          },
          parent: function (a) {
            return !!a.firstChild
          },
          empty: function (a) {
            return !a.firstChild
          },
          has: function (a, b, d) {
            return !!m(d[3], a).length
          },
          header: function (a) {
            return /h\d/i.test(a.nodeName)
          },
          text: function (a) {
            var b = a.getAttribute("type"),
              d = a.type;
            return a.nodeName.toLowerCase() === "input" && "text" === d && (b === d || b === null)
          },
          radio: function (a) {
            return a.nodeName.toLowerCase() === "input" && "radio" === a.type
          },
          checkbox: function (a) {
            return a.nodeName.toLowerCase() === "input" && "checkbox" === a.type
          },
          file: function (a) {
            return a.nodeName.toLowerCase() === "input" && "file" === a.type
          },
          password: function (a) {
            return a.nodeName.toLowerCase() === "input" && "password" === a.type
          },
          submit: function (a) {
            var b = a.nodeName.toLowerCase();
            return (b === "input" || b === "button") && "submit" === a.type
          },
          image: function (a) {
            return a.nodeName.toLowerCase() === "input" && "image" === a.type
          },
          reset: function (a) {
            var b = a.nodeName.toLowerCase();
            return (b === "input" || b === "button") && "reset" === a.type
          },
          button: function (a) {
            var b = a.nodeName.toLowerCase();
            return b === "input" && "button" === a.type || b === "button"
          },
          input: function (a) {
            return /input|select|textarea|button/i.test(a.nodeName)
          },
          focus: function (a) {
            return a === a.ownerDocument.activeElement
          }
        },
        setFilters: {
          first: function (a, b) {
            return b === 0
          },
          last: function (a, b, d, c) {
            return b === c.length - 1
          },
          even: function (a, b) {
            return b % 2 === 0
          },
          odd: function (a, b) {
            return b % 2 === 1
          },
          lt: function (a, b, d) {
            return b < d[3] - 0
          },
          gt: function (a, b, d) {
            return b > d[3] - 0
          },
          nth: function (a, b, d) {
            return d[3] - 0 === b
          },
          eq: function (a, b, d) {
            return d[3] - 0 === b
          }
        },
        filter: {
          PSEUDO: function (a, b, d, c) {
            var f = b[1],
              e = o.filters[f];
            if (e) return e(a, d, b, c);
            if (f === "contains") return (a.textContent || a.innerText || q([a]) || "").indexOf(b[3]) >= 0;
            if (f === "not") {
              b = b[3];
              d = 0;
              for (c = b.length; d < c; d++) if (b[d] === a) return false;
              return true
            }
            m.error(f)
          },
          CHILD: function (a, b) {
            var d, f, e, g, v, h;
            d = b[1];
            h = a;
            switch (d) {
            case "only":
            case "first":
              for (; h = h.previousSibling;) if (h.nodeType === 1) return false;
              if (d === "first") return true;
              h = a;
            case "last":
              for (; h = h.nextSibling;) if (h.nodeType === 1) return false;
              return true;
            case "nth":
              d = b[2];
              f = b[3];
              if (d === 1 && f === 0) return true;
              e = b[0];
              g = a.parentNode;
              if (g && (g[c] !== e || !a.nodeIndex)) {
                v = 0;
                for (h = g.firstChild; h; h = h.nextSibling) h.nodeType === 1 && (h.nodeIndex = ++v);
                g[c] = e
              }
              h = a.nodeIndex - f;
              return d === 0 ? h === 0 : h % d === 0 && h / d >= 0
            }
          },
          ID: function (a, b) {
            return a.nodeType === 1 && a.getAttribute("id") === b
          },
          TAG: function (a, b) {
            return b === "*" && a.nodeType === 1 || !! a.nodeName && a.nodeName.toLowerCase() === b
          },
          CLASS: function (a, b) {
            return (" " + (a.className || a.getAttribute("class")) + " ").indexOf(b) > -1
          },
          ATTR: function (a, b) {
            var d = b[1],
              d = m.attr ? m.attr(a, d) : o.attrHandle[d] ? o.attrHandle[d](a) : a[d] != null ? a[d] : a.getAttribute(d),
              c = d + "",
              f = b[2],
              e = b[4];
            return d == null ? f === "!=" : !f && m.attr ? d != null : f === "=" ? c === e : f === "*=" ? c.indexOf(e) >= 0 : f === "~=" ? (" " + c + " ").indexOf(e) >= 0 : e ? f === "!=" ? c !== e : f === "^=" ? c.indexOf(e) === 0 : f === "$=" ? c.substr(c.length - e.length) === e : f === "|=" ? c === e || c.substr(0, e.length + 1) === e + "-" : false : c && d !== false
          },
          POS: function (a, b, d, c) {
            var f = o.setFilters[b[2]];
            if (f) return f(a, d, b, c)
          }
        }
      },
      F = o.match.POS,
      z = function (a, b) {
        return "\\" + (b - 0 + 1)
      },
      p;
    for (p in o.match) o.match[p] = RegExp(o.match[p].source + /(?![^\[]*\])(?![^\(]*\))/.source), o.leftMatch[p] = RegExp(/(^(?:.|\r|\n)*?)/.source + o.match[p].source.replace(/\\(\d+)/g, z));
    var s = function (a, b) {
        a = Array.prototype.slice.call(a, 0);
        return b ? (b.push.apply(b, a), b) : a
      };
    try {
      Array.prototype.slice.call(H.documentElement.childNodes, 0)
    } catch (Ia) {
      s = function (a, b) {
        var d = 0,
          c = b || [];
        if (g.call(a) === "[object Array]") Array.prototype.push.apply(c, a);
        else if (typeof a.length == "number") for (var f = a.length; d < f; d++) c.push(a[d]);
        else for (; a[d]; d++) c.push(a[d]);
        return c
      }
    }
    var va, oa;
    H.documentElement.compareDocumentPosition ? va = function (a, b) {
      return a === b ? (h = true, 0) : !a.compareDocumentPosition || !b.compareDocumentPosition ? a.compareDocumentPosition ? -1 : 1 : a.compareDocumentPosition(b) & 4 ? -1 : 1
    } : (va = function (a, b) {
      if (a === b) return h = true, 0;
      if (a.sourceIndex && b.sourceIndex) return a.sourceIndex - b.sourceIndex;
      var d, c, f = [],
        e = [];
      d = a.parentNode;
      c = b.parentNode;
      var g = d;
      if (d === c) return oa(a, b);
      if (!d) return -1;
      if (!c) return 1;
      for (; g;) f.unshift(g), g = g.parentNode;
      for (g = c; g;) e.unshift(g), g = g.parentNode;
      d = f.length;
      c = e.length;
      for (g = 0; g < d && g < c; g++) if (f[g] !== e[g]) return oa(f[g], e[g]);
      return g === d ? oa(a, e[g], -1) : oa(f[g], b, 1)
    }, oa = function (a, b, d) {
      if (a === b) return d;
      for (a = a.nextSibling; a;) {
        if (a === b) return -1;
        a = a.nextSibling
      }
      return 1
    });
    (function () {
      var a = H.createElement("div"),
        b = "script" + (new Date).getTime(),
        d = H.documentElement;
      a.innerHTML = "<a name='" + b + "'/>";
      d.insertBefore(a, d.firstChild);
      H.getElementById(b) && (o.find.ID = function (a, b, d) {
        if (typeof b.getElementById != "undefined" && !d) return (b = b.getElementById(a[1])) ? b.id === a[1] || typeof b.getAttributeNode != "undefined" && b.getAttributeNode("id").nodeValue === a[1] ? [b] : e : []
      }, o.filter.ID = function (a, b) {
        var d = typeof a.getAttributeNode != "undefined" && a.getAttributeNode("id");
        return a.nodeType === 1 && d && d.nodeValue === b
      });
      d.removeChild(a);
      d = a = null
    })();
    (function () {
      var a = H.createElement("div");
      a.appendChild(H.createComment(""));
      a.getElementsByTagName("*").length > 0 && (o.find.TAG = function (a, b) {
        var d = b.getElementsByTagName(a[1]);
        if (a[1] === "*") {
          for (var c = [], f = 0; d[f]; f++) d[f].nodeType === 1 && c.push(d[f]);
          d = c
        }
        return d
      });
      a.innerHTML = "<a href='#'></a>";
      a.firstChild && typeof a.firstChild.getAttribute != "undefined" && a.firstChild.getAttribute("href") !== "#" && (o.attrHandle.href = function (a) {
        return a.getAttribute("href", 2)
      });
      a = null
    })();
    H.querySelectorAll &&
    function () {
      var a = m,
        b = H.createElement("div");
      b.innerHTML = "<p class='TEST'></p>";
      if (!b.querySelectorAll || b.querySelectorAll(".TEST").length !== 0) {
        m = function (b, d, c, f) {
          d = d || H;
          if (!f && !m.isXML(d)) {
            var e = /^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(b);
            if (e && (d.nodeType === 1 || d.nodeType === 9)) {
              if (e[1]) return s(d.getElementsByTagName(b), c);
              if (e[2] && o.find.CLASS && d.getElementsByClassName) return s(d.getElementsByClassName(e[2]), c)
            }
            if (d.nodeType === 9) {
              if (b === "body" && d.body) return s([d.body], c);
              if (e && e[3]) {
                var g = d.getElementById(e[3]);
                if (!g || !g.parentNode) return s([], c);
                if (g.id === e[3]) return s([g], c)
              }
              try {
                return s(d.querySelectorAll(b), c)
              } catch (h) {}
            } else if (d.nodeType === 1 && d.nodeName.toLowerCase() !== "object") {
              var e = d,
                v = (g = d.getAttribute("id")) || "__sizzle__",
                j = d.parentNode,
                n = /^\s*[+~]/.test(b);
              g ? v = v.replace(/'/g, "\\$&") : d.setAttribute("id", v);
              n && j && (d = d.parentNode);
              try {
                if (!n || j) return s(d.querySelectorAll("[id='" + v + "'] " + b), c)
              } catch (l) {} finally {
                g || e.removeAttribute("id")
              }
            }
          }
          return a(b, d, c, f)
        };
        for (var d in a) m[d] = a[d];
        b = null
      }
    }();
    (function () {
      var a = H.documentElement,
        b = a.matchesSelector || a.mozMatchesSelector || a.webkitMatchesSelector || a.msMatchesSelector;
      if (b) {
        var d = !b.call(H.createElement("div"), "div"),
          c = false;
        try {
          b.call(H.documentElement, "[test!='']:sizzle")
        } catch (f) {
          c = true
        }
        m.matchesSelector = function (a, f) {
          f = f.replace(/\=\s*([^'"\]]*)\s*\]/g, "='$1']");
          if (!m.isXML(a)) try {
            if (c || !o.match.PSEUDO.test(f) && !/!=/.test(f)) {
              var e = b.call(a, f);
              if (e || !d || a.document && a.document.nodeType !== 11) return e
            }
          } catch (g) {}
          return m(f, null, null, [a]).length > 0
        }
      }
    })();
    (function () {
      var a = H.createElement("div");
      a.innerHTML = "<div class='test e'></div><div class='test'></div>";
      if (a.getElementsByClassName && a.getElementsByClassName("e").length !== 0 && (a.lastChild.className = "e", a.getElementsByClassName("e").length !== 1)) o.order.splice(1, 0, "CLASS"), o.find.CLASS = function (a, b, d) {
        if (typeof b.getElementsByClassName != "undefined" && !d) return b.getElementsByClassName(a[1])
      }, a = null
    })();
    H.documentElement.contains ? m.contains = function (a, b) {
      return a !== b && (a.contains ? a.contains(b) : true)
    } : H.documentElement.compareDocumentPosition ? m.contains = function (a, b) {
      return !!(a.compareDocumentPosition(b) & 16)
    } : m.contains = function () {
      return false
    };
    m.isXML = function (a) {
      return (a = (a ? a.ownerDocument || a : 0).documentElement) ? a.nodeName !== "HTML" : false
    };
    var w = function (a, b, d) {
        for (var c, f = [], e = "", b = b.nodeType ? [b] : b; c = o.match.PSEUDO.exec(a);) e += c[0], a = a.replace(o.match.PSEUDO, "");
        a = o.relative[a] ? a + "*" : a;
        c = 0;
        for (var g = b.length; c < g; c++) m(a, b[c], f, d);
        return m.filter(e, f)
      };
    m.attr = k.attr;
    m.selectors.attrMap = {};
    k.find = m;
    k.expr = m.selectors;
    k.expr[":"] = k.expr.filters;
    k.unique = m.uniqueSort;
    k.text = m.getText;
    k.isXMLDoc = m.isXML;
    k.contains = m.contains
  })();
  var Ia = /Until$/,
    oa = /^(?:parents|prevUntil|prevAll)/,
    pa = /,/,
    ra = /^.[^:#\[\.,]*$/,
    bb = Array.prototype.slice,
    mc = k.expr.match.POS,
    Da = {
      children: true,
      contents: true,
      next: true,
      prev: true
    };
  k.fn.extend({
    find: function (a) {
      var b = this,
        d, c;
      if (typeof a != "string") return k(a).filter(function () {
        for (d = 0, c = b.length; d < c; d++) if (k.contains(b[d], this)) return true
      });
      var f = this.pushStack("", "find", a),
        e, g, v;
      for (d = 0, c = this.length; d < c; d++) if (e = f.length, k.find(a, this[d], f), d > 0) for (g = e; g < f.length; g++) for (v = 0; v < e; v++) if (f[v] === f[g]) {
        f.splice(g--, 1);
        break
      }
      return f
    },
    has: function (a) {
      var b = k(a);
      return this.filter(function () {
        for (var a = 0, d = b.length; a < d; a++) if (k.contains(this, b[a])) return true
      })
    },
    not: function (a) {
      return this.pushStack(F(this, a, false), "not", a)
    },
    filter: function (a) {
      return this.pushStack(F(this, a, true), "filter", a)
    },
    is: function (a) {
      return !!a && (typeof a == "string" ? mc.test(a) ? k(a, this.context).index(this[0]) >= 0 : k.filter(a, this).length > 0 : this.filter(a).length > 0)
    },
    closest: function (a, b) {
      var d = [],
        c, f, e = this[0];
      if (k.isArray(a)) {
        for (f = 1; e && e.ownerDocument && e !== b;) {
          for (c = 0; c < a.length; c++) k(e).is(a[c]) && d.push({
            selector: a[c],
            elem: e,
            level: f
          });
          e = e.parentNode;
          f++
        }
        return d
      }
      var g = mc.test(a) || typeof a != "string" ? k(a, b || this.context) : 0;
      for (c = 0, f = this.length; c < f; c++) for (e = this[c]; e;) {
        if (g ? g.index(e) > -1 : k.find.matchesSelector(e, a)) {
          d.push(e);
          break
        }
        e = e.parentNode;
        if (!e || !e.ownerDocument || e === b || e.nodeType === 11) break
      }
      d = d.length > 1 ? k.unique(d) : d;
      return this.pushStack(d, "closest", a)
    },
    index: function (a) {
      return !a ? this[0] && this[0].parentNode ? this.prevAll().length : -1 : typeof a == "string" ? k.inArray(this[0], k(a)) : k.inArray(a.jquery ? a[0] : a, this)
    },
    add: function (a, b) {
      var d = typeof a == "string" ? k(a, b) : k.makeArray(a && a.nodeType ? [a] : a),
        c = k.merge(this.get(), d);
      return this.pushStack(!d[0] || !d[0].parentNode || d[0].parentNode.nodeType === 11 || !c[0] || !c[0].parentNode || c[0].parentNode.nodeType === 11 ? c : k.unique(c))
    },
    andSelf: function () {
      return this.add(this.prevObject)
    }
  });
  k.each({
    parent: function (a) {
      return (a = a.parentNode) && a.nodeType !== 11 ? a : null
    },
    parents: function (a) {
      return k.dir(a, "parentNode")
    },
    parentsUntil: function (a, b, d) {
      return k.dir(a, "parentNode", d)
    },
    next: function (a) {
      return k.nth(a, 2, "nextSibling")
    },
    prev: function (a) {
      return k.nth(a, 2, "previousSibling")
    },
    nextAll: function (a) {
      return k.dir(a, "nextSibling")
    },
    prevAll: function (a) {
      return k.dir(a, "previousSibling")
    },
    nextUntil: function (a, b, d) {
      return k.dir(a, "nextSibling", d)
    },
    prevUntil: function (a, b, d) {
      return k.dir(a, "previousSibling", d)
    },
    siblings: function (a) {
      return k.sibling(a.parentNode.firstChild, a)
    },
    children: function (a) {
      return k.sibling(a.firstChild)
    },
    contents: function (a) {
      return k.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : k.makeArray(a.childNodes)
    }
  }, function (a, b) {
    k.fn[a] = function (d, c) {
      var f = k.map(this, b, d);
      Ia.test(a) || (c = d);
      c && typeof c == "string" && (f = k.filter(c, f));
      f = this.length > 1 && !Da[a] ? k.unique(f) : f;
      (this.length > 1 || pa.test(c)) && oa.test(a) && (f = f.reverse());
      return this.pushStack(f, a, bb.call(arguments).join(","))
    }
  });
  k.extend({
    filter: function (a, b, d) {
      d && (a = ":not(" + a + ")");
      return b.length === 1 ? k.find.matchesSelector(b[0], a) ? [b[0]] : [] : k.find.matches(a, b)
    },
    dir: function (a, b, d) {
      for (var c = [], a = a[b]; a && a.nodeType !== 9 && (d === e || a.nodeType !== 1 || !k(a).is(d));) a.nodeType === 1 && c.push(a), a = a[b];
      return c
    },
    nth: function (a, b, d) {
      for (var b = b || 1, c = 0; a; a = a[d]) if (a.nodeType === 1 && ++c === b) break;
      return a
    },
    sibling: function (a, b) {
      for (var d = []; a; a = a.nextSibling) a.nodeType === 1 && a !== b && d.push(a);
      return d
    }
  });
  var Ba = "abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
    nc = / jQuery\d+="(?:\d+|null)"/g,
    Wb = /^\s+/,
    Ab = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,
    ma = /<([\w:]+)/,
    Hb = /<tbody/i,
    Mb = /<|&#?\w+;/,
    tc = /<(?:script|style)/i,
    rb = /<(?:script|object|embed|option|style)/i,
    Bb = RegExp("<(?:" + Ba + ")", "i"),
    sb = /checked\s*(?:[^=]|=\s*.checked.)/i,
    Sa = /\/(java|ecma)script/i,
    ob = /^\s*<!(?:\[CDATA\[|\-\-)/,
    Aa = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      legend: [1, "<fieldset>", "</fieldset>"],
      thead: [1, "<table>", "</table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
      area: [1, "<map>", "</map>"],
      _default: [0, "", ""]
    },
    Ma = z(H);
  Aa.optgroup = Aa.option;
  Aa.tbody = Aa.tfoot = Aa.colgroup = Aa.caption = Aa.thead;
  Aa.th = Aa.td;
  k.support.htmlSerialize || (Aa._default = [1, "div<div>", "</div>"]);
  k.fn.extend({
    text: function (a) {
      return k.isFunction(a) ? this.each(function (b) {
        var d = k(this);
        d.text(a.call(this, b, d.text()))
      }) : typeof a != "object" && a !== e ? this.empty().append((this[0] && this[0].ownerDocument || H).createTextNode(a)) : k.text(this)
    },
    wrapAll: function (a) {
      if (k.isFunction(a)) return this.each(function (b) {
        k(this).wrapAll(a.call(this, b))
      });
      if (this[0]) {
        var b = k(a, this[0].ownerDocument).eq(0).clone(true);
        this[0].parentNode && b.insertBefore(this[0]);
        b.map(function () {
          for (var a = this; a.firstChild && a.firstChild.nodeType === 1;) a = a.firstChild;
          return a
        }).append(this)
      }
      return this
    },
    wrapInner: function (a) {
      return k.isFunction(a) ? this.each(function (b) {
        k(this).wrapInner(a.call(this, b))
      }) : this.each(function () {
        var b = k(this),
          d = b.contents();
        d.length ? d.wrapAll(a) : b.append(a)
      })
    },
    wrap: function (a) {
      var b = k.isFunction(a);
      return this.each(function (d) {
        k(this).wrapAll(b ? a.call(this, d) : a)
      })
    },
    unwrap: function () {
      return this.parent().each(function () {
        k.nodeName(this, "body") || k(this).replaceWith(this.childNodes)
      }).end()
    },
    append: function () {
      return this.domManip(arguments, true, function (a) {
        this.nodeType === 1 && this.appendChild(a)
      })
    },
    prepend: function () {
      return this.domManip(arguments, true, function (a) {
        this.nodeType === 1 && this.insertBefore(a, this.firstChild)
      })
    },
    before: function () {
      if (this[0] && this[0].parentNode) return this.domManip(arguments, false, function (a) {
        this.parentNode.insertBefore(a, this)
      });
      if (arguments.length) {
        var a = k.clean(arguments);
        a.push.apply(a, this.toArray());
        return this.pushStack(a, "before", arguments)
      }
    },
    after: function () {
      if (this[0] && this[0].parentNode) return this.domManip(arguments, false, function (a) {
        this.parentNode.insertBefore(a, this.nextSibling)
      });
      if (arguments.length) {
        var a = this.pushStack(this, "after", arguments);
        a.push.apply(a, k.clean(arguments));
        return a
      }
    },
    remove: function (a, b) {
      for (var d = 0, c;
      (c = this[d]) != null; d++) if (!a || k.filter(a, [c]).length)!b && c.nodeType === 1 && (k.cleanData(c.getElementsByTagName("*")), k.cleanData([c])), c.parentNode && c.parentNode.removeChild(c);
      return this
    },
    empty: function () {
      for (var a = 0, b;
      (b = this[a]) != null; a++) for (b.nodeType === 1 && k.cleanData(b.getElementsByTagName("*")); b.firstChild;) b.removeChild(b.firstChild);
      return this
    },
    clone: function (a, b) {
      a = a == null ? false : a;
      b = b == null ? a : b;
      return this.map(function () {
        return k.clone(this, a, b)
      })
    },
    html: function (a) {
      if (a === e) return this[0] && this[0].nodeType === 1 ? this[0].innerHTML.replace(nc, "") : null;
      if (typeof a == "string" && !tc.test(a) && (k.support.leadingWhitespace || !Wb.test(a)) && !Aa[(ma.exec(a) || ["", ""])[1].toLowerCase()]) {
        a = a.replace(Ab, "<$1></$2>");
        try {
          for (var b = 0, d = this.length; b < d; b++) this[b].nodeType === 1 && (k.cleanData(this[b].getElementsByTagName("*")), this[b].innerHTML = a)
        } catch (c) {
          this.empty().append(a)
        }
      } else k.isFunction(a) ? this.each(function (b) {
        var d = k(this);
        d.html(a.call(this, b, d.html()))
      }) : this.empty().append(a);
      return this
    },
    replaceWith: function (a) {
      if (this[0] && this[0].parentNode) {
        if (k.isFunction(a)) return this.each(function (b) {
          var d = k(this),
            c = d.html();
          d.replaceWith(a.call(this, b, c))
        });
        typeof a != "string" && (a = k(a).detach());
        return this.each(function () {
          var b = this.nextSibling,
            d = this.parentNode;
          k(this).remove();
          b ? k(b).before(a) : k(d).append(a)
        })
      }
      return this.length ? this.pushStack(k(k.isFunction(a) ? a() : a), "replaceWith", a) : this
    },
    detach: function (a) {
      return this.remove(a, true)
    },
    domManip: function (a, b, d) {
      var c, f, g, h, v = a[0],
        j = [];
      if (!k.support.checkClone && arguments.length === 3 && typeof v == "string" && sb.test(v)) return this.each(function () {
        k(this).domManip(a, b, d, true)
      });
      if (k.isFunction(v)) return this.each(function (c) {
        var f = k(this);
        a[0] = v.call(this, c, b ? f.html() : e);
        f.domManip(a, b, d)
      });
      if (this[0]) {
        h = v && v.parentNode;
        k.support.parentNode && h && h.nodeType === 11 && h.childNodes.length === this.length ? c = {
          fragment: h
        } : c = k.buildFragment(a, this, j);
        g = c.fragment;
        g.childNodes.length === 1 ? f = g = g.firstChild : f = g.firstChild;
        if (f) {
          b = b && k.nodeName(f, "tr");
          f = 0;
          h = this.length;
          for (var n = h - 1; f < h; f++) d.call(b ? k.nodeName(this[f], "table") ? this[f].getElementsByTagName("tbody")[0] || this[f].appendChild(this[f].ownerDocument.createElement("tbody")) : this[f] : this[f], c.cacheable || h > 1 && f < n ? k.clone(g, true, true) : g)
        }
        j.length && k.each(j, o)
      }
      return this
    }
  });
  k.buildFragment = function (a, b, d) {
    var c, f, e, g, h = a[0];
    b && b[0] && (g = b[0].ownerDocument || b[0]);
    g.createDocumentFragment || (g = H);
    a.length === 1 && typeof h == "string" && h.length < 512 && g === H && h.charAt(0) === "<" && !rb.test(h) && (k.support.checkClone || !sb.test(h)) && (k.support.html5Clone || !Bb.test(h)) && (f = true, e = k.fragments[h], e && e !== 1 && (c = e));
    c || (c = g.createDocumentFragment(), k.clean(a, g, c, d));
    f && (k.fragments[h] = e ? c : 1);
    return {
      fragment: c,
      cacheable: f
    }
  };
  k.fragments = {};
  k.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function (a, b) {
    k.fn[a] = function (d) {
      var c = [],
        d = k(d),
        f = this.length === 1 && this[0].parentNode;
      if (f && f.nodeType === 11 && f.childNodes.length === 1 && d.length === 1) return d[b](this[0]), this;
      for (var f = 0, e = d.length; f < e; f++) {
        var g = (f > 0 ? this.clone(true) : this).get();
        k(d[f])[b](g);
        c = c.concat(g)
      }
      return this.pushStack(c, a, d.selector)
    }
  });
  k.extend({
    clone: function (a, b, d) {
      var c, f, e;
      k.support.html5Clone || !Bb.test("<" + a.nodeName) ? c = a.cloneNode(true) : (c = H.createElement("div"), Ma.appendChild(c), c.innerHTML = a.outerHTML, c = c.firstChild);
      var g = c;
      if ((!k.support.noCloneEvent || !k.support.noCloneChecked) && (a.nodeType === 1 || a.nodeType === 11) && !k.isXMLDoc(a)) {
        D(a, g);
        c = s(a);
        f = s(g);
        for (e = 0; c[e]; ++e) f[e] && D(c[e], f[e])
      }
      if (b && (C(a, g), d)) {
        c = s(a);
        f = s(g);
        for (e = 0; c[e]; ++e) C(c[e], f[e])
      }
      return g
    },
    clean: function (a, b, d, c) {
      b = b || H;
      typeof b.createElement == "undefined" && (b = b.ownerDocument || b[0] && b[0].ownerDocument || H);
      for (var f = [], e, g = 0, h;
      (h = a[g]) != null; g++) if (typeof h == "number" && (h += ""), h) {
        if (typeof h == "string") if (Mb.test(h)) {
          h = h.replace(Ab, "<$1></$2>");
          e = (ma.exec(h) || ["", ""])[1].toLowerCase();
          var j = Aa[e] || Aa._default,
            n = j[0],
            l = b.createElement("div");
          for (b === H ? Ma.appendChild(l) : z(b).appendChild(l), l.innerHTML = j[1] + h + j[2]; n--;) l = l.lastChild;
          if (!k.support.tbody) {
            n = Hb.test(h);
            j = e === "table" && !n ? l.firstChild && l.firstChild.childNodes : j[1] === "<table>" && !n ? l.childNodes : [];
            for (e = j.length - 1; e >= 0; --e) k.nodeName(j[e], "tbody") && !j[e].childNodes.length && j[e].parentNode.removeChild(j[e])
          }!k.support.leadingWhitespace && Wb.test(h) && l.insertBefore(b.createTextNode(Wb.exec(h)[0]), l.firstChild);
          h = l.childNodes
        } else h = b.createTextNode(h);
        var m;
        if (!k.support.appendChecked) if (h[0] && typeof (m = h.length) == "number") for (e = 0; e < m; e++) u(h[e]);
        else u(h);
        h.nodeType ? f.push(h) : f = k.merge(f, h)
      }
      if (d) {
        a = function (a) {
          return !a.type || Sa.test(a.type)
        };
        for (g = 0; f[g]; g++) c && k.nodeName(f[g], "script") && (!f[g].type || f[g].type.toLowerCase() === "text/javascript") ? c.push(f[g].parentNode ? f[g].parentNode.removeChild(f[g]) : f[g]) : (f[g].nodeType === 1 && (b = k.grep(f[g].getElementsByTagName("script"), a), f.splice.apply(f, [g + 1, 0].concat(b))), d.appendChild(f[g]))
      }
      return f
    },
    cleanData: function (a) {
      for (var b, d, c = k.cache, f = k.event.special, e = k.support.deleteExpando, g = 0, h;
      (h = a[g]) != null; g++) if (!h.nodeName || !k.noData[h.nodeName.toLowerCase()]) if (d = h[k.expando]) {
        if ((b = c[d]) && b.events) {
          for (var j in b.events) f[j] ? k.event.remove(h, j) : k.removeEvent(h, j, b.handle);
          b.handle && (b.handle.elem = null)
        }
        e ? delete h[k.expando] : h.removeAttribute && h.removeAttribute(k.expando);
        delete c[d]
      }
    }
  });
  var vb = /alpha\([^)]*\)/i,
    Wa = /opacity=([^)]*)/,
    wb = /([A-Z]|^ms)/g,
    Ib = /^-?\d+(?:px)?$/i,
    Nb = /^-?\d/,
    Ob = /^([\-+])=([\-+.\de]+)/,
    Na = {
      position: "absolute",
      visibility: "hidden",
      display: "block"
    },
    Va = ["Left", "Right"],
    Eb = ["Top", "Bottom"],
    na, Xa, Jb;
  k.fn.css = function (a, b) {
    return arguments.length === 2 && b === e ? this : k.access(this, a, b, true, function (a, b, d) {
      return d !== e ? k.style(a, b, d) : k.css(a, b)
    })
  };
  k.extend({
    cssHooks: {
      opacity: {
        get: function (a, b) {
          if (b) {
            var d = na(a, "opacity", "opacity");
            return d === "" ? "1" : d
          }
          return a.style.opacity
        }
      }
    },
    cssNumber: {
      fillOpacity: true,
      fontWeight: true,
      lineHeight: true,
      opacity: true,
      orphans: true,
      widows: true,
      zIndex: true,
      zoom: true
    },
    cssProps: {
      "float": k.support.cssFloat ? "cssFloat" : "styleFloat"
    },
    style: function (a, b, d, c) {
      if (a && a.nodeType !== 3 && a.nodeType !== 8 && a.style) {
        var f, g, h = k.camelCase(b),
          v = a.style,
          j = k.cssHooks[h],
          b = k.cssProps[h] || h;
        if (d === e) return j && "get" in j && (f = j.get(a, false, c)) !== e ? f : v[b];
        g = typeof d;
        g === "string" && (f = Ob.exec(d)) && (d = +(f[1] + 1) * +f[2] + parseFloat(k.css(a, b)), g = "number");
        if (!(d == null || g === "number" && isNaN(d))) if (g === "number" && !k.cssNumber[h] && (d += "px"), !j || !("set" in j) || (d = j.set(a, d)) !== e) try {
          v[b] = d
        } catch (n) {}
      }
    },
    css: function (a, b, d) {
      var c, f;
      b = k.camelCase(b);
      f = k.cssHooks[b];
      b = k.cssProps[b] || b;
      b === "cssFloat" && (b = "float");
      if (f && "get" in f && (c = f.get(a, true, d)) !== e) return c;
      if (na) return na(a, b)
    },
    swap: function (a, b, d) {
      var c = {},
        f;
      for (f in b) c[f] = a.style[f], a.style[f] = b[f];
      d.call(a);
      for (f in b) a.style[f] = c[f]
    }
  });
  k.curCSS = k.css;
  k.each(["height", "width"], function (a, b) {
    k.cssHooks[b] = {
      get: function (a, d, c) {
        var f;
        if (d) {
          if (a.offsetWidth !== 0) return p(a, b, c);
          k.swap(a, Na, function () {
            f = p(a, b, c)
          });
          return f
        }
      },
      set: function (a, b) {
        if (!Ib.test(b)) return b;
        b = parseFloat(b);
        if (b >= 0) return b + "px"
      }
    }
  });
  k.support.opacity || (k.cssHooks.opacity = {
    get: function (a, b) {
      return Wa.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? parseFloat(RegExp.$1) / 100 + "" : b ? "1" : ""
    },
    set: function (a, b) {
      var d = a.style,
        c = a.currentStyle,
        f = k.isNumeric(b) ? "alpha(opacity=" + b * 100 + ")" : "",
        e = c && c.filter || d.filter || "";
      d.zoom = 1;
      if (b >= 1 && k.trim(e.replace(vb, "")) === "" && (d.removeAttribute("filter"), c && !c.filter)) return;
      d.filter = vb.test(e) ? e.replace(vb, f) : e + " " + f
    }
  });
  k(function () {
    k.support.reliableMarginRight || (k.cssHooks.marginRight = {
      get: function (a, b) {
        var d;
        k.swap(a, {
          display: "inline-block"
        }, function () {
          b ? d = na(a, "margin-right", "marginRight") : d = a.style.marginRight
        });
        return d
      }
    })
  });
  H.defaultView && H.defaultView.getComputedStyle && (Xa = function (a, b) {
    var d, c, f;
    b = b.replace(wb, "-$1").toLowerCase();
    (c = a.ownerDocument.defaultView) && (f = c.getComputedStyle(a, null)) && (d = f.getPropertyValue(b), d === "" && !k.contains(a.ownerDocument.documentElement, a) && (d = k.style(a, b)));
    return d
  });
  H.documentElement.currentStyle && (Jb = function (a, b) {
    var d, c, f, e = a.currentStyle && a.currentStyle[b],
      g = a.style;
    e === null && g && (f = g[b]) && (e = f);
    !Ib.test(e) && Nb.test(e) && (d = g.left, c = a.runtimeStyle && a.runtimeStyle.left, c && (a.runtimeStyle.left = a.currentStyle.left), g.left = b === "fontSize" ? "1em" : e || 0, e = g.pixelLeft + "px", g.left = d, c && (a.runtimeStyle.left = c));
    return e === "" ? "auto" : e
  });
  na = Xa || Jb;
  k.expr && k.expr.filters && (k.expr.filters.hidden = function (a) {
    var b = a.offsetHeight;
    return a.offsetWidth === 0 && b === 0 || !k.support.reliableHiddenOffsets && (a.style && a.style.display || k.css(a, "display")) === "none"
  }, k.expr.filters.visible = function (a) {
    return !k.expr.filters.hidden(a)
  });
  var Xb = /%20/g,
    Tb = /\[\]$/,
    Yb = /\r?\n/g,
    Kb = /#.*$/,
    Pb = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
    Qb = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
    oc = /^(?:GET|HEAD)$/,
    wc = /^\/\//,
    Cb = /\?/,
    xc = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
    yc = /^(?:select|textarea)/i,
    T = /\s+/,
    Oa = /([?&])_=[^&]*/,
    Zb = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/,
    J = k.fn.load,
    hc = {},
    uc = {},
    xb, da, Pa = ["*/"] + ["*"];
  try {
    xb = gb.href
  } catch (nb) {
    xb = H.createElement("a"), xb.href = "", xb = xb.href
  }
  da = Zb.exec(xb.toLowerCase()) || [];
  k.fn.extend({
    load: function (a, b, d) {
      if (typeof a != "string" && J) return J.apply(this, arguments);
      if (!this.length) return this;
      var c = a.indexOf(" ");
      if (c >= 0) var f = a.slice(c, a.length),
        a = a.slice(0, c);
      c = "GET";
      b && (k.isFunction(b) ? (d = b, b = e) : typeof b == "object" && (b = k.param(b, k.ajaxSettings.traditional), c = "POST"));
      var g = this;
      k.ajax({
        url: a,
        type: c,
        dataType: "html",
        data: b,
        complete: function (a, b, c) {
          c = a.responseText;
          a.isResolved() && (a.done(function (a) {
            c = a
          }), g.html(f ? k("<div>").append(c.replace(xc, "")).find(f) : c));
          d && g.each(d, [c, b, a])
        }
      });
      return this
    },
    serialize: function () {
      return k.param(this.serializeArray())
    },
    serializeArray: function () {
      return this.map(function () {
        return this.elements ? k.makeArray(this.elements) : this
      }).filter(function () {
        return this.name && !this.disabled && (this.checked || yc.test(this.nodeName) || Qb.test(this.type))
      }).map(function (a, b) {
        var d = k(this).val();
        return d == null ? null : k.isArray(d) ? k.map(d, function (a) {
          return {
            name: b.name,
            value: a.replace(Yb, "\r\n")
          }
        }) : {
          name: b.name,
          value: d.replace(Yb, "\r\n")
        }
      }).get()
    }
  });
  k.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function (a, b) {
    k.fn[b] = function (a) {
      return this.on(b, a)
    }
  });
  k.each(["get", "post"], function (a, b) {
    k[b] = function (a, d, c, f) {
      k.isFunction(d) && (f = f || c, c = d, d = e);
      return k.ajax({
        type: b,
        url: a,
        data: d,
        success: c,
        dataType: f
      })
    }
  });
  k.extend({
    getScript: function (a, b) {
      return k.get(a, e, b, "script")
    },
    getJSON: function (a, b, d) {
      return k.get(a, b, d, "json")
    },
    ajaxSetup: function (a, b) {
      b ? l(a, k.ajaxSettings) : (b = a, a = k.ajaxSettings);
      l(a, b);
      return a
    },
    ajaxSettings: {
      url: xb,
      isLocal: /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/.test(da[1]),
      global: true,
      type: "GET",
      contentType: "application/x-www-form-urlencoded",
      processData: true,
      async: true,
      accepts: {
        xml: "application/xml, text/xml",
        html: "text/html",
        text: "text/plain",
        json: "application/json, text/javascript",
        "*": Pa
      },
      contents: {
        xml: /xml/,
        html: /html/,
        json: /json/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText"
      },
      converters: {
        "* text": a.String,
        "text html": true,
        "text json": k.parseJSON,
        "text xml": k.parseXML
      },
      flatOptions: {
        context: true,
        url: true
      }
    },
    ajaxPrefilter: m(hc),
    ajaxTransport: m(uc),
    ajax: function (a, b) {
      function d(a, b, n, m) {
        if (Ia !== 2) {
          Ia = 2;
          s && clearTimeout(s);
          z = e;
          q = m || "";
          w.readyState = a > 0 ? 4 : 0;
          var R, o, F, m = b;
          if (n) {
            var qa = c,
              p = w,
              oa = qa.contents,
              U = qa.dataTypes,
              C = qa.responseFields,
              pa, u, K, bb;
            for (u in C) u in n && (p[C[u]] = n[u]);
            for (; U[0] === "*";) U.shift(), pa === e && (pa = qa.mimeType || p.getResponseHeader("content-type"));
            if (pa) for (u in oa) if (oa[u] && oa[u].test(pa)) {
              U.unshift(u);
              break
            }
            if (U[0] in n) K = U[0];
            else {
              for (u in n) {
                if (!U[0] || qa.converters[u + " " + U[0]]) {
                  K = u;
                  break
                }
                bb || (bb = u)
              }
              K = K || bb
            }
            K ? (K !== U[0] && U.unshift(K), n = n[K]) : n = void 0
          } else n = e;
          if (a >= 200 && a < 300 || a === 304) {
            if (c.ifModified) {
              if (pa = w.getResponseHeader("Last-Modified")) k.lastModified[l] = pa;
              if (pa = w.getResponseHeader("Etag")) k.etag[l] = pa
            }
            if (a === 304) m = "notmodified", R = true;
            else try {
              pa = c;
              pa.dataFilter && (n = pa.dataFilter(n, pa.dataType));
              var ra = pa.dataTypes;
              u = {};
              var Z, V, O = ra.length,
                ba, D = ra[0],
                fa, Ca, ia, Da, P;
              for (Z = 1; Z < O; Z++) {
                if (Z === 1) for (V in pa.converters) typeof V == "string" && (u[V.toLowerCase()] = pa.converters[V]);
                fa = D;
                D = ra[Z];
                if (D === "*") D = fa;
                else if (fa !== "*" && fa !== D) {
                  Ca = fa + " " + D;
                  ia = u[Ca] || u["* " + D];
                  if (!ia) for (Da in P = e, u) if (ba = Da.split(" "), ba[0] === fa || ba[0] === "*") if (P = u[ba[1] + " " + D]) {
                    Da = u[Da];
                    Da === true ? ia = P : P === true && (ia = Da);
                    break
                  }!ia && !P && k.error("No conversion from " + Ca.replace(" ", " to "));
                  ia !== true && (n = ia ? ia(n) : P(Da(n)))
                }
              }
              o = n;
              m = "success";
              R = true
            } catch (A) {
              m = "parsererror", F = A
            }
          } else if (F = m, !m || a) m = "error", a < 0 && (a = 0);
          w.status = a;
          w.statusText = "" + (b || m);
          R ? h.resolveWith(f, [o, m, w]) : h.rejectWith(f, [w, m, F]);
          w.statusCode(j);
          j = e;
          va && g.trigger("ajax" + (R ? "Success" : "Error"), [w, c, R ? o : F]);
          v.fireWith(f, [w, m]);
          va && (g.trigger("ajaxComplete", [w, c]), --k.active || k.event.trigger("ajaxStop"))
        }
      }
      typeof a == "object" && (b = a, a = e);
      b = b || {};
      var c = k.ajaxSetup({}, b),
        f = c.context || c,
        g = f !== c && (f.nodeType || f instanceof k) ? k(f) : k.event,
        h = k.Deferred(),
        v = k.Callbacks("once memory"),
        j = c.statusCode || {},
        l, m = {},
        o = {},
        q, F, z, s, p, Ia = 0,
        va, oa, w = {
          readyState: 0,
          setRequestHeader: function (a, b) {
            if (!Ia) {
              var d = a.toLowerCase();
              a = o[d] = o[d] || a;
              m[a] = b
            }
            return this
          },
          getAllResponseHeaders: function () {
            return Ia === 2 ? q : null
          },
          getResponseHeader: function (a) {
            var b;
            if (Ia === 2) {
              if (!F) for (F = {}; b = Pb.exec(q);) F[b[1].toLowerCase()] = b[2];
              b = F[a.toLowerCase()]
            }
            return b === e ? null : b
          },
          overrideMimeType: function (a) {
            Ia || (c.mimeType = a);
            return this
          },
          abort: function (a) {
            a = a || "abort";
            z && z.abort(a);
            d(0, a);
            return this
          }
        };
      h.promise(w);
      w.success = w.done;
      w.error = w.fail;
      w.complete = v.add;
      w.statusCode = function (a) {
        if (a) {
          var b;
          if (Ia < 2) for (b in a) j[b] = [j[b], a[b]];
          else b = a[w.status], w.then(b, b)
        }
        return this
      };
      c.url = ((a || c.url) + "").replace(Kb, "").replace(wc, da[1] + "//");
      c.dataTypes = k.trim(c.dataType || "*").toLowerCase().split(T);
      c.crossDomain == null && (p = Zb.exec(c.url.toLowerCase()), c.crossDomain = !(!p || p[1] == da[1] && p[2] == da[2] && (p[3] || (p[1] === "http:" ? 80 : 443)) == (da[3] || (da[1] === "http:" ? 80 : 443))));
      c.data && c.processData && typeof c.data != "string" && (c.data = k.param(c.data, c.traditional));
      n(hc, c, b, w);
      if (Ia === 2) return false;
      va = c.global;
      c.type = c.type.toUpperCase();
      c.hasContent = !oc.test(c.type);
      va && k.active++ === 0 && k.event.trigger("ajaxStart");
      if (!c.hasContent && (c.data && (c.url += (Cb.test(c.url) ? "&" : "?") + c.data, delete c.data), l = c.url, c.cache === false)) {
        p = k.now();
        var C = c.url.replace(Oa, "$1_=" + p);
        c.url = C + (C === c.url ? (Cb.test(c.url) ? "&" : "?") + "_=" + p : "")
      }(c.data && c.hasContent && c.contentType !== false || b.contentType) && w.setRequestHeader("Content-Type", c.contentType);
      c.ifModified && (l = l || c.url, k.lastModified[l] && w.setRequestHeader("If-Modified-Since", k.lastModified[l]), k.etag[l] && w.setRequestHeader("If-None-Match", k.etag[l]));
      w.setRequestHeader("Accept", c.dataTypes[0] && c.accepts[c.dataTypes[0]] ? c.accepts[c.dataTypes[0]] + (c.dataTypes[0] !== "*" ? ", " + Pa + "; q=0.01" : "") : c.accepts["*"]);
      for (oa in c.headers) w.setRequestHeader(oa, c.headers[oa]);
      if (c.beforeSend && (c.beforeSend.call(f, w, c) === false || Ia === 2)) return w.abort(), false;
      for (oa in {
        success: 1,
        error: 1,
        complete: 1
      }) w[oa](c[oa]);
      if (z = n(uc, c, b, w)) {
        w.readyState = 1;
        va && g.trigger("ajaxSend", [w, c]);
        c.async && c.timeout > 0 && (s = setTimeout(function () {
          w.abort("timeout")
        }, c.timeout));
        try {
          Ia = 1, z.send(m, d)
        } catch (pa) {
          if (Ia < 2) d(-1, pa);
          else throw pa;
        }
      } else d(-1, "No Transport");
      return w
    },
    param: function (a, b) {
      var d = [],
        c = function (a, b) {
          b = k.isFunction(b) ? b() : b;
          d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b)
        };
      b === e && (b = k.ajaxSettings.traditional);
      if (k.isArray(a) || a.jquery && !k.isPlainObject(a)) k.each(a, function () {
        c(this.name, this.value)
      });
      else for (var f in a) j(f, a[f], b, c);
      return d.join("&").replace(Xb, "+")
    }
  });
  k.extend({
    active: 0,
    lastModified: {},
    etag: {}
  });
  var Ja = k.now(),
    $b = /(\=)\?(&|$)|\?\?/i;
  k.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function () {
      return k.expando + "_" + Ja++
    }
  });
  k.ajaxPrefilter("json jsonp", function (b, d, c) {
    d = b.contentType === "application/x-www-form-urlencoded" && typeof b.data == "string";
    if (b.dataTypes[0] === "jsonp" || b.jsonp !== false && ($b.test(b.url) || d && $b.test(b.data))) {
      var f, e = b.jsonpCallback = k.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback,
        g = a[e],
        h = b.url,
        v = b.data,
        j = "$1" + e + "$2";
      b.jsonp !== false && (h = h.replace($b, j), b.url === h && (d && (v = v.replace($b, j)), b.data === v && (h += (/\?/.test(h) ? "&" : "?") + b.jsonp + "=" + e)));
      b.url = h;
      b.data = v;
      a[e] = function (a) {
        f = [a]
      };
      c.always(function () {
        a[e] = g;
        f && k.isFunction(g) && a[e](f[0])
      });
      b.converters["script json"] = function () {
        f || k.error(e + " was not called");
        return f[0]
      };
      b.dataTypes[0] = "json";
      return "script"
    }
  });
  k.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /javascript|ecmascript/
    },
    converters: {
      "text script": function (a) {
        k.globalEval(a);
        return a
      }
    }
  });
  k.ajaxPrefilter("script", function (a) {
    a.cache === e && (a.cache = false);
    a.crossDomain && (a.type = "GET", a.global = false)
  });
  k.ajaxTransport("script", function (a) {
    if (a.crossDomain) {
      var b, d = H.head || H.getElementsByTagName("head")[0] || H.documentElement;
      return {
        send: function (c, f) {
          b = H.createElement("script");
          b.async = "async";
          a.scriptCharset && (b.charset = a.scriptCharset);
          b.src = a.url;
          b.onload = b.onreadystatechange = function (a, c) {
            if (c || !b.readyState || /loaded|complete/.test(b.readyState)) b.onload = b.onreadystatechange = null, d && b.parentNode && d.removeChild(b), b = e, c || f(200, "success")
          };
          d.insertBefore(b, d.firstChild)
        },
        abort: function () {
          b && b.onload(0, 1)
        }
      }
    }
  });
  var Rb = a.ActiveXObject ?
  function () {
    for (var a in Ta) Ta[a](0, 1)
  } : false, tb = 0, Ta;
  k.ajaxSettings.xhr = a.ActiveXObject ?
  function () {
    var b;
    if (!(b = !this.isLocal && h())) a: {
      try {
        b = new a.ActiveXObject("Microsoft.XMLHTTP");
        break a
      } catch (d) {}
      b = void 0
    }
    return b
  } : h;
  (function (a) {
    k.extend(k.support, {
      ajax: !! a,
      cors: !! a && "withCredentials" in a
    })
  })(k.ajaxSettings.xhr());
  k.support.ajax && k.ajaxTransport(function (b) {
    if (!b.crossDomain || k.support.cors) {
      var d;
      return {
        send: function (c, f) {
          var g = b.xhr(),
            h, j;
          b.username ? g.open(b.type, b.url, b.async, b.username, b.password) : g.open(b.type, b.url, b.async);
          if (b.xhrFields) for (j in b.xhrFields) g[j] = b.xhrFields[j];
          b.mimeType && g.overrideMimeType && g.overrideMimeType(b.mimeType);
          !b.crossDomain && !c["X-Requested-With"] && (c["X-Requested-With"] = "XMLHttpRequest");
          try {
            for (j in c) g.setRequestHeader(j, c[j])
          } catch (v) {}
          g.send(b.hasContent && b.data || null);
          d = function (a, c) {
            var v, j, n, l, m;
            try {
              if (d && (c || g.readyState === 4)) if (d = e, h && (g.onreadystatechange = k.noop, Rb && delete Ta[h]), c) g.readyState !== 4 && g.abort();
              else {
                v = g.status;
                n = g.getAllResponseHeaders();
                l = {};
                m = g.responseXML;
                m && m.documentElement && (l.xml = m);
                l.text = g.responseText;
                try {
                  j = g.statusText
                } catch (o) {
                  j = ""
                }!v && b.isLocal && !b.crossDomain ? v = l.text ? 200 : 404 : v === 1223 && (v = 204)
              }
            } catch (q) {
              c || f(-1, q)
            }
            l && f(v, j, l, n)
          };
          !b.async || g.readyState === 4 ? d() : (h = ++tb, Rb && (Ta || (Ta = {}, k(a).unload(Rb)), Ta[h] = d), g.onreadystatechange = d)
        },
        abort: function () {
          d && d(0, 1)
        }
      }
    }
  });
  var zb = {},
    jb, Db, vc = /^(?:toggle|show|hide)$/,
    Ac = /^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i,
    pc, qc = [
      ["height", "marginTop", "marginBottom", "paddingTop", "paddingBottom"],
      ["width", "marginLeft", "marginRight", "paddingLeft", "paddingRight"],
      ["opacity"]
    ],
    kc;
  k.fn.extend({
    show: function (a, b, f) {
      var e, g;
      if (a || a === 0) return this.animate(d("show", 3), a, b, f);
      a = 0;
      for (b = this.length; a < b; a++) e = this[a], e.style && (g = e.style.display, !k._data(e, "olddisplay") && g === "none" && (g = e.style.display = ""), g === "" && k.css(e, "display") === "none" && k._data(e, "olddisplay", c(e.nodeName)));
      for (a = 0; a < b; a++) if (e = this[a], e.style && (g = e.style.display, g === "" || g === "none")) e.style.display = k._data(e, "olddisplay") || "";
      return this
    },
    hide: function (a, b, c) {
      if (a || a === 0) return this.animate(d("hide", 3), a, b, c);
      for (var f, e, a = 0, b = this.length; a < b; a++) f = this[a], f.style && (e = k.css(f, "display"), e !== "none" && !k._data(f, "olddisplay") && k._data(f, "olddisplay", e));
      for (a = 0; a < b; a++) this[a].style && (this[a].style.display = "none");
      return this
    },
    _toggle: k.fn.toggle,
    toggle: function (a, b, c) {
      var f = typeof a == "boolean";
      k.isFunction(a) && k.isFunction(b) ? this._toggle.apply(this, arguments) : a == null || f ? this.each(function () {
        var b = f ? a : k(this).is(":hidden");
        k(this)[b ? "show" : "hide"]()
      }) : this.animate(d("toggle", 3), a, b, c);
      return this
    },
    fadeTo: function (a, b, d, c) {
      return this.filter(":hidden").css("opacity", 0).show().end().animate({
        opacity: b
      }, a, d, c)
    },
    animate: function (a, b, d, f) {
      function e() {
        var v;
        g.queue === false && k._mark(this);
        var b = k.extend({}, g),
          d = this.nodeType === 1,
          f = d && k(this).is(":hidden"),
          h, j, n, l, m, o, q, F, z;
        b.animatedProperties = {};
        for (n in a) {
          h = k.camelCase(n);
          n !== h && (a[h] = a[n], delete a[n]);
          j = a[h];
          k.isArray(j) ? (b.animatedProperties[h] = j[1], v = a[h] = j[0], j = v) : b.animatedProperties[h] = b.specialEasing && b.specialEasing[h] || b.easing || "swing";
          if (j === "hide" && f || j === "show" && !f) return b.complete.call(this);
          d && (h === "height" || h === "width") && (b.overflow = [this.style.overflow, this.style.overflowX, this.style.overflowY], k.css(this, "display") === "inline" && k.css(this, "float") === "none" && (!k.support.inlineBlockNeedsLayout || c(this.nodeName) === "inline" ? this.style.display = "inline-block" : this.style.zoom = 1))
        }
        b.overflow != null && (this.style.overflow = "hidden");
        for (n in a) l = new k.fx(this, b, n), j = a[n], vc.test(j) ? (z = k._data(this, "toggle" + n) || (j === "toggle" ? f ? "show" : "hide" : 0), z ? (k._data(this, "toggle" + n, z === "show" ? "hide" : "show"), l[z]()) : l[j]()) : (m = Ac.exec(j), o = l.cur(), m ? (q = parseFloat(m[2]), F = m[3] || (k.cssNumber[n] ? "" : "px"), F !== "px" && (k.style(this, n, (q || 1) + F), o *= (q || 1) / l.cur(), k.style(this, n, o + F)), m[1] && (q = (m[1] === "-=" ? -1 : 1) * q + o), l.custom(o, q, F)) : l.custom(o, j, ""));
        return true
      }
      var g = k.speed(b, d, f);
      if (k.isEmptyObject(a)) return this.each(g.complete, [false]);
      a = k.extend({}, a);
      return g.queue === false ? this.each(e) : this.queue(g.queue, e)
    },
    stop: function (a, b, d) {
      typeof a != "string" && (d = b, b = a, a = e);
      b && a !== false && this.queue(a || "fx", []);
      return this.each(function () {
        var b, c = false,
          f = k.timers,
          e = k._data(this);
        d || k._unmark(true, this);
        if (a == null) for (b in e) {
          if (e[b] && e[b].stop && b.indexOf(".run") === b.length - 4) {
            var g = e[b];
            k.removeData(this, b, true);
            g.stop(d)
          }
        } else if (e[b = a + ".run"] && e[b].stop) e = e[b], k.removeData(this, b, true), e.stop(d);
        for (b = f.length; b--;) f[b].elem === this && (a == null || f[b].queue === a) && (d ? f[b](true) : f[b].saveState(), c = true, f.splice(b, 1));
        (!d || !c) && k.dequeue(this, a)
      })
    }
  });
  k.each({
    slideDown: d("show", 1),
    slideUp: d("hide", 1),
    slideToggle: d("toggle", 1),
    fadeIn: {
      opacity: "show"
    },
    fadeOut: {
      opacity: "hide"
    },
    fadeToggle: {
      opacity: "toggle"
    }
  }, function (a, b) {
    k.fn[a] = function (a, d, c) {
      return this.animate(b, a, d, c)
    }
  });
  k.extend({
    speed: function (a, b, d) {
      var c = a && typeof a == "object" ? k.extend({}, a) : {
        complete: d || !d && b || k.isFunction(a) && a,
        duration: a,
        easing: d && b || b && !k.isFunction(b) && b
      };
      c.duration = k.fx.off ? 0 : typeof c.duration == "number" ? c.duration : c.duration in k.fx.speeds ? k.fx.speeds[c.duration] : k.fx.speeds._default;
      if (c.queue == null || c.queue === true) c.queue = "fx";
      c.old = c.complete;
      c.complete = function (a) {
        k.isFunction(c.old) && c.old.call(this);
        c.queue ? k.dequeue(this, c.queue) : a !== false && k._unmark(this)
      };
      return c
    },
    easing: {
      linear: function (a, b, d, c) {
        return d + c * a
      },
      swing: function (a, b, d, c) {
        return (-Math.cos(a * Math.PI) / 2 + 0.5) * c + d
      }
    },
    timers: [],
    fx: function (a, b, d) {
      this.options = b;
      this.elem = a;
      this.prop = d;
      b.orig = b.orig || {}
    }
  });
  k.fx.prototype = {
    update: function () {
      this.options.step && this.options.step.call(this.elem, this.now, this);
      (k.fx.step[this.prop] || k.fx.step._default)(this)
    },
    cur: function () {
      if (this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null)) return this.elem[this.prop];
      var a, b = k.css(this.elem, this.prop);
      return isNaN(a = parseFloat(b)) ? !b || b === "auto" ? 0 : b : a
    },
    custom: function (a, b, d) {
      function c(a) {
        return f.step(a)
      }
      var f = this,
        h = k.fx;
      this.startTime = kc || g();
      this.end = b;
      this.now = this.start = a;
      this.pos = this.state = 0;
      this.unit = d || this.unit || (k.cssNumber[this.prop] ? "" : "px");
      c.queue = this.options.queue;
      c.elem = this.elem;
      c.saveState = function () {
        f.options.hide && k._data(f.elem, "fxshow" + f.prop) === e && k._data(f.elem, "fxshow" + f.prop, f.start)
      };
      c() && k.timers.push(c) && !pc && (pc = setInterval(h.tick, h.interval))
    },
    show: function () {
      var a = k._data(this.elem, "fxshow" + this.prop);
      this.options.orig[this.prop] = a || k.style(this.elem, this.prop);
      this.options.show = true;
      a !== e ? this.custom(this.cur(), a) : this.custom(this.prop === "width" || this.prop === "height" ? 1 : 0, this.cur());
      k(this.elem).show()
    },
    hide: function () {
      this.options.orig[this.prop] = k._data(this.elem, "fxshow" + this.prop) || k.style(this.elem, this.prop);
      this.options.hide = true;
      this.custom(this.cur(), 0)
    },
    step: function (a) {
      var b, d, c, f = kc || g(),
        e = true,
        h = this.elem,
        v = this.options;
      if (a || f >= v.duration + this.startTime) {
        this.now = this.end;
        this.pos = this.state = 1;
        this.update();
        v.animatedProperties[this.prop] = true;
        for (b in v.animatedProperties) v.animatedProperties[b] !== true && (e = false);
        if (e) {
          v.overflow != null && !k.support.shrinkWrapBlocks && k.each(["", "X", "Y"], function (a, b) {
            h.style["overflow" + b] = v.overflow[a]
          });
          v.hide && k(h).hide();
          if (v.hide || v.show) for (b in v.animatedProperties) k.style(h, b, v.orig[b]), k.removeData(h, "fxshow" + b, true), k.removeData(h, "toggle" + b, true);
          c = v.complete;
          c && (v.complete = false, c.call(h))
        }
        return false
      }
      v.duration == Infinity ? this.now = f : (d = f - this.startTime, this.state = d / v.duration, this.pos = k.easing[v.animatedProperties[this.prop]](this.state, d, 0, 1, v.duration), this.now = this.start + (this.end - this.start) * this.pos);
      this.update();
      return true
    }
  };
  k.extend(k.fx, {
    tick: function () {
      for (var a, b = k.timers, d = 0; d < b.length; d++) a = b[d], !a() && b[d] === a && b.splice(d--, 1);
      b.length || k.fx.stop()
    },
    interval: 13,
    stop: function () {
      clearInterval(pc);
      pc = null
    },
    speeds: {
      slow: 600,
      fast: 200,
      _default: 400
    },
    step: {
      opacity: function (a) {
        k.style(a.elem, "opacity", a.now)
      },
      _default: function (a) {
        a.elem.style && a.elem.style[a.prop] != null ? a.elem.style[a.prop] = a.now + a.unit : a.elem[a.prop] = a.now
      }
    }
  });
  k.each(["width", "height"], function (a, b) {
    k.fx.step[b] = function (a) {
      k.style(a.elem, b, Math.max(0, a.now) + a.unit)
    }
  });
  k.expr && k.expr.filters && (k.expr.filters.animated = function (a) {
    return k.grep(k.timers, function (b) {
      return a === b.elem
    }).length
  });
  var lb = /^t(?:able|d|h)$/i,
    yb = /^(?:body|html)$/i;
  "getBoundingClientRect" in H.documentElement ? k.fn.offset = function (a) {
    var d = this[0],
      c;
    if (a) return this.each(function (b) {
      k.offset.setOffset(this, a, b)
    });
    if (!d || !d.ownerDocument) return null;
    if (d === d.ownerDocument.body) return k.offset.bodyOffset(d);
    try {
      c = d.getBoundingClientRect()
    } catch (f) {}
    var e = d.ownerDocument,
      g = e.documentElement;
    if (!c || !k.contains(g, d)) return c ? {
      top: c.top,
      left: c.left
    } : {
      top: 0,
      left: 0
    };
    d = e.body;
    e = b(e);
    return {
      top: c.top + (e.pageYOffset || k.support.boxModel && g.scrollTop || d.scrollTop) - (g.clientTop || d.clientTop || 0),
      left: c.left + (e.pageXOffset || k.support.boxModel && g.scrollLeft || d.scrollLeft) - (g.clientLeft || d.clientLeft || 0)
    }
  } : k.fn.offset = function (a) {
    var b = this[0];
    if (a) return this.each(function (b) {
      k.offset.setOffset(this, a, b)
    });
    if (!b || !b.ownerDocument) return null;
    if (b === b.ownerDocument.body) return k.offset.bodyOffset(b);
    for (var d, c = b.offsetParent, f = b.ownerDocument, e = f.documentElement, g = f.body, h = (f = f.defaultView) ? f.getComputedStyle(b, null) : b.currentStyle, j = b.offsetTop, n = b.offsetLeft;
    (b = b.parentNode) && b !== g && b !== e;) {
      if (k.support.fixedPosition && h.position === "fixed") break;
      d = f ? f.getComputedStyle(b, null) : b.currentStyle;
      j -= b.scrollTop;
      n -= b.scrollLeft;
      b === c && (j += b.offsetTop, n += b.offsetLeft, k.support.doesNotAddBorder && (!k.support.doesAddBorderForTableAndCells || !lb.test(b.nodeName)) && (j += parseFloat(d.borderTopWidth) || 0, n += parseFloat(d.borderLeftWidth) || 0), c = b.offsetParent);
      k.support.subtractsBorderForOverflowNotVisible && d.overflow !== "visible" && (j += parseFloat(d.borderTopWidth) || 0, n += parseFloat(d.borderLeftWidth) || 0);
      h = d
    }
    if (h.position === "relative" || h.position === "static") j += g.offsetTop, n += g.offsetLeft;
    k.support.fixedPosition && h.position === "fixed" && (j += Math.max(e.scrollTop, g.scrollTop), n += Math.max(e.scrollLeft, g.scrollLeft));
    return {
      top: j,
      left: n
    }
  };
  k.offset = {
    bodyOffset: function (a) {
      var b = a.offsetTop,
        d = a.offsetLeft;
      k.support.doesNotIncludeMarginInBodyOffset && (b += parseFloat(k.css(a, "marginTop")) || 0, d += parseFloat(k.css(a, "marginLeft")) || 0);
      return {
        top: b,
        left: d
      }
    },
    setOffset: function (a, b, d) {
      var c = k.css(a, "position");
      c === "static" && (a.style.position = "relative");
      var f = k(a),
        e = f.offset(),
        g = k.css(a, "top"),
        h = k.css(a, "left"),
        j = {},
        n = {},
        l, m;
      (c === "absolute" || c === "fixed") && k.inArray("auto", [g, h]) > -1 ? (n = f.position(), l = n.top, m = n.left) : (l = parseFloat(g) || 0, m = parseFloat(h) || 0);
      k.isFunction(b) && (b = b.call(a, d, e));
      b.top != null && (j.top = b.top - e.top + l);
      b.left != null && (j.left = b.left - e.left + m);
      "using" in b ? b.using.call(a, j) : f.css(j)
    }
  };
  k.fn.extend({
    position: function () {
      if (!this[0]) return null;
      var a = this[0],
        b = this.offsetParent(),
        d = this.offset(),
        c = yb.test(b[0].nodeName) ? {
          top: 0,
          left: 0
        } : b.offset();
      d.top -= parseFloat(k.css(a, "marginTop")) || 0;
      d.left -= parseFloat(k.css(a, "marginLeft")) || 0;
      c.top += parseFloat(k.css(b[0], "borderTopWidth")) || 0;
      c.left += parseFloat(k.css(b[0], "borderLeftWidth")) || 0;
      return {
        top: d.top - c.top,
        left: d.left - c.left
      }
    },
    offsetParent: function () {
      return this.map(function () {
        for (var a = this.offsetParent || H.body; a && !yb.test(a.nodeName) && k.css(a, "position") === "static";) a = a.offsetParent;
        return a
      })
    }
  });
  k.each(["Left", "Top"], function (a, d) {
    var c = "scroll" + d;
    k.fn[c] = function (d) {
      var f, g;
      if (d === e) {
        f = this[0];
        return !f ? null : (g = b(f)) ? "pageXOffset" in g ? g[a ? "pageYOffset" : "pageXOffset"] : k.support.boxModel && g.document.documentElement[c] || g.document.body[c] : f[c]
      }
      return this.each(function () {
        g = b(this);
        g ? g.scrollTo(a ? k(g).scrollLeft() : d, a ? d : k(g).scrollTop()) : this[c] = d
      })
    }
  });
  k.each(["Height", "Width"], function (a, b) {
    var d = b.toLowerCase();
    k.fn["inner" + b] = function () {
      var a = this[0];
      return a ? a.style ? parseFloat(k.css(a, d, "padding")) : this[d]() : null
    };
    k.fn["outer" + b] = function (a) {
      var b = this[0];
      return b ? b.style ? parseFloat(k.css(b, d, a ? "margin" : "border")) : this[d]() : null
    };
    k.fn[d] = function (a) {
      var c = this[0];
      if (!c) return a == null ? null : this;
      if (k.isFunction(a)) return this.each(function (b) {
        var c = k(this);
        c[d](a.call(this, b, c[d]()))
      });
      if (k.isWindow(c)) {
        var f = c.document.documentElement["client" + b],
          g = c.document.body;
        return c.document.compatMode === "CSS1Compat" && f || g && g["client" + b] || f
      }
      if (c.nodeType === 9) return Math.max(c.documentElement["client" + b], c.body["scroll" + b], c.documentElement["scroll" + b], c.body["offset" + b], c.documentElement["offset" + b]);
      return a === e ? (c = k.css(c, d), f = parseFloat(c), k.isNumeric(f) ? f : c) : this.css(d, typeof a == "string" ? a : a + "px")
    }
  });
  a.jQuery = a.$ = k;
  typeof define == "function" && define.amd && define.amd.jQuery && define("jquery", [], function () {
    return k
  })
})(window);
(function () {
  function a(b, d, c) {
    if (b === d) return b !== 0 || 1 / b == 1 / d;
    if (b == null || d == null) return b === d;
    if (b._chain) b = b._wrapped;
    if (d._chain) d = d._wrapped;
    if (b.isEqual && q.isFunction(b.isEqual)) return b.isEqual(d);
    if (d.isEqual && q.isFunction(d.isEqual)) return d.isEqual(b);
    var f = j.call(b);
    if (f != j.call(d)) return false;
    switch (f) {
    case "[object String]":
      return b == String(d);
    case "[object Number]":
      return b != +b ? d != +d : b == 0 ? 1 / b == 1 / d : b == +d;
    case "[object Date]":
    case "[object Boolean]":
      return +b == +d;
    case "[object RegExp]":
      return b.source == d.source && b.global == d.global && b.multiline == d.multiline && b.ignoreCase == d.ignoreCase
    }
    if (typeof b != "object" || typeof d != "object") return false;
    for (var e = c.length; e--;) if (c[e] == b) return true;
    c.push(b);
    var e = 0,
      g = true;
    if (f == "[object Array]") {
      if (e = b.length, g = e == d.length) for (; e--;) if (!(g = e in b == e in d && a(b[e], d[e], c))) break
    } else {
      if ("constructor" in b != "constructor" in d || b.constructor != d.constructor) return false;
      for (var h in b) if (q.has(b, h) && (e++, !(g = q.has(d, h) && a(b[h], d[h], c)))) break;
      if (g) {
        for (h in d) if (q.has(d, h) && !e--) break;
        g = !e
      }
    }
    c.pop();
    return g
  }
  var e = this,
    b = e._,
    c = {},
    d = Array.prototype,
    f = Object.prototype,
    g = d.slice,
    h = d.unshift,
    j = f.toString,
    l = f.hasOwnProperty,
    n = d.forEach,
    m = d.map,
    p = d.reduce,
    o = d.reduceRight,
    u = d.filter,
    w = d.every,
    s = d.some,
    D = d.indexOf,
    C = d.lastIndexOf,
    f = Array.isArray,
    z = Object.keys,
    F = Function.prototype.bind,
    q = function (a) {
      return new sa(a)
    };
  if (typeof exports !== "undefined") {
    if (typeof module !== "undefined" && module.exports) exports = module.exports = q;
    exports._ = q
  } else e._ = q;
  q.VERSION = "1.3.1";
  var K = q.each = q.forEach = function (a, b, d) {
      if (a != null) if (n && a.forEach === n) a.forEach(b, d);
      else if (a.length === +a.length) for (var f = 0, e = a.length; f < e; f++) {
        if (f in a && b.call(d, a[f], f, a) === c) break
      } else for (f in a) if (q.has(a, f) && b.call(d, a[f], f, a) === c) break
    };
  q.map = q.collect = function (a, b, d) {
    var c = [];
    if (a == null) return c;
    if (m && a.map === m) return a.map(b, d);
    K(a, function (a, f, e) {
      c[c.length] = b.call(d, a, f, e)
    });
    if (a.length === +a.length) c.length = a.length;
    return c
  };
  q.reduce = q.foldl = q.inject = function (a, b, d, c) {
    var f = arguments.length > 2;
    a == null && (a = []);
    if (p && a.reduce === p) return c && (b = q.bind(b, c)), f ? a.reduce(b, d) : a.reduce(b);
    K(a, function (a, e, g) {
      f ? d = b.call(c, d, a, e, g) : (d = a, f = true)
    });
    if (!f) throw new TypeError("Reduce of empty array with no initial value");
    return d
  };
  q.reduceRight = q.foldr = function (a, b, d, c) {
    var f = arguments.length > 2;
    a == null && (a = []);
    if (o && a.reduceRight === o) return c && (b = q.bind(b, c)), f ? a.reduceRight(b, d) : a.reduceRight(b);
    var e = q.toArray(a).reverse();
    c && !f && (b = q.bind(b, c));
    return f ? q.reduce(e, b, d, c) : q.reduce(e, b)
  };
  q.find = q.detect = function (a, b, d) {
    var c;
    O(a, function (a, f, e) {
      if (b.call(d, a, f, e)) return c = a, true
    });
    return c
  };
  q.filter = q.select = function (a, b, d) {
    var c = [];
    if (a == null) return c;
    if (u && a.filter === u) return a.filter(b, d);
    K(a, function (a, f, e) {
      b.call(d, a, f, e) && (c[c.length] = a)
    });
    return c
  };
  q.reject = function (a, b, d) {
    var c = [];
    if (a == null) return c;
    K(a, function (a, f, e) {
      b.call(d, a, f, e) || (c[c.length] = a)
    });
    return c
  };
  q.every = q.all = function (a, b, d) {
    var f = true;
    if (a == null) return f;
    if (w && a.every === w) return a.every(b, d);
    K(a, function (a, e, g) {
      if (!(f = f && b.call(d, a, e, g))) return c
    });
    return f
  };
  var O = q.some = q.any = function (a, b, d) {
      b || (b = q.identity);
      var f = false;
      if (a == null) return f;
      if (s && a.some === s) return a.some(b, d);
      K(a, function (a, e, g) {
        if (f || (f = b.call(d, a, e, g))) return c
      });
      return !!f
    };
  q.include = q.contains = function (a, b) {
    var d;
    return a == null ? false : D && a.indexOf === D ? a.indexOf(b) != -1 : d = O(a, function (a) {
      return a === b
    })
  };
  q.invoke = function (a, b) {
    var d = g.call(arguments, 2);
    return q.map(a, function (a) {
      return (q.isFunction(b) ? b || a : a[b]).apply(a, d)
    })
  };
  q.pluck = function (a, b) {
    return q.map(a, function (a) {
      return a[b]
    })
  };
  q.max = function (a, b, d) {
    if (!b && q.isArray(a)) return Math.max.apply(Math, a);
    if (!b && q.isEmpty(a)) return -Infinity;
    var c = {
      computed: -Infinity
    };
    K(a, function (a, f, e) {
      f = b ? b.call(d, a, f, e) : a;
      f >= c.computed && (c = {
        value: a,
        computed: f
      })
    });
    return c.value
  };
  q.min = function (a, b, d) {
    if (!b && q.isArray(a)) return Math.min.apply(Math, a);
    if (!b && q.isEmpty(a)) return Infinity;
    var c = {
      computed: Infinity
    };
    K(a, function (a, f, e) {
      f = b ? b.call(d, a, f, e) : a;
      f < c.computed && (c = {
        value: a,
        computed: f
      })
    });
    return c.value
  };
  q.shuffle = function (a) {
    var b = [],
      d;
    K(a, function (a, c) {
      c == 0 ? b[0] = a : (d = Math.floor(Math.random() * (c + 1)), b[c] = b[d], b[d] = a)
    });
    return b
  };
  q.sortBy = function (a, b, d) {
    return q.pluck(q.map(a, function (a, c, f) {
      return {
        value: a,
        criteria: b.call(d, a, c, f)
      }
    }).sort(function (a, b) {
      var d = a.criteria,
        c = b.criteria;
      return d < c ? -1 : d > c ? 1 : 0
    }), "value")
  };
  q.groupBy = function (a, b) {
    var d = {},
      c = q.isFunction(b) ? b : function (a) {
        return a[b]
      };
    K(a, function (a, b) {
      var f = c(a, b);
      (d[f] || (d[f] = [])).push(a)
    });
    return d
  };
  q.sortedIndex = function (a, b, d) {
    d || (d = q.identity);
    for (var c = 0, f = a.length; c < f;) {
      var e = c + f >> 1;
      d(a[e]) < d(b) ? c = e + 1 : f = e
    }
    return c
  };
  q.toArray = function (a) {
    return !a ? [] : a.toArray ? a.toArray() : q.isArray(a) ? g.call(a) : q.isArguments(a) ? g.call(a) : q.values(a)
  };
  q.size = function (a) {
    return q.toArray(a).length
  };
  q.first = q.head = function (a, b, d) {
    return b != null && !d ? g.call(a, 0, b) : a[0]
  };
  q.initial = function (a, b, d) {
    return g.call(a, 0, a.length - (b == null || d ? 1 : b))
  };
  q.last = function (a, b, d) {
    return b != null && !d ? g.call(a, Math.max(a.length - b, 0)) : a[a.length - 1]
  };
  q.rest = q.tail = function (a, b, d) {
    return g.call(a, b == null || d ? 1 : b)
  };
  q.compact = function (a) {
    return q.filter(a, function (a) {
      return !!a
    })
  };
  q.flatten = function (a, b) {
    return q.reduce(a, function (a, d) {
      if (q.isArray(d)) return a.concat(b ? d : q.flatten(d));
      a[a.length] = d;
      return a
    }, [])
  };
  q.without = function (a) {
    return q.difference(a, g.call(arguments, 1))
  };
  q.uniq = q.unique = function (a, b, d) {
    var d = d ? q.map(a, d) : a,
      c = [];
    q.reduce(d, function (d, f, e) {
      if (0 == e || (b === true ? q.last(d) != f : !q.include(d, f))) d[d.length] = f, c[c.length] = a[e];
      return d
    }, []);
    return c
  };
  q.union = function () {
    return q.uniq(q.flatten(arguments, true))
  };
  q.intersection = q.intersect = function (a) {
    var b = g.call(arguments, 1);
    return q.filter(q.uniq(a), function (a) {
      return q.every(b, function (b) {
        return q.indexOf(b, a) >= 0
      })
    })
  };
  q.difference = function (a) {
    var b = q.flatten(g.call(arguments, 1));
    return q.filter(a, function (a) {
      return !q.include(b, a)
    })
  };
  q.zip = function () {
    for (var a = g.call(arguments), b = q.max(q.pluck(a, "length")), d = Array(b), c = 0; c < b; c++) d[c] = q.pluck(a, "" + c);
    return d
  };
  q.indexOf = function (a, b, d) {
    if (a == null) return -1;
    var c;
    if (d) return d = q.sortedIndex(a, b), a[d] === b ? d : -1;
    if (D && a.indexOf === D) return a.indexOf(b);
    for (d = 0, c = a.length; d < c; d++) if (d in a && a[d] === b) return d;
    return -1
  };
  q.lastIndexOf = function (a, b) {
    if (a == null) return -1;
    if (C && a.lastIndexOf === C) return a.lastIndexOf(b);
    for (var d = a.length; d--;) if (d in a && a[d] === b) return d;
    return -1
  };
  q.range = function (a, b, d) {
    arguments.length <= 1 && (b = a || 0, a = 0);
    for (var d = arguments[2] || 1, c = Math.max(Math.ceil((b - a) / d), 0), f = 0, e = Array(c); f < c;) e[f++] = a, a += d;
    return e
  };
  var V = function () {};
  q.bind = function (a, b) {
    var d, c;
    if (a.bind === F && F) return F.apply(a, g.call(arguments, 1));
    if (!q.isFunction(a)) throw new TypeError;
    c = g.call(arguments, 2);
    return d = function () {
      if (!(this instanceof d)) return a.apply(b, c.concat(g.call(arguments)));
      V.prototype = a.prototype;
      var f = new V,
        e = a.apply(f, c.concat(g.call(arguments)));
      return Object(e) === e ? e : f
    }
  };
  q.bindAll = function (a) {
    var b = g.call(arguments, 1);
    b.length == 0 && (b = q.functions(a));
    K(b, function (b) {
      a[b] = q.bind(a[b], a)
    });
    return a
  };
  q.memoize = function (a, b) {
    var d = {};
    b || (b = q.identity);
    return function () {
      var c = b.apply(this, arguments);
      return q.has(d, c) ? d[c] : d[c] = a.apply(this, arguments)
    }
  };
  q.delay = function (a, b) {
    var d = g.call(arguments, 2);
    return setTimeout(function () {
      return a.apply(a, d)
    }, b)
  };
  q.defer = function (a) {
    return q.delay.apply(q, [a, 1].concat(g.call(arguments, 1)))
  };
  q.throttle = function (a, b) {
    var d, c, f, e, g, h = q.debounce(function () {
      g = e = false
    }, b);
    return function () {
      d = this;
      c = arguments;
      f || (f = setTimeout(function () {
        f = null;
        g && a.apply(d, c);
        h()
      }, b));
      e ? g = true : a.apply(d, c);
      h();
      e = true
    }
  };
  q.debounce = function (a, b) {
    var d;
    return function () {
      var c = this,
        f = arguments;
      clearTimeout(d);
      d = setTimeout(function () {
        d = null;
        a.apply(c, f)
      }, b)
    }
  };
  q.once = function (a) {
    var b = false,
      d;
    return function () {
      if (b) return d;
      b = true;
      return d = a.apply(this, arguments)
    }
  };
  q.wrap = function (a, b) {
    return function () {
      var d = [a].concat(g.call(arguments, 0));
      return b.apply(this, d)
    }
  };
  q.compose = function () {
    var a = arguments;
    return function () {
      for (var b = arguments, d = a.length - 1; d >= 0; d--) b = [a[d].apply(this, b)];
      return b[0]
    }
  };
  q.after = function (a, b) {
    return a <= 0 ? b() : function () {
      if (--a < 1) return b.apply(this, arguments)
    }
  };
  q.keys = z ||
  function (a) {
    if (a !== Object(a)) throw new TypeError("Invalid object");
    var b = [],
      d;
    for (d in a) q.has(a, d) && (b[b.length] = d);
    return b
  };
  q.values = function (a) {
    return q.map(a, q.identity)
  };
  q.functions = q.methods = function (a) {
    var b = [],
      d;
    for (d in a) q.isFunction(a[d]) && b.push(d);
    return b.sort()
  };
  q.extend = function (a) {
    K(g.call(arguments, 1), function (b) {
      for (var d in b) a[d] = b[d]
    });
    return a
  };
  q.defaults = function (a) {
    K(g.call(arguments, 1), function (b) {
      for (var d in b) a[d] == null && (a[d] = b[d])
    });
    return a
  };
  q.clone = function (a) {
    return !q.isObject(a) ? a : q.isArray(a) ? a.slice() : q.extend({}, a)
  };
  q.tap = function (a, b) {
    b(a);
    return a
  };
  q.isEqual = function (b, d) {
    return a(b, d, [])
  };
  q.isEmpty = function (a) {
    if (q.isArray(a) || q.isString(a)) return a.length === 0;
    for (var b in a) if (q.has(a, b)) return false;
    return true
  };
  q.isElement = function (a) {
    return !!(a && a.nodeType == 1)
  };
  q.isArray = f ||
  function (a) {
    return j.call(a) == "[object Array]"
  };
  q.isObject = function (a) {
    return a === Object(a)
  };
  q.isArguments = function (a) {
    return j.call(a) == "[object Arguments]"
  };
  if (!q.isArguments(arguments)) q.isArguments = function (a) {
    return !(!a || !q.has(a, "callee"))
  };
  q.isFunction = function (a) {
    return j.call(a) == "[object Function]"
  };
  q.isString = function (a) {
    return j.call(a) == "[object String]"
  };
  q.isNumber = function (a) {
    return j.call(a) == "[object Number]"
  };
  q.isNaN = function (a) {
    return a !== a
  };
  q.isBoolean = function (a) {
    return a === true || a === false || j.call(a) == "[object Boolean]"
  };
  q.isDate = function (a) {
    return j.call(a) == "[object Date]"
  };
  q.isRegExp = function (a) {
    return j.call(a) == "[object RegExp]"
  };
  q.isNull = function (a) {
    return a === null
  };
  q.isUndefined = function (a) {
    return a === void 0
  };
  q.has = function (a, b) {
    return l.call(a, b)
  };
  q.noConflict = function () {
    e._ = b;
    return this
  };
  q.identity = function (a) {
    return a
  };
  q.times = function (a, b, d) {
    for (var c = 0; c < a; c++) b.call(d, c)
  };
  q.escape = function (a) {
    return ("" + a).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#x27;").replace(/\//g, "&#x2F;")
  };
  q.mixin = function (a) {
    K(q.functions(a), function (b) {
      k(b, q[b] = a[b])
    })
  };
  var cb = 0;
  q.uniqueId = function (a) {
    var b = cb++;
    return a ? a + b : b
  };
  q.templateSettings = {
    evaluate: /<%([\s\S]+?)%>/g,
    interpolate: /<%=([\s\S]+?)%>/g,
    escape: /<%-([\s\S]+?)%>/g
  };
  var Ha = /.^/,
    H = function (a) {
      return a.replace(/\\\\/g, "\\").replace(/\\'/g, "'")
    };
  q.template = function (a, b) {
    var d = q.templateSettings,
      d = "var __p=[],print=function(){__p.push.apply(__p,arguments);};with(obj||{}){__p.push('" + a.replace(/\\/g, "\\\\").replace(/'/g, "\\'").replace(d.escape || Ha, function (a, b) {
        return "',_.escape(" + H(b) + "),'"
      }).replace(d.interpolate || Ha, function (a, b) {
        return "'," + H(b) + ",'"
      }).replace(d.evaluate || Ha, function (a, b) {
        return "');" + H(b).replace(/[\r\n\t]/g, " ") + ";__p.push('"
      }).replace(/\r/g, "\\r").replace(/\n/g, "\\n").replace(/\t/g, "\\t") + "');}return __p.join('');",
      c = new Function("obj", "_", d);
    return b ? c(b, q) : function (a) {
      return c.call(this, a, q)
    }
  };
  q.chain = function (a) {
    return q(a).chain()
  };
  var sa = function (a) {
      this._wrapped = a
    };
  q.prototype = sa.prototype;
  var gb = function (a, b) {
      return b ? q(a).chain() : a
    },
    k = function (a, b) {
      sa.prototype[a] = function () {
        var a = g.call(arguments);
        h.call(a, this._wrapped);
        return gb(b.apply(q, a), this._chain)
      }
    };
  q.mixin(q);
  K("pop,push,reverse,shift,sort,splice,unshift".split(","), function (a) {
    var b = d[a];
    sa.prototype[a] = function () {
      var d = this._wrapped;
      b.apply(d, arguments);
      var c = d.length;
      (a == "shift" || a == "splice") && c === 0 && delete d[0];
      return gb(d, this._chain)
    }
  });
  K(["concat", "join", "slice"], function (a) {
    var b = d[a];
    sa.prototype[a] = function () {
      return gb(b.apply(this._wrapped, arguments), this._chain)
    }
  });
  sa.prototype.chain = function () {
    this._chain = true;
    return this
  };
  sa.prototype.value = function () {
    return this._wrapped
  }
}).call(this);
(function () {
  var a = this,
    e = a.Backbone,
    b = Array.prototype.slice,
    c = Array.prototype.splice,
    d;
  d = typeof exports !== "undefined" ? exports : a.Backbone = {};
  d.VERSION = "0.9.0";
  var f = a._;
  !f && typeof require !== "undefined" && (f = require("underscore"));
  var g = a.jQuery || a.Zepto || a.ender;
  d.noConflict = function () {
    a.Backbone = e;
    return this
  };
  d.emulateHTTP = false;
  d.emulateJSON = false;
  d.Events = {
    on: function (a, b, d) {
      for (var c, a = a.split(/\s+/), f = this._callbacks || (this._callbacks = {}); c = a.shift();) {
        c = f[c] || (f[c] = {});
        var e = c.tail || (c.tail = c.next = {});
        e.callback = b;
        e.context = d;
        c.tail = e.next = {}
      }
      return this
    },
    off: function (a, b, d) {
      var c, f, e;
      if (a) {
        if (f = this._callbacks) for (a = a.split(/\s+/); c = a.shift();) if (e = f[c], delete f[c], b && e) for (;
        (e = e.next) && e.next;) if (!(e.callback === b && (!d || e.context === d))) this.on(c, e.callback, e.context)
      } else delete this._callbacks;
      return this
    },
    trigger: function (a) {
      var d, c, f, e;
      if (!(f = this._callbacks)) return this;
      e = f.all;
      for ((a = a.split(/\s+/)).push(null); d = a.shift();) e && a.push({
        next: e.next,
        tail: e.tail,
        event: d
      }), (c = f[d]) && a.push({
        next: c.next,
        tail: c.tail
      });
      for (e = b.call(arguments, 1); c = a.pop();) {
        d = c.tail;
        for (f = c.event ? [c.event].concat(e) : e;
        (c = c.next) !== d;) c.callback.apply(c.context || this, f)
      }
      return this
    }
  };
  d.Events.bind = d.Events.on;
  d.Events.unbind = d.Events.off;
  d.Model = function (a, b) {
    var d;
    a || (a = {});
    b && b.parse && (a = this.parse(a));
    if (d = C(this, "defaults")) a = f.extend({}, d, a);
    if (b && b.collection) this.collection = b.collection;
    this.attributes = {};
    this._escapedAttributes = {};
    this.cid = f.uniqueId("c");
    this._changed = {};
    if (!this.set(a, {
      silent: true
    })) throw Error("Can't create an invalid model");
    this._changed = {};
    this._previousAttributes = f.clone(this.attributes);
    this.initialize.apply(this, arguments)
  };
  f.extend(d.Model.prototype, d.Events, {
    idAttribute: "id",
    initialize: function () {},
    toJSON: function () {
      return f.clone(this.attributes)
    },
    get: function (a) {
      return this.attributes[a]
    },
    escape: function (a) {
      var b;
      if (b = this._escapedAttributes[a]) return b;
      b = this.attributes[a];
      return this._escapedAttributes[a] = f.escape(b == null ? "" : "" + b)
    },
    has: function (a) {
      return this.attributes[a] != null
    },
    set: function (a, b, c) {
      var e, g;
      f.isObject(a) || a == null ? (e = a, c = b) : (e = {}, e[a] = b);
      c || (c = {});
      if (!e) return this;
      if (e instanceof d.Model) e = e.attributes;
      if (c.unset) for (g in e) e[g] = void 0;
      if (this.validate && !this._performValidation(e, c)) return false;
      if (this.idAttribute in e) this.id = e[this.idAttribute];
      var b = this.attributes,
        h = this._escapedAttributes,
        j = this._previousAttributes || {},
        n = this._changing;
      this._changing = true;
      for (g in e) if (a = e[g], f.isEqual(b[g], a) || delete h[g], c.unset ? delete b[g] : b[g] = a, delete this._changed[g], !f.isEqual(j[g], a) || f.has(b, g) != f.has(j, g)) this._changed[g] = a;
      if (!n)!c.silent && this.hasChanged() && this.change(c), this._changing = false;
      return this
    },
    unset: function (a, b) {
      (b || (b = {})).unset = true;
      return this.set(a, null, b)
    },
    clear: function (a) {
      (a || (a = {})).unset = true;
      return this.set(f.clone(this.attributes), a)
    },
    fetch: function (a) {
      var a = a ? f.clone(a) : {},
        b = this,
        c = a.success;
      a.success = function (d, f, e) {
        if (!b.set(b.parse(d, e), a)) return false;
        c && c(b, d)
      };
      a.error = d.wrapError(a.error, b, a);
      return (this.sync || d.sync).call(this, "read", this, a)
    },
    save: function (a, b, c) {
      var e;
      f.isObject(a) || a == null ? (e = a, c = b) : (e = {}, e[a] = b);
      c = c ? f.clone(c) : {};
      if (e && !this[c.wait ? "_performValidation" : "set"](e, c)) return false;
      var g = this,
        h = c.success;
      c.success = function (a, b, d) {
        b = g.parse(a, d);
        c.wait && (b = f.extend(e || {}, b));
        if (!g.set(b, c)) return false;
        h ? h(g, a) : g.trigger("sync", g, a, c)
      };
      c.error = d.wrapError(c.error, g, c);
      a = this.isNew() ? "create" : "update";
      return (this.sync || d.sync).call(this, a, this, c)
    },
    destroy: function (a) {
      var a = a ? f.clone(a) : {},
        b = this,
        c = a.success,
        e = function () {
          b.trigger("destroy", b, b.collection, a)
        };
      if (this.isNew()) return e();
      a.success = function (d) {
        a.wait && e();
        c ? c(b, d) : b.trigger("sync", b, d, a)
      };
      a.error = d.wrapError(a.error, b, a);
      var g = (this.sync || d.sync).call(this, "delete", this, a);
      a.wait || e();
      return g
    },
    url: function () {
      var a = C(this.collection, "url") || C(this, "urlRoot") || z();
      return this.isNew() ? a : a + (a.charAt(a.length - 1) == "/" ? "" : "/") + encodeURIComponent(this.id)
    },
    parse: function (a) {
      return a
    },
    clone: function () {
      return new this.constructor(this.attributes)
    },
    isNew: function () {
      return this.id == null
    },
    change: function (a) {
      for (var b in this._changed) this.trigger("change:" + b, this, this._changed[b], a);
      this.trigger("change", this, a);
      this._previousAttributes = f.clone(this.attributes);
      this._changed = {}
    },
    hasChanged: function (a) {
      return a ? f.has(this._changed, a) : !f.isEmpty(this._changed)
    },
    changedAttributes: function (a) {
      if (!a) return this.hasChanged() ? f.clone(this._changed) : false;
      var b, d = false,
        c = this._previousAttributes,
        e;
      for (e in a) if (!f.isEqual(c[e], b = a[e]))(d || (d = {}))[e] = b;
      return d
    },
    previous: function (a) {
      return !a || !this._previousAttributes ? null : this._previousAttributes[a]
    },
    previousAttributes: function () {
      return f.clone(this._previousAttributes)
    },
    _performValidation: function (a, b) {
      var d = this.validate(f.extend({}, this.attributes, a), b);
      return d ? (b.error ? b.error(this, d, b) : this.trigger("error", this, d, b), false) : true
    }
  });
  d.Collection = function (a, b) {
    b || (b = {});
    if (b.comparator) this.comparator = b.comparator;
    this._reset();
    this.initialize.apply(this, arguments);
    a && this.reset(a, {
      silent: true,
      parse: b.parse
    })
  };
  f.extend(d.Collection.prototype, d.Events, {
    model: d.Model,
    initialize: function () {},
    toJSON: function () {
      return this.map(function (a) {
        return a.toJSON()
      })
    },
    add: function (a, b) {
      var d, e, g, h, j, n = {},
        l = {};
      b || (b = {});
      a = f.isArray(a) ? a.slice() : [a];
      for (d = 0, e = a.length; d < e; d++) {
        if (!(g = a[d] = this._prepareModel(a[d], b))) throw Error("Can't add an invalid model to a collection");
        if (n[h = g.cid] || this._byCid[h] || (j = g.id) != null && (l[j] || this._byId[j])) throw Error("Can't add the same model to a collection twice");
        n[h] = l[j] = g
      }
      for (d = 0; d < e; d++)(g = a[d]).on("all", this._onModelEvent, this), this._byCid[g.cid] = g, g.id != null && (this._byId[g.id] = g);
      this.length += e;
      c.apply(this.models, [b.at != null ? b.at : this.models.length, 0].concat(a));
      this.comparator && this.sort({
        silent: true
      });
      if (b.silent) return this;
      for (d = 0, e = this.models.length; d < e; d++) if (n[(g = this.models[d]).cid]) b.index = d, g.trigger("add", g, this, b);
      return this
    },
    remove: function (a, b) {
      var d, c, e, g;
      b || (b = {});
      a = f.isArray(a) ? a.slice() : [a];
      for (d = 0, c = a.length; d < c; d++) if (g = this.getByCid(a[d]) || this.get(a[d])) {
        delete this._byId[g.id];
        delete this._byCid[g.cid];
        e = this.indexOf(g);
        this.models.splice(e, 1);
        this.length--;
        if (!b.silent) b.index = e, g.trigger("remove", g, this, b);
        this._removeReference(g)
      }
      return this
    },
    get: function (a) {
      return a == null ? null : this._byId[a.id != null ? a.id : a]
    },
    getByCid: function (a) {
      return a && this._byCid[a.cid || a]
    },
    at: function (a) {
      return this.models[a]
    },
    sort: function (a) {
      a || (a = {});
      if (!this.comparator) throw Error("Cannot sort a set without a comparator");
      var b = f.bind(this.comparator, this);
      this.comparator.length == 1 ? this.models = this.sortBy(b) : this.models.sort(b);
      a.silent || this.trigger("reset", this, a);
      return this
    },
    pluck: function (a) {
      return f.map(this.models, function (b) {
        return b.get(a)
      })
    },
    reset: function (a, b) {
      a || (a = []);
      b || (b = {});
      for (var d = 0, c = this.models.length; d < c; d++) this._removeReference(this.models[d]);
      this._reset();
      this.add(a, {
        silent: true,
        parse: b.parse
      });
      b.silent || this.trigger("reset", this, b);
      return this
    },
    fetch: function (a) {
      a = a ? f.clone(a) : {};
      if (a.parse === void 0) a.parse = true;
      var b = this,
        c = a.success;
      a.success = function (d, f, e) {
        b[a.add ? "add" : "reset"](b.parse(d, e), a);
        c && c(b, d)
      };
      a.error = d.wrapError(a.error, b, a);
      return (this.sync || d.sync).call(this, "read", this, a)
    },
    create: function (a, b) {
      var d = this,
        b = b ? f.clone(b) : {},
        a = this._prepareModel(a, b);
      if (!a) return false;
      b.wait || d.add(a, b);
      var c = b.success;
      b.success = function (f, e) {
        b.wait && d.add(f, b);
        c ? c(f, e) : f.trigger("sync", a, e, b)
      };
      a.save(null, b);
      return a
    },
    parse: function (a) {
      return a
    },
    chain: function () {
      return f(this.models).chain()
    },
    _reset: function () {
      this.length = 0;
      this.models = [];
      this._byId = {};
      this._byCid = {}
    },
    _prepareModel: function (a, b) {
      if (a instanceof d.Model) {
        if (!a.collection) a.collection = this
      } else b.collection = this, a = new this.model(a, b), a.validate && !a._performValidation(a.attributes, b) && (a = false);
      return a
    },
    _removeReference: function (a) {
      this == a.collection && delete a.collection;
      a.off("all", this._onModelEvent, this)
    },
    _onModelEvent: function (a, b, d, c) {
      (a == "add" || a == "remove") && d != this || (a == "destroy" && this.remove(b, c), b && a === "change:" + b.idAttribute && (delete this._byId[b.previous(b.idAttribute)], this._byId[b.id] = b), this.trigger.apply(this, arguments))
    }
  });
  f.each("forEach,each,map,reduce,reduceRight,find,detect,filter,select,reject,every,all,some,any,include,contains,invoke,max,min,sortBy,sortedIndex,toArray,size,first,initial,rest,last,without,indexOf,shuffle,lastIndexOf,isEmpty,groupBy".split(","), function (a) {
    d.Collection.prototype[a] = function () {
      return f[a].apply(f, [this.models].concat(f.toArray(arguments)))
    }
  });
  d.Router = function (a) {
    a || (a = {});
    if (a.routes) this.routes = a.routes;
    this._bindRoutes();
    this.initialize.apply(this, arguments)
  };
  var h = /:\w+/g,
    j = /\*\w+/g,
    l = /[-[\]{}()+?.,\\^$|#\s]/g;
  f.extend(d.Router.prototype, d.Events, {
    initialize: function () {},
    route: function (a, b, c) {
      d.history || (d.history = new d.History);
      f.isRegExp(a) || (a = this._routeToRegExp(a));
      c || (c = this[b]);
      d.history.route(a, f.bind(function (f) {
        f = this._extractParameters(a, f);
        c && c.apply(this, f);
        this.trigger.apply(this, ["route:" + b].concat(f));
        d.history.trigger("route", this, b, f)
      }, this));
      return this
    },
    navigate: function (a, b) {
      d.history.navigate(a, b)
    },
    _bindRoutes: function () {
      if (this.routes) {
        var a = [],
          b;
        for (b in this.routes) a.unshift([b, this.routes[b]]);
        b = 0;
        for (var d = a.length; b < d; b++) this.route(a[b][0], a[b][1], this[a[b][1]])
      }
    },
    _routeToRegExp: function (a) {
      a = a.replace(l, "\\$&").replace(h, "([^/]+)").replace(j, "(.*?)");
      return RegExp("^" + a + "$")
    },
    _extractParameters: function (a, b) {
      return a.exec(b).slice(1)
    }
  });
  d.History = function () {
    this.handlers = [];
    f.bindAll(this, "checkUrl")
  };
  var n = /^[#\/]/,
    m = /msie [\w.]+/,
    p = false;
  f.extend(d.History.prototype, d.Events, {
    interval: 50,
    getFragment: function (a, b) {
      if (a == null) if (this._hasPushState || b) {
        var a = window.location.pathname,
          d = window.location.search;
        d && (a += d)
      } else a = window.location.hash;
      a = decodeURIComponent(a.replace(n, ""));
      a.indexOf(this.options.root) || (a = a.substr(this.options.root.length));
      return a
    },
    start: function (a) {
      if (p) throw Error("Backbone.history has already been started");
      this.options = f.extend({}, {
        root: "/"
      }, this.options, a);
      this._wantsHashChange = this.options.hashChange !== false;
      this._wantsPushState = !! this.options.pushState;
      this._hasPushState = !(!this.options.pushState || !window.history || !window.history.pushState);
      var a = this.getFragment(),
        b = document.documentMode;
      if (b = m.exec(navigator.userAgent.toLowerCase()) && (!b || b <= 7)) this.iframe = g('<iframe src="javascript:0" tabindex="-1" />').hide().appendTo("body")[0].contentWindow, this.navigate(a);
      if (this._hasPushState) g(window).bind("popstate", this.checkUrl);
      else if (this._wantsHashChange && "onhashchange" in window && !b) g(window).bind("hashchange", this.checkUrl);
      else if (this._wantsHashChange) this._checkUrlInterval = setInterval(this.checkUrl, this.interval);
      this.fragment = a;
      p = true;
      a = window.location;
      b = a.pathname == this.options.root;
      if (this._wantsHashChange && this._wantsPushState && !this._hasPushState && !b) return this.fragment = this.getFragment(null, true), window.location.replace(this.options.root + "#" + this.fragment), true;
      else if (this._wantsPushState && this._hasPushState && b && a.hash) this.fragment = a.hash.replace(n, ""), window.history.replaceState({}, document.title, a.protocol + "//" + a.host + this.options.root + this.fragment);
      if (!this.options.silent) return this.loadUrl()
    },
    stop: function () {
      g(window).unbind("popstate", this.checkUrl).unbind("hashchange", this.checkUrl);
      clearInterval(this._checkUrlInterval);
      p = false
    },
    route: function (a, b) {
      this.handlers.unshift({
        route: a,
        callback: b
      })
    },
    checkUrl: function () {
      var a = this.getFragment();
      a == this.fragment && this.iframe && (a = this.getFragment(this.iframe.location.hash));
      if (a == this.fragment || a == decodeURIComponent(this.fragment)) return false;
      this.iframe && this.navigate(a);
      this.loadUrl() || this.loadUrl(window.location.hash)
    },
    loadUrl: function (a) {
      var b = this.fragment = this.getFragment(a);
      return f.any(this.handlers, function (a) {
        if (a.route.test(b)) return a.callback(b), true
      })
    },
    navigate: function (a, b) {
      if (!p) return false;
      if (!b || b === true) b = {
        trigger: b
      };
      var d = (a || "").replace(n, "");
      if (!(this.fragment == d || this.fragment == decodeURIComponent(d))) this._hasPushState ? (d.indexOf(this.options.root) != 0 && (d = this.options.root + d), this.fragment = d, window.history[b.replace ? "replaceState" : "pushState"]({}, document.title, d)) : this._wantsHashChange ? (this.fragment = d, this._updateHash(window.location, d, b.replace), this.iframe && d != this.getFragment(this.iframe.location.hash) && (b.replace || this.iframe.document.open().close(), this._updateHash(this.iframe.location, d, b.replace))) : window.location.assign(this.options.root + a), b.trigger && this.loadUrl(a)
    },
    _updateHash: function (a, b, d) {
      d ? a.replace(a.toString().replace(/(javascript:|#).*$/, "") + "#" + b) : a.hash = b
    }
  });
  d.View = function (a) {
    this.cid = f.uniqueId("view");
    this._configure(a || {});
    this._ensureElement();
    this.initialize.apply(this, arguments);
    this.delegateEvents()
  };
  var o = /^(\S+)\s*(.*)$/,
    u = "model,collection,el,id,attributes,className,tagName".split(",");
  f.extend(d.View.prototype, d.Events, {
    tagName: "div",
    $: function (a) {
      return this.$el.find(a)
    },
    initialize: function () {},
    render: function () {
      return this
    },
    remove: function () {
      this.$el.remove();
      return this
    },
    make: function (a, b, d) {
      a = document.createElement(a);
      b && g(a).attr(b);
      d && g(a).html(d);
      return a
    },
    setElement: function (a, b) {
      this.$el = g(a);
      this.el = this.$el[0];
      b !== false && this.delegateEvents()
    },
    delegateEvents: function (a) {
      if (a || (a = C(this, "events"))) {
        this.undelegateEvents();
        for (var b in a) {
          var d = a[b];
          f.isFunction(d) || (d = this[a[b]]);
          if (!d) throw Error('Event "' + a[b] + '" does not exist');
          var c = b.match(o),
            e = c[1],
            c = c[2],
            d = f.bind(d, this);
          e += ".delegateEvents" + this.cid;
          c === "" ? this.$el.bind(e, d) : this.$el.delegate(c, e, d)
        }
      }
    },
    undelegateEvents: function () {
      this.$el.unbind(".delegateEvents" + this.cid)
    },
    _configure: function (a) {
      this.options && (a = f.extend({}, this.options, a));
      for (var b = 0, d = u.length; b < d; b++) {
        var c = u[b];
        a[c] && (this[c] = a[c])
      }
      this.options = a
    },
    _ensureElement: function () {
      if (this.el) this.setElement(this.el, false);
      else {
        var a = C(this, "attributes") || {};
        if (this.id) a.id = this.id;
        if (this.className) a["class"] = this.className;
        this.setElement(this.make(this.tagName, a), false)
      }
    }
  });
  d.Model.extend = d.Collection.extend = d.Router.extend = d.View.extend = function (a, b) {
    var d = D(this, a, b);
    d.extend = this.extend;
    return d
  };
  var w = {
    create: "POST",
    update: "PUT",
    "delete": "DELETE",
    read: "GET"
  };
  d.sync = function (a, b, c) {
    var e = w[a],
      h = {
        type: e,
        dataType: "json"
      };
    if (!c.url) h.url = C(b, "url") || z();
    if (!c.data && b && (a == "create" || a == "update")) h.contentType = "application/json", h.data = JSON.stringify(b.toJSON());
    if (d.emulateJSON) h.contentType = "application/x-www-form-urlencoded", h.data = h.data ? {
      model: h.data
    } : {};
    if (d.emulateHTTP && (e === "PUT" || e === "DELETE")) {
      if (d.emulateJSON) h.data._method = e;
      h.type = "POST";
      h.beforeSend = function (a) {
        a.setRequestHeader("X-HTTP-Method-Override", e)
      }
    }
    if (h.type !== "GET" && !d.emulateJSON) h.processData = false;
    return g.ajax(f.extend(h, c))
  };
  d.wrapError = function (a, b, d) {
    return function (c, f) {
      f = c === b ? f : c;
      a ? a(c, f, d) : b.trigger("error", c, f, d)
    }
  };
  var s = function () {},
    D = function (a, b, d) {
      var c;
      c = b && b.hasOwnProperty("constructor") ? b.constructor : function () {
        a.apply(this, arguments)
      };
      f.extend(c, a);
      s.prototype = a.prototype;
      c.prototype = new s;
      b && f.extend(c.prototype, b);
      d && f.extend(c, d);
      c.prototype.constructor = c;
      c.__super__ = a.prototype;
      return c
    },
    C = function (a, b) {
      return !a || !a[b] ? null : f.isFunction(a[b]) ? a[b]() : a[b]
    },
    z = function () {
      throw Error('A "url" property or function must be specified');
    }
}).call(this);

function printStackTrace(a) {
  var e = a && a.e ? a.e : null,
    a = a ? !! a.guess : true,
    b = new printStackTrace.implementation,
    e = b.run(e);
  return a ? b.guessFunctions(e) : e
}
printStackTrace.implementation = function () {};
printStackTrace.implementation.prototype = {
  run: function (a) {
    var e = this._mode || this.mode();
    if (e === "other") return this.other(arguments.callee);
    else {
      var b;
      if (!(b = a)) a: {
        try {
          0()
        } catch (c) {
          b = c;
          break a
        }
        b = void 0
      }
      return this[e](b)
    }
  },
  mode: function () {
    try {
      0()
    } catch (a) {
      if (a.arguments) return this._mode = "chrome";
      else if (window.opera && a.stacktrace) return this._mode = "opera10";
      else if (a.stack) return this._mode = "firefox";
      else if (window.opera && !("stacktrace" in a)) return this._mode = "opera"
    }
    return this._mode = "other"
  },
  instrumentFunction: function (a, e, b) {
    a = a || window;
    a["_old" + e] = a[e];
    a[e] = function () {
      b.call(this, printStackTrace());
      return a["_old" + e].apply(this, arguments)
    };
    a[e]._instrumented = true
  },
  deinstrumentFunction: function (a, e) {
    a[e].constructor === Function && a[e]._instrumented && a["_old" + e].constructor === Function && (a[e] = a["_old" + e])
  },
  chrome: function (a) {
    return a.stack.replace(/^.*?\n/, "").replace(/^.*?\n/, "").replace(/^.*?\n/, "").replace(/^[^\(]+?[\n$]/gm, "").replace(/^\s+at\s+/gm, "").replace(/^Object.<anonymous>\s*\(/gm, "{anonymous}()@").split("\n")
  },
  firefox: function (a) {
    return a.stack.replace(/^.*?\n/, "").replace(/(?:\n@:0)?\s+$/m, "").replace(/^\(/gm, "{anonymous}(").split("\n")
  },
  opera10: function (a) {
    var a = a.stacktrace.split("\n"),
      e = /.*?line (\d+), column (\d+) in ((<anonymous function\:?\s*(\S+))|([^\(]+)\([^\)]*\))(?: in )?(.*)\s*$/i,
      b, c, d;
    for (b = 2, c = 0, d = a.length; b < d - 2; b++) if (e.test(a[b])) {
      var f = RegExp.$6 + ":" + RegExp.$1 + ":" + RegExp.$2,
        g = RegExp.$3,
        g = g.replace(/<anonymous function\s?(\S+)?>/g, "{anonymous}");
      a[c++] = g + "@" + f
    }
    a.splice(c, a.length - c);
    return a
  },
  opera: function (a) {
    var a = a.message.split("\n"),
      e = /Line\s+(\d+).*?script\s+(http\S+)(?:.*?in\s+function\s+(\S+))?/i,
      b, c, d;
    for (b = 4, c = 0, d = a.length; b < d; b += 2) e.test(a[b]) && (a[c++] = (RegExp.$3 ? RegExp.$3 + "()@" + RegExp.$2 + RegExp.$1 : "{anonymous}()@" + RegExp.$2 + ":" + RegExp.$1) + " -- " + a[b + 1].replace(/^\s+/, ""));
    a.splice(c, a.length - c);
    return a
  },
  other: function (a) {
    for (var e = /function\s*([\w\-$]+)?\s*\(/i, b = [], c = 0, d, f; a && b.length < 10;) d = e.test(a.toString()) ? RegExp.$1 || "{anonymous}" : "{anonymous}", f = Array.prototype.slice.call(a.arguments), b[c++] = d + "(" + this.stringifyArguments(f) + ")", a = a.caller;
    return b
  },
  stringifyArguments: function (a) {
    for (var e = 0; e < a.length; ++e) {
      var b = a[e];
      b === void 0 ? a[e] = "undefined" : b === null ? a[e] = "null" : b.constructor && (b.constructor === Array ? a[e] = b.length < 3 ? "[" + this.stringifyArguments(b) + "]" : "[" + this.stringifyArguments(Array.prototype.slice.call(b, 0, 1)) + "..." + this.stringifyArguments(Array.prototype.slice.call(b, -1)) + "]" : b.constructor === Object ? a[e] = "#object" : b.constructor === Function ? a[e] = "#function" : b.constructor === String && (a[e] = '"' + b + '"'))
    }
    return a.join(",")
  },
  sourceCache: {},
  ajax: function (a) {
    var e = this.createXMLHTTPObject();
    if (e) return e.open("GET", a, false), e.setRequestHeader("User-Agent", "XMLHTTP/1.0"), e.send(""), e.responseText
  },
  createXMLHTTPObject: function () {
    for (var a, e = [function () {
      return new XMLHttpRequest
    }, function () {
      return new ActiveXObject("Msxml2.XMLHTTP")
    }, function () {
      return new ActiveXObject("Msxml3.XMLHTTP")
    }, function () {
      return new ActiveXObject("Microsoft.XMLHTTP")
    }], b = 0; b < e.length; b++) try {
      return a = e[b](), this.createXMLHTTPObject = e[b], a
    } catch (c) {}
  },
  isSameDomain: function (a) {
    return a.indexOf(location.hostname) !== -1
  },
  getSource: function (a) {
    a in this.sourceCache || (this.sourceCache[a] = this.ajax(a).split("\n"));
    return this.sourceCache[a]
  },
  guessFunctions: function (a) {
    for (var e = 0; e < a.length; ++e) {
      var b = a[e],
        c = /{anonymous}\(.*\)@(\w+:\/\/([-\w\.]+)+(:\d+)?[^:]+):(\d+):?(\d+)?/.exec(b);
      if (c) {
        var d = c[1],
          c = c[4];
        d && this.isSameDomain(d) && c && (d = this.guessFunctionName(d, c), a[e] = b.replace("{anonymous}", d))
      }
    }
    return a
  },
  guessFunctionName: function (a, e) {
    try {
      return this.guessFunctionNameFromLines(e, this.getSource(a))
    } catch (b) {
      return "getSource failed with url: " + a + ", exception: " + b.toString()
    }
  },
  guessFunctionNameFromLines: function (a, e) {
    for (var b = /function ([^(]*)\(([^)]*)\)/, c = /['"]?([0-9A-Za-z_]+)['"]?\s*[:=]\s*(function|eval|new Function)/, d = "", f = 0; f < 10; ++f) if (d = e[a - f] + d, d !== void 0) {
      var g = c.exec(d);
      if (g && g[1]) return g[1];
      else if ((g = b.exec(d)) && g[1]) return g[1]
    }
    return "(?)"
  }
};
(function (a) {
  a.ui = a.ui || {};
  a.ui.version || (a.extend(a.ui, {
    version: "1.8.2",
    plugin: {
      add: function (e, b, c) {
        var e = a.ui[e].prototype,
          d;
        for (d in c) e.plugins[d] = e.plugins[d] || [], e.plugins[d].push([b, c[d]])
      },
      call: function (a, b, c) {
        if ((b = a.plugins[b]) && a.element[0].parentNode) for (var d = 0; d < b.length; d++) a.options[b[d][0]] && b[d][1].apply(a.element, c)
      }
    },
    contains: function (a, b) {
      return document.compareDocumentPosition ? a.compareDocumentPosition(b) & 16 : a !== b && a.contains(b)
    },
    hasScroll: function (e, b) {
      if (a(e).css("overflow") == "hidden") return false;
      var b = b && b == "left" ? "scrollLeft" : "scrollTop",
        c = false;
      if (e[b] > 0) return true;
      e[b] = 1;
      c = e[b] > 0;
      e[b] = 0;
      return c
    },
    isOverAxis: function (a, b, c) {
      return a > b && a < b + c
    },
    isOver: function (e, b, c, d, f, g) {
      return a.ui.isOverAxis(e, c, f) && a.ui.isOverAxis(b, d, g)
    },
    keyCode: {
      ALT: 18,
      BACKSPACE: 8,
      CAPS_LOCK: 20,
      COMMA: 188,
      COMMAND: 91,
      COMMAND_LEFT: 91,
      COMMAND_RIGHT: 93,
      CONTROL: 17,
      DELETE: 46,
      DOWN: 40,
      END: 35,
      ENTER: 13,
      ESCAPE: 27,
      HOME: 36,
      INSERT: 45,
      LEFT: 37,
      MENU: 93,
      NUMPAD_ADD: 107,
      NUMPAD_DECIMAL: 110,
      NUMPAD_DIVIDE: 111,
      NUMPAD_ENTER: 108,
      NUMPAD_MULTIPLY: 106,
      NUMPAD_SUBTRACT: 109,
      PAGE_DOWN: 34,
      PAGE_UP: 33,
      PERIOD: 190,
      RIGHT: 39,
      SHIFT: 16,
      SPACE: 32,
      TAB: 9,
      UP: 38,
      WINDOWS: 91
    }
  }), a.fn.extend({
    _focus: a.fn.focus,
    focus: function (e, b) {
      return typeof e === "number" ? this.each(function () {
        var c = this;
        setTimeout(function () {
          a(c).focus();
          b && b.call(c)
        }, e)
      }) : this._focus.apply(this, arguments)
    },
    enableSelection: function () {
      return this.attr("unselectable", "off").css("MozUserSelect", "")
    },
    disableSelection: function () {
      return this.attr("unselectable", "on").css("MozUserSelect", "none")
    },
    scrollParent: function () {
      var e;
      e = a.browser.msie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function () {
        return /(relative|absolute|fixed)/.test(a.curCSS(this, "position", 1)) && /(auto|scroll)/.test(a.curCSS(this, "overflow", 1) + a.curCSS(this, "overflow-y", 1) + a.curCSS(this, "overflow-x", 1))
      }).eq(0) : this.parents().filter(function () {
        return /(auto|scroll)/.test(a.curCSS(this, "overflow", 1) + a.curCSS(this, "overflow-y", 1) + a.curCSS(this, "overflow-x", 1))
      }).eq(0);
      return /fixed/.test(this.css("position")) || !e.length ? a(document) : e
    },
    zIndex: function (e) {
      if (e !== void 0) return this.css("zIndex", e);
      if (this.length) for (var e = a(this[0]), b; e.length && e[0] !== document;) {
        b = e.css("position");
        if (b == "absolute" || b == "relative" || b == "fixed") if (b = parseInt(e.css("zIndex")), !isNaN(b) && b != 0) return b;
        e = e.parent()
      }
      return 0
    }
  }), a.extend(a.expr[":"], {
    data: function (e, b, c) {
      return !!a.data(e, c[3])
    },
    focusable: function (e) {
      var b = e.nodeName.toLowerCase(),
        c = a.attr(e, "tabindex");
      return (/input|select|textarea|button|object/.test(b) ? !e.disabled : "a" == b || "area" == b ? e.href || !isNaN(c) : !isNaN(c)) && !a(e)["area" == b ? "parents" : "closest"](":hidden").length
    },
    tabbable: function (e) {
      var b = a.attr(e, "tabindex");
      return (isNaN(b) || b >= 0) && a(e).is(":focusable")
    }
  }))
})(jQuery);
(function (a) {
  var e = a.fn.remove;
  a.fn.remove = function (b, c) {
    return this.each(function () {
      c || (!b || a.filter(b, [this]).length) && a("*", this).add(this).each(function () {
        a(this).triggerHandler("remove")
      });
      return e.call(a(this), b, c)
    })
  };
  a.widget = function (b, c, d) {
    var f = b.split(".")[0],
      e, b = b.split(".")[1];
    e = f + "-" + b;
    if (!d) d = c, c = a.Widget;
    a.expr[":"][e] = function (d) {
      return !!a.data(d, b)
    };
    a[f] = a[f] || {};
    a[f][b] = function (a, b) {
      arguments.length && this._createWidget(a, b)
    };
    c = new c;
    c.options = a.extend({}, c.options);
    a[f][b].prototype = a.extend(true, c, {
      namespace: f,
      widgetName: b,
      widgetEventPrefix: a[f][b].prototype.widgetEventPrefix || b,
      widgetBaseClass: e
    }, d);
    a.widget.bridge(b, a[f][b])
  };
  a.widget.bridge = function (b, c) {
    a.fn[b] = function (d) {
      var f = typeof d === "string",
        e = Array.prototype.slice.call(arguments, 1),
        h = this,
        d = !f && e.length ? a.extend.apply(null, [true, d].concat(e)) : d;
      if (f && d.substring(0, 1) === "_") return h;
      f ? this.each(function () {
        var c = a.data(this, b),
          f = c && a.isFunction(c[d]) ? c[d].apply(c, e) : c;
        if (f !== c && f !== void 0) return h = f, false
      }) : this.each(function () {
        var f = a.data(this, b);
        f ? (d && f.option(d), f._init()) : a.data(this, b, new c(d, this))
      });
      return h
    }
  };
  a.Widget = function (a, c) {
    arguments.length && this._createWidget(a, c)
  };
  a.Widget.prototype = {
    widgetName: "widget",
    widgetEventPrefix: "",
    options: {
      disabled: false
    },
    _createWidget: function (b, c) {
      this.element = a(c).data(this.widgetName, this);
      this.options = a.extend(true, {}, this.options, a.metadata && a.metadata.get(c)[this.widgetName], b);
      var d = this;
      this.element.bind("remove." + this.widgetName, function () {
        d.destroy()
      });
      this._create();
      this._init()
    },
    _create: function () {},
    _init: function () {},
    destroy: function () {
      this.element.unbind("." + this.widgetName).removeData(this.widgetName);
      this.widget().unbind("." + this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass + "-disabled ui-state-disabled")
    },
    widget: function () {
      return this.element
    },
    option: function (b, c) {
      var d = b,
        f = this;
      if (arguments.length === 0) return a.extend({}, f.options);
      if (typeof b === "string") {
        if (c === void 0) return this.options[b];
        d = {};
        d[b] = c
      }
      a.each(d, function (a, b) {
        f._setOption(a, b)
      });
      return f
    },
    _setOption: function (a, c) {
      this.options[a] = c;
      a === "disabled" && this.widget()[c ? "addClass" : "removeClass"](this.widgetBaseClass + "-disabled ui-state-disabled").attr("aria-disabled", c);
      return this
    },
    enable: function () {
      return this._setOption("disabled", false)
    },
    disable: function () {
      return this._setOption("disabled", true)
    },
    _trigger: function (b, c, d) {
      var f = this.options[b],
        c = a.Event(c);
      c.type = (b === this.widgetEventPrefix ? b : this.widgetEventPrefix + b).toLowerCase();
      d = d || {};
      if (c.originalEvent) for (var b = a.event.props.length, e; b;) e = a.event.props[--b], c[e] = c.originalEvent[e];
      this.element.trigger(c, d);
      return !(a.isFunction(f) && f.call(this.element[0], c, d) === false || c.isDefaultPrevented())
    }
  }
})(jQuery);
(function (a) {
  a.ui = a.ui || {};
  var e = /left|center|right/,
    b = /top|center|bottom/,
    c = a.fn.position,
    d = a.fn.offset;
  a.fn.position = function (d) {
    if (!d || !d.of) return c.apply(this, arguments);
    var d = a.extend({}, d),
      g = a(d.of),
      h = (d.collision || "flip").split(" "),
      j = d.offset ? d.offset.split(" ") : [0, 0],
      l, n, m;
    d.of.nodeType === 9 ? (l = g.width(), n = g.height(), m = {
      top: 0,
      left: 0
    }) : d.of.scrollTo && d.of.document ? (l = g.width(), n = g.height(), m = {
      top: g.scrollTop(),
      left: g.scrollLeft()
    }) : d.of.preventDefault ? (d.at = "left top", l = n = 0, m = {
      top: d.of.pageY,
      left: d.of.pageX
    }) : (l = g.outerWidth(), n = g.outerHeight(), m = g.offset());
    a.each(["my", "at"], function () {
      var a = (d[this] || "").split(" ");
      a.length === 1 && (a = e.test(a[0]) ? a.concat(["center"]) : b.test(a[0]) ? ["center"].concat(a) : ["center", "center"]);
      a[0] = e.test(a[0]) ? a[0] : "center";
      a[1] = b.test(a[1]) ? a[1] : "center";
      d[this] = a
    });
    h.length === 1 && (h[1] = h[0]);
    j[0] = parseInt(j[0], 10) || 0;
    j.length === 1 && (j[1] = j[0]);
    j[1] = parseInt(j[1], 10) || 0;
    d.at[0] === "right" ? m.left += l : d.at[0] === "center" && (m.left += l / 2);
    d.at[1] === "bottom" ? m.top += n : d.at[1] === "center" && (m.top += n / 2);
    m.left += j[0];
    m.top += j[1];
    return this.each(function () {
      var b = a(this),
        c = b.outerWidth(),
        e = b.outerHeight(),
        g = a.extend({}, m);
      d.my[0] === "right" ? g.left -= c : d.my[0] === "center" && (g.left -= c / 2);
      d.my[1] === "bottom" ? g.top -= e : d.my[1] === "center" && (g.top -= e / 2);
      g.left = parseInt(g.left);
      g.top = parseInt(g.top);
      a.each(["left", "top"], function (b, m) {
        a.ui.position[h[b]] && a.ui.position[h[b]][m](g, {
          targetWidth: l,
          targetHeight: n,
          elemWidth: c,
          elemHeight: e,
          offset: j,
          my: d.my,
          at: d.at
        })
      });
      a.fn.bgiframe && b.bgiframe();
      b.offset(a.extend(g, {
        using: d.using
      }))
    })
  };
  a.ui.position = {
    fit: {
      left: function (b, d) {
        var c = a(window),
          d = b.left + d.elemWidth - c.width() - c.scrollLeft();
        b.left = d > 0 ? b.left - d : Math.max(0, b.left)
      },
      top: function (b, d) {
        var c = a(window),
          d = b.top + d.elemHeight - c.height() - c.scrollTop();
        b.top = d > 0 ? b.top - d : Math.max(0, b.top)
      }
    },
    flip: {
      left: function (b, d) {
        if (d.at[0] !== "center") {
          var c = a(window),
            c = b.left + d.elemWidth - c.width() - c.scrollLeft(),
            e = d.my[0] === "left" ? -d.elemWidth : d.my[0] === "right" ? d.elemWidth : 0,
            l = -2 * d.offset[0];
          b.left += b.left < 0 ? e + d.targetWidth + l : c > 0 ? e - d.targetWidth + l : 0
        }
      },
      top: function (b, d) {
        if (d.at[1] !== "center") {
          var c = a(window),
            c = b.top + d.elemHeight - c.height() - c.scrollTop(),
            e = d.my[1] === "top" ? -d.elemHeight : d.my[1] === "bottom" ? d.elemHeight : 0,
            l = d.at[1] === "top" ? d.targetHeight : -d.targetHeight,
            n = -2 * d.offset[1];
          b.top += b.top < 0 ? e + d.targetHeight + n : c > 0 ? e + l + n : 0
        }
      }
    }
  };
  if (!a.offset.setOffset) a.offset.setOffset = function (b, d) {
    if (/static/.test(a.curCSS(b, "position"))) b.style.position = "relative";
    var c = a(b),
      e = c.offset(),
      l = parseInt(a.curCSS(b, "top", true), 10) || 0,
      n = parseInt(a.curCSS(b, "left", true), 10) || 0,
      e = {
        top: d.top - e.top + l,
        left: d.left - e.left + n
      };
    "using" in d ? d.using.call(b, e) : c.css(e)
  }, a.fn.offset = function (b) {
    var c = this[0];
    return !c || !c.ownerDocument ? null : b ? this.each(function () {
      a.offset.setOffset(this, b)
    }) : d.call(this)
  }
})(jQuery);
(function (a) {
  a.widget("ui.autocomplete", {
    options: {
      minLength: 1,
      delay: 300
    },
    _create: function () {
      var e = this,
        b = this.element[0].ownerDocument;
      this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off").attr({
        role: "textbox",
        "aria-autocomplete": "list",
        "aria-haspopup": "true"
      }).bind("keydown.autocomplete", function (b) {
        var d = a.ui.keyCode;
        switch (b.keyCode) {
        case d.PAGE_UP:
          e._move("previousPage", b);
          break;
        case d.PAGE_DOWN:
          e._move("nextPage", b);
          break;
        case d.UP:
          e._move("previous", b);
          b.preventDefault();
          break;
        case d.DOWN:
          e._move("next", b);
          b.preventDefault();
          break;
        case d.ENTER:
        case d.NUMPAD_ENTER:
          e.menu.active && b.preventDefault();
        case d.TAB:
          if (!e.menu.active) break;
          e.menu.select(b);
          break;
        case d.ESCAPE:
          e.element.val(e.term);
          e.close(b);
          break;
        case d.LEFT:
        case d.RIGHT:
        case d.SHIFT:
        case d.CONTROL:
        case d.ALT:
        case d.COMMAND:
        case d.COMMAND_RIGHT:
        case d.INSERT:
        case d.CAPS_LOCK:
        case d.END:
        case d.HOME:
          break;
        default:
          clearTimeout(e.searching), e.searching = setTimeout(function () {
            e.search(null, b)
          }, e.options.delay)
        }
      }).bind("focus.autocomplete", function () {
        e.selectedItem = null;
        e.previous = e.element.val()
      }).bind("blur.autocomplete", function (a) {
        clearTimeout(e.searching);
        e.closing = setTimeout(function () {
          e.close(a);
          e._change(a)
        }, 150)
      });
      this._initSource();
      this.response = function () {
        return e._response.apply(e, arguments)
      };
      this.menu = a("<ul></ul>").addClass("ui-autocomplete").appendTo("body", b).mousedown(function () {
        setTimeout(function () {
          clearTimeout(e.closing)
        }, 13)
      }).menu({
        focus: function (a, b) {
          b = b.item.data("item.autocomplete");
          false !== e._trigger("focus", null, {
            item: b
          }) && /^key/.test(a.originalEvent.type) && e.element.val(b.value)
        },
        selected: function (a, d) {
          d = d.item.data("item.autocomplete");
          false !== e._trigger("select", a, {
            item: d
          }) && e.element.val(d.value);
          e.close(a);
          a = e.previous;
          if (e.element[0] !== b.activeElement) e.element.focus(), e.previous = a;
          e.selectedItem = d
        },
        blur: function () {
          e.menu.element.is(":visible") && e.element.val(e.term)
        }
      }).zIndex(this.element.zIndex() + 1).css({
        top: 0,
        left: 0
      }).hide().data("menu");
      a.fn.bgiframe && this.menu.element.bgiframe()
    },
    destroy: function () {
      this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete").removeAttr("role").removeAttr("aria-autocomplete").removeAttr("aria-haspopup");
      this.menu.element.remove();
      a.Widget.prototype.destroy.call(this)
    },
    _setOption: function (e) {
      a.Widget.prototype._setOption.apply(this, arguments);
      e === "source" && this._initSource()
    },
    _initSource: function () {
      var e, b;
      a.isArray(this.options.source) ? (e = this.options.source, this.source = function (b, d) {
        d(a.ui.autocomplete.filter(e, b.term))
      }) : typeof this.options.source === "string" ? (b = this.options.source, this.source = function (c, d) {
        a.getJSON(b, c, d)
      }) : this.source = this.options.source
    },
    search: function (a, b) {
      a = a != null ? a : this.element.val();
      if (a.length < this.options.minLength) return this.close(b);
      clearTimeout(this.closing);
      if (this._trigger("search") !== false) return this._search(a)
    },
    _search: function (a) {
      this.term = this.element.addClass("ui-autocomplete-loading").val();
      this.source({
        term: a
      }, this.response)
    },
    _response: function (a) {
      a.length ? (a = this._normalize(a), this._suggest(a), this._trigger("open")) : this.close();
      this.element.removeClass("ui-autocomplete-loading")
    },
    close: function (a) {
      clearTimeout(this.closing);
      this.menu.element.is(":visible") && (this._trigger("close", a), this.menu.element.hide(), this.menu.deactivate())
    },
    _change: function (a) {
      this.previous !== this.element.val() && this._trigger("change", a, {
        item: this.selectedItem
      })
    },
    _normalize: function (e) {
      return e.length && e[0].label && e[0].value ? e : a.map(e, function (b) {
        return typeof b === "string" ? {
          label: b,
          value: b
        } : a.extend({
          label: b.label || b.value,
          value: b.value || b.label
        }, b)
      })
    },
    _suggest: function (a) {
      var b = this.menu.element.empty().zIndex(this.element.zIndex() + 1),
        c;
      this._renderMenu(b, a);
      this.menu.deactivate();
      this.menu.refresh();
      this.menu.element.show().position({
        my: "left top",
        at: "left bottom",
        of: this.element,
        collision: "none"
      });
      a = b.width("").width();
      c = this.element.width();
      b.width(Math.max(a, c))
    },
    _renderMenu: function (e, b) {
      var c = this;
      a.each(b, function (a, b) {
        c._renderItem(e, b)
      })
    },
    _renderItem: function (e, b) {
      return a("<li></li>").data("item.autocomplete", b).append("<a>" + b.label + "</a>").appendTo(e)
    },
    _move: function (a, b) {
      if (this.menu.element.is(":visible")) if (this.menu.first() && /^previous/.test(a) || this.menu.last() && /^next/.test(a)) this.element.val(this.term), this.menu.deactivate();
      else this.menu[a](b);
      else this.search(null, b)
    },
    widget: function () {
      return this.menu.element
    }
  });
  a.extend(a.ui.autocomplete, {
    escapeRegex: function (a) {
      return a.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1")
    },
    filter: function (e, b) {
      var c = RegExp(a.ui.autocomplete.escapeRegex(b), "i");
      return a.grep(e, function (a) {
        return c.test(a.label || a.value || a)
      })
    }
  })
})(jQuery);
(function (a) {
  a.widget("ui.menu", {
    _create: function () {
      var e = this;
      this.element.addClass("ui-menu ui-widget ui-widget-content ui-corner-all").attr({
        role: "listbox",
        "aria-activedescendant": "ui-active-menuitem"
      }).click(function (b) {
        a(b.target).closest(".ui-menu-item a").length && (b.preventDefault(), e.select(b))
      });
      this.refresh()
    },
    refresh: function () {
      var e = this;
      this.element.children("li:not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role", "menuitem").children("a").addClass("ui-corner-all").attr("tabindex", -1).mouseenter(function (b) {
        e.activate(b, a(this).parent())
      }).mouseleave(function () {
        e.deactivate()
      })
    },
    activate: function (a, b) {
      this.deactivate();
      if (this.hasScroll()) {
        var c = b.offset().top - this.element.offset().top,
          d = this.element.attr("scrollTop"),
          f = this.element.height();
        c < 0 ? this.element.attr("scrollTop", d + c) : c > f && this.element.attr("scrollTop", d + c - f + b.height())
      }
      this.active = b.eq(0).children("a").addClass("ui-state-hover").attr("id", "ui-active-menuitem").end();
      this._trigger("focus", a, {
        item: b
      })
    },
    deactivate: function () {
      if (this.active) this.active.children("a").removeClass("ui-state-hover").removeAttr("id"), this._trigger("blur"), this.active = null
    },
    next: function (a) {
      this.move("next", ".ui-menu-item:first", a)
    },
    previous: function (a) {
      this.move("prev", ".ui-menu-item:last", a)
    },
    first: function () {
      return this.active && !this.active.prev().length
    },
    last: function () {
      return this.active && !this.active.next().length
    },
    move: function (a, b, c) {
      this.active ? (a = this.active[a + "All"](".ui-menu-item").eq(0), a.length ? this.activate(c, a) : this.activate(c, this.element.children(b))) : this.activate(c, this.element.children(b))
    },
    nextPage: function (e) {
      if (this.hasScroll()) if (!this.active || this.last()) this.activate(e, this.element.children(":first"));
      else {
        var b = this.active.offset().top,
          c = this.element.height(),
          d = this.element.children("li").filter(function () {
            var d = a(this).offset().top - b - c + a(this).height();
            return d < 10 && d > -10
          });
        d.length || (d = this.element.children(":last"));
        this.activate(e, d)
      } else this.activate(e, this.element.children(!this.active || this.last() ? ":first" : ":last"))
    },
    previousPage: function (e) {
      if (this.hasScroll()) if (!this.active || this.first()) this.activate(e, this.element.children(":last"));
      else {
        var b = this.active.offset().top,
          c = this.element.height();
        result = this.element.children("li").filter(function () {
          var d = a(this).offset().top - b + c - a(this).height();
          return d < 10 && d > -10
        });
        result.length || (result = this.element.children(":first"));
        this.activate(e, result)
      } else this.activate(e, this.element.children(!this.active || this.first() ? ":last" : ":first"))
    },
    hasScroll: function () {
      return this.element.height() < this.element.attr("scrollHeight")
    },
    select: function (a) {
      this._trigger("selected", a, {
        item: this.active
      })
    }
  })
})(jQuery);
(function (a) {
  function e() {
    this.debug = false;
    this._curInst = null;
    this._keyEvent = false;
    this._disabledInputs = [];
    this._inDialog = this._datepickerShowing = false;
    this._mainDivId = "ui-datepicker-div";
    this._inlineClass = "ui-datepicker-inline";
    this._appendClass = "ui-datepicker-append";
    this._triggerClass = "ui-datepicker-trigger";
    this._dialogClass = "ui-datepicker-dialog";
    this._disableClass = "ui-datepicker-disabled";
    this._unselectableClass = "ui-datepicker-unselectable";
    this._currentClass = "ui-datepicker-current-day";
    this._dayOverClass = "ui-datepicker-days-cell-over";
    this.regional = [];
    this.regional[""] = {
      closeText: "Done",
      prevText: "Prev",
      nextText: "Next",
      currentText: "Today",
      monthNames: "January,February,March,April,May,June,July,August,September,October,November,December".split(","),
      monthNamesShort: "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),
      dayNames: "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),
      dayNamesShort: "Sun,Mon,Tue,Wed,Thu,Fri,Sat".split(","),
      dayNamesMin: "Su,Mo,Tu,We,Th,Fr,Sa".split(","),
      weekHeader: "Wk",
      dateFormat: "mm/dd/yy",
      firstDay: 0,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ""
    };
    this._defaults = {
      showOn: "focus",
      showAnim: "show",
      showOptions: {},
      defaultDate: null,
      appendText: "",
      buttonText: "...",
      buttonImage: "",
      buttonImageOnly: false,
      hideIfNoPrevNext: false,
      navigationAsDateFormat: false,
      gotoCurrent: false,
      changeMonth: false,
      changeYear: false,
      yearRange: "c-10:c+10",
      showOtherMonths: false,
      selectOtherMonths: false,
      showWeek: false,
      calculateWeek: this.iso8601Week,
      shortYearCutoff: "+10",
      minDate: null,
      maxDate: null,
      duration: "_default",
      beforeShowDay: null,
      beforeShow: null,
      onSelect: null,
      onChangeMonthYear: null,
      onClose: null,
      numberOfMonths: 1,
      showCurrentAtPos: 0,
      stepMonths: 1,
      stepBigMonths: 12,
      altField: "",
      altFormat: "",
      constrainInput: true,
      showButtonPanel: false,
      autoSize: false
    };
    a.extend(this._defaults, this.regional[""]);
    this.dpDiv = a('<div id="' + this._mainDivId + '" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ui-helper-hidden-accessible"></div>')
  }
  function b(b, c) {
    a.extend(b, c);
    for (var e in c) if (c[e] == null || c[e] == void 0) b[e] = c[e];
    return b
  }
  a.extend(a.ui, {
    datepicker: {
      version: "1.8.1"
    }
  });
  var c = (new Date).getTime();
  a.extend(e.prototype, {
    markerClassName: "hasDatepicker",
    log: function () {
      this.debug && console.log.apply("", arguments)
    },
    _widgetDatepicker: function () {
      return this.dpDiv
    },
    setDefaults: function (a) {
      b(this._defaults, a || {});
      return this
    },
    _attachDatepicker: function (b, c) {
      var e = null,
        h;
      for (h in this._defaults) {
        var j = b.getAttribute("date:" + h);
        if (j) {
          e = e || {};
          try {
            e[h] = eval(j)
          } catch (l) {
            e[h] = j
          }
        }
      }
      h = b.nodeName.toLowerCase();
      j = h == "div" || h == "span";
      if (!b.id) b.id = "dp" + ++this.uuid;
      var n = this._newInst(a(b), j);
      n.settings = a.extend({}, c || {}, e || {});
      h == "input" ? this._connectDatepicker(b, n) : j && this._inlineDatepicker(b, n)
    },
    _newInst: function (b, c) {
      return {
        id: b[0].id.replace(/([^A-Za-z0-9_])/g, "\\\\$1"),
        input: b,
        selectedDay: 0,
        selectedMonth: 0,
        selectedYear: 0,
        drawMonth: 0,
        drawYear: 0,
        inline: c,
        dpDiv: !c ? this.dpDiv : a('<div class="' + this._inlineClass + ' ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>')
      }
    },
    _connectDatepicker: function (b, c) {
      var e = a(b);
      c.append = a([]);
      c.trigger = a([]);
      e.hasClass(this.markerClassName) || (this._attachments(e, c), e.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp).bind("setData.datepicker", function (a, b, d) {
        c.settings[b] = d
      }).bind("getData.datepicker", function (a, b) {
        return this._get(c, b)
      }), this._autoSize(c), a.data(b, "datepicker", c))
    },
    _attachments: function (b, c) {
      var e = this._get(c, "appendText"),
        h = this._get(c, "isRTL");
      c.append && c.append.remove();
      if (e) c.append = a('<span class="' + this._appendClass + '">' + e + "</span>"), b[h ? "before" : "after"](c.append);
      b.unbind("focus", this._showDatepicker);
      c.trigger && c.trigger.remove();
      e = this._get(c, "showOn");
      (e == "focus" || e == "both") && b.focus(this._showDatepicker);
      if (e == "button" || e == "both") {
        var e = this._get(c, "buttonText"),
          j = this._get(c, "buttonImage");
        c.trigger = a(this._get(c, "buttonImageOnly") ? a("<img/>").addClass(this._triggerClass).attr({
          src: j,
          alt: e,
          title: e
        }) : a('<button type="button"></button>').addClass(this._triggerClass).html(j == "" ? e : a("<img/>").attr({
          src: j,
          alt: e,
          title: e
        })));
        b[h ? "before" : "after"](c.trigger);
        c.trigger.click(function () {
          a.datepicker._datepickerShowing && a.datepicker._lastInput == b[0] ? a.datepicker._hideDatepicker() : a.datepicker._showDatepicker(b[0]);
          return false
        })
      }
    },
    _autoSize: function (a) {
      if (this._get(a, "autoSize") && !a.inline) {
        var b = new Date(2009, 11, 20),
          c = this._get(a, "dateFormat");
        if (c.match(/[DM]/)) {
          var e = function (a) {
              for (var b = 0, d = 0, c = 0; c < a.length; c++) if (a[c].length > b) b = a[c].length, d = c;
              return d
            };
          b.setMonth(e(this._get(a, c.match(/MM/) ? "monthNames" : "monthNamesShort")));
          b.setDate(e(this._get(a, c.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - b.getDay())
        }
        a.input.attr("size", this._formatDate(a, b).length)
      }
    },
    _inlineDatepicker: function (b, c) {
      var e = a(b);
      e.hasClass(this.markerClassName) || (e.addClass(this.markerClassName).append(c.dpDiv).bind("setData.datepicker", function (a, b, d) {
        c.settings[b] = d
      }).bind("getData.datepicker", function (a, b) {
        return this._get(c, b)
      }), a.data(b, "datepicker", c), this._setDate(c, this._getDefaultDate(c), true), this._updateDatepicker(c), this._updateAlternate(c))
    },
    _dialogDatepicker: function (d, c, e, h, j) {
      d = this._dialogInst;
      if (!d) d = "dp" + ++this.uuid, this._dialogInput = a('<input type="text" id="' + d + '" style="position: absolute; top: -100px; width: 0px; z-index: -10;"/>'), this._dialogInput.keydown(this._doKeyDown), a("body").append(this._dialogInput), d = this._dialogInst = this._newInst(this._dialogInput, false), d.settings = {}, a.data(this._dialogInput[0], "datepicker", d);
      b(d.settings, h || {});
      c = c && c.constructor == Date ? this._formatDate(d, c) : c;
      this._dialogInput.val(c);
      this._pos = j ? j.length ? j : [j.pageX, j.pageY] : null;
      if (!this._pos) this._pos = [document.documentElement.clientWidth / 2 - 100 + (document.documentElement.scrollLeft || document.body.scrollLeft), document.documentElement.clientHeight / 2 - 150 + (document.documentElement.scrollTop || document.body.scrollTop)];
      this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px");
      d.settings.onSelect = e;
      this._inDialog = true;
      this.dpDiv.addClass(this._dialogClass);
      this._showDatepicker(this._dialogInput[0]);
      a.blockUI && a.blockUI(this.dpDiv);
      a.data(this._dialogInput[0], "datepicker", d);
      return this
    },
    _destroyDatepicker: function (b) {
      var c = a(b),
        e = a.data(b, "datepicker");
      if (c.hasClass(this.markerClassName)) {
        var h = b.nodeName.toLowerCase();
        a.removeData(b, "datepicker");
        h == "input" ? (e.append.remove(), e.trigger.remove(), c.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)) : (h == "div" || h == "span") && c.removeClass(this.markerClassName).empty()
      }
    },
    _enableDatepicker: function (b) {
      var c = a(b),
        e = a.data(b, "datepicker");
      if (c.hasClass(this.markerClassName)) {
        var h = b.nodeName.toLowerCase();
        h == "input" ? (b.disabled = false, e.trigger.filter("button").each(function () {
          this.disabled = false
        }).end().filter("img").css({
          opacity: "1.0",
          cursor: ""
        })) : (h == "div" || h == "span") && c.children("." + this._inlineClass).children().removeClass("ui-state-disabled");
        this._disabledInputs = a.map(this._disabledInputs, function (a) {
          return a == b ? null : a
        })
      }
    },
    _disableDatepicker: function (b) {
      var c = a(b),
        e = a.data(b, "datepicker");
      if (c.hasClass(this.markerClassName)) {
        var h = b.nodeName.toLowerCase();
        h == "input" ? (b.disabled = true, e.trigger.filter("button").each(function () {
          this.disabled = true
        }).end().filter("img").css({
          opacity: "0.5",
          cursor: "default"
        })) : (h == "div" || h == "span") && c.children("." + this._inlineClass).children().addClass("ui-state-disabled");
        this._disabledInputs = a.map(this._disabledInputs, function (a) {
          return a == b ? null : a
        });
        this._disabledInputs[this._disabledInputs.length] = b
      }
    },
    _isDisabledDatepicker: function (a) {
      if (!a) return false;
      for (var b = 0; b < this._disabledInputs.length; b++) if (this._disabledInputs[b] == a) return true;
      return false
    },
    _getInst: function (b) {
      try {
        return a.data(b, "datepicker")
      } catch (c) {
        throw "Missing instance data for this datepicker";
      }
    },
    _optionDatepicker: function (d, c, e) {
      var h = this._getInst(d);
      if (arguments.length == 2 && typeof c == "string") return c == "defaults" ? a.extend({}, a.datepicker._defaults) : h ? c == "all" ? a.extend({}, h.settings) : this._get(h, c) : null;
      var j = c || {};
      typeof c == "string" && (j = {}, j[c] = e);
      if (h) {
        this._curInst == h && this._hideDatepicker();
        var l = this._getDateDatepicker(d, true);
        b(h.settings, j);
        this._attachments(a(d), h);
        this._autoSize(h);
        this._setDateDatepicker(d, l);
        this._updateDatepicker(h)
      }
    },
    _changeDatepicker: function (a, b, c) {
      this._optionDatepicker(a, b, c)
    },
    _refreshDatepicker: function (a) {
      (a = this._getInst(a)) && this._updateDatepicker(a)
    },
    _setDateDatepicker: function (a, b) {
      if (a = this._getInst(a)) this._setDate(a, b), this._updateDatepicker(a), this._updateAlternate(a)
    },
    _getDateDatepicker: function (a, b) {
      (a = this._getInst(a)) && !a.inline && this._setDateFromField(a, b);
      return a ? this._getDate(a) : null
    },
    _doKeyDown: function (b) {
      var c = a.datepicker._getInst(b.target),
        e = true,
        h = c.dpDiv.is(".ui-datepicker-rtl");
      c._keyEvent = true;
      if (a.datepicker._datepickerShowing) switch (b.keyCode) {
      case 9:
        a.datepicker._hideDatepicker();
        e = false;
        break;
      case 13:
        return e = a("td." + a.datepicker._dayOverClass, c.dpDiv).add(a("td." + a.datepicker._currentClass, c.dpDiv)), e[0] ? a.datepicker._selectDay(b.target, c.selectedMonth, c.selectedYear, e[0]) : a.datepicker._hideDatepicker(), false;
      case 27:
        a.datepicker._hideDatepicker();
        break;
      case 33:
        a.datepicker._adjustDate(b.target, b.ctrlKey ? -a.datepicker._get(c, "stepBigMonths") : -a.datepicker._get(c, "stepMonths"), "M");
        break;
      case 34:
        a.datepicker._adjustDate(b.target, b.ctrlKey ? +a.datepicker._get(c, "stepBigMonths") : +a.datepicker._get(c, "stepMonths"), "M");
        break;
      case 35:
        (b.ctrlKey || b.metaKey) && a.datepicker._clearDate(b.target);
        e = b.ctrlKey || b.metaKey;
        break;
      case 36:
        (b.ctrlKey || b.metaKey) && a.datepicker._gotoToday(b.target);
        e = b.ctrlKey || b.metaKey;
        break;
      case 37:
        if (b.ctrlKey || b.metaKey) a.datepicker._adjustDate(b.target, h ? 1 : -1, "D");
        e = b.ctrlKey || b.metaKey;
        b.originalEvent.altKey && a.datepicker._adjustDate(b.target, b.ctrlKey ? -a.datepicker._get(c, "stepBigMonths") : -a.datepicker._get(c, "stepMonths"), "M");
        break;
      case 38:
        (b.ctrlKey || b.metaKey) && a.datepicker._adjustDate(b.target, -7, "D");
        e = b.ctrlKey || b.metaKey;
        break;
      case 39:
        if (b.ctrlKey || b.metaKey) a.datepicker._adjustDate(b.target, h ? -1 : 1, "D");
        e = b.ctrlKey || b.metaKey;
        b.originalEvent.altKey && a.datepicker._adjustDate(b.target, b.ctrlKey ? +a.datepicker._get(c, "stepBigMonths") : +a.datepicker._get(c, "stepMonths"), "M");
        break;
      case 40:
        (b.ctrlKey || b.metaKey) && a.datepicker._adjustDate(b.target, 7, "D");
        e = b.ctrlKey || b.metaKey;
        break;
      default:
        e = false
      } else b.keyCode == 36 && b.ctrlKey ? a.datepicker._showDatepicker(this) : e = false;
      e && (b.preventDefault(), b.stopPropagation())
    },
    _doKeyPress: function (b) {
      var c = a.datepicker._getInst(b.target);
      if (a.datepicker._get(c, "constrainInput")) {
        var c = a.datepicker._possibleChars(a.datepicker._get(c, "dateFormat")),
          e = String.fromCharCode(b.charCode == void 0 ? b.keyCode : b.charCode);
        return b.ctrlKey || e < " " || !c || c.indexOf(e) > -1
      }
    },
    _doKeyUp: function (b) {
      b = a.datepicker._getInst(b.target);
      if (b.input.val() != b.lastVal) try {
        if (a.datepicker.parseDate(a.datepicker._get(b, "dateFormat"), b.input ? b.input.val() : null, a.datepicker._getFormatConfig(b))) a.datepicker._setDateFromField(b), a.datepicker._updateAlternate(b), a.datepicker._updateDatepicker(b)
      } catch (c) {
        a.datepicker.log(c)
      }
      return true
    },
    _showDatepicker: function (d) {
      d = d.target || d;
      d.nodeName.toLowerCase() != "input" && (d = a("input", d.parentNode)[0]);
      if (!(a.datepicker._isDisabledDatepicker(d) || a.datepicker._lastInput == d)) {
        var c = a.datepicker._getInst(d);
        a.datepicker._curInst && a.datepicker._curInst != c && a.datepicker._curInst.dpDiv.stop(true, true);
        var e = a.datepicker._get(c, "beforeShow");
        b(c.settings, e ? e.apply(d, [d, c]) : {});
        c.lastVal = null;
        a.datepicker._lastInput = d;
        a.datepicker._setDateFromField(c);
        if (a.datepicker._inDialog) d.value = "";
        if (!a.datepicker._pos) a.datepicker._pos = a.datepicker._findPos(d), a.datepicker._pos[1] += d.offsetHeight;
        var h = false;
        a(d).parents().each(function () {
          h |= a(this).css("position") == "fixed";
          return !h
        });
        h && a.browser.opera && (a.datepicker._pos[0] -= document.documentElement.scrollLeft, a.datepicker._pos[1] -= document.documentElement.scrollTop);
        e = {
          left: a.datepicker._pos[0],
          top: a.datepicker._pos[1]
        };
        a.datepicker._pos = null;
        c.dpDiv.css({
          position: "absolute",
          display: "block",
          top: "-1000px"
        });
        a.datepicker._updateDatepicker(c);
        e = a.datepicker._checkOffset(c, e, h);
        c.dpDiv.css({
          position: a.datepicker._inDialog && a.blockUI ? "static" : h ? "fixed" : "absolute",
          display: "none",
          left: e.left + "px",
          top: e.top + "px"
        });
        if (!c.inline) {
          var e = a.datepicker._get(c, "showAnim"),
            j = a.datepicker._get(c, "duration"),
            l = function () {
              a.datepicker._datepickerShowing = true;
              var b = a.datepicker._getBorders(c.dpDiv);
              c.dpDiv.find("iframe.ui-datepicker-cover").css({
                left: -b[0],
                top: -b[1],
                width: c.dpDiv.outerWidth(),
                height: c.dpDiv.outerHeight()
              })
            };
          c.dpDiv.zIndex(a(d).zIndex() + 1);
          a.effects && a.effects[e] ? c.dpDiv.show(e, a.datepicker._get(c, "showOptions"), j, l) : c.dpDiv[e || "show"](e ? j : null, l);
          (!e || !j) && l();
          c.input.is(":visible") && !c.input.is(":disabled") && c.input.focus();
          a.datepicker._curInst = c
        }
      }
    },
    _updateDatepicker: function (b) {
      var c = this,
        e = a.datepicker._getBorders(b.dpDiv);
      b.dpDiv.empty().append(this._generateHTML(b)).find("iframe.ui-datepicker-cover").css({
        left: -e[0],
        top: -e[1],
        width: b.dpDiv.outerWidth(),
        height: b.dpDiv.outerHeight()
      }).end().find("button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a").bind("mouseout", function () {
        a(this).removeClass("ui-state-hover");
        this.className.indexOf("ui-datepicker-prev") != -1 && a(this).removeClass("ui-datepicker-prev-hover");
        this.className.indexOf("ui-datepicker-next") != -1 && a(this).removeClass("ui-datepicker-next-hover")
      }).bind("mouseover", function () {
        if (!c._isDisabledDatepicker(b.inline ? b.dpDiv.parent()[0] : b.input[0])) a(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), a(this).addClass("ui-state-hover"), this.className.indexOf("ui-datepicker-prev") != -1 && a(this).addClass("ui-datepicker-prev-hover"), this.className.indexOf("ui-datepicker-next") != -1 && a(this).addClass("ui-datepicker-next-hover")
      }).end().find("." + this._dayOverClass + " a").trigger("mouseover").end();
      var e = this._getNumberOfMonths(b),
        h = e[1];
      h > 1 ? b.dpDiv.addClass("ui-datepicker-multi-" + h).css("width", 17 * h + "em") : b.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width("");
      b.dpDiv[(e[0] != 1 || e[1] != 1 ? "add" : "remove") + "Class"]("ui-datepicker-multi");
      b.dpDiv[(this._get(b, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl");
      b == a.datepicker._curInst && a.datepicker._datepickerShowing && b.input && b.input.is(":visible") && !b.input.is(":disabled") && b.input.focus()
    },
    _getBorders: function (a) {
      var b = function (a) {
          return {
            thin: 1,
            medium: 2,
            thick: 3
          }[a] || a
        };
      return [parseFloat(b(a.css("border-left-width"))), parseFloat(b(a.css("border-top-width")))]
    },
    _checkOffset: function (b, c, e) {
      var h = b.dpDiv.outerWidth(),
        j = b.dpDiv.outerHeight(),
        l = b.input ? b.input.outerWidth() : 0,
        n = b.input ? b.input.outerHeight() : 0,
        m = document.documentElement.clientWidth + a(document).scrollLeft(),
        p = document.documentElement.clientHeight + a(document).scrollTop();
      c.left -= this._get(b, "isRTL") ? h - l : 0;
      c.left -= e && c.left == b.input.offset().left ? a(document).scrollLeft() : 0;
      c.top -= e && c.top == b.input.offset().top + n ? a(document).scrollTop() : 0;
      c.left -= Math.min(c.left, c.left + h > m && m > h ? Math.abs(c.left + h - m) : 0);
      c.top -= Math.min(c.top, c.top + j > p && p > j ? Math.abs(j + n) : 0);
      return c
    },
    _findPos: function (b) {
      for (var c = this._get(this._getInst(b), "isRTL"); b && (b.type == "hidden" || b.nodeType != 1);) b = b[c ? "previousSibling" : "nextSibling"];
      b = a(b).offset();
      return [b.left, b.top]
    },
    _hideDatepicker: function (b) {
      var c = this._curInst;
      if (c && !(b && c != a.data(b, "datepicker")) && this._datepickerShowing) {
        var b = this._get(c, "showAnim"),
          e = this._get(c, "duration"),
          h = function () {
            a.datepicker._tidyDialog(c);
            this._curInst = null
          };
        a.effects && a.effects[b] ? c.dpDiv.hide(b, a.datepicker._get(c, "showOptions"), e, h) : c.dpDiv[b == "slideDown" ? "slideUp" : b == "fadeIn" ? "fadeOut" : "hide"](b ? e : null, h);
        b || h();
        if (b = this._get(c, "onClose")) b.apply(c.input ? c.input[0] : null, [c.input ? c.input.val() : "", c]);
        this._datepickerShowing = false;
        this._lastInput = null;
        this._inDialog && (this._dialogInput.css({
          position: "absolute",
          left: "0",
          top: "-100px"
        }), a.blockUI && (a.unblockUI(), a("body").append(this.dpDiv)));
        this._inDialog = false
      }
    },
    _tidyDialog: function (a) {
      a.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")
    },
    _checkExternalClick: function (b) {
      a.datepicker._curInst && (b = a(b.target), b[0].id != a.datepicker._mainDivId && b.parents("#" + a.datepicker._mainDivId).length == 0 && !b.hasClass(a.datepicker.markerClassName) && !b.hasClass(a.datepicker._triggerClass) && a.datepicker._datepickerShowing && (!a.datepicker._inDialog || !a.blockUI) && a.datepicker._hideDatepicker())
    },
    _adjustDate: function (b, c, e) {
      var b = a(b),
        h = this._getInst(b[0]);
      this._isDisabledDatepicker(b[0]) || (this._adjustInstDate(h, c + (e == "M" ? this._get(h, "showCurrentAtPos") : 0), e), this._updateDatepicker(h))
    },
    _gotoToday: function (b) {
      var b = a(b),
        c = this._getInst(b[0]);
      if (this._get(c, "gotoCurrent") && c.currentDay) c.selectedDay = c.currentDay, c.drawMonth = c.selectedMonth = c.currentMonth, c.drawYear = c.selectedYear = c.currentYear;
      else {
        var e = new Date;
        c.selectedDay = e.getDate();
        c.drawMonth = c.selectedMonth = e.getMonth();
        c.drawYear = c.selectedYear = e.getFullYear()
      }
      this._notifyChange(c);
      this._adjustDate(b)
    },
    _selectMonthYear: function (b, c, e) {
      var b = a(b),
        h = this._getInst(b[0]);
      h._selectingMonthYear = false;
      h["selected" + (e == "M" ? "Month" : "Year")] = h["draw" + (e == "M" ? "Month" : "Year")] = parseInt(c.options[c.selectedIndex].value, 10);
      this._notifyChange(h);
      this._adjustDate(b)
    },
    _clickMonthYear: function (b) {
      b = this._getInst(a(b)[0]);
      b.input && b._selectingMonthYear && !a.browser.msie && b.input.focus();
      b._selectingMonthYear = !b._selectingMonthYear
    },
    _selectDay: function (b, c, e, h) {
      var j = a(b);
      if (!a(h).hasClass(this._unselectableClass) && !this._isDisabledDatepicker(j[0])) j = this._getInst(j[0]), j.selectedDay = j.currentDay = a("a", h).html(), j.selectedMonth = j.currentMonth = c, j.selectedYear = j.currentYear = e, this._selectDate(b, this._formatDate(j, j.currentDay, j.currentMonth, j.currentYear))
    },
    _clearDate: function (b) {
      b = a(b);
      this._getInst(b[0]);
      this._selectDate(b, "")
    },
    _selectDate: function (b, c) {
      b = this._getInst(a(b)[0]);
      c = c != null ? c : this._formatDate(b);
      b.input && b.input.val(c);
      this._updateAlternate(b);
      var e = this._get(b, "onSelect");
      e ? e.apply(b.input ? b.input[0] : null, [c, b]) : b.input && b.input.trigger("change");
      b.inline ? this._updateDatepicker(b) : (this._hideDatepicker(), this._lastInput = b.input[0], typeof b.input[0] != "object" && b.input.focus(), this._lastInput = null)
    },
    _updateAlternate: function (b) {
      var c = this._get(b, "altField");
      if (c) {
        var e = this._get(b, "altFormat") || this._get(b, "dateFormat"),
          h = this._getDate(b),
          j = this.formatDate(e, h, this._getFormatConfig(b));
        a(c).each(function () {
          a(this).val(j)
        })
      }
    },
    noWeekends: function (a) {
      a = a.getDay();
      return [a > 0 && a < 6, ""]
    },
    iso8601Week: function (a) {
      a = new Date(a.getTime());
      a.setDate(a.getDate() + 4 - (a.getDay() || 7));
      var b = a.getTime();
      a.setMonth(0);
      a.setDate(1);
      return Math.floor(Math.round((b - a) / 864E5) / 7) + 1
    },
    parseDate: function (a, b, c) {
      if (a == null || b == null) throw "Invalid arguments";
      b = typeof b == "object" ? b.toString() : b + "";
      if (b == "") return null;
      for (var e = (c ? c.shortYearCutoff : null) || this._defaults.shortYearCutoff, j = (c ? c.dayNamesShort : null) || this._defaults.dayNamesShort, l = (c ? c.dayNames : null) || this._defaults.dayNames, n = (c ? c.monthNamesShort : null) || this._defaults.monthNamesShort, m = (c ? c.monthNames : null) || this._defaults.monthNames, p = c = -1, o = -1, u = -1, w = false, s = function (b) {
          (b = q + 1 < a.length && a.charAt(q + 1) == b) && q++;
          return b
        }, D = function (a) {
          s(a);
          a = b.substring(F).match(RegExp("^\\d{1," + (a == "@" ? 14 : a == "!" ? 20 : a == "y" ? 4 : a == "o" ? 3 : 2) + "}"));
          if (!a) throw "Missing number at position " + F;
          F += a[0].length;
          return parseInt(a[0], 10)
        }, C = function (a, c, d) {
          a = s(a) ? d : c;
          for (c = 0; c < a.length; c++) if (b.substr(F, a[c].length) == a[c]) return F += a[c].length, c + 1;
          throw "Unknown name at position " + F;
        }, z = function () {
          if (b.charAt(F) != a.charAt(q)) throw "Unexpected literal at position " + F;
          F++
        }, F = 0, q = 0; q < a.length; q++) if (w) a.charAt(q) == "'" && !s("'") ? w = false : z();
      else switch (a.charAt(q)) {
      case "d":
        o = D("d");
        break;
      case "D":
        C("D", j, l);
        break;
      case "o":
        u = D("o");
        break;
      case "m":
        p = D("m");
        break;
      case "M":
        p = C("M", n, m);
        break;
      case "y":
      case "Y":
        c = D("y");
        break;
      case "@":
        var K = new Date(D("@")),
          c = K.getFullYear(),
          p = K.getMonth() + 1,
          o = K.getDate();
        break;
      case "!":
        K = new Date((D("!") - this._ticksTo1970) / 1E4);
        c = K.getFullYear();
        p = K.getMonth() + 1;
        o = K.getDate();
        break;
      case "'":
        s("'") ? z() : w = true;
        break;
      default:
        z()
      }
      c == -1 ? c = (new Date).getFullYear() : c < 100 && (c += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (c <= e ? 0 : -100));
      if (u > -1) {
        p = 1;
        o = u;
        do {
          e = this._getDaysInMonth(c, p - 1);
          if (o <= e) break;
          p++;
          o -= e
        } while (1)
      }
      K = this._daylightSavingAdjust(new Date(c, p - 1, o));
      if (K.getFullYear() != c || K.getMonth() + 1 != p || K.getDate() != o) throw "Invalid date";
      return K
    },
    ATOM: "yy-mm-dd",
    COOKIE: "D, dd M yy",
    ISO_8601: "yy-mm-dd",
    RFC_822: "D, d M y",
    RFC_850: "DD, dd-M-y",
    RFC_1036: "D, d M y",
    RFC_1123: "D, d M yy",
    RFC_2822: "D, d M yy",
    RSS: "D, d M y",
    TICKS: "!",
    TIMESTAMP: "@",
    W3C: "yy-mm-dd",
    _ticksTo1970: (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)) * 864E9,
    formatDate: function (a, b, c) {
      if (!b) return "";
      var e = (c ? c.dayNamesShort : null) || this._defaults.dayNamesShort,
        j = (c ? c.dayNames : null) || this._defaults.dayNames,
        l = (c ? c.monthNamesShort : null) || this._defaults.monthNamesShort,
        c = (c ? c.monthNames : null) || this._defaults.monthNames,
        n = function (b) {
          (b = w + 1 < a.length && a.charAt(w + 1) == b) && w++;
          return b
        },
        m = function (a, b, c) {
          b = "" + b;
          if (n(a)) for (; b.length < c;) b = "0" + b;
          return b
        },
        p = function (a, b, c, d) {
          return n(a) ? d[b] : c[b]
        },
        o = "",
        u = false;
      if (b) for (var w = 0; w < a.length; w++) if (u) a.charAt(w) == "'" && !n("'") ? u = false : o += a.charAt(w);
      else switch (a.charAt(w)) {
      case "d":
        o += m("d", b.getDate(), 2);
        break;
      case "D":
        o += p("D", b.getDay(), e, j);
        break;
      case "o":
        o += m("o", (b.getTime() - (new Date(b.getFullYear(), 0, 0)).getTime()) / 864E5, 3);
        break;
      case "m":
        o += m("m", b.getMonth() + 1, 2);
        break;
      case "M":
        o += p("M", b.getMonth(), l, c);
        break;
      case "y":
        o += n("y") ? b.getFullYear() : (b.getYear() % 100 < 10 ? "0" : "") + b.getYear() % 100;
        break;
      case "Y":
        o += b.getFullYear();
        break;
      case "@":
        o += b.getTime();
        break;
      case "!":
        o += b.getTime() * 1E4 + this._ticksTo1970;
        break;
      case "'":
        n("'") ? o += "'" : u = true;
        break;
      default:
        o += a.charAt(w)
      }
      return o
    },
    _possibleChars: function (a) {
      for (var b = "", c = false, e = function (b) {
          (b = j + 1 < a.length && a.charAt(j + 1) == b) && j++;
          return b
        }, j = 0; j < a.length; j++) if (c) a.charAt(j) == "'" && !e("'") ? c = false : b += a.charAt(j);
      else switch (a.charAt(j)) {
      case "d":
      case "m":
      case "y":
      case "@":
        b += "0123456789";
        break;
      case "D":
      case "M":
        return null;
      case "'":
        e("'") ? b += "'" : c = true;
        break;
      default:
        b += a.charAt(j)
      }
      return b
    },
    _get: function (a, b) {
      return a.settings[b] !== void 0 ? a.settings[b] : this._defaults[b]
    },
    _setDateFromField: function (a, b) {
      if (a.input.val() != a.lastVal) {
        var c = this._get(a, "dateFormat"),
          e = a.lastVal = a.input ? a.input.val() : null,
          j, l;
        j = l = this._getDefaultDate(a);
        var n = this._getFormatConfig(a);
        try {
          j = this.parseDate(c, e, n) || l
        } catch (m) {
          this.log(m), e = b ? "" : e
        }
        a.selectedDay = j.getDate();
        a.drawMonth = a.selectedMonth = j.getMonth();
        a.drawYear = a.selectedYear = j.getFullYear();
        a.currentDay = e ? j.getDate() : 0;
        a.currentMonth = e ? j.getMonth() : 0;
        a.currentYear = e ? j.getFullYear() : 0;
        this._adjustInstDate(a)
      }
    },
    _getDefaultDate: function (a) {
      return this._restrictMinMax(a, this._determineDate(a, this._get(a, "defaultDate"), new Date))
    },
    _determineDate: function (b, c, e) {
      var h = function (a) {
          var b = new Date;
          b.setDate(b.getDate() + a);
          return b
        };
      if (c = (c = c == null ? e : typeof c == "string" ?
      function (c) {
        try {
          return a.datepicker.parseDate(a.datepicker._get(b, "dateFormat"), c, a.datepicker._getFormatConfig(b))
        } catch (e) {}
        for (var f = (c.toLowerCase().match(/^c/) ? a.datepicker._getDate(b) : null) || new Date, g = f.getFullYear(), h = f.getMonth(), f = f.getDate(), o = /([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, u = o.exec(c); u;) {
          switch (u[2] || "d") {
          case "d":
          case "D":
            f += parseInt(u[1], 10);
            break;
          case "w":
          case "W":
            f += parseInt(u[1], 10) * 7;
            break;
          case "m":
          case "M":
            h += parseInt(u[1], 10);
            f = Math.min(f, a.datepicker._getDaysInMonth(g, h));
            break;
          case "y":
          case "Y":
            g += parseInt(u[1], 10), f = Math.min(f, a.datepicker._getDaysInMonth(g, h))
          }
          u = o.exec(c)
        }
        return new Date(g, h, f)
      }(c) : typeof c == "number" ? isNaN(c) ? e : h(c) : c) && c.toString() == "Invalid Date" ? e : c) c.setHours(0), c.setMinutes(0), c.setSeconds(0), c.setMilliseconds(0);
      return this._daylightSavingAdjust(c)
    },
    _daylightSavingAdjust: function (a) {
      if (!a) return null;
      a.setHours(a.getHours() > 12 ? a.getHours() + 2 : 0);
      return a
    },
    _setDate: function (a, b, c) {
      var e = !b,
        j = a.selectedMonth,
        l = a.selectedYear,
        b = this._restrictMinMax(a, this._determineDate(a, b, new Date));
      a.selectedDay = a.currentDay = b.getDate();
      a.drawMonth = a.selectedMonth = a.currentMonth = b.getMonth();
      a.drawYear = a.selectedYear = a.currentYear = b.getFullYear();
      (j != a.selectedMonth || l != a.selectedYear) && !c && this._notifyChange(a);
      this._adjustInstDate(a);
      a.input && a.input.val(e ? "" : this._formatDate(a))
    },
    _getDate: function (a) {
      return !a.currentYear || a.input && a.input.val() == "" ? null : this._daylightSavingAdjust(new Date(a.currentYear, a.currentMonth, a.currentDay))
    },
    _generateHTML: function (b) {
      var e = new Date,
        e = this._daylightSavingAdjust(new Date(e.getFullYear(), e.getMonth(), e.getDate())),
        g = this._get(b, "isRTL"),
        h = this._get(b, "showButtonPanel"),
        j = this._get(b, "hideIfNoPrevNext"),
        l = this._get(b, "navigationAsDateFormat"),
        n = this._getNumberOfMonths(b),
        m = this._get(b, "showCurrentAtPos"),
        p = this._get(b, "stepMonths"),
        o = n[0] != 1 || n[1] != 1,
        u = this._daylightSavingAdjust(!b.currentDay ? new Date(9999, 9, 9) : new Date(b.currentYear, b.currentMonth, b.currentDay)),
        w = this._getMinMaxDate(b, "min"),
        s = this._getMinMaxDate(b, "max"),
        m = b.drawMonth - m,
        D = b.drawYear;
      m < 0 && (m += 12, D--);
      if (s) for (var C = this._daylightSavingAdjust(new Date(s.getFullYear(), s.getMonth() - n[0] * n[1] + 1, s.getDate())), C = w && C < w ? w : C; this._daylightSavingAdjust(new Date(D, m, 1)) > C;) m--, m < 0 && (m = 11, D--);
      b.drawMonth = m;
      b.drawYear = D;
      var C = this._get(b, "prevText"),
        C = !l ? C : this.formatDate(C, this._daylightSavingAdjust(new Date(D, m - p, 1)), this._getFormatConfig(b)),
        C = this._canAdjustMonth(b, -1, D, m) ? '<a class="ui-datepicker-prev ui-corner-all" onclick="DP_jQuery_' + c + ".datepicker._adjustDate('#" + b.id + "', -" + p + ", 'M');\" title=\"" + C + '"><span class="ui-icon ui-icon-circle-triangle-' + (g ? "e" : "w") + '">' + C + "</span></a>" : j ? "" : '<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="' + C + '"><span class="ui-icon ui-icon-circle-triangle-' + (g ? "e" : "w") + '">' + C + "</span></a>",
        z = this._get(b, "nextText"),
        z = !l ? z : this.formatDate(z, this._daylightSavingAdjust(new Date(D, m + p, 1)), this._getFormatConfig(b)),
        j = this._canAdjustMonth(b, 1, D, m) ? '<a class="ui-datepicker-next ui-corner-all" onclick="DP_jQuery_' + c + ".datepicker._adjustDate('#" + b.id + "', +" + p + ", 'M');\" title=\"" + z + '"><span class="ui-icon ui-icon-circle-triangle-' + (g ? "w" : "e") + '">' + z + "</span></a>" : j ? "" : '<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="' + z + '"><span class="ui-icon ui-icon-circle-triangle-' + (g ? "w" : "e") + '">' + z + "</span></a>",
        p = this._get(b, "currentText"),
        z = this._get(b, "gotoCurrent") && b.currentDay ? u : e,
        p = !l ? p : this.formatDate(p, z, this._getFormatConfig(b)),
        l = !b.inline ? '<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" onclick="DP_jQuery_' + c + '.datepicker._hideDatepicker();">' + this._get(b, "closeText") + "</button>" : "",
        h = h ? '<div class="ui-datepicker-buttonpane ui-widget-content">' + (g ? l : "") + (this._isInRange(b, z) ? '<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" onclick="DP_jQuery_' + c + ".datepicker._gotoToday('#" + b.id + "');\">" + p + "</button>" : "") + (g ? "" : l) + "</div>" : "",
        l = parseInt(this._get(b, "firstDay"), 10),
        l = isNaN(l) ? 0 : l,
        p = this._get(b, "showWeek"),
        z = this._get(b, "dayNames");
      this._get(b, "dayNamesShort");
      var F = this._get(b, "dayNamesMin"),
        q = this._get(b, "monthNames"),
        K = this._get(b, "monthNamesShort"),
        O = this._get(b, "beforeShowDay"),
        V = this._get(b, "showOtherMonths"),
        cb = this._get(b, "selectOtherMonths");
      this._get(b, "calculateWeek");
      for (var Ha = this._getDefaultDate(b), H = "", sa = 0; sa < n[0]; sa++) {
        for (var gb = "", k = 0; k < n[1]; k++) {
          var va = this._daylightSavingAdjust(new Date(D, m, b.selectedDay)),
            Z = " ui-corner-all",
            fa = "";
          if (o) {
            fa += '<div class="ui-datepicker-group';
            if (n[1] > 1) switch (k) {
            case 0:
              fa += " ui-datepicker-group-first";
              Z = " ui-corner-" + (g ? "right" : "left");
              break;
            case n[1] - 1:
              fa += " ui-datepicker-group-last";
              Z = " ui-corner-" + (g ? "left" : "right");
              break;
            default:
              fa += " ui-datepicker-group-middle", Z = ""
            }
            fa += '">'
          }
          fa += '<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix' + Z + '">' + (/all|left/.test(Z) && sa == 0 ? g ? j : C : "") + (/all|right/.test(Z) && sa == 0 ? g ? C : j : "") + this._generateMonthYearHeader(b, m, D, w, s, sa > 0 || k > 0, q, K) + '</div><table class="ui-datepicker-calendar"><thead><tr>';
          for (var Ea = p ? '<th class="ui-datepicker-week-col">' + this._get(b, "weekHeader") + "</th>" : "", Z = 0; Z < 7; Z++) {
            var P = (Z + l) % 7;
            Ea += "<th" + ((Z + l + 6) % 7 >= 5 ? ' class="ui-datepicker-week-end"' : "") + '><span title="' + z[P] + '">' + F[P] + "</span></th>"
          }
          fa += Ea + "</tr></thead><tbody>";
          Ea = this._getDaysInMonth(D, m);
          if (D == b.selectedYear && m == b.selectedMonth) b.selectedDay = Math.min(b.selectedDay, Ea);
          for (var Z = (this._getFirstDayOfMonth(D, m) - l + 7) % 7, Ea = o ? 6 : Math.ceil((Z + Ea) / 7), P = this._daylightSavingAdjust(new Date(D, m, 1 - Z)), Y = 0; Y < Ea; Y++) {
            fa += "<tr>";
            for (var M = !p ? "" : '<td class="ui-datepicker-week-col">' + this._get(b, "calculateWeek")(P) + "</td>", Z = 0; Z < 7; Z++) {
              var A = O ? O.apply(b.input ? b.input[0] : null, [P]) : [true, ""],
                S = P.getMonth() != m,
                La = S && !cb || !A[0] || w && P < w || s && P > s;
              M += '<td class="' + ((Z + l + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (S ? " ui-datepicker-other-month" : "") + (P.getTime() == va.getTime() && m == b.selectedMonth && b._keyEvent || Ha.getTime() == P.getTime() && Ha.getTime() == va.getTime() ? " " + this._dayOverClass : "") + (La ? " " + this._unselectableClass + " ui-state-disabled" : "") + (S && !V ? "" : " " + A[1] + (P.getTime() == u.getTime() ? " " + this._currentClass : "") + (P.getTime() == e.getTime() ? " ui-datepicker-today" : "")) + '"' + ((!S || V) && A[2] ? ' title="' + A[2] + '"' : "") + (La ? "" : ' onclick="DP_jQuery_' + c + ".datepicker._selectDay('#" + b.id + "'," + P.getMonth() + "," + P.getFullYear() + ', this);return false;"') + ">" + (S && !V ? "&#xa0;" : La ? '<span class="ui-state-default">' + P.getDate() + "</span>" : '<a class="ui-state-default' + (P.getTime() == e.getTime() ? " ui-state-highlight" : "") + (P.getTime() == u.getTime() ? " ui-state-active" : "") + (S ? " ui-priority-secondary" : "") + '" href="#">' + P.getDate() + "</a>") + "</td>";
              P.setDate(P.getDate() + 1);
              P = this._daylightSavingAdjust(P)
            }
            fa += M + "</tr>"
          }
          m++;
          m > 11 && (m = 0, D++);
          fa += "</tbody></table>" + (o ? "</div>" + (n[0] > 0 && k == n[1] - 1 ? '<div class="ui-datepicker-row-break"></div>' : "") : "");
          gb += fa
        }
        H += gb
      }
      H += h + (a.browser.msie && parseInt(a.browser.version, 10) < 7 && !b.inline ? '<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>' : "");
      b._keyEvent = false;
      return H
    },
    _generateMonthYearHeader: function (a, b, e, h, j, l, n, m) {
      var p = this._get(a, "changeMonth"),
        o = this._get(a, "changeYear"),
        u = this._get(a, "showMonthAfterYear"),
        w = '<div class="ui-datepicker-title">',
        s = "";
      if (l || !p) s += '<span class="ui-datepicker-month">' + n[b] + "</span>";
      else {
        var n = h && h.getFullYear() == e,
          D = j && j.getFullYear() == e;
        s += '<select class="ui-datepicker-month" onchange="DP_jQuery_' + c + ".datepicker._selectMonthYear('#" + a.id + "', this, 'M');\" onclick=\"DP_jQuery_" + c + ".datepicker._clickMonthYear('#" + a.id + "');\">";
        for (var C = 0; C < 12; C++) if ((!n || C >= h.getMonth()) && (!D || C <= j.getMonth())) s += '<option value="' + C + '"' + (C == b ? ' selected="selected"' : "") + ">" + m[C] + "</option>";
        s += "</select>"
      }
      u || (w += s + (l || !p || !o ? "&#xa0;" : ""));
      if (l || !o) w += '<span class="ui-datepicker-year">' + e + "</span>";
      else {
        var m = this._get(a, "yearRange").split(":"),
          z = (new Date).getFullYear(),
          n = function (a) {
            a = a.match(/c[+-].*/) ? e + parseInt(a.substring(1), 10) : a.match(/[+-].*/) ? z + parseInt(a, 10) : parseInt(a, 10);
            return isNaN(a) ? z : a
          },
          b = n(m[0]),
          m = Math.max(b, n(m[1] || "")),
          b = h ? Math.max(b, h.getFullYear()) : b,
          m = j ? Math.min(m, j.getFullYear()) : m;
        for (w += '<select class="ui-datepicker-year" onchange="DP_jQuery_' + c + ".datepicker._selectMonthYear('#" + a.id + "', this, 'Y');\" onclick=\"DP_jQuery_" + c + ".datepicker._clickMonthYear('#" + a.id + "');\">"; b <= m; b++) w += '<option value="' + b + '"' + (b == e ? ' selected="selected"' : "") + ">" + b + "</option>";
        w += "</select>"
      }
      w += this._get(a, "yearSuffix");
      u && (w += (l || !p || !o ? "&#xa0;" : "") + s);
      w += "</div>";
      return w
    },
    _adjustInstDate: function (a, b, c) {
      var e = a.drawYear + (c == "Y" ? b : 0),
        j = a.drawMonth + (c == "M" ? b : 0),
        b = Math.min(a.selectedDay, this._getDaysInMonth(e, j)) + (c == "D" ? b : 0),
        e = this._restrictMinMax(a, this._daylightSavingAdjust(new Date(e, j, b)));
      a.selectedDay = e.getDate();
      a.drawMonth = a.selectedMonth = e.getMonth();
      a.drawYear = a.selectedYear = e.getFullYear();
      (c == "M" || c == "Y") && this._notifyChange(a)
    },
    _restrictMinMax: function (a, b) {
      var c = this._getMinMaxDate(a, "min"),
        a = this._getMinMaxDate(a, "max"),
        b = c && b < c ? c : b;
      return a && b > a ? a : b
    },
    _notifyChange: function (a) {
      var b = this._get(a, "onChangeMonthYear");
      b && b.apply(a.input ? a.input[0] : null, [a.selectedYear, a.selectedMonth + 1, a])
    },
    _getNumberOfMonths: function (a) {
      a = this._get(a, "numberOfMonths");
      return a == null ? [1, 1] : typeof a == "number" ? [1, a] : a
    },
    _getMinMaxDate: function (a, b) {
      return this._determineDate(a, this._get(a, b + "Date"), null)
    },
    _getDaysInMonth: function (a, b) {
      return 32 - (new Date(a, b, 32)).getDate()
    },
    _getFirstDayOfMonth: function (a, b) {
      return (new Date(a, b, 1)).getDay()
    },
    _canAdjustMonth: function (a, b, c, e) {
      var j = this._getNumberOfMonths(a),
        c = this._daylightSavingAdjust(new Date(c, e + (b < 0 ? b : j[0] * j[1]), 1));
      b < 0 && c.setDate(this._getDaysInMonth(c.getFullYear(), c.getMonth()));
      return this._isInRange(a, c)
    },
    _isInRange: function (a, b) {
      var c = this._getMinMaxDate(a, "min"),
        a = this._getMinMaxDate(a, "max");
      return (!c || b.getTime() >= c.getTime()) && (!a || b.getTime() <= a.getTime())
    },
    _getFormatConfig: function (a) {
      var b = this._get(a, "shortYearCutoff"),
        b = typeof b != "string" ? b : (new Date).getFullYear() % 100 + parseInt(b, 10);
      return {
        shortYearCutoff: b,
        dayNamesShort: this._get(a, "dayNamesShort"),
        dayNames: this._get(a, "dayNames"),
        monthNamesShort: this._get(a, "monthNamesShort"),
        monthNames: this._get(a, "monthNames")
      }
    },
    _formatDate: function (a, b, c, e) {
      if (!b) a.currentDay = a.selectedDay, a.currentMonth = a.selectedMonth, a.currentYear = a.selectedYear;
      b = b ? typeof b == "object" ? b : this._daylightSavingAdjust(new Date(e, c, b)) : this._daylightSavingAdjust(new Date(a.currentYear, a.currentMonth, a.currentDay));
      return this.formatDate(this._get(a, "dateFormat"), b, this._getFormatConfig(a))
    }
  });
  a.fn.datepicker = function (b) {
    if (!a.datepicker.initialized) a(document).mousedown(a.datepicker._checkExternalClick).find("body").append(a.datepicker.dpDiv), a.datepicker.initialized = true;
    var c = Array.prototype.slice.call(arguments, 1);
    return typeof b == "string" && (b == "isDisabled" || b == "getDate" || b == "widget") ? a.datepicker["_" + b + "Datepicker"].apply(a.datepicker, [this[0]].concat(c)) : b == "option" && arguments.length == 2 && typeof arguments[1] == "string" ? a.datepicker["_" + b + "Datepicker"].apply(a.datepicker, [this[0]].concat(c)) : this.each(function () {
      typeof b == "string" ? a.datepicker["_" + b + "Datepicker"].apply(a.datepicker, [this].concat(c)) : a.datepicker._attachDatepicker(this, b)
    })
  };
  a.datepicker = new e;
  a.datepicker.initialized = false;
  a.datepicker.uuid = (new Date).getTime();
  a.datepicker.version = "1.8.1";
  window["DP_jQuery_" + c] = a
})(jQuery);
(function (a) {
  a.widget("ui.progressbar", {
    options: {
      value: 0
    },
    _create: function () {
      this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({
        role: "progressbar",
        "aria-valuemin": this._valueMin(),
        "aria-valuemax": this._valueMax(),
        "aria-valuenow": this._value()
      });
      this.valueDiv = a("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element);
      this._refreshValue()
    },
    destroy: function () {
      this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow");
      this.valueDiv.remove();
      a.Widget.prototype.destroy.apply(this, arguments)
    },
    value: function (a) {
      if (a === void 0) return this._value();
      this._setOption("value", a);
      return this
    },
    _setOption: function (e, b) {
      switch (e) {
      case "value":
        this.options.value = b, this._refreshValue(), this._trigger("change")
      }
      a.Widget.prototype._setOption.apply(this, arguments)
    },
    _value: function () {
      var a = this.options.value;
      typeof a !== "number" && (a = 0);
      a < this._valueMin() && (a = this._valueMin());
      a > this._valueMax() && (a = this._valueMax());
      return a
    },
    _valueMin: function () {
      return 0
    },
    _valueMax: function () {
      return 100
    },
    _refreshValue: function () {
      var a = this.value();
      this.valueDiv[a === this._valueMax() ? "addClass" : "removeClass"]("ui-corner-right").width(a + "%");
      this.element.attr("aria-valuenow", a)
    }
  });
  a.extend(a.ui.progressbar, {
    version: "1.8.2"
  })
})(jQuery);
jQuery(function (a) {
  a.datepicker.regional.de = {
    closeText: "schlie\u00dfen",
    prevText: "&#x3c;zur\u00fcck",
    nextText: "Vor&#x3e;",
    currentText: "heute",
    monthNames: "Januar,Februar,M\u00e4rz,April,Mai,Juni,Juli,August,September,Oktober,November,Dezember".split(","),
    monthNamesShort: "Jan,Feb,M\u00e4r,Apr,Mai,Jun,Jul,Aug,Sep,Okt,Nov,Dez".split(","),
    dayNames: "Sonntag,Montag,Dienstag,Mittwoch,Donnerstag,Freitag,Samstag".split(","),
    dayNamesShort: "So,Mo,Di,Mi,Do,Fr,Sa".split(","),
    dayNamesMin: "So,Mo,Di,Mi,Do,Fr,Sa".split(","),
    dateFormat: "dd.mm.yy",
    firstDay: 1,
    isRTL: false
  };
  a.datepicker.regional.es = {
    closeText: "Cerrar",
    prevText: "&#x3c;Ant",
    nextText: "Sig&#x3e;",
    currentText: "Hoy",
    monthNames: "Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre".split(","),
    monthNamesShort: "Ene,Feb,Mar,Abr,May,Jun,Jul,Ago,Sep,Oct,Nov,Dic".split(","),
    dayNames: "Domingo,Lunes,Martes,Mi&eacute;rcoles,Jueves,Viernes,S&aacute;bado".split(","),
    dayNamesShort: "Dom,Lun,Mar,Mi&eacute;,Juv,Vie,S&aacute;b".split(","),
    dayNamesMin: "Do,Lu,Ma,Mi,Ju,Vi,S&aacute;".split(","),
    dateFormat: "dd/mm/yy",
    firstDay: 0,
    isRTL: false
  };
  a.datepicker.regional.fi = {
    closeText: "Sulje",
    prevText: "&laquo;Edellinen",
    nextText: "Seuraava&raquo;",
    currentText: "T&auml;n&auml;&auml;n",
    monthNames: "Tammikuu,Helmikuu,Maaliskuu,Huhtikuu,Toukokuu,Kes&auml;kuu,Hein&auml;kuu,Elokuu,Syyskuu,Lokakuu,Marraskuu,Joulukuu".split(","),
    monthNamesShort: "Tammi,Helmi,Maalis,Huhti,Touko,Kes&auml;,Hein&auml;,Elo,Syys,Loka,Marras,Joulu".split(","),
    dayNamesShort: "Su,Ma,Ti,Ke,To,Pe,Su".split(","),
    dayNames: "Sunnuntai,Maanantai,Tiistai,Keskiviikko,Torstai,Perjantai,Lauantai".split(","),
    dayNamesMin: "Su,Ma,Ti,Ke,To,Pe,La".split(","),
    dateFormat: "dd.mm.yy",
    firstDay: 1,
    isRTL: false
  };
  a.datepicker.regional.fr = {
    closeText: "Fermer",
    prevText: "&#x3c;Pr\u00e9c",
    nextText: "Suiv&#x3e;",
    currentText: "Courant",
    monthNames: "Janvier,F\u00e9vrier,Mars,Avril,Mai,Juin,Juillet,Ao\u00fbt,Septembre,Octobre,Novembre,D\u00e9cembre".split(","),
    monthNamesShort: "Jan,F\u00e9v,Mar,Avr,Mai,Jun,Jul,Ao\u00fb,Sep,Oct,Nov,D\u00e9c".split(","),
    dayNames: "Dimanche,Lundi,Mardi,Mercredi,Jeudi,Vendredi,Samedi".split(","),
    dayNamesShort: "Dim,Lun,Mar,Mer,Jeu,Ven,Sam".split(","),
    dayNamesMin: "Di,Lu,Ma,Me,Je,Ve,Sa".split(","),
    dateFormat: "dd/mm/yy",
    firstDay: 1,
    isRTL: false
  };
  a.datepicker.regional.ja = {
    closeText: "\u9589\u3058\u308b",
    prevText: "&#x3c;\u524d",
    nextText: "\u6b21&#x3e;",
    currentText: "\u4eca\u65e5",
    monthNames: "1\u6708,2\u6708,3\u6708,4\u6708,5\u6708,6\u6708,7\u6708,8\u6708,9\u6708,10\u6708,11\u6708,12\u6708".split(","),
    monthNamesShort: "1\u6708,2\u6708,3\u6708,4\u6708,5\u6708,6\u6708,7\u6708,8\u6708,9\u6708,10\u6708,11\u6708,12\u6708".split(","),
    dayNames: "\u65e5\u66dc\u65e5,\u6708\u66dc\u65e5,\u706b\u66dc\u65e5,\u6c34\u66dc\u65e5,\u6728\u66dc\u65e5,\u91d1\u66dc\u65e5,\u571f\u66dc\u65e5".split(","),
    dayNamesShort: "\u65e5,\u6708,\u706b,\u6c34,\u6728,\u91d1,\u571f".split(","),
    dayNamesMin: "\u65e5,\u6708,\u706b,\u6c34,\u6728,\u91d1,\u571f".split(","),
    dateFormat: "yy/mm/dd",
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: true
  };
  a.datepicker.regional.ms = {
    closeText: "Tutup",
    prevText: "&#x3c;Sebelum",
    nextText: "Selepas&#x3e;",
    currentText: "hari ini",
    monthNames: "Januari,Februari,Mac,April,Mei,Jun,Julai,Ogos,September,Oktober,November,Disember".split(","),
    monthNamesShort: "Jan,Feb,Mac,Apr,Mei,Jun,Jul,Ogo,Sep,Okt,Nov,Dis".split(","),
    dayNames: "Ahad,Isnin,Selasa,Rabu,Khamis,Jumaat,Sabtu".split(","),
    dayNamesShort: "Aha,Isn,Sel,Rab,kha,Jum,Sab".split(","),
    dayNamesMin: "Ah,Is,Se,Ra,Kh,Ju,Sa".split(","),
    dateFormat: "dd/mm/yy",
    firstDay: 0,
    isRTL: false
  };
  a.datepicker.regional.ru = {
    closeText: "\u0417\u0430\u043a\u0440\u044b\u0442\u044c",
    prevText: "&#x3c;\u041f\u0440\u0435\u0434",
    nextText: "\u0421\u043b\u0435\u0434&#x3e;",
    currentText: "\u0421\u0435\u0433\u043e\u0434\u043d\u044f",
    monthNames: "\u042f\u043d\u0432\u0430\u0440\u044c,\u0424\u0435\u0432\u0440\u0430\u043b\u044c,\u041c\u0430\u0440\u0442,\u0410\u043f\u0440\u0435\u043b\u044c,\u041c\u0430\u0439,\u0418\u044e\u043d\u044c,\u0418\u044e\u043b\u044c,\u0410\u0432\u0433\u0443\u0441\u0442,\u0421\u0435\u043d\u0442\u044f\u0431\u0440\u044c,\u041e\u043a\u0442\u044f\u0431\u0440\u044c,\u041d\u043e\u044f\u0431\u0440\u044c,\u0414\u0435\u043a\u0430\u0431\u0440\u044c".split(","),
    monthNamesShort: "\u042f\u043d\u0432,\u0424\u0435\u0432,\u041c\u0430\u0440,\u0410\u043f\u0440,\u041c\u0430\u0439,\u0418\u044e\u043d,\u0418\u044e\u043b,\u0410\u0432\u0433,\u0421\u0435\u043d,\u041e\u043a\u0442,\u041d\u043e\u044f,\u0414\u0435\u043a".split(","),
    dayNames: "\u0432\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u044c\u0435,\u043f\u043e\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u0438\u043a,\u0432\u0442\u043e\u0440\u043d\u0438\u043a,\u0441\u0440\u0435\u0434\u0430,\u0447\u0435\u0442\u0432\u0435\u0440\u0433,\u043f\u044f\u0442\u043d\u0438\u0446\u0430,\u0441\u0443\u0431\u0431\u043e\u0442\u0430".split(","),
    dayNamesShort: "\u0432\u0441\u043a,\u043f\u043d\u0434,\u0432\u0442\u0440,\u0441\u0440\u0434,\u0447\u0442\u0432,\u043f\u0442\u043d,\u0441\u0431\u0442".split(","),
    dayNamesMin: "\u0412\u0441,\u041f\u043d,\u0412\u0442,\u0421\u0440,\u0427\u0442,\u041f\u0442,\u0421\u0431".split(","),
    dateFormat: "dd.mm.yy",
    firstDay: 1,
    isRTL: false
  };
  a.datepicker.regional.et = {
    closeText: "sulge",
    prevText: "&#x3c;tagasi",
    nextText: "Edasi&#x3e;",
    currentText: "t\u00e4na",
    monthNames: "Jaanuar,Veebruar,M\u00e4rts,Aprill,Mai,Juuni,Juuli,August,September,Oktoober,November,Detsember".split(","),
    monthNamesShort: "Jaan,Veeb,M\u00e4rts,Apr,Mai,Juuni,Juuli,Aug,Sep,Okt,Nov,Dets".split(","),
    dayNames: "P\u00fchap\u00e4ev,Esmasp\u00e4ev,Teisip\u00e4ev,Kolmap\u00e4ev,Neljap\u00e4ev,Reede,Laup\u00e4ev".split(","),
    dayNamesShort: "P,E,T,K,N,R,L".split(","),
    dayNamesMin: "P,E,T,K,N,R,L".split(","),
    dateFormat: "dd.mm.yy",
    firstDay: 1,
    isRTL: false
  }
});
(function (a) {
  var e, b = function (b) {
      a(":ui-button", b.target.form).each(function () {
        var b = a(this).data("button");
        setTimeout(function () {
          b.refresh()
        }, 1)
      })
    },
    c = function (b) {
      var c = b.name,
        e = b.form,
        h = a([]);
      c && (h = e ? a(e).find("[name='" + c + "']") : a("[name='" + c + "']", b.ownerDocument).filter(function () {
        return !this.form
      }));
      return h
    };
  a.widget("ui.button", {
    options: {
      text: true,
      label: null,
      icons: {
        primary: null,
        secondary: null
      }
    },
    _create: function () {
      this.element.closest("form").unbind("reset.button").bind("reset.button", b);
      this._determineButtonType();
      this.hasTitle = !! this.buttonElement.attr("title");
      var d = this,
        f = this.options,
        g = this.type === "checkbox" || this.type === "radio",
        h = "ui-state-hover" + (!g ? " ui-state-active" : "");
      if (f.label === null) f.label = this.buttonElement.html();
      if (this.element.is(":disabled")) f.disabled = true;
      this.buttonElement.addClass("ui-button ui-widget ui-state-default ui-corner-all").attr("role", "button").bind("mouseenter.button", function () {
        f.disabled || (a(this).addClass("ui-state-hover"), this === e && a(this).addClass("ui-state-active"))
      }).bind("mouseleave.button", function () {
        f.disabled || a(this).removeClass(h)
      }).bind("focus.button", function () {
        a(this).addClass("ui-state-focus")
      }).bind("blur.button", function () {
        a(this).removeClass("ui-state-focus")
      });
      g && this.element.bind("change.button", function () {
        d.refresh()
      });
      this.type === "checkbox" ? this.buttonElement.bind("click.button", function () {
        if (f.disabled) return false;
        a(this).toggleClass("ui-state-active");
        d.buttonElement.attr("aria-pressed", d.element[0].checked)
      }) : this.type === "radio" ? this.buttonElement.bind("click.button", function () {
        if (f.disabled) return false;
        a(this).addClass("ui-state-active");
        d.buttonElement.attr("aria-pressed", true);
        var b = d.element[0];
        c(b).not(b).map(function () {
          return a(this).button("widget")[0]
        }).removeClass("ui-state-active").attr("aria-pressed", false)
      }) : (this.buttonElement.bind("mousedown.button", function () {
        if (f.disabled) return false;
        a(this).addClass("ui-state-active");
        e = this;
        a(document).one("mouseup", function () {
          e = null
        })
      }).bind("mouseup.button", function () {
        if (f.disabled) return false;
        a(this).removeClass("ui-state-active")
      }).bind("keydown.button", function (b) {
        if (f.disabled) return false;
        (b.keyCode == a.ui.keyCode.SPACE || b.keyCode == a.ui.keyCode.ENTER) && a(this).addClass("ui-state-active")
      }).bind("keyup.button", function () {
        a(this).removeClass("ui-state-active")
      }), this.buttonElement.is("a") && this.buttonElement.keyup(function (b) {
        b.keyCode === a.ui.keyCode.SPACE && a(this).click()
      }));
      this._setOption("disabled", f.disabled)
    },
    _determineButtonType: function () {
      this.type = this.element.is(":checkbox") ? "checkbox" : this.element.is(":radio") ? "radio" : this.element.is("input") ? "input" : "button";
      if (this.type === "checkbox" || this.type === "radio") {
        this.buttonElement = this.element.parents().last().find("[for=" + this.element.attr("id") + "]");
        this.element.addClass("ui-helper-hidden-accessible");
        var a = this.element.is(":checked");
        a && this.buttonElement.addClass("ui-state-active");
        this.buttonElement.attr("aria-pressed", a)
      } else this.buttonElement = this.element
    },
    widget: function () {
      return this.buttonElement
    },
    destroy: function () {
      this.element.removeClass("ui-helper-hidden-accessible");
      this.buttonElement.removeClass("ui-button ui-widget ui-state-default ui-corner-all ui-state-hover ui-state-active  ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon ui-button-text-only").removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html());
      this.hasTitle || this.buttonElement.removeAttr("title");
      a.Widget.prototype.destroy.call(this)
    },
    _setOption: function (b, c) {
      a.Widget.prototype._setOption.apply(this, arguments);
      b === "disabled" && (c ? this.element.attr("disabled", true) : this.element.removeAttr("disabled"));
      this._resetButton()
    },
    refresh: function () {
      var b = this.element.is(":disabled");
      b !== this.options.disabled && this._setOption("disabled", b);
      this.type === "radio" ? c(this.element[0]).each(function () {
        a(this).is(":checked") ? a(this).button("widget").addClass("ui-state-active").attr("aria-pressed", true) : a(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", false)
      }) : this.type === "checkbox" && (this.element.is(":checked") ? this.buttonElement.addClass("ui-state-active").attr("aria-pressed", true) : this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", false))
    },
    _resetButton: function () {
      if (this.type === "input") this.options.label && this.element.val(this.options.label);
      else {
        var b = this.buttonElement.removeClass("ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon ui-button-text-only"),
          c = a("<span></span>").addClass("ui-button-text").html(this.options.label).appendTo(b.empty()).text(),
          e = this.options.icons,
          h = e.primary && e.secondary;
        e.primary || e.secondary ? (b.addClass("ui-button-text-icon" + (h ? "s" : "")), e.primary && b.prepend("<span class='ui-button-icon-primary ui-icon " + e.primary + "'></span>"), e.secondary && b.append("<span class='ui-button-icon-secondary ui-icon " + e.secondary + "'></span>"), this.options.text || (b.addClass(h ? "ui-button-icons-only" : "ui-button-icon-only").removeClass("ui-button-text-icons ui-button-text-icon"), this.hasTitle || b.attr("title", c))) : b.addClass("ui-button-text-only")
      }
    }
  });
  a.widget("ui.buttonset", {
    _create: function () {
      this.element.addClass("ui-buttonset");
      this._init()
    },
    _init: function () {
      this.refresh()
    },
    _setOption: function (b, c) {
      b === "disabled" && this.buttons.button("option", b, c);
      a.Widget.prototype._setOption.apply(this, arguments)
    },
    refresh: function () {
      this.buttons = this.element.find(":button, :submit, :reset, :checkbox, :radio, a, :data(button)").filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function () {
        return a(this).button("widget")[0]
      }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass("ui-corner-left").end().filter(":last").addClass("ui-corner-right").end().end()
    },
    destroy: function () {
      this.element.removeClass("ui-buttonset");
      this.buttons.map(function () {
        return a(this).button("widget")[0]
      }).removeClass("ui-corner-left ui-corner-right").end().button("destroy");
      a.Widget.prototype.destroy.call(this)
    }
  })
})(jQuery);
(function (a) {
  a.widget("ui.mouse", {
    options: {
      cancel: ":input,option",
      distance: 1,
      delay: 0
    },
    _mouseInit: function () {
      var a = this;
      this.element.bind("mousedown." + this.widgetName, function (b) {
        return a._mouseDown(b)
      }).bind("click." + this.widgetName, function (b) {
        if (a._preventClickEvent) return a._preventClickEvent = false, b.stopImmediatePropagation(), false
      });
      this.started = false
    },
    _mouseDestroy: function () {
      this.element.unbind("." + this.widgetName)
    },
    _mouseDown: function (e) {
      e.originalEvent = e.originalEvent || {};
      if (!e.originalEvent.mouseHandled) {
        this._mouseStarted && this._mouseUp(e);
        this._mouseDownEvent = e;
        var b = this,
          c = e.which == 1,
          d = typeof this.options.cancel == "string" ? a(e.target).parents().add(e.target).filter(this.options.cancel).length : false;
        if (!c || d || !this._mouseCapture(e)) return true;
        this.mouseDelayMet = !this.options.delay;
        if (!this.mouseDelayMet) this._mouseDelayTimer = setTimeout(function () {
          b.mouseDelayMet = true
        }, this.options.delay);
        if (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(e) !== false, !this._mouseStarted)) return e.preventDefault(), true;
        this._mouseMoveDelegate = function (a) {
          return b._mouseMove(a)
        };
        this._mouseUpDelegate = function (a) {
          return b._mouseUp(a)
        };
        a(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate);
        a.browser.safari || e.preventDefault();
        return e.originalEvent.mouseHandled = true
      }
    },
    _mouseMove: function (e) {
      if (a.browser.msie && !e.button) return this._mouseUp(e);
      if (this._mouseStarted) return this._mouseDrag(e), e.preventDefault();
      if (this._mouseDistanceMet(e) && this._mouseDelayMet(e))(this._mouseStarted = this._mouseStart(this._mouseDownEvent, e) !== false) ? this._mouseDrag(e) : this._mouseUp(e);
      return !this._mouseStarted
    },
    _mouseUp: function (e) {
      a(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate);
      if (this._mouseStarted) this._mouseStarted = false, this._preventClickEvent = e.target == this._mouseDownEvent.target, this._mouseStop(e);
      return false
    },
    _mouseDistanceMet: function (a) {
      return Math.max(Math.abs(this._mouseDownEvent.pageX - a.pageX), Math.abs(this._mouseDownEvent.pageY - a.pageY)) >= this.options.distance
    },
    _mouseDelayMet: function () {
      return this.mouseDelayMet
    },
    _mouseStart: function () {},
    _mouseDrag: function () {},
    _mouseStop: function () {},
    _mouseCapture: function () {
      return true
    }
  })
})(jQuery);
(function (a) {
  a.widget("ui.dialog", {
    options: {
      autoOpen: true,
      buttons: {},
      closeOnEscape: true,
      closeText: "close",
      dialogClass: "",
      draggable: true,
      hide: null,
      height: "auto",
      maxHeight: false,
      maxWidth: false,
      minHeight: 150,
      minWidth: 150,
      modal: false,
      position: "center",
      resizable: true,
      show: null,
      stack: true,
      title: "",
      width: 300,
      zIndex: 1E3
    },
    _create: function () {
      this.originalTitle = this.element.attr("title");
      var e = this,
        b = e.options,
        c = b.title || e.originalTitle || "&#160;",
        d = a.ui.dialog.getTitleId(e.element),
        f = (e.uiDialog = a("<div></div>")).appendTo(document.body).hide().addClass("ui-dialog ui-widget ui-widget-content ui-corner-all " + b.dialogClass).css({
          zIndex: b.zIndex
        }).attr("tabIndex", -1).css("outline", 0).keydown(function (c) {
          b.closeOnEscape && c.keyCode && c.keyCode === a.ui.keyCode.ESCAPE && (e.close(c), c.preventDefault())
        }).attr({
          role: "dialog",
          "aria-labelledby": d
        }).mousedown(function (a) {
          e.moveToTop(false, a)
        });
      e.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(f);
      var g = (e.uiDialogTitlebar = a("<div></div>")).addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(f),
        h = a('<a href="#"></a>').addClass("ui-dialog-titlebar-close ui-corner-all").attr("role", "button").hover(function () {
          h.addClass("ui-state-hover")
        }, function () {
          h.removeClass("ui-state-hover")
        }).focus(function () {
          h.addClass("ui-state-focus")
        }).blur(function () {
          h.removeClass("ui-state-focus")
        }).click(function (a) {
          e.close(a);
          return false
        }).appendTo(g);
      (e.uiDialogTitlebarCloseText = a("<span></span>")).addClass("ui-icon ui-icon-closethick").text(b.closeText).appendTo(h);
      a("<span></span>").addClass("ui-dialog-title").attr("id", d).html(c).prependTo(g);
      if (a.isFunction(b.beforeclose) && !a.isFunction(b.beforeClose)) b.beforeClose = b.beforeclose;
      g.find("*").add(g).disableSelection();
      b.draggable && a.fn.draggable && e._makeDraggable();
      b.resizable && a.fn.resizable && e._makeResizable();
      e._createButtons(b.buttons);
      e._isOpen = false;
      a.fn.bgiframe && f.bgiframe()
    },
    _init: function () {
      this.options.autoOpen && this.open()
    },
    destroy: function () {
      this.overlay && this.overlay.destroy();
      this.uiDialog.hide();
      this.element.unbind(".dialog").removeData("dialog").removeClass("ui-dialog-content ui-widget-content").hide().appendTo("body");
      this.uiDialog.remove();
      this.originalTitle && this.element.attr("title", this.originalTitle);
      return this
    },
    widget: function () {
      return this.uiDialog
    },
    close: function (e) {
      var b = this,
        c;
      if (false !== b._trigger("beforeClose", e)) {
        b.overlay && b.overlay.destroy();
        b.uiDialog.unbind("keypress.ui-dialog");
        b._isOpen = false;
        b.options.hide ? b.uiDialog.hide(b.options.hide, function () {
          b._trigger("close", e)
        }) : (b.uiDialog.hide(), b._trigger("close", e));
        a.ui.dialog.overlay.resize();
        if (b.options.modal) c = 0, a(".ui-dialog").each(function () {
          this !== b.uiDialog[0] && (c = Math.max(c, a(this).css("z-index")))
        }), a.ui.dialog.maxZ = c;
        return b
      }
    },
    isOpen: function () {
      return this._isOpen
    },
    moveToTop: function (e, b) {
      var c = this.options;
      if (c.modal && !e || !c.stack && !c.modal) return this._trigger("focus", b);
      if (c.zIndex > a.ui.dialog.maxZ) a.ui.dialog.maxZ = c.zIndex;
      if (this.overlay) a.ui.dialog.maxZ += 1, this.overlay.$el.css("z-index", a.ui.dialog.overlay.maxZ = a.ui.dialog.maxZ);
      e = {
        scrollTop: this.element.attr("scrollTop"),
        scrollLeft: this.element.attr("scrollLeft")
      };
      a.ui.dialog.maxZ += 1;
      this.uiDialog.css("z-index", a.ui.dialog.maxZ);
      this.element.attr(e);
      this._trigger("focus", b);
      return this
    },
    open: function () {
      if (!this._isOpen) {
        var e = this.options,
          b = this.uiDialog;
        this.overlay = e.modal ? new a.ui.dialog.overlay(this) : null;
        b.next().length && b.appendTo("body");
        this._size();
        this._position(e.position);
        b.show(e.show);
        this.moveToTop(true);
        e.modal && b.bind("keypress.ui-dialog", function (b) {
          if (b.keyCode === a.ui.keyCode.TAB) {
            var d = a(":tabbable", this),
              e = d.filter(":first"),
              d = d.filter(":last");
            if (b.target === d[0] && !b.shiftKey) return e.focus(1), false;
            else if (b.target === e[0] && b.shiftKey) return d.focus(1), false
          }
        });
        a([]).add(b.find(".ui-dialog-content :tabbable:first")).add(b.find(".ui-dialog-buttonpane :tabbable:first")).add(b).filter(":first").focus();
        this._trigger("open");
        this._isOpen = true;
        return this
      }
    },
    _createButtons: function (e) {
      var b = this,
        c = false,
        d = a("<div></div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix");
      b.uiDialog.find(".ui-dialog-buttonpane").remove();
      typeof e === "object" && e !== null && a.each(e, function () {
        return !(c = true)
      });
      c && (a.each(e, function (c, e) {
        c = a('<button type="button"></button>').text(c).click(function () {
          e.apply(b.element[0], arguments)
        }).appendTo(d);
        a.fn.button && c.button()
      }), d.appendTo(b.uiDialog))
    },
    _makeDraggable: function () {
      function e(a) {
        return {
          position: a.position,
          offset: a.offset
        }
      }
      var b = this,
        c = b.options,
        d = a(document),
        f;
      b.uiDialog.draggable({
        cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
        handle: ".ui-dialog-titlebar",
        containment: "document",
        start: function (d, h) {
          f = c.height === "auto" ? "auto" : a(this).height();
          a(this).height(a(this).height()).addClass("ui-dialog-dragging");
          b._trigger("dragStart", d, e(h))
        },
        drag: function (a, c) {
          b._trigger("drag", a, e(c))
        },
        stop: function (g, h) {
          c.position = [h.position.left - d.scrollLeft(), h.position.top - d.scrollTop()];
          a(this).removeClass("ui-dialog-dragging").height(f);
          b._trigger("dragStop", g, e(h));
          a.ui.dialog.overlay.resize()
        }
      })
    },
    _makeResizable: function (e) {
      function b(a) {
        return {
          originalPosition: a.originalPosition,
          originalSize: a.originalSize,
          position: a.position,
          size: a.size
        }
      }
      var e = e === void 0 ? this.options.resizable : e,
        c = this,
        d = c.options,
        f = c.uiDialog.css("position"),
        e = typeof e === "string" ? e : "n,e,s,w,se,sw,ne,nw";
      c.uiDialog.resizable({
        cancel: ".ui-dialog-content",
        containment: "document",
        alsoResize: c.element,
        maxWidth: d.maxWidth,
        maxHeight: d.maxHeight,
        minWidth: d.minWidth,
        minHeight: c._minHeight(),
        handles: e,
        start: function (d, e) {
          a(this).addClass("ui-dialog-resizing");
          c._trigger("resizeStart", d, b(e))
        },
        resize: function (a, d) {
          c._trigger("resize", a, b(d))
        },
        stop: function (e, f) {
          a(this).removeClass("ui-dialog-resizing");
          d.height = a(this).height();
          d.width = a(this).width();
          c._trigger("resizeStop", e, b(f));
          a.ui.dialog.overlay.resize()
        }
      }).css("position", f).find(".ui-resizable-se").addClass("ui-icon ui-icon-grip-diagonal-se")
    },
    _minHeight: function () {
      var a = this.options;
      return a.height === "auto" ? a.minHeight : Math.min(a.minHeight, a.height)
    },
    _position: function (e) {
      var b = [],
        c = [0, 0],
        e = e || a.ui.dialog.prototype.options.position;
      if (typeof e === "string" || typeof e === "object" && "0" in e) b = e.split ? e.split(" ") : [e[0], e[1]], b.length === 1 && (b[1] = b[0]), a.each(["left", "top"], function (a, e) {
        +b[a] === b[a] && (c[a] = b[a], b[a] = e)
      });
      else if (typeof e === "object") "left" in e ? (b[0] = "left", c[0] = e.left) : "right" in e && (b[0] = "right", c[0] = -e.right), "top" in e ? (b[1] = "top", c[1] = e.top) : "bottom" in e && (b[1] = "bottom", c[1] = -e.bottom);
      (e = this.uiDialog.is(":visible")) || this.uiDialog.show();
      this.uiDialog.css({
        top: 0,
        left: 0
      }).position({
        my: b.join(" "),
        at: b.join(" "),
        offset: c.join(" "),
        of: window,
        collision: "fit",
        using: function (b) {
          var c = a(this).css(b).offset().top;
          c < 0 && a(this).css("top", b.top - c)
        }
      });
      e || this.uiDialog.hide()
    },
    _setOption: function (e, b) {
      var c = this.uiDialog,
        d = c.is(":data(resizable)"),
        f = false;
      switch (e) {
      case "beforeclose":
        e = "beforeClose";
        break;
      case "buttons":
        this._createButtons(b);
        break;
      case "closeText":
        this.uiDialogTitlebarCloseText.text("" + b);
        break;
      case "dialogClass":
        c.removeClass(this.options.dialogClass).addClass("ui-dialog ui-widget ui-widget-content ui-corner-all " + b);
        break;
      case "disabled":
        b ? c.addClass("ui-dialog-disabled") : c.removeClass("ui-dialog-disabled");
        break;
      case "draggable":
        b ? this._makeDraggable() : c.draggable("destroy");
        break;
      case "height":
        f = true;
        break;
      case "maxHeight":
        d && c.resizable("option", "maxHeight", b);
        f = true;
        break;
      case "maxWidth":
        d && c.resizable("option", "maxWidth", b);
        f = true;
        break;
      case "minHeight":
        d && c.resizable("option", "minHeight", b);
        f = true;
        break;
      case "minWidth":
        d && c.resizable("option", "minWidth", b);
        f = true;
        break;
      case "position":
        this._position(b);
        break;
      case "resizable":
        d && !b && c.resizable("destroy");
        d && typeof b === "string" && c.resizable("option", "handles", b);
        !d && b !== false && this._makeResizable(b);
        break;
      case "title":
        a(".ui-dialog-title", this.uiDialogTitlebar).html("" + (b || "&#160;"));
        break;
      case "width":
        f = true
      }
      a.Widget.prototype._setOption.apply(this, arguments);
      f && this._size()
    },
    _size: function () {
      var a = this.options,
        b;
      this.element.css({
        width: "auto",
        minHeight: 0,
        height: 0
      });
      b = this.uiDialog.css({
        height: "auto",
        width: a.width
      }).height();
      this.element.css(a.height === "auto" ? {
        minHeight: Math.max(a.minHeight - b, 0),
        height: "auto"
      } : {
        minHeight: 0,
        height: Math.max(a.height - b, 0)
      }).show();
      this.uiDialog.is(":data(resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight())
    }
  });
  a.extend(a.ui.dialog, {
    version: "1.8.2",
    uuid: 0,
    maxZ: 0,
    getTitleId: function (a) {
      a = a.attr("id");
      if (!a) this.uuid += 1, a = this.uuid;
      return "ui-dialog-title-" + a
    },
    overlay: function (e) {
      this.$el = a.ui.dialog.overlay.create(e)
    }
  });
  a.extend(a.ui.dialog.overlay, {
    instances: [],
    oldInstances: [],
    maxZ: 0,
    events: a.map("focus,mousedown,mouseup,keydown,keypress,click".split(","), function (a) {
      return a + ".dialog-overlay"
    }).join(" "),
    create: function (e) {
      this.instances.length === 0 && (setTimeout(function () {
        a.ui.dialog.overlay.instances.length && a(document).bind(a.ui.dialog.overlay.events, function (b) {
          return a(b.target).zIndex() >= a.ui.dialog.overlay.maxZ
        })
      }, 1), a(document).bind("keydown.dialog-overlay", function (b) {
        e.options.closeOnEscape && b.keyCode && b.keyCode === a.ui.keyCode.ESCAPE && (e.close(b), b.preventDefault())
      }), a(window).bind("resize.dialog-overlay", a.ui.dialog.overlay.resize));
      var b = (this.oldInstances.pop() || a("<div></div>").addClass("ui-widget-overlay")).appendTo(document.body).css({
        width: this.width(),
        height: this.height()
      });
      a.fn.bgiframe && b.bgiframe();
      this.instances.push(b);
      return b
    },
    destroy: function (e) {
      this.oldInstances.push(this.instances.splice(a.inArray(e, this.instances), 1)[0]);
      this.instances.length === 0 && a([document, window]).unbind(".dialog-overlay");
      e.remove();
      var b = 0;
      a.each(this.instances, function () {
        b = Math.max(b, this.css("z-index"))
      });
      this.maxZ = b
    },
    height: function () {
      var e, b;
      return a.browser.msie && a.browser.version < 7 ? (e = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight), b = Math.max(document.documentElement.offsetHeight, document.body.offsetHeight), e < b ? a(window).height() + "px" : e + "px") : a(document).height() + "px"
    },
    width: function () {
      var e, b;
      return a.browser.msie && a.browser.version < 7 ? (e = Math.max(document.documentElement.scrollWidth, document.body.scrollWidth), b = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth), e < b ? a(window).width() + "px" : e + "px") : a(document).width() + "px"
    },
    resize: function () {
      var e = a([]);
      a.each(a.ui.dialog.overlay.instances, function () {
        e = e.add(this)
      });
      e.css({
        width: 0,
        height: 0
      }).css({
        width: a.ui.dialog.overlay.width(),
        height: a.ui.dialog.overlay.height()
      })
    }
  });
  a.extend(a.ui.dialog.overlay.prototype, {
    destroy: function () {
      a.ui.dialog.overlay.destroy(this.$el)
    }
  })
})(jQuery);
(function (a) {
  a.widget("ui.accordion", {
    options: {
      active: 0,
      animated: "slide",
      autoHeight: true,
      clearStyle: false,
      collapsible: false,
      event: "click",
      fillSpace: false,
      header: "> li > :first-child,> :not(li):even",
      icons: {
        header: "ui-icon-triangle-1-e",
        headerSelected: "ui-icon-triangle-1-s"
      },
      navigation: false,
      navigationFilter: function () {
        return this.href.toLowerCase() == location.href.toLowerCase()
      }
    },
    _create: function () {
      var e = this.options,
        b = this;
      this.running = 0;
      this.element.addClass("ui-accordion ui-widget ui-helper-reset");
      this.element.children("li").addClass("ui-accordion-li-fix");
      this.headers = this.element.find(e.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all").bind("mouseenter.accordion", function () {
        a(this).addClass("ui-state-hover")
      }).bind("mouseleave.accordion", function () {
        a(this).removeClass("ui-state-hover")
      }).bind("focus.accordion", function () {
        a(this).addClass("ui-state-focus")
      }).bind("blur.accordion", function () {
        a(this).removeClass("ui-state-focus")
      });
      this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom");
      if (e.navigation) {
        var c = this.element.find("a").filter(e.navigationFilter);
        if (c.length) {
          var d = c.closest(".ui-accordion-header");
          this.active = d.length ? d : c.closest(".ui-accordion-content").prev()
        }
      }
      this.active = this._findActive(this.active || e.active).toggleClass("ui-state-default").toggleClass("ui-state-active").toggleClass("ui-corner-all").toggleClass("ui-corner-top");
      this.active.next().addClass("ui-accordion-content-active");
      this._createIcons();
      this.resize();
      this.element.attr("role", "tablist");
      this.headers.attr("role", "tab").bind("keydown", function (a) {
        return b._keydown(a)
      }).next().attr("role", "tabpanel");
      this.headers.not(this.active || "").attr("aria-expanded", "false").attr("tabIndex", "-1").next().hide();
      this.active.length ? this.active.attr("aria-expanded", "true").attr("tabIndex", "0") : this.headers.eq(0).attr("tabIndex", "0");
      a.browser.safari || this.headers.find("a").attr("tabIndex", "-1");
      e.event && this.headers.bind(e.event + ".accordion", function (a) {
        b._clickHandler.call(b, a, this);
        a.preventDefault()
      })
    },
    _createIcons: function () {
      var e = this.options;
      e.icons && (a("<span/>").addClass("ui-icon " + e.icons.header).prependTo(this.headers), this.active.find(".ui-icon").toggleClass(e.icons.header).toggleClass(e.icons.headerSelected), this.element.addClass("ui-accordion-icons"))
    },
    _destroyIcons: function () {
      this.headers.children(".ui-icon").remove();
      this.element.removeClass("ui-accordion-icons")
    },
    destroy: function () {
      var a = this.options;
      this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role").unbind(".accordion").removeData("accordion");
      this.headers.unbind(".accordion").removeClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("tabIndex");
      this.headers.find("a").removeAttr("tabIndex");
      this._destroyIcons();
      var b = this.headers.next().css("display", "").removeAttr("role").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active");
      (a.autoHeight || a.fillHeight) && b.css("height", "");
      return this
    },
    _setOption: function (e, b) {
      a.Widget.prototype._setOption.apply(this, arguments);
      e == "active" && this.activate(b);
      e == "icons" && (this._destroyIcons(), b && this._createIcons())
    },
    _keydown: function (e) {
      var b = a.ui.keyCode;
      if (!this.options.disabled && !e.altKey && !e.ctrlKey) {
        var c = this.headers.length,
          d = this.headers.index(e.target),
          f = false;
        switch (e.keyCode) {
        case b.RIGHT:
        case b.DOWN:
          f = this.headers[(d + 1) % c];
          break;
        case b.LEFT:
        case b.UP:
          f = this.headers[(d - 1 + c) % c];
          break;
        case b.SPACE:
        case b.ENTER:
          this._clickHandler({
            target: e.target
          }, e.target), e.preventDefault()
        }
        return f ? (a(e.target).attr("tabIndex", "-1"), a(f).attr("tabIndex", "0"), f.focus(), false) : true
      }
    },
    resize: function () {
      var e = this.options,
        b;
      if (e.fillSpace) {
        if (a.browser.msie) {
          var c = this.element.parent().css("overflow");
          this.element.parent().css("overflow", "hidden")
        }
        b = this.element.parent().height();
        a.browser.msie && this.element.parent().css("overflow", c);
        this.headers.each(function () {
          b -= a(this).outerHeight(true)
        });
        this.headers.next().each(function () {
          a(this).height(Math.max(0, b - a(this).innerHeight() + a(this).height()))
        }).css("overflow", "auto")
      } else e.autoHeight && (b = 0, this.headers.next().each(function () {
        b = Math.max(b, a(this).height())
      }).height(b));
      return this
    },
    activate: function (a) {
      this.options.active = a;
      a = this._findActive(a)[0];
      this._clickHandler({
        target: a
      }, a);
      return this
    },
    _findActive: function (e) {
      return e ? typeof e == "number" ? this.headers.filter(":eq(" + e + ")") : this.headers.not(this.headers.not(e)) : e === false ? a([]) : this.headers.filter(":eq(0)")
    },
    _clickHandler: function (e, b) {
      var c = this.options;
      if (!c.disabled) if (e.target) {
        if (e = a(e.currentTarget || b), b = e[0] == this.active[0], c.active = c.collapsible && b ? false : a(".ui-accordion-header", this.element).index(e), !(this.running || !c.collapsible && b)) this.active.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").find(".ui-icon").removeClass(c.icons.headerSelected).addClass(c.icons.header), b || (e.removeClass("ui-state-default ui-corner-all").addClass("ui-state-active ui-corner-top").find(".ui-icon").removeClass(c.icons.header).addClass(c.icons.headerSelected), e.next().addClass("ui-accordion-content-active")), g = e.next(), d = this.active.next(), f = {
          options: c,
          newHeader: b && c.collapsible ? a([]) : e,
          oldHeader: this.active,
          newContent: b && c.collapsible ? a([]) : g,
          oldContent: d
        }, c = this.headers.index(this.active[0]) > this.headers.index(e[0]), this.active = b ? a([]) : e, this._toggle(g, d, f, b, c)
      } else if (c.collapsible) {
        this.active.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").find(".ui-icon").removeClass(c.icons.headerSelected).addClass(c.icons.header);
        this.active.next().addClass("ui-accordion-content-active");
        var d = this.active.next(),
          f = {
            options: c,
            newHeader: a([]),
            oldHeader: c.active,
            newContent: a([]),
            oldContent: d
          },
          g = this.active = a([]);
        this._toggle(g, d, f)
      }
    },
    _toggle: function (e, b, c, d, f) {
      var g = this.options,
        h = this;
      this.toShow = e;
      this.toHide = b;
      this.data = c;
      var j = function () {
          if (h) return h._completed.apply(h, arguments)
        };
      this._trigger("changestart", null, this.data);
      this.running = b.size() === 0 ? e.size() : b.size();
      if (g.animated) {
        c = {};
        c = g.collapsible && d ? {
          toShow: a([]),
          toHide: b,
          complete: j,
          down: f,
          autoHeight: g.autoHeight || g.fillSpace
        } : {
          toShow: e,
          toHide: b,
          complete: j,
          down: f,
          autoHeight: g.autoHeight || g.fillSpace
        };
        if (!g.proxied) g.proxied = g.animated;
        if (!g.proxiedDuration) g.proxiedDuration = g.duration;
        g.animated = a.isFunction(g.proxied) ? g.proxied(c) : g.proxied;
        g.duration = a.isFunction(g.proxiedDuration) ? g.proxiedDuration(c) : g.proxiedDuration;
        var d = a.ui.accordion.animations,
          l = g.duration,
          n = g.animated;
        n && !d[n] && !a.easing[n] && (n = "slide");
        d[n] || (d[n] = function (a) {
          this.slide(a, {
            easing: n,
            duration: l || 700
          })
        });
        d[n](c)
      } else g.collapsible && d ? e.toggle() : (b.hide(), e.show()), j(true);
      b.prev().attr("aria-expanded", "false").attr("tabIndex", "-1").blur();
      e.prev().attr("aria-expanded", "true").attr("tabIndex", "0").focus()
    },
    _completed: function (a) {
      var b = this.options;
      this.running = a ? 0 : --this.running;
      this.running || (b.clearStyle && this.toShow.add(this.toHide).css({
        height: "",
        overflow: ""
      }), this.toHide.removeClass("ui-accordion-content-active"), this._trigger("change", null, this.data))
    }
  });
  a.extend(a.ui.accordion, {
    version: "1.8.2",
    animations: {
      slide: function (e, b) {
        e = a.extend({
          easing: "swing",
          duration: 300
        }, e, b);
        if (e.toHide.size()) if (e.toShow.size()) {
          var c = e.toShow.css("overflow"),
            d = 0,
            f = {},
            g = {},
            h, b = e.toShow;
          h = b[0].style.width;
          b.width(parseInt(b.parent().width(), 10) - parseInt(b.css("paddingLeft"), 10) - parseInt(b.css("paddingRight"), 10) - (parseInt(b.css("borderLeftWidth"), 10) || 0) - (parseInt(b.css("borderRightWidth"), 10) || 0));
          a.each(["height", "paddingTop", "paddingBottom"], function (b, c) {
            g[c] = "hide";
            b = ("" + a.css(e.toShow[0], c)).match(/^([\d+-.]+)(.*)$/);
            f[c] = {
              value: b[1],
              unit: b[2] || "px"
            }
          });
          e.toShow.css({
            height: 0,
            overflow: "hidden"
          }).show();
          e.toHide.filter(":hidden").each(e.complete).end().filter(":visible").animate(g, {
            step: function (a, b) {
              b.prop == "height" && (d = b.end - b.start === 0 ? 0 : (b.now - b.start) / (b.end - b.start));
              e.toShow[0].style[b.prop] = d * f[b.prop].value + f[b.prop].unit
            },
            duration: e.duration,
            easing: e.easing,
            complete: function () {
              e.autoHeight || e.toShow.css("height", "");
              e.toShow.css("width", h);
              e.toShow.css({
                overflow: c
              });
              e.complete()
            }
          })
        } else e.toHide.animate({
          height: "hide"
        }, e);
        else e.toShow.animate({
          height: "show"
        }, e)
      },
      bounceslide: function (a) {
        this.slide(a, {
          easing: a.down ? "easeOutBounce" : "swing",
          duration: a.down ? 1E3 : 200
        })
      }
    }
  })
})(jQuery);
(function (a, e) {
  var b = 0,
    c = 0;
  a.widget("ui.tabs", {
    options: {
      add: null,
      ajaxOptions: null,
      cache: false,
      cookie: null,
      collapsible: false,
      disable: null,
      disabled: [],
      enable: null,
      event: "click",
      fx: null,
      idPrefix: "ui-tabs-",
      load: null,
      panelTemplate: "<div></div>",
      remove: null,
      select: null,
      show: null,
      spinner: "<em>Loading&#8230;</em>",
      tabTemplate: "<li><a href='#{href}'><span>#{label}</span></a></li>"
    },
    _create: function () {
      this._tabify(true)
    },
    _setOption: function (a, b) {
      a == "selected" ? this.options.collapsible && b == this.options.selected || this.select(b) : (this.options[a] = b, this._tabify())
    },
    _tabId: function (a) {
      return a.title && a.title.replace(/\s/g, "_").replace(/[^\w\u00c0-\uFFFF-]/g, "") || this.options.idPrefix + ++b
    },
    _sanitizeSelector: function (a) {
      return a.replace(/:/g, "\\:")
    },
    _cookie: function () {
      var b = this.cookie || (this.cookie = this.options.cookie.name || "ui-tabs-" + ++c);
      return a.cookie.apply(null, [b].concat(a.makeArray(arguments)))
    },
    _ui: function (a, b) {
      return {
        tab: a,
        panel: b,
        index: this.anchors.index(a)
      }
    },
    _cleanup: function () {
      this.lis.filter(".ui-state-processing").removeClass("ui-state-processing").find("span:data(label.tabs)").each(function () {
        var b = a(this);
        b.html(b.data("label.tabs")).removeData("label.tabs")
      })
    },
    _tabify: function (b) {
      function c(b, d) {
        b.css("display", "");
        !a.support.opacity && d.opacity && b[0].style.removeAttribute("filter")
      }
      var g = this,
        h = this.options,
        j = /^#.+/;
      this.list = this.element.find("ol,ul").eq(0);
      this.lis = a(" > li:has(a[href])", this.list);
      this.anchors = this.lis.map(function () {
        return a("a", this)[0]
      });
      this.panels = a([]);
      this.anchors.each(function (b, c) {
        var d = a(c).attr("href"),
          e = d.split("#")[0],
          f;
        if (e && (e === location.toString().split("#")[0] || (f = a("base")[0]) && e === f.href)) d = c.hash, c.href = d;
        j.test(d) ? g.panels = g.panels.add(g.element.find(g._sanitizeSelector(d))) : d && d !== "#" ? (a.data(c, "href.tabs", d), a.data(c, "load.tabs", d.replace(/#.*$/, "")), d = g._tabId(c), c.href = "#" + d, c = g.element.find("#" + d), c.length || (c = a(h.panelTemplate).attr("id", d).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").insertAfter(g.panels[b - 1] || g.list), c.data("destroy.tabs", true)), g.panels = g.panels.add(c)) : h.disabled.push(b)
      });
      if (b) {
        this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all");
        this.list.addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");
        this.lis.addClass("ui-state-default ui-corner-top");
        this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom");
        if (h.selected === e) {
          location.hash && this.anchors.each(function (a, b) {
            if (b.hash == location.hash) return h.selected = a, false
          });
          if (typeof h.selected !== "number" && h.cookie) h.selected = parseInt(g._cookie(), 10);
          if (typeof h.selected !== "number" && this.lis.filter(".ui-tabs-selected").length) h.selected = this.lis.index(this.lis.filter(".ui-tabs-selected"));
          h.selected = h.selected || (this.lis.length ? 0 : -1)
        } else if (h.selected === null) h.selected = -1;
        h.selected = h.selected >= 0 && this.anchors[h.selected] || h.selected < 0 ? h.selected : 0;
        h.disabled = a.unique(h.disabled.concat(a.map(this.lis.filter(".ui-state-disabled"), function (a) {
          return g.lis.index(a)
        }))).sort();
        a.inArray(h.selected, h.disabled) != -1 && h.disabled.splice(a.inArray(h.selected, h.disabled), 1);
        this.panels.addClass("ui-tabs-hide");
        this.lis.removeClass("ui-tabs-selected ui-state-active");
        h.selected >= 0 && this.anchors.length && (g.element.find(g._sanitizeSelector(g.anchors[h.selected].hash)).removeClass("ui-tabs-hide"), this.lis.eq(h.selected).addClass("ui-tabs-selected ui-state-active"), g.element.queue("tabs", function () {
          g._trigger("show", null, g._ui(g.anchors[h.selected], g.element.find(g._sanitizeSelector(g.anchors[h.selected].hash))[0]))
        }), this.load(h.selected));
        a(window).bind("unload", function () {
          g.lis.add(g.anchors).unbind(".tabs");
          g.lis = g.anchors = g.panels = null
        })
      } else h.selected = this.lis.index(this.lis.filter(".ui-tabs-selected"));
      this.element[h.collapsible ? "addClass" : "removeClass"]("ui-tabs-collapsible");
      h.cookie && this._cookie(h.selected, h.cookie);
      for (var b = 0, l; l = this.lis[b]; b++) a(l)[a.inArray(b, h.disabled) != -1 && !a(l).hasClass("ui-tabs-selected") ? "addClass" : "removeClass"]("ui-state-disabled");
      h.cache === false && this.anchors.removeData("cache.tabs");
      this.lis.add(this.anchors).unbind(".tabs");
      if (h.event !== "mouseover") {
        var n = function (a, b) {
            b.is(":not(.ui-state-disabled)") && b.addClass("ui-state-" + a)
          };
        this.lis.bind("mouseover.tabs", function () {
          n("hover", a(this))
        });
        this.lis.bind("mouseout.tabs", function () {
          a(this).removeClass("ui-state-hover")
        });
        this.anchors.bind("focus.tabs", function () {
          n("focus", a(this).closest("li"))
        });
        this.anchors.bind("blur.tabs", function () {
          a(this).closest("li").removeClass("ui-state-focus")
        })
      }
      var m, p;
      if (h.fx) a.isArray(h.fx) ? (m = h.fx[0], p = h.fx[1]) : m = p = h.fx;
      var o = p ?
      function (b, d) {
        a(b).closest("li").addClass("ui-tabs-selected ui-state-active");
        d.hide().removeClass("ui-tabs-hide").animate(p, p.duration || "normal", function () {
          c(d, p);
          g._trigger("show", null, g._ui(b, d[0]))
        })
      } : function (b, c) {
        a(b).closest("li").addClass("ui-tabs-selected ui-state-active");
        c.removeClass("ui-tabs-hide");
        g._trigger("show", null, g._ui(b, c[0]))
      }, u = m ?
      function (a, b) {
        b.animate(m, m.duration || "normal", function () {
          g.lis.removeClass("ui-tabs-selected ui-state-active");
          b.addClass("ui-tabs-hide");
          c(b, m);
          g.element.dequeue("tabs")
        })
      } : function (a, b) {
        g.lis.removeClass("ui-tabs-selected ui-state-active");
        b.addClass("ui-tabs-hide");
        g.element.dequeue("tabs")
      };
      this.anchors.bind(h.event + ".tabs", function () {
        var b = this,
          c = a(b).closest("li"),
          d = g.panels.filter(":not(.ui-tabs-hide)"),
          e = g.element.find(g._sanitizeSelector(b.hash));
        if (c.hasClass("ui-tabs-selected") && !h.collapsible || c.hasClass("ui-state-disabled") || c.hasClass("ui-state-processing") || g.panels.filter(":animated").length || g._trigger("select", null, g._ui(this, e[0])) === false) return this.blur(), false;
        h.selected = g.anchors.index(this);
        g.abort();
        if (h.collapsible) if (c.hasClass("ui-tabs-selected")) return h.selected = -1, h.cookie && g._cookie(h.selected, h.cookie), g.element.queue("tabs", function () {
          u(b, d)
        }).dequeue("tabs"), this.blur(), false;
        else if (!d.length) return h.cookie && g._cookie(h.selected, h.cookie), g.element.queue("tabs", function () {
          o(b, e)
        }), g.load(g.anchors.index(this)), this.blur(), false;
        h.cookie && g._cookie(h.selected, h.cookie);
        if (e.length) d.length && g.element.queue("tabs", function () {
          u(b, d)
        }), g.element.queue("tabs", function () {
          o(b, e)
        }), g.load(g.anchors.index(this));
        else throw "jQuery UI Tabs: Mismatching fragment identifier.";
        a.browser.msie && this.blur()
      });
      this.anchors.bind("click.tabs", function () {
        return false
      })
    },
    _getIndex: function (a) {
      typeof a == "string" && (a = this.anchors.index(this.anchors.filter("[href$=" + a + "]")));
      return a
    },
    destroy: function () {
      var b = this.options;
      this.abort();
      this.element.unbind(".tabs").removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible").removeData("tabs");
      this.list.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");
      this.anchors.each(function () {
        var b = a.data(this, "href.tabs");
        if (b) this.href = b;
        var c = a(this).unbind(".tabs");
        a.each(["href", "load", "cache"], function (a, b) {
          c.removeData(b + ".tabs")
        })
      });
      this.lis.unbind(".tabs").add(this.panels).each(function () {
        a.data(this, "destroy.tabs") ? a(this).remove() : a(this).removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active ui-state-hover ui-state-focus ui-state-disabled ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide")
      });
      b.cookie && this._cookie(null, b.cookie);
      return this
    },
    add: function (b, c, g) {
      if (g === e) g = this.anchors.length;
      var h = this,
        j = this.options,
        c = a(j.tabTemplate.replace(/#\{href\}/g, b).replace(/#\{label\}/g, c)),
        b = !b.indexOf("#") ? b.replace("#", "") : this._tabId(a("a", c)[0]);
      c.addClass("ui-state-default ui-corner-top").data("destroy.tabs", true);
      var l = h.element.find("#" + b);
      l.length || (l = a(j.panelTemplate).attr("id", b).data("destroy.tabs", true));
      l.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide");
      g >= this.lis.length ? (c.appendTo(this.list), l.appendTo(this.list[0].parentNode)) : (c.insertBefore(this.lis[g]), l.insertBefore(this.panels[g]));
      j.disabled = a.map(j.disabled, function (a) {
        return a >= g ? ++a : a
      });
      this._tabify();
      if (this.anchors.length == 1) j.selected = 0, c.addClass("ui-tabs-selected ui-state-active"), l.removeClass("ui-tabs-hide"), this.element.queue("tabs", function () {
        h._trigger("show", null, h._ui(h.anchors[0], h.panels[0]))
      }), this.load(0);
      this._trigger("add", null, this._ui(this.anchors[g], this.panels[g]));
      return this
    },
    remove: function (b) {
      var b = this._getIndex(b),
        c = this.options,
        e = this.lis.eq(b).remove(),
        h = this.panels.eq(b).remove();
      e.hasClass("ui-tabs-selected") && this.anchors.length > 1 && this.select(b + (b + 1 < this.anchors.length ? 1 : -1));
      c.disabled = a.map(a.grep(c.disabled, function (a) {
        return a != b
      }), function (a) {
        return a >= b ? --a : a
      });
      this._tabify();
      this._trigger("remove", null, this._ui(e.find("a")[0], h[0]));
      return this
    },
    enable: function (b) {
      var b = this._getIndex(b),
        c = this.options;
      if (a.inArray(b, c.disabled) != -1) return this.lis.eq(b).removeClass("ui-state-disabled"), c.disabled = a.grep(c.disabled, function (a) {
        return a != b
      }), this._trigger("enable", null, this._ui(this.anchors[b], this.panels[b])), this
    },
    disable: function (a) {
      var a = this._getIndex(a),
        b = this.options;
      a != b.selected && (this.lis.eq(a).addClass("ui-state-disabled"), b.disabled.push(a), b.disabled.sort(), this._trigger("disable", null, this._ui(this.anchors[a], this.panels[a])));
      return this
    },
    select: function (a) {
      a = this._getIndex(a);
      if (a == -1) if (this.options.collapsible && this.options.selected != -1) a = this.options.selected;
      else return this;
      this.anchors.eq(a).trigger(this.options.event + ".tabs");
      return this
    },
    load: function (b) {
      var b = this._getIndex(b),
        c = this,
        e = this.options,
        h = this.anchors.eq(b)[0],
        j = a.data(h, "load.tabs");
      this.abort();
      if (!j || this.element.queue("tabs").length !== 0 && a.data(h, "cache.tabs")) this.element.dequeue("tabs");
      else {
        this.lis.eq(b).addClass("ui-state-processing");
        if (e.spinner) {
          var l = a("span", h);
          l.data("label.tabs", l.html()).html(e.spinner)
        }
        this.xhr = a.ajax(a.extend({}, e.ajaxOptions, {
          url: j,
          success: function (j, l) {
            c.element.find(c._sanitizeSelector(h.hash)).html(j);
            c._cleanup();
            e.cache && a.data(h, "cache.tabs", true);
            c._trigger("load", null, c._ui(c.anchors[b], c.panels[b]));
            try {
              e.ajaxOptions.success(j, l)
            } catch (p) {}
          },
          error: function (a, j) {
            c._cleanup();
            c._trigger("load", null, c._ui(c.anchors[b], c.panels[b]));
            try {
              e.ajaxOptions.error(a, j, b, h)
            } catch (l) {}
          }
        }));
        c.element.dequeue("tabs");
        return this
      }
    },
    abort: function () {
      this.element.queue([]);
      this.panels.stop(false, true);
      this.element.queue("tabs", this.element.queue("tabs").splice(-2, 2));
      this.xhr && (this.xhr.abort(), delete this.xhr);
      this._cleanup();
      return this
    },
    url: function (a, b) {
      this.anchors.eq(a).removeData("cache.tabs").data("load.tabs", b);
      return this
    },
    length: function () {
      return this.anchors.length
    }
  });
  a.extend(a.ui.tabs, {
    version: "1.8.16"
  });
  a.extend(a.ui.tabs.prototype, {
    rotation: null,
    rotate: function (a, b) {
      var c = this,
        e = this.options,
        j = c._rotate || (c._rotate = function (b) {
          clearTimeout(c.rotation);
          c.rotation = setTimeout(function () {
            var a = e.selected;
            c.select(++a < c.anchors.length ? a : 0)
          }, a);
          b && b.stopPropagation()
        }),
        b = c._unrotate || (c._unrotate = !b ?
        function (a) {
          a.clientX && c.rotate(null)
        } : function () {
          t = e.selected;
          j()
        });
      a ? (this.element.bind("tabsshow", j), this.anchors.bind(e.event + ".tabs", b), j()) : (clearTimeout(c.rotation), this.element.unbind("tabsshow", j), this.anchors.unbind(e.event + ".tabs", b), delete this._rotate, delete this._unrotate);
      return this
    }
  })
})(jQuery);
(function (a) {
  a.fn.placeholder = function () {
    function e() {
      var b = a(this);
      b.hasClass("placeholder") && b.val() === b.attr("placeholder") && b.removeClass("placeholder").val("")
    }
    function b() {
      var b = a(this);
      (b.val() === "" || b.val() === b.attr("placeholder")) && b.addClass("placeholder").val(b.attr("placeholder"))
    }
    function c() {
      a(this).find(".placeholder").each(function () {
        a(this).removeClass("placeholder").val("")
      })
    }
    return "placeholder" in document.createElement("input") ? this : this.each(function () {
      a(this).unbind(".ph").bind({
        "focus.ph": e,
        "blur.ph": b
      }).trigger("blur.ph").closest("form").unbind(".ph").bind("submit.ph clear-placeholders", c)
    })
  }
})(jQuery);
(function (a) {
  a.toJSON = function (b) {
    if (typeof JSON == "object" && JSON.stringify) return JSON.stringify(b);
    var d = typeof b;
    if (b === null) return "null";
    if (d != "undefined") {
      if (d == "number" || d == "boolean") return b + "";
      if (d == "string") return a.quoteString(b);
      if (d == "object") {
        if (typeof b.toJSON == "function") return a.toJSON(b.toJSON());
        if (b.constructor === Date) {
          var e = b.getUTCMonth() + 1;
          e < 10 && (e = "0" + e);
          var g = b.getUTCDate();
          g < 10 && (g = "0" + g);
          var d = b.getUTCFullYear(),
            h = b.getUTCHours();
          h < 10 && (h = "0" + h);
          var j = b.getUTCMinutes();
          j < 10 && (j = "0" + j);
          var l = b.getUTCSeconds();
          l < 10 && (l = "0" + l);
          b = b.getUTCMilliseconds();
          b < 100 && (b = "0" + b);
          b < 10 && (b = "0" + b);
          return '"' + d + "-" + e + "-" + g + "T" + h + ":" + j + ":" + l + "." + b + 'Z"'
        }
        if (b.constructor === Array) {
          e = [];
          for (g = 0; g < b.length; g++) e.push(a.toJSON(b[g]) || "null");
          return "[" + e.join(",") + "]"
        }
        e = [];
        for (g in b) {
          d = typeof g;
          if (d == "number") d = '"' + g + '"';
          else if (d == "string") d = a.quoteString(g);
          else continue;
          typeof b[g] != "function" && (h = a.toJSON(b[g]), e.push(d + ":" + h))
        }
        return "{" + e.join(", ") + "}"
      }
    }
  };
  a.evalJSON = function (a) {
    return typeof JSON == "object" && JSON.parse ? JSON.parse(a) : eval("(" + a + ")")
  };
  a.secureEvalJSON = function (a) {
    if (typeof JSON == "object" && JSON.parse) return JSON.parse(a);
    var b;
    b = a.replace(/\\["\\\/bfnrtu]/g, "@");
    b = b.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]");
    b = b.replace(/(?:^|:|,)(?:\s*\[)+/g, "");
    if (/^[\],:{}\s]*$/.test(b)) return eval("(" + a + ")");
    else throw new SyntaxError("Error parsing JSON, source is not valid.");
  };
  a.quoteString = function (a) {
    return a.match(e) ? '"' + a.replace(e, function (a) {
      var c = b[a];
      if (typeof c === "string") return c;
      c = a.charCodeAt();
      return "\\u00" + Math.floor(c / 16).toString(16) + (c % 16).toString(16)
    }) + '"' : '"' + a + '"'
  };
  var e = /["\\\x00-\x1f\x7f-\x9f]/g,
    b = {
      "\u0008": "\\b",
      "\t": "\\t",
      "\n": "\\n",
      "\u000c": "\\f",
      "\r": "\\r",
      '"': '\\"',
      "\\": "\\\\"
    }
})(jQuery);
(function (a) {
  function e() {
    if (!a.ajaxQueue.currentRequest) a.ajaxQueue.currentRequest = a(document).queue("ajax").shift(), a.ajaxQueue.currentRequest && a.ajaxQueue.currentRequest()
  }
  var b = function (b, c, g) {
      var h = g.complete || a.noop,
        j = a.extend(g || {}, {
          url: b,
          type: c,
          complete: function () {
            h(arguments);
            a.ajaxQueue.currentRequest = null;
            e()
          }
        });
      return function () {
        if (a.isFunction(g.data)) j.data = g.data();
        a.ajax(j)
      }
    },
    c = function (b) {
      a(document).queue("ajax", b);
      e()
    };
  a.ajaxQueue = {
    currentRequest: null,
    length: function () {
      return a(document).queue("ajax").length
    },
    reset: function () {
      a(document).clearQueue("ajax")
    },
    add: function (b) {
      c(function () {
        b();
        a.ajaxQueue.currentRequest = null;
        e()
      })
    },
    post: function (a, e) {
      c(new b(a, "POST", e))
    },
    get: function (a, e) {
      c(new b(a, "GET", e))
    },
    put: function (a, e) {
      c(new b(a, "PUT", e))
    },
    del: function (a, e) {
      c(new b(a, "DELETE", e))
    }
  }
})(jQuery);
jQuery(function (a) {
  a.fn.quicksearch = function (e) {
    function b(a) {
      return a === null || a === void 0 || a === false ? true : false
    }
    function c(b) {
      b = b.replace(RegExp(/\<[^\<]+\>/g), "");
      return b = a.trim(b.toLowerCase().replace(/\n/, "").replace(/\s{2,}/, " "))
    }
    function d() {
      var b = c(a('input[rel="' + o.randomElement + '"]').val());
      return b.indexOf(" ") === -1 ? b : b.split(" ")
    }
    function f(b) {
      return o.hideElement === "grandparent" ? a(b).parent().parent() : o.hideElement === "parent" ? a(b).parent() : a(b)
    }
    function g(b) {
      if (D) {
        var c = 0;
        f(b).filter(":visible").each(function () {
          for (var b = 0; b < s; b++) c === b ? a(this).addClass(o.stripeRowClass[c]) : a(this).removeClass(o.stripeRowClass[b]);
          c = (c + 1) % s
        })
      }
    }
    function h(b) {
      a(b).find("td").each(function () {
        a(this).attr("width", parseInt(a(this).css("width")))
      })
    }
    function j(b) {
      if (o.loaderId) {
        var c = a('input[rel="' + o.randomElement + '"]').parent().find(".loader");
        b === "hide" ? c.hide() : c.show()
      }
    }
    function l() {
      var a = !o.isFieldset ? "form" : "fieldset",
        c = "<" + a + ' action="#" id="' + o.formId + '" class="quicksearch">' + (!b(o.labelText) ? '<label for="' + o.randomElement + '" class="' + o.labelClass + '">' + o.labelText + "</label> " : "") + ('<input type="text" value="' + (!b(o.inputText) ? o.inputText : "") + '" rel="' + o.randomElement + '" class="' + o.inputClass + " " + (o.withReset ? o.inputWithResetClass : "") + '" id="' + o.randomElement + '" /> ') + (o.withReset ? '<img class="qs_reset hidden" src="images/nano/x.png" rel="' + o.randomElement + '" title="' + o.resetText + '"/>' : ""),
        d;
      d = b(o.loaderImg) ? '<span id="' + o.loaderId + '" class="' + o.loaderClass + '">' + o.loaderText + "</span>" : '<img src="' + o.loaderImg + '" alt="Loading" id="' + o.loaderId + '" class="' + o.loaderClass + '" />';
      return c + d + "</" + a + ">"
    }
    function n() {
      a('input[rel="' + o.randomElement + '"]').focus(function () {
        a(this).val() === o.inputText && a(this).val("")
      });
      a('input[rel="' + o.randomElement + '"]').blur(function () {
        a(this).val() === "" && a(this).val(o.inputText)
      })
    }
    function m(b) {
      return a(b).map(function () {
        var b = c(this.innerHTML);
        a(this).children("input[type='hidden']").each(function () {
          b = b + " " + c(a(this).val())
        });
        return b
      })
    }
    function p() {
      clearTimeout(u);
      u = setTimeout(function () {
        j("show");
        setTimeout(function () {
          o.onBefore();
          var b = d(),
            c = typeof b,
            b = o.filter(b);
          b != "" ? (typeof w[b] === "undefined" && (w[b] = [], z.each(function (a) {
            var d;
            if (c === "string") d = z[a].indexOf(b) > -1;
            else a: {
              d = b;
              for (var e = z[a], f = 0; f < d.length; f++) if (e.indexOf(d[f]) === -1) {
                d = false;
                break a
              }
              d = true
            }
            d && (w[b][a] = true)
          })), w[b].length === 0 ? f(C).hide() : a(C).each(function (a) {
            w[b][a] ? f(this).show() : f(this).hide()
          })) : f(C).show();
          g(C)
        }, o.delay / 2);
        setTimeout(function () {
          j("hide")
        }, o.delay / 2);
        o.onAfter()
      }, o.delay / 2)
    }
    var o = a.extend({
      position: "prepend",
      attached: "body",
      formId: "quicksearch",
      labelText: "Quick Search",
      labelClass: "qs_label",
      inputText: null,
      inputClass: "qs_input",
      inputWithResetClass: "qs_input_with_reset",
      loaderId: "loader",
      loaderClass: "loader",
      loaderImg: null,
      loaderText: "Loading...",
      stripeRowClass: null,
      hideElement: null,
      delay: 500,
      focusOnLoad: false,
      onBefore: function () {},
      onAfter: function () {},
      filter: function (a) {
        return a
      },
      randomElement: "qs" + Math.floor(Math.random() * 1E6),
      isFieldset: false,
      fixWidths: false,
      withReset: false,
      resetText: "Clear search"
    }, e),
      u, w = {},
      s = !b(o.stripeRowClass) ? o.stripeRowClass.length : 0,
      D = s > 0 ? true : false,
      C = this,
      z, F = a(this).selector;
    a.fn.extend({
      reset_cache: function () {
        C = a(F);
        z = m(C)
      }
    });
    (function () {
      var b = o.position,
        c = o.attached;
      b === "before" ? a(c).before(l()) : b === "prepend" ? a(c).prepend(l()) : b === "append" ? a(c).append(l()) : a(c).after(l())
    })();
    o.fixWidths && h(C);
    o.focusOnLoad && a('input[rel="' + o.randomElement + '"]').get(0).focus();
    o.inputText != "" && o.inputText != null && n();
    z = m(C);
    g(C);
    j("hide");
    a('input[rel="' + o.randomElement + '"]').keydown(function (a) {
      a = a.keyCode;
      a === 9 || a === 13 || a === 16 || a === 17 || a === 18 || a === 38 || a === 40 || a === 224 || p()
    });
    a('input[rel="' + o.randomElement + '"]').keyup(function () {
      var b = a('input[rel="' + o.randomElement + '"]').val();
      a('img[rel="' + o.randomElement + '"]').toggleClass("hidden", !(b && b.length > 0))
    });
    a('img[rel="' + o.randomElement + '"]').bind("click", function () {
      var b = a('input[rel="' + o.randomElement + '"]');
      b.val("");
      a('img[rel="' + o.randomElement + '"]').addClass("hidden");
      p();
      b.focus()
    });
    a("form.quicksearch, fieldset.quicksearch").submit(function () {
      return false
    });
    return this
  }
});
(function (a) {
  var e = function (a, b) {
      var c, d, e, f;
      e = a & 2147483648;
      f = b & 2147483648;
      c = a & 1073741824;
      d = b & 1073741824;
      a = (a & 1073741823) + (b & 1073741823);
      return c & d ? a ^ 2147483648 ^ e ^ f : c | d ? a & 1073741824 ? a ^ 3221225472 ^ e ^ f : a ^ 1073741824 ^ e ^ f : a ^ e ^ f
    },
    b = function (a, b, c, d, f, g, o) {
      a = e(a, e(e(b & c | ~b & d, f), o));
      return e(a << g | a >>> 32 - g, b)
    },
    c = function (a, b, c, d, f, g, o) {
      a = e(a, e(e(b & d | c & ~d, f), o));
      return e(a << g | a >>> 32 - g, b)
    },
    d = function (a, b, c, d, f, g, o) {
      a = e(a, e(e(b ^ c ^ d, f), o));
      return e(a << g | a >>> 32 - g, b)
    },
    f = function (a, b, c, d, f, g, o) {
      a = e(a, e(e(c ^ (b | ~d), f), o));
      return e(a << g | a >>> 32 - g, b)
    },
    g = function (a) {
      var b = "",
        c = "",
        d;
      for (d = 0; d <= 3; d++) c = a >>> d * 8 & 255, c = "0" + c.toString(16), b += c.substr(c.length - 2, 2);
      return b
    };
  a.extend({
    md5: function (a) {
      var j = [],
        l, n, m, p, o, u, w, s, a = a.replace(/\x0d\x0a/g, "\n"),
        j = "";
      for (l = 0; l < a.length; l++) n = a.charCodeAt(l), n < 128 ? j += String.fromCharCode(n) : (n > 127 && n < 2048 ? j += String.fromCharCode(n >> 6 | 192) : (j += String.fromCharCode(n >> 12 | 224), j += String.fromCharCode(n >> 6 & 63 | 128)), j += String.fromCharCode(n & 63 | 128));
      a = j;
      l = a.length;
      j = l + 8;
      n = ((j - j % 64) / 64 + 1) * 16;
      m = Array(n - 1);
      for (o = p = 0; o < l;) j = (o - o % 4) / 4, p = o % 4 * 8, m[j] |= a.charCodeAt(o) << p, o++;
      m[(o - o % 4) / 4] |= 128 << o % 4 * 8;
      m[n - 2] = l << 3;
      m[n - 1] = l >>> 29;
      j = m;
      o = 1732584193;
      u = 4023233417;
      w = 2562383102;
      s = 271733878;
      for (a = 0; a < j.length; a += 16) l = o, n = u, m = w, p = s, o = b(o, u, w, s, j[a + 0], 7, 3614090360), s = b(s, o, u, w, j[a + 1], 12, 3905402710), w = b(w, s, o, u, j[a + 2], 17, 606105819), u = b(u, w, s, o, j[a + 3], 22, 3250441966), o = b(o, u, w, s, j[a + 4], 7, 4118548399), s = b(s, o, u, w, j[a + 5], 12, 1200080426), w = b(w, s, o, u, j[a + 6], 17, 2821735955), u = b(u, w, s, o, j[a + 7], 22, 4249261313), o = b(o, u, w, s, j[a + 8], 7, 1770035416), s = b(s, o, u, w, j[a + 9], 12, 2336552879), w = b(w, s, o, u, j[a + 10], 17, 4294925233), u = b(u, w, s, o, j[a + 11], 22, 2304563134), o = b(o, u, w, s, j[a + 12], 7, 1804603682), s = b(s, o, u, w, j[a + 13], 12, 4254626195), w = b(w, s, o, u, j[a + 14], 17, 2792965006), u = b(u, w, s, o, j[a + 15], 22, 1236535329), o = c(o, u, w, s, j[a + 1], 5, 4129170786), s = c(s, o, u, w, j[a + 6], 9, 3225465664), w = c(w, s, o, u, j[a + 11], 14, 643717713), u = c(u, w, s, o, j[a + 0], 20, 3921069994), o = c(o, u, w, s, j[a + 5], 5, 3593408605), s = c(s, o, u, w, j[a + 10], 9, 38016083), w = c(w, s, o, u, j[a + 15], 14, 3634488961), u = c(u, w, s, o, j[a + 4], 20, 3889429448), o = c(o, u, w, s, j[a + 9], 5, 568446438), s = c(s, o, u, w, j[a + 14], 9, 3275163606), w = c(w, s, o, u, j[a + 3], 14, 4107603335), u = c(u, w, s, o, j[a + 8], 20, 1163531501), o = c(o, u, w, s, j[a + 13], 5, 2850285829), s = c(s, o, u, w, j[a + 2], 9, 4243563512), w = c(w, s, o, u, j[a + 7], 14, 1735328473), u = c(u, w, s, o, j[a + 12], 20, 2368359562), o = d(o, u, w, s, j[a + 5], 4, 4294588738), s = d(s, o, u, w, j[a + 8], 11, 2272392833), w = d(w, s, o, u, j[a + 11], 16, 1839030562), u = d(u, w, s, o, j[a + 14], 23, 4259657740), o = d(o, u, w, s, j[a + 1], 4, 2763975236), s = d(s, o, u, w, j[a + 4], 11, 1272893353), w = d(w, s, o, u, j[a + 7], 16, 4139469664), u = d(u, w, s, o, j[a + 10], 23, 3200236656), o = d(o, u, w, s, j[a + 13], 4, 681279174), s = d(s, o, u, w, j[a + 0], 11, 3936430074), w = d(w, s, o, u, j[a + 3], 16, 3572445317), u = d(u, w, s, o, j[a + 6], 23, 76029189), o = d(o, u, w, s, j[a + 9], 4, 3654602809), s = d(s, o, u, w, j[a + 12], 11, 3873151461), w = d(w, s, o, u, j[a + 15], 16, 530742520), u = d(u, w, s, o, j[a + 2], 23, 3299628645), o = f(o, u, w, s, j[a + 0], 6, 4096336452), s = f(s, o, u, w, j[a + 7], 10, 1126891415), w = f(w, s, o, u, j[a + 14], 15, 2878612391), u = f(u, w, s, o, j[a + 5], 21, 4237533241), o = f(o, u, w, s, j[a + 12], 6, 1700485571), s = f(s, o, u, w, j[a + 3], 10, 2399980690), w = f(w, s, o, u, j[a + 10], 15, 4293915773), u = f(u, w, s, o, j[a + 1], 21, 2240044497), o = f(o, u, w, s, j[a + 8], 6, 1873313359), s = f(s, o, u, w, j[a + 15], 10, 4264355552), w = f(w, s, o, u, j[a + 6], 15, 2734768916), u = f(u, w, s, o, j[a + 13], 21, 1309151649), o = f(o, u, w, s, j[a + 4], 6, 4149444226), s = f(s, o, u, w, j[a + 11], 10, 3174756917), w = f(w, s, o, u, j[a + 2], 15, 718787259), u = f(u, w, s, o, j[a + 9], 21, 3951481745), o = e(o, l), u = e(u, n), w = e(w, m), s = e(s, p);
      return (g(o) + g(u) + g(w) + g(s)).toLowerCase()
    }
  })
})(jQuery);
(function (a) {
  function e(a) {
    return typeof a == "object" ? a : {
      top: a,
      left: a
    }
  }
  var b = a.scrollTo = function (b, d, e) {
      a(window).scrollTo(b, d, e)
    };
  b.defaults = {
    axis: "xy",
    duration: parseFloat(a.fn.jquery) >= 1.3 ? 0 : 1
  };
  b.window = function () {
    return a(window)._scrollable()
  };
  a.fn._scrollable = function () {
    return this.map(function () {
      if (this.nodeName && a.inArray(this.nodeName.toLowerCase(), ["iframe", "#document", "html", "body"]) == -1) return this;
      var b = (this.contentWindow || this).document || this.ownerDocument || this;
      return a.browser.safari || b.compatMode == "BackCompat" ? b.body : b.documentElement
    })
  };
  a.fn.scrollTo = function (c, d, f) {
    typeof d == "object" && (f = d, d = 0);
    typeof f == "function" && (f = {
      onAfter: f
    });
    c == "max" && (c = 9E9);
    f = a.extend({}, b.defaults, f);
    d = d || f.speed || f.duration;
    f.queue = f.queue && f.axis.length > 1;
    f.queue && (d /= 2);
    f.offset = e(f.offset);
    f.over = e(f.over);
    return this._scrollable().each(function () {
      function g(a) {
        j.animate(m, d, f.easing, a &&
        function () {
          a.call(this, c, f)
        })
      }
      var h = this,
        j = a(h),
        l = c,
        n, m = {},
        p = j.is("html,body");
      switch (typeof l) {
      case "number":
      case "string":
        if (/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(l)) {
          l = e(l);
          break
        }
        l = a(l, this);
      case "object":
        if (l.is || l.style) n = (l = a(l)).offset()
      }
      a.each(f.axis.split(""), function (a, c) {
        var d = c == "x" ? "Left" : "Top",
          e = d.toLowerCase(),
          D = "scroll" + d,
          C = h[D],
          z = b.max(h, c);
        n ? (m[D] = n[e] + (p ? 0 : C - j.offset()[e]), f.margin && (m[D] -= parseInt(l.css("margin" + d)) || 0, m[D] -= parseInt(l.css("border" + d + "Width")) || 0), m[D] += f.offset[e] || 0, f.over[e] && (m[D] += l[c == "x" ? "width" : "height"]() * f.over[e])) : (d = l[e], m[D] = d.slice && d.slice(-1) == "%" ? parseFloat(d) / 100 * z : d);
        /^\d+$/.test(m[D]) && (m[D] = m[D] <= 0 ? 0 : Math.min(m[D], z));
        !a && f.queue && (C != m[D] && g(f.onAfterFirst), delete m[D])
      });
      g(f.onAfter)
    }).end()
  };
  b.max = function (b, d) {
    var e = d == "x" ? "Width" : "Height",
      g = "scroll" + e;
    if (!a(b).is("html,body")) return b[g] - a(b)[e.toLowerCase()]();
    var e = "client" + e,
      h = b.ownerDocument.documentElement,
      j = b.ownerDocument.body;
    return Math.max(h[g], j[g]) - Math.min(h[e], j[e])
  }
})(jQuery);
(function () {
  jQuery.color = {};
  jQuery.color.make = function (a, b, c, d) {
    var f = {};
    f.r = a || 0;
    f.g = b || 0;
    f.b = c || 0;
    f.a = d != null ? d : 1;
    f.add = function (a, b) {
      for (var c = 0; c < a.length; ++c) f[a.charAt(c)] += b;
      return f.normalize()
    };
    f.scale = function (a, b) {
      for (var c = 0; c < a.length; ++c) f[a.charAt(c)] *= b;
      return f.normalize()
    };
    f.toString = function () {
      return f.a >= 1 ? "rgb(" + [f.r, f.g, f.b].join(",") + ")" : "rgba(" + [f.r, f.g, f.b, f.a].join(",") + ")"
    };
    f.normalize = function () {
      function a(b, c, d) {
        return c < b ? b : c > d ? d : c
      }
      f.r = a(0, parseInt(f.r), 255);
      f.g = a(0, parseInt(f.g), 255);
      f.b = a(0, parseInt(f.b), 255);
      f.a = a(0, f.a, 1);
      return f
    };
    f.clone = function () {
      return jQuery.color.make(f.r, f.b, f.g, f.a)
    };
    return f.normalize()
  };
  jQuery.color.extract = function (a, b) {
    var c;
    do {
      c = a.css(b).toLowerCase();
      if (c != "" && c != "transparent") break;
      a = a.parent()
    } while (!jQuery.nodeName(a.get(0), "body"));
    c == "rgba(0, 0, 0, 0)" && (c = "transparent");
    return jQuery.color.parse(c)
  };
  jQuery.color.parse = function (e) {
    var b, c = jQuery.color.make;
    if (b = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(e)) return c(parseInt(b[1], 10), parseInt(b[2], 10), parseInt(b[3], 10));
    if (b = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]+(?:\.[0-9]+)?)\s*\)/.exec(e)) return c(parseInt(b[1], 10), parseInt(b[2], 10), parseInt(b[3], 10), parseFloat(b[4]));
    if (b = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(e)) return c(parseFloat(b[1]) * 2.55, parseFloat(b[2]) * 2.55, parseFloat(b[3]) * 2.55);
    if (b = /rgba\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\s*\)/.exec(e)) return c(parseFloat(b[1]) * 2.55, parseFloat(b[2]) * 2.55, parseFloat(b[3]) * 2.55, parseFloat(b[4]));
    if (b = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(e)) return c(parseInt(b[1], 16), parseInt(b[2], 16), parseInt(b[3], 16));
    if (b = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(e)) return c(parseInt(b[1] + b[1], 16), parseInt(b[2] + b[2], 16), parseInt(b[3] + b[3], 16));
    e = jQuery.trim(e).toLowerCase();
    return e == "transparent" ? c(255, 255, 255, 0) : (b = a[e], c(b[0], b[1], b[2]))
  };
  var a = {
    aqua: [0, 255, 255],
    azure: [240, 255, 255],
    beige: [245, 245, 220],
    black: [0, 0, 0],
    blue: [0, 0, 255],
    brown: [165, 42, 42],
    cyan: [0, 255, 255],
    darkblue: [0, 0, 139],
    darkcyan: [0, 139, 139],
    darkgrey: [169, 169, 169],
    darkgreen: [0, 100, 0],
    darkkhaki: [189, 183, 107],
    darkmagenta: [139, 0, 139],
    darkolivegreen: [85, 107, 47],
    darkorange: [255, 140, 0],
    darkorchid: [153, 50, 204],
    darkred: [139, 0, 0],
    darksalmon: [233, 150, 122],
    darkviolet: [148, 0, 211],
    fuchsia: [255, 0, 255],
    gold: [255, 215, 0],
    green: [0, 128, 0],
    indigo: [75, 0, 130],
    khaki: [240, 230, 140],
    lightblue: [173, 216, 230],
    lightcyan: [224, 255, 255],
    lightgreen: [144, 238, 144],
    lightgrey: [211, 211, 211],
    lightpink: [255, 182, 193],
    lightyellow: [255, 255, 224],
    lime: [0, 255, 0],
    magenta: [255, 0, 255],
    maroon: [128, 0, 0],
    navy: [0, 0, 128],
    olive: [128, 128, 0],
    orange: [255, 165, 0],
    pink: [255, 192, 203],
    purple: [128, 0, 128],
    violet: [128, 0, 128],
    red: [255, 0, 0],
    silver: [192, 192, 192],
    white: [255, 255, 255],
    yellow: [255, 255, 0]
  }
})();
(function (a) {
  function e(c, d, e, g) {
    function h(a, b) {
      for (var b = [aa].concat(b), c = 0; c < a.length; ++c) a[c].apply(this, b)
    }
    function j(b) {
      for (var c = [], d = 0; d < b.length; ++d) {
        var e = a.extend(true, {}, A.series);
        b[d].data ? (e.data = b[d].data, delete b[d].data, a.extend(true, e, b[d]), b[d].data = e.data) : e.data = b[d];
        c.push(e)
      }
      M = c;
      c = M.length;
      d = [];
      e = [];
      for (b = 0; b < M.length; ++b) {
        var f = M[b].color;
        f != null && (--c, typeof f == "number" ? e.push(f) : d.push(a.color.parse(M[b].color)))
      }
      for (b = 0; b < e.length; ++b) c = Math.max(c, e[b] + 1);
      d = [];
      for (b = e = 0; d.length < c;) f = A.colors.length == b ? a.color.make(100, 100, 100) : a.color.parse(A.colors[b]), f.scale("rgb", 1 + (e % 2 == 1 ? -1 : 1) * Math.ceil(e / 2) * 0.2), d.push(f), ++b, b >= A.colors.length && (b = 0, ++e);
      for (b = c = 0; b < M.length; ++b) {
        e = M[b];
        if (e.color == null) e.color = d[c].toString(), ++c;
        else if (typeof e.color == "number") e.color = d[e.color].toString();
        if (e.lines.show == null) {
          var g, f = true;
          for (g in e) if (e[g] && e[g].show) {
            f = false;
            break
          }
          if (f) e.lines.show = true
        }
        e.xaxis = p(W, l(e, "x"));
        e.yaxis = p(X, l(e, "y"))
      }
      o()
    }
    function l(a, b) {
      var c = a[b + "axis"];
      if (typeof c == "object") c = c.n;
      typeof c != "number" && (c = 1);
      return c
    }
    function n(a) {
      var b = {},
        c, d;
      for (c = 0; c < W.length; ++c)(d = W[c]) && d.used && (b["x" + d.n] = d.c2p(a.left));
      for (c = 0; c < X.length; ++c)(d = X[c]) && d.used && (b["y" + d.n] = d.c2p(a.top));
      if (b.x1 !== void 0) b.x = b.x1;
      if (b.y1 !== void 0) b.y = b.y1;
      return b
    }
    function m() {
      var a = [],
        b, c;
      for (b = 0; b < W.length; ++b)(c = W[b]) && c.used && a.push(c);
      for (b = 0; b < X.length; ++b)(c = X[b]) && c.used && a.push(c);
      return a
    }
    function p(b, c) {
      b[c - 1] || (b[c - 1] = {
        n: c,
        direction: b == W ? "x" : "y",
        options: a.extend(true, {}, b == W ? A.xaxis : A.yaxis)
      });
      return b[c - 1]
    }
    function o() {
      function b(a) {
        if (a) a.datamin = d, a.datamax = e, a.used = false
      }
      function c(a, b, d) {
        if (b < a.datamin) a.datamin = b;
        if (d > a.datamax) a.datamax = d
      }
      var d = Number.POSITIVE_INFINITY,
        e = Number.NEGATIVE_INFINITY,
        f, g, j, n, l, k, o, z, p, s;
      for (f = 0; f < W.length; ++f) b(W[f]);
      for (f = 0; f < X.length; ++f) b(X[f]);
      for (f = 0; f < M.length; ++f) l = M[f], l.datapoints = {
        points: []
      }, h(la.processRawData, [l, l.data, l.datapoints]);
      for (f = 0; f < M.length; ++f) {
        l = M[f];
        var q = l.data,
          w = l.datapoints.format;
        if (!w) {
          w = [];
          w.push({
            x: true,
            number: true,
            required: true
          });
          w.push({
            y: true,
            number: true,
            required: true
          });
          if (l.bars.show || l.lines.show && l.lines.fill) if (w.push({
            y: true,
            number: true,
            required: false,
            defaultValue: 0
          }), l.bars.horizontal) delete w[w.length - 1].y, w[w.length - 1].x = true;
          l.datapoints.format = w
        }
        if (l.datapoints.pointsize == null) {
          if (l.datapoints.pointsize == null) l.datapoints.pointsize = w.length;
          o = l.datapoints.pointsize;
          k = l.datapoints.points;
          insertSteps = l.lines.show && l.lines.steps;
          l.xaxis.used = l.yaxis.used = true;
          for (g = j = 0; g < q.length; ++g, j += o) {
            s = q[g];
            var u = s == null;
            if (!u) for (n = 0; n < o; ++n) {
              z = s[n];
              if (p = w[n]) if (p.number && z != null && (z = +z, isNaN(z) && (z = null)), z == null && (p.required && (u = true), p.defaultValue != null)) z = p.defaultValue;
              k[j + n] = z
            }
            if (u) for (n = 0; n < o; ++n) z = k[j + n], z != null && (p = w[n], p.x && c(l.xaxis, z, z), p.y && c(l.yaxis, z, z)), k[j + n] = null;
            else if (insertSteps && j > 0 && k[j - o] != null && k[j - o] != k[j] && k[j - o + 1] != k[j + 1]) {
              for (n = 0; n < o; ++n) k[j + o + n] = k[j + n];
              k[j + 1] = k[j - o + 1];
              j += o
            }
          }
        }
      }
      for (f = 0; f < M.length; ++f) l = M[f], h(la.processDatapoints, [l, l.datapoints]);
      for (f = 0; f < M.length; ++f) {
        l = M[f];
        k = l.datapoints.points;
        o = l.datapoints.pointsize;
        s = j = d;
        u = q = e;
        for (g = 0; g < k.length; g += o) if (k[g] != null) for (n = 0; n < o; ++n) if (z = k[g + n], p = w[n]) p.x && (z < j && (j = z), z > q && (q = z)), p.y && (z < s && (s = z), z > u && (u = z));
        l.bars.show && (g = l.bars.align == "left" ? 0 : -l.bars.barWidth / 2, l.bars.horizontal ? (s += g, u += g + l.bars.barWidth) : (j += g, q += g + l.bars.barWidth));
        c(l.xaxis, j, q);
        c(l.yaxis, s, u)
      }
      a.each(m(), function (a, b) {
        if (b.datamin == d) b.datamin = null;
        if (b.datamax == e) b.datamax = null
      })
    }
    function u(a) {
      function b(a) {
        return a
      }
      var c, d, e = a.options.transform || b,
        f = a.options.inverseTransform;
      a.direction == "x" ? (c = a.scale = Fa / (e(a.max) - e(a.min)), d = e(a.min), a.p2c = e == b ?
      function (a) {
        return (a - d) * c
      } : function (a) {
        return (e(a) - d) * c
      }, a.c2p = f ?
      function (a) {
        return f(d + a / c)
      } : function (a) {
        return d + a / c
      }) : (c = a.scale = za / (e(a.max) - e(a.min)), d = e(a.max), a.p2c = e == b ?
      function (a) {
        return (d - a) * c
      } : function (a) {
        return (d - e(a)) * c
      }, a.c2p = f ?
      function (a) {
        return f(d - a / c)
      } : function (a) {
        return d - a / c
      })
    }
    function w(b) {
      function d(e, f) {
        return a('<div style="position:absolute;top:-10000px;' + f + 'font-size:smaller"><div class="' + b.direction + "Axis " + b.direction + b.n + 'Axis">' + e.join("") + "</div></div>").appendTo(c)
      }
      if (b) {
        var e = b.options,
          f, g = b.ticks || [],
          h = [],
          j, n = e.labelWidth,
          e = e.labelHeight;
        if (b.direction == "x") {
          if (n == null && (n = Math.floor(ua / (g.length > 0 ? g.length : 1))), e == null) {
            h = [];
            for (f = 0; f < g.length; ++f)(j = g[f].label) && h.push('<div class="tickLabel" style="float:left;width:' + n + 'px">' + j + "</div>");
            h.length > 0 && (h.push('<div style="clear:left"></div>'), g = d(h, "width:10000px;"), e = g.height(), g.remove())
          }
        } else if (n == null || e == null) {
          for (f = 0; f < g.length; ++f)(j = g[f].label) && h.push('<div class="tickLabel">' + j + "</div>");
          h.length > 0 && (g = d(h, ""), n == null && (n = g.children().width()), e == null && (e = g.find("div.tickLabel").height()), g.remove())
        }
        n == null && (n = 0);
        e == null && (e = 0);
        b.labelWidth = n;
        b.labelHeight = e
      }
    }
    function s(b) {
      if (b && (b.used || b.labelWidth || b.labelHeight)) {
        var c = b.labelWidth,
          d = b.labelHeight,
          e = b.options.position,
          f = b.options.tickLength,
          g = A.grid.axisMargin,
          h = A.grid.labelMargin,
          j = b.direction == "x" ? W : X,
          n = a.grep(j, function (a) {
            return a && a.options.position == e && (a.labelHeight || a.labelWidth)
          });
        a.inArray(b, n) == n.length - 1 && (g = 0);
        f == null && (f = "full");
        j = a.grep(j, function (a) {
          return a && (a.labelHeight || a.labelWidth)
        });
        j = a.inArray(b, j) == 0;
        !j && f == "full" && (f = 5);
        isNaN(+f) || (h += +f);
        b.direction == "x" ? (d += h, e == "bottom" ? (I.bottom += d + g, b.box = {
          top: ya - I.bottom,
          height: d
        }) : (b.box = {
          top: I.top + g,
          height: d
        }, I.top += d + g)) : (c += h, e == "left" ? (b.box = {
          left: I.left + g,
          width: c
        }, I.left += c + g) : (I.right += c + g, b.box = {
          left: ua - I.right,
          width: c
        }));
        b.position = e;
        b.tickLength = f;
        b.box.padding = h;
        b.innermost = j
      }
    }
    function D() {
      var b = m(),
        d;
      for (d = 0; d < b.length; ++d) {
        var e = b[d],
          f = e.options,
          g = +(f.min != null ? f.min : e.datamin),
          h = +(f.max != null ? f.max : e.datamax),
          j = h - g;
        if (j == 0) {
          if (j = h == 0 ? 1 : 0.01, f.min == null && (g -= j), f.max == null || f.min != null) h += j
        } else {
          var n = f.autoscaleMargin;
          n != null && (f.min == null && (g -= j * n, g < 0 && e.datamin != null && e.datamin >= 0 && (g = 0)), f.max == null && (h += j * n, h > 0 && e.datamax != null && e.datamax <= 0 && (h = 0)))
        }
        e.min = g;
        e.max = h
      }
      I.left = I.right = I.top = I.bottom = 0;
      if (A.grid.show) {
        for (d = 0; d < b.length; ++d) {
          C(b[d]);
          e = b[d];
          e.ticks = [];
          g = e.options.ticks;
          f = null;
          g == null || typeof g == "number" && g > 0 ? f = e.tickGenerator(e) : g && (f = a.isFunction(g) ? g({
            min: e.min,
            max: e.max
          }) : g);
          h = g = void 0;
          for (g = 0; g < f.length; ++g) j = null, n = f[g], typeof n == "object" ? (h = n[0], n.length > 1 && (j = n[1])) : h = n, j == null && (j = e.tickFormatter(h, e)), e.ticks[g] = {
            v: h,
            label: j
          };
          e = b[d];
          f = b[d].ticks;
          if (e.options.autoscaleMargin != null && f.length > 0) {
            if (e.options.min == null) e.min = Math.min(e.min, f[0].v);
            if (e.options.max == null && f.length > 1) e.max = Math.max(e.max, f[f.length - 1].v)
          }
        }
        for (d = 0; d < W.length; ++d) w(W[d]);
        for (d = 0; d < X.length; ++d) w(X[d]);
        for (d = W.length - 1; d >= 0; --d) s(W[d]);
        for (d = X.length - 1; d >= 0; --d) s(X[d]);
        for (e = d = 0; e < M.length; ++e) d = Math.max(d, 2 * (M[e].points.radius + M[e].points.lineWidth / 2));
        for (var l in I) I[l] += A.grid.borderWidth, I[l] = Math.max(d, I[l])
      }
      Fa = ua - I.left - I.right;
      za = ya - I.bottom - I.top;
      for (d = 0; d < b.length; ++d) u(b[d]);
      if (A.grid.show) {
        for (d = 0; d < b.length; ++d) l = b[d], l.direction == "x" ? (l.box.left = I.left, l.box.width = Fa) : (l.box.top = I.top, l.box.height = za);
        c.find(".tickLabels").remove();
        b = ['<div class="tickLabels" style="font-size:smaller">'];
        l = m();
        for (d = 0; d < l.length; ++d) {
          e = l[d];
          f = e.box;
          b.push('<div class="' + e.direction + "Axis " + e.direction + e.n + 'Axis" style="color:' + e.options.color + '">');
          for (g = 0; g < e.ticks.length; ++g) if (h = e.ticks[g], h.label && !(h.v < e.min || h.v > e.max)) {
            j = {};
            e.direction == "x" ? (n = "center", j.left = Math.round(I.left + e.p2c(h.v) - e.labelWidth / 2), e.position == "bottom" ? j.top = f.top + f.padding : j.bottom = ya - (f.top + f.height - f.padding)) : (j.top = Math.round(I.top + e.p2c(h.v) - e.labelHeight / 2), e.position == "left" ? (j.right = ua - (f.left + f.width - f.padding), n = "right") : (j.left = f.left + f.padding, n = "left"));
            j.width = e.labelWidth;
            var n = ["position:absolute", "text-align:" + n],
              k;
            for (k in j) n.push(k + ":" + j[k] + "px");
            b.push('<div class="tickLabel" style="' + n.join(";") + '">' + h.label + "</div>")
          }
          b.push("</div>")
        }
        b.push("</div>");
        c.append(b.join(""))
      }
      c.find(".legend").remove();
      if (A.legend.show) {
        k = [];
        b = false;
        l = A.legend.labelFormatter;
        for (f = 0; f < M.length; ++f) if (d = M[f], e = d.label) f % A.legend.noColumns == 0 && (b && k.push("</tr>"), k.push("<tr>"), b = true), l && (e = l(e, d)), k.push('<td class="legendColorBox"><div style="border:1px solid ' + A.legend.labelBoxBorderColor + ';padding:1px"><div style="width:4px;height:0;border:5px solid ' + d.color + ';overflow:hidden"></div></div></td><td class="legendLabel">' + e + "</td>");
        b && k.push("</tr>");
        if (k.length != 0) if (b = '<table style="font-size:smaller;color:' + A.grid.color + '">' + k.join("") + "</table>", A.legend.container != null) a(A.legend.container).html(b);
        else if (k = "", l = A.legend.position, d = A.legend.margin, d[0] == null && (d = [d, d]), l.charAt(0) == "n" ? k += "top:" + (d[1] + I.top) + "px;" : l.charAt(0) == "s" && (k += "bottom:" + (d[1] + I.bottom) + "px;"), l.charAt(1) == "e" ? k += "right:" + (d[0] + I.right) + "px;" : l.charAt(1) == "w" && (k += "left:" + (d[0] + I.left) + "px;"), b = a('<div class="legend">' + b.replace('style="', 'style="position:absolute;' + k + ";") + "</div>").appendTo(c), A.legend.backgroundOpacity != 0) {
          l = A.legend.backgroundColor;
          if (l == null) l = (l = A.grid.backgroundColor) && typeof l == "string" ? a.color.parse(l) : a.color.extract(b, "background-color"), l.a = 1, l = l.toString();
          d = b.children();
          a('<div style="position:absolute;width:' + d.width() + "px;height:" + d.height() + "px;" + k + "background-color:" + l + ';"> </div>').prependTo(b).css("opacity", A.legend.backgroundOpacity)
        }
      }
    }
    function C(c) {
      var d = c.options,
        e;
      e = typeof d.ticks == "number" && d.ticks > 0 ? d.ticks : c.direction == "x" ? 0.3 * Math.sqrt(ua) : 0.3 * Math.sqrt(ya);
      e = (c.max - c.min) / e;
      var f, g, h, j;
      if (d.mode == "time") {
        var n = {
          second: 1E3,
          minute: 6E4,
          hour: 36E5,
          day: 864E5,
          month: 2592E6,
          year: 525949.2 * 6E4
        };
        j = [
          [1, "second"],
          [2, "second"],
          [5, "second"],
          [10, "second"],
          [30, "second"],
          [1, "minute"],
          [2, "minute"],
          [5, "minute"],
          [10, "minute"],
          [30, "minute"],
          [1, "hour"],
          [2, "hour"],
          [4, "hour"],
          [8, "hour"],
          [12, "hour"],
          [1, "day"],
          [2, "day"],
          [3, "day"],
          [0.25, "month"],
          [0.5, "month"],
          [1, "month"],
          [2, "month"],
          [3, "month"],
          [6, "month"],
          [1, "year"]
        ];
        f = 0;
        d.minTickSize != null && (f = typeof d.tickSize == "number" ? d.tickSize : d.minTickSize[0] * n[d.minTickSize[1]]);
        for (g = 0; g < j.length - 1; ++g) if (e < (j[g][0] * n[j[g][1]] + j[g + 1][0] * n[j[g + 1][1]]) / 2 && j[g][0] * n[j[g][1]] >= f) break;
        f = j[g][0];
        h = j[g][1];
        h == "year" && (g = Math.pow(10, Math.floor(Math.log(e / n.year) / Math.LN10)), j = e / n.year / g, f = j < 1.5 ? 1 : j < 3 ? 2 : j < 7.5 ? 5 : 10, f *= g);
        c.tickSize = d.tickSize || [f, h];
        g = function (a) {
          var c = [],
            d = a.tickSize[0],
            e = a.tickSize[1],
            f = new Date(a.min),
            g = d * n[e];
          e == "second" && f.setUTCSeconds(b(f.getUTCSeconds(), d));
          e == "minute" && f.setUTCMinutes(b(f.getUTCMinutes(), d));
          e == "hour" && f.setUTCHours(b(f.getUTCHours(), d));
          e == "month" && f.setUTCMonth(b(f.getUTCMonth(), d));
          e == "year" && f.setUTCFullYear(b(f.getUTCFullYear(), d));
          f.setUTCMilliseconds(0);
          g >= n.minute && f.setUTCSeconds(0);
          g >= n.hour && f.setUTCMinutes(0);
          g >= n.day && f.setUTCHours(0);
          g >= n.day * 4 && f.setUTCDate(1);
          g >= n.year && f.setUTCMonth(0);
          var h = 0,
            j = Number.NaN,
            l;
          do if (l = j, j = f.getTime(), c.push(j), e == "month") if (d < 1) {
            f.setUTCDate(1);
            var k = f.getTime();
            f.setUTCMonth(f.getUTCMonth() + 1);
            var m = f.getTime();
            f.setTime(j + h * n.hour + (m - k) * d);
            h = f.getUTCHours();
            f.setUTCHours(0)
          } else f.setUTCMonth(f.getUTCMonth() + d);
          else e == "year" ? f.setUTCFullYear(f.getUTCFullYear() + d) : f.setTime(j + g);
          while (j < a.max && j != l);
          return c
        };
        f = function (b, c) {
          var e = new Date(b);
          if (d.timeformat != null) return a.plot.formatDate(e, d.timeformat, d.monthNames);
          var f = c.tickSize[0] * n[c.tickSize[1]],
            g = c.max - c.min,
            h = d.twelveHourClock ? " %p" : "";
          fmt = f < n.minute ? "%h:%M:%S" + h : f < n.day ? g < 2 * n.day ? "%h:%M" + h : "%b %d %h:%M" + h : f < n.month ? "%b %d" : f < n.year ? g < n.year ? "%b" : "%b %y" : "%y";
          return a.plot.formatDate(e, fmt, d.monthNames)
        }
      } else {
        h = d.tickDecimals;
        var l = -Math.floor(Math.log(e) / Math.LN10);
        h != null && l > h && (l = h);
        g = Math.pow(10, -l);
        j = e / g;
        if (j < 1.5) f = 1;
        else if (j < 3) {
          if (f = 2, j > 2.25 && (h == null || l + 1 <= h)) f = 2.5, ++l
        } else f = j < 7.5 ? 5 : 10;
        f *= g;
        if (d.minTickSize != null && f < d.minTickSize) f = d.minTickSize;
        c.tickDecimals = Math.max(0, h != null ? h : l);
        c.tickSize = d.tickSize || f;
        g = function (a) {
          var c = [],
            d = b(a.min, a.tickSize),
            e = 0,
            f = Number.NaN,
            g;
          do g = f, f = d + e * a.tickSize, c.push(f), ++e;
          while (f < a.max && f != g);
          return c
        };
        f = function (a, b) {
          return a.toFixed(b.tickDecimals)
        }
      }
      if (d.alignTicksWithAxis != null) {
        var k = (c.direction == "x" ? W : X)[d.alignTicksWithAxis - 1];
        if (k && k.used && k != c) {
          g = g(c);
          if (g.length > 0) {
            if (d.min == null) c.min = Math.min(c.min, g[0]);
            if (d.max == null && g.length > 1) c.max = Math.max(c.max, g[g.length - 1])
          }
          g = function (a) {
            var b = [],
              c, d;
            for (d = 0; d < k.ticks.length; ++d) c = (k.ticks[d].v - k.min) / (k.max - k.min), c = a.min + c * (a.max - a.min), b.push(c);
            return b
          };
          if (c.mode != "time" && d.tickDecimals == null && (e = Math.max(0, -Math.floor(Math.log(e) / Math.LN10) + 1), j = g(c), !(j.length > 1 && /\..*0$/.test((j[1] - j[0]).toFixed(e))))) c.tickDecimals = e
        }
      }
      c.tickGenerator = g;
      c.tickFormatter = a.isFunction(d.tickFormatter) ?
      function (a, b) {
        return "" + d.tickFormatter(a, b)
      } : f
    }
    function z() {
      E.clearRect(0, 0, ua, ya);
      var a = A.grid;
      a.show && !a.aboveData && q();
      for (var b = 0; b < M.length; ++b) {
        h(la.drawSeries, [E, M[b]]);
        var c = M[b];
        c.lines.show && K(c);
        c.bars.show && cb(c);
        c.points.show && O(c)
      }
      h(la.draw, [E]);
      a.show && a.aboveData && q()
    }
    function F(a, b) {
      var c, d, e, f, g;
      f = m();
      for (i = 0; i < f.length; ++i) if (c = f[i], c.direction == b && (g = b + c.n + "axis", !a[g] && c.n == 1 && (g = b + "axis"), a[g])) {
        d = a[g].from;
        e = a[g].to;
        break
      }
      a[g] || (c = b == "x" ? W[0] : X[0], d = a[b + "1"], e = a[b + "2"]);
      d != null && e != null && d > e && (f = d, d = e, e = f);
      return {
        from: d,
        to: e,
        axis: c
      }
    }
    function q() {
      var b;
      E.save();
      E.translate(I.left, I.top);
      if (A.grid.backgroundColor) E.fillStyle = Y(A.grid.backgroundColor, za, 0, "rgba(255, 255, 255, 0)"), E.fillRect(0, 0, Fa, za);
      var c = A.grid.markings;
      if (c) {
        if (a.isFunction(c)) {
          var d = aa.getAxes();
          d.xmin = d.xaxis.min;
          d.xmax = d.xaxis.max;
          d.ymin = d.yaxis.min;
          d.ymax = d.yaxis.max;
          c = c(d)
        }
        for (b = 0; b < c.length; ++b) {
          var d = c[b],
            e = F(d, "x"),
            f = F(d, "y");
          if (e.from == null) e.from = e.axis.min;
          if (e.to == null) e.to = e.axis.max;
          if (f.from == null) f.from = f.axis.min;
          if (f.to == null) f.to = f.axis.max;
          if (!(e.to < e.axis.min || e.from > e.axis.max || f.to < f.axis.min || f.from > f.axis.max)) if (e.from = Math.max(e.from, e.axis.min), e.to = Math.min(e.to, e.axis.max), f.from = Math.max(f.from, f.axis.min), f.to = Math.min(f.to, f.axis.max), !(e.from == e.to && f.from == f.to)) e.from = e.axis.p2c(e.from), e.to = e.axis.p2c(e.to), f.from = f.axis.p2c(f.from), f.to = f.axis.p2c(f.to), e.from == e.to || f.from == f.to ? (E.beginPath(), E.strokeStyle = d.color || A.grid.markingsColor, E.lineWidth = d.lineWidth || A.grid.markingsLineWidth, E.moveTo(e.from, f.from), E.lineTo(e.to, f.to), E.stroke()) : (E.fillStyle = d.color || A.grid.markingsColor, E.fillRect(e.from, f.to, e.to - e.from, f.from - f.to))
        }
      }
      d = m();
      c = A.grid.borderWidth;
      for (e = 0; e < d.length; ++e) {
        f = d[e];
        b = f.box;
        var g = f.tickLength,
          h, j, n, l;
        E.strokeStyle = f.options.tickColor || a.color.parse(f.options.color).scale("a", 0.22).toString();
        E.lineWidth = 1;
        f.direction == "x" ? (h = 0, j = g == "full" ? f.position == "top" ? 0 : za : b.top - I.top + (f.position == "top" ? b.height : 0)) : (j = 0, h = g == "full" ? f.position == "left" ? 0 : Fa : b.left - I.left + (f.position == "left" ? b.width : 0));
        f.innermost || (E.beginPath(), n = l = 0, f.direction == "x" ? n = Fa : l = za, E.lineWidth == 1 && (h = Math.floor(h) + 0.5, j = Math.floor(j) + 0.5), E.moveTo(h, j), E.lineTo(h + n, j + l), E.stroke());
        E.beginPath();
        for (b = 0; b < f.ticks.length; ++b) {
          var k = f.ticks[b].v;
          n = l = 0;
          k < f.min || k > f.max || g == "full" && c > 0 && (k == f.min || k == f.max) || (f.direction == "x" ? (h = f.p2c(k), l = g == "full" ? -za : g, f.position == "top" && (l = -l)) : (j = f.p2c(k), n = g == "full" ? -Fa : g, f.position == "left" && (n = -n)), E.lineWidth == 1 && (f.direction == "x" ? h = Math.floor(h) + 0.5 : j = Math.floor(j) + 0.5), E.moveTo(h, j), E.lineTo(h + n, j + l))
        }
        E.stroke()
      }
      if (c) E.lineWidth = c, E.strokeStyle = A.grid.borderColor, E.strokeRect(-c / 2, -c / 2, Fa + c, za + c);
      E.restore()
    }
    function K(a) {
      function b(a, c, d, e, f) {
        var g = a.points,
          a = a.pointsize,
          h = null,
          j = null;
        E.beginPath();
        for (var n = a; n < g.length; n += a) {
          var l = g[n - a],
            k = g[n - a + 1],
            m = g[n],
            o = g[n + 1];
          if (!(l == null || m == null)) {
            if (k <= o && k < f.min) {
              if (o < f.min) continue;
              l = (f.min - k) / (o - k) * (m - l) + l;
              k = f.min
            } else if (o <= k && o < f.min) {
              if (k < f.min) continue;
              m = (f.min - k) / (o - k) * (m - l) + l;
              o = f.min
            }
            if (k >= o && k > f.max) {
              if (o > f.max) continue;
              l = (f.max - k) / (o - k) * (m - l) + l;
              k = f.max
            } else if (o >= k && o > f.max) {
              if (k > f.max) continue;
              m = (f.max - k) / (o - k) * (m - l) + l;
              o = f.max
            }
            if (l <= m && l < e.min) {
              if (m < e.min) continue;
              k = (e.min - l) / (m - l) * (o - k) + k;
              l = e.min
            } else if (m <= l && m < e.min) {
              if (l < e.min) continue;
              o = (e.min - l) / (m - l) * (o - k) + k;
              m = e.min
            }
            if (l >= m && l > e.max) {
              if (m > e.max) continue;
              k = (e.max - l) / (m - l) * (o - k) + k;
              l = e.max
            } else if (m >= l && m > e.max) {
              if (l > e.max) continue;
              o = (e.max - l) / (m - l) * (o - k) + k;
              m = e.max
            }(l != h || k != j) && E.moveTo(e.p2c(l) + c, f.p2c(k) + d);
            h = m;
            j = o;
            E.lineTo(e.p2c(m) + c, f.p2c(o) + d)
          }
        }
        E.stroke()
      }
      function c(a, b, d) {
        for (var e = a.points, a = a.pointsize, f = Math.min(Math.max(0, d.min), d.max), g = 0, h = false, j = 1, n = 0, l = 0;;) {
          if (a > 0 && g > e.length + a) break;
          g += a;
          var k = e[g - a],
            m = e[g - a + j],
            o = e[g],
            z = e[g + j];
          if (h) {
            if (a > 0 && k != null && o == null) {
              l = g;
              a = -a;
              j = 2;
              continue
            }
            if (a < 0 && g == n + a) {
              E.fill();
              h = false;
              a = -a;
              j = 1;
              g = n = l + a;
              continue
            }
          }
          if (!(k == null || o == null)) {
            if (k <= o && k < b.min) {
              if (o < b.min) continue;
              m = (b.min - k) / (o - k) * (z - m) + m;
              k = b.min
            } else if (o <= k && o < b.min) {
              if (k < b.min) continue;
              z = (b.min - k) / (o - k) * (z - m) + m;
              o = b.min
            }
            if (k >= o && k > b.max) {
              if (o > b.max) continue;
              m = (b.max - k) / (o - k) * (z - m) + m;
              k = b.max
            } else if (o >= k && o > b.max) {
              if (k > b.max) continue;
              z = (b.max - k) / (o - k) * (z - m) + m;
              o = b.max
            }
            h || (E.beginPath(), E.moveTo(b.p2c(k), d.p2c(f)), h = true);
            if (m >= d.max && z >= d.max) E.lineTo(b.p2c(k), d.p2c(d.max)), E.lineTo(b.p2c(o), d.p2c(d.max));
            else if (m <= d.min && z <= d.min) E.lineTo(b.p2c(k), d.p2c(d.min)), E.lineTo(b.p2c(o), d.p2c(d.min));
            else {
              var p = k,
                s = o;
              if (m <= z && m < d.min && z >= d.min) k = (d.min - m) / (z - m) * (o - k) + k, m = d.min;
              else if (z <= m && z < d.min && m >= d.min) o = (d.min - m) / (z - m) * (o - k) + k, z = d.min;
              if (m >= z && m > d.max && z <= d.max) k = (d.max - m) / (z - m) * (o - k) + k, m = d.max;
              else if (z >= m && z > d.max && m <= d.max) o = (d.max - m) / (z - m) * (o - k) + k, z = d.max;
              k != p && E.lineTo(b.p2c(p), d.p2c(m));
              E.lineTo(b.p2c(k), d.p2c(m));
              E.lineTo(b.p2c(o), d.p2c(z));
              o != s && (E.lineTo(b.p2c(o), d.p2c(z)), E.lineTo(b.p2c(s), d.p2c(z)))
            }
          }
        }
      }
      E.save();
      E.translate(I.left, I.top);
      E.lineJoin = "round";
      var d = a.lines.lineWidth,
        e = a.shadowSize;
      if (d > 0 && e > 0) {
        E.lineWidth = e;
        E.strokeStyle = "rgba(0,0,0,0.1)";
        var f = Math.PI / 18;
        b(a.datapoints, Math.sin(f) * (d / 2 + e / 2), Math.cos(f) * (d / 2 + e / 2), a.xaxis, a.yaxis);
        E.lineWidth = e / 2;
        b(a.datapoints, Math.sin(f) * (d / 2 + e / 4), Math.cos(f) * (d / 2 + e / 4), a.xaxis, a.yaxis)
      }
      E.lineWidth = d;
      E.strokeStyle = a.color;
      if (e = Ha(a.lines, a.color, 0, za)) E.fillStyle = e, c(a.datapoints, a.xaxis, a.yaxis);
      d > 0 && b(a.datapoints, 0, 0, a.xaxis, a.yaxis);
      E.restore()
    }
    function O(a) {
      function b(a, c, d, e, f, g, h) {
        for (var j = a.points, a = a.pointsize, n = 0; n < j.length; n += a) {
          var l = j[n],
            k = j[n + 1];
          if (!(l == null || l < g.min || l > g.max || k < h.min || k > h.max)) {
            E.beginPath();
            E.arc(g.p2c(l), h.p2c(k) + e, c, 0, f, false);
            if (d) E.fillStyle = d, E.fill();
            E.stroke()
          }
        }
      }
      E.save();
      E.translate(I.left, I.top);
      var c = a.lines.lineWidth,
        d = a.shadowSize,
        e = a.points.radius;
      if (c > 0 && d > 0) d /= 2, E.lineWidth = d, E.strokeStyle = "rgba(0,0,0,0.1)", b(a.datapoints, e, null, d + d / 2, Math.PI, a.xaxis, a.yaxis), E.strokeStyle = "rgba(0,0,0,0.2)", b(a.datapoints, e, null, d / 2, Math.PI, a.xaxis, a.yaxis);
      E.lineWidth = c;
      E.strokeStyle = a.color;
      b(a.datapoints, e, Ha(a.points, a.color), 0, 2 * Math.PI, a.xaxis, a.yaxis);
      E.restore()
    }
    function V(a, b, c, d, e, f, g, h, j, n, l, k) {
      var m, o, z, p;
      l ? (p = o = z = true, m = false, l = c, c = b + d, e = b + e, a < l && (b = a, a = l, l = b, m = true, o = false)) : (m = o = z = true, p = false, l = a + d, a += e, e = c, c = b, c < e && (b = c, c = e, e = b, p = true, z = false));
      if (!(a < h.min || l > h.max || c < j.min || e > j.max)) {
        if (l < h.min) l = h.min, m = false;
        if (a > h.max) a = h.max, o = false;
        if (e < j.min) e = j.min, p = false;
        if (c > j.max) c = j.max, z = false;
        l = h.p2c(l);
        e = j.p2c(e);
        a = h.p2c(a);
        c = j.p2c(c);
        if (g) n.beginPath(), n.moveTo(l, e), n.lineTo(l, c), n.lineTo(a, c), n.lineTo(a, e), n.fillStyle = g(e, c), n.fill();
        if (k > 0 && (m || o || z || p)) n.beginPath(), n.moveTo(l, e + f), m ? n.lineTo(l, c + f) : n.moveTo(l, c + f), z ? n.lineTo(a, c + f) : n.moveTo(a, c + f), o ? n.lineTo(a, e + f) : n.moveTo(a, e + f), p ? n.lineTo(l, e + f) : n.moveTo(l, e + f), n.stroke()
      }
    }

    function cb(a) {
      E.save();
      E.translate(I.left, I.top);
      E.lineWidth = a.bars.lineWidth;
      E.strokeStyle = a.color;
      var b = a.bars.align == "left" ? 0 : -a.bars.barWidth / 2;
      (function (b, c, d, e, f, g, h) {
        for (var j = b.points, b = b.pointsize, n = 0; n < j.length; n += b) j[n] != null && V(j[n], j[n + 1], j[n + 2], c, d, e, f, g, h, E, a.bars.horizontal, a.bars.lineWidth)
      })(a.datapoints, b, b + a.bars.barWidth, 0, a.bars.fill ?
      function (b, c) {
        return Ha(a.bars, a.color, b, c)
      } : null, a.xaxis, a.yaxis);
      E.restore()
    }
    function Ha(b, c, d, e) {
      var f = b.fill;
      if (!f) return null;
      if (b.fillColor) return Y(b.fillColor, d, e, c);
      b = a.color.parse(c);
      b.a = typeof f == "number" ? f : 0.4;
      b.normalize();
      return b.toString()
    }
    function H(a) {
      A.grid.hoverable && gb("plothover", a, function (a) {
        return a.hoverable != false
      })
    }
    function sa(a) {
      gb("plotclick", a, function (a) {
        return a.clickable != false
      })
    }
    function gb(a, b, d) {
      var e = kb.offset(),
        f = b.pageX - e.left - I.left,
        g = b.pageY - e.top - I.top,
        h = n({
          left: f,
          top: g
        });
      h.pageX = b.pageX;
      h.pageY = b.pageY;
      var b = A.grid.mouseActiveRadius,
        j = b * b + 1,
        l = null,
        k, m;
      for (k = M.length - 1; k >= 0; --k) if (d(M[k])) {
        var o = M[k],
          z = o.xaxis,
          p = o.yaxis,
          s = o.datapoints.points,
          q = o.datapoints.pointsize,
          w = z.c2p(f),
          u = p.c2p(g),
          C = b / z.scale,
          F = b / p.scale;
        if (o.lines.show || o.points.show) for (m = 0; m < s.length; m += q) {
          var D = s[m],
            V = s[m + 1];
          if (D != null && !(D - w > C || D - w < -C || V - u > F || V - u < -F)) D = Math.abs(z.p2c(D) - f), V = Math.abs(p.p2c(V) - g), V = D * D + V * V, V < j && (j = V, l = [k, m / q])
        }
        if (o.bars.show && !l) {
          z = o.bars.align == "left" ? 0 : -o.bars.barWidth / 2;
          o = z + o.bars.barWidth;
          for (m = 0; m < s.length; m += q) if (D = s[m], V = s[m + 1], p = s[m + 2], D != null && (M[k].bars.horizontal ? w <= Math.max(p, D) && w >= Math.min(p, D) && u >= V + z && u <= V + o : w >= D + z && w <= D + o && u >= Math.min(p, V) && u <= Math.max(p, V))) l = [k, m / q]
        }
      }
      l ? (k = l[0], m = l[1], q = M[k].datapoints.pointsize, d = {
        datapoint: M[k].datapoints.points.slice(m * q, (m + 1) * q),
        dataIndex: m,
        series: M[k],
        seriesIndex: k
      }) : d = null;
      if (d) d.pageX = parseInt(d.series.xaxis.p2c(d.datapoint[0]) + e.left + I.left), d.pageY = parseInt(d.series.yaxis.p2c(d.datapoint[1]) + e.top + I.top);
      if (A.grid.autoHighlight) {
        for (e = 0; e < Ra.length; ++e) f = Ra[e], f.auto == a && (!d || !(f.series == d.series && f.point == d.datapoint)) && fa(f.series, f.point);
        d && Z(d.series, d.datapoint, a)
      }
      c.trigger(a, [h, d])
    }
    function k() {
      hb || (hb = setTimeout(va, 30))
    }
    function va() {
      hb = null;
      ea.save();
      ea.clearRect(0, 0, ua, ya);
      ea.translate(I.left, I.top);
      var b, c;
      for (b = 0; b < Ra.length; ++b) if (c = Ra[b], c.series.bars.show) P(c.series, c.point);
      else {
        var d = c.series,
          e = c.point;
        c = e[0];
        var e = e[1],
          f = d.xaxis,
          g = d.yaxis;
        if (!(c < f.min || c > f.max || e < g.min || e > g.max)) {
          var j = d.points.radius + d.points.lineWidth / 2;
          ea.lineWidth = j;
          ea.strokeStyle = a.color.parse(d.color).scale("a", 0.5).toString();
          d = 1.5 * j;
          ea.beginPath();
          ea.arc(f.p2c(c), g.p2c(e), d, 0, 2 * Math.PI, false);
          ea.stroke()
        }
      }
      ea.restore();
      h(la.drawOverlay, [ea])
    }
    function Z(a, b, c) {
      typeof a == "number" && (a = M[a]);
      if (typeof b == "number") var d = a.datapoints.pointsize,
        b = a.datapoints.points.slice(d * b, d * (b + 1));
      d = Ea(a, b);
      if (d == -1) Ra.push({
        series: a,
        point: b,
        auto: c
      }), k();
      else if (!c) Ra[d].auto = false
    }
    function fa(a, b) {
      a == null && b == null && (Ra = [], k());
      typeof a == "number" && (a = M[a]);
      typeof b == "number" && (b = a.data[b]);
      var c = Ea(a, b);
      c != -1 && (Ra.splice(c, 1), k())
    }
    function Ea(a, b) {
      for (var c = 0; c < Ra.length; ++c) {
        var d = Ra[c];
        if (d.series == a && d.point[0] == b[0] && d.point[1] == b[1]) return c
      }
      return -1
    }
    function P(b, c) {
      ea.lineWidth = b.bars.lineWidth;
      ea.strokeStyle = a.color.parse(b.color).scale("a", 0.5).toString();
      var d = a.color.parse(b.color).scale("a", 0.5).toString(),
        e = b.bars.align == "left" ? 0 : -b.bars.barWidth / 2;
      V(c[0], c[1], c[2] || 0, e, e + b.bars.barWidth, 0, function () {
        return d
      }, b.xaxis, b.yaxis, ea, b.bars.horizontal, b.bars.lineWidth)
    }
    function Y(b, c, d, e) {
      if (typeof b == "string") return b;
      else {
        for (var c = E.createLinearGradient(0, d, 0, c), d = 0, f = b.colors.length; d < f; ++d) {
          var g = b.colors[d];
          if (typeof g != "string") {
            var h = a.color.parse(e);
            g.brightness != null && (h = h.scale("rgb", g.brightness));
            g.opacity != null && (h.a *= g.opacity);
            g = h.toString()
          }
          c.addColorStop(d / (f - 1), g)
        }
        return c
      }
    }
    var M = [],
      A = {
        colors: ["#edc240", "#afd8f8", "#cb4b4b", "#4da74d", "#9440ed"],
        legend: {
          show: true,
          noColumns: 1,
          labelFormatter: null,
          labelBoxBorderColor: "#ccc",
          container: null,
          position: "ne",
          margin: 5,
          backgroundColor: null,
          backgroundOpacity: 0.85
        },
        xaxis: {
          position: "bottom",
          mode: null,
          color: null,
          tickColor: null,
          transform: null,
          inverseTransform: null,
          min: null,
          max: null,
          autoscaleMargin: null,
          ticks: null,
          tickFormatter: null,
          labelWidth: null,
          labelHeight: null,
          tickLength: null,
          alignTicksWithAxis: null,
          tickDecimals: null,
          tickSize: null,
          minTickSize: null,
          monthNames: null,
          timeformat: null,
          twelveHourClock: false
        },
        yaxis: {
          autoscaleMargin: 0.02,
          position: "left"
        },
        xaxes: [],
        yaxes: [],
        series: {
          points: {
            show: false,
            radius: 3,
            lineWidth: 2,
            fill: true,
            fillColor: "#ffffff"
          },
          lines: {
            lineWidth: 2,
            fill: false,
            fillColor: null,
            steps: false
          },
          bars: {
            show: false,
            lineWidth: 2,
            barWidth: 1,
            fill: true,
            fillColor: null,
            align: "left",
            horizontal: false
          },
          shadowSize: 3
        },
        grid: {
          show: true,
          aboveData: false,
          color: "#545454",
          backgroundColor: null,
          borderColor: null,
          tickColor: null,
          labelMargin: 5,
          axisMargin: 8,
          borderWidth: 2,
          markings: null,
          markingsColor: "#f4f4f4",
          markingsLineWidth: 2,
          clickable: false,
          hoverable: false,
          autoHighlight: true,
          mouseActiveRadius: 10
        },
        hooks: {}
      },
      S = null,
      La = null,
      kb = null,
      E = null,
      ea = null,
      W = [],
      X = [],
      I = {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      },
      ua = 0,
      ya = 0,
      Fa = 0,
      za = 0,
      la = {
        processOptions: [],
        processRawData: [],
        processDatapoints: [],
        drawSeries: [],
        draw: [],
        bindEvents: [],
        drawOverlay: []
      },
      aa = this;
    aa.setData = j;
    aa.setupGrid = D;
    aa.draw = z;
    aa.getPlaceholder = function () {
      return c
    };
    aa.getCanvas = function () {
      return S
    };
    aa.getPlotOffset = function () {
      return I
    };
    aa.width = function () {
      return Fa
    };
    aa.height = function () {
      return za
    };
    aa.offset = function () {
      var a = kb.offset();
      a.left += I.left;
      a.top += I.top;
      return a
    };
    aa.getData = function () {
      return M
    };
    aa.getAxis = function (a, b) {
      var c = (a == x ? W : X)[b - 1];
      c && !c.used && (c = null);
      return c
    };
    aa.getAxes = function () {
      var a = {},
        b;
      for (b = 0; b < W.length; ++b) a["x" + (b ? b + 1 : "") + "axis"] = W[b] || {};
      for (b = 0; b < X.length; ++b) a["y" + (b ? b + 1 : "") + "axis"] = X[b] || {};
      if (!a.x2axis) a.x2axis = {
        n: 2
      };
      if (!a.y2axis) a.y2axis = {
        n: 2
      };
      return a
    };
    aa.getXAxes = function () {
      return W
    };
    aa.getYAxes = function () {
      return X
    };
    aa.getUsedAxes = m;
    aa.c2p = n;
    aa.p2c = function (a) {
      var b = {},
        c, d, e;
      for (c = 0; c < W.length; ++c) if ((d = W[c]) && d.used) if (e = "x" + d.n, a[e] == null && d.n == 1 && (e = "x"), a[e]) {
        b.left = d.p2c(a[e]);
        break
      }
      for (c = 0; c < X.length; ++c) if ((d = X[c]) && d.used) if (e = "y" + d.n, a[e] == null && d.n == 1 && (e = "y"), a[e]) {
        b.top = d.p2c(a[e]);
        break
      }
      return b
    };
    aa.getOptions = function () {
      return A
    };
    aa.highlight = Z;
    aa.unhighlight = fa;
    aa.triggerRedrawOverlay = k;
    aa.pointOffset = function (a) {
      return {
        left: parseInt(W[l(a, "x") - 1].p2c(+a.x) + I.left),
        top: parseInt(X[l(a, "y") - 1].p2c(+a.y) + I.top)
      }
    };
    aa.hooks = la;
    (function () {
      for (var b = 0; b < g.length; ++b) {
        var c = g[b];
        c.init(aa);
        c.options && a.extend(true, A, c.options)
      }
    })(aa);
    (function (b) {
      a.extend(true, A, b);
      if (A.xaxis.color == null) A.xaxis.color = A.grid.color;
      if (A.yaxis.color == null) A.yaxis.color = A.grid.color;
      if (A.xaxis.tickColor == null) A.xaxis.tickColor = A.grid.tickColor;
      if (A.yaxis.tickColor == null) A.yaxis.tickColor = A.grid.tickColor;
      if (A.grid.borderColor == null) A.grid.borderColor = A.grid.color;
      if (A.grid.tickColor == null) A.grid.tickColor = a.color.parse(A.grid.color).scale("a", 0.22).toString();
      for (b = 0; b < Math.max(1, A.xaxes.length); ++b) A.xaxes[b] = a.extend(true, {}, A.xaxis, A.xaxes[b]);
      for (b = 0; b < Math.max(1, A.yaxes.length); ++b) A.yaxes[b] = a.extend(true, {}, A.yaxis, A.yaxes[b]);
      if (A.xaxis.noTicks && A.xaxis.ticks == null) A.xaxis.ticks = A.xaxis.noTicks;
      if (A.yaxis.noTicks && A.yaxis.ticks == null) A.yaxis.ticks = A.yaxis.noTicks;
      if (A.x2axis) A.y2axis.position = "top", A.xaxes[1] = A.x2axis;
      if (A.y2axis) {
        if (A.y2axis.autoscaleMargin === void 0) A.y2axis.autoscaleMargin = 0.02;
        A.y2axis.position = "right";
        A.yaxes[1] = A.y2axis
      }
      if (A.grid.coloredAreas) A.grid.markings = A.grid.coloredAreas;
      if (A.grid.coloredAreasColor) A.grid.markingsColor = A.grid.coloredAreasColor;
      A.lines && a.extend(true, A.series.lines, A.lines);
      A.points && a.extend(true, A.series.points, A.points);
      A.bars && a.extend(true, A.series.bars, A.bars);
      if (A.shadowSize) A.series.shadowSize = A.shadowSize;
      for (b = 0; b < A.xaxes.length; ++b) p(W, b + 1).options = A.xaxes[b];
      for (b = 0; b < A.yaxes.length; ++b) p(X, b + 1).options = A.yaxes[b];
      for (var c in la) A.hooks[c] && A.hooks[c].length && (la[c] = la[c].concat(A.hooks[c]));
      h(la.processOptions, [A])
    })(e);
    (function () {
      function b(a, c) {
        var d = document.createElement("canvas");
        d.width = a;
        d.height = c;
        d.getContext || (d = window.G_vmlCanvasManager.initElement(d));
        return d
      }
      ua = c.width();
      ya = c.height();
      c.html("");
      c.css("position") == "static" && c.css("position", "relative");
      if (ua <= 0 || ya <= 0) throw "Invalid dimensions for plot, width = " + ua + ", height = " + ya;
      window.G_vmlCanvasManager && window.G_vmlCanvasManager.init_(document);
      S = a(b(ua, ya)).appendTo(c).get(0);
      E = S.getContext("2d");
      La = a(b(ua, ya)).css({
        position: "absolute",
        left: 0,
        top: 0
      }).appendTo(c).get(0);
      ea = La.getContext("2d");
      ea.stroke()
    })();
    j(d);
    D();
    z();
    kb = a([La, S]);
    A.grid.hoverable && kb.mousemove(H);
    A.grid.clickable && kb.click(sa);
    h(la.bindEvents, [kb]);
    var Ra = [],
      hb = null
  }
  function b(a, b) {
    return b * Math.floor(a / b)
  }
  a.plot = function (b, d, f) {
    return new e(a(b), d, f, a.plot.plugins)
  };
  a.plot.plugins = [];
  a.plot.formatDate = function (a, b, e) {
    var g = function (a) {
        a = "" + a;
        return a.length == 1 ? "0" + a : a
      },
      h = [],
      j = false,
      l = false,
      n = a.getUTCHours(),
      m = n < 12;
    e == null && (e = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","));
    b.search(/%p|%P/) != -1 && (n > 12 ? n -= 12 : n == 0 && (n = 12));
    for (var p = 0; p < b.length; ++p) {
      var o = b.charAt(p);
      if (j) {
        switch (o) {
        case "h":
          o = "" + n;
          break;
        case "H":
          o = g(n);
          break;
        case "M":
          o = g(a.getUTCMinutes());
          break;
        case "S":
          o = g(a.getUTCSeconds());
          break;
        case "d":
          o = "" + a.getUTCDate();
          break;
        case "m":
          o = "" + (a.getUTCMonth() + 1);
          break;
        case "y":
          o = "" + a.getUTCFullYear();
          break;
        case "b":
          o = "" + e[a.getUTCMonth()];
          break;
        case "p":
          o = m ? "am" : "pm";
          break;
        case "P":
          o = m ? "AM" : "PM";
          break;
        case "0":
          o = "", l = true
        }
        o && l && (o = g(o), l = false);
        h.push(o);
        l || (j = false)
      } else o == "%" ? j = true : h.push(o)
    }
    return h.join("")
  }
})(jQuery);
(function (a) {
  var e = {
    series: {
      pie: {
        show: false,
        radius: "auto",
        innerRadius: 0,
        startAngle: 1.5,
        tilt: 1,
        offset: {
          top: 0,
          left: "auto"
        },
        stroke: {
          color: "#FFF",
          width: 1
        },
        label: {
          show: "auto",
          formatter: function (a, c) {
            return '<div style="font-size:x-small;text-align:center;padding:2px;color:' + c.color + ';">' + a + "<br/>" + Math.round(c.percent) + "%</div>"
          },
          radius: 1,
          background: {
            color: null,
            opacity: 0
          },
          threshold: 0
        },
        combine: {
          threshold: -1,
          color: null,
          label: "Other"
        },
        highlight: {
          opacity: 0.5
        }
      }
    }
  };
  a.plot.plugins.push({
    init: function (b) {
      function c(b) {
        K || (K = true, p = b.getCanvas(), o = a(p).parent(), e = b.getOptions(), b.setData(d(b.getData())))
      }
      function d(a) {
        for (var b = 0; b < a.length; ++b) if (typeof a[b].data == "number") a[b].data = [
          [1, a[b].data]
        ];
        else if (typeof a[b].data == "undefined" || typeof a[b].data[0] == "undefined") {
          if (typeof a[b].data != "undefined" && typeof a[b].data.label != "undefined") a[b].label = a[b].data.label;
          a[b].data = [
            [1, 0]
          ]
        }
        for (var b = a, c = 0; c < b.length; ++c) {
          var d = parseFloat(b[c].data[0][1]);
          d && (D += d)
        }
        for (var c = b = 0, d = e.series.pie.combine.color, f = [], g = 0; g < a.length; ++g) if (a[g].data[0][1] = parseFloat(a[g].data[0][1]), a[g].data[0][1] || (a[g].data[0][1] = 0), a[g].data[0][1] / D <= e.series.pie.combine.threshold) {
          if (b += a[g].data[0][1], c++, !d) d = a[g].color
        } else f.push({
          data: [
            [1, a[g].data[0][1]]
          ],
          color: a[g].color,
          label: a[g].label,
          angle: a[g].data[0][1] * Math.PI * 2 / D,
          percent: a[g].data[0][1] / D * 100
        });
        c > 0 && f.push({
          data: [
            [1, b]
          ],
          color: d,
          label: e.series.pie.combine.label,
          angle: b * Math.PI * 2 / D,
          percent: b / D * 100
        });
        return f
      }
      function f(b, c) {
        function d() {
          ctx.clearRect(0, 0, p.width, p.height);
          o.children().filter(".pieLabel, .pieLabelBackground").remove()
        }

        function f() {
          var a = e.series.pie.radius > 1 ? e.series.pie.radius : u * e.series.pie.radius;
          if (!(a >= p.width / 2 - 5 || a * e.series.pie.tilt >= p.height / 2 - 15 || a <= 10)) {
            ctx.save();
            ctx.translate(5, 15);
            ctx.globalAlpha = 0.02;
            ctx.fillStyle = "#000";
            ctx.translate(w, s);
            ctx.scale(1, e.series.pie.tilt);
            for (var b = 1; b <= 10; b++) ctx.beginPath(), ctx.arc(0, 0, a, 0, Math.PI * 2, false), ctx.fill(), a -= b;
            ctx.restore()
          }
        }
        function h() {
          function b(c, e, g) {
            if (!(c <= 0)) pieWithSingleSlice = c == Math.PI * 2 ? true : false, g ? ctx.fillStyle = e : (ctx.strokeStyle = e, ctx.lineJoin = "round"), ctx.beginPath(), a.browser.msie && (c -= 1.0E-4), ctx.arc(0, 0, d, f, f + c, false), pieWithSingleSlice || ctx.lineTo(0, 0), ctx.closePath(), f += c, g ? ctx.fill() : ctx.stroke()
          }
          function c() {
            for (var b = startAngle, d = e.series.pie.label.radius > 1 ? e.series.pie.label.radius : u * e.series.pie.label.radius, f = 0; f < j.length; ++f) {
              if (j[f].percent >= e.series.pie.label.threshold * 100) {
                var g = j[f],
                  h = b,
                  n = f;
                if (g.data[0][1] != 0) {
                  var l = e.legend.labelFormatter,
                    k = void 0,
                    m = e.series.pie.label.formatter,
                    k = l ? l(g.label, g) : g.label;
                  m && (k = m(k, g));
                  l = (h + g.angle + h) / 2;
                  h = w + Math.round(Math.cos(l) * d);
                  l = s + Math.round(Math.sin(l) * d) * e.series.pie.tilt;
                  o.append('<span class="pieLabel" id="pieLabel' + n + '" style="position:absolute;top:' + l + "px;left:" + h + 'px;">' + k + "</span>");
                  n = o.children("#pieLabel" + n);
                  k = l - n.height() / 2;
                  l = h - n.width() / 2;
                  n.css("top", k);
                  n.css("left", l);
                  if (0 - k > 0 || 0 - l > 0 || p.height - (k + n.height()) < 0 || p.width - (l + n.width()) < 0) C = true;
                  if (e.series.pie.label.background.opacity != 0) {
                    h = e.series.pie.label.background.color;
                    if (h == null) h = g.color;
                    g = "top:" + k + "px;left:" + l + "px;";
                    a('<div class="pieLabelBackground" style="position:absolute;width:' + n.width() + "px;height:" + n.height() + "px;" + g + "background-color:" + h + ';"> </div>').insertBefore(n).css("opacity", e.series.pie.label.background.opacity)
                  }
                }
              }
              b += j[f].angle
            }
          }
          startAngle = Math.PI * e.series.pie.startAngle;
          var d = e.series.pie.radius > 1 ? e.series.pie.radius : u * e.series.pie.radius;
          ctx.save();
          ctx.translate(w, s);
          ctx.scale(1, e.series.pie.tilt);
          ctx.save();
          for (var f = startAngle, n = 0; n < j.length; ++n) j[n].startAngle = f, b(j[n].angle, j[n].color, true);
          ctx.restore();
          ctx.save();
          ctx.lineWidth = e.series.pie.stroke.width;
          f = startAngle;
          for (n = 0; n < j.length; ++n) b(j[n].angle, e.series.pie.stroke.color, false);
          ctx.restore();
          g(ctx);
          e.series.pie.label.show && c();
          ctx.restore()
        }
        if (o) {
          ctx = c;
          q = o.children().filter(".legend").children().width();
          u = Math.min(p.width, p.height / e.series.pie.tilt) / 2;
          s = p.height / 2 + e.series.pie.offset.top;
          w = p.width / 2;
          e.series.pie.offset.left == "auto" ? e.legend.position.match("w") ? w += q / 2 : w -= q / 2 : w += e.series.pie.offset.left;
          w < u ? w = u : w > p.width - u && (w = p.width - u);
          for (var j = b.getData(), n = 0; C && n < z;) C = false, n > 0 && (u *= F), n += 1, d(), e.series.pie.tilt <= 0.8 && f(), h();
          n >= z && (d(), o.prepend('<div class="error">Could not draw pie with labels contained inside canvas</div>'));
          b.setSeries && b.insertLegend && (b.setSeries(j), b.insertLegend())
        }
      }
      function g(a) {
        if (e.series.pie.innerRadius > 0) a.save(), innerRadius = e.series.pie.innerRadius > 1 ? e.series.pie.innerRadius : u * e.series.pie.innerRadius, a.globalCompositeOperation = "destination-out", a.beginPath(), a.fillStyle = e.series.pie.stroke.color, a.arc(0, 0, innerRadius, 0, Math.PI * 2, false), a.fill(), a.closePath(), a.restore(), a.save(), a.beginPath(), a.strokeStyle = e.series.pie.stroke.color, a.arc(0, 0, innerRadius, 0, Math.PI * 2, false), a.stroke(), a.closePath(), a.restore()
      }
      function h(a) {
        l("plothover", a)
      }
      function j(a) {
        l("plotclick", a)
      }
      function l(a, c) {
        var d = b.offset(),
          f = parseInt(c.pageX - d.left);
        a: {
          for (var d = parseInt(c.pageY - d.top), g = b.getData(), h = b.getOptions(), h = h.series.pie.radius > 1 ? h.series.pie.radius : u * h.series.pie.radius, j = 0; j < g.length; ++j) {
            var l = g[j];
            if (l.pie.show) {
              ctx.save();
              ctx.beginPath();
              ctx.moveTo(0, 0);
              ctx.arc(0, 0, h, l.startAngle, l.startAngle + l.angle, false);
              ctx.closePath();
              x = f - w;
              y = d - s;
              if (ctx.isPointInPath) {
                if (ctx.isPointInPath(f - w, d - s)) {
                  ctx.restore();
                  f = {
                    datapoint: [l.percent, l.data],
                    dataIndex: 0,
                    series: l,
                    seriesIndex: j
                  };
                  break a
                }
              } else {
                p1X = h * Math.cos(l.startAngle);
                p1Y = h * Math.sin(l.startAngle);
                p2X = h * Math.cos(l.startAngle + l.angle / 4);
                p2Y = h * Math.sin(l.startAngle + l.angle / 4);
                p3X = h * Math.cos(l.startAngle + l.angle / 2);
                p3Y = h * Math.sin(l.startAngle + l.angle / 2);
                p4X = h * Math.cos(l.startAngle + l.angle / 1.5);
                p4Y = h * Math.sin(l.startAngle + l.angle / 1.5);
                p5X = h * Math.cos(l.startAngle + l.angle);
                p5Y = h * Math.sin(l.startAngle + l.angle);
                arrPoly = [
                  [0, 0],
                  [p1X, p1Y],
                  [p2X, p2Y],
                  [p3X, p3Y],
                  [p4X, p4Y],
                  [p5X, p5Y]
                ];
                arrPoint = [x, y];
                for (var m = false, z = -1, p = arrPoly.length, q = p - 1; ++z < p; q = z)(arrPoly[z][1] <= arrPoint[1] && arrPoint[1] < arrPoly[q][1] || arrPoly[q][1] <= arrPoint[1] && arrPoint[1] < arrPoly[z][1]) && arrPoint[0] < (arrPoly[q][0] - arrPoly[z][0]) * (arrPoint[1] - arrPoly[z][1]) / (arrPoly[q][1] - arrPoly[z][1]) + arrPoly[z][0] && (m = !m);
                if (m) {
                  ctx.restore();
                  f = {
                    datapoint: [l.percent, l.data],
                    dataIndex: 0,
                    series: l,
                    seriesIndex: j
                  };
                  break a
                }
              }
              ctx.restore()
            }
          }
          f = null
        }
        if (e.grid.autoHighlight) for (d = 0; d < O.length; ++d) if (g = O[d], g.auto == a && !(f && g.series == f.series)) g = g.series,
        g == null && (O = [], b.triggerRedrawOverlay()),
        typeof g == "number" && (g = series[g]),
        g = n(g),
        g != -1 && (O.splice(g, 1), b.triggerRedrawOverlay());
        if (f) if (d = f.series, typeof d == "number" && (d = series[d]), g = n(d), g == -1) O.push({
          series: d,
          auto: a
        }), b.triggerRedrawOverlay();
        else if (!a) O[g].auto = false;
        o.trigger(a, [{
          pageX: c.pageX,
          pageY: c.pageY
        },
        f])
      }
      function n(a) {
        for (var b = 0; b < O.length; ++b) if (O[b].series == a) return b;
        return -1
      }
      function m(a, b) {
        var c = a.getOptions(),
          d = c.series.pie.radius > 1 ? c.series.pie.radius : u * c.series.pie.radius;
        b.save();
        b.translate(w, s);
        b.scale(1, c.series.pie.tilt);
        for (i = 0; i < O.length; ++i) {
          var e = O[i].series;
          if (!(e.angle < 0)) b.fillStyle = "rgba(255, 255, 255, " + c.series.pie.highlight.opacity + ")", b.beginPath(), e.angle != Math.PI * 2 && b.moveTo(0, 0), b.arc(0, 0, d, e.startAngle, e.startAngle + e.angle, false), b.closePath(), b.fill()
        }
        g(b);
        b.restore()
      }
      var p = null,
        o = null,
        u = null,
        w = null,
        s = null,
        D = 0,
        C = true,
        z = 10,
        F = 0.9,
        q = 0,
        K = false,
        O = [];
      b.hooks.processOptions.push(function (a, b) {
        if (b.series.pie.show) {
          b.grid.show = false;
          if (b.series.pie.label.show == "auto") b.series.pie.label.show = b.legend.show ? false : true;
          if (b.series.pie.radius == "auto") b.series.pie.radius = b.series.pie.label.show ? 0.75 : 1;
          if (b.series.pie.tilt > 1) b.series.pie.tilt = 1;
          if (b.series.pie.tilt < 0) b.series.pie.tilt = 0;
          a.hooks.processDatapoints.push(c);
          a.hooks.drawOverlay.push(m);
          a.hooks.draw.push(f)
        }
      });
      b.hooks.bindEvents.push(function (a, b) {
        var c = a.getOptions();
        c.series.pie.show && c.grid.hoverable && b.unbind("mousemove").mousemove(h);
        c.series.pie.show && c.grid.clickable && b.unbind("click").click(j)
      })
    },
    options: e,
    name: "pie",
    version: "1.0"
  })
})(jQuery);
(function () {
  function a(a, b) {
    var c;
    a || (a = {});
    for (c in b) a[c] = b[c];
    return a
  }
  function e() {
    for (var a = 0, b = arguments, c = b.length, d = {}; a < c; a++) d[b[a++]] = b[a];
    return d
  }
  function b(a, b) {
    return parseInt(a, b || 10)
  }
  function c(a) {
    return typeof a === "string"
  }
  function d(a) {
    return typeof a === "object"
  }
  function f(a) {
    return Object.prototype.toString.call(a) === "[object Array]"
  }
  function g(a) {
    return typeof a === "number"
  }
  function h(a) {
    return A.log(a) / A.LN10
  }
  function j(a) {
    return A.pow(10, a)
  }
  function l(a, b) {
    for (var c = a.length; c--;) if (a[c] === b) {
      a.splice(c, 1);
      break
    }
  }
  function n(a) {
    return a !== P && a !== null
  }
  function m(a, b, e) {
    var f, g;
    if (c(b)) n(e) ? a.setAttribute(b, e) : a && a.getAttribute && (g = a.getAttribute(b));
    else if (n(b) && d(b)) for (f in b) a.setAttribute(f, b[f]);
    return g
  }
  function p(a) {
    return f(a) ? a : [a]
  }
  function o() {
    var a = arguments,
      b, c, d = a.length;
    for (b = 0; b < d; b++) if (c = a[b], typeof c !== "undefined" && c !== null) return c
  }
  function u(b, c) {
    if (za && c && c.opacity !== P) c.filter = "alpha(opacity=" + c.opacity * 100 + ")";
    a(b.style, c)
  }
  function w(b, c, d, e, f) {
    b = Y.createElement(b);
    c && a(b, c);
    f && u(b, {
      padding: 0,
      border: ob,
      margin: 0
    });
    d && u(b, d);
    e && e.appendChild(b);
    return b
  }
  function s(b, c) {
    var d = function () {};
    d.prototype = new b;
    a(d.prototype, c);
    return d
  }
  function D(a, c, d, e) {
    var f = Ba.lang,
      g = isNaN(c = W(c)) ? 2 : c,
      c = d === void 0 ? f.decimalPoint : d,
      e = e === void 0 ? f.thousandsSep : e,
      f = a < 0 ? "-" : "",
      d = String(b(a = W(+a || 0).toFixed(g))),
      h = d.length > 3 ? d.length % 3 : 0;
    return f + (h ? d.substr(0, h) + e : "") + d.substr(h).replace(/(\d{3})(?=\d)/g, "$1" + e) + (g ? c + W(a - d).toFixed(g).slice(2) : "")
  }
  function C(a, b) {
    return Array((b || 2) + 1 - String(a).length).join(0) + a
  }
  function z(a, b, c, d) {
    var e, c = o(c, 1);
    e = a / c;
    b || (b = [1, 2, 2.5, 5, 10], d && d.allowDecimals === false && (c === 1 ? b = [1, 2, 5, 10] : c <= 0.1 && (b = [1 / c])));
    for (d = 0; d < b.length; d++) if (a = b[d], e <= (b[d] + (b[d + 1] || b[d])) / 2) break;
    a *= c;
    return a
  }
  function F(a, b) {
    var c = b || [
      [Ib, [1, 2, 5, 10, 20, 25, 50, 100, 200, 500]],
      [Nb, [1, 2, 5, 10, 15, 30]],
      [Ob, [1, 2, 5, 10, 15, 30]],
      [Na, [1, 2, 3, 4, 6, 8, 12]],
      [Va, [1, 2]],
      [Eb, [1, 2]],
      [na, [1, 2, 3, 4, 6]],
      [Xa, null]
    ],
      d = c[c.length - 1],
      e = ma[d[0]],
      f = d[1],
      g;
    for (g = 0; g < c.length; g++) if (d = c[g], e = ma[d[0]], f = d[1], c[g + 1] && a <= (e * f[f.length - 1] + ma[c[g + 1][0]]) / 2) break;
    e === ma[Xa] && a < 5 * e && (f = [1, 2, 5]);
    e === ma[Xa] && a < 5 * e && (f = [1, 2, 5]);
    c = z(a / e, f);
    return {
      unitRange: e,
      count: c,
      unitName: d[0]
    }
  }
  function q(b, c, d, e) {
    var f = [],
      g = {},
      h = Ba.global.useUTC,
      j, n = new Date(c),
      c = b.unitRange,
      l = b.count;
    c >= ma[Nb] && (n.setMilliseconds(0), n.setSeconds(c >= ma[Ob] ? 0 : l * La(n.getSeconds() / l)));
    if (c >= ma[Ob]) n[oc](c >= ma[Na] ? 0 : l * La(n[Xb]() / l));
    if (c >= ma[Na]) n[wc](c >= ma[Va] ? 0 : l * La(n[Tb]() / l));
    if (c >= ma[Va]) n[Cb](c >= ma[na] ? 1 : l * La(n[Kb]() / l));
    c >= ma[na] && (n[xc](c >= ma[Xa] ? 0 : l * La(n[Pb]() / l)), j = n[Qb]());
    c >= ma[Xa] && (j -= j % l, n[yc](j));
    if (c === ma[Eb]) n[Cb](n[Kb]() - n[Yb]() + o(e, 1));
    e = 1;
    j = n[Qb]();
    for (var k = n.getTime(), m = n[Pb](), n = n[Kb](); k < d;) f.push(k), c === ma[Xa] ? k = Jb(j + e * l, 0) : c === ma[na] ? k = Jb(j, m + e * l) : !h && (c === ma[Va] || c === ma[Eb]) ? k = Jb(j, m, n + e * l * (c === ma[Va] ? 1 : 7)) : (k += c * l, c <= ma[Na] && k % ma[Va] === 0 && (g[k] = Va)), e++;
    f.push(k);
    f.info = a(b, {
      higherRanks: g,
      totalRange: c * l
    });
    return f
  }
  function K() {
    this.symbol = this.color = 0
  }
  function O(a, b, c, d, e, f, g, h, j) {
    var n = g.x,
      g = g.y,
      j = n + c + (j ? h : -a - h),
      l = g - b + d + 15,
      k;
    j < 7 && (j = c + n + h);
    j + a > c + e && (j -= j + a - (c + e), l = g - b + d - h, k = true);
    l < d + 5 ? (l = d + 5, k && g >= l && g <= l + b && (l = g + d + h)) : l + b > d + f && (l = d + f - b - h);
    return {
      x: j,
      y: l
    }
  }
  function V(a, b) {
    var c = a.length,
      d, e;
    for (e = 0; e < c; e++) a[e].ss_i = e;
    a.sort(function (a, c) {
      d = b(a, c);
      return d === 0 ? a.ss_i - c.ss_i : d
    });
    for (e = 0; e < c; e++) delete a[e].ss_i
  }
  function cb(a) {
    for (var b = a.length, c = a[0]; b--;) a[b] < c && (c = a[b]);
    return c
  }
  function Ha(a) {
    for (var b = a.length, c = a[0]; b--;) a[b] > c && (c = a[b]);
    return c
  }
  function H(a) {
    for (var b in a) a[b] && a[b].destroy && a[b].destroy(), delete a[b]
  }
  function sa(a) {
    Da || (Da = w(Hb));
    a && Da.appendChild(a);
    Da.innerHTML = ""
  }
  function gb(a, b) {
    var c = "Highcharts error #" + a + ": www.highcharts.com/errors/" + a;
    if (b) throw c;
    else M.console && console.log(c)
  }
  function k(a) {
    return parseFloat(a.toPrecision(14))
  }
  function va(a, b) {
    Wb = o(a, b.animation)
  }
  function Z() {
    var a = Ba.global.useUTC,
      b = a ? "getUTC" : "get",
      c = a ? "setUTC" : "set";
    Jb = a ? Date.UTC : function (a, b, c, d, e, f) {
      return (new Date(a, b, o(c, 1), o(d, 0), o(e, 0), o(f, 0))).getTime()
    };
    Xb = b + "Minutes";
    Tb = b + "Hours";
    Yb = b + "Day";
    Kb = b + "Date";
    Pb = b + "Month";
    Qb = b + "FullYear";
    oc = c + "Minutes";
    wc = c + "Hours";
    Cb = c + "Date";
    xc = c + "Month";
    yc = c + "FullYear"
  }
  function fa() {}
  function Ea(e, f) {
    function g(c) {
      function d(a, b) {
        this.pos = a;
        this.type = b || "";
        this.isNew = true;
        b || this.addLabel()
      }
      function e(a) {
        if (a) this.options = a, this.id = a.id;
        return this
      }
      function f(a, b, c, d) {
        this.isNegative = b;
        this.options = a;
        this.x = c;
        this.stack = d;
        this.alignOptions = {
          align: a.align || (Ga ? b ? "left" : "right" : "center"),
          verticalAlign: a.verticalAlign || (Ga ? "middle" : b ? "bottom" : "top"),
          y: o(a.y, Ga ? 4 : b ? 14 : -6),
          x: o(a.x, Ga ? b ? -6 : 6 : 0)
        };
        this.textAlign = a.textAlign || (Ga ? b ? "right" : "left" : "center")
      }
      function v() {
        var a = [],
          b = [],
          c;
        T = ra = null;
        J(Q.series, function (d) {
          if (d.visible || !ca.ignoreHiddenSeries) {
            var e = d.options,
              g, h, j, v, l, k, m, G, N, z = e.threshold,
              R, p = [],
              s = 0;
            if (db && z <= 0) z = e.threshold = null;
            if (Qa) e = d.xData, e.length && (T = ea(o(T, e[0]), cb(e)), ra = E(o(ra, e[0]), Ha(e)));
            else {
              var qa, q, U, w = d.cropped,
                u = d.xAxis.getExtremes(),
                C = !! d.modifyValue;
              g = e.stacking;
              ya = g === "percent";
              if (g) l = e.stack, v = d.type + o(l, ""), k = "-" + v, d.stackKey = v, h = a[v] || [], a[v] = h, j = b[k] || [], b[k] = j;
              ya && (T = 0, ra = 99);
              e = d.processedXData;
              m = d.processedYData;
              R = m.length;
              for (c = 0; c < R; c++) if (G = e[c], N = m[c], N !== null && N !== P && (g ? (q = (qa = N < z) ? j : h, U = qa ? k : v, N = q[G] = n(q[G]) ? q[G] + N : N, $a[U] || ($a[U] = {}), $a[U][G] || ($a[U][G] = new f(L.stackLabels, qa, G, l)), $a[U][G].setTotal(N)) : C && (N = d.modifyValue(N)), w || (e[c + 1] || G) >= u.min && (e[c - 1] || G) <= u.max)) if (G = N.length) for (; G--;) N[G] !== null && (p[s++] = N[G]);
              else p[s++] = N;
              !ya && p.length && (T = ea(o(T, p[0]), cb(p)), ra = E(o(ra, p[0]), Ha(p)));
              n(z) && (T >= z ? (T = z, Cc = true) : ra < z && (ra = z, Dc = true))
            }
          }
        })
      }
      function m(a, b, c) {
        for (var d, b = k(La(b / a) * a), c = k(kb(c / a) * a), e = []; b <= c;) {
          e.push(b);
          b = k(b + a);
          if (b === d) break;
          d = b
        }
        return e
      }
      function G(a, b, c, d) {
        var e = [];
        if (!d) Q._minorAutoInterval = null;
        if (a >= 0.5) a = S(a), e = m(a, b, c);
        else if (a >= 0.08) {
          var f = La(b),
            g, n, v, l, k, N;
          for (g = a > 0.3 ? [1, 2, 4] : a > 0.15 ? [1, 2, 4, 6, 8] : [1, 2, 3, 4, 5, 6, 7, 8, 9]; f < c + 1 && !N; f++) {
            v = g.length;
            for (n = 0; n < v && !N; n++) l = h(j(f) * g[n]), l > b && e.push(k), k > c && (N = true), k = l
          }
        } else if (b = j(b), c = j(c), a = L[d ? "minorTickInterval" : "tickInterval"], a = o(a === "auto" ? null : a, Q._minorAutoInterval, (c - b) * (L.tickPixelInterval / (d ? 5 : 1)) / ((d ? V / wa.length : V) || 1)), a = z(a, null, A.pow(10, La(A.log(a) / A.LN10))), e = xb(m(a, b, c), h), !d) Q._minorAutoInterval = a / 5;
        d || (ia = a);
        return e
      }
      function N() {
        var a = [],
          b, c;
        if (db) {
          c = wa.length;
          for (b = 1; b < c; b++) a = a.concat(G(sc, wa[b - 1], wa[b], true))
        } else for (b = ha + (wa[0] - ha) % sc; b <= ja; b += sc) a.push(b);
        return a
      }
      function R() {
        var a, b = ra - T >= Gb,
          c, d, e, f, g;
        Qa && Gb === P && !db && (n(L.min) || n(L.max) ? Gb = null : (J(Q.series, function (a) {
          f = a.xData;
          for (d = a.xIncrement ? 1 : f.length - 1; d > 0; d--) if (e = f[d] - f[d - 1], c === P || e < c) c = e
        }), Gb = ea(c * 5, ra - T)));
        ja - ha < Gb && (a = (Gb - ja + ha) / 2, a = [ha - a, o(L.min, ha - a)], b && (a[2] = T), ha = Ha(a), g = [ha + Gb, o(L.max, ha + Gb)], b && (g[2] = ra), ja = cb(g), ja - ha < Gb && (a[0] = ja - Gb, a[1] = o(L.min, ja - Gb), ha = Ha(a)))
      }
      function p(a) {
        var b, c = L.tickInterval,
          d = L.tickPixelInterval;
        qb ? (la = B[Qa ? "xAxis" : "yAxis"][L.linkedTo], b = la.getExtremes(), ha = o(b.min, b.dataMin), ja = o(b.max, b.dataMax), L.type !== la.options.type && gb(11, 1)) : (ha = o(ic, L.min, T), ja = o(ba, L.max, ra));
        db && (!a && ea(ha, T) <= 0 && gb(10, 1), ha = h(ha), ja = h(ja));
        aa && (ic = ha = E(ha, ja - aa), ba = ja, a && (aa = null));
        R();
        if (!Ba && !ya && !qb && n(ha) && n(ja)) {
          b = ja - ha || 1;
          if (!n(L.min) && !n(ic) && ma && (T < 0 || !Cc)) ha -= b * ma;
          if (!n(L.max) && !n(ba) && ua && (ra > 0 || !Dc)) ja += b * ua
        }
        ia = ha === ja || ha === void 0 || ja === void 0 ? 1 : qb && !c && d === la.options.tickPixelInterval ? la.tickInterval : o(c, Ba ? 1 : (ja - ha) * d / (V || 1));
        Qa && !a && J(Q.series, function (a) {
          a.processData(ha !== ga || ja !== bb)
        });
        va();
        Q.beforeSetTickPositions && Q.beforeSetTickPositions();
        Q.postProcessTickInterval && (ia = Q.postProcessTickInterval(ia));
        !Vb && !db && (Ta = A.pow(10, La(A.log(ia) / A.LN10)), n(L.tickInterval) || (ia = z(ia, null, Ta, L)));
        Q.tickInterval = ia;
        sc = L.minorTickInterval === "auto" && ia ? ia / 5 : L.minorTickInterval;
        (wa = L.tickPositions || Ua && Ua.apply(Q, [ha, ja])) || (wa = Vb ? (Q.getNonLinearTimeTicks || q)(F(ia, L.units), ha, ja, L.startOfWeek, Q.ordinalPositions, Q.closestPointRange, true) : db ? G(ia, ha, ja) : m(ia, ha, ja));
        if (!qb && (a = wa[0], c = wa[wa.length - 1], L.startOnTick ? ha = a : ha > a && wa.shift(), L.endOnTick ? ja = c : ja < c && wa.pop(), Sb || (Sb = {
          x: 0,
          y: 0
        }), !Vb && wa.length > Sb[Ia] && L.alignTicks !== false)) Sb[Ia] = wa.length
      }
      function s(a) {
        a = (new e(a)).render();
        za.push(a);
        return a
      }
      function qa() {
        var a = L.title,
          c = L.stackLabels,
          f = L.alternateGridColor,
          g = L.lineWidth,
          h, v, l = B.hasRendered && n(ga) && !isNaN(ga),
          k = (h = Q.series.length && n(ha) && n(ja)) || o(L.showEmpty, true),
          m, G;
        if (h || qb) if (sc && !Ba && J(N(), function (a) {
          bc[a] || (bc[a] = new d(a, "minor"));
          l && bc[a].isNew && bc[a].render(null, true);
          bc[a].isActive = true;
          bc[a].render()
        }), J(wa.slice(1).concat([wa[0]]), function (a, b) {
          b = b === wa.length - 1 ? 0 : b + 1;
          if (!qb || a >= ha && a <= ja) na[a] || (na[a] = new d(a)), l && na[a].isNew && na[a].render(b, true), na[a].isActive = true, na[a].render(b)
        }), f && J(wa, function (a, b) {
          if (b % 2 === 0 && a < ja) jc[a] || (jc[a] = new e), m = a, G = wa[b + 1] !== P ? wa[b + 1] : ja, jc[a].options = {
            from: db ? j(m) : m,
            to: db ? j(G) : G,
            color: f
          }, jc[a].render(), jc[a].isActive = true
        }), !Q._addedPlotLB) J((L.plotLines || []).concat(L.plotBands || []), function (a) {
          s(a)
        }), Q._addedPlotLB = true;
        J([na, bc, jc], function (a) {
          for (var b in a) a[b].isActive ? a[b].isActive = false : (a[b].destroy(), delete a[b])
        });
        g && (h = pb + (u ? ub : 0) + O, v = fb - Fb - (u ? M : 0) + O, h = ka.crispLine([Aa, U ? pb : h, U ? v : mb, Ma, U ? eb - Z : h, U ? v : fb - Fb], g), X ? X.animate({
          d: h
        }) : X = ka.path(h).attr({
          stroke: L.lineColor,
          "stroke-width": g,
          zIndex: 7
        }).add(), X[k ? "show" : "hide"]());
        if (Ca && k) k = U ? pb : mb, g = b(a.style.fontSize || 12), k = {
          low: k + (U ? 0 : V),
          middle: k + V / 2,
          high: k + (U ? V : 0)
        }[a.align], g = (U ? mb + M : pb) + (U ? 1 : -1) * (u ? -1 : 1) * Xa + (C === 2 ? g : 0), Ca[Ca.isNew ? "attr" : "animate"]({
          x: U ? k : g + (u ? ub : 0) + O + (a.x || 0),
          y: U ? g - (u ? M : 0) + O : k + (a.y || 0)
        }), Ca.isNew = false;
        if (c && c.enabled) {
          var z, R, c = Q.stackTotalGroup;
          if (!c) Q.stackTotalGroup = c = ka.g("stack-labels").attr({
            visibility: sb,
            zIndex: 6
          }).translate(xa, ta).add();
          for (z in $a) for (R in a = $a[z], a) a[R].render(c)
        }
        Q.isDirty = false
      }
      function w(a) {
        for (var b = za.length; b--;) za[b].id === a && za[b].destroy()
      }
      var Qa = c.isX,
        u = c.opposite,
        U = Ga ? !Qa : Qa,
        C = U ? u ? 0 : 2 : u ? 1 : 3,
        $a = {},
        L = da(Qa ? Db : vc, [kc, pc, qc, Ac][C], c),
        Q = this,
        Ca, K = L.type,
        Vb = K === "datetime",
        db = K === "logarithmic",
        O = L.offset || 0,
        Ia = Qa ? "x" : "y",
        V = 0,
        oa, I, rc, ab, pb, mb, ub, M, Fb, Z, pa, va, sa, Y, fa, X, T, ra, Gb = L.minRange || L.maxZoom,
        aa = L.range,
        ic, ba, lc, Da, ja = null,
        ha = null,
        ga, bb, ma = L.minPadding,
        ua = L.maxPadding,
        Ea = 0,
        qb = n(L.linkedTo),
        la, Cc, Dc, ya, K = L.events,
        Ka, za = [],
        ia, sc, Ta, wa, Ua = L.tickPositioner,
        na = {},
        bc = {},
        jc = {},
        Oa, Ra, Xa, Ba = L.categories,
        hb = L.labels.formatter ||
      function () {
        var a = this.value,
          b = this.dateTimeLabelFormat;
        return b ? nc(b, a) : ia % 1E6 === 0 ? a / 1E6 + "M" : ia % 1E3 === 0 ? a / 1E3 + "k" : !Ba && a >= 1E3 ? D(a, 0) : a
      }, Wa = U && L.labels.staggerLines, Na = L.reversed, Va = Ba && L.tickmarkPlacement === "between" ? 0.5 : 0;
      d.prototype = {
        addLabel: function () {
          var b = this.pos,
            c = L.labels,
            d = Ba && U && Ba.length && !c.step && !c.staggerLines && !c.rotation && Ya / Ba.length || !U && Ya / 2,
            e = b === wa[0],
            f = b === wa[wa.length - 1],
            g = Ba && n(Ba[b]) ? Ba[b] : b,
            h = this.label,
            v = wa.info,
            l;
          Vb && v && (l = L.dateTimeLabelFormats[v.higherRanks[b] || v.unitName]);
          this.isFirst = e;
          this.isLast = f;
          b = hb.call({
            axis: Q,
            chart: B,
            isFirst: e,
            isLast: f,
            dateTimeLabelFormat: l,
            value: db ? k(j(g)) : g
          });
          d = d && {
            width: E(1, S(d - 2 * (c.padding || 10))) + Sa
          };
          d = a(d, c.style);
          n(h) ? h && h.attr({
            text: b
          }).css(d) : this.label = n(b) && c.enabled ? ka.text(b, 0, 0, c.useHTML).attr({
            align: c.align,
            rotation: c.rotation
          }).css(d).add(Y) : null
        },
        getLabelSize: function () {
          var a = this.label;
          return a ? (this.labelBBox = a.getBBox())[U ? "height" : "width"] : 0
        },
        getLabelSides: function () {
          var a = L.labels,
            b = this.labelBBox.width,
            a = b * {
              left: 0,
              center: 0.5,
              right: 1
            }[a.align] - a.x;
          return [-a, b - a]
        },
        handleOverflow: function (a) {
          var b = true,
            c = this.isFirst,
            d = this.isLast,
            e = this.label,
            f = e.x;
          if (c || d) {
            var g = this.getLabelSides(),
              h = g[0],
              g = g[1],
              j = B.plotLeft,
              n = j + Q.len,
              v = (a = na[wa[a + (c ? 1 : -1)]]) && a.label.x + a.getLabelSides()[c ? 0 : 1];
            c && !Na || d && Na ? f + h < j && (f = j - h, a && f + g > v && (b = false)) : f + g > n && (f = n - g, a && f + h < v && (b = false));
            e.x = f
          }
          return b
        },
        render: function (a, c) {
          var d = this.type,
            e = this.label,
            f = this.pos,
            g = L.labels,
            h = this.gridLine,
            j = d ? d + "Grid" : "grid",
            v = d ? d + "Tick" : "tick",
            l = L[j + "LineWidth"],
            k = L[j + "LineColor"],
            m = L[j + "LineDashStyle"],
            G = L[v + "Length"],
            j = L[v + "Width"] || 0,
            N = L[v + "Color"],
            z = L[v + "Position"],
            v = this.mark,
            R = g.step,
            p = c && jb || fb,
            s = true,
            qa;
          qa = U ? pa(f + Va, null, null, c) + rc : pb + O + (u ? (c && lb || eb) - Z - pb : 0);
          p = U ? p - Fb + O - (u ? M : 0) : p - pa(f + Va, null, null, c) - rc;
          if (l) {
            f = sa(f + Va, l, c);
            if (h === P) {
              h = {
                stroke: k,
                "stroke-width": l
              };
              if (m) h.dashstyle = m;
              if (!d) h.zIndex = 1;
              this.gridLine = h = l ? ka.path(f).attr(h).add(fa) : null
            }!c && h && f && h.animate({
              d: f
            })
          }
          if (j) z === "inside" && (G = -G), u && (G = -G), d = ka.crispLine([Aa, qa, p, Ma, qa + (U ? 0 : -G), p + (U ? G : 0)], j), v ? v.animate({
            d: d
          }) : this.mark = ka.path(d).attr({
            stroke: N,
            "stroke-width": j
          }).add(Y);
          if (e && !isNaN(qa)) qa = qa + g.x - (Va && U ? Va * I * (Na ? -1 : 1) : 0), p = p + g.y - (Va && !U ? Va * I * (Na ? 1 : -1) : 0), n(g.y) || (p += b(e.styles.lineHeight) * 0.9 - e.getBBox().height / 2), Wa && (p += a / (R || 1) % Wa * 16), e.x = qa, e.y = p, this.isFirst && !o(L.showFirstLabel, 1) || this.isLast && !o(L.showLastLabel, 1) ? s = false : !Wa && U && g.overflow === "justify" && !this.handleOverflow(a) && (s = false), R && a % R && (s = false), s ? (e[this.isNew ? "attr" : "animate"]({
            x: e.x,
            y: e.y
          }), e.show(), this.isNew = false) : e.hide()
        },
        destroy: function () {
          H(this)
        }
      };
      e.prototype = {
        render: function () {
          var a = this,
            b = (Q.pointRange || 0) / 2,
            c = a.options,
            d = c.label,
            e = a.label,
            f = c.width,
            g = c.to,
            j = c.from,
            v = c.value,
            l, k = c.dashStyle,
            m = a.svgElem,
            G = [],
            N, z, R = c.color;
          z = c.zIndex;
          var p = c.events;
          db && (j = h(j), g = h(g), v = h(v));
          if (f) {
            if (G = sa(v, f), b = {
              stroke: R,
              "stroke-width": f
            }, k) b.dashstyle = k
          } else if (n(j) && n(g)) j = E(j, ha - b), g = ea(g, ja + b), l = sa(g), (G = sa(j)) && l ? G.push(l[4], l[5], l[1], l[2]) : G = null, b = {
            fill: R
          };
          else return;
          if (n(z)) b.zIndex = z;
          if (m) G ? m.animate({
            d: G
          }, null, m.onGetPath) : (m.hide(), m.onGetPath = function () {
            m.show()
          });
          else if (G && G.length && (a.svgElem = m = ka.path(G).attr(b).add(), p)) for (N in k = function (b) {
            m.on(b, function (c) {
              p[b].apply(a, [c])
            })
          }, p) k(N);
          if (d && n(d.text) && G && G.length && ub > 0 && M > 0) {
            d = da({
              align: U && l && "center",
              x: U ? !l && 4 : 10,
              verticalAlign: !U && l && "middle",
              y: U ? l ? 16 : 10 : l ? 6 : -4,
              rotation: U && !l && 90
            }, d);
            if (!e) a.label = e = ka.text(d.text, 0, 0).attr({
              align: d.textAlign || d.align,
              rotation: d.rotation,
              zIndex: z
            }).css(d.style).add();
            l = [G[1], G[4], o(G[6], G[1])];
            G = [G[2], G[5], o(G[7], G[2])];
            N = cb(l);
            z = cb(G);
            e.align(d, false, {
              x: N,
              y: z,
              width: Ha(l) - N,
              height: Ha(G) - z
            });
            e.show()
          } else e && e.hide();
          return a
        },
        destroy: function () {
          H(this);
          l(za, this)
        }
      };
      f.prototype = {
        destroy: function () {
          H(this)
        },
        setTotal: function (a) {
          this.cum = this.total = a
        },
        render: function (a) {
          var b = this.options.formatter.call(this);
          this.label ? this.label.attr({
            text: b,
            visibility: rb
          }) : this.label = B.renderer.text(b, 0, 0).css(this.options.style).attr({
            align: this.textAlign,
            rotation: this.options.rotation,
            visibility: rb
          }).add(a)
        },
        setOffset: function (a, b) {
          var c = this.isNegative,
            d = Q.translate(this.total, 0, 0, 0, 1),
            e = Q.translate(0),
            e = W(d - e),
            f = B.xAxis[0].translate(this.x) + a,
            g = B.plotHeight,
            c = {
              x: Ga ? c ? d : d - e : f,
              y: Ga ? g - f - b : c ? g - d - e : g - d,
              width: Ga ? e : b,
              height: Ga ? b : e
            };
          this.label && this.label.align(this.alignOptions, null, c).attr({
            visibility: sb
          })
        }
      };
      pa = function (a, b, c, d, e) {
        var f = 1,
          g = 0,
          h = d ? ab : I,
          d = d ? ga : ha,
          e = L.ordinal || db && e;
        h || (h = I);
        c && (f *= -1, g = V);
        Na && (f *= -1, g -= f * V);
        b ? (Na && (a = V - a), a = a / h + d, e && (a = Q.lin2val(a))) : (e && (a = Q.val2lin(a)), a = f * (a - d) * h + g + f * Ea);
        return a
      };
      sa = function (a, b, c) {
        var d, e, f, a = pa(a, null, null, c),
          g = c && jb || fb,
          h = c && lb || eb,
          j, c = e = S(a + rc);
        d = f = S(g - a - rc);
        if (isNaN(a)) j = true;
        else if (U) {
          if (d = mb, f = g - Fb, c < pb || c > pb + ub) j = true
        } else if (c = pb, e = h - Z, d < mb || d > mb + M) j = true;
        return j ? null : ka.crispLine([Aa, c, d, Ma, e, f], b || 0)
      };
      va = function () {
        var a = ja - ha,
          b = 0,
          c, d;
        if (Qa) qb ? b = la.pointRange : J(Q.series, function (a) {
          b = E(b, a.pointRange);
          d = a.closestPointRange;
          !a.noSharedTooltip && n(d) && (c = n(c) ? ea(c, d) : d)
        }), Q.pointRange = b, Q.closestPointRange = c;
        ab = I;
        Q.translationSlope = I = V / (a + b || 1);
        rc = U ? pb : Fb;
        Ea = I * (b / 2)
      };
      ib.push(Q);
      B[Qa ? "xAxis" : "yAxis"].push(Q);
      Ga && Qa && Na === P && (Na = true);
      a(Q, {
        addPlotBand: s,
        addPlotLine: s,
        adjustTickAmount: function () {
          if (Sb && Sb[Ia] && !Vb && !Ba && !qb && L.alignTicks !== false) {
            var a = Oa,
              b = wa.length;
            Oa = Sb[Ia];
            if (b < Oa) {
              for (; wa.length < Oa;) wa.push(k(wa[wa.length - 1] + ia));
              I *= (b - 1) / (Oa - 1);
              ja = wa[wa.length - 1]
            }
            if (n(a) && Oa !== a) Q.isDirty = true
          }
        },
        categories: Ba,
        getExtremes: function () {
          return {
            min: db ? k(j(ha)) : ha,
            max: db ? k(j(ja)) : ja,
            dataMin: T,
            dataMax: ra,
            userMin: ic,
            userMax: ba
          }
        },
        getPlotLinePath: sa,
        getThreshold: function (a) {
          var b = db ? j(ha) : ha,
            c = db ? j(ja) : ja;
          b > a || a === null ? a = b : c < a && (a = c);
          return pa(a, 0, 1, 0, 1)
        },
        isXAxis: Qa,
        options: L,
        plotLinesAndBands: za,
        getOffset: function () {
          var a = Q.series.length && n(ha) && n(ja),
            b = a || o(L.showEmpty, true),
            c = 0,
            e, f = 0,
            g = L.title,
            h = L.labels,
            j = [-1, 1, 1, -1][C],
            v;
          Y || (Y = ka.g("axis").attr({
            zIndex: 7
          }).add(), fa = ka.g("grid").attr({
            zIndex: L.gridZIndex || 1
          }).add());
          Ra = 0;
          if (a || qb) J(wa, function (a) {
            na[a] ? na[a].addLabel() : na[a] = new d(a)
          }), J(wa, function (a) {
            if (C === 0 || C === 2 || {
              1: "left",
              3: "right"
            }[C] === h.align) Ra = E(na[a].getLabelSize(), Ra)
          }), Wa && (Ra += (Wa - 1) * 16);
          else for (v in na) na[v].destroy(), delete na[v];
          if (g && g.text) {
            if (!Ca) Ca = Q.axisTitle = ka.text(g.text, 0, 0, g.useHTML).attr({
              zIndex: 7,
              rotation: g.rotation || 0,
              align: g.textAlign || {
                low: "left",
                middle: "center",
                high: "right"
              }[g.align]
            }).css(g.style).add(), Ca.isNew = true;
            if (b) c = Ca.getBBox()[U ? "height" : "width"], f = o(g.margin, U ? 5 : 10), e = g.offset;
            Ca[b ? "show" : "hide"]()
          }
          O = j * o(L.offset, Fa[C]);
          Xa = o(e, Ra + f + (C !== 2 && Ra && j * L.labels[U ? "y" : "x"]));
          Fa[C] = E(Fa[C], Xa + c + j * O)
        },
        render: qa,
        setAxisSize: function () {
          var a = L.offsetLeft || 0,
            b = L.offsetRight || 0;
          pb = o(L.left, xa + a);
          mb = o(L.top, ta);
          ub = o(L.width, Ya - a + b);
          M = o(L.height, Za);
          Fb = fb - M - mb;
          Z = eb - ub - pb;
          V = U ? ub : M;
          Q.left = pb;
          Q.top = mb;
          Q.len = V
        },
        setAxisTranslation: va,
        setCategories: function (a, b) {
          Q.categories = c.categories = Ba = a;
          J(Q.series, function (a) {
            a.translate();
            a.setTooltipPoints(true)
          });
          Q.isDirty = true;
          o(b, true) && B.redraw()
        },
        setExtremes: function (b, c, d, e, f) {
          d = o(d, true);
          f = a(f, {
            min: b,
            max: c
          });
          Ja(Q, "setExtremes", f, function () {
            ic = b;
            ba = c;
            Q.isDirtyExtremes = true;
            d && B.redraw(e)
          })
        },
        setScale: function () {
          var a, b, c, d;
          ga = ha;
          bb = ja;
          oa = V;
          V = U ? ub : M;
          d = V !== oa;
          J(Q.series, function (a) {
            if (a.isDirtyData || a.isDirty || a.xAxis.isDirty) c = true
          });
          if (d || c || qb || ic !== lc || ba !== Da) {
            v();
            p();
            lc = ic;
            Da = ba;
            if (!Qa) for (a in $a) for (b in $a[a]) $a[a][b].cum = $a[a][b].total;
            if (!Q.isDirty) Q.isDirty = d || ha !== ga || ja !== bb
          }
        },
        setTickPositions: p,
        translate: pa,
        redraw: function () {
          cc.resetTracker && cc.resetTracker();
          qa();
          J(za, function (a) {
            a.render()
          });
          J(Q.series, function (a) {
            a.isDirty = true
          })
        },
        removePlotBand: w,
        removePlotLine: w,
        reversed: Na,
        setTitle: function (a, b) {
          L.title = da(L.title, a);
          Ca = Ca.destroy();
          Q.isDirty = true;
          o(b, true) && B.redraw()
        },
        series: [],
        stacks: $a,
        destroy: function () {
          var a;
          nb(Q);
          for (a in $a) H($a[a]), $a[a] = null;
          if (Q.stackTotalGroup) Q.stackTotalGroup = Q.stackTotalGroup.destroy();
          J([na, bc, jc, za], function (a) {
            H(a)
          });
          J([X, Y, fa, Ca], function (a) {
            a && a.destroy()
          });
          X = Y = fa = Ca = null
        }
      });
      for (Ka in K) Pa(Q, Ka, K[Ka]);
      if (db) Q.val2lin = h, Q.lin2val = j
    }
    function R(a) {
      function c() {
        var b = this.points || p(this),
          d = b[0].series,
          e;
        e = [d.tooltipHeaderFormatter(b[0].key)];
        J(b, function (a) {
          d = a.series;
          e.push(d.tooltipFormatter && d.tooltipFormatter(a) || a.point.tooltipFormatter(d.tooltipOptions.pointFormat))
        });
        e.push(a.footerFormat || "");
        return e.join("")
      }
      function d(a, b) {
        m = k ? a : (2 * m + a) / 3;
        G = k ? b : (G + b) / 2;
        N.attr({
          x: m,
          y: G
        });
        Nb = W(a - m) > 1 || W(b - G) > 1 ?
        function () {
          d(a, b)
        } : null
      }
      function e() {
        if (!k) {
          var a = B.hoverPoints;
          N.hide();
          a && J(a, function (a) {
            a.setState()
          });
          B.hoverPoints = null;
          k = true
        }
      }
      var f, g = a.borderWidth,
        h = a.crosshairs,
        j = [],
        v = a.style,
        n = a.shared,
        l = b(v.padding),
        k = true,
        m = 0,
        G = 0;
      v.padding = 0;
      var N = ka.label("", 0, 0, null, null, null, a.useHTML).attr({
        padding: l,
        fill: a.backgroundColor,
        "stroke-width": g,
        r: a.borderRadius,
        zIndex: 8
      }).css(v).hide().add();
      oa || N.shadow(a.shadow);
      return {
        shared: n,
        refresh: function (b) {
          var g, v, l, m, G = {},
            z = [];
          l = b.tooltipPos;
          g = a.formatter || c;
          var G = B.hoverPoints,
            R;
          n && (!b.series || !b.series.noSharedTooltip) ? (m = 0, G && J(G, function (a) {
            a.setState()
          }), B.hoverPoints = b, J(b, function (a) {
            a.setState(wb);
            m += a.plotY;
            z.push(a.getLabelConfig())
          }), v = b[0].plotX, m = S(m) / b.length, G = {
            x: b[0].category
          }, G.points = z, b = b[0]) : G = b.getLabelConfig();
          G = g.call(G);
          f = b.series;
          v = o(v, b.plotX);
          m = o(m, b.plotY);
          g = S(l ? l[0] : Ga ? Ya - m : v);
          v = S(l ? l[1] : Ga ? Za - v : m);
          l = n || !f.isCartesian || f.tooltipOutsidePlot || Na(g, v);
          G === false || !l ? e() : (k && (N.show(), k = false), N.attr({
            text: G
          }), R = a.borderColor || b.color || f.color || "#606060", N.attr({
            stroke: R
          }), l = O(N.width, N.height, xa, ta, Ya, Za, {
            x: g,
            y: v
          }, o(a.distance, 12), Ga), d(S(l.x), S(l.y)));
          if (h) {
            h = p(h);
            var s;
            l = h.length;
            for (var qa; l--;) if (s = b.series[l ? "yAxis" : "xAxis"], h[l] && s) if (s = s.getPlotLinePath(l ? o(b.stackY, b.y) : b.x, 1), j[l]) j[l].attr({
              d: s,
              visibility: sb
            });
            else {
              qa = {
                "stroke-width": h[l].width || 1,
                stroke: h[l].color || "#C0C0C0",
                zIndex: h[l].zIndex || 2
              };
              if (h[l].dashStyle) qa.dashstyle = h[l].dashStyle;
              j[l] = ka.path(s).attr(qa).add()
            }
          }
          Ja(B, "tooltipRefresh", {
            text: G,
            x: g + xa,
            y: v + ta,
            borderColor: R
          })
        },
        hide: e,
        hideCrosshairs: function () {
          J(j, function (a) {
            a && a.hide()
          })
        },
        destroy: function () {
          J(j, function (a) {
            a && a.destroy()
          });
          N && (N = N.destroy())
        }
      }
    }
    function s(b) {
      function c(b) {
        var d, e, f, b = b || M.event;
        if (!b.target) b.target = b.srcElement;
        if (b.originalEvent) b = b.originalEvent;
        if (b.event) b = b.event;
        d = b.touches ? b.touches.item(0) : b;
        ac = uc(ga);
        e = ac.left;
        f = ac.top;
        za ? (e = b.x, d = b.y) : (e = d.pageX - e, d = d.pageY - f);
        return a(b, {
          chartX: S(e),
          chartY: S(d)
        })
      }
      function d(a) {
        var b = {
          xAxis: [],
          yAxis: []
        };
        J(ib, function (c) {
          var d = c.translate,
            e = c.isXAxis;
          b[e ? "xAxis" : "yAxis"].push({
            axis: c,
            value: d((Ga ? !e : e) ? a.chartX - xa : Za - a.chartY + ta, true)
          })
        });
        return b
      }
      function e() {
        var a = B.hoverSeries,
          b = B.hoverPoint;
        if (b) b.onMouseOut();
        if (a) a.onMouseOut();
        dc && (dc.hide(), dc.hideCrosshairs());
        Pb = null
      }
      function f() {
        if (k) {
          var a = {
            xAxis: [],
            yAxis: []
          },
            b = k.getBBox(),
            c = b.x - xa,
            d = b.y - ta;
          l && (J(ib, function (e) {
            if (e.options.zoomEnabled !== false) {
              var f = e.translate,
                g = e.isXAxis,
                h = Ga ? !g : g,
                j = f(h ? c : Za - d - b.height, true, 0, 0, 1),
                f = f(h ? c + b.width : Za - d, true, 0, 0, 1);
              a[g ? "xAxis" : "yAxis"].push({
                axis: e,
                min: ea(j, f),
                max: E(j, f)
              })
            }
          }), Ja(B, "selection", a, Zb));
          k = k.destroy()
        }
        u(ga, {
          cursor: "auto"
        });
        B.mouseIsDown = Eb = l = false;
        nb(Y, ra ? "touchend" : "mouseup", f)
      }
      function g(a) {
        var b = n(a.pageX) ? a.pageX : a.page.x,
          a = n(a.pageX) ? a.pageY : a.page.y;
        ac && !Na(b - ac.left - xa, a - ac.top - ta) && e()
      }
      function h() {
        e();
        ac = null
      }
      var j, v, l, k, G = oa ? "" : ca.zoomType,
        o = /x/.test(G),
        N = /y/.test(G),
        z = o && !Ga || N && Ga,
        p = N && !Ga || o && Ga;
      if (!Kb) B.trackerGroup = Kb = ka.g("tracker").attr({
        zIndex: 9
      }).add();
      if (b.enabled) B.tooltip = dc = R(b), Yb = setInterval(function () {
        Nb && Nb()
      }, 32);
      (function () {
        ga.onmousedown = function (a) {
          a = c(a);
          !ra && a.preventDefault && a.preventDefault();
          B.mouseIsDown = Eb = true;
          B.mouseDownX = j = a.chartX;
          v = a.chartY;
          Pa(Y, ra ? "touchend" : "mouseup", f)
        };
        var n = function (a) {
            if (!a || !(a.touches && a.touches.length > 1)) {
              a = c(a);
              if (!ra) a.returnValue = false;
              var d = a.chartX,
                e = a.chartY,
                f = !Na(d - xa, e - ta);
              ra && a.type === "touchstart" && (m(a.target, "isTracker") ? B.runTrackerClick || a.preventDefault() : !yb && !f && a.preventDefault());
              f && (d < xa ? d = xa : d > xa + Ya && (d = xa + Ya), e < ta ? e = ta : e > ta + Za && (e = ta + Za));
              if (Eb && a.type !== "touchstart") {
                if (l = Math.sqrt(Math.pow(j - d, 2) + Math.pow(v - e, 2)), l > 10) {
                  var g = Na(j - xa, v - ta);
                  if (vb && (o || N) && g) k || (k = ka.rect(xa, ta, z ? 1 : Ya, p ? 1 : Za, 0).attr({
                    fill: ca.selectionMarkerFill || "rgba(69,114,167,0.25)",
                    zIndex: 7
                  }).add());
                  k && z && (a = d - j, k.attr({
                    width: W(a),
                    x: (a > 0 ? 0 : a) + j
                  }));
                  k && p && (e -= v, k.attr({
                    height: W(e),
                    y: (e > 0 ? 0 : e) + v
                  }));
                  g && !k && ca.panning && B.pan(d)
                }
              } else if (!f) {
                var h, d = B.hoverPoint,
                  e = B.hoverSeries,
                  n, G, g = eb,
                  R = Ga ? a.chartY : a.chartX - xa;
                if (dc && b.shared && (!e || !e.noSharedTooltip)) {
                  h = [];
                  n = Ka.length;
                  for (G = 0; G < n; G++) if (Ka[G].visible && Ka[G].options.enableMouseTracking !== false && !Ka[G].noSharedTooltip && Ka[G].tooltipPoints.length) a = Ka[G].tooltipPoints[R], a._dist = W(R - a.plotX), g = ea(g, a._dist), h.push(a);
                  for (n = h.length; n--;) h[n]._dist > g && h.splice(n, 1);
                  if (h.length && h[0].plotX !== Pb) dc.refresh(h), Pb = h[0].plotX
                }
                if (e && e.tracker && (a = e.tooltipPoints[R]) && a !== d) a.onMouseOver()
              }
              return f || !vb
            }
          };
        ga.onmousemove = n;
        Pa(ga, "mouseleave", h);
        Pa(Y, "mousemove", g);
        ga.ontouchstart = function (a) {
          if (o || N) ga.onmousedown(a);
          n(a)
        };
        ga.ontouchmove = n;
        ga.ontouchend = function () {
          l && e()
        };
        ga.onclick = function (b) {
          var e = B.hoverPoint,
            b = c(b);
          b.cancelBubble = true;
          if (!l) if (e && (m(b.target, "isTracker") || m(b.target.parentNode, "isTracker"))) {
            var f = e.plotX,
              g = e.plotY;
            a(e, {
              pageX: ac.left + xa + (Ga ? Ya - g : f),
              pageY: ac.top + ta + (Ga ? Za - f : g)
            });
            Ja(e.series, "click", a(b, {
              point: e
            }));
            e.firePointEvent("click", b)
          } else a(b, d(b)), Na(b.chartX - xa, b.chartY - ta) && Ja(B, "click", b);
          l = false
        }
      })();
      a(this, {
        zoomX: o,
        zoomY: N,
        resetTracker: e,
        normalizeMouseEvent: c,
        destroy: function () {
          if (B.trackerGroup) B.trackerGroup = Kb = B.trackerGroup.destroy();
          nb(ga, "mouseleave", h);
          nb(Y, "mousemove", g);
          ga.onclick = ga.onmousedown = ga.onmousemove = ga.ontouchstart = ga.ontouchend = ga.ontouchmove = null
        }
      })
    }
    function Qa(a) {
      var b = a.type || ca.type || ca.defaultSeriesType,
        c = tb[b],
        d = B.hasRendered;
      if (d) if (Ga && b === "column") c = tb.bar;
      else if (!Ga && b === "bar") c = tb.column;
      b = new c;
      b.init(B, a);
      !d && b.inverted && (Ga = true);
      if (b.isCartesian) vb = b.isCartesian;
      Ka.push(b);
      return b
    }
    function U() {
      ca.alignTicks !== false && J(ib, function (a) {
        a.adjustTickAmount()
      });
      Sb = null
    }
    function C(a) {
      var b = B.isDirtyLegend,
        c, d = B.isDirtyBox,
        e = Ka.length,
        f = e,
        g = B.clipRect;
      for (va(a, B); f--;) if (a = Ka[f], a.isDirty && a.options.stacking) {
        c = true;
        break
      }
      if (c) for (f = e; f--;) if (a = Ka[f], a.options.stacking) a.isDirty = true;
      J(Ka, function (a) {
        a.isDirty && a.options.legendType === "point" && (b = true)
      });
      if (b && zb.renderLegend) zb.renderLegend(), B.isDirtyLegend = false;
      vb && (Ib || (Sb = null, J(ib, function (a) {
        a.setScale()
      })), U(), Cb(), J(ib, function (a) {
        if (a.isDirtyExtremes) a.isDirtyExtremes = false, Ja(a, "afterSetExtremes", a.getExtremes());
        if (a.isDirty || d) a.redraw(), d = true
      }));
      d && (Qb(), g && (Rb(g), g.animate({
        width: B.plotSizeX,
        height: B.plotSizeY + 1
      })));
      J(Ka, function (a) {
        a.isDirty && a.visible && (!a.isCartesian || a.xAxis) && a.redraw()
      });
      cc && cc.resetTracker && cc.resetTracker();
      ka.draw();
      Ja(B, "redraw")
    }
    function L() {
      var a = I.xAxis || {},
        b = I.yAxis || {},
        a = p(a);
      J(a, function (a, b) {
        a.index = b;
        a.isX = true
      });
      b = p(b);
      J(b, function (a, b) {
        a.index = b
      });
      a = a.concat(b);
      J(a, function (a) {
        new g(a)
      });
      U()
    }
    function Ca() {
      var a = Ba.lang,
        b = ca.resetZoomButton,
        c = b.theme,
        d = c.states,
        e = b.relativeTo === "chart" ? null : {
          x: xa,
          y: ta,
          width: Ya,
          height: Za
        };
      B.resetZoomButton = ka.button(a.resetZoom, null, null, gc, c, d && d.hover).attr({
        align: b.position.align,
        title: a.resetZoomTitle
      }).add().align(b.position, false, e)
    }
    function Q(a, b) {
      ma = da(I.title, a);
      ua = da(I.subtitle, b);
      J([
        ["title", a, ma],
        ["subtitle", b, ua]
      ], function (a) {
        var b = a[0],
          c = B[b],
          d = a[1],
          a = a[2];
        c && d && (c = c.destroy());
        a && a.text && !c && (B[b] = ka.text(a.text, 0, 0, a.useHTML).attr({
          align: a.align,
          "class": Bb + b,
          zIndex: a.zIndex || 4
        }).css(a.style).add().align(a, false, bb))
      })
    }
    function db() {
      Da = ca.renderTo;
      Ta = Bb + mc++;
      c(Da) && (Da = Y.getElementById(Da));
      Da || gb(13, true);
      Da.innerHTML = "";
      Da.offsetWidth || (ya = Da.cloneNode(0), u(ya, {
        position: Mb,
        top: "-9999px",
        display: ""
      }), Y.body.appendChild(ya));
      ia = (ya || Da).offsetWidth;
      Ua = (ya || Da).offsetHeight;
      B.chartWidth = eb = ca.width || ia || 600;
      B.chartHeight = fb = ca.height || (Ua > 19 ? Ua : 400);
      B.container = ga = w(Hb, {
        className: Bb + "container" + (ca.className ? " " + ca.className : ""),
        id: Ta
      }, a({
        position: tc,
        overflow: rb,
        width: eb + Sa,
        height: fb + Sa,
        textAlign: "left",
        lineHeight: "normal"
      }, ca.style), ya || Da);
      B.renderer = ka = ca.forExport ? new ba(ga, eb, fb, true) : new pa(ga, eb, fb);
      oa && ka.create(B, ga, eb, fb);
      var d, e;
      Ra && ga.getBoundingClientRect && (d = function () {
        u(ga, {
          left: 0,
          top: 0
        });
        e = ga.getBoundingClientRect();
        u(ga, {
          left: -(e.left - b(e.left)) + Sa,
          top: -(e.top - b(e.top)) + Sa
        })
      }, d(), Pa(M, "resize", d), Pa(B, "destroy", function () {
        nb(M, "resize", d)
      }))
    }
    function Vb() {
      function a(c) {
        var d = ca.width || Da.offsetWidth,
          e = ca.height || Da.offsetHeight,
          c = c ? c.target : M;
        if (d && e && (c === M || c === Y)) {
          if (d !== ia || e !== Ua) clearTimeout(b), b = setTimeout(function () {
            Xb(d, e, false)
          }, 100);
          ia = d;
          Ua = e
        }
      }
      var b;
      Pa(M, "resize", a);
      Pa(B, "destroy", function () {
        nb(M, "resize", a)
      })
    }
    function Ia() {
      B && Ja(B, "endResize", null, function () {
        Ib -= 1
      })
    }
    function pb() {
      for (var a = Ga || ca.inverted || ca.type === "bar" || ca.defaultSeriesType === "bar", b = I.series, c = b && b.length; !a && c--;) b[c].type === "bar" && (a = true);
      B.inverted = Ga = a
    }
    function mb() {
      var c = I.labels,
        d = I.credits,
        e;
      Q();
      zb = B.legend = new oc;
      J(ib, function (a) {
        a.setScale()
      });
      Cb();
      J(ib, function (a) {
        a.setTickPositions(true)
      });
      U();
      Cb();
      Qb();
      vb && J(ib, function (a) {
        a.render()
      });
      if (!B.seriesGroup) B.seriesGroup = ka.g("series-group").attr({
        zIndex: 3
      }).add();
      J(Ka, function (a) {
        a.translate();
        a.setTooltipPoints();
        a.render()
      });
      c.items && J(c.items, function () {
        var d = a(c.style, this.style),
          e = b(d.left) + xa,
          f = b(d.top) + ta + 12;
        delete d.left;
        delete d.top;
        ka.text(this.html, e, f).attr({
          zIndex: 2
        }).css(d).add()
      });
      if (d.enabled && !B.credits) e = d.href, B.credits = ka.text(d.text, 0, 0).on("click", function () {
        if (e) location.href = e
      }).attr({
        align: d.position.align,
        zIndex: 8
      }).css(d.style).add().align(d.position);
      B.hasRendered = true
    }
    function ub() {
      if (!hb && M == M.top && Y.readyState !== "complete" || oa && !M.canvg) oa ? Bc.push(ub, I.global.canvasToolsURL) : Y.attachEvent("onreadystatechange", function () {
        Y.detachEvent("onreadystatechange", ub);
        Y.readyState === "complete" && ub()
      });
      else {
        db();
        Ja(B, "init");
        if (Highcharts.RangeSelector && I.rangeSelector.enabled) B.rangeSelector = new Highcharts.RangeSelector(B);
        Tb();
        Ub();
        pb();
        L();
        J(I.series || [], function (a) {
          Qa(a)
        });
        if (Highcharts.Scroller && (I.navigator.enabled || I.scrollbar.enabled)) B.scroller = new Highcharts.Scroller(B);
        B.render = mb;
        B.tracker = cc = new s(I.tooltip);
        mb();
        ka.draw();
        f && f.apply(B, [B]);
        J(B.callbacks, function (a) {
          a.apply(B, [B])
        });
        ya && (Da.appendChild(ga), sa(ya));
        Ja(B, "load")
      }
    }
    var I, ab = e.series;
    e.series = null;
    I = da(Ba, e);
    I.series = e.series = ab;
    var ca = I.chart,
      ab = ca.margin,
      ab = d(ab) ? ab : [ab, ab, ab, ab],
      Z = o(ca.marginTop, ab[0]),
      fa = o(ca.marginRight, ab[1]),
      X = o(ca.marginBottom, ab[2]),
      aa = o(ca.marginLeft, ab[3]),
      T = ca.spacingTop,
      lc = ca.spacingRight,
      Fb = ca.spacingBottom,
      qb = ca.spacingLeft,
      bb, ma, ua, ta, la, Ea, xa, Fa, Da, ya, ga, Ta, ia, Ua, eb, fb, lb, jb, na, Oa, Va, Xa, B = this,
      yb = (ab = ca.events) && !! ab.click,
      Ab, Na, dc, Eb, ec, Ob, Jb, Za, Ya, cc, Kb, zb, Lb, fc, ac, vb = ca.showAxes,
      Ib = 0,
      ib = [],
      Sb, Ka = [],
      Ga, ka, Nb, Yb, Pb, Qb, Cb, Tb, Ub, Xb, Zb, gc, oc = function () {
        function c(a, b) {
          var d = a.legendItem,
            e = a.legendLine,
            f = a.legendSymbol,
            g = o.color,
            j = b ? h.itemStyle.color : g,
            g = b ? a.color : g;
          d && d.css({
            fill: j
          });
          e && e.attr({
            stroke: g
          });
          f && f.attr({
            stroke: g,
            fill: g
          })
        }
        function d(a) {
          var b = a.legendItem,
            c = a.legendLine,
            e = a._legendItemPos,
            f = e[0],
            e = e[1],
            g = a.legendSymbol,
            a = a.checkbox;
          b && b.attr({
            x: z ? f : Lb - f,
            y: e
          });
          c && c.translate(z ? f : Lb - f, e - 4);
          g && (b = f + g.xOff, g.attr({
            x: z ? b : Lb - b,
            y: e + g.yOff
          }));
          if (a) a.x = f, a.y = e
        }
        function e() {
          J(n, function (a) {
            var b = a.checkbox,
              c = Ca.alignAttr;
            b && u(b, {
              left: c.translateX + a.legendItemWidth + b.x - 40 + Sa,
              top: c.translateY + b.y - 11 + Sa
            })
          })
        }
        function f(a) {
          var b, d, e, g, n = a.legendItem;
          g = a.series || a;
          var k = g.options,
            s = k && k.borderWidth || 0;
          if (!n) {
            g = /^(bar|pie|area|column)$/.test(g.type);
            a.legendItem = n = ka.text(h.labelFormatter.call(a), 0, 0, h.useHTML).css(a.visible ? m : o).on("mouseover", function () {
              a.setState(wb);
              n.css(G)
            }).on("mouseout", function () {
              n.css(a.visible ? m : o);
              a.setState()
            }).on("click", function () {
              var b = function () {
                  a.setVisible()
                };
              a.firePointEvent ? a.firePointEvent("legendItemClick", null, b) : Ja(a, "legendItemClick", null, b)
            }).attr({
              align: z ? "left" : "right",
              zIndex: 2
            }).add(Ca);
            if (!g && k && k.lineWidth) {
              var u = {
                "stroke-width": k.lineWidth,
                zIndex: 2
              };
              if (k.dashStyle) u.dashstyle = k.dashStyle;
              a.legendLine = ka.path([Aa, (-v - l) * (z ? 1 : -1), 0, Ma, -l * (z ? 1 : -1), 0]).attr(u).add(Ca)
            }
            if (g) e = ka.rect(b = -v - l, d = -11, v, 12, 2).attr({
              zIndex: 3
            }).add(Ca), z || (b += v);
            else if (k && k.marker && k.marker.enabled) e = k.marker.radius, e = ka.symbol(a.symbol, b = -v / 2 - l - e, d = -4 - e, 2 * e, 2 * e).attr(a.pointAttr[Wa]).attr({
              zIndex: 3
            }).add(Ca), z || (b += v / 2);
            if (e) e.xOff = b + s % 2 / 2, e.yOff = d + s % 2 / 2;
            a.legendSymbol = e;
            c(a, a.visible);
            if (k && k.showCheckbox) a.checkbox = w("input", {
              type: "checkbox",
              checked: a.selected,
              defaultChecked: a.selected
            }, h.itemCheckboxStyle, ga), Pa(a.checkbox, "click", function (b) {
              Ja(a, "checkboxClick", {
                checked: b.target.checked
              }, function () {
                a.select()
              })
            })
          }
          b = n.getBBox();
          d = a.legendItemWidth = h.itemWidth || v + l + b.width + N;
          $a = b.height;
          if (j && Qa - q + d > (A || eb - 2 * N - q)) Qa = q, C += R + $a + p;
          !j && C + h.y + $a > fb - T - Fb && (C = U, Qa += qa, qa = 0);
          qa = E(qa, d);
          L = E(L, C + p);
          a._legendItemPos = [Qa, C];
          j ? Qa += d : C += R + $a + p;
          K = A || E(Qa - q + (j ? 0 : d), K)
        }
        function g() {
          Qa = q;
          C = U;
          L = K = 0;
          Ca || (Ca = ka.g("legend").attr({
            zIndex: 7
          }).add());
          n = [];
          J(db, function (a) {
            var b = a.options;
            b.showInLegend && (n = n.concat(a.legendItems || (b.legendType === "point" ? a.data : a)))
          });
          V(n, function (a, b) {
            return (a.options.legendIndex || 0) - (b.options.legendIndex || 0)
          });
          Vb && n.reverse();
          J(n, f);
          Lb = A || K;
          fc = L - s + $a;
          if (Q || D) {
            Lb += 2 * N;
            fc += 2 * N;
            if (F) {
              if (Lb > 0 && fc > 0) F[F.isNew ? "attr" : "animate"](F.crisp(null, null, null, Lb, fc)), F.isNew = false
            } else F = ka.rect(0, 0, Lb, fc, h.borderRadius, Q || 0).attr({
              stroke: h.borderColor,
              "stroke-width": Q || 0,
              fill: D || ob
            }).add(Ca).shadow(h.shadow), F.isNew = true;
            F[n.length ? "show" : "hide"]()
          }
          J(n, d);
          for (var c = ["left", "right", "top", "bottom"], j, v = 4; v--;) j = c[v], k[j] && k[j] !== "auto" && (h[v < 2 ? "align" : "verticalAlign"] = j, h[v < 2 ? "x" : "y"] = b(k[j]) * (v % 2 ? -1 : 1));
          n.length && Ca.align(a(h, {
            width: Lb,
            height: fc
          }), true, bb);
          Ib || e()
        }
        var h = B.options.legend;
        if (h.enabled) {
          var j = h.layout === "horizontal",
            v = h.symbolWidth,
            l = h.symbolPadding,
            n, k = h.style,
            m = h.itemStyle,
            G = h.itemHoverStyle,
            o = da(m, h.itemHiddenStyle),
            N = h.padding || b(k.padding),
            z = !h.rtl,
            R = h.itemMarginTop || 0,
            p = h.itemMarginBottom || 0,
            s = 18,
            qa = 0,
            q = 4 + N + v + l,
            U = N + R + s - 5,
            Qa, C, L, $a = 0,
            F, Q = h.borderWidth,
            D = h.backgroundColor,
            Ca, K, A = h.width,
            db = B.series,
            Vb = h.reversed;
          g();
          Pa(B, "endResize", e);
          return {
            colorizeItem: c,
            destroyItem: function (a) {
              var b = a.checkbox;
              J(["legendItem", "legendLine", "legendSymbol"], function (b) {
                a[b] && a[b].destroy()
              });
              b && sa(a.checkbox)
            },
            renderLegend: g,
            destroy: function () {
              F && (F = F.destroy());
              Ca && (Ca = Ca.destroy())
            }
          }
        }
      };
    Na = function (a, b) {
      return a >= 0 && a <= Ya && b >= 0 && b <= Za
    };
    gc = function () {
      var a = B.resetZoomButton;
      Ja(B, "selection", {
        resetSelection: true
      }, Zb);
      if (a) B.resetZoomButton = a.destroy()
    };
    Zb = function (a) {
      var b;
      B.resetZoomEnabled !== false && !B.resetZoomButton && Ca();
      !a || a.resetSelection ? J(ib, function (a) {
        a.options.zoomEnabled !== false && (a.setExtremes(null, null, false), b = true)
      }) : J(a.xAxis.concat(a.yAxis), function (a) {
        var c = a.axis;
        if (B.tracker[c.isXAxis ? "zoomX" : "zoomY"]) c.setExtremes(a.min, a.max, false), b = true
      });
      b && C(o(ca.animation, B.pointCount < 100))
    };
    B.pan = function (a) {
      var b = B.xAxis[0],
        c = B.mouseDownX,
        d = b.pointRange / 2,
        e = b.getExtremes(),
        f = b.translate(c - a, true) + d,
        c = b.translate(c + Ya - a, true) - d;
      (d = B.hoverPoints) && J(d, function (a) {
        a.setState()
      });
      f > ea(e.dataMin, e.min) && c < E(e.dataMax, e.max) && b.setExtremes(f, c, true, false);
      B.mouseDownX = a;
      u(ga, {
        cursor: "move"
      })
    };
    Cb = function () {
      var a = I.legend,
        b = o(a.margin, 10),
        c = a.x,
        d = a.y,
        e = a.align,
        f = a.verticalAlign,
        g;
      Tb();
      if ((B.title || B.subtitle) && !n(Z))(g = E(B.title && !ma.floating && !ma.verticalAlign && ma.y || 0, B.subtitle && !ua.floating && !ua.verticalAlign && ua.y || 0)) && (ta = E(ta, g + o(ma.margin, 15) + T));
      a.enabled && !a.floating && (e === "right" ? n(fa) || (la = E(la, Lb - c + b + lc)) : e === "left" ? n(aa) || (xa = E(xa, Lb + c + b + qb)) : f === "top" ? n(Z) || (ta = E(ta, fc + d + b + T)) : f === "bottom" && (n(X) || (Ea = E(Ea, fc - d + b + Fb))));
      B.extraBottomMargin && (Ea += B.extraBottomMargin);
      B.extraTopMargin && (ta += B.extraTopMargin);
      vb && J(ib, function (a) {
        a.getOffset()
      });
      n(aa) || (xa += Fa[3]);
      n(Z) || (ta += Fa[0]);
      n(X) || (Ea += Fa[2]);
      n(fa) || (la += Fa[1]);
      Ub()
    };
    Xb = function (a, b, c) {
      var d = B.title,
        e = B.subtitle;
      Ib += 1;
      va(c, B);
      jb = fb;
      lb = eb;
      if (n(a)) B.chartWidth = eb = S(a);
      if (n(b)) B.chartHeight = fb = S(b);
      u(ga, {
        width: eb + Sa,
        height: fb + Sa
      });
      ka.setSize(eb, fb, c);
      Ya = eb - xa - la;
      Za = fb - ta - Ea;
      Sb = null;
      J(ib, function (a) {
        a.isDirty = true;
        a.setScale()
      });
      J(Ka, function (a) {
        a.isDirty = true
      });
      B.isDirtyLegend = true;
      B.isDirtyBox = true;
      Cb();
      d && d.align(null, null, bb);
      e && e.align(null, null, bb);
      C(c);
      jb = null;
      Ja(B, "resize");
      Wb === false ? Ia() : setTimeout(Ia, Wb && Wb.duration || 500)
    };
    Ub = function () {
      B.plotLeft = xa = S(xa);
      B.plotTop = ta = S(ta);
      B.plotWidth = Ya = S(eb - xa - la);
      B.plotHeight = Za = S(fb - ta - Ea);
      B.plotSizeX = Ga ? Za : Ya;
      B.plotSizeY = Ga ? Ya : Za;
      bb = {
        x: qb,
        y: T,
        width: eb - qb - lc,
        height: fb - T - Fb
      };
      J(ib, function (a) {
        a.setAxisSize();
        a.setAxisTranslation()
      })
    };
    Tb = function () {
      ta = o(Z, T);
      la = o(fa, lc);
      Ea = o(X, Fb);
      xa = o(aa, qb);
      Fa = [0, 0, 0, 0]
    };
    Qb = function () {
      var a = ca.borderWidth || 0,
        b = ca.backgroundColor,
        c = ca.plotBackgroundColor,
        d = ca.plotBackgroundImage,
        e, f = {
          x: xa,
          y: ta,
          width: Ya,
          height: Za
        };
      e = a + (ca.shadow ? 8 : 0);
      if (a || b) na ? na.animate(na.crisp(null, null, null, eb - e, fb - e)) : na = ka.rect(e / 2, e / 2, eb - e, fb - e, ca.borderRadius, a).attr({
        stroke: ca.borderColor,
        "stroke-width": a,
        fill: b || ob
      }).add().shadow(ca.shadow);
      c && (Oa ? Oa.animate(f) : Oa = ka.rect(xa, ta, Ya, Za, 0).attr({
        fill: c
      }).add().shadow(ca.plotShadow));
      d && (Va ? Va.animate(f) : Va = ka.image(d, xa, ta, Ya, Za).add());
      ca.plotBorderWidth && (Xa ? Xa.animate(Xa.crisp(null, xa, ta, Ya, Za)) : Xa = ka.rect(xa, ta, Ya, Za, 0, ca.plotBorderWidth).attr({
        stroke: ca.plotBorderColor,
        "stroke-width": ca.plotBorderWidth,
        zIndex: 4
      }).add());
      B.isDirtyBox = false
    };
    ca.reflow !== false && Pa(B, "load", Vb);
    if (ab) for (Ab in ab) Pa(B, Ab, ab[Ab]);
    B.options = I;
    B.series = Ka;
    B.xAxis = [];
    B.yAxis = [];
    B.addSeries = function (a, b, c) {
      var d;
      a && (va(c, B), b = o(b, true), Ja(B, "addSeries", {
        options: a
      }, function () {
        d = Qa(a);
        d.isDirty = true;
        B.isDirtyLegend = true;
        b && B.redraw()
      }));
      return d
    };
    B.animation = oa ? false : o(ca.animation, true);
    B.Axis = g;
    B.destroy = function () {
      var a, b = ga && ga.parentNode;
      if (B !== null) {
        Ja(B, "destroy");
        nb(B);
        for (a = ib.length; a--;) ib[a] = ib[a].destroy();
        for (a = Ka.length; a--;) Ka[a] = Ka[a].destroy();
        J("title,subtitle,seriesGroup,clipRect,credits,tracker,scroller,rangeSelector".split(","), function (a) {
          var b = B[a];
          b && (B[a] = b.destroy())
        });
        J([na, Xa, Oa, zb, dc, ka, cc], function (a) {
          a && a.destroy && a.destroy()
        });
        na = Xa = Oa = zb = dc = ka = cc = null;
        if (ga) ga.innerHTML = "", nb(ga), b && sa(ga), ga = null;
        clearInterval(Yb);
        for (a in B) delete B[a];
        I = B = null
      }
    };
    B.get = function (a) {
      var b, c, d;
      for (b = 0; b < ib.length; b++) if (ib[b].options.id === a) return ib[b];
      for (b = 0; b < Ka.length; b++) if (Ka[b].options.id === a) return Ka[b];
      for (b = 0; b < Ka.length; b++) {
        d = Ka[b].points || [];
        for (c = 0; c < d.length; c++) if (d[c].id === a) return d[c]
      }
      return null
    };
    B.getSelectedPoints = function () {
      var a = [];
      J(Ka, function (b) {
        a = a.concat(hc(b.points, function (a) {
          return a.selected
        }))
      });
      return a
    };
    B.getSelectedSeries = function () {
      return hc(Ka, function (a) {
        return a.selected
      })
    };
    B.hideLoading = function () {
      ec && $b(ec, {
        opacity: 0
      }, {
        duration: I.loading.hideDuration || 100,
        complete: function () {
          u(ec, {
            display: ob
          })
        }
      });
      Jb = false
    };
    B.initSeries = Qa;
    B.isInsidePlot = Na;
    B.redraw = C;
    B.setSize = Xb;
    B.setTitle = Q;
    B.showLoading = function (b) {
      var c = I.loading;
      ec || (ec = w(Hb, {
        className: Bb + "loading"
      }, a(c.style, {
        left: xa + Sa,
        top: ta + Sa,
        width: Ya + Sa,
        height: Za + Sa,
        zIndex: 10,
        display: ob
      }), ga), Ob = w("span", null, c.labelStyle, ec));
      Ob.innerHTML = b || I.lang.loading;
      Jb || (u(ec, {
        opacity: 0,
        display: ""
      }), $b(ec, {
        opacity: c.style.opacity
      }, {
        duration: c.showDuration || 0
      }), Jb = true)
    };
    B.pointCount = 0;
    B.counters = new K;
    ub()
  }
  var P, Y = document,
    M = window,
    A = Math,
    S = A.round,
    La = A.floor,
    kb = A.ceil,
    E = A.max,
    ea = A.min,
    W = A.abs,
    X = A.cos,
    I = A.sin,
    ua = A.PI,
    ya = ua * 2 / 360,
    Fa = navigator.userAgent,
    za = /msie/i.test(Fa) && !M.opera,
    la = Y.documentMode === 8,
    aa = /AppleWebKit/.test(Fa),
    Ra = /Firefox/.test(Fa),
    hb = !! Y.createElementNS && !! Y.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect,
    Ia = Ra && parseInt(Fa.split("Firefox/")[1], 10) < 4,
    oa = !hb && !za && !! Y.createElement("canvas").getContext,
    pa, ra = Y.documentElement.ontouchstart !== P,
    bb = {},
    mc = 0,
    Da, Ba, nc, Wb, Ab, ma, Hb = "div",
    Mb = "absolute",
    tc = "relative",
    rb = "hidden",
    Bb = "highcharts-",
    sb = "visible",
    Sa = "px",
    ob = "none",
    Aa = "M",
    Ma = "L",
    vb = "rgba(192,192,192," + (hb ? 1.0E-6 : 0.0020) + ")",
    Wa = "",
    wb = "hover",
    Ib = "millisecond",
    Nb = "second",
    Ob = "minute",
    Na = "hour",
    Va = "day",
    Eb = "week",
    na = "month",
    Xa = "year",
    Jb, Xb, Tb, Yb, Kb, Pb, Qb, oc, wc, Cb, xc, yc, T = M.HighchartsAdapter,
    Oa = T || {},
    Zb = Oa.getScript,
    J = Oa.each,
    hc = Oa.grep,
    uc = Oa.offset,
    xb = Oa.map,
    da = Oa.merge,
    Pa = Oa.addEvent,
    nb = Oa.removeEvent,
    Ja = Oa.fireEvent,
    $b = Oa.animate,
    Rb = Oa.stop,
    tb = {};
  M.Highcharts = {};
  nc = function (a, b, c) {
    if (!n(b) || isNaN(b)) return "Invalid date";
    var a = o(a, "%Y-%m-%d %H:%M:%S"),
      d = new Date(b),
      e, f = d[Tb](),
      g = d[Yb](),
      h = d[Kb](),
      j = d[Pb](),
      l = d[Qb](),
      k = Ba.lang,
      m = k.weekdays,
      b = {
        a: m[g].substr(0, 3),
        A: m[g],
        d: C(h),
        e: h,
        b: k.shortMonths[j],
        B: k.months[j],
        m: C(j + 1),
        y: l.toString().substr(2, 2),
        Y: l,
        H: C(f),
        I: C(f % 12 || 12),
        l: f % 12 || 12,
        M: C(d[Xb]()),
        p: f < 12 ? "AM" : "PM",
        P: f < 12 ? "am" : "pm",
        S: C(d.getSeconds()),
        L: C(S(b % 1E3), 3)
      };
    for (e in b) a = a.replace("%" + e, b[e]);
    return c ? a.substr(0, 1).toUpperCase() + a.substr(1) : a
  };
  K.prototype = {
    wrapColor: function (a) {
      if (this.color >= a) this.color = 0
    },
    wrapSymbol: function (a) {
      if (this.symbol >= a) this.symbol = 0
    }
  };
  ma = e(Ib, 1, Nb, 1E3, Ob, 6E4, Na, 36E5, Va, 864E5, Eb, 6048E5, na, 2592E6, Xa, 31556952E3);
  Ab = {
    init: function (a, b, c) {
      var b = b || "",
        d = a.shift,
        e = b.indexOf("C") > -1,
        f = e ? 7 : 3,
        g, b = b.split(" "),
        c = [].concat(c),
        h, j, n = function (a) {
          for (g = a.length; g--;) a[g] === Aa && a.splice(g + 1, 0, a[g + 1], a[g + 2], a[g + 1], a[g + 2])
        };
      e && (n(b), n(c));
      a.isArea && (h = b.splice(b.length - 6, 6), j = c.splice(c.length - 6, 6));
      d === 1 && (c = [].concat(c).splice(0, f).concat(c));
      a.shift = 0;
      if (b.length) for (a = c.length; b.length < a;) d = [].concat(b).splice(b.length - f, f), e && (d[f - 6] = d[f - 2], d[f - 5] = d[f - 1]), b = b.concat(d);
      h && (b = b.concat(h), c = c.concat(j));
      return [b, c]
    },
    step: function (a, b, c, d) {
      var e = [],
        f = a.length;
      if (c === 1) e = d;
      else if (f === b.length && c < 1) for (; f--;) d = parseFloat(a[f]), e[f] = isNaN(d) ? a[f] : c * parseFloat(b[f] - d) + d;
      else e = b;
      return e
    }
  };
  T && T.init && T.init(Ab);
  if (!T && M.jQuery) {
    var Ta = jQuery,
      Zb = Ta.getScript,
      J = function (a, b) {
        for (var c = 0, d = a.length; c < d; c++) if (b.call(a[c], a[c], c, a) === false) return c
      },
      hc = Ta.grep,
      xb = function (a, b) {
        for (var c = [], d = 0, e = a.length; d < e; d++) c[d] = b.call(a[d], a[d], d, a);
        return c
      },
      da = function () {
        var a = arguments;
        return Ta.extend(true, null, a[0], a[1], a[2], a[3])
      },
      uc = function (a) {
        return Ta(a).offset()
      },
      Pa = function (a, b, c) {
        Ta(a).bind(b, c)
      },
      nb = function (a, b, c) {
        var d = Y.removeEventListener ? "removeEventListener" : "detachEvent";
        Y[d] && !a[d] && (a[d] = function () {});
        Ta(a).unbind(b, c)
      },
      Ja = function (b, c, d, e) {
        var f = Ta.Event(c),
          g = "detached" + c,
          h;
        a(f, d);
        b[c] && (b[g] = b[c], b[c] = null);
        J(["preventDefault", "stopPropagation"], function (a) {
          var b = f[a];
          f[a] = function () {
            try {
              b.call(f)
            } catch (c) {
              a === "preventDefault" && (h = true)
            }
          }
        });
        Ta(b).trigger(f);
        b[g] && (b[c] = b[g], b[g] = null);
        e && !f.isDefaultPrevented() && !h && e(f)
      },
      $b = function (a, b, c) {
        var d = Ta(a);
        if (b.d) a.toD = b.d, b.d = 1;
        d.stop();
        d.animate(b, c)
      },
      Rb = function (a) {
        Ta(a).stop()
      };
    Ta.extend(Ta.easing, {
      easeOutQuad: function (a, b, c, d, e) {
        return -d * (b /= e) * (b - 2) + c
      }
    });
    var zb = jQuery.fx,
      jb = zb.step;
    J(["cur", "_default", "width", "height"], function (a, b) {
      var c = b ? jb : zb.prototype,
        d = c[a],
        e;
      d && (c[a] = function (a) {
        a = b ? a : this;
        e = a.elem;
        return e.attr ? e.attr(a.prop, a.now) : d.apply(this, arguments)
      })
    });
    jb.d = function (a) {
      var b = a.elem;
      if (!a.started) {
        var c = Ab.init(b, b.d, b.toD);
        a.start = c[0];
        a.end = c[1];
        a.started = true
      }
      b.attr("d", Ab.step(a.start, a.end, a.pos, b.toD))
    }
  }
  T = {
    enabled: true,
    align: "center",
    x: 0,
    y: 15,
    style: {
      color: "#666",
      fontSize: "11px",
      lineHeight: "14px"
    }
  };
  Ba = {
    colors: "#4572A7,#AA4643,#89A54E,#80699B,#3D96AE,#DB843D,#92A8CD,#A47D7C,#B5CA92".split(","),
    symbols: ["circle", "diamond", "square", "triangle", "triangle-down"],
    lang: {
      loading: "Loading...",
      months: "January,February,March,April,May,June,July,August,September,October,November,December".split(","),
      shortMonths: "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),
      weekdays: "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),
      decimalPoint: ".",
      resetZoom: "Reset zoom",
      resetZoomTitle: "Reset zoom level 1:1",
      thousandsSep: ","
    },
    global: {
      useUTC: true,
      canvasToolsURL: "http://code.highcharts.com/2.2.1/modules/canvas-tools.js"
    },
    chart: {
      borderColor: "#4572A7",
      borderRadius: 5,
      defaultSeriesType: "line",
      ignoreHiddenSeries: true,
      spacingTop: 10,
      spacingRight: 10,
      spacingBottom: 15,
      spacingLeft: 10,
      style: {
        fontFamily: '"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif',
        fontSize: "12px"
      },
      backgroundColor: "#FFFFFF",
      plotBorderColor: "#C0C0C0",
      resetZoomButton: {
        theme: {
          zIndex: 20
        },
        position: {
          align: "right",
          x: -10,
          y: 10
        }
      }
    },
    title: {
      text: "Chart title",
      align: "center",
      y: 15,
      style: {
        color: "#3E576F",
        fontSize: "16px"
      }
    },
    subtitle: {
      text: "",
      align: "center",
      y: 30,
      style: {
        color: "#6D869F"
      }
    },
    plotOptions: {
      line: {
        allowPointSelect: false,
        showCheckbox: false,
        animation: {
          duration: 1E3
        },
        events: {},
        lineWidth: 2,
        shadow: true,
        marker: {
          enabled: true,
          lineWidth: 0,
          radius: 4,
          lineColor: "#FFFFFF",
          states: {
            hover: {},
            select: {
              fillColor: "#FFFFFF",
              lineColor: "#000000",
              lineWidth: 2
            }
          }
        },
        point: {
          events: {}
        },
        dataLabels: da(T, {
          enabled: false,
          y: -6,
          formatter: function () {
            return this.y
          }
        }),
        cropThreshold: 300,
        pointRange: 0,
        showInLegend: true,
        states: {
          hover: {
            marker: {}
          },
          select: {
            marker: {}
          }
        },
        stickyTracking: true
      }
    },
    labels: {
      style: {
        position: Mb,
        color: "#3E576F"
      }
    },
    legend: {
      enabled: true,
      align: "center",
      layout: "horizontal",
      labelFormatter: function () {
        return this.name
      },
      borderWidth: 1,
      borderColor: "#909090",
      borderRadius: 5,
      shadow: false,
      style: {
        padding: "5px"
      },
      itemStyle: {
        cursor: "pointer",
        color: "#3E576F"
      },
      itemHoverStyle: {
        color: "#000000"
      },
      itemHiddenStyle: {
        color: "#C0C0C0"
      },
      itemCheckboxStyle: {
        position: Mb,
        width: "13px",
        height: "13px"
      },
      symbolWidth: 16,
      symbolPadding: 5,
      verticalAlign: "bottom",
      x: 0,
      y: 0
    },
    loading: {
      labelStyle: {
        fontWeight: "bold",
        position: tc,
        top: "1em"
      },
      style: {
        position: Mb,
        backgroundColor: "white",
        opacity: 0.5,
        textAlign: "center"
      }
    },
    tooltip: {
      enabled: true,
      backgroundColor: "rgba(255, 255, 255, .85)",
      borderWidth: 2,
      borderRadius: 5,
      headerFormat: '<span style="font-size: 10px">{point.key}</span><br/>',
      pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
      shadow: true,
      shared: oa,
      snap: ra ? 25 : 10,
      style: {
        color: "#333333",
        fontSize: "12px",
        padding: "5px",
        whiteSpace: "nowrap"
      }
    },
    credits: {
      enabled: true,
      text: "Highcharts.com",
      href: "http://www.highcharts.com",
      position: {
        align: "right",
        x: -10,
        verticalAlign: "bottom",
        y: -5
      },
      style: {
        cursor: "pointer",
        color: "#909090",
        fontSize: "10px"
      }
    }
  };
  var Db = {
    dateTimeLabelFormats: e(Ib, "%H:%M:%S.%L", Nb, "%H:%M:%S", Ob, "%H:%M", Na, "%H:%M", Va, "%e. %b", Eb, "%e. %b", na, "%b '%y", Xa, "%Y"),
    endOnTick: false,
    gridLineColor: "#C0C0C0",
    labels: T,
    lineColor: "#C0D0E0",
    lineWidth: 1,
    max: null,
    min: null,
    minPadding: 0.01,
    maxPadding: 0.01,
    minorGridLineColor: "#E0E0E0",
    minorGridLineWidth: 1,
    minorTickColor: "#A0A0A0",
    minorTickLength: 2,
    minorTickPosition: "outside",
    startOfWeek: 1,
    startOnTick: false,
    tickColor: "#C0D0E0",
    tickLength: 5,
    tickmarkPlacement: "between",
    tickPixelInterval: 100,
    tickPosition: "outside",
    tickWidth: 1,
    title: {
      align: "middle",
      style: {
        color: "#6D869F",
        fontWeight: "bold"
      }
    },
    type: "linear"
  },
    vc = da(Db, {
      endOnTick: true,
      gridLineWidth: 1,
      tickPixelInterval: 72,
      showLastLabel: true,
      labels: {
        align: "right",
        x: -8,
        y: 3
      },
      lineWidth: 0,
      maxPadding: 0.05,
      minPadding: 0.05,
      startOnTick: true,
      tickWidth: 0,
      title: {
        rotation: 270,
        text: "Y-values"
      },
      stackLabels: {
        enabled: false,
        formatter: function () {
          return this.total
        },
        style: T.style
      }
    }),
    Ac = {
      labels: {
        align: "right",
        x: -8,
        y: null
      },
      title: {
        rotation: 270
      }
    },
    pc = {
      labels: {
        align: "left",
        x: 8,
        y: null
      },
      title: {
        rotation: 90
      }
    },
    qc = {
      labels: {
        align: "center",
        x: 0,
        y: 14,
        overflow: "justify"
      },
      title: {
        rotation: 0
      }
    },
    kc = da(qc, {
      labels: {
        y: -5,
        overflow: "justify"
      }
    }),
    lb = Ba.plotOptions,
    T = lb.line;
  lb.spline = da(T);
  lb.scatter = da(T, {
    lineWidth: 0,
    states: {
      hover: {
        lineWidth: 0
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size: 10px; color:{series.color}">{series.name}</span><br/>',
      pointFormat: "x: <b>{point.x}</b><br/>y: <b>{point.y}</b><br/>"
    }
  });
  lb.area = da(T, {
    threshold: 0
  });
  lb.areaspline = da(lb.area);
  lb.column = da(T, {
    borderColor: "#FFFFFF",
    borderWidth: 1,
    borderRadius: 0,
    groupPadding: 0.2,
    marker: null,
    pointPadding: 0.1,
    minPointLength: 0,
    cropThreshold: 50,
    pointRange: null,
    states: {
      hover: {
        brightness: 0.1,
        shadow: false
      },
      select: {
        color: "#C0C0C0",
        borderColor: "#000000",
        shadow: false
      }
    },
    dataLabels: {
      y: null,
      verticalAlign: null
    },
    threshold: 0
  });
  lb.bar = da(lb.column, {
    dataLabels: {
      align: "left",
      x: 5,
      y: null,
      verticalAlign: "middle"
    }
  });
  lb.pie = da(T, {
    borderColor: "#FFFFFF",
    borderWidth: 1,
    center: ["50%", "50%"],
    colorByPoint: true,
    dataLabels: {
      distance: 30,
      enabled: true,
      formatter: function () {
        return this.point.name
      },
      y: 5
    },
    legendType: "point",
    marker: null,
    size: "75%",
    showInLegend: false,
    slicedOffset: 10,
    states: {
      hover: {
        brightness: 0.1,
        shadow: false
      }
    }
  });
  Z();
  var yb = function (a) {
      var c = [],
        d;
      (function (a) {
        (d = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/.exec(a)) ? c = [b(d[1]), b(d[2]), b(d[3]), parseFloat(d[4], 10)] : (d = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(a)) && (c = [b(d[1], 16), b(d[2], 16), b(d[3], 16), 1])
      })(a);
      return {
        get: function (b) {
          return c && !isNaN(c[0]) ? b === "rgb" ? "rgb(" + c[0] + "," + c[1] + "," + c[2] + ")" : b === "a" ? c[3] : "rgba(" + c.join(",") + ")" : a
        },
        brighten: function (a) {
          if (g(a) && a !== 0) {
            var d;
            for (d = 0; d < 3; d++) c[d] += b(a * 255), c[d] < 0 && (c[d] = 0), c[d] > 255 && (c[d] = 255)
          }
          return this
        },
        setOpacity: function (a) {
          c[3] = a;
          return this
        }
      }
    };
  fa.prototype = {
    init: function (a, b) {
      this.element = b === "span" ? w(b) : Y.createElementNS("http://www.w3.org/2000/svg", b);
      this.renderer = a;
      this.attrSetters = {}
    },
    animate: function (a, b, c) {
      b = o(b, Wb, true);
      Rb(this);
      if (b) {
        b = da(b);
        if (c) b.complete = c;
        $b(this, a, b)
      } else this.attr(a), c && c()
    },
    attr: function (a, d) {
      var e, f, g, h, j = this.element,
        l = j.nodeName,
        k = this.renderer,
        o, z = this.attrSetters,
        p = this.shadows,
        s, q = this;
      c(a) && n(d) && (e = a, a = {}, a[e] = d);
      if (c(a)) e = a, l === "circle" ? e = {
        x: "cx",
        y: "cy"
      }[e] || e : e === "strokeWidth" && (e = "stroke-width"), q = m(j, e) || this[e] || 0, e !== "d" && e !== "visibility" && (q = parseFloat(q));
      else for (e in a) if (o = false, f = a[e], g = z[e] && z[e](f, e), g !== false) {
        g !== P && (f = g);
        if (e === "d") f && f.join && (f = f.join(" ")), /(NaN| {2}|^$)/.test(f) && (f = "M 0 0"), this.d = f;
        else if (e === "x" && l === "text") {
          for (g = 0; g < j.childNodes.length; g++) h = j.childNodes[g], m(h, "x") === m(j, "x") && m(h, "x", f);
          this.rotation && m(j, "transform", "rotate(" + this.rotation + " " + f + " " + b(a.y || m(j, "y")) + ")")
        } else if (e === "fill") f = k.color(f, j, e);
        else if (l === "circle" && (e === "x" || e === "y")) e = {
          x: "cx",
          y: "cy"
        }[e] || e;
        else if (l === "rect" && e === "r") m(j, {
          rx: f,
          ry: f
        }), o = true;
        else if (e === "translateX" || e === "translateY" || e === "rotation" || e === "verticalAlign") this[e] = f, this.updateTransform(), o = true;
        else if (e === "stroke") f = k.color(f, j, e);
        else if (e === "dashstyle") if (e = "stroke-dasharray", f = f && f.toLowerCase(), f === "solid") f = ob;
        else {
          if (f) {
            f = f.replace("shortdashdotdot", "3,1,1,1,1,1,").replace("shortdashdot", "3,1,1,1").replace("shortdot", "1,1,").replace("shortdash", "3,1,").replace("longdash", "8,3,").replace(/dot/g, "1,3,").replace("dash", "4,3,").replace(/,$/, "").split(",");
            for (g = f.length; g--;) f[g] = b(f[g]) * a["stroke-width"];
            f = f.join(",")
          }
        } else e === "isTracker" ? this[e] = f : e === "width" ? f = b(f) : e === "align" ? (e = "text-anchor", f = {
          left: "start",
          center: "middle",
          right: "end"
        }[f]) : e === "title" && (g = Y.createElementNS("http://www.w3.org/2000/svg", "title"), g.appendChild(Y.createTextNode(f)), j.appendChild(g));
        e === "strokeWidth" && (e = "stroke-width");
        aa && e === "stroke-width" && f === 0 && (f = 1.0E-6);
        this.symbolName && /^(x|y|r|start|end|innerR|anchorX|anchorY)/.test(e) && (s || (this.symbolAttr(a), s = true), o = true);
        if (p && /^(width|height|visibility|x|y|d|transform)$/.test(e)) for (g = p.length; g--;) m(p[g], e, f);
        if ((e === "width" || e === "height") && l === "rect" && f < 0) f = 0;
        e === "text" ? (this.textStr = f, this.added && k.buildText(this)) : o || m(j, e, f)
      }
      if (aa && /Chrome\/(18|19)/.test(Fa) && l === "text" && (a.x !== P || a.y !== P)) e = j.parentNode, f = j.nextSibling, e && (e.removeChild(j), f ? e.insertBefore(j, f) : e.appendChild(j));
      return q
    },
    symbolAttr: function (a) {
      var b = this;
      J("x,y,r,start,end,width,height,innerR,anchorX,anchorY".split(","), function (c) {
        b[c] = o(a[c], b[c])
      });
      b.attr({
        d: b.renderer.symbols[b.symbolName](b.x, b.y, b.width, b.height, b)
      })
    },
    clip: function (a) {
      return this.attr("clip-path", "url(" + this.renderer.url + "#" + a.id + ")")
    },
    crisp: function (a, b, c, d, e) {
      var f, g = {},
        h = {},
        j, a = a || this.strokeWidth || this.attr && this.attr("stroke-width") || 0;
      j = S(a) % 2 / 2;
      h.x = La(b || this.x || 0) + j;
      h.y = La(c || this.y || 0) + j;
      h.width = La((d || this.width || 0) - 2 * j);
      h.height = La((e || this.height || 0) - 2 * j);
      h.strokeWidth = a;
      for (f in h) this[f] !== h[f] && (this[f] = g[f] = h[f]);
      return g
    },
    css: function (b) {
      var c = this.element,
        c = b && b.width && c.nodeName === "text",
        d, e = "",
        f = function (a, b) {
          return "-" + b.toLowerCase()
        };
      if (b && b.color) b.fill = b.color;
      this.styles = b = a(this.styles, b);
      if (za && !hb) c && delete b.width, u(this.element, b);
      else {
        for (d in b) e += d.replace(/([A-Z])/g, f) + ":" + b[d] + ";";
        this.attr({
          style: e
        })
      }
      c && this.added && this.renderer.buildText(this);
      return this
    },
    on: function (a, b) {
      var c = b;
      ra && a === "click" && (a = "touchstart", c = function (a) {
        a.preventDefault();
        b()
      });
      this.element["on" + a] = c;
      return this
    },
    translate: function (a, b) {
      return this.attr({
        translateX: a,
        translateY: b
      })
    },
    invert: function () {
      this.inverted = true;
      this.updateTransform();
      return this
    },
    htmlCss: function (b) {
      var c = this.element;
      if (c = b && c.tagName === "SPAN" && b.width) delete b.width, this.textWidth = c, this.updateTransform();
      this.styles = a(this.styles, b);
      u(this.element, b);
      return this
    },
    htmlGetBBox: function (a) {
      var b = this.element,
        c = this.bBox;
      if (!c || a) {
        if (b.nodeName === "text") b.style.position = Mb;
        c = this.bBox = {
          x: b.offsetLeft,
          y: b.offsetTop,
          width: b.offsetWidth,
          height: b.offsetHeight
        }
      }
      return c
    },
    htmlUpdateTransform: function () {
      if (this.added) {
        var a = this.renderer,
          c = this.element,
          d = this.translateX || 0,
          e = this.translateY || 0,
          f = this.x || 0,
          g = this.y || 0,
          h = this.textAlign || "left",
          j = {
            left: 0,
            center: 0.5,
            right: 1
          }[h],
          l = h && h !== "left",
          k = this.shadows;
        if (d || e) u(c, {
          marginLeft: d,
          marginTop: e
        }), k && J(k, function (a) {
          u(a, {
            marginLeft: d + 1,
            marginTop: e + 1
          })
        });
        this.inverted && J(c.childNodes, function (b) {
          a.invertChild(b, c)
        });
        if (c.tagName === "SPAN") {
          var m, z, k = this.rotation,
            p;
          m = 0;
          var s = 1,
            q = 0,
            w;
          p = b(this.textWidth);
          var C = this.xCorr || 0,
            F = this.yCorr || 0,
            D = [k, h, c.innerHTML, this.textWidth].join(",");
          if (D !== this.cTT) n(k) && (m = k * ya, s = X(m), q = I(m), u(c, {
            filter: k ? ["progid:DXImageTransform.Microsoft.Matrix(M11=", s, ", M12=", -q, ", M21=", q, ", M22=", s, ", sizingMethod='auto expand')"].join("") : ob
          })), m = o(this.elemWidth, c.offsetWidth), z = o(this.elemHeight, c.offsetHeight), m > p && (u(c, {
            width: p + Sa,
            display: "block",
            whiteSpace: "normal"
          }), m = p), p = a.fontMetrics(c.style.fontSize).b, C = s < 0 && -m, F = q < 0 && -z, w = s * q < 0, C += q * p * (w ? 1 - j : j), F -= s * p * (k ? w ? j : 1 - j : 1), l && (C -= m * j * (s < 0 ? -1 : 1), k && (F -= z * j * (q < 0 ? -1 : 1)), u(c, {
            textAlign: h
          })), this.xCorr = C, this.yCorr = F;
          u(c, {
            left: f + C + Sa,
            top: g + F + Sa
          });
          this.cTT = D
        }
      } else this.alignOnAdd = true
    },
    updateTransform: function () {
      var a = this.translateX || 0,
        b = this.translateY || 0,
        c = this.inverted,
        d = this.rotation,
        e = [];
      c && (a += this.attr("width"), b += this.attr("height"));
      (a || b) && e.push("translate(" + a + "," + b + ")");
      c ? e.push("rotate(90) scale(-1,1)") : d && e.push("rotate(" + d + " " + this.x + " " + this.y + ")");
      e.length && m(this.element, "transform", e.join(" "))
    },
    toFront: function () {
      var a = this.element;
      a.parentNode.appendChild(a);
      return this
    },
    align: function (a, b, c) {
      a ? (this.alignOptions = a, this.alignByTranslate = b, c || this.renderer.alignedObjects.push(this)) : (a = this.alignOptions, b = this.alignByTranslate);
      var c = o(c, this.renderer),
        d = a.align,
        e = a.verticalAlign,
        f = (c.x || 0) + (a.x || 0),
        g = (c.y || 0) + (a.y || 0),
        h = {};
      /^(right|center)$/.test(d) && (f += (c.width - (a.width || 0)) / {
        right: 1,
        center: 2
      }[d]);
      h[b ? "translateX" : "x"] = S(f);
      /^(bottom|middle)$/.test(e) && (g += (c.height - (a.height || 0)) / ({
        bottom: 1,
        middle: 2
      }[e] || 1));
      h[b ? "translateY" : "y"] = S(g);
      this[this.placed ? "animate" : "attr"](h);
      this.placed = true;
      this.alignAttr = h;
      return this
    },
    getBBox: function (b) {
      var c, d, e = this.rotation;
      d = this.element;
      var f = e * ya;
      if (d.namespaceURI === "http://www.w3.org/2000/svg") {
        try {
          c = d.getBBox ? a({}, d.getBBox()) : {
            width: d.offsetWidth,
            height: d.offsetHeight
          }
        } catch (g) {}
        if (!c || c.width < 0) c = {
          width: 0,
          height: 0
        };
        b = c.width;
        d = c.height;
        if (e) c.width = W(d * I(f)) + W(b * X(f)), c.height = W(d * X(f)) + W(b * I(f))
      } else c = this.htmlGetBBox(b);
      return c
    },
    show: function () {
      return this.attr({
        visibility: sb
      })
    },
    hide: function () {
      return this.attr({
        visibility: rb
      })
    },
    add: function (a) {
      var c = this.renderer,
        d = a || c,
        e = d.element || c.box,
        f = e.childNodes,
        g = this.element,
        h = m(g, "zIndex"),
        j;
      this.parentInverted = a && a.inverted;
      this.textStr !== void 0 && c.buildText(this);
      if (h) d.handleZ = true, h = b(h);
      if (d.handleZ) for (d = 0; d < f.length; d++) if (a = f[d], c = m(a, "zIndex"), a !== g && (b(c) > h || !n(h) && n(c))) {
        e.insertBefore(g, a);
        j = true;
        break
      }
      j || e.appendChild(g);
      this.added = true;
      Ja(this, "add");
      return this
    },
    safeRemoveChild: function (a) {
      var b = a.parentNode;
      b && b.removeChild(a)
    },
    destroy: function () {
      var a = this,
        b = a.element || {},
        c = a.shadows,
        d = a.box,
        e, f;
      b.onclick = b.onmouseout = b.onmouseover = b.onmousemove = null;
      Rb(a);
      if (a.clipPath) a.clipPath = a.clipPath.destroy();
      if (a.stops) {
        for (f = 0; f < a.stops.length; f++) a.stops[f] = a.stops[f].destroy();
        a.stops = null
      }
      a.safeRemoveChild(b);
      c && J(c, function (b) {
        a.safeRemoveChild(b)
      });
      d && d.destroy();
      l(a.renderer.alignedObjects, a);
      for (e in a) delete a[e];
      return null
    },
    empty: function () {
      for (var a = this.element, b = a.childNodes, c = b.length; c--;) a.removeChild(b[c])
    },
    shadow: function (a, b) {
      var c = [],
        d, e, f = this.element,
        g = this.parentInverted ? "(-1,-1)" : "(1,1)";
      if (a) {
        for (d = 1; d <= 3; d++) e = f.cloneNode(0), m(e, {
          isShadow: "true",
          stroke: "rgb(0, 0, 0)",
          "stroke-opacity": 0.05 * d,
          "stroke-width": 7 - 2 * d,
          transform: "translate" + g,
          fill: ob
        }), b ? b.element.appendChild(e) : f.parentNode.insertBefore(e, f), c.push(e);
        this.shadows = c
      }
      return this
    }
  };
  var ba = function () {
      this.init.apply(this, arguments)
    };
  ba.prototype = {
    Element: fa,
    init: function (a, b, c, d) {
      var e = location,
        f;
      f = this.createElement("svg").attr({
        xmlns: "http://www.w3.org/2000/svg",
        version: "1.1"
      });
      a.appendChild(f.element);
      this.isSVG = true;
      this.box = f.element;
      this.boxWrapper = f;
      this.alignedObjects = [];
      this.url = za ? "" : e.href.replace(/#.*?$/, "").replace(/([\('\)])/g, "\\$1");
      this.defs = this.createElement("defs").add();
      this.forExport = d;
      this.gradients = {};
      this.setSize(b, c, false)
    },
    destroy: function () {
      var a = this.defs;
      this.box = null;
      this.boxWrapper = this.boxWrapper.destroy();
      H(this.gradients || {});
      this.gradients = null;
      if (a) this.defs = a.destroy();
      return this.alignedObjects = null
    },
    createElement: function (a) {
      var b = new this.Element;
      b.init(this, a);
      return b
    },
    draw: function () {},
    buildText: function (a) {
      for (var c = a.element, d = o(a.textStr, "").toString().replace(/<(b|strong)>/g, '<span style="font-weight:bold">').replace(/<(i|em)>/g, '<span style="font-style:italic">').replace(/<a/g, "<span").replace(/<\/(b|strong|i|em|a)>/g, "</span>").split(/<br.*?>/g), e = c.childNodes, f = /style="([^"]+)"/, g = /href="([^"]+)"/, h = m(c, "x"), j = a.styles, l = j && b(j.width), n = j && j.lineHeight, k, j = e.length; j--;) c.removeChild(e[j]);
      l && !a.added && this.box.appendChild(c);
      d[d.length - 1] === "" && d.pop();
      J(d, function (d, e) {
        var j, o = 0,
          z, d = d.replace(/<span/g, "|||<span").replace(/<\/span>/g, "</span>|||");
        j = d.split("|||");
        J(j, function (d) {
          if (d !== "" || j.length === 1) {
            var G = {},
              p = Y.createElementNS("http://www.w3.org/2000/svg", "tspan");
            f.test(d) && m(p, "style", d.match(f)[1].replace(/(;| |^)color([ :])/, "$1fill$2"));
            g.test(d) && (m(p, "onclick", 'location.href="' + d.match(g)[1] + '"'), u(p, {
              cursor: "pointer"
            }));
            d = (d.replace(/<(.|\n)*?>/g, "") || " ").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
            p.appendChild(Y.createTextNode(d));
            o ? G.dx = 3 : G.x = h;
            if (!o) {
              if (e) {
                !hb && a.renderer.forExport && u(p, {
                  display: "block"
                });
                z = M.getComputedStyle && b(M.getComputedStyle(k, null).getPropertyValue("line-height"));
                if (!z || isNaN(z)) z = n || k.offsetHeight || 18;
                m(p, "dy", z)
              }
              k = p
            }
            m(p, G);
            c.appendChild(p);
            o++;
            if (l) for (var d = d.replace(/-/g, "- ").split(" "), s, R = []; d.length || R.length;) s = a.getBBox().width, G = s > l, !G || d.length === 1 ? (d = R, R = [], d.length && (p = Y.createElementNS("http://www.w3.org/2000/svg", "tspan"), m(p, {
              dy: n || 16,
              x: h
            }), c.appendChild(p), s > l && (l = s))) : (p.removeChild(p.firstChild), R.unshift(d.pop())), d.length && p.appendChild(Y.createTextNode(d.join(" ").replace(/- /g, "-")))
          }
        })
      })
    },
    button: function (b, c, d, f, g, h, j) {
      var l = this.label(b, c, d),
        n = 0,
        k, m, o, z, p, b = {
          x1: 0,
          y1: 0,
          x2: 0,
          y2: 1
        },
        g = da(e("stroke-width", 1, "stroke", "#999", "fill", e("linearGradient", b, "stops", [
          [0, "#FFF"],
          [1, "#DDD"]
        ]), "r", 3, "padding", 3, "style", e("color", "black")), g);
      o = g.style;
      delete g.style;
      h = da(g, e("stroke", "#68A", "fill", e("linearGradient", b, "stops", [
        [0, "#FFF"],
        [1, "#ACF"]
      ])), h);
      z = h.style;
      delete h.style;
      j = da(g, e("stroke", "#68A", "fill", e("linearGradient", b, "stops", [
        [0, "#9BD"],
        [1, "#CDF"]
      ])), j);
      p = j.style;
      delete j.style;
      Pa(l.element, "mouseenter", function () {
        l.attr(h).css(z)
      });
      Pa(l.element, "mouseleave", function () {
        k = [g, h, j][n];
        m = [o, z, p][n];
        l.attr(k).css(m)
      });
      l.setState = function (a) {
        (n = a) ? a === 2 && l.attr(j).css(p) : l.attr(g).css(o)
      };
      return l.on("click", function () {
        f.call(l)
      }).attr(g).css(a({
        cursor: "default"
      }, o))
    },
    crispLine: function (a, b) {
      a[1] === a[4] && (a[1] = a[4] = S(a[1]) + b % 2 / 2);
      a[2] === a[5] && (a[2] = a[5] = S(a[2]) + b % 2 / 2);
      return a
    },
    path: function (a) {
      return this.createElement("path").attr({
        d: a,
        fill: ob
      })
    },
    circle: function (a, b, c) {
      a = d(a) ? a : {
        x: a,
        y: b,
        r: c
      };
      return this.createElement("circle").attr(a)
    },
    arc: function (a, b, c, e, f, g) {
      if (d(a)) b = a.y, c = a.r, e = a.innerR, f = a.start, g = a.end, a = a.x;
      return this.symbol("arc", a || 0, b || 0, c || 0, c || 0, {
        innerR: e || 0,
        start: f || 0,
        end: g || 0
      })
    },
    rect: function (a, b, c, e, f, g) {
      if (d(a)) b = a.y, c = a.width, e = a.height, f = a.r, g = a.strokeWidth, a = a.x;
      f = this.createElement("rect").attr({
        rx: f,
        ry: f,
        fill: ob
      });
      return f.attr(f.crisp(g, a, b, E(c, 0), E(e, 0)))
    },
    setSize: function (a, b, c) {
      var d = this.alignedObjects,
        e = d.length;
      this.width = a;
      this.height = b;
      for (this.boxWrapper[o(c, true) ? "animate" : "attr"]({
        width: a,
        height: b
      }); e--;) d[e].align()
    },
    g: function (a) {
      var b = this.createElement("g");
      return n(a) ? b.attr({
        "class": Bb + a
      }) : b
    },
    image: function (b, c, d, e, f) {
      var g = {
        preserveAspectRatio: ob
      };
      arguments.length > 1 && a(g, {
        x: c,
        y: d,
        width: e,
        height: f
      });
      g = this.createElement("image").attr(g);
      g.element.setAttributeNS ? g.element.setAttributeNS("http://www.w3.org/1999/xlink", "href", b) : g.element.setAttribute("hc-svg-href", b);
      return g
    },
    symbol: function (b, c, d, e, f, g) {
      var h, j = this.symbols[b],
        j = j && j(S(c), S(d), e, f, g),
        l = /^url\((.*?)\)$/,
        n;
      if (j) h = this.path(j), a(h, {
        symbolName: b,
        x: c,
        y: d,
        width: e,
        height: f
      }), g && a(h, g);
      else if (l.test(b)) {
        var k = function (a, b) {
            a.attr({
              width: b[0],
              height: b[1]
            }).translate(-S(b[0] / 2), -S(b[1] / 2))
          };
        n = b.match(l)[1];
        b = bb[n];
        h = this.image(n).attr({
          x: c,
          y: d
        });
        b ? k(h, b) : (h.attr({
          width: 0,
          height: 0
        }), w("img", {
          onload: function () {
            k(h, bb[n] = [this.width, this.height])
          },
          src: n
        }))
      }
      return h
    },
    symbols: {
      circle: function (a, b, c, d) {
        var e = 0.166 * c;
        return [Aa, a + c / 2, b, "C", a + c + e, b, a + c + e, b + d, a + c / 2, b + d, "C", a - e, b + d, a - e, b, a + c / 2, b, "Z"]
      },
      square: function (a, b, c, d) {
        return [Aa, a, b, Ma, a + c, b, a + c, b + d, a, b + d, "Z"]
      },
      triangle: function (a, b, c, d) {
        return [Aa, a + c / 2, b, Ma, a + c, b + d, a, b + d, "Z"]
      },
      "triangle-down": function (a, b, c, d) {
        return [Aa, a, b, Ma, a + c, b, a + c / 2, b + d, "Z"]
      },
      diamond: function (a, b, c, d) {
        return [Aa, a + c / 2, b, Ma, a + c, b + d / 2, a + c / 2, b + d, a, b + d / 2, "Z"]
      },
      arc: function (a, b, c, d, e) {
        var f = e.start,
          c = e.r || c || d,
          g = e.end - 1.0E-6,
          d = e.innerR,
          h = X(f),
          j = I(f),
          l = X(g),
          g = I(g),
          e = e.end - f < ua ? 0 : 1;
        return [Aa, a + c * h, b + c * j, "A", c, c, 0, e, 1, a + c * l, b + c * g, Ma, a + d * l, b + d * g, "A", d, d, 0, e, 0, a + d * h, b + d * j, "Z"]
      }
    },
    clipRect: function (a, b, c, d) {
      var e = Bb + mc++,
        f = this.createElement("clipPath").attr({
          id: e
        }).add(this.defs),
        a = this.rect(a, b, c, d, 0).add(f);
      a.id = e;
      a.clipPath = f;
      return a
    },
    color: function (b, c, d) {
      var e, g = /^rgba/;
      if (b && b.linearGradient) {
        var h = this,
          j = b.linearGradient,
          c = !f(j),
          d = h.gradients,
          l, n = j.x1 || j[0] || 0,
          k = j.y1 || j[1] || 0,
          o = j.x2 || j[2] || 0,
          z = j.y2 || j[3] || 0,
          p, s, q = [c, n, k, o, z, b.stops.join(",")].join(",");
        d[q] ? j = m(d[q].element, "id") : (j = Bb + mc++, l = h.createElement("linearGradient").attr(a({
          id: j,
          x1: n,
          y1: k,
          x2: o,
          y2: z
        }, c ? null : {
          gradientUnits: "userSpaceOnUse"
        })).add(h.defs), l.stops = [], J(b.stops, function (a) {
          g.test(a[1]) ? (e = yb(a[1]), p = e.get("rgb"), s = e.get("a")) : (p = a[1], s = 1);
          a = h.createElement("stop").attr({
            offset: a[0],
            "stop-color": p,
            "stop-opacity": s
          }).add(l);
          l.stops.push(a)
        }), d[q] = l);
        return "url(" + this.url + "#" + j + ")"
      } else return g.test(b) ? (e = yb(b), m(c, d + "-opacity", e.get("a")), e.get("rgb")) : (c.removeAttribute(d + "-opacity"), b)
    },
    text: function (a, b, c, d) {
      var e = Ba.chart.style;
      if (d && !this.forExport) return this.html(a, b, c);
      b = S(o(b, 0));
      c = S(o(c, 0));
      a = this.createElement("text").attr({
        x: b,
        y: c,
        text: a
      }).css({
        fontFamily: e.fontFamily,
        fontSize: e.fontSize
      });
      a.x = b;
      a.y = c;
      return a
    },
    html: function (b, c, d) {
      var e = Ba.chart.style,
        f = this.createElement("span"),
        g = f.attrSetters,
        h = f.element,
        j = f.renderer;
      g.text = function (a) {
        h.innerHTML = a;
        return false
      };
      g.x = g.y = g.align = function (a, b) {
        b === "align" && (b = "textAlign");
        f[b] = a;
        f.htmlUpdateTransform();
        return false
      };
      f.attr({
        text: b,
        x: S(c),
        y: S(d)
      }).css({
        position: Mb,
        whiteSpace: "nowrap",
        fontFamily: e.fontFamily,
        fontSize: e.fontSize
      });
      f.css = f.htmlCss;
      if (j.isSVG) f.add = function (b) {
        var c, d, e = j.box.parentNode;
        if (b) {
          if (c = b.div, !c) c = b.div = w(Hb, {
            className: m(b.element, "class")
          }, {
            position: Mb,
            left: b.attr("translateX") + Sa,
            top: b.attr("translateY") + Sa
          }, e), d = c.style, a(b.attrSetters, {
            translateX: function (a) {
              d.left = a + Sa
            },
            translateY: function (a) {
              d.top = a + Sa
            },
            visibility: function (a, b) {
              d[b] = a
            }
          })
        } else c = e;
        c.appendChild(h);
        f.added = true;
        f.alignOnAdd && f.htmlUpdateTransform();
        return f
      };
      return f
    },
    fontMetrics: function (a) {
      var a = b(a || 11),
        a = a < 24 ? a + 4 : S(a * 1.2),
        c = S(a * 0.8);
      return {
        h: a,
        b: c
      }
    },
    label: function (b, c, d, e, f, g, h, j) {
      function l() {
        var a = z.styles,
          a = a && a.textAlign,
          b = C,
          c;
        c = j ? 0 : E;
        if (n(u) && (a === "center" || a === "right")) b += {
          center: 0.5,
          right: 1
        }[a] * (u - q.width);
        (b !== p.x || c !== p.y) && p.attr({
          x: b,
          y: c
        });
        p.x = b;
        p.y = c
      }
      function k(a, b) {
        s ? s.attr(a, b) : O[a] = b
      }
      function m() {
        z.attr({
          text: b,
          x: c,
          y: d,
          anchorX: f,
          anchorY: g
        })
      }
      var o = this,
        z = o.g(),
        p = o.text("", 0, 0, h).attr({
          zIndex: 1
        }).add(z),
        s, q, w = "left",
        C = 3,
        u, F, D, K, A = 0,
        O = {},
        E, h = z.attrSetters;
      Pa(z, "add", m);
      h.width = function (a) {
        u = a;
        return false
      };
      h.height = function (a) {
        F = a;
        return false
      };
      h.padding = function (a) {
        n(a) && a !== C && (C = a, l());
        return false
      };
      h.align = function (a) {
        w = a;
        return false
      };
      h.text = function (a, b) {
        p.attr(b, a);
        var c;
        c = p.element.style;
        q = (u === void 0 || F === void 0 || z.styles.textAlign) && p.getBBox(true);
        z.width = (u || q.width) + 2 * C;
        z.height = (F || q.height) + 2 * C;
        E = C + o.fontMetrics(c && c.fontSize).b;
        if (!s) c = j ? -E : 0, z.box = s = e ? o.symbol(e, 0, c, z.width, z.height) : o.rect(0, c, z.width, z.height, 0, O["stroke-width"]), s.add(z);
        s.attr(da({
          width: z.width,
          height: z.height
        }, O));
        O = null;
        l();
        return false
      };
      h["stroke-width"] = function (a, b) {
        A = a % 2 / 2;
        k(b, a);
        return false
      };
      h.stroke = h.fill = h.r = function (a, b) {
        k(b, a);
        return false
      };
      h.anchorX = function (a, b) {
        f = a;
        k(b, a + A - D);
        return false
      };
      h.anchorY = function (a, b) {
        g = a;
        k(b, a - K);
        return false
      };
      h.x = function (a) {
        a -= {
          left: 0,
          center: 0.5,
          right: 1
        }[w] * ((u || q.width) + C);
        D = z.x = S(a);
        z.attr("translateX", D);
        return false
      };
      h.y = function (a) {
        K = z.y = S(a);
        z.attr("translateY", a);
        return false
      };
      var V = z.css;
      return a(z, {
        css: function (a) {
          if (a) {
            var b = {},
              a = da({}, a);
            J("fontSize,fontWeight,fontFamily,color,lineHeight,width".split(","), function (c) {
              a[c] !== P && (b[c] = a[c], delete a[c])
            });
            p.css(b)
          }
          return V.call(z, a)
        },
        getBBox: function () {
          return s.getBBox()
        },
        shadow: function (a) {
          s.shadow(a);
          return z
        },
        destroy: function () {
          nb(z, "add", m);
          nb(z.element, "mouseenter");
          nb(z.element, "mouseleave");
          p && (p = p.destroy());
          fa.prototype.destroy.call(z)
        }
      })
    }
  };
  pa = ba;
  var ia;
  if (!hb && !oa) ia = {
    init: function (a, b) {
      var c = ["<", b, ' filled="f" stroked="f"'],
        d = ["position: ", Mb, ";"];
      (b === "shape" || b === Hb) && d.push("left:0;top:0;width:10px;height:10px;");
      la && d.push("visibility: ", b === Hb ? rb : sb);
      c.push(' style="', d.join(""), '"/>');
      if (b) c = b === Hb || b === "span" || b === "img" ? c.join("") : a.prepVML(c), this.element = w(c);
      this.renderer = a;
      this.attrSetters = {}
    },
    add: function (a) {
      var b = this.renderer,
        c = this.element,
        d = b.box,
        d = a ? a.element || a : d;
      a && a.inverted && b.invertChild(c, d);
      la && d.gVis === rb && u(c, {
        visibility: rb
      });
      d.appendChild(c);
      this.added = true;
      this.alignOnAdd && !this.deferUpdateTransform && this.updateTransform();
      Ja(this, "add");
      return this
    },
    toggleChildren: function (a, b) {
      for (var c = a.childNodes, d = c.length; d--;) u(c[d], {
        visibility: b
      }), c[d].nodeName === "DIV" && this.toggleChildren(c[d], b)
    },
    updateTransform: fa.prototype.htmlUpdateTransform,
    attr: function (a, b) {
      var d, e, f, h = this.element || {},
        j = h.style,
        l = h.nodeName,
        k = this.renderer,
        o = this.symbolName,
        z, p = this.shadows,
        s, q = this.attrSetters,
        C = this;
      c(a) && n(b) && (d = a, a = {}, a[d] = b);
      if (c(a)) d = a, C = d === "strokeWidth" || d === "stroke-width" ? this.strokeweight : this[d];
      else for (d in a) if (e = a[d], s = false, f = q[d] && q[d](e, d), f !== false && e !== null) {
        f !== P && (e = f);
        if (o && /^(x|y|r|start|end|width|height|innerR|anchorX|anchorY)/.test(d)) z || (this.symbolAttr(a), z = true), s = true;
        else if (d === "d") {
          e = e || [];
          this.d = e.join(" ");
          f = e.length;
          for (s = []; f--;) s[f] = g(e[f]) ? S(e[f] * 10) - 5 : e[f] === "Z" ? "x" : e[f];
          e = s.join(" ") || "x";
          h.path = e;
          if (p) for (f = p.length; f--;) p[f].path = e;
          s = true
        } else if (d === "zIndex" || d === "visibility") {
          if (la && d === "visibility" && l === "DIV") h.gVis = e, this.toggleChildren(h, e), e === sb && (e = null);
          e && (j[d] = e);
          s = true
        } else if (d === "width" || d === "height") e = E(0, e), this[d] = e, this.updateClipping ? (this[d] = e, this.updateClipping()) : j[d] = e, s = true;
        else if (d === "x" || d === "y") this[d] = e, j[{
          x: "left",
          y: "top"
        }[d]] = e;
        else if (d === "class") h.className = e;
        else if (d === "stroke") e = k.color(e, h, d), d = "strokecolor";
        else if (d === "stroke-width" || d === "strokeWidth") h.stroked = e ? true : false, d = "strokeweight", this[d] = e, g(e) && (e += Sa);
        else if (d === "dashstyle")(h.getElementsByTagName("stroke")[0] || w(k.prepVML(["<stroke/>"]), null, null, h))[d] = e || "solid", this.dashstyle = e, s = true;
        else if (d === "fill") l === "SPAN" ? j.color = e : (h.filled = e !== ob ? true : false, e = k.color(e, h, d), d = "fillcolor");
        else if (d === "translateX" || d === "translateY" || d === "rotation") this[d] = e, this.updateTransform(), s = true;
        else if (d === "text") this.bBox = null, h.innerHTML = e, s = true;
        if (p && d === "visibility") for (f = p.length; f--;) p[f].style[d] = e;
        s || (la ? h[d] = e : m(h, d, e))
      }
      return C
    },
    clip: function (a) {
      var b = this,
        c = a.members;
      c.push(b);
      b.destroyClip = function () {
        l(c, b)
      };
      return b.css(a.getCSS(b.inverted))
    },
    css: fa.prototype.htmlCss,
    safeRemoveChild: function (a) {
      a.parentNode && sa(a)
    },
    destroy: function () {
      this.destroyClip && this.destroyClip();
      return fa.prototype.destroy.apply(this)
    },
    empty: function () {
      for (var a = this.element.childNodes, b = a.length, c; b--;) c = a[b], c.parentNode.removeChild(c)
    },
    on: function (a, b) {
      this.element["on" + a] = function () {
        var a = M.event;
        a.target = a.srcElement;
        b(a)
      };
      return this
    },
    shadow: function (a, c) {
      var d = [],
        e, f = this.element,
        g = this.renderer,
        h, j = f.style,
        l, n = f.path;
      n && typeof n.value !== "string" && (n = "x");
      if (a) {
        for (e = 1; e <= 3; e++) l = ['<shape isShadow="true" strokeweight="', 7 - 2 * e, '" filled="false" path="', n, '" coordsize="100,100" style="', f.style.cssText, '" />'], h = w(g.prepVML(l), null, {
          left: b(j.left) + 1,
          top: b(j.top) + 1
        }), l = ['<stroke color="black" opacity="', 0.05 * e, '"/>'], w(g.prepVML(l), null, null, h), c ? c.element.appendChild(h) : f.parentNode.insertBefore(h, f), d.push(h);
        this.shadows = d
      }
      return this
    }
  }, ia = s(fa, ia), T = {
    Element: ia,
    isIE8: Fa.indexOf("MSIE 8.0") > -1,
    init: function (a, b, c) {
      var d, e;
      this.alignedObjects = [];
      d = this.createElement(Hb);
      e = d.element;
      e.style.position = tc;
      a.appendChild(d.element);
      this.box = e;
      this.boxWrapper = d;
      this.setSize(b, c, false);
      if (!Y.namespaces.hcv) Y.namespaces.add("hcv", "urn:schemas-microsoft-com:vml"), Y.createStyleSheet().cssText = "hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "
    },
    clipRect: function (b, c, d, e) {
      var f = this.createElement();
      return a(f, {
        members: [],
        left: b,
        top: c,
        width: d,
        height: e,
        getCSS: function (b) {
          var c = this.top,
            d = this.left,
            e = d + this.width,
            f = c + this.height,
            c = {
              clip: "rect(" + S(b ? d : c) + "px," + S(b ? f : e) + "px," + S(b ? e : f) + "px," + S(b ? c : d) + "px)"
            };
          !b && la && a(c, {
            width: e + Sa,
            height: f + Sa
          });
          return c
        },
        updateClipping: function () {
          J(f.members, function (a) {
            a.css(f.getCSS(a.inverted))
          })
        }
      })
    },
    color: function (a, b, c) {
      var d, e = /^rgba/;
      if (a && a.linearGradient) {
        var f, g, h = a.linearGradient,
          j = h.x1 || h[0] || 0,
          l = h.y1 || h[1] || 0,
          n = h.x2 || h[2] || 0,
          h = h.y2 || h[3] || 0,
          k, m, o, z;
        J(a.stops, function (a, b) {
          e.test(a[1]) ? (d = yb(a[1]), f = d.get("rgb"), g = d.get("a")) : (f = a[1], g = 1);
          b ? (o = f, z = g) : (k = f, m = g)
        });
        if (c === "fill") a = 90 - A.atan((h - l) / (n - j)) * 180 / ua, a = ['<fill colors="0% ', k, ",100% ", o, '" angle="', a, '" opacity="', z, '" o:opacity2="', m, '" type="gradient" focus="100%" method="sigma" />'], w(this.prepVML(a), null, null, b);
        else return f
      } else if (e.test(a) && b.tagName !== "IMG") return d = yb(a), a = ["<", c, ' opacity="', d.get("a"), '"/>'], w(this.prepVML(a), null, null, b), d.get("rgb");
      else {
        b = b.getElementsByTagName(c);
        if (b.length) b[0].opacity = 1;
        return a
      }
    },
    prepVML: function (a) {
      var b = this.isIE8,
        a = a.join("");
      b ? (a = a.replace("/>", ' xmlns="urn:schemas-microsoft-com:vml" />'), a = a.indexOf('style="') === -1 ? a.replace("/>", ' style="display:inline-block;behavior:url(#default#VML);" />') : a.replace('style="', 'style="display:inline-block;behavior:url(#default#VML);')) : a = a.replace("<", "<hcv:");
      return a
    },
    text: ba.prototype.html,
    path: function (a) {
      return this.createElement("shape").attr({
        coordsize: "100 100",
        d: a
      })
    },
    circle: function (a, b, c) {
      return this.symbol("circle").attr({
        x: a - c,
        y: b - c,
        width: 2 * c,
        height: 2 * c
      })
    },
    g: function (a) {
      var b;
      a && (b = {
        className: Bb + a,
        "class": Bb + a
      });
      return this.createElement(Hb).attr(b)
    },
    image: function (a, b, c, d, e) {
      var f = this.createElement("img").attr({
        src: a
      });
      arguments.length > 1 && f.css({
        left: b,
        top: c,
        width: d,
        height: e
      });
      return f
    },
    rect: function (a, b, c, e, f, g) {
      if (d(a)) b = a.y, c = a.width, e = a.height, g = a.strokeWidth, a = a.x;
      var h = this.symbol("rect");
      h.r = f;
      return h.attr(h.crisp(g, a, b, E(c, 0), E(e, 0)))
    },
    invertChild: function (a, c) {
      var d = c.style;
      u(a, {
        flip: "x",
        left: b(d.width) - 10,
        top: b(d.height) - 10,
        rotation: -90
      })
    },
    symbols: {
      arc: function (a, b, c, d, e) {
        var f = e.start,
          g = e.end,
          c = e.r || c || d,
          d = X(f),
          h = I(f),
          j = X(g),
          l = I(g),
          e = e.innerR,
          n = 0.08 / c,
          k = e && 0.25 / e || 0;
        if (g - f === 0) return ["x"];
        else 2 * ua - g + f < n ? j = -n : g - f < k && (j = X(f + k));
        return ["wa", a - c, b - c, a + c, b + c, a + c * d, b + c * h, a + c * j, b + c * l, "at", a - e, b - e, a + e, b + e, a + e * j, b + e * l, a + e * d, b + e * h, "x", "e"]
      },
      circle: function (a, b, c, d) {
        return ["wa", a, b, a + c, b + d, a + c, b + d / 2, a + c, b + d / 2, "e"]
      },
      rect: function (a, b, c, d, e) {
        if (!n(e)) return [];
        var f = a + c,
          g = b + d,
          c = ea(e.r || 0, c, d);
        return [Aa, a + c, b, Ma, f - c, b, "wa", f - 2 * c, b, f, b + 2 * c, f - c, b, f, b + c, Ma, f, g - c, "wa", f - 2 * c, g - 2 * c, f, g, f, g - c, f - c, g, Ma, a + c, g, "wa", a, g - 2 * c, a + 2 * c, g, a + c, g, a, g - c, Ma, a, b + c, "wa", a, b, a + 2 * c, b + 2 * c, a, b + c, a + c, b, "x", "e"]
      }
    }
  }, ia = function () {
    this.init.apply(this, arguments)
  }, ia.prototype = da(ba.prototype, T), pa = ia;
  var zc, Bc;
  oa && (zc = function () {}, Bc = function () {
    function a() {
      var c = b.length,
        d;
      for (d = 0; d < c; d++) b[d]();
      b = []
    }
    var b = [];
    return {
      push: function (c, d) {
        b.length === 0 && Zb(d, a);
        b.push(c)
      }
    }
  }());
  pa = ia || zc || ba;
  Ea.prototype.callbacks = [];
  var Ub = function () {};
  Ub.prototype = {
    init: function (a, b, c) {
      var d = a.chart.counters;
      this.series = a;
      this.applyOptions(b, c);
      this.pointAttr = {};
      if (a.options.colorByPoint) {
        b = a.chart.options.colors;
        if (!this.options) this.options = {};
        this.color = this.options.color = this.color || b[d.color++];
        d.wrapColor(b.length)
      }
      a.chart.pointCount++;
      return this
    },
    applyOptions: function (b, c) {
      var d = this.series,
        e = typeof b;
      this.config = b;
      if (e === "number" || b === null) this.y = b;
      else if (typeof b[0] === "number") this.x = b[0], this.y = b[1];
      else if (e === "object" && typeof b.length !== "number") {
        if (a(this, b), this.options = b, b.dataLabels) d._hasPointLabels = true
      } else if (typeof b[0] === "string") this.name = b[0], this.y = b[1];
      if (this.x === P) this.x = c === P ? d.autoIncrement() : c
    },
    destroy: function () {
      var a = this.series,
        b = a.chart.hoverPoints,
        c;
      a.chart.pointCount--;
      b && (this.setState(), l(b, this));
      if (this === a.chart.hoverPoint) this.onMouseOut();
      a.chart.hoverPoints = null;
      if (this.graphic || this.dataLabel) nb(this), this.destroyElements();
      this.legendItem && this.series.chart.legend.destroyItem(this);
      for (c in this) this[c] = null
    },
    destroyElements: function () {
      for (var a = "graphic,tracker,dataLabel,group,connector,shadowGroup".split(","), b, c = 6; c--;) b = a[c], this[b] && (this[b] = this[b].destroy())
    },
    getLabelConfig: function () {
      return {
        x: this.category,
        y: this.y,
        key: this.name || this.category,
        series: this.series,
        point: this,
        percentage: this.percentage,
        total: this.total || this.stackTotal
      }
    },
    select: function (a, b) {
      var c = this,
        d = c.series.chart,
        a = o(a, !c.selected);
      c.firePointEvent(a ? "select" : "unselect", {
        accumulate: b
      }, function () {
        c.selected = a;
        c.setState(a && "select");
        b || J(d.getSelectedPoints(), function (a) {
          if (a.selected && a !== c) a.selected = false, a.setState(Wa), a.firePointEvent("unselect")
        })
      })
    },
    onMouseOver: function () {
      var a = this.series,
        b = a.chart,
        c = b.tooltip,
        d = b.hoverPoint;
      if (d && d !== this) d.onMouseOut();
      this.firePointEvent("mouseOver");
      c && (!c.shared || a.noSharedTooltip) && c.refresh(this);
      this.setState(wb);
      b.hoverPoint = this
    },
    onMouseOut: function () {
      this.firePointEvent("mouseOut");
      this.setState();
      this.series.chart.hoverPoint = null
    },
    tooltipFormatter: function (a) {
      var b = this.series,
        d = b.tooltipOptions,
        e = String(this.y).split("."),
        e = e[1] ? e[1].length : 0,
        f = a.match(/\{(series|point)\.[a-zA-Z]+\}/g),
        g = /[{\.}]/,
        h, j, l, n;
      for (n in f) j = f[n], c(j) && j !== a && (l = (" " + j).split(g), h = {
        point: this,
        series: b
      }[l[1]], l = l[2], h = h === this && (l === "y" || l === "open" || l === "high" || l === "low" || l === "close") ? (d.valuePrefix || d.yPrefix || "") + D(this[l], o(d.valueDecimals, d.yDecimals, e)) + (d.valueSuffix || d.ySuffix || "") : h[l], a = a.replace(j, h));
      return a
    },
    update: function (a, b, c) {
      var e = this,
        f = e.series,
        g = e.graphic,
        h, j = f.data,
        l = j.length,
        n = f.chart,
        b = o(b, true);
      e.firePointEvent("update", {
        options: a
      }, function () {
        e.applyOptions(a);
        d(a) && (f.getAttribs(), g && g.attr(e.pointAttr[f.state]));
        for (h = 0; h < l; h++) if (j[h] === e) {
          f.xData[h] = e.x;
          f.yData[h] = e.y;
          f.options.data[h] = a;
          break
        }
        f.isDirty = true;
        f.isDirtyData = true;
        b && n.redraw(c)
      })
    },
    remove: function (a, b) {
      var c = this,
        d = c.series,
        e = d.chart,
        f, g = d.data,
        h = g.length;
      va(b, e);
      a = o(a, true);
      c.firePointEvent("remove", null, function () {
        for (f = 0; f < h; f++) if (g[f] === c) {
          g.splice(f, 1);
          d.options.data.splice(f, 1);
          d.xData.splice(f, 1);
          d.yData.splice(f, 1);
          break
        }
        c.destroy();
        d.isDirty = true;
        d.isDirtyData = true;
        a && e.redraw()
      })
    },
    firePointEvent: function (a, b, c) {
      var d = this,
        e = this.series.options;
      (e.point.events[a] || d.options && d.options.events && d.options.events[a]) && this.importEvents();
      a === "click" && e.allowPointSelect && (c = function (a) {
        d.select(null, a.ctrlKey || a.metaKey || a.shiftKey)
      });
      Ja(this, a, b, c)
    },
    importEvents: function () {
      if (!this.hasImportedEvents) {
        var a = da(this.series.options.point, this.options).events,
          b;
        this.events = a;
        for (b in a) Pa(this, b, a[b]);
        this.hasImportedEvents = true
      }
    },
    setState: function (a) {
      var b = this.plotX,
        c = this.plotY,
        d = this.series,
        e = d.options.states,
        f = lb[d.type].marker && d.options.marker,
        g = f && !f.enabled,
        h = f && f.states[a],
        j = h && h.enabled === false,
        l = d.stateMarkerGraphic,
        n = d.chart,
        k = this.pointAttr,
        a = a || Wa;
      if (!(a === this.state || this.selected && a !== "select" || e[a] && e[a].enabled === false || a && (j || g && !h.enabled))) {
        if (this.graphic) e = f && this.graphic.symbolName && k[a].r, this.graphic.attr(da(k[a], e ? {
          x: b - e,
          y: c - e,
          width: 2 * e,
          height: 2 * e
        } : {}));
        else {
          if (a) {
            if (!l) e = f.radius, d.stateMarkerGraphic = l = n.renderer.symbol(d.symbol, -e, -e, 2 * e, 2 * e).attr(k[a]).add(d.group);
            l.translate(b, c)
          }
          if (l) l[a ? "show" : "hide"]()
        }
        this.state = a
      }
    }
  };
  var Ua = function () {};
  Ua.prototype = {
    isCartesian: true,
    type: "line",
    pointClass: Ub,
    sorted: true,
    pointAttrToOptions: {
      stroke: "lineColor",
      "stroke-width": "lineWidth",
      fill: "fillColor",
      r: "radius"
    },
    init: function (b, c) {
      var d, e;
      e = b.series.length;
      this.chart = b;
      this.options = c = this.setOptions(c);
      this.bindAxes();
      a(this, {
        index: e,
        name: c.name || "Series " + (e + 1),
        state: Wa,
        pointAttr: {},
        visible: c.visible !== false,
        selected: c.selected === true
      });
      if (oa) c.animation = false;
      e = c.events;
      for (d in e) Pa(this, d, e[d]);
      if (e && e.click || c.point && c.point.events && c.point.events.click || c.allowPointSelect) b.runTrackerClick = true;
      this.getColor();
      this.getSymbol();
      this.setData(c.data, false)
    },
    bindAxes: function () {
      var a = this,
        b = a.options,
        c = a.chart,
        d;
      a.isCartesian && J(["xAxis", "yAxis"], function (e) {
        J(c[e], function (c) {
          d = c.options;
          if (b[e] === d.index || b[e] === P && d.index === 0) c.series.push(a), a[e] = c, c.isDirty = true
        })
      })
    },
    autoIncrement: function () {
      var a = this.options,
        b = this.xIncrement,
        b = o(b, a.pointStart, 0);
      this.pointInterval = o(this.pointInterval, a.pointInterval, 1);
      this.xIncrement = b + this.pointInterval;
      return b
    },
    getSegments: function () {
      var a = -1,
        b = [],
        c, d = this.points,
        e = d.length;
      if (e) if (this.options.connectNulls) {
        for (c = e; c--;) d[c].y === null && d.splice(c, 1);
        d.length && (b = [d])
      } else J(d, function (c, f) {
        c.y === null ? (f > a + 1 && b.push(d.slice(a + 1, f)), a = f) : f === e - 1 && b.push(d.slice(a + 1, f + 1))
      });
      this.segments = b
    },
    setOptions: function (a) {
      var b = this.chart.options,
        c = b.plotOptions,
        d = a.data;
      a.data = null;
      c = da(c[this.type], c.series, a);
      c.data = a.data = d;
      this.tooltipOptions = da(b.tooltip, c.tooltip);
      return c
    },
    getColor: function () {
      var a = this.chart.options.colors,
        b = this.chart.counters;
      this.color = this.options.color || a[b.color++] || "#0000ff";
      b.wrapColor(a.length)
    },
    getSymbol: function () {
      var a = this.options.marker,
        b = this.chart,
        c = b.options.symbols,
        b = b.counters;
      this.symbol = a.symbol || c[b.symbol++];
      if (/^url/.test(this.symbol)) a.radius = 0;
      b.wrapSymbol(c.length)
    },
    addPoint: function (a, b, c, d) {
      var e = this.data,
        f = this.graph,
        g = this.area,
        h = this.chart,
        j = this.xData,
        l = this.yData,
        n = f && f.shift || 0,
        k = this.options.data;
      va(d, h);
      if (f && c) f.shift = n + 1;
      if (g) {
        if (c) g.shift = n + 1;
        g.isArea = true
      }
      b = o(b, true);
      d = {
        series: this
      };
      this.pointClass.prototype.applyOptions.apply(d, [a]);
      j.push(d.x);
      l.push(this.valueCount === 4 ? [d.open, d.high, d.low, d.close] : d.y);
      k.push(a);
      c && (e[0] ? e[0].remove(false) : (e.shift(), j.shift(), l.shift(), k.shift()));
      this.getAttribs();
      this.isDirtyData = this.isDirty = true;
      b && h.redraw()
    },
    setData: function (a, b) {
      var c = this.points,
        d = this.options,
        e = this.initialColor,
        h = this.chart,
        j = null;
      this.xIncrement = null;
      this.pointRange = this.xAxis && this.xAxis.categories && 1 || d.pointRange;
      if (n(e)) h.counters.color = e;
      var l = [],
        k = [],
        m = a ? a.length : [],
        z = this.valueCount === 4;
      if (m > (d.turboThreshold || 1E3)) {
        for (e = 0; j === null && e < m;) j = a[e], e++;
        if (g(j)) {
          j = o(d.pointStart, 0);
          d = o(d.pointInterval, 1);
          for (e = 0; e < m; e++) l[e] = j, k[e] = a[e], j += d;
          this.xIncrement = j
        } else if (f(j)) if (z) for (e = 0; e < m; e++) d = a[e], l[e] = d[0], k[e] = d.slice(1, 5);
        else for (e = 0; e < m; e++) d = a[e], l[e] = d[0], k[e] = d[1]
      } else for (e = 0; e < m; e++) d = {
        series: this
      }, this.pointClass.prototype.applyOptions.apply(d, [a[e]]), l[e] = d.x, k[e] = z ? [d.open, d.high, d.low, d.close] : d.y;
      this.data = [];
      this.options.data = a;
      this.xData = l;
      this.yData = k;
      for (e = c && c.length || 0; e--;) c[e] && c[e].destroy && c[e].destroy();
      this.isDirty = this.isDirtyData = h.isDirtyBox = true;
      o(b, true) && h.redraw(false)
    },
    remove: function (a, b) {
      var c = this,
        d = c.chart,
        a = o(a, true);
      if (!c.isRemoving) c.isRemoving = true, Ja(c, "remove", null, function () {
        c.destroy();
        d.isDirtyLegend = d.isDirtyBox = true;
        a && d.redraw(b)
      });
      c.isRemoving = false
    },
    processData: function (a) {
      var b = this.xData,
        c = this.yData,
        d = b.length,
        e = 0,
        f = d,
        g, h, j = this.xAxis,
        l = this.options,
        n = l.cropThreshold,
        k = this.isCartesian;
      if (k && !this.isDirty && !j.isDirty && !this.yAxis.isDirty && !a) return false;
      if (k && this.sorted && (!n || d > n || this.forceCrop)) if (a = j.getExtremes(), j = a.min, n = a.max, b[d - 1] < j || b[0] > n) b = [], c = [];
      else if (b[0] < j || b[d - 1] > n) {
        for (a = 0; a < d; a++) if (b[a] >= j) {
          e = E(0, a - 1);
          break
        }
        for (; a < d; a++) if (b[a] > n) {
          f = a + 1;
          break
        }
        b = b.slice(e, f);
        c = c.slice(e, f);
        g = true
      }
      for (a = b.length - 1; a > 0; a--) if (d = b[a] - b[a - 1], d > 0 && (h === P || d < h)) h = d;
      this.cropped = g;
      this.cropStart = e;
      this.processedXData = b;
      this.processedYData = c;
      if (l.pointRange === null) this.pointRange = h || 1;
      this.closestPointRange = h
    },
    generatePoints: function () {
      var a = this.options.data,
        b = this.data,
        c, d = this.processedXData,
        e = this.processedYData,
        f = this.pointClass,
        g = d.length,
        h = this.cropStart || 0,
        j, l = this.hasGroupedData,
        n, k = [],
        m;
      if (!b && !l) b = [], b.length = a.length, b = this.data = b;
      for (m = 0; m < g; m++) j = h + m, l ? k[m] = (new f).init(this, [d[m]].concat(p(e[m]))) : (b[j] ? n = b[j] : b[j] = n = (new f).init(this, a[j], d[m]), k[m] = n);
      if (b && (g !== (c = b.length) || l)) for (m = 0; m < c; m++) m === h && !l && (m += g), b[m] && b[m].destroyElements();
      this.data = b;
      this.points = k
    },
    translate: function () {
      this.processedXData || this.processData();
      this.generatePoints();
      for (var a = this.chart, b = this.options, c = b.stacking, d = this.xAxis, e = d.categories, f = this.yAxis, g = this.points, h = g.length, j = !! this.modifyValue, l, k = f.series, m = k.length; m--;) if (k[m].visible) {
        m === this.index && (l = true);
        break
      }
      for (m = 0; m < h; m++) {
        var k = g[m],
          o = k.x,
          z = k.y,
          p = k.low,
          s = f.stacks[(z < b.threshold ? "-" : "") + this.stackKey];
        k.plotX = S(d.translate(o, 0, 0, 0, 1) * 10) / 10;
        if (c && this.visible && s && s[o]) {
          p = s[o];
          o = p.total;
          p.cum = p = p.cum - z;
          z = p + z;
          if (l) p = b.threshold;
          c === "percent" && (p = o ? p * 100 / o : 0, z = o ? z * 100 / o : 0);
          k.percentage = o ? k.y * 100 / o : 0;
          k.stackTotal = o;
          k.stackY = z
        }
        k.yBottom = n(p) ? f.translate(p, 0, 1, 0, 1) : null;
        j && (z = this.modifyValue(z, k));
        k.plotY = typeof z === "number" ? S(f.translate(z, 0, 1, 0, 1) * 10) / 10 : P;
        k.clientX = a.inverted ? a.plotHeight - k.plotX : k.plotX;
        k.category = e && e[k.x] !== P ? e[k.x] : k.x
      }
      this.getSegments()
    },
    setTooltipPoints: function (a) {
      var b = this.chart,
        c = b.inverted,
        d = [],
        b = S((c ? b.plotTop : b.plotLeft) + b.plotSizeX),
        e, f;
      e = this.xAxis;
      var g, h, j = [];
      if (this.options.enableMouseTracking !== false) {
        if (a) this.tooltipPoints = null;
        J(this.segments || this.points, function (a) {
          d = d.concat(a)
        });
        e && e.reversed && (d = d.reverse());
        a = d.length;
        for (h = 0; h < a; h++) {
          g = d[h];
          e = d[h - 1] ? d[h - 1]._high + 1 : 0;
          for (f = g._high = d[h + 1] ? La((g.plotX + (d[h + 1] ? d[h + 1].plotX : b)) / 2) : b; e <= f;) j[c ? b - e++ : e++] = g
        }
        this.tooltipPoints = j
      }
    },
    tooltipHeaderFormatter: function (a) {
      var b = this.tooltipOptions,
        c = b.xDateFormat || "%A, %b %e, %Y",
        d = this.xAxis;
      return b.headerFormat.replace("{point.key}", d && d.options.type === "datetime" ? nc(c, a) : a).replace("{series.name}", this.name).replace("{series.color}", this.color)
    },
    onMouseOver: function () {
      var a = this.chart,
        b = a.hoverSeries;
      if (ra || !a.mouseIsDown) {
        if (b && b !== this) b.onMouseOut();
        this.options.events.mouseOver && Ja(this, "mouseOver");
        this.setState(wb);
        a.hoverSeries = this
      }
    },
    onMouseOut: function () {
      var a = this.options,
        b = this.chart,
        c = b.tooltip,
        d = b.hoverPoint;
      if (d) d.onMouseOut();
      this && a.events.mouseOut && Ja(this, "mouseOut");
      c && !a.stickyTracking && !c.shared && c.hide();
      this.setState();
      b.hoverSeries = null
    },
    animate: function (a) {
      var b = this.chart,
        c = this.clipRect,
        e = this.options.animation;
      e && !d(e) && (e = {});
      if (a) {
        if (!c.isAnimating) c.attr("width", 0), c.isAnimating = true
      } else c.animate({
        width: b.plotSizeX
      }, e), this.animate = null
    },
    drawPoints: function () {
      var b, c = this.points,
        d = this.chart,
        e, f, g, h, j, l, n, k;
      if (this.options.marker.enabled) for (g = c.length; g--;) if (h = c[g], e = h.plotX, f = h.plotY, k = h.graphic, f !== P && !isNaN(f)) if (b = h.pointAttr[h.selected ? "select" : Wa], j = b.r, l = o(h.marker && h.marker.symbol, this.symbol), n = l.indexOf("url") === 0, k) k.animate(a({
        x: e - j,
        y: f - j
      }, k.symbolName ? {
        width: 2 * j,
        height: 2 * j
      } : {}));
      else if (j > 0 || n) h.graphic = d.renderer.symbol(l, e - j, f - j, 2 * j, 2 * j).attr(b).add(this.group)
    },
    convertAttribs: function (a, b, c, d) {
      var e = this.pointAttrToOptions,
        f, g, h = {},
        a = a || {},
        b = b || {},
        c = c || {},
        d = d || {};
      for (f in e) g = e[f], h[f] = o(a[g], b[f], c[f], d[f]);
      return h
    },
    getAttribs: function () {
      var a = this,
        b = lb[a.type].marker ? a.options.marker : a.options,
        c = b.states,
        d = c[wb],
        e, f = a.color,
        g = {
          stroke: f,
          fill: f
        },
        h = a.points,
        j = [],
        l, k = a.pointAttrToOptions,
        m;
      a.options.marker ? (d.radius = d.radius || b.radius + 2, d.lineWidth = d.lineWidth || b.lineWidth + 1) : d.color = d.color || yb(d.color || f).brighten(d.brightness).get();
      j[Wa] = a.convertAttribs(b, g);
      J([wb, "select"], function (b) {
        j[b] = a.convertAttribs(c[b], j[Wa])
      });
      a.pointAttr = j;
      for (f = h.length; f--;) {
        g = h[f];
        if ((b = g.options && g.options.marker || g.options) && b.enabled === false) b.radius = 0;
        e = false;
        if (g.options) for (m in k) n(b[k[m]]) && (e = true);
        if (e) {
          l = [];
          c = b.states || {};
          e = c[wb] = c[wb] || {};
          if (!a.options.marker) e.color = yb(e.color || g.options.color).brighten(e.brightness || d.brightness).get();
          l[Wa] = a.convertAttribs(b, j[Wa]);
          l[wb] = a.convertAttribs(c[wb], j[wb], l[Wa]);
          l.select = a.convertAttribs(c.select, j.select, l[Wa])
        } else l = j;
        g.pointAttr = l
      }
    },
    destroy: function () {
      var a = this,
        b = a.chart,
        c = a.clipRect,
        d = /AppleWebKit\/533/.test(Fa),
        e, f, g = a.data || [],
        h, j, n;
      Ja(a, "destroy");
      nb(a);
      J(["xAxis", "yAxis"], function (b) {
        if (n = a[b]) l(n.series, a), n.isDirty = true
      });
      a.legendItem && a.chart.legend.destroyItem(a);
      for (f = g.length; f--;)(h = g[f]) && h.destroy && h.destroy();
      a.points = null;
      if (c && c !== b.clipRect) a.clipRect = c.destroy();
      J(["area", "graph", "dataLabelsGroup", "group", "tracker"], function (b) {
        a[b] && (e = d && b === "group" ? "hide" : "destroy", a[b][e]())
      });
      if (b.hoverSeries === a) b.hoverSeries = null;
      l(b.series, a);
      for (j in a) delete a[j]
    },
    drawDataLabels: function () {
      var a = this,
        b = a.options,
        c = b.dataLabels;
      if (c.enabled || a._hasPointLabels) {
        var d, e, f = a.points,
          g, h, j, l = a.dataLabelsGroup,
          k = a.chart,
          m = a.xAxis,
          m = m ? m.left : k.plotLeft,
          z = a.yAxis,
          z = z ? z.top : k.plotTop,
          p = k.renderer,
          s = k.inverted,
          q = a.type,
          w = b.stacking,
          C = q === "column" || q === "bar",
          u = c.verticalAlign === null,
          F = c.y === null,
          D = p.fontMetrics(c.style.fontSize),
          K = D.h,
          A = D.b,
          O, E;
        C && (D = {
          top: A,
          middle: A - K / 2,
          bottom: -K + A
        }, w ? (u && (c = da(c, {
          verticalAlign: "middle"
        })), F && (c = da(c, {
          y: D[c.verticalAlign]
        }))) : u ? c = da(c, {
          verticalAlign: "top"
        }) : F && (c = da(c, {
          y: D[c.verticalAlign]
        })));
        l ? l.translate(m, z) : l = a.dataLabelsGroup = p.g("data-labels").attr({
          visibility: a.visible ? sb : rb,
          zIndex: 6
        }).translate(m, z).add();
        h = c;
        J(f, function (f) {
          O = f.dataLabel;
          c = h;
          (g = f.options) && g.dataLabels && (c = da(c, g.dataLabels));
          if (E = c.enabled) {
            var m = f.barX && f.barX + f.barW / 2 || o(f.plotX, -999),
              z = o(f.plotY, -999),
              u = c.y === null ? f.y >= b.threshold ? -K + A : A : c.y;
            d = (s ? k.plotWidth - z : m) + c.x;
            e = S((s ? k.plotHeight - m : z) + u)
          }
          if (O && a.isCartesian && (!k.isInsidePlot(d, e) || !E)) f.dataLabel = O.destroy();
          else if (E) {
            m = c.align;
            j = c.formatter.call(f.getLabelConfig(), c);
            q === "column" && (d += {
              left: -1,
              right: 1
            }[m] * f.barW / 2 || 0);
            !w && s && f.y < 0 && (m = "right", d -= 10);
            c.style.color = o(c.color, c.style.color, a.color, "black");
            if (O) O.attr({
              text: j
            }).animate({
              x: d,
              y: e
            });
            else if (n(j)) O = f.dataLabel = p[c.rotation ? "text" : "label"](j, d, e, null, null, null, c.useHTML, true).attr({
              align: m,
              fill: c.backgroundColor,
              stroke: c.borderColor,
              "stroke-width": c.borderWidth,
              r: c.borderRadius,
              rotation: c.rotation,
              padding: c.padding,
              zIndex: 1
            }).css(c.style).add(l).shadow(c.shadow);
            if (C && b.stacking && O) m = f.barX, z = f.barY, u = f.barW, f = f.barH, O.align(c, null, {
              x: s ? k.plotWidth - z - f : m,
              y: s ? k.plotHeight - m - u : z,
              width: s ? f : u,
              height: s ? u : f
            })
          }
        })
      }
    },
    drawGraph: function () {
      var a = this,
        b = a.options,
        c = a.graph,
        d = [],
        e, f = a.area,
        g = a.group,
        h = b.lineColor || a.color,
        j = b.lineWidth,
        l = b.dashStyle,
        n, k = a.chart.renderer,
        m = a.yAxis.getThreshold(b.threshold),
        z = /^area/.test(a.type),
        p = [],
        s = [];
      J(a.segments, function (c) {
        n = [];
        J(c, function (d, e) {
          a.getPointSpline ? n.push.apply(n, a.getPointSpline(c, d, e)) : (n.push(e ? Ma : Aa), e && b.step && n.push(d.plotX, c[e - 1].plotY), n.push(d.plotX, d.plotY))
        });
        c.length > 1 ? d = d.concat(n) : p.push(c[0]);
        if (z) {
          var e = [],
            f, g = n.length;
          for (f = 0; f < g; f++) e.push(n[f]);
          g === 3 && e.push(Ma, n[1], n[2]);
          if (b.stacking && a.type !== "areaspline") for (f = c.length - 1; f >= 0; f--) f < c.length - 1 && b.step && e.push(c[f + 1].plotX, c[f].yBottom), e.push(c[f].plotX, c[f].yBottom);
          else e.push(Ma, c[c.length - 1].plotX, m, Ma, c[0].plotX, m);
          s = s.concat(e)
        }
      });
      a.graphPath = d;
      a.singlePoints = p;
      if (z) e = o(b.fillColor, yb(a.color).setOpacity(b.fillOpacity || 0.75).get()), f ? f.animate({
        d: s
      }) : a.area = a.chart.renderer.path(s).attr({
        fill: e
      }).add(g);
      if (c) Rb(c), c.animate({
        d: d
      });
      else if (j) {
        c = {
          stroke: h,
          "stroke-width": j
        };
        if (l) c.dashstyle = l;
        a.graph = k.path(d).attr(c).add(g).shadow(b.shadow)
      }
    },
    invertGroups: function () {
      function a() {
        var e = {
          width: b.yAxis.len,
          height: b.xAxis.len
        };
        c.attr(e).invert();
        d && d.attr(e).invert()
      }
      var b = this,
        c = b.group,
        d = b.trackerGroup,
        e = b.chart;
      Pa(e, "resize", a);
      Pa(b, "destroy", function () {
        nb(e, "resize", a)
      });
      a();
      b.invertGroups = a
    },
    render: function () {
      var a = this,
        b = a.chart,
        c, d = a.options,
        e = d.clip !== false,
        f = d.animation,
        g = f && a.animate,
        f = g ? f && f.duration || 500 : 0,
        h = a.clipRect,
        j = b.renderer;
      if (!h && (h = a.clipRect = !b.hasRendered && b.clipRect ? b.clipRect : j.clipRect(0, 0, b.plotSizeX, b.plotSizeY + 1), !b.clipRect)) b.clipRect = h;
      if (!a.group) c = a.group = j.g("series"), c.attr({
        visibility: a.visible ? sb : rb,
        zIndex: d.zIndex
      }).translate(a.xAxis.left, a.yAxis.top).add(b.seriesGroup);
      a.drawDataLabels();
      g && a.animate(true);
      a.getAttribs();
      a.drawGraph && a.drawGraph();
      a.drawPoints();
      a.options.enableMouseTracking !== false && a.drawTracker();
      b.inverted && a.invertGroups();
      e && !a.hasRendered && (c.clip(h), a.trackerGroup && a.trackerGroup.clip(b.clipRect));
      g && a.animate();
      setTimeout(function () {
        h.isAnimating = false;
        if ((c = a.group) && h !== b.clipRect && h.renderer) {
          if (e) c.clip(a.clipRect = b.clipRect);
          h.destroy()
        }
      }, f);
      a.isDirty = a.isDirtyData = false;
      a.hasRendered = true
    },
    redraw: function () {
      var a = this.chart,
        b = this.isDirtyData,
        c = this.group;
      c && (a.inverted && c.attr({
        width: a.plotWidth,
        height: a.plotHeight
      }), c.animate({
        translateX: this.xAxis.left,
        translateY: this.yAxis.top
      }));
      this.translate();
      this.setTooltipPoints(true);
      this.render();
      b && Ja(this, "updatedData")
    },
    setState: function (a) {
      var b = this.options,
        c = this.graph,
        d = b.states,
        b = b.lineWidth,
        a = a || Wa;
      if (this.state !== a) this.state = a, d[a] && d[a].enabled === false || (a && (b = d[a].lineWidth || b + 1), c && !c.dashstyle && c.attr({
        "stroke-width": b
      }, a ? 0 : 500))
    },
    setVisible: function (a, b) {
      var c = this.chart,
        d = this.legendItem,
        e = this.group,
        f = this.tracker,
        g = this.dataLabelsGroup,
        h, j = this.points,
        l = c.options.chart.ignoreHiddenSeries;
      h = this.visible;
      h = (this.visible = a = a === P ? !h : a) ? "show" : "hide";
      if (e) e[h]();
      if (f) f[h]();
      else if (j) for (e = j.length; e--;) if (f = j[e], f.tracker) f.tracker[h]();
      if (g) g[h]();
      d && c.legend.colorizeItem(this, a);
      this.isDirty = true;
      this.options.stacking && J(c.series, function (a) {
        if (a.options.stacking && a.visible) a.isDirty = true
      });
      if (l) c.isDirtyBox = true;
      b !== false && c.redraw();
      Ja(this, h)
    },
    show: function () {
      this.setVisible(true)
    },
    hide: function () {
      this.setVisible(false)
    },
    select: function (a) {
      this.selected = a = a === P ? !this.selected : a;
      if (this.checkbox) this.checkbox.checked = a;
      Ja(this, a ? "select" : "unselect")
    },
    drawTrackerGroup: function () {
      var a = this.trackerGroup,
        b = this.chart;
      if (this.isCartesian) {
        if (!a) this.trackerGroup = a = b.renderer.g().attr({
          zIndex: this.options.zIndex || 1
        }).add(b.trackerGroup);
        a.translate(this.xAxis.left, this.yAxis.top)
      }
      return a
    },
    drawTracker: function () {
      var a = this,
        b = a.options,
        c = [].concat(a.graphPath),
        d = c.length,
        e = a.chart,
        f = e.renderer,
        g = e.options.tooltip.snap,
        h = a.tracker,
        j = b.cursor,
        j = j && {
          cursor: j
        },
        l = a.singlePoints,
        n = a.drawTrackerGroup(),
        k;
      if (d) for (k = d + 1; k--;) c[k] === Aa && c.splice(k + 1, 0, c[k + 1] - g, c[k + 2], Ma), (k && c[k] === Aa || k === d) && c.splice(k, 0, Ma, c[k - 2] + g, c[k - 1]);
      for (k = 0; k < l.length; k++) d = l[k], c.push(Aa, d.plotX - g, d.plotY, Ma, d.plotX + g, d.plotY);
      h ? h.attr({
        d: c
      }) : a.tracker = f.path(c).attr({
        isTracker: true,
        stroke: vb,
        fill: ob,
        "stroke-linejoin": "bevel",
        "stroke-width": b.lineWidth + 2 * g,
        visibility: a.visible ? sb : rb
      }).on(ra ? "touchstart" : "mouseover", function () {
        if (e.hoverSeries !== a) a.onMouseOver()
      }).on("mouseout", function () {
        if (!b.stickyTracking) a.onMouseOut()
      }).css(j).add(n)
    }
  };
  T = s(Ua);
  tb.line = T;
  T = s(Ua, {
    type: "area"
  });
  tb.area = T;
  T = s(Ua, {
    type: "spline",
    getPointSpline: function (a, b, c) {
      var d = b.plotX,
        e = b.plotY,
        f = a[c - 1],
        g = a[c + 1],
        h, j, l, n;
      if (c && c < a.length - 1) {
        a = f.plotY;
        l = g.plotX;
        var g = g.plotY,
          k;
        h = (1.5 * d + f.plotX) / 2.5;
        j = (1.5 * e + a) / 2.5;
        l = (1.5 * d + l) / 2.5;
        n = (1.5 * e + g) / 2.5;
        k = (n - j) * (l - d) / (l - h) + e - n;
        j += k;
        n += k;
        j > a && j > e ? (j = E(a, e), n = 2 * e - j) : j < a && j < e && (j = ea(a, e), n = 2 * e - j);
        n > g && n > e ? (n = E(g, e), j = 2 * e - n) : n < g && n < e && (n = ea(g, e), j = 2 * e - n);
        b.rightContX = l;
        b.rightContY = n
      }
      c ? (b = ["C", f.rightContX || f.plotX, f.rightContY || f.plotY, h || d, j || e, d, e], f.rightContX = f.rightContY = null) : b = [Aa, d, e];
      return b
    }
  });
  tb.spline = T;
  T = s(T, {
    type: "areaspline"
  });
  tb.areaspline = T;
  var gc = s(Ua, {
    type: "column",
    tooltipOutsidePlot: true,
    pointAttrToOptions: {
      stroke: "borderColor",
      "stroke-width": "borderWidth",
      fill: "color",
      r: "borderRadius"
    },
    init: function () {
      Ua.prototype.init.apply(this, arguments);
      var a = this,
        b = a.chart;
      b.hasRendered && J(b.series, function (b) {
        if (b.type === a.type) b.isDirty = true
      })
    },
    translate: function () {
      var b = this,
        c = b.chart,
        d = b.options,
        e = d.stacking,
        f = d.borderWidth,
        g = 0,
        h = b.xAxis,
        j = h.reversed,
        l = {},
        k, m;
      Ua.prototype.translate.apply(b);
      J(c.series, function (a) {
        if (a.type === b.type && a.visible && b.options.group === a.options.group) a.options.stacking ? (k = a.stackKey, l[k] === P && (l[k] = g++), m = l[k]) : m = g++, a.columnIndex = m
      });
      var c = b.points,
        h = W(h.translationSlope) * (h.ordinalSlope || h.closestPointRange || 1),
        z = h * d.groupPadding,
        p = (h - 2 * z) / g,
        s = d.pointWidth,
        q = n(s) ? (p - s) / 2 : p * d.pointPadding,
        w = kb(E(o(s, p - 2 * q), 1 + 2 * f)),
        C = q + (z + ((j ? g - b.columnIndex : b.columnIndex) || 0) * p - h / 2) * (j ? -1 : 1),
        u = b.yAxis.getThreshold(d.threshold),
        F = o(d.minPointLength, 5);
      J(c, function (c) {
        var g = c.plotY,
          h = o(c.yBottom, u),
          j = c.plotX + C,
          l = kb(ea(g, h)),
          n = kb(E(g, h) - l),
          k = b.yAxis.stacks[(c.y < 0 ? "-" : "") + b.stackKey];
        e && b.visible && k && k[c.x] && k[c.x].setOffset(C, w);
        W(n) < F && F && (n = F, l = W(l - u) > F ? h - F : u - (g <= u ? F : 0));
        a(c, {
          barX: j,
          barY: l,
          barW: w,
          barH: n
        });
        c.shapeType = "rect";
        g = {
          x: j,
          y: l,
          width: w,
          height: n,
          r: d.borderRadius,
          strokeWidth: f
        };
        f % 2 && (g.y -= 1, g.height += 1);
        c.shapeArgs = g;
        c.trackerArgs = W(n) < 3 && da(c.shapeArgs, {
          height: 6,
          y: l - 3
        })
      })
    },
    getSymbol: function () {},
    drawGraph: function () {},
    drawPoints: function () {
      var a = this,
        b = a.options,
        c = a.chart.renderer,
        d, e;
      J(a.points, function (f) {
        var g = f.plotY;
        if (g !== P && !isNaN(g) && f.y !== null) d = f.graphic, e = f.shapeArgs, d ? (Rb(d), d.animate(c.Element.prototype.crisp.apply({}, [e.strokeWidth, e.x, e.y, e.width, e.height]))) : f.graphic = d = c[f.shapeType](e).attr(f.pointAttr[f.selected ? "select" : Wa]).add(a.group).shadow(b.shadow)
      })
    },
    drawTracker: function () {
      var a = this,
        b = a.chart,
        c = b.renderer,
        d, e, f = +new Date,
        g = a.options,
        h = g.cursor,
        j = h && {
          cursor: h
        },
        l = a.drawTrackerGroup(),
        n;
      J(a.points, function (h) {
        e = h.tracker;
        d = h.trackerArgs || h.shapeArgs;
        delete d.strokeWidth;
        if (h.y !== null) e ? e.attr(d) : h.tracker = c[h.shapeType](d).attr({
          isTracker: f,
          fill: vb,
          visibility: a.visible ? sb : rb
        }).on(ra ? "touchstart" : "mouseover", function (c) {
          n = c.relatedTarget || c.fromElement;
          if (b.hoverSeries !== a && m(n, "isTracker") !== f) a.onMouseOver();
          h.onMouseOver()
        }).on("mouseout", function (b) {
          if (!g.stickyTracking && (n = b.relatedTarget || b.toElement, m(n, "isTracker") !== f)) a.onMouseOut()
        }).css(j).add(h.group || l)
      })
    },
    animate: function (a) {
      var b = this,
        c = b.points,
        d = b.options;
      if (!a) J(c, function (a) {
        var c = a.graphic,
          a = a.shapeArgs,
          e = b.yAxis,
          f = d.threshold;
        c && (c.attr({
          height: 0,
          y: n(f) ? e.getThreshold(f) : e.translate(e.getExtremes().min, 0, 1, 0, 1)
        }), c.animate({
          height: a.height,
          y: a.y
        }, d.animation))
      }), b.animate = null
    },
    remove: function () {
      var a = this,
        b = a.chart;
      b.hasRendered && J(b.series, function (b) {
        if (b.type === a.type) b.isDirty = true
      });
      Ua.prototype.remove.apply(a, arguments)
    }
  });
  tb.column = gc;
  T = s(gc, {
    type: "bar",
    init: function () {
      this.inverted = true;
      gc.prototype.init.apply(this, arguments)
    }
  });
  tb.bar = T;
  T = s(Ua, {
    type: "scatter",
    sorted: false,
    translate: function () {
      var a = this;
      Ua.prototype.translate.apply(a);
      J(a.points, function (b) {
        b.shapeType = "circle";
        b.shapeArgs = {
          x: b.plotX,
          y: b.plotY,
          r: a.chart.options.tooltip.snap
        }
      })
    },
    drawTracker: function () {
      for (var a = this, b = a.options.cursor, b = b && {
        cursor: b
      }, c = a.points, d = c.length, e; d--;) if (e = c[d].graphic) e.element._i = d;
      a._hasTracking ? a._hasTracking = true : a.group.attr({
        isTracker: true
      }).on(ra ? "touchstart" : "mouseover", function (b) {
        a.onMouseOver();
        if (b.target._i !== P) c[b.target._i].onMouseOver()
      }).on("mouseout", function () {
        if (!a.options.stickyTracking) a.onMouseOut()
      }).css(b)
    }
  });
  tb.scatter = T;
  T = s(Ub, {
    init: function () {
      Ub.prototype.init.apply(this, arguments);
      var b = this,
        c;
      a(b, {
        visible: b.visible !== false,
        name: o(b.name, "Slice")
      });
      c = function () {
        b.slice()
      };
      Pa(b, "select", c);
      Pa(b, "unselect", c);
      return b
    },
    setVisible: function (a) {
      var b = this.series.chart,
        c = this.tracker,
        d = this.dataLabel,
        e = this.connector,
        f = this.shadowGroup,
        g;
      g = (this.visible = a = a === P ? !this.visible : a) ? "show" : "hide";
      this.group[g]();
      if (c) c[g]();
      if (d) d[g]();
      if (e) e[g]();
      if (f) f[g]();
      this.legendItem && b.legend.colorizeItem(this, a)
    },
    slice: function (a, b, c) {
      var d = this.series.chart,
        e = this.slicedTranslation;
      va(c, d);
      o(b, true);
      a = this.sliced = n(a) ? a : !this.sliced;
      a = {
        translateX: a ? e[0] : d.plotLeft,
        translateY: a ? e[1] : d.plotTop
      };
      this.group.animate(a);
      this.shadowGroup && this.shadowGroup.animate(a)
    }
  });
  T = s(Ua, {
    type: "pie",
    isCartesian: false,
    pointClass: T,
    pointAttrToOptions: {
      stroke: "borderColor",
      "stroke-width": "borderWidth",
      fill: "color"
    },
    getColor: function () {
      this.initialColor = this.chart.counters.color
    },
    animate: function () {
      var a = this;
      J(a.points, function (b) {
        var c = b.graphic,
          b = b.shapeArgs,
          d = -ua / 2;
        c && (c.attr({
          r: 0,
          start: d,
          end: d
        }), c.animate({
          r: b.r,
          start: b.start,
          end: b.end
        }, a.options.animation))
      });
      a.animate = null
    },
    setData: function () {
      Ua.prototype.setData.apply(this, arguments);
      this.processData();
      this.generatePoints()
    },
    translate: function () {
      this.generatePoints();
      var a = 0,
        c = -0.25,
        d = this.options,
        e = d.slicedOffset,
        f = e + d.borderWidth,
        g = d.center.concat([d.size, d.innerSize || 0]),
        h = this.chart,
        j = h.plotWidth,
        l = h.plotHeight,
        n, k, m, o = this.points,
        z = 2 * ua,
        p, s = ea(j, l),
        q, w, C = d.dataLabels.distance,
        g = xb(g, function (a, c) {
          return /%$/.test(a) ? [j, l, s, s][c] * b(a) / 100 : a
        });
      this.getX = function (a, b) {
        m = A.asin((a - g[1]) / (g[2] / 2 + C));
        return g[0] + (b ? -1 : 1) * X(m) * (g[2] / 2 + C)
      };
      this.center = g;
      J(o, function (b) {
        a += b.y
      });
      J(o, function (b) {
        p = a ? b.y / a : 0;
        n = S(c * z * 1E3) / 1E3;
        c += p;
        k = S(c * z * 1E3) / 1E3;
        b.shapeType = "arc";
        b.shapeArgs = {
          x: g[0],
          y: g[1],
          r: g[2] / 2,
          innerR: g[3] / 2,
          start: n,
          end: k
        };
        m = (k + n) / 2;
        b.slicedTranslation = xb([X(m) * e + h.plotLeft, I(m) * e + h.plotTop], S);
        q = X(m) * g[2] / 2;
        w = I(m) * g[2] / 2;
        b.tooltipPos = [g[0] + q * 0.7, g[1] + w * 0.7];
        b.labelPos = [g[0] + q + X(m) * C, g[1] + w + I(m) * C, g[0] + q + X(m) * f, g[1] + w + I(m) * f, g[0] + q, g[1] + w, C < 0 ? "center" : m < z / 4 ? "left" : "right", m];
        b.percentage = p * 100;
        b.total = a
      });
      this.setTooltipPoints()
    },
    render: function () {
      this.getAttribs();
      this.drawPoints();
      this.options.enableMouseTracking !== false && this.drawTracker();
      this.drawDataLabels();
      this.options.animation && this.animate && this.animate();
      this.isDirty = false
    },
    drawPoints: function () {
      var b = this.chart,
        c = b.renderer,
        d, e, f, g = this.options.shadow,
        h, j;
      J(this.points, function (l) {
        e = l.graphic;
        j = l.shapeArgs;
        f = l.group;
        h = l.shadowGroup;
        if (g && !h) h = l.shadowGroup = c.g("shadow").attr({
          zIndex: 4
        }).add();
        if (!f) f = l.group = c.g("point").attr({
          zIndex: 5
        }).add();
        d = l.sliced ? l.slicedTranslation : [b.plotLeft, b.plotTop];
        f.translate(d[0], d[1]);
        h && h.translate(d[0], d[1]);
        e ? e.animate(j) : l.graphic = c.arc(j).attr(a(l.pointAttr[Wa], {
          "stroke-linejoin": "round"
        })).add(l.group).shadow(g, h);
        l.visible === false && l.setVisible(false)
      })
    },
    drawDataLabels: function () {
      var a = this.data,
        b, c = this.chart,
        d = this.options.dataLabels,
        e = o(d.connectorPadding, 10),
        f = o(d.connectorWidth, 1),
        g, h, j = o(d.softConnector, true),
        l = d.distance,
        n = this.center,
        k = n[2] / 2,
        n = n[1],
        m = l > 0,
        z = [
          [],
          []
        ],
        p, s, q, w, C = 2,
        u;
      if (d.enabled) {
        Ua.prototype.drawDataLabels.apply(this);
        J(a, function (a) {
          a.dataLabel && z[a.labelPos[7] < ua / 2 ? 0 : 1].push(a)
        });
        z[1].reverse();
        w = function (a, b) {
          return b.y - a.y
        };
        for (a = z[0][0] && z[0][0].dataLabel && z[0][0].dataLabel.getBBox().height; C--;) {
          var F = [],
            D = [],
            K = z[C],
            A = K.length,
            O;
          for (u = n - k - l; u <= n + k + l; u += a) F.push(u);
          q = F.length;
          if (A > q) {
            h = [].concat(K);
            h.sort(w);
            for (u = A; u--;) h[u].rank = u;
            for (u = A; u--;) K[u].rank >= q && K.splice(u, 1);
            A = K.length
          }
          for (u = 0; u < A; u++) {
            b = K[u];
            h = b.labelPos;
            b = 9999;
            for (s = 0; s < q; s++) g = W(F[s] - h[1]), g < b && (b = g, O = s);
            if (O < u && F[u] !== null) O = u;
            else for (q < A - u + O && F[u] !== null && (O = q - A + u); F[O] === null;) O++;
            D.push({
              i: O,
              y: F[O]
            });
            F[O] = null
          }
          D.sort(w);
          for (u = 0; u < A; u++) {
            b = K[u];
            h = b.labelPos;
            g = b.dataLabel;
            s = D.pop();
            p = h[1];
            q = b.visible === false ? rb : sb;
            O = s.i;
            s = s.y;
            if (p > s && F[O + 1] !== null || p < s && F[O - 1] !== null) s = p;
            p = this.getX(O === 0 || O === F.length - 1 ? p : s, C);
            g.attr({
              visibility: q,
              align: h[6]
            })[g.moved ? "animate" : "attr"]({
              x: p + d.x + ({
                left: e,
                right: -e
              }[h[6]] || 0),
              y: s + d.y
            });
            g.moved = true;
            if (m && f) g = b.connector, h = j ? [Aa, p + (h[6] === "left" ? 5 : -5), s, "C", p, s, 2 * h[2] - h[4], 2 * h[3] - h[5], h[2], h[3], Ma, h[4], h[5]] : [Aa, p + (h[6] === "left" ? 5 : -5), s, Ma, h[2], h[3], Ma, h[4], h[5]], g ? (g.animate({
              d: h
            }), g.attr("visibility", q)) : b.connector = g = this.chart.renderer.path(h).attr({
              "stroke-width": f,
              stroke: d.connectorColor || b.color || "#606060",
              visibility: q,
              zIndex: 3
            }).translate(c.plotLeft, c.plotTop).add()
          }
        }
      }
    },
    drawTracker: gc.prototype.drawTracker,
    getSymbol: function () {}
  });
  tb.pie = T;
  a(Highcharts, {
    Chart: Ea,
    dateFormat: nc,
    pathAnim: Ab,
    getOptions: function () {
      return Ba
    },
    hasBidiBug: Ia,
    numberFormat: D,
    Point: Ub,
    Color: yb,
    Renderer: pa,
    SVGRenderer: ba,
    VMLRenderer: ia,
    CanVGRenderer: zc,
    seriesTypes: tb,
    setOptions: function (a) {
      Db = da(Db, a.xAxis);
      vc = da(vc, a.yAxis);
      a.xAxis = a.yAxis = P;
      Ba = da(Ba, a);
      Z();
      return Ba
    },
    Series: Ua,
    addEvent: Pa,
    removeEvent: nb,
    createElement: w,
    discardElement: sa,
    css: u,
    each: J,
    extend: a,
    map: xb,
    merge: da,
    pick: o,
    splat: p,
    extendClass: s,
    placeBox: O,
    product: "Highcharts",
    version: "2.2.1"
  })
})();
(function (a) {
  function e(b) {
    a.extend(this, {
      x: String(b).replace(/middle/i, "center").match(/left|right|center/i)[0].toLowerCase(),
      y: String(b).replace(/middle/i, "center").match(/top|bottom|center/i)[0].toLowerCase(),
      offset: {
        left: 0,
        top: 0
      },
      precedance: b.charAt(0).search(/^(t|b)/) > -1 ? "y" : "x",
      string: function () {
        return this.precedance === "y" ? this.y + this.x : this.x + this.y
      }
    })
  }
  function b(a, b, c) {
    b = {
      bottomright: [
        [0, 0],
        [b, c],
        [b, 0]
      ],
      bottomleft: [
        [0, 0],
        [b, 0],
        [0, c]
      ],
      topright: [
        [0, c],
        [b, 0],
        [b, c]
      ],
      topleft: [
        [0, 0],
        [0, c],
        [b, c]
      ],
      topcenter: [
        [0, c],
        [b / 2, 0],
        [b, c]
      ],
      bottomcenter: [
        [0, 0],
        [b, 0],
        [b / 2, c]
      ],
      rightcenter: [
        [0, 0],
        [b, c / 2],
        [0, c]
      ],
      leftcenter: [
        [b, 0],
        [b, c],
        [0, c / 2]
      ]
    };
    b.lefttop = b.bottomright;
    b.righttop = b.bottomleft;
    b.leftbottom = b.topright;
    b.rightbottom = b.topleft;
    return b[a]
  }
  function c(b) {
    var c;
    a("<canvas />").get(0).getContext ? c = {
      topLeft: [b, b],
      topRight: [0, b],
      bottomLeft: [b, 0],
      bottomRight: [0, 0]
    } : a.browser.msie && (c = {
      topLeft: [-90, 90, 0],
      topRight: [-90, 90, -b],
      bottomLeft: [90, 270, 0],
      bottomRight: [90, 270, -b]
    });
    return c
  }
  function d(b, c) {
    var d, e;
    d = a.extend(true, {}, b);
    for (e in d) c === true && /(tip|classes)/i.test(e) ? delete d[e] : !c && /(width|border|tip|title|classes|user)/i.test(e) && delete d[e];
    return d
  }
  function f(a) {
    if (typeof a.tip !== "object") a.tip = {
      corner: a.tip
    };
    if (typeof a.tip.size !== "object") a.tip.size = {
      width: a.tip.size,
      height: a.tip.size
    };
    if (typeof a.border !== "object") a.border = {
      width: a.border
    };
    if (typeof a.width !== "object") a.width = {
      value: a.width
    };
    if (typeof a.width.max === "string") a.width.max = parseInt(a.width.max.replace(/([0-9]+)/i, "$1"), 10);
    if (typeof a.width.min === "string") a.width.min = parseInt(a.width.min.replace(/([0-9]+)/i, "$1"), 10);
    if (typeof a.tip.size.x === "number") a.tip.size.width = a.tip.size.x, delete a.tip.size.x;
    if (typeof a.tip.size.y === "number") a.tip.size.height = a.tip.size.y, delete a.tip.size.y;
    return a
  }
  function g() {
    var b, c;
    c = [true,
    {}];
    for (b = 0; b < arguments.length; b++) c.push(arguments[b]);
    for (b = [a.extend.apply(a, c)]; typeof b[0].name === "string";) b.unshift(f(a.fn.qtip.styles[b[0].name]));
    b.unshift(true, {
      classes: {
        tooltip: "qtip-" + (arguments[0].name || "defaults")
      }
    }, a.fn.qtip.styles.defaults);
    b = a.extend.apply(a, b);
    c = a.browser.msie ? 1 : 0;
    b.tip.size.width += c;
    b.tip.size.height += c;
    b.tip.size.width % 2 > 0 && (b.tip.size.width += 1);
    b.tip.size.height % 2 > 0 && (b.tip.size.height += 1);
    if (b.tip.corner === true) b.tip.corner = this.options.position.corner.tooltip === "center" && this.options.position.corner.target === "center" ? false : this.options.position.corner.tooltip;
    return b
  }
  function h(a, b, c, d) {
    a = a.get(0).getContext("2d");
    a.fillStyle = d;
    a.beginPath();
    a.arc(b[0], b[1], c, 0, Math.PI * 2, false);
    a.fill()
  }
  function j() {
    var b, d, e, f, g, j, l, n, m;
    b = this;
    b.elements.wrapper.find(".qtip-borderBottom, .qtip-borderTop").remove();
    e = b.options.style.border.width;
    f = b.options.style.border.radius;
    g = b.options.style.border.color || b.options.style.tip.color;
    j = c(f);
    l = {};
    for (d in j) l[d] = '<div rel="' + d + '" style="' + (/Left/.test(d) ? "left" : "right") + ":0; position:absolute; height:" + f + "px; width:" + f + 'px; overflow:hidden; line-height:0.1px; font-size:1px">', a("<canvas />").get(0).getContext ? l[d] += '<canvas height="' + f + '" width="' + f + '" style="vertical-align: top"></canvas>' : a.browser.msie && (n = f * 2 + 3, l[d] += '<v:arc stroked="false" fillcolor="' + g + '" startangle="' + j[d][0] + '" endangle="' + j[d][1] + '" style="width:' + n + "px; height:" + n + "px; margin-top:" + (/bottom/.test(d) ? -2 : -1) + "px; margin-left:" + (/Right/.test(d) ? j[d][2] - 3.5 : -1) + 'px; vertical-align:top; display:inline-block; behavior:url(#default#VML)"></v:arc>'), l[d] += "</div>";
    d = b.getDimensions().width - Math.max(e, f) * 2;
    d = '<div class="qtip-betweenCorners" style="height:' + f + "px; width:" + d + "px; overflow:hidden; background-color:" + g + '; line-height:0.1px; font-size:1px;">';
    b.elements.wrapper.prepend('<div class="qtip-borderTop" dir="ltr" style="height:' + f + "px; margin-left:" + f + 'px; line-height:0.1px; font-size:1px; padding:0;">' + l.topLeft + l.topRight + d);
    b.elements.wrapper.append('<div class="qtip-borderBottom" dir="ltr" style="height:' + f + "px; margin-left:" + f + 'px; line-height:0.1px; font-size:1px; padding:0;">' + l.bottomLeft + l.bottomRight + d);
    a("<canvas />").get(0).getContext ? b.elements.wrapper.find("canvas").each(function () {
      m = j[a(this).parent("[rel]:first").attr("rel")];
      h.call(b, a(this), m, f, g)
    }) : a.browser.msie && b.elements.tooltip.append('<v:image style="behavior:url(#default#VML);"></v:image>');
    l = Math.max(f, f + (e - f));
    e = Math.max(e - f, 0);
    b.elements.contentWrapper.css({
      border: "0px solid " + g,
      borderWidth: e + "px " + l + "px"
    })
  }
  function l(a, b, c) {
    a = a.get(0).getContext("2d");
    a.fillStyle = c;
    a.beginPath();
    a.moveTo(b[0][0], b[0][1]);
    a.lineTo(b[1][0], b[1][1]);
    a.lineTo(b[2][0], b[2][1]);
    a.fill()
  }

  function n(b) {
    var c, d;
    this.options.style.tip.corner !== false && this.elements.tip && (b || (b = new e(this.elements.tip.attr("rel"))), c = d = a.browser.msie ? 1 : 0, this.elements.tip.css(b[b.precedance], 0), b.precedance === "y" ? (a.browser.msie && (d = parseInt(a.browser.version.charAt(0), 10) === 6 ? b.y === "top" ? -3 : 1 : b.y === "top" ? 1 : 2), b.x === "center" ? this.elements.tip.css({
      left: "50%",
      marginLeft: -(this.options.style.tip.size.width / 2)
    }) : b.x === "left" ? this.elements.tip.css({
      left: this.options.style.border.radius - c
    }) : this.elements.tip.css({
      right: this.options.style.border.radius + c
    }), b.y === "top" ? this.elements.tip.css({
      top: -d
    }) : this.elements.tip.css({
      bottom: d
    })) : (a.browser.msie && (d = parseInt(a.browser.version.charAt(0), 10) === 6 ? 1 : b.x === "left" ? 1 : 2), b.y === "center" ? this.elements.tip.css({
      top: "50%",
      marginTop: -(this.options.style.tip.size.height / 2)
    }) : b.y === "top" ? this.elements.tip.css({
      top: this.options.style.border.radius - c
    }) : this.elements.tip.css({
      bottom: this.options.style.border.radius + c
    }), b.x === "left" ? this.elements.tip.css({
      left: -d
    }) : this.elements.tip.css({
      right: d
    })), c = "padding-" + b[b.precedance], b = this.options.style.tip.size[b.precedance === "x" ? "width" : "height"], this.elements.tooltip.css("padding", 0), this.elements.tooltip.css(c, b), a.browser.msie && parseInt(a.browser.version.charAt(0), 6) === 6 && (b = parseInt(this.elements.tip.css("margin-top"), 10) || 0, b += parseInt(this.elements.content.css("margin-top"), 10) || 0, this.elements.tip.css({
      marginTop: b
    })))
  }
  function m() {
    var b = this;
    b.elements.title !== null && b.elements.title.remove();
    b.elements.tooltip.attr("aria-labelledby", "qtip-" + b.id + "-title");
    b.elements.title = a('<div id="qtip-' + b.id + '-title" class="' + b.options.style.classes.title + '"></div>').css(d(b.options.style.title, true)).css({
      zoom: a.browser.msie ? 1 : 0
    }).prependTo(b.elements.contentWrapper);
    b.options.content.title.text && b.updateTitle.call(b, b.options.content.title.text);
    if (b.options.content.title.button !== false && typeof b.options.content.title.button === "string") b.elements.button = a('<a class="' + b.options.style.classes.button + '" role="button" style="float:right; position: relative"></a>').css(d(b.options.style.button, true)).html(b.options.content.title.button).prependTo(b.elements.title).click(function (a) {
      b.status.disabled || b.hide(a)
    })
  }
  function p() {
    function b(c) {
      if (e.status.disabled !== true) clearTimeout(e.timers.inactive), e.timers.inactive = setTimeout(function () {
        a(h).each(function () {
          g.unbind(this + ".qtip-inactive");
          e.elements.content.unbind(this + ".qtip-inactive")
        });
        e.hide(c)
      }, e.options.hide.delay)
    }
    function c(d) {
      if (e.status.disabled !== true) e.options.hide.when.event === "inactive" && (a(h).each(function () {
        g.bind(this + ".qtip-inactive", b);
        e.elements.content.bind(this + ".qtip-inactive", b)
      }), b()), clearTimeout(e.timers.show), clearTimeout(e.timers.hide), e.options.show.delay > 0 ? e.timers.show = setTimeout(function () {
        e.show(d)
      }, e.options.show.delay) : e.show(d)
    }
    function d(b) {
      if (e.status.disabled !== true) {
        if (e.options.hide.fixed === true && /mouse(out|leave)/i.test(e.options.hide.when.event) && a(b.relatedTarget).parents('div.qtip[id^="qtip"]').length > 0) return b.stopPropagation(), b.preventDefault(), clearTimeout(e.timers.hide), false;
        clearTimeout(e.timers.show);
        clearTimeout(e.timers.hide);
        e.elements.tooltip.stop(true, true);
        e.timers.hide = setTimeout(function () {
          e.hide(b)
        }, e.options.hide.delay)
      }
    }
    var e, f, g, h;
    e = this;
    f = e.options.show.when.target;
    g = e.options.hide.when.target;
    e.options.hide.fixed && (g = g.add(e.elements.tooltip));
    h = "click,dblclick,mousedown,mouseup,mousemove,mouseout,mouseenter,mouseleave,mouseover".split(",");
    e.options.hide.fixed === true && e.elements.tooltip.bind("mouseover.qtip", function () {
      e.status.disabled !== true && clearTimeout(e.timers.hide)
    });
    e.options.show.when.target.add(e.options.hide.when.target).length === 1 && e.options.show.when.event === e.options.hide.when.event && e.options.hide.when.event !== "inactive" || e.options.hide.when.event === "unfocus" ? (e.cache.toggle = 0, f.bind(e.options.show.when.event + ".qtip", function (a) {
      e.cache.toggle === 0 ? c(a) : d(a)
    })) : (f.bind(e.options.show.when.event + ".qtip", c), e.options.hide.when.event !== "inactive" && g.bind(e.options.hide.when.event + ".qtip", d));
    /(fixed|absolute)/.test(e.options.position.type) && e.elements.tooltip.bind("mouseover.qtip", e.focus);
    e.options.position.target === "mouse" && e.options.position.type !== "static" && f.bind("mousemove.qtip", function (a) {
      e.cache.mouse = {
        x: a.pageX,
        y: a.pageY
      };
      e.status.disabled === false && e.options.position.adjust.mouse === true && e.options.position.type !== "static" && e.elements.tooltip.css("display") !== "none" && e.updatePosition(a)
    })
  }
  function o() {
    var a;
    a = this.getDimensions();
    this.elements.bgiframe = this.elements.wrapper.prepend('<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:false" style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=\'0\'); border: 1px solid red; height:' + a.height + "px; width:" + a.width + 'px" />').children(".qtip-bgiframe:first")
  }
  function u() {
    var c, f, g;
    this.beforeRender.call(this);
    this.status.rendered = true;
    this.elements.tooltip = '<div qtip="' + this.id + '" id="qtip-' + this.id + '" role="tooltip" aria-describedby="qtip-' + this.id + '-content" class="qtip ' + (this.options.style.classes.tooltip || this.options.style) + '" style="display:none; -moz-border-radius:0; -webkit-border-radius:0; border-radius:0; position:' + this.options.position.type + ';">   <div class="qtip-wrapper" style="position:relative; overflow:hidden; text-align:left;">     <div class="qtip-contentWrapper" style="overflow:hidden;">        <div id="qtip-' + this.id + '-content" class="qtip-content ' + this.options.style.classes.content + '"></div> </div></div></div>';
    this.elements.tooltip = a(this.elements.tooltip);
    this.elements.tooltip.appendTo(this.options.position.container);
    this.elements.tooltip.data("qtip", {
      current: 0,
      interfaces: [this]
    });
    this.elements.wrapper = this.elements.tooltip.children("div:first");
    this.elements.contentWrapper = this.elements.wrapper.children("div:first").css({
      background: this.options.style.background
    });
    this.elements.content = this.elements.contentWrapper.children("div:first").css(d(this.options.style));
    a.browser.msie && this.elements.wrapper.add(this.elements.content).css({
      zoom: 1
    });
    this.options.hide.when.event === "unfocus" && this.elements.tooltip.attr("unfocus", true);
    typeof this.options.style.width.value === "number" && this.updateWidth();
    if (a("<canvas />").get(0).getContext || a.browser.msie) {
      if (this.options.style.border.radius > 0 ? j.call(this) : this.elements.contentWrapper.css({
        border: this.options.style.border.width + "px solid " + this.options.style.border.color
      }), this.options.style.tip.corner !== false) {
        c = void 0;
        var h, o;
        this.elements.tip !== null && this.elements.tip.remove();
        f = this.options.style.tip.color || this.options.style.border.color;
        if (this.options.style.tip.corner !== false) c || (c = new e(this.options.style.tip.corner)), g = b(c.string(), this.options.style.tip.size.width, this.options.style.tip.size.height), this.elements.tip = '<div class="' + this.options.style.classes.tip + '" dir="ltr" rel="' + c.string() + '" style="position:absolute; height:' + this.options.style.tip.size.height + "px; width:" + this.options.style.tip.size.width + 'px; margin:0 auto; line-height:0.1px; font-size:1px;"></div>', this.elements.tooltip.prepend(this.elements.tip), a("<canvas />").get(0).getContext ? h = '<canvas height="' + this.options.style.tip.size.height + '" width="' + this.options.style.tip.size.width + '"></canvas>' : a.browser.msie && (h = this.options.style.tip.size.width + "," + this.options.style.tip.size.height, o = "m" + g[0][0] + "," + g[0][1], o += " l" + g[1][0] + "," + g[1][1], o += " " + g[2][0] + "," + g[2][1], o += " xe", h = '<v:shape fillcolor="' + f + '" stroked="false" filled="true" path="' + o + '" coordsize="' + h + '" style="width:' + this.options.style.tip.size.width + "px; height:" + this.options.style.tip.size.height + "px; line-height:0.1px; display:inline-block; behavior:url(#default#VML); vertical-align:" + (c.y === "top" ? "bottom" : "top") + '"></v:shape>', h += '<v:image style="behavior:url(#default#VML);"></v:image>', this.elements.contentWrapper.css("position", "relative")), this.elements.tip = this.elements.tooltip.find("." + this.options.style.classes.tip).eq(0), this.elements.tip.html(h), a("<canvas  />").get(0).getContext && l.call(this, this.elements.tip.find("canvas:first"), g, f), c.y === "top" && a.browser.msie && parseInt(a.browser.version.charAt(0), 10) === 6 && this.elements.tip.css({
          marginTop: -4
        }), n.call(this, c)
      }
    } else this.elements.contentWrapper.css({
      border: this.options.style.border.width + "px solid " + this.options.style.border.color
    }), this.options.style.border.radius = 0, this.options.style.tip.corner = false;
    typeof this.options.content.text === "string" && this.options.content.text.length > 0 || this.options.content.text.jquery && this.options.content.text.length > 0 ? c = this.options.content.text : typeof this.elements.target.attr("title") === "string" && this.elements.target.attr("title").length > 0 ? (c = this.elements.target.attr("title").replace(/\n/gi, "<br />"), this.elements.target.attr("title", "")) : typeof this.elements.target.attr("alt") === "string" && this.elements.target.attr("alt").length > 0 ? (c = this.elements.target.attr("alt").replace(/\n/gi, "<br />"), this.elements.target.attr("alt", "")) : c = " ";
    this.options.content.title.text !== false && m.call(this);
    this.updateContent(c);
    p.call(this);
    this.options.show.ready === true && this.show();
    if (this.options.content.url !== false) c = this.options.content.url, f = this.options.content.data, g = this.options.content.method || "get", this.loadContent(c, f, g);
    this.onRender.call(this)
  }
  function w(e, f, j) {
    var m = this;
    m.id = j;
    m.options = f;
    m.status = {
      animated: false,
      rendered: false,
      disabled: false,
      focused: false
    };
    m.elements = {
      target: e.addClass(m.options.style.classes.target),
      tooltip: null,
      wrapper: null,
      content: null,
      contentWrapper: null,
      title: null,
      button: null,
      tip: null,
      bgiframe: null
    };
    m.cache = {
      mouse: {},
      position: {},
      toggle: 0
    };
    m.timers = {};
    a.extend(m, m.options.api, {
      show: function (b) {
        function c() {
          m.elements.tooltip.attr("aria-hidden", true);
          m.options.position.type !== "static" && m.focus();
          m.onShow.call(m, b);
          a.browser.msie && m.elements.tooltip.get(0).style.removeAttribute("filter");
          m.elements.tooltip.css({
            opacity: ""
          })
        }
        var d;
        if (!m.status.rendered) return false;
        if (m.elements.tooltip.css("display") !== "none") return m;
        m.elements.tooltip.stop(true, false);
        if (m.beforeShow.call(m, b) === false) return m;
        m.cache.toggle = 1;
        m.options.position.type !== "static" && m.updatePosition(b, m.options.show.effect.length > 0);
        typeof m.options.show.solo === "object" ? d = a(m.options.show.solo) : m.options.show.solo === true && (d = a("div.qtip").not(m.elements.tooltip));
        d && d.each(function () {
          a(this).qtip("api").status.rendered === true && a(this).qtip("api").hide()
        });
        if (typeof m.options.show.effect.type === "function") m.options.show.effect.type.call(m.elements.tooltip, m.options.show.effect.length), m.elements.tooltip.queue(function () {
          c();
          a(this).dequeue()
        });
        else {
          switch (m.options.show.effect.type.toLowerCase()) {
          case "fade":
            m.elements.tooltip.fadeIn(m.options.show.effect.length, c);
            break;
          case "slide":
            m.elements.tooltip.slideDown(m.options.show.effect.length, function () {
              c();
              m.options.position.type !== "static" && m.updatePosition(b, true)
            });
            break;
          case "grow":
            m.elements.tooltip.show(m.options.show.effect.length, c);
            break;
          default:
            m.elements.tooltip.show(null, c)
          }
          m.elements.tooltip.addClass(m.options.style.classes.active)
        }
        return m
      },
      hide: function (b) {
        function c() {
          m.elements.tooltip.attr("aria-hidden", true);
          m.elements.tooltip.css({
            opacity: ""
          });
          m.onHide.call(m, b)
        }
        if (m.status.rendered) {
          if (m.elements.tooltip.css("display") === "none") return m
        } else return false;
        clearTimeout(m.timers.show);
        m.elements.tooltip.stop(true, false);
        if (m.beforeHide.call(m, b) === false) return m;
        m.cache.toggle = 0;
        if (typeof m.options.hide.effect.type === "function") m.options.hide.effect.type.call(m.elements.tooltip, m.options.hide.effect.length), m.elements.tooltip.queue(function () {
          c();
          a(this).dequeue()
        });
        else {
          switch (m.options.hide.effect.type.toLowerCase()) {
          case "fade":
            m.elements.tooltip.fadeOut(m.options.hide.effect.length, c);
            break;
          case "slide":
            m.elements.tooltip.slideUp(m.options.hide.effect.length, c);
            break;
          case "grow":
            m.elements.tooltip.hide(m.options.hide.effect.length, c);
            break;
          default:
            m.elements.tooltip.hide(null, c)
          }
          m.elements.tooltip.removeClass(m.options.style.classes.active)
        }
        return m
      },
      toggle: function (a, b) {
        var c = /boolean|number/.test(typeof b) ? b : !m.elements.tooltip.is(":visible");
        m[c ? "show" : "hide"](a);
        return m
      },
      updatePosition: function (b, c) {
        if (!m.status.rendered) return false;
        var d = a(f.position.target),
          e = f.position,
          g, h, j = m.elements.tooltip.width(),
          l = m.elements.tooltip.height(),
          n, p, k, s, u, w, C = {
            left: function () {
              var b = n.left + j - a(window).width() - a(window).scrollLeft(),
                c = p.x === "left" ? -j : p.x === "right" ? j : 0,
                d = -2 * e.adjust.x;
              n.left += n.left < 0 ? c + g + d : b > 0 ? c - g + d : 0;
              return Math.round(b)
            },
            top: function () {
              var b = n.top + l - a(window).height() - a(window).scrollTop(),
                c = p.y === "top" ? -l : p.y === "bottom" ? l : 0,
                d = k.y === "top" ? h : k.y === "bottom" ? -h : 0,
                f = -2 * e.adjust.y;
              n.top += n.top < 0 ? c + h + f : b > 0 ? c + d + f : 0;
              return Math.round(b)
            }
          };
        p = f.position.corner.tooltip;
        k = f.position.corner.target;
        if (b && f.position.target === "mouse") k = {
          x: "left",
          y: "top"
        }, g = h = 0, n = {
          top: b.pageY,
          left: b.pageX
        };
        else {
          if (d[0] === document) g = d.width(), h = d.height(), n = {
            top: 0,
            left: 0
          };
          else if (d[0] === window) g = d.width(), h = d.height(), n = {
            top: d.scrollTop(),
            left: d.scrollLeft()
          };
          else if (d.is("area")) {
            s = m.options.position.target.attr("coords").split(",");
            for (u = 0; u < s.length; u++) s[u] = parseInt(s[u], 10);
            u = m.options.position.target.parent("map").attr("name");
            w = a('img[usemap="#' + u + '"]:first').offset();
            d.position = {
              left: Math.floor(w.left + s[0]),
              top: Math.floor(w.top + s[1])
            };
            switch (m.options.position.target.attr("shape").toLowerCase()) {
            case "rect":
              g = Math.ceil(Math.abs(s[2] - s[0]));
              h = Math.ceil(Math.abs(s[3] - s[1]));
              break;
            case "circle":
              g = s[2] + 1;
              h = s[2] + 1;
              break;
            case "poly":
              g = s[0];
              h = s[1];
              for (u = 0; u < s.length; u++) if (u % 2 === 0) {
                if (s[u] > g && (g = s[u]), s[u] < s[0]) n.left = Math.floor(w.left + s[u])
              } else if (s[u] > h && (h = s[u]), s[u] < s[1]) n.top = Math.floor(w.top + s[u]);
              g -= n.left - w.left;
              h -= n.top - w.top
            }
            g -= 2;
            h -= 2
          } else g = d.outerWidth(), h = d.outerHeight(), n = d.offset();
          n.left += k.x === "right" ? g : k.x === "center" ? g / 2 : 0;
          n.top += k.y === "bottom" ? h : k.y === "center" ? h / 2 : 0
        }
        n.left += e.adjust.x + (p.x === "right" ? -j : p.x === "center" ? -j / 2 : 0);
        n.top += e.adjust.y + (p.y === "bottom" ? -l : p.y === "center" ? -l / 2 : 0);
        e.adjust.screen && (C.left(), C.top());
        !m.elements.bgiframe && a.browser.msie && parseInt(a.browser.version.charAt(0), 10) === 6 && o.call(m);
        if (m.beforePositionUpdate.call(m, b) === false) return m;
        m.cache.position = n;
        c === true ? (m.status.animated = true, m.elements.tooltip.animate(n, 200, "swing", function () {
          m.status.animated = false
        })) : m.elements.tooltip.css(n);
        m.onPositionUpdate.call(m, b);
        return m
      },
      updateWidth: function (b) {
        if (!m.status.rendered || b && typeof b !== "number") return false;
        var c = m.elements.contentWrapper.siblings().add(m.elements.tip).add(m.elements.button),
          d = m.elements.wrapper.add(m.elements.contentWrapper.children()),
          e = m.elements.tooltip,
          f = m.options.style.width.max,
          g = m.options.style.width.min;
        if (!b) typeof m.options.style.width.value === "number" ? b = m.options.style.width.value : (m.elements.tooltip.css({
          width: "auto"
        }), c.hide(), e.width(b), a.browser.msie && d.css({
          zoom: ""
        }), b = m.getDimensions().width, m.options.style.width.value || (b = Math.min(Math.max(b, g), f)));
        b % 2 && (b -= 1);
        m.elements.tooltip.width(b);
        c.show();
        m.options.style.border.radius && m.elements.tooltip.find(".qtip-betweenCorners").each(function () {
          a(this).width(b - m.options.style.border.radius * 2)
        });
        a.browser.msie && (d.css({
          zoom: 1
        }), m.elements.wrapper.width(b), m.elements.bgiframe && m.elements.bgiframe.width(b).height(m.getDimensions.height));
        return m
      },
      updateStyle: function (e) {
        var f, j, n, o;
        if (!m.status.rendered || typeof e !== "string" || !a.fn.qtip.styles[e]) return false;
        m.options.style = g.call(m, a.fn.qtip.styles[e], m.options.user.style);
        m.elements.content.css(d(m.options.style));
        m.options.content.title.text !== false && m.elements.title.css(d(m.options.style.title, true));
        m.elements.contentWrapper.css({
          borderColor: m.options.style.border.color
        });
        m.options.style.tip.corner !== false && (a("<canvas />").get(0).getContext ? (e = m.elements.tooltip.find(".qtip-tip canvas:first"), j = e.get(0).getContext("2d"), j.clearRect(0, 0, 300, 300), n = e.parent("div[rel]:first").attr("rel"), o = b(n, m.options.style.tip.size.width, m.options.style.tip.size.height), l.call(m, e, o, m.options.style.tip.color || m.options.style.border.color)) : a.browser.msie && (e = m.elements.tooltip.find('.qtip-tip [nodeName="shape"]'), e.attr("fillcolor", m.options.style.tip.color || m.options.style.border.color)));
        m.options.style.border.radius > 0 && (m.elements.tooltip.find(".qtip-betweenCorners").css({
          backgroundColor: m.options.style.border.color
        }), a("<canvas />").get(0).getContext ? (f = c(m.options.style.border.radius), m.elements.tooltip.find(".qtip-wrapper canvas").each(function () {
          j = a(this).get(0).getContext("2d");
          j.clearRect(0, 0, 300, 300);
          n = a(this).parent("div[rel]:first").attr("rel");
          h.call(m, a(this), f[n], m.options.style.border.radius, m.options.style.border.color)
        })) : a.browser.msie && m.elements.tooltip.find('.qtip-wrapper [nodeName="arc"]').each(function () {
          a(this).attr("fillcolor", m.options.style.border.color)
        }));
        return m
      },
      updateContent: function (b, c) {
        function d() {
          m.updateWidth();
          c !== false && (m.options.position.type !== "static" && m.updatePosition(m.elements.tooltip.is(":visible"), true), m.options.style.tip.corner !== false && n.call(m))
        }
        var e, f, g;
        if (!m.status.rendered || !b) return false;
        e = m.beforeContentUpdate.call(m, b);
        if (typeof e === "string") b = e;
        else if (e === false) return;
        a.browser.msie && m.elements.contentWrapper.children().css({
          zoom: "normal"
        });
        b.jquery && b.length > 0 ? b.clone(true).appendTo(m.elements.content).show() : m.elements.content.html(b);
        f = m.elements.content.find("img[complete=false]");
        f.length > 0 ? (g = 0, f.each(function () {
          a('<img src="' + a(this).attr("src") + '" />').load(function () {
            ++g === f.length && d()
          })
        })) : d();
        m.onContentUpdate.call(m);
        return m
      },
      loadContent: function (b, c, d) {
        function e(a) {
          m.onContentLoad.call(m);
          m.updateContent(a)
        }
        if (!m.status.rendered) return false;
        if (m.beforeContentLoad.call(m) === false) return m;
        d === "post" ? a.post(b, c, e) : a.get(b, c, e);
        return m
      },
      updateTitle: function (a) {
        if (!m.status.rendered || !a) return false;
        if (m.beforeTitleUpdate.call(m) === false) return m;
        if (m.elements.button) m.elements.button = m.elements.button.clone(true);
        m.elements.title.html(a);
        m.elements.button && m.elements.title.prepend(m.elements.button);
        m.onTitleUpdate.call(m);
        return m
      },
      focus: function (b) {
        var c, d, e;
        if (!m.status.rendered || m.options.position.type === "static") return false;
        c = parseInt(m.elements.tooltip.css("z-index"), 10);
        d = 15E3 + a('div.qtip[id^="qtip"]').length - 1;
        if (!m.status.focused && c !== d) {
          c = m.beforeFocus.call(m, b);
          if (c === false) return m;
          a('div.qtip[id^="qtip"]').not(m.elements.tooltip).each(function () {
            if (a(this).qtip("api").status.rendered === true) e = parseInt(a(this).css("z-index"), 10), typeof e === "number" && e > -1 && a(this).css({
              zIndex: parseInt(a(this).css("z-index"), 10) - 1
            }), a(this).qtip("api").status.focused = false
          });
          m.elements.tooltip.css({
            zIndex: d
          });
          m.status.focused = true;
          m.onFocus.call(m, b)
        }
        return m
      },
      disable: function (a) {
        if (!m.status.rendered) return false;
        m.status.disabled = a ? true : false;
        return m
      },
      destroy: function () {
        var b, c;
        if (m.beforeDestroy.call(m) === false) return m;
        m.status.rendered ? (m.options.show.when.target.unbind("mousemove.qtip", m.updatePosition), m.options.show.when.target.unbind("mouseout.qtip", m.hide), m.options.show.when.target.unbind(m.options.show.when.event + ".qtip"), m.options.hide.when.target.unbind(m.options.hide.when.event + ".qtip"), m.elements.tooltip.unbind(m.options.hide.when.event + ".qtip"), m.elements.tooltip.unbind("mouseover.qtip", m.focus), m.elements.tooltip.remove()) : m.options.show.when.target.unbind(m.options.show.when.event + ".qtip-create");
        if (typeof m.elements.target.data("qtip") === "object" && (c = m.elements.target.data("qtip").interfaces, typeof c === "object" && c.length > 0)) for (b = 0; b < c.length - 1; b++) c[b].id === m.id && c.splice(b, 1);
        a.fn.qtip.interfaces.splice(m.id, 1);
        typeof c === "object" && c.length > 0 ? m.elements.target.data("qtip").current = c.length - 1 : m.elements.target.removeData("qtip");
        m.onDestroy.call(m);
        return m.elements.target
      },
      getPosition: function () {
        var a, b;
        if (!m.status.rendered) return false;
        (a = m.elements.tooltip.css("display") !== "none" ? false : true) && m.elements.tooltip.css({
          visiblity: "hidden"
        }).show();
        b = m.elements.tooltip.offset();
        a && m.elements.tooltip.css({
          visiblity: "visible"
        }).hide();
        return b
      },
      getDimensions: function () {
        var a, b;
        if (!m.status.rendered) return false;
        (a = !m.elements.tooltip.is(":visible") ? true : false) && m.elements.tooltip.css({
          visiblity: "hidden"
        }).show();
        b = {
          height: m.elements.tooltip.outerHeight(),
          width: m.elements.tooltip.outerWidth()
        };
        a && m.elements.tooltip.css({
          visiblity: "visible"
        }).hide();
        return b
      }
    })
  }
  a(document).ready(function () {
    a.fn.qtip.cache = {
      screen: {
        scroll: {
          left: a(window).scrollLeft(),
          top: a(window).scrollTop()
        },
        width: a(window).width(),
        height: a(window).height()
      }
    };
    var b, c;
    a(window).bind("resize scroll", function (d) {
      clearTimeout(b);
      b = setTimeout(function () {
        d.type === "scroll" ? a.fn.qtip.cache.screen.scroll = {
          left: a(window).scrollLeft(),
          top: a(window).scrollTop()
        } : (a.fn.qtip.cache.screen.width = a(window).width(), a.fn.qtip.cache.screen.height = a(window).height());
        for (c = 0; c < a.fn.qtip.interfaces.length; c++) {
          var b = a.fn.qtip.interfaces[c];
          b && b.status && b.status.rendered === true && b.options.position.type !== "static" && (b.options.position.adjust.scroll && d.type === "scroll" || b.options.position.adjust.resize && d.type === "resize") && b.updatePosition(d, true)
        }
      }, 100)
    });
    a(document).bind("mousedown.qtip", function (b) {
      a(b.target).parents("div.qtip").length === 0 && a(".qtip[unfocus]").each(function () {
        var c = a(this).qtip("api");
        a(this).is(":visible") && c && c.status && !c.status.disabled && a(b.target).add(c.elements.target).length > 1 && c.hide(b)
      })
    })
  });
  a.fn.qtip = function (b, c) {
    var d, h, j, l, n, m, o, p;
    if (typeof b === "string") if (b === "api") return a(this).data("qtip").interfaces[a(this).data("qtip").current];
    else {
      if (b === "interfaces") return a(this).data("qtip").interfaces
    } else {
      b || (b = {});
      if (typeof b.content !== "object" || b.content.jquery && b.content.length > 0) b.content = {
        text: b.content
      };
      if (typeof b.content.title !== "object") b.content.title = {
        text: b.content.title
      };
      if (typeof b.position !== "object") b.position = {
        corner: b.position
      };
      if (typeof b.position.corner !== "object") b.position.corner = {
        target: b.position.corner,
        tooltip: b.position.corner
      };
      if (typeof b.show !== "object") b.show = {
        when: b.show
      };
      if (typeof b.show.when !== "object") b.show.when = {
        event: b.show.when
      };
      if (typeof b.show.effect !== "object") b.show.effect = {
        type: b.show.effect
      };
      if (typeof b.hide !== "object") b.hide = {
        when: b.hide
      };
      if (typeof b.hide.when !== "object") b.hide.when = {
        event: b.hide.when
      };
      if (typeof b.hide.effect !== "object") b.hide.effect = {
        type: b.hide.effect
      };
      if (typeof b.style !== "object") b.style = {
        name: b.style
      };
      b.style = f(b.style);
      l = a.extend(true, {}, a.fn.qtip.defaults, b);
      l.style = g.call({
        options: l
      }, l.style);
      l.user = a.extend(true, {}, b)
    }
    return a(this).each(function () {
      if (typeof b === "string") {
        if (m = b.toLowerCase(), j = a(this).qtip("interfaces"), typeof j === "object") if (c === true && m === "destroy") for (; j.length > 0;) j[j.length - 1].destroy();
        else {
          c !== true && (j = [a(this).qtip("api")]);
          for (d = 0; d < j.length; d++) m === "destroy" ? j[d].destroy() : j[d].status.rendered === true && (m === "show" ? j[d].show() : m === "hide" ? j[d].hide() : m === "focus" ? j[d].focus() : m === "disable" ? j[d].disable(true) : m === "enable" ? j[d].disable(false) : m === "update" && j[d].updatePosition())
        }
      } else {
        o = a.extend(true, {}, l);
        o.hide.effect.length = l.hide.effect.length;
        o.show.effect.length = l.show.effect.length;
        if (o.position.container === false) o.position.container = a(document.body);
        if (o.position.target === false) o.position.target = a(this);
        if (o.show.when.target === false) o.show.when.target = a(this);
        if (o.hide.when.target === false) o.hide.when.target = a(this);
        o.position.corner.tooltip = new e(o.position.corner.tooltip);
        o.position.corner.target = new e(o.position.corner.target);
        h = a.fn.qtip.interfaces.length;
        for (d = 0; d < h; d++) if (typeof a.fn.qtip.interfaces[d] === "undefined") {
          h = d;
          break
        }
        n = new w(a(this), o, h);
        a.fn.qtip.interfaces[h] = n;
        if (typeof a(this).data("qtip") === "object" && a(this).data("qtip")) {
          if (typeof a(this).attr("qtip") === "undefined") a(this).data("qtip").current = a(this).data("qtip").interfaces.length;
          a(this).data("qtip").interfaces.push(n)
        } else a(this).data("qtip", {
          current: 0,
          interfaces: [n]
        });
        o.content.prerender === false && o.show.when.event !== false && o.show.ready !== true ? o.show.when.target.bind(o.show.when.event + ".qtip-" + h + "-create", {
          qtip: h
        }, function (b) {
          p = a.fn.qtip.interfaces[b.data.qtip];
          p.options.show.when.target.unbind(p.options.show.when.event + ".qtip-" + b.data.qtip + "-create");
          p.cache.mouse = {
            x: b.pageX,
            y: b.pageY
          };
          u.call(p);
          p.options.show.when.target.trigger(p.options.show.when.event)
        }) : (n.cache.mouse = {
          x: o.show.when.target.offset().left,
          y: o.show.when.target.offset().top
        }, u.call(n))
      }
    })
  };
  a.fn.qtip.interfaces = [];
  a.fn.qtip.log = {
    error: function () {
      return this
    }
  };
  a.fn.qtip.constants = {};
  a.fn.qtip.defaults = {
    content: {
      prerender: false,
      text: false,
      url: false,
      data: null,
      title: {
        text: false,
        button: false
      }
    },
    position: {
      target: false,
      corner: {
        target: "bottomRight",
        tooltip: "topLeft"
      },
      adjust: {
        x: 0,
        y: 0,
        mouse: true,
        screen: false,
        scroll: true,
        resize: true
      },
      type: "absolute",
      container: false
    },
    show: {
      when: {
        target: false,
        event: "mouseover"
      },
      effect: {
        type: "fade",
        length: 100
      },
      delay: 140,
      solo: false,
      ready: false
    },
    hide: {
      when: {
        target: false,
        event: "mouseout"
      },
      effect: {
        type: "fade",
        length: 100
      },
      delay: 0,
      fixed: false
    },
    api: {
      beforeRender: function () {},
      onRender: function () {},
      beforePositionUpdate: function () {},
      onPositionUpdate: function () {},
      beforeShow: function () {},
      onShow: function () {},
      beforeHide: function () {},
      onHide: function () {},
      beforeContentUpdate: function () {},
      onContentUpdate: function () {},
      beforeContentLoad: function () {},
      onContentLoad: function () {},
      beforeTitleUpdate: function () {},
      onTitleUpdate: function () {},
      beforeDestroy: function () {},
      onDestroy: function () {},
      beforeFocus: function () {},
      onFocus: function () {}
    }
  };
  a.fn.qtip.styles = {
    defaults: {
      background: "white",
      color: "#111",
      overflow: "hidden",
      textAlign: "left",
      width: {
        min: 0,
        max: 250
      },
      padding: "5px 9px",
      border: {
        width: 1,
        radius: 0,
        color: "#d3d3d3"
      },
      tip: {
        corner: false,
        color: false,
        size: {
          width: 13,
          height: 13
        },
        opacity: 1
      },
      title: {
        background: "#e1e1e1",
        fontWeight: "bold",
        padding: "7px 12px"
      },
      button: {
        cursor: "pointer"
      },
      classes: {
        target: "",
        tip: "qtip-tip",
        title: "qtip-title",
        button: "qtip-button",
        content: "qtip-content",
        active: "qtip-active"
      }
    },
    cream: {
      border: {
        width: 3,
        radius: 0,
        color: "#F9E98E"
      },
      title: {
        background: "#F0DE7D",
        color: "#A27D35"
      },
      background: "#FBF7AA",
      color: "#A27D35",
      classes: {
        tooltip: "qtip-cream"
      }
    },
    light: {
      border: {
        width: 3,
        radius: 0,
        color: "#E2E2E2"
      },
      title: {
        background: "#f1f1f1",
        color: "#454545"
      },
      background: "white",
      color: "#454545",
      classes: {
        tooltip: "qtip-light"
      }
    },
    dark: {
      border: {
        width: 3,
        radius: 0,
        color: "#303030"
      },
      title: {
        background: "#404040",
        color: "#f3f3f3"
      },
      background: "#505050",
      color: "#f3f3f3",
      classes: {
        tooltip: "qtip-dark"
      }
    },
    red: {
      border: {
        width: 3,
        radius: 0,
        color: "#CE6F6F"
      },
      title: {
        background: "#f28279",
        color: "#9C2F2F"
      },
      background: "#F79992",
      color: "#9C2F2F",
      classes: {
        tooltip: "qtip-red"
      }
    },
    green: {
      border: {
        width: 3,
        radius: 0,
        color: "#A9DB66"
      },
      title: {
        background: "#b9db8c",
        color: "#58792E"
      },
      background: "#CDE6AC",
      color: "#58792E",
      classes: {
        tooltip: "qtip-green"
      }
    },
    blue: {
      border: {
        width: 3,
        radius: 0,
        color: "#ADD9ED"
      },
      title: {
        background: "#D0E9F5",
        color: "#5E99BD"
      },
      background: "#E5F6FE",
      color: "#4D9FBF",
      classes: {
        tooltip: "qtip-blue"
      }
    }
  }
})(jQuery);
jQuery.effects ||
function (a) {
  function e(b) {
    var c;
    return b && b.constructor == Array && b.length == 3 ? b : (c = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(b)) ? [parseInt(c[1], 10), parseInt(c[2], 10), parseInt(c[3], 10)] : (c = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(b)) ? [parseFloat(c[1]) * 2.55, parseFloat(c[2]) * 2.55, parseFloat(c[3]) * 2.55] : (c = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(b)) ? [parseInt(c[1], 16), parseInt(c[2], 16), parseInt(c[3], 16)] : (c = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(b)) ? [parseInt(c[1] + c[1], 16), parseInt(c[2] + c[2], 16), parseInt(c[3] + c[3], 16)] : /rgba\(0, 0, 0, 0\)/.exec(b) ? g.transparent : g[a.trim(b).toLowerCase()]
  }
  function b() {
    var a = document.defaultView ? document.defaultView.getComputedStyle(this, null) : this.currentStyle,
      b = {},
      c, d;
    if (a && a.length && a[0] && a[a[0]]) for (var e = a.length; e--;) c = a[e], typeof a[c] == "string" && (d = c.replace(/\-(\w)/g, function (a, b) {
      return b.toUpperCase()
    }), b[d] = a[c]);
    else for (c in a) typeof a[c] === "string" && (b[c] = a[c]);
    return b
  }
  function c(b) {
    var c, d;
    for (c in b) d = b[c], (d == null || a.isFunction(d) || c in j || /scrollbar/.test(c) || !/color/i.test(c) && isNaN(parseFloat(d))) && delete b[c];
    return b
  }
  function d(a, b) {
    var c = {
      _: 0
    },
      d;
    for (d in b) a[d] != b[d] && (c[d] = b[d]);
    return c
  }
  function f(b, c, d, e) {
    if (typeof b == "object") e = c, d = null, c = b, b = c.effect;
    a.isFunction(c) && (e = c, d = null, c = {});
    a.isFunction(d) && (e = d, d = null);
    if (typeof c == "number" || a.fx.speeds[c]) e = d, d = c, c = {};
    c = c || {};
    d = d || c.duration;
    d = a.fx.off ? 0 : typeof d == "number" ? d : a.fx.speeds[d] || a.fx.speeds._default;
    e = e || c.complete;
    return [b, c, d, e]
  }
  a.effects = {};
  a.each("backgroundColor,borderBottomColor,borderLeftColor,borderRightColor,borderTopColor,color,outlineColor".split(","), function (b, c) {
    a.fx.step[c] = function (b) {
      if (!b.colorInit) {
        var d;
        d = b.elem;
        var f = c,
          g;
        do {
          g = a.curCSS(d, f);
          if (g != "" && g != "transparent" || a.nodeName(d, "body")) break;
          f = "backgroundColor"
        } while (d = d.parentNode);
        d = e(g);
        b.start = d;
        b.end = e(b.end);
        b.colorInit = true
      }
      b.elem.style[c] = "rgb(" + Math.max(Math.min(parseInt(b.pos * (b.end[0] - b.start[0]) + b.start[0], 10), 255), 0) + "," + Math.max(Math.min(parseInt(b.pos * (b.end[1] - b.start[1]) + b.start[1], 10), 255), 0) + "," + Math.max(Math.min(parseInt(b.pos * (b.end[2] - b.start[2]) + b.start[2], 10), 255), 0) + ")"
    }
  });
  var g = {
    aqua: [0, 255, 255],
    azure: [240, 255, 255],
    beige: [245, 245, 220],
    black: [0, 0, 0],
    blue: [0, 0, 255],
    brown: [165, 42, 42],
    cyan: [0, 255, 255],
    darkblue: [0, 0, 139],
    darkcyan: [0, 139, 139],
    darkgrey: [169, 169, 169],
    darkgreen: [0, 100, 0],
    darkkhaki: [189, 183, 107],
    darkmagenta: [139, 0, 139],
    darkolivegreen: [85, 107, 47],
    darkorange: [255, 140, 0],
    darkorchid: [153, 50, 204],
    darkred: [139, 0, 0],
    darksalmon: [233, 150, 122],
    darkviolet: [148, 0, 211],
    fuchsia: [255, 0, 255],
    gold: [255, 215, 0],
    green: [0, 128, 0],
    indigo: [75, 0, 130],
    khaki: [240, 230, 140],
    lightblue: [173, 216, 230],
    lightcyan: [224, 255, 255],
    lightgreen: [144, 238, 144],
    lightgrey: [211, 211, 211],
    lightpink: [255, 182, 193],
    lightyellow: [255, 255, 224],
    lime: [0, 255, 0],
    magenta: [255, 0, 255],
    maroon: [128, 0, 0],
    navy: [0, 0, 128],
    olive: [128, 128, 0],
    orange: [255, 165, 0],
    pink: [255, 192, 203],
    purple: [128, 0, 128],
    violet: [128, 0, 128],
    red: [255, 0, 0],
    silver: [192, 192, 192],
    white: [255, 255, 255],
    yellow: [255, 255, 0],
    transparent: [255, 255, 255]
  },
    h = ["add", "remove", "toggle"],
    j = {
      border: 1,
      borderBottom: 1,
      borderColor: 1,
      borderLeft: 1,
      borderRight: 1,
      borderTop: 1,
      borderWidth: 1,
      margin: 1,
      padding: 1
    };
  a.effects.animateClass = function (e, f, g, j) {
    a.isFunction(g) && (j = g, g = null);
    return this.each(function () {
      var o = a(this),
        u = o.attr("style") || " ",
        w = c(b.call(this)),
        s, D = o.attr("className");
      a.each(h, function (a, b) {
        e[b] && o[b + "Class"](e[b])
      });
      s = c(b.call(this));
      o.attr("className", D);
      o.animate(d(w, s), f, g, function () {
        a.each(h, function (a, b) {
          e[b] && o[b + "Class"](e[b])
        });
        typeof o.attr("style") == "object" ? (o.attr("style").cssText = "", o.attr("style").cssText = u) : o.attr("style", u);
        j && j.apply(this, arguments)
      })
    })
  };
  a.fn.extend({
    _addClass: a.fn.addClass,
    addClass: function (b, c, d, e) {
      return c ? a.effects.animateClass.apply(this, [{
        add: b
      },
      c, d, e]) : this._addClass(b)
    },
    _removeClass: a.fn.removeClass,
    removeClass: function (b, c, d, e) {
      return c ? a.effects.animateClass.apply(this, [{
        remove: b
      },
      c, d, e]) : this._removeClass(b)
    },
    _toggleClass: a.fn.toggleClass,
    toggleClass: function (b, c, d, e, f) {
      return typeof c == "boolean" || c === void 0 ? d ? a.effects.animateClass.apply(this, [c ? {
        add: b
      } : {
        remove: b
      },
      d, e, f]) : this._toggleClass(b, c) : a.effects.animateClass.apply(this, [{
        toggle: b
      },
      c, d, e])
    },
    switchClass: function (b, c, d, e, f) {
      return a.effects.animateClass.apply(this, [{
        add: c,
        remove: b
      },
      d, e, f])
    }
  });
  a.extend(a.effects, {
    version: "1.8.2",
    save: function (a, b) {
      for (var c = 0; c < b.length; c++) b[c] !== null && a.data("ec.storage." + b[c], a[0].style[b[c]])
    },
    restore: function (a, b) {
      for (var c = 0; c < b.length; c++) b[c] !== null && a.css(b[c], a.data("ec.storage." + b[c]))
    },
    setMode: function (a, b) {
      b == "toggle" && (b = a.is(":hidden") ? "show" : "hide");
      return b
    },
    getBaseline: function (a, b) {
      var c;
      switch (a[0]) {
      case "top":
        c = 0;
        break;
      case "middle":
        c = 0.5;
        break;
      case "bottom":
        c = 1;
        break;
      default:
        c = a[0] / b.height
      }
      switch (a[1]) {
      case "left":
        a = 0;
        break;
      case "center":
        a = 0.5;
        break;
      case "right":
        a = 1;
        break;
      default:
        a = a[1] / b.width
      }
      return {
        x: a,
        y: c
      }
    },
    createWrapper: function (b) {
      if (b.parent().is(".ui-effects-wrapper")) return b.parent();
      var c = {
        width: b.outerWidth(true),
        height: b.outerHeight(true),
        "float": b.css("float")
      },
        d = a("<div></div>").addClass("ui-effects-wrapper").css({
          fontSize: "100%",
          background: "transparent",
          border: "none",
          margin: 0,
          padding: 0
        });
      b.wrap(d);
      d = b.parent();
      b.css("position") == "static" ? (d.css({
        position: "relative"
      }), b.css({
        position: "relative"
      })) : (a.extend(c, {
        position: b.css("position"),
        zIndex: b.css("z-index")
      }), a.each(["top", "left", "bottom", "right"], function (a, d) {
        c[d] = b.css(d);
        isNaN(parseInt(c[d], 10)) && (c[d] = "auto")
      }), b.css({
        position: "relative",
        top: 0,
        left: 0
      }));
      return d.css(c).show()
    },
    removeWrapper: function (a) {
      return a.parent().is(".ui-effects-wrapper") ? a.parent().replaceWith(a) : a
    },
    setTransition: function (b, c, d, e) {
      e = e || {};
      a.each(c, function (a, c) {
        unit = b.cssUnit(c);
        unit[0] > 0 && (e[c] = unit[0] * d + unit[1])
      });
      return e
    }
  });
  a.fn.extend({
    effect: function (b) {
      var c = f.apply(this, arguments),
        c = {
          options: c[1],
          duration: c[2],
          callback: c[3]
        },
        d = a.effects[b];
      return d && !a.fx.off ? d.call(this, c) : this
    },
    _show: a.fn.show,
    show: function (b) {
      if (!b || typeof b == "number" || a.fx.speeds[b]) return this._show.apply(this, arguments);
      else {
        var c = f.apply(this, arguments);
        c[1].mode = "show";
        return this.effect.apply(this, c)
      }
    },
    _hide: a.fn.hide,
    hide: function (b) {
      if (!b || typeof b == "number" || a.fx.speeds[b]) return this._hide.apply(this, arguments);
      else {
        var c = f.apply(this, arguments);
        c[1].mode = "hide";
        return this.effect.apply(this, c)
      }
    },
    __toggle: a.fn.toggle,
    toggle: function (b) {
      if (!b || typeof b == "number" || a.fx.speeds[b] || typeof b == "boolean" || a.isFunction(b)) return this.__toggle.apply(this, arguments);
      else {
        var c = f.apply(this, arguments);
        c[1].mode = "toggle";
        return this.effect.apply(this, c)
      }
    },
    cssUnit: function (b) {
      var c = this.css(b),
        d = [];
      a.each(["em", "px", "%", "pt"], function (a, b) {
        c.indexOf(b) > 0 && (d = [parseFloat(c), b])
      });
      return d
    }
  });
  a.easing.jswing = a.easing.swing;
  a.extend(a.easing, {
    def: "easeOutQuad",
    swing: function (b, c, d, e, f) {
      return a.easing[a.easing.def](b, c, d, e, f)
    },
    easeInQuad: function (a, b, c, d, e) {
      return d * (b /= e) * b + c
    },
    easeOutQuad: function (a, b, c, d, e) {
      return -d * (b /= e) * (b - 2) + c
    },
    easeInOutQuad: function (a, b, c, d, e) {
      return (b /= e / 2) < 1 ? d / 2 * b * b + c : -d / 2 * (--b * (b - 2) - 1) + c
    },
    easeInCubic: function (a, b, c, d, e) {
      return d * (b /= e) * b * b + c
    },
    easeOutCubic: function (a, b, c, d, e) {
      return d * ((b = b / e - 1) * b * b + 1) + c
    },
    easeInOutCubic: function (a, b, c, d, e) {
      return (b /= e / 2) < 1 ? d / 2 * b * b * b + c : d / 2 * ((b -= 2) * b * b + 2) + c
    },
    easeInQuart: function (a, b, c, d, e) {
      return d * (b /= e) * b * b * b + c
    },
    easeOutQuart: function (a, b, c, d, e) {
      return -d * ((b = b / e - 1) * b * b * b - 1) + c
    },
    easeInOutQuart: function (a, b, c, d, e) {
      return (b /= e / 2) < 1 ? d / 2 * b * b * b * b + c : -d / 2 * ((b -= 2) * b * b * b - 2) + c
    },
    easeInQuint: function (a, b, c, d, e) {
      return d * (b /= e) * b * b * b * b + c
    },
    easeOutQuint: function (a, b, c, d, e) {
      return d * ((b = b / e - 1) * b * b * b * b + 1) + c
    },
    easeInOutQuint: function (a, b, c, d, e) {
      return (b /= e / 2) < 1 ? d / 2 * b * b * b * b * b + c : d / 2 * ((b -= 2) * b * b * b * b + 2) + c
    },
    easeInSine: function (a, b, c, d, e) {
      return -d * Math.cos(b / e * (Math.PI / 2)) + d + c
    },
    easeOutSine: function (a, b, c, d, e) {
      return d * Math.sin(b / e * (Math.PI / 2)) + c
    },
    easeInOutSine: function (a, b, c, d, e) {
      return -d / 2 * (Math.cos(Math.PI * b / e) - 1) + c
    },
    easeInExpo: function (a, b, c, d, e) {
      return b == 0 ? c : d * Math.pow(2, 10 * (b / e - 1)) + c
    },
    easeOutExpo: function (a, b, c, d, e) {
      return b == e ? c + d : d * (-Math.pow(2, -10 * b / e) + 1) + c
    },
    easeInOutExpo: function (a, b, c, d, e) {
      return b == 0 ? c : b == e ? c + d : (b /= e / 2) < 1 ? d / 2 * Math.pow(2, 10 * (b - 1)) + c : d / 2 * (-Math.pow(2, -10 * --b) + 2) + c
    },
    easeInCirc: function (a, b, c, d, e) {
      return -d * (Math.sqrt(1 - (b /= e) * b) - 1) + c
    },
    easeOutCirc: function (a, b, c, d, e) {
      return d * Math.sqrt(1 - (b = b / e - 1) * b) + c
    },
    easeInOutCirc: function (a, b, c, d, e) {
      return (b /= e / 2) < 1 ? -d / 2 * (Math.sqrt(1 - b * b) - 1) + c : d / 2 * (Math.sqrt(1 - (b -= 2) * b) + 1) + c
    },
    easeInElastic: function (a, b, c, d, e) {
      var f = 0,
        g = d;
      if (b == 0) return c;
      if ((b /= e) == 1) return c + d;
      f || (f = e * 0.3);
      g < Math.abs(d) ? (g = d, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g);
      return -(g * Math.pow(2, 10 * (b -= 1)) * Math.sin((b * e - a) * 2 * Math.PI / f)) + c
    },
    easeOutElastic: function (a, b, c, d, e) {
      var f = 0,
        g = d;
      if (b == 0) return c;
      if ((b /= e) == 1) return c + d;
      f || (f = e * 0.3);
      g < Math.abs(d) ? (g = d, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g);
      return g * Math.pow(2, -10 * b) * Math.sin((b * e - a) * 2 * Math.PI / f) + d + c
    },
    easeInOutElastic: function (a, b, c, d, e) {
      var f = 0,
        g = d;
      if (b == 0) return c;
      if ((b /= e / 2) == 2) return c + d;
      f || (f = e * 0.3 * 1.5);
      g < Math.abs(d) ? (g = d, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g);
      return b < 1 ? -0.5 * g * Math.pow(2, 10 * (b -= 1)) * Math.sin((b * e - a) * 2 * Math.PI / f) + c : g * Math.pow(2, -10 * (b -= 1)) * Math.sin((b * e - a) * 2 * Math.PI / f) * 0.5 + d + c
    },
    easeInBack: function (a, b, c, d, e, f) {
      f == void 0 && (f = 1.70158);
      return d * (b /= e) * b * ((f + 1) * b - f) + c
    },
    easeOutBack: function (a, b, c, d, e, f) {
      f == void 0 && (f = 1.70158);
      return d * ((b = b / e - 1) * b * ((f + 1) * b + f) + 1) + c
    },
    easeInOutBack: function (a, b, c, d, e, f) {
      f == void 0 && (f = 1.70158);
      return (b /= e / 2) < 1 ? d / 2 * b * b * (((f *= 1.525) + 1) * b - f) + c : d / 2 * ((b -= 2) * b * (((f *= 1.525) + 1) * b + f) + 2) + c
    },
    easeInBounce: function (b, c, d, e, f) {
      return e - a.easing.easeOutBounce(b, f - c, 0, e, f) + d
    },
    easeOutBounce: function (a, b, c, d, e) {
      return (b /= e) < 1 / 2.75 ? d * 7.5625 * b * b + c : b < 2 / 2.75 ? d * (7.5625 * (b -= 1.5 / 2.75) * b + 0.75) + c : b < 2.5 / 2.75 ? d * (7.5625 * (b -= 2.25 / 2.75) * b + 0.9375) + c : d * (7.5625 * (b -= 2.625 / 2.75) * b + 0.984375) + c
    },
    easeInOutBounce: function (b, c, d, e, f) {
      return c < f / 2 ? a.easing.easeInBounce(b, c * 2, 0, e, f) * 0.5 + d : a.easing.easeOutBounce(b, c * 2 - f, 0, e, f) * 0.5 + e * 0.5 + d
    }
  })
}(jQuery);
(function (a) {
  a.effects.highlight = function (e) {
    return this.queue(function () {
      var b = a(this),
        c = ["backgroundImage", "backgroundColor", "opacity"],
        d = a.effects.setMode(b, e.options.mode || "show"),
        f = {
          backgroundColor: b.css("backgroundColor")
        };
      if (d == "hide") f.opacity = 0;
      a.effects.save(b, c);
      b.show().css({
        backgroundImage: "none",
        backgroundColor: e.options.color || "#ffff99"
      }).animate(f, {
        queue: false,
        duration: e.duration,
        easing: e.options.easing,
        complete: function () {
          d == "hide" && b.hide();
          a.effects.restore(b, c);
          d == "show" && !a.support.opacity && this.style.removeAttribute("filter");
          e.callback && e.callback.apply(this, arguments);
          b.dequeue()
        }
      })
    })
  }
})(jQuery);
(function (a) {
  a.effects.puff = function (e) {
    return this.queue(function () {
      var b = a(this),
        c = a.effects.setMode(b, e.options.mode || "hide"),
        d = parseInt(e.options.percent, 10) || 150,
        f = d / 100,
        g = {
          height: b.height(),
          width: b.width()
        };
      a.extend(e.options, {
        fade: true,
        mode: c,
        percent: c == "hide" ? d : 100,
        from: c == "hide" ? g : {
          height: g.height * f,
          width: g.width * f
        }
      });
      b.effect("scale", e.options, e.duration, e.callback);
      b.dequeue()
    })
  };
  a.effects.scale = function (e) {
    return this.queue(function () {
      var b = a(this),
        c = a.extend(true, {}, e.options),
        d = a.effects.setMode(b, e.options.mode || "effect"),
        f = parseInt(e.options.percent, 10) || (parseInt(e.options.percent, 10) == 0 ? 0 : d == "hide" ? 0 : 100),
        g = e.options.direction || "both",
        h = e.options.origin;
      if (d != "effect") c.origin = h || ["middle", "center"], c.restore = true;
      h = {
        height: b.height(),
        width: b.width()
      };
      b.from = e.options.from || (d == "show" ? {
        height: 0,
        width: 0
      } : h);
      f = {
        y: g != "horizontal" ? f / 100 : 1,
        x: g != "vertical" ? f / 100 : 1
      };
      b.to = {
        height: h.height * f.y,
        width: h.width * f.x
      };
      if (e.options.fade) {
        if (d == "show") b.from.opacity = 0, b.to.opacity = 1;
        if (d == "hide") b.from.opacity = 1, b.to.opacity = 0
      }
      c.from = b.from;
      c.to = b.to;
      c.mode = d;
      b.effect("size", c, e.duration, e.callback);
      b.dequeue()
    })
  };
  a.effects.size = function (e) {
    return this.queue(function () {
      var b = a(this),
        c = "position,top,left,width,height,overflow,opacity".split(","),
        d = ["position", "top", "left", "overflow", "opacity"],
        f = ["width", "height", "overflow"],
        g = ["fontSize"],
        h = ["borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom"],
        j = ["borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight"],
        l = a.effects.setMode(b, e.options.mode || "effect"),
        n = e.options.restore || false,
        m = e.options.scale || "both",
        p = e.options.origin,
        o = {
          height: b.height(),
          width: b.width()
        };
      b.from = e.options.from || o;
      b.to = e.options.to || o;
      if (p) p = a.effects.getBaseline(p, o), b.from.top = (o.height - b.from.height) * p.y, b.from.left = (o.width - b.from.width) * p.x, b.to.top = (o.height - b.to.height) * p.y, b.to.left = (o.width - b.to.width) * p.x;
      var u = b.from.height / o.height,
        w = b.from.width / o.width,
        s = b.to.height / o.height,
        D = b.to.width / o.width;
      if (m == "box" || m == "both") {
        if (u != s) c = c.concat(h), b.from = a.effects.setTransition(b, h, u, b.from), b.to = a.effects.setTransition(b, h, s, b.to);
        if (w != D) c = c.concat(j), b.from = a.effects.setTransition(b, j, w, b.from), b.to = a.effects.setTransition(b, j, D, b.to)
      }
      if ((m == "content" || m == "both") && u != s) c = c.concat(g), b.from = a.effects.setTransition(b, g, u, b.from), b.to = a.effects.setTransition(b, g, s, b.to);
      a.effects.save(b, n ? c : d);
      b.show();
      a.effects.createWrapper(b);
      b.css("overflow", "hidden").css(b.from);
      if (m == "content" || m == "both") h = h.concat(["marginTop", "marginBottom"]).concat(g), j = j.concat(["marginLeft", "marginRight"]), f = c.concat(h).concat(j), b.find("*[width]").each(function () {
        child = a(this);
        n && a.effects.save(child, f);
        var b = child.height(),
          c = child.width();
        child.from = {
          height: b * u,
          width: c * w
        };
        child.to = {
          height: b * s,
          width: c * D
        };
        if (u != s) child.from = a.effects.setTransition(child, h, u, child.from), child.to = a.effects.setTransition(child, h, s, child.to);
        if (w != D) child.from = a.effects.setTransition(child, j, w, child.from), child.to = a.effects.setTransition(child, j, D, child.to);
        child.css(child.from);
        child.animate(child.to, e.duration, e.options.easing, function () {
          n && a.effects.restore(child, f)
        })
      });
      b.animate(b.to, {
        queue: false,
        duration: e.duration,
        easing: e.options.easing,
        complete: function () {
          b.to.opacity === 0 && b.css("opacity", b.from.opacity);
          l == "hide" && b.hide();
          a.effects.restore(b, n ? c : d);
          a.effects.removeWrapper(b);
          e.callback && e.callback.apply(this, arguments);
          b.dequeue()
        }
      })
    })
  }
})(jQuery);
(function (a, e) {
  var b;
  a.rails = b = {
    linkClickSelector: "a[data-confirm], a[data-method], a[data-remote], a[data-disable-with]",
    inputChangeSelector: "select[data-remote], input[data-remote], textarea[data-remote]",
    formSubmitSelector: "form",
    formInputClickSelector: "form input[type=submit], form input[type=image], form button[type=submit], form button:not(button[type])",
    disableSelector: "input[data-disable-with], button[data-disable-with], textarea[data-disable-with]",
    enableSelector: "input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled",
    requiredInputSelector: "input[name][required]:not([disabled]),textarea[name][required]:not([disabled])",
    fileInputSelector: "input:file",
    linkDisableSelector: "a[data-disable-with]",
    CSRFProtection: function (b) {
      var d = a('meta[name="csrf-token"]').attr("content");
      d && b.setRequestHeader("X-CSRF-Token", d)
    },
    fire: function (b, d, e) {
      d = a.Event(d);
      b.trigger(d, e);
      return d.result !== false
    },
    confirm: function (a) {
      return confirm(a)
    },
    ajax: function (b) {
      return a.ajax(b)
    },
    handleRemote: function (c) {
      var d, f, g, h = c.data("cross-domain") || null,
        j = c.data("type") || a.ajaxSettings && a.ajaxSettings.dataType;
      if (b.fire(c, "ajax:before")) {
        if (c.is("form")) {
          d = c.attr("method");
          f = c.attr("action");
          g = c.serializeArray();
          var l = c.data("ujs:submit-button");
          l && (g.push(l), c.data("ujs:submit-button", null))
        } else c.is(b.inputChangeSelector) ? (d = c.data("method"), f = c.data("url"), g = c.serialize(), c.data("params") && (g = g + "&" + c.data("params"))) : (d = c.data("method"), f = c.attr("href"), g = c.data("params") || null);
        d = {
          type: d || "GET",
          data: g,
          dataType: j,
          crossDomain: h,
          beforeSend: function (a, d) {
            d.dataType === e && a.setRequestHeader("accept", "*/*;q=0.5, " + d.accepts.script);
            return b.fire(c, "ajax:beforeSend", [a, d])
          },
          success: function (a, b, d) {
            c.trigger("ajax:success", [a, b, d])
          },
          complete: function (a, b) {
            c.trigger("ajax:complete", [a, b])
          },
          error: function (a, b, d) {
            c.trigger("ajax:error", [a, b, d])
          }
        };
        if (f) d.url = f;
        return b.ajax(d)
      } else return false
    },
    handleMethod: function (b) {
      var d = b.attr("href"),
        f = b.data("method"),
        b = b.attr("target"),
        g = a("meta[name=csrf-token]").attr("content"),
        h = a("meta[name=csrf-param]").attr("content"),
        d = a('<form method="post" action="' + d + '"></form>'),
        f = '<input name="_method" value="' + f + '" type="hidden" />';
      h !== e && g !== e && (f += '<input name="' + h + '" value="' + g + '" type="hidden" />');
      b && d.attr("target", b);
      d.hide().append(f).appendTo("body");
      d.submit()
    },
    disableFormElements: function (c) {
      c.find(b.disableSelector).each(function () {
        var b = a(this),
          c = b.is("button") ? "html" : "val";
        b.data("ujs:enable-with", b[c]());
        b[c](b.data("disable-with"));
        b.prop("disabled", true)
      })
    },
    enableFormElements: function (c) {
      c.find(b.enableSelector).each(function () {
        var b = a(this),
          c = b.is("button") ? "html" : "val";
        if (b.data("ujs:enable-with")) b[c](b.data("ujs:enable-with"));
        b.prop("disabled", false)
      })
    },
    allowAction: function (a) {
      var d = a.data("confirm"),
        e = false,
        g;
      if (!d) return true;
      b.fire(a, "confirm") && (e = b.confirm(d), g = b.fire(a, "confirm:complete", [e]));
      return e && g
    },
    blankInputs: function (b, d, e) {
      var g = a(),
        h;
      b.find(d || "input,textarea").each(function () {
        h = a(this);
        if (e ? h.val() : !h.val()) g = g.add(h)
      });
      return g.length ? g : false
    },
    nonBlankInputs: function (a, d) {
      return b.blankInputs(a, d, true)
    },
    stopEverything: function (b) {
      a(b.target).trigger("ujs:everythingStopped");
      b.stopImmediatePropagation();
      return false
    },
    callFormSubmitBindings: function (b, d) {
      var f = b.data("events"),
        g = true;
      f !== e && f.submit !== e && a.each(f.submit, function (a, b) {
        if (typeof b.handler === "function") return g = b.handler(d)
      });
      return g
    },
    disableElement: function (a) {
      a.data("ujs:enable-with", a.html());
      a.html(a.data("disable-with"));
      a.bind("click.railsDisable", function (a) {
        return b.stopEverything(a)
      })
    },
    enableElement: function (a) {
      a.data("ujs:enable-with") !== e && (a.html(a.data("ujs:enable-with")), a.data("ujs:enable-with", false));
      a.unbind("click.railsDisable")
    }
  };
  a.ajaxPrefilter(function (a, d, e) {
    a.crossDomain || b.CSRFProtection(e)
  });
  a(document).delegate(b.linkDisableSelector, "ajax:complete", function () {
    b.enableElement(a(this))
  });
  a(document).delegate(b.linkClickSelector, "click.rails", function (c) {
    var d = a(this),
      f = d.data("method"),
      g = d.data("params");
    if (!b.allowAction(d)) return b.stopEverything(c);
    d.is(b.linkDisableSelector) && b.disableElement(d);
    if (d.data("remote") !== e) {
      if ((c.metaKey || c.ctrlKey) && (!f || f === "GET") && !g) return true;
      b.handleRemote(d) === false && b.enableElement(d);
      return false
    } else if (d.data("method")) return b.handleMethod(d), false
  });
  a(document).delegate(b.inputChangeSelector, "change.rails", function (c) {
    var d = a(this);
    if (!b.allowAction(d)) return b.stopEverything(c);
    b.handleRemote(d);
    return false
  });
  a(document).delegate(b.formSubmitSelector, "submit.rails", function (c) {
    var d = a(this),
      f = d.data("remote") !== e,
      g = b.blankInputs(d, b.requiredInputSelector),
      h = b.nonBlankInputs(d, b.fileInputSelector);
    if (!b.allowAction(d)) return b.stopEverything(c);
    if (g && d.attr("novalidate") == e && b.fire(d, "ajax:aborted:required", [g])) return b.stopEverything(c);
    if (f) {
      if (h) return b.fire(d, "ajax:aborted:file", [h]);
      if (!a.support.submitBubbles && a().jquery < "1.7" && b.callFormSubmitBindings(d, c) === false) return b.stopEverything(c);
      b.handleRemote(d);
      return false
    } else setTimeout(function () {
      b.disableFormElements(d)
    }, 13)
  });
  a(document).delegate(b.formInputClickSelector, "click.rails", function (c) {
    var d = a(this);
    if (!b.allowAction(d)) return b.stopEverything(c);
    c = (c = d.attr("name")) ? {
      name: c,
      value: d.val()
    } : null;
    d.closest("form").data("ujs:submit-button", c)
  });
  a(document).delegate(b.formSubmitSelector, "ajax:beforeSend.rails", function (c) {
    this == c.target && b.disableFormElements(a(this))
  });
  a(document).delegate(b.formSubmitSelector, "ajax:complete.rails", function (c) {
    this == c.target && b.enableFormElements(a(this))
  })
})(jQuery);
$.datepicker.setDefaults({
  buttonImage: "/images/cal.png",
  buttonImageOnly: true,
  changeMonth: true,
  showOn: "both",
  changeYear: true,
  showAnim: false,
  dateFormat: window.jquery_date_format
});
$.expr[":"].econtains = function (a, e, b) {
  return (a.textContent || a.innerText || $(a).text() || "").toLowerCase() == b[3].toLowerCase()
};
_.templateSettings = {
  start: "{{",
  end: "}}",
  interpolate: /\{\{(.+?)\}\}/g
};
window.togglWeb = {
  ui: {},
  model: {}
};

/**



++++++++++++++++++++++++++++++++++



*///






function TimeParser() {
  function a(a, c) {
    this.match = function (c) {
      if (!c) return false;
      var e = c.match(a);
      if (!e) return e;
      e[0] = c;
      return e
    };
    this.interpret = function (a) {
      a = this.match(a);
      return !a ? false : c(a)
    }
  }
  var e = [new a(/^\s*0*(\d{1,10})\s*(:|h)\s*0*(\d{1,10})\s*(:|m)\s*0*(\d{1,10})\s*s?\s*$/, function (a) {
    return 1E3 * a[5] + 6E4 * a[3] + 36E5 * a[1]
  }), new a(/^\s*0*(\d{1,10})\s*(m|mins?|minutes?)?$/, function (a) {
    return 6E4 * a[1]
  }), new a(/^\s*0*(\d{1,10})\s*(h|hrs?|hours?)\s*$/, function (a) {
    return 36E5 * a[1]
  }), new a(/^\s*0*(\d{1,10})\s*(s|secs?|seconds?)\s*$/, function (a) {
    return 1E3 * a[1]
  }), new a(/^\s*0*(\d{1,10})?\s*[.,]\s*(\d{0,9})\s*(h|hrs?|hours?)?$/, function (a) {
    var c = a[2];
    return parseFloat(a[1] || 0) * 36E5 + parseFloat("0." + c) * 36E5
  }), new a(/^\s*0*(\d{1,10})\s*[.,]\s*(\d{0,9})\s*(m|mins?|minutes?)?\s*$/, function (a) {
    var c = a[2];
    return parseFloat(a[1]) * 6E4 + parseFloat("0." + c) * 6E4
  }), new a(/^\s*0*(\d{1,10})\s*[.,]\s*(\d{0,9})\s*(s|secs?|seconds?)?\s*$/, function (a) {
    return parseFloat(a[1] + "." + a[2]) * 1E3
  }), new a(/^\s*0*(\d{1,10})\s*:\s*0*(\d{1,10})\s*mins?\s*$/, function (a) {
    return 6E4 * a[1] + 1E3 * a[2]
  }), new a(/^\s*0*(\d{1,10})\s*(:|h|hrs?|hours?)\s*0*(\d{1,10})\s*(m|mins?|minutes?)?\s*$/, function (a) {
    return 6E4 * a[3] + 36E5 * a[1]
  })];
  this.is_valid = function (a) {
    for (var c = 0; c < e.length; ++c) if (e[c].match(a)) return true;
    return false
  };
  this.interpret = function (a) {
    for (var a = a.toLowerCase(), c = 0; c < e.length; ++c) if (e[c].match(a)) return e[c].interpret(a);
    return null
  }
}
var TIME_PARSERS = new TimeParser,
  hhmmss_to_milliseconds = window.TIME_PARSERS.interpret;

function time_check(a) {
  return function (e) {
    return window.TIME_PARSERS.is_valid(e) ? true : a.replace("$VALUE$", e)
  }
}
(function () {
  var a = Date,
    e = a.prototype;
  a.parseFunctions = {
    count: 0
  };
  a.parseRegexes = [];
  a.formatFunctions = {
    count: 0
  };
  e.dateFormat = function (b, c) {
    a.formatFunctions[b] || a.createNewFormat(b);
    var d = a.formatFunctions[b];
    return c && this.offset ? (new Date(this.valueOf() - this.offset))[d]() : this[d]()
  };
  e.getGMTOffset = function (a) {
    return (this.getTimezoneOffset() > 0 ? "-" : "+") + String.leftPad(Math.floor(Math.abs(this.getTimezoneOffset()) / 60), 2, "0") + (a ? ":" : "") + String.leftPad(Math.abs(this.getTimezoneOffset() % 60), 2, "0")
  };
  a.createNewFormat = function (b) {
    var c = "format" + a.formatFunctions.count++;
    a.formatFunctions[b] = c;
    for (var c = "Date.prototype." + c + " = function(){return ", d = false, e = "", g = 0; g < b.length; ++g) e = b.charAt(g), !d && e == "\\" ? d = true : d ? (d = false, c += "'" + String.escape(e) + "' + ") : c += a.getFormatCode(e);
    eval(c.substring(0, c.length - 3) + ";}")
  };
  a.getFormatCode = function (a) {
    switch (a) {
    case "d":
      return "String.leftPad(this.getDate(), 2, '0') + ";
    case "D":
      return "Date.dayNames[this.getDay()].substring(0, 3) + ";
    case "j":
      return "this.getDate() + ";
    case "l":
      return "Date.dayNames[this.getDay()] + ";
    case "S":
      return "this.getSuffix() + ";
    case "w":
      return "this.getDay() + ";
    case "z":
      return "this.getDayOfYear() + ";
    case "W":
      return "this.getWeekOfYear() + ";
    case "F":
      return "Date.monthNames[this.getMonth()] + ";
    case "m":
      return "String.leftPad(this.getMonth() + 1, 2, '0') + ";
    case "M":
      return "Date.monthNames[this.getMonth()].substring(0, 3) + ";
    case "n":
      return "(this.getMonth() + 1) + ";
    case "t":
      return "this.getDaysInMonth() + ";
    case "L":
      return "(this.isLeapYear() ? 1 : 0) + ";
    case "Y":
      return "this.getFullYear() + ";
    case "y":
      return "('' + this.getFullYear()).substring(2, 4) + ";
    case "a":
      return "(this.getHours() < 12 ? 'am' : 'pm') + ";
    case "A":
      return "(this.getHours() < 12 ? 'AM' : 'PM') + ";
    case "g":
      return "((this.getHours() %12) ? this.getHours() % 12 : 12) + ";
    case "G":
      return "this.getHours() + ";
    case "h":
      return "String.leftPad((this.getHours() %12) ? this.getHours() % 12 : 12, 2, '0') + ";
    case "H":
      return "String.leftPad(this.getHours(), 2, '0') + ";
    case "i":
      return "String.leftPad(this.getMinutes(), 2, '0') + ";
    case "s":
      return "String.leftPad(this.getSeconds(), 2, '0') + ";
    case "O":
      return "this.getGMTOffset(true) + ";
    case "T":
      return "this.getTimezone() + ";
    case "Z":
      return "(this.getTimezoneOffset() * -60) + ";
    default:
      return "'" + String.escape(a) + "' + "
    }
  };
  a.parseDate = function (b, c) {
    a.parseFunctions[c] || a.createParser(c);
    return Date[a.parseFunctions[c]](b)
  };
  a.createParser = function (b) {
    var c = "parse" + a.parseFunctions.count++,
      d = a.parseRegexes.length,
      e = 1,
      g;
    a.parseFunctions[b] = c;
    var c = "Date." + c + " = function(input){\nvar y = -1, m = -1, d = -1, h = -1, i = -1, s = -1, z = 0;\nvar d = new Date();\ny = d.getFullYear();\nm = d.getMonth();\nd = d.getDate();\nvar results = input.match(Date.parseRegexes[" + d + "]);\nif (results && results.length > 0) {",
      h = "",
      j = false;
    g = "";
    for (var l = 0; l < b.length; ++l) g = b.charAt(l), !j && g == "\\" ? j = true : j ? (j = false, h += String.escape(g)) : (g = a.formatCodeToRegex(g, e), e += g.g, h += g.s, g.g && g.c && (c += g.c));
    c += "if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0 && s >= 0)\n{return new Date(y, m, d, h, i, s).setOffset(z);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0)\n{return new Date(y, m, d, h, i).setOffset(z);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0)\n{return new Date(y, m, d, h).setOffset(z);}\nelse if (y > 0 && m >= 0 && d > 0)\n{return new Date(y, m, d).setOffset(z);}\nelse if (y > 0 && m >= 0)\n{return new Date(y, m).setOffset(z);}\nelse if (y > 0)\n{return new Date(y).setOffset(z);}\n}return null;}";
    a.parseRegexes[d] = RegExp("^" + h + "$");
    eval(c)
  };
  a.formatCodeToRegex = function (b, c) {
    switch (b) {
    case "D":
      return {
        g: 0,
        c: null,
        s: "(?:Sun|Mon|Tue|Wed|Thu|Fri|Sat)"
      };
    case "j":
    case "d":
      return {
        g: 1,
        c: "d = parseInt(results[" + c + "], 10);\n",
        s: "(\\d{1,2})"
      };
    case "l":
      return {
        g: 0,
        c: null,
        s: "(?:" + a.dayNames.join("|") + ")"
      };
    case "S":
      return {
        g: 0,
        c: null,
        s: "(?:st|nd|rd|th)"
      };
    case "w":
      return {
        g: 0,
        c: null,
        s: "\\d"
      };
    case "z":
      return {
        g: 0,
        c: null,
        s: "(?:\\d{1,3})"
      };
    case "W":
      return {
        g: 0,
        c: null,
        s: "(?:\\d{2})"
      };
    case "F":
      return {
        g: 1,
        c: "m = parseInt(Date.monthNumbers[results[" + c + "].substring(0, 3)], 10);\n",
        s: "(" + a.monthNames.join("|") + ")"
      };
    case "M":
      return {
        g: 1,
        c: "m = parseInt(Date.monthNumbers[results[" + c + "]], 10);\n",
        s: "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)"
      };
    case "n":
    case "m":
      return {
        g: 1,
        c: "m = parseInt(results[" + c + "], 10) - 1;\n",
        s: "(\\d{1,2})"
      };
    case "t":
      return {
        g: 0,
        c: null,
        s: "\\d{1,2}"
      };
    case "L":
      return {
        g: 0,
        c: null,
        s: "(?:1|0)"
      };
    case "Y":
      return {
        g: 1,
        c: "y = parseInt(results[" + c + "], 10);\n",
        s: "(\\d{4})"
      };
    case "y":
      return {
        g: 1,
        c: "var ty = parseInt(results[" + c + "], 10);\ny = ty > Date.y2kYear ? 1900 + ty : 2000 + ty;\n",
        s: "(\\d{1,2})"
      };
    case "a":
      return {
        g: 1,
        c: "if (results[" + c + "] == 'am') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}",
        s: "(am|pm)"
      };
    case "A":
      return {
        g: 1,
        c: "if (results[" + c + "].replace(/[\\. ]/g, '').toUpperCase() == 'AM') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}",
        s: "( *[aApP]\\.? *[mM]\\.?)"
      };
    case "N":
      return {
        g: 1,
        c: "if (results[" + c + "].replace(/[\\. ]/g, '').toUpperCase() == 'A') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}",
        s: "( *[aApP]\\.?)"
      };
    case "g":
    case "G":
    case "h":
    case "H":
      return {
        g: 1,
        c: "h = parseInt(results[" + c + "], 10);\n",
        s: "(\\d{1,2})"
      };
    case "i":
      return {
        g: 1,
        c: "i = parseInt(results[" + c + "], 10);\n",
        s: "(\\d{2})"
      };
    case "s":
      return {
        g: 1,
        c: "s = parseInt(results[" + c + "], 10);\n",
        s: "(\\d{2})"
      };
    case "O":
    case "P":
      return {
        g: 1,
        c: "z = Date.parseOffset(results[" + c + "], 10);\n",
        s: "(Z|[+-]\\d{2}:?\\d{2})"
      };
    case "T":
      return {
        g: 0,
        c: null,
        s: "[A-Z]{3}"
      };
    case "Z":
      return {
        g: 1,
        c: "s = parseInt(results[" + c + "], 10);\n",
        s: "([+-]\\d{1,5})"
      };
    default:
      return {
        g: 0,
        c: null,
        s: String.escape(b)
      }
    }
  };
  a.parseOffset = function (a) {
    if (a == "Z") return 0;
    var c, a = a.split("");
    c = parseInt(a[0] + a[1] + a[2], 10) * 3600;
    c += a[3] == ":" ? parseInt(a[4] + a[5], 10) * 60 : parseInt(a[3] + a[4], 10) * 60;
    return c
  };
  e.setOffset = function (a) {
    this.setTime(this.getTime() - a * 1E3);
    this.offset = 0;
    this.applyOffset(a);
    return this
  };
  e.applyOffset = function (a) {
    var c = this.offset || 0;
    this.offset = a * 1E3;
    this.setTime(this.valueOf() - c + this.offset);
    return this
  };
  e.getTimezone = function () {
    return this.toString().replace(/^.*? ([A-Z]{3}) [0-9]{4}.*$/, "$1").replace(/^.*?\(([A-Z])[a-z]+ ([A-Z])[a-z]+ ([A-Z])[a-z]+\)$/, "$1$2$3").replace(/^.*?[0-9]{4} \(([A-Z]{3})\)/, "$1")
  };
  e.getDayOfYear = function () {
    var b = 0;
    a.daysInMonth[1] = this.isLeapYear() ? 29 : 28;
    for (var c = 0; c < this.getMonth(); ++c) b += a.daysInMonth[c];
    return b + this.getDate() - 1
  };
  e.getWeekOfYear = function () {
    var a = this.getDayOfYear() + (4 - this.getDay()),
      c = 7 - (new Date(this.getFullYear(), 0, 1)).getDay() + 4;
    return String.leftPad((a - c) / 7 + 1, 2, "0")
  };
  e.isLeapYear = function () {
    var a = this.getFullYear();
    return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0
  };
  e.getFirstDayOfMonth = function () {
    var a = (this.getDay() - (this.getDate() - 1)) % 7;
    return a < 0 ? a + 7 : a
  };
  e.getLastDayOfMonth = function () {
    var b = (this.getDay() + (a.daysInMonth[this.getMonth()] - this.getDate())) % 7;
    return b < 0 ? b + 7 : b
  };
  e.getDaysInMonth = function () {
    a.daysInMonth[1] = this.isLeapYear() ? 29 : 28;
    return a.daysInMonth[this.getMonth()]
  };
  e.getSuffix = function () {
    switch (this.getDate()) {
    case 1:
    case 21:
    case 31:
      return "st";
    case 2:
    case 22:
      return "nd";
    case 3:
    case 23:
      return "rd";
    default:
      return "th"
    }
  };
  String.escape = function (a) {
    return a.replace(/('|\\)/g, "\\$1")
  };
  String.leftPad = function (a, c, d) {
    a = "" + a;
    for (d = d || " "; a.length < c;) a = d + a;
    return a
  };
  a.daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  a.monthNames = "January,February,March,April,May,June,July,August,September,October,November,December".split(",");
  a.dayNames = "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(",");
  a.y2kYear = 50;
  a.monthNumbers = {
    Jan: 0,
    Feb: 1,
    Mar: 2,
    Apr: 3,
    May: 4,
    Jun: 5,
    Jul: 6,
    Aug: 7,
    Sep: 8,
    Oct: 9,
    Nov: 10,
    Dec: 11
  };
  a.patterns = {
    ISO8601LongPattern: "Y\\-m\\-d\\TH\\:i\\:sO",
    ISO8601ShortPattern: "Y\\-m\\-d",
    ShortDatePattern: "n/j/Y",
    LongDatePattern: "l, F d, Y",
    FullDateTimePattern: "l, F d, Y g:i:s A",
    MonthDayPattern: "F d",
    ShortTimePattern: "g:i A",
    LongTimePattern: "g:i:s A",
    SortableDateTimePattern: "Y-m-d\\TH:i:s",
    UniversalSortableDateTimePattern: "Y-m-d H:i:sO",
    YearMonthPattern: "F, Y"
  }
})();
Date.CultureInfo = {
  name: "en-US",
  englishName: "English (United States)",
  nativeName: "English (United States)",
  dayNames: "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),
  abbreviatedDayNames: "Sun,Mon,Tue,Wed,Thu,Fri,Sat".split(","),
  shortestDayNames: "Su,Mo,Tu,We,Th,Fr,Sa".split(","),
  firstLetterDayNames: "S,M,T,W,T,F,S".split(","),
  monthNames: "January,February,March,April,May,June,July,August,September,October,November,December".split(","),
  abbreviatedMonthNames: "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),
  amDesignator: "AM",
  pmDesignator: "PM",
  firstDayOfWeek: 0,
  twoDigitYearMax: 2029,
  dateElementOrder: "mdy",
  formatPatterns: {
    shortDate: "M/d/yyyy",
    longDate: "dddd, MMMM dd, yyyy",
    shortTime: "h:mm tt",
    longTime: "h:mm:ss tt",
    fullDateTime: "dddd, MMMM dd, yyyy h:mm:ss tt",
    sortableDateTime: "yyyy-MM-ddTHH:mm:ss",
    universalSortableDateTime: "yyyy-MM-dd HH:mm:ssZ",
    rfc1123: "ddd, dd MMM yyyy HH:mm:ss GMT",
    monthDay: "MMMM dd",
    yearMonth: "MMMM, yyyy"
  },
  regexPatterns: {
    jan: /^jan(uary)?/i,
    feb: /^feb(ruary)?/i,
    mar: /^mar(ch)?/i,
    apr: /^apr(il)?/i,
    may: /^may/i,
    jun: /^jun(e)?/i,
    jul: /^jul(y)?/i,
    aug: /^aug(ust)?/i,
    sep: /^sep(t(ember)?)?/i,
    oct: /^oct(ober)?/i,
    nov: /^nov(ember)?/i,
    dec: /^dec(ember)?/i,
    sun: /^su(n(day)?)?/i,
    mon: /^mo(n(day)?)?/i,
    tue: /^tu(e(s(day)?)?)?/i,
    wed: /^we(d(nesday)?)?/i,
    thu: /^th(u(r(s(day)?)?)?)?/i,
    fri: /^fr(i(day)?)?/i,
    sat: /^sa(t(urday)?)?/i,
    future: /^next/i,
    past: /^last|past|prev(ious)?/i,
    add: /^(\+|aft(er)?|from|hence)/i,
    subtract: /^(\-|bef(ore)?|ago)/i,
    yesterday: /^yes(terday)?/i,
    today: /^t(od(ay)?)?/i,
    tomorrow: /^tom(orrow)?/i,
    now: /^n(ow)?/i,
    millisecond: /^ms|milli(second)?s?/i,
    second: /^sec(ond)?s?/i,
    minute: /^mn|min(ute)?s?/i,
    hour: /^h(our)?s?/i,
    week: /^w(eek)?s?/i,
    month: /^m(onth)?s?/i,
    day: /^d(ay)?s?/i,
    year: /^y(ear)?s?/i,
    shortMeridian: /^(a|p)/i,
    longMeridian: /^(a\.?m?\.?|p\.?m?\.?)/i,
    timezone: /^((e(s|d)t|c(s|d)t|m(s|d)t|p(s|d)t)|((gmt)?\s*(\+|\-)\s*\d\d\d\d?)|gmt|utc)/i,
    ordinalSuffix: /^\s*(st|nd|rd|th)/i,
    timeContext: /^\s*(\:|a(?!u|p)|p)/i
  },
  timezones: [{
    name: "UTC",
    offset: "-000"
  }, {
    name: "GMT",
    offset: "-000"
  }, {
    name: "EST",
    offset: "-0500"
  }, {
    name: "EDT",
    offset: "-0400"
  }, {
    name: "CST",
    offset: "-0600"
  }, {
    name: "CDT",
    offset: "-0500"
  }, {
    name: "MST",
    offset: "-0700"
  }, {
    name: "MDT",
    offset: "-0600"
  }, {
    name: "PST",
    offset: "-0800"
  }, {
    name: "PDT",
    offset: "-0700"
  }]
};
(function () {
  var a = Date,
    e = a.prototype,
    b = a.CultureInfo,
    c = function (a, b) {
      b || (b = 2);
      return ("000" + a).slice(b * -1)
    };
  e.clearTime = function () {
    this.setHours(0);
    this.setMinutes(0);
    this.setSeconds(0);
    this.setMilliseconds(0);
    return this
  };
  e.setTimeToNow = function () {
    var a = new Date;
    this.setHours(a.getHours());
    this.setMinutes(a.getMinutes());
    this.setSeconds(a.getSeconds());
    this.setMilliseconds(a.getMilliseconds());
    return this
  };
  a.today = function () {
    return (new Date).clearTime()
  };
  a.compare = function (a, b) {
    if (isNaN(a) || isNaN(b)) throw Error(a + " - " + b);
    else if (a instanceof Date && b instanceof Date) return a < b ? -1 : a > b ? 1 : 0;
    else throw new TypeError(a + " - " + b);
  };
  a.equals = function (a, b) {
    return a.compareTo(b) === 0
  };
  a.getDayNumberFromName = function (a) {
    for (var c = b.dayNames, d = b.abbreviatedDayNames, e = b.shortestDayNames, a = a.toLowerCase(), f = 0; f < c.length; f++) if (c[f].toLowerCase() == a || d[f].toLowerCase() == a || e[f].toLowerCase() == a) return f;
    return -1
  };
  a.getMonthNumberFromName = function (a) {
    for (var c = b.monthNames, d = b.abbreviatedMonthNames, a = a.toLowerCase(), e = 0; e < c.length; e++) if (c[e].toLowerCase() == a || d[e].toLowerCase() == a) return e;
    return -1
  };
  a.isLeapYear = function (a) {
    return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0
  };
  a.getDaysInMonth = function (b, c) {
    return [31, a.isLeapYear(b) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][c]
  };
  a.getTimezoneAbbreviation = function (a) {
    for (var c = b.timezones, d = 0; d < c.length; d++) if (c[d].offset === a) return c[d].name;
    return null
  };
  a.getTimezoneOffset = function (a) {
    for (var c = b.timezones, d = 0; d < c.length; d++) if (c[d].name === a.toUpperCase()) return c[d].offset;
    return null
  };
  e.clone = function () {
    return new Date(this.getTime())
  };
  e.compareTo = function (a) {
    return Date.compare(this, a)
  };
  e.equals = function (a) {
    return Date.equals(this, a || new Date)
  };
  e.between = function (a, b) {
    return this.getTime() >= a.getTime() && this.getTime() <= b.getTime()
  };
  e.isAfter = function (a) {
    return this.compareTo(a || new Date) === 1
  };
  e.isBefore = function (a) {
    return this.compareTo(a || new Date) === -1
  };
  e.isToday = function () {
    return this.isSameDay(new Date)
  };
  e.isSameDay = function (a) {
    return this.clone().clearTime().equals(a.clone().clearTime())
  };
  e.addMilliseconds = function (a) {
    this.setMilliseconds(this.getMilliseconds() + a);
    return this
  };
  e.addSeconds = function (a) {
    return this.addMilliseconds(a * 1E3)
  };
  e.addMinutes = function (a) {
    return this.addMilliseconds(a * 6E4)
  };
  e.addHours = function (a) {
    return this.addMilliseconds(a * 36E5)
  };
  e.addDays = function (a) {
    this.setDate(this.getDate() + a);
    return this
  };
  e.addWeeks = function (a) {
    return this.addDays(a * 7)
  };
  e.addMonths = function (b) {
    var c = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + b);
    this.setDate(Math.min(c, a.getDaysInMonth(this.getFullYear(), this.getMonth())));
    return this
  };
  e.addYears = function (a) {
    return this.addMonths(a * 12)
  };
  e.add = function (a) {
    if (typeof a == "number") return this._orient = a, this;
    a.milliseconds && this.addMilliseconds(a.milliseconds);
    a.seconds && this.addSeconds(a.seconds);
    a.minutes && this.addMinutes(a.minutes);
    a.hours && this.addHours(a.hours);
    a.weeks && this.addWeeks(a.weeks);
    a.months && this.addMonths(a.months);
    a.years && this.addYears(a.years);
    a.days && this.addDays(a.days);
    return this
  };
  var d, f, g;
  e.getWeek = function () {
    var a, b, c, e, m;
    d = !d ? this.getFullYear() : d;
    f = !f ? this.getMonth() + 1 : f;
    g = !g ? this.getDate() : g;
    f <= 2 ? (a = d - 1, b = (a / 4 | 0) - (a / 100 | 0) + (a / 400 | 0), c = b - (((a - 1) / 4 | 0) - ((a - 1) / 100 | 0) + ((a - 1) / 400 | 0)), e = 0, m = g - 1 + 31 * (f - 1)) : (a = d, b = (a / 4 | 0) - (a / 100 | 0) + (a / 400 | 0), c = b - (((a - 1) / 4 | 0) - ((a - 1) / 100 | 0) + ((a - 1) / 400 | 0)), e = c + 1, m = g + (153 * (f - 3) + 2) / 5 + 58 + c);
    a = (a + b) % 7;
    e = m + 3 - (m + a - e) % 7 | 0;
    d = f = g = null;
    return e < 0 ? 53 - ((a - c) / 5 | 0) : e > 364 + c ? 1 : (e / 7 | 0) + 1
  };
  e.getISOWeek = function () {
    d = this.getUTCFullYear();
    f = this.getUTCMonth() + 1;
    g = this.getUTCDate();
    return c(this.getWeek())
  };
  e.setWeek = function (a) {
    return this.moveToDayOfWeek(1).addWeeks(a - this.getWeek())
  };
  a._validate = function (a, b, c, d) {
    if (typeof a == "undefined") return false;
    else if (typeof a != "number") throw new TypeError(a + " is not a Number.");
    else if (a < b || a > c) throw new RangeError(a + " is not a valid value for " + d + ".");
    return true
  };
  a.validateMillisecond = function (b) {
    return a._validate(b, 0, 999, "millisecond")
  };
  a.validateSecond = function (b) {
    return a._validate(b, 0, 59, "second")
  };
  a.validateMinute = function (b) {
    return a._validate(b, 0, 59, "minute")
  };
  a.validateHour = function (b) {
    return a._validate(b, 0, 23, "hour")
  };
  a.validateDay = function (b, c, d) {
    return a._validate(b, 1, a.getDaysInMonth(c, d), "day")
  };
  a.validateMonth = function (b) {
    return a._validate(b, 0, 11, "month")
  };
  a.validateYear = function (b) {
    return a._validate(b, 0, 9999, "year")
  };
  e.set = function (b) {
    a.validateMillisecond(b.millisecond) && this.addMilliseconds(b.millisecond - this.getMilliseconds());
    a.validateSecond(b.second) && this.addSeconds(b.second - this.getSeconds());
    a.validateMinute(b.minute) && this.addMinutes(b.minute - this.getMinutes());
    a.validateHour(b.hour) && this.addHours(b.hour - this.getHours());
    a.validateMonth(b.month) && this.addMonths(b.month - this.getMonth());
    a.validateYear(b.year) && this.addYears(b.year - this.getFullYear());
    a.validateDay(b.day, this.getFullYear(), this.getMonth()) && this.addDays(b.day - this.getDate());
    b.timezone && this.setTimezone(b.timezone);
    b.timezoneOffset && this.setTimezoneOffset(b.timezoneOffset);
    b.week && a._validate(b.week, 0, 53, "week") && this.setWeek(b.week);
    return this
  };
  e.moveToFirstDayOfMonth = function () {
    return this.set({
      day: 1
    })
  };
  e.moveToLastDayOfMonth = function () {
    return this.set({
      day: a.getDaysInMonth(this.getFullYear(), this.getMonth())
    })
  };
  e.moveToNthOccurrence = function (a, b) {
    var c = 0;
    if (b > 0) c = b - 1;
    else if (b === -1) return this.moveToLastDayOfMonth(), this.getDay() !== a && this.moveToDayOfWeek(a, -1), this;
    return this.moveToFirstDayOfMonth().addDays(-1).moveToDayOfWeek(a, 1).addWeeks(c)
  };
  e.moveToDayOfWeek = function (a, b) {
    var c = (a - this.getDay() + 7 * (b || 1)) % 7;
    return this.addDays(c === 0 ? c + 7 * (b || 1) : c)
  };
  e.moveToMonth = function (a, b) {
    var c = (a - this.getMonth() + 12 * (b || 1)) % 12;
    return this.addMonths(c === 0 ? c + 12 * (b || 1) : c)
  };
  e.getOrdinalNumber = function () {
    return Math.ceil((this.clone().clearTime() - new Date(this.getFullYear(), 0, 1)) / 864E5) + 1
  };
  e.getTimezone = function () {
    return a.getTimezoneAbbreviation(this.getUTCOffset())
  };
  e.setTimezoneOffset = function (a) {
    var b = this.getTimezoneOffset();
    return this.addMinutes(Number(a) * -6 / 10 - b)
  };
  e.setTimezone = function (b) {
    return this.setTimezoneOffset(a.getTimezoneOffset(b))
  };
  e.hasDaylightSavingTime = function () {
    return Date.today().set({
      month: 0,
      day: 1
    }).getTimezoneOffset() !== Date.today().set({
      month: 6,
      day: 1
    }).getTimezoneOffset()
  };
  e.isDaylightSavingTime = function () {
    return this.hasDaylightSavingTime() && (new Date).getTimezoneOffset() === Date.today().set({
      month: 6,
      day: 1
    }).getTimezoneOffset()
  };
  e.getUTCOffset = function () {
    var a = this.getTimezoneOffset() * -10 / 6;
    return a < 0 ? (a = (a - 1E4).toString(), a.charAt(0) + a.substr(2)) : (a = (a + 1E4).toString(), "+" + a.substr(1))
  };
  e.getElapsed = function (a) {
    return (a || new Date) - this
  };
  if (!e.toISOString) e.toISOString = function () {
    function a(b) {
      return b < 10 ? "0" + b : b
    }
    return '"' + this.getUTCFullYear() + "-" + a(this.getUTCMonth() + 1) + "-" + a(this.getUTCDate()) + "T" + a(this.getUTCHours()) + ":" + a(this.getUTCMinutes()) + ":" + a(this.getUTCSeconds()) + 'Z"'
  };
  e._toString = e.toString;
  e.toString = function (a) {
    var d = this;
    if (a && a.length == 1) {
      var e = b.formatPatterns;
      d.t = d.toString;
      switch (a) {
      case "d":
        return d.t(e.shortDate);
      case "D":
        return d.t(e.longDate);
      case "F":
        return d.t(e.fullDateTime);
      case "m":
        return d.t(e.monthDay);
      case "r":
        return d.t(e.rfc1123);
      case "s":
        return d.t(e.sortableDateTime);
      case "t":
        return d.t(e.shortTime);
      case "T":
        return d.t(e.longTime);
      case "u":
        return d.t(e.universalSortableDateTime);
      case "y":
        return d.t(e.yearMonth)
      }
    }
    var f = function (a) {
        switch (a * 1) {
        case 1:
        case 21:
        case 31:
          return "st";
        case 2:
        case 22:
          return "nd";
        case 3:
        case 23:
          return "rd";
        default:
          return "th"
        }
      };
    return a ? a.replace(/(\\)?(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|S)/g, function (a) {
      if (a.charAt(0) === "\\") return a.replace("\\", "");
      d.h = d.getHours;
      switch (a) {
      case "hh":
        return c(d.h() < 13 ? d.h() === 0 ? 12 : d.h() : d.h() - 12);
      case "h":
        return d.h() < 13 ? d.h() === 0 ? 12 : d.h() : d.h() - 12;
      case "HH":
        return c(d.h());
      case "H":
        return d.h();
      case "mm":
        return c(d.getMinutes());
      case "m":
        return d.getMinutes();
      case "ss":
        return c(d.getSeconds());
      case "s":
        return d.getSeconds();
      case "yyyy":
        return c(d.getFullYear(), 4);
      case "yy":
        return c(d.getFullYear());
      case "dddd":
        return b.dayNames[d.getDay()];
      case "ddd":
        return b.abbreviatedDayNames[d.getDay()];
      case "dd":
        return c(d.getDate());
      case "d":
        return d.getDate();
      case "MMMM":
        return b.monthNames[d.getMonth()];
      case "MMM":
        return b.abbreviatedMonthNames[d.getMonth()];
      case "MM":
        return c(d.getMonth() + 1);
      case "M":
        return d.getMonth() + 1;
      case "t":
        return d.h() < 12 ? b.amDesignator.substring(0, 1) : b.pmDesignator.substring(0, 1);
      case "tt":
        return d.h() < 12 ? b.amDesignator : b.pmDesignator;
      case "S":
        return f(d.getDate());
      default:
        return a
      }
    }) : this._toString()
  }
})();
(function () {
  var a = Date,
    e = a.prototype,
    b = a.CultureInfo,
    c = Number.prototype;
  e._orient = 1;
  e._nth = null;
  e._is = false;
  e._same = false;
  e._isSecond = false;
  c._dateElement = "day";
  e.next = function () {
    this._orient = 1;
    return this
  };
  a.next = function () {
    return a.today().next()
  };
  e.last = e.prev = e.previous = function () {
    this._orient = -1;
    return this
  };
  a.last = a.prev = a.previous = function () {
    return a.today().last()
  };
  e.is = function () {
    this._is = true;
    return this
  };
  e.same = function () {
    this._same = true;
    this._isSecond = false;
    return this
  };
  e.today = function () {
    return this.same().day()
  };
  e.weekday = function () {
    return this._is ? (this._is = false, !this.is().sat() && !this.is().sun()) : false
  };
  e.at = function (b) {
    return typeof b === "string" ? a.parse(this.toString("d") + " " + b) : this.set(b)
  };
  c.fromNow = c.after = function (a) {
    var b = {};
    b[this._dateElement] = this;
    return (!a ? new Date : a.clone()).add(b)
  };
  c.ago = c.before = function (a) {
    var b = {};
    b[this._dateElement] = this * -1;
    return (!a ? new Date : a.clone()).add(b)
  };
  var d = "sunday monday tuesday wednesday thursday friday saturday".split(/\s/),
    f = "january february march april may june july august september october november december".split(/\s/),
    g = "Millisecond Second Minute Hour Day Week Month Year".split(/\s/),
    h = "Milliseconds Seconds Minutes Hours Date Week Month FullYear".split(/\s/),
    j = "final first second third fourth fifth".split(/\s/);
  e.toObject = function () {
    for (var a = {}, b = 0; b < g.length; b++) a[g[b].toLowerCase()] = this["get" + h[b]]();
    return a
  };
  a.fromObject = function (a) {
    a.week = null;
    return Date.today().set(a)
  };
  for (var l = function (b) {
      return function () {
        if (this._is) return this._is = false, this.getDay() == b;
        if (this._nth !== null) {
          this._isSecond && this.addSeconds(this._orient * -1);
          this._isSecond = false;
          var c = this._nth;
          this._nth = null;
          var d = this.clone().moveToLastDayOfMonth();
          this.moveToNthOccurrence(b, c);
          if (this > d) throw new RangeError(a.getDayName(b) + " does not occur " + c + " times in the month of " + a.getMonthName(d.getMonth()) + " " + d.getFullYear() + ".");
          return this
        }
        return this.moveToDayOfWeek(b, this._orient)
      }
    }, n = function (c) {
      return function () {
        var d = a.today(),
          e = c - d.getDay();
        c === 0 && b.firstDayOfWeek === 1 && d.getDay() !== 0 && (e += 7);
        return d.addDays(e)
      }
    }, m = 0; m < d.length; m++) a[d[m].toUpperCase()] = a[d[m].toUpperCase().substring(0, 3)] = m, a[d[m]] = a[d[m].substring(0, 3)] = n(m), e[d[m]] = e[d[m].substring(0, 3)] = l(m);
  d = function (a) {
    return function () {
      return this._is ? (this._is = false, this.getMonth() === a) : this.moveToMonth(a, this._orient)
    }
  };
  l = function (b) {
    return function () {
      return a.today().set({
        month: b,
        day: 1
      })
    }
  };
  for (n = 0; n < f.length; n++) a[f[n].toUpperCase()] = a[f[n].toUpperCase().substring(0, 3)] = n, a[f[n]] = a[f[n].substring(0, 3)] = l(n), e[f[n]] = e[f[n].substring(0, 3)] = d(n);
  d = function (a) {
    return function (b) {
      if (this._isSecond) return this._isSecond = false, this;
      if (this._same) {
        this._same = this._is = false;
        for (var c = this.toObject(), b = (b || new Date).toObject(), d = "", e = a.toLowerCase(), f = g.length - 1; f > -1; f--) {
          d = g[f].toLowerCase();
          if (c[d] != b[d]) return false;
          if (e == d) break
        }
        return true
      }
      a.substring(a.length - 1) != "s" && (a += "s");
      return this["add" + a](this._orient)
    }
  };
  l = function (a) {
    return function () {
      this._dateElement = a;
      return this
    }
  };
  for (n = 0; n < g.length; n++) f = g[n].toLowerCase(), e[f] = e[f + "s"] = d(g[n]), c[f] = c[f + "s"] = l(f);
  e._ss = d("Second");
  c = function (a) {
    return function (b) {
      if (this._same) return this._ss(b);
      if (b || b === 0) return this.moveToNthOccurrence(b, a);
      this._nth = a;
      return a === 2 && (b === void 0 || b === null) ? (this._isSecond = true, this.addSeconds(this._orient)) : this
    }
  };
  for (f = 0; f < j.length; f++) e[j[f]] = f === 0 ? c(-1) : c(f)
})();
(function () {
  Date.Parsing = {
    Exception: function (a) {
      this.message = "Parse error at '" + a.substring(0, 10) + " ...'"
    }
  };
  for (var a = Date.Parsing, e = a.Operators = {
    rtoken: function (b) {
      return function (c) {
        var d = c.match(b);
        if (d) return [d[0], c.substring(d[0].length)];
        else throw new a.Exception(c);
      }
    },
    token: function () {
      return function (a) {
        return e.rtoken(RegExp("^s*" + a + "s*"))(a)
      }
    },
    stoken: function (a) {
      return e.rtoken(RegExp("^" + a))
    },
    until: function (a) {
      return function (b) {
        for (var c = [], d = null; b.length;) {
          try {
            d = a.call(this, b)
          } catch (e) {
            c.push(d[0]);
            b = d[1];
            continue
          }
          break
        }
        return [c, b]
      }
    },
    many: function (a) {
      return function (b) {
        for (var c = [], d = null; b.length;) {
          try {
            d = a.call(this, b)
          } catch (e) {
            break
          }
          c.push(d[0]);
          b = d[1]
        }
        return [c, b]
      }
    },
    optional: function (a) {
      return function (b) {
        var c = null;
        try {
          c = a.call(this, b)
        } catch (d) {
          return [null, b]
        }
        return [c[0], c[1]]
      }
    },
    not: function (b) {
      return function (c) {
        try {
          b.call(this, c)
        } catch (d) {
          return [null, c]
        }
        throw new a.Exception(c);
      }
    },
    ignore: function (a) {
      return a ?
      function (b) {
        var c = null,
          c = a.call(this, b);
        return [null, c[1]]
      } : null
    },
    product: function () {
      for (var a = arguments[0], b = Array.prototype.slice.call(arguments, 1), c = [], d = 0; d < a.length; d++) c.push(e.each(a[d], b));
      return c
    },
    cache: function (b) {
      var c = {},
        d = null;
      return function (e) {
        try {
          d = c[e] = c[e] || b.call(this, e)
        } catch (l) {
          d = c[e] = l
        }
        if (d instanceof a.Exception) throw d;
        else return d
      }
    },
    any: function () {
      var b = arguments;
      return function (c) {
        for (var d = null, e = 0; e < b.length; e++) if (b[e] != null) {
          try {
            d = b[e].call(this, c)
          } catch (l) {
            d = null
          }
          if (d) return d
        }
        throw new a.Exception(c);
      }
    },
    each: function () {
      var b = arguments;
      return function (c) {
        for (var d = [], e = null, l = 0; l < b.length; l++) if (b[l] != null) {
          try {
            e = b[l].call(this, c)
          } catch (n) {
            throw new a.Exception(c);
          }
          d.push(e[0]);
          c = e[1]
        }
        return [d, c]
      }
    },
    all: function () {
      var a = a;
      return a.each(a.optional(arguments))
    },
    sequence: function (b, c, d) {
      c = c || e.rtoken(/^\s*/);
      d = d || null;
      return b.length == 1 ? b[0] : function (e) {
        for (var l = null, n = null, m = [], p = 0; p < b.length; p++) {
          try {
            l = b[p].call(this, e)
          } catch (o) {
            break
          }
          m.push(l[0]);
          try {
            n = c.call(this, l[1])
          } catch (u) {
            n = null;
            break
          }
          e = n[1]
        }
        if (!l) throw new a.Exception(e);
        if (n) throw new a.Exception(n[1]);
        if (d) try {
          l = d.call(this, l[1])
        } catch (w) {
          throw new a.Exception(l[1]);
        }
        return [m, l ? l[1] : e]
      }
    },
    between: function (a, b, c) {
      var c = c || a,
        d = e.each(e.ignore(a), b, e.ignore(c));
      return function (a) {
        a = d.call(this, a);
        return [[a[0][0], r[0][2]], a[1]]
      }
    },
    list: function (a, b, c) {
      b = b || e.rtoken(/^\s*/);
      c = c || null;
      return a instanceof Array ? e.each(e.product(a.slice(0, -1), e.ignore(b)), a.slice(-1), e.ignore(c)) : e.each(e.many(e.each(a, e.ignore(b))), px, e.ignore(c))
    },
    set: function (b, c, d) {
      c = c || e.rtoken(/^\s*/);
      d = d || null;
      return function (j) {
        for (var l = null, n = l = null, m = null, p = [
          [], j], o = false, u = 0; u < b.length; u++) {
          l = n = null;
          o = b.length == 1;
          try {
            l = b[u].call(this, j)
          } catch (w) {
            continue
          }
          m = [
            [l[0]], l[1]
          ];
          if (l[1].length > 0 && !o) try {
            n = c.call(this, l[1])
          } catch (s) {
            o = true
          } else o = true;
          !o && n[1].length === 0 && (o = true);
          if (!o) {
            l = [];
            for (o = 0; o < b.length; o++) u != o && l.push(b[o]);
            l = e.set(l, c).call(this, n[1]);
            l[0].length > 0 && (m[0] = m[0].concat(l[0]), m[1] = l[1])
          }
          m[1].length < p[1].length && (p = m);
          if (p[1].length === 0) break
        }
        if (p[0].length === 0) return p;
        if (d) {
          try {
            n = d.call(this, p[1])
          } catch (D) {
            throw new a.Exception(p[1]);
          }
          p[1] = n[1]
        }
        return p
      }
    },
    forward: function (a, b) {
      return function (c) {
        return a[b].call(this, c)
      }
    },
    replace: function (a, b) {
      return function (c) {
        c = a.call(this, c);
        return [b, c[1]]
      }
    },
    process: function (a, b) {
      return function (c) {
        c = a.call(this, c);
        return [b.call(this, c[0]), c[1]]
      }
    },
    min: function (b, c) {
      return function (d) {
        var e = c.call(this, d);
        if (e[0].length < b) throw new a.Exception(d);
        return e
      }
    }
  }, b = function (a) {
      return function () {
        var b = null,
          c = [];
        arguments.length > 1 ? b = Array.prototype.slice.call(arguments) : arguments[0] instanceof Array && (b = arguments[0]);
        if (b) for (var d = b.shift(); 0 < d.length;) return b.unshift(d[0]), c.push(a.apply(null, b)), b.shift(), c;
        else return a.apply(null, arguments)
      }
    }, c = "optional not ignore cache".split(/\s/), d = 0; d < c.length; d++) e[c[d]] = b(e[c[d]]);
  b = function (a) {
    return function () {
      return arguments[0] instanceof Array ? a.apply(null, arguments[0]) : a.apply(null, arguments)
    }
  };
  c = "each any all".split(/\s/);
  for (d = 0; d < c.length; d++) e[c[d]] = b(e[c[d]])
})();
(function () {
  var a = Date,
    e = a.CultureInfo,
    b = function (a) {
      for (var c = [], d = 0; d < a.length; d++) a[d] instanceof Array ? c = c.concat(b(a[d])) : a[d] && c.push(a[d]);
      return c
    };
  a.Grammar = {};
  a.Translator = {
    hour: function (a) {
      return function () {
        this.hour = Number(a)
      }
    },
    minute: function (a) {
      return function () {
        this.minute = Number(a)
      }
    },
    second: function (a) {
      return function () {
        this.second = Number(a)
      }
    },
    meridian: function (a) {
      return function () {
        this.meridian = a.slice(0, 1).toLowerCase()
      }
    },
    timezone: function (a) {
      return function () {
        var b = a.replace(/[^\d\+\-]/g, "");
        b.length ? this.timezoneOffset = Number(b) : this.timezone = a.toLowerCase()
      }
    },
    day: function (a) {
      var b = a[0];
      return function () {
        this.day = Number(b.match(/\d+/)[0])
      }
    },
    month: function (a) {
      return function () {
        this.month = a.length == 3 ? "jan feb mar apr may jun jul aug sep oct nov dec".indexOf(a) / 4 : Number(a) - 1
      }
    },
    year: function (a) {
      return function () {
        var b = Number(a);
        this.year = a.length > 2 ? b : b + (b + 2E3 < e.twoDigitYearMax ? 2E3 : 1900)
      }
    },
    rday: function (a) {
      return function () {
        switch (a) {
        case "yesterday":
          this.days = -1;
          break;
        case "tomorrow":
          this.days = 1;
          break;
        case "today":
          this.days = 0;
          break;
        case "now":
          this.days = 0, this.now = true
        }
      }
    },
    finishExact: function (b) {
      for (var b = b instanceof Array ? b : [b], c = 0; c < b.length; c++) b[c] && b[c].call(this);
      b = new Date;
      if ((this.hour || this.minute) && !this.month && !this.year && !this.day) this.day = b.getDate();
      if (!this.year) this.year = b.getFullYear();
      if (!this.month && this.month !== 0) this.month = b.getMonth();
      if (!this.day) this.day = 1;
      if (!this.hour) this.hour = 0;
      if (!this.minute) this.minute = 0;
      if (!this.second) this.second = 0;
      if (this.meridian && this.hour) if (this.meridian == "p" && this.hour < 12) this.hour += 12;
      else if (this.meridian == "a" && this.hour == 12) this.hour = 0;
      if (this.day > a.getDaysInMonth(this.year, this.month)) throw new RangeError(this.day + " is not a valid value for days.");
      b = new Date(this.year, this.month, this.day, this.hour, this.minute, this.second);
      this.timezone ? b.set({
        timezone: this.timezone
      }) : this.timezoneOffset && b.set({
        timezoneOffset: this.timezoneOffset
      });
      return b
    },
    finish: function (c) {
      c = c instanceof Array ? b(c) : [c];
      if (c.length === 0) return null;
      for (var d = 0; d < c.length; d++) typeof c[d] == "function" && c[d].call(this);
      c = a.today();
      if (this.now && !this.unit && !this.operator) return new Date;
      else this.now && (c = new Date);
      var d = !! (this.days && this.days !== null || this.orient || this.operator),
        e, f, g;
      g = this.orient == "past" || this.operator == "subtract" ? -1 : 1;
      !this.now && "hour minute second".indexOf(this.unit) != -1 && c.setTimeToNow();
      if ((this.month || this.month === 0) && "year day hour minute second".indexOf(this.unit) != -1) this.value = this.month + 1, this.month = null, d = true;
      if (!d && this.weekday && !this.day && !this.days) {
        e = Date[this.weekday]();
        this.day = e.getDate();
        if (!this.month) this.month = e.getMonth();
        this.year = e.getFullYear()
      }
      if (d && this.weekday && this.unit != "month") this.unit = "day", e = a.getDayNumberFromName(this.weekday) - c.getDay(), f = 7, this.days = e ? (e + g * f) % f : g * f;
      if (this.month && this.unit == "day" && this.operator) this.value = this.month + 1, this.month = null;
      if (this.value != null && this.month != null && this.year != null) this.day = this.value * 1;
      if (this.month && !this.day && this.value && (c.set({
        day: this.value * 1
      }), !d)) this.day = this.value * 1;
      if (!this.month && this.value && this.unit == "month" && !this.now) this.month = this.value, d = true;
      if (d && (this.month || this.month === 0) && this.unit != "year") this.unit = "month", e = this.month - c.getMonth(), f = 12, this.months = e ? (e + g * f) % f : g * f, this.month = null;
      if (!this.unit) this.unit = "day";
      if (!this.value && this.operator && this.operator !== null && this[this.unit + "s"] && this[this.unit + "s"] !== null) this[this.unit + "s"] = this[this.unit + "s"] + (this.operator == "add" ? 1 : -1) + (this.value || 0) * g;
      else if (this[this.unit + "s"] == null || this.operator != null) {
        if (!this.value) this.value = 1;
        this[this.unit + "s"] = this.value * g
      }
      if (this.meridian && this.hour) if (this.meridian == "p" && this.hour < 12) this.hour += 12;
      else if (this.meridian == "a" && this.hour == 12) this.hour = 0;
      if (this.weekday && !this.day && !this.days && (e = Date[this.weekday](), this.day = e.getDate(), e.getMonth() !== c.getMonth())) this.month = e.getMonth();
      if ((this.month || this.month === 0) && !this.day) this.day = 1;
      if (!this.orient && !this.operator && this.unit == "week" && this.value && !this.day && !this.month) return Date.today().setWeek(this.value);
      if (d && this.timezone && this.day && this.days) this.day = this.days;
      return d ? c.add(this) : c.set(this)
    }
  };
  var c = a.Parsing.Operators,
    d = a.Grammar,
    f = a.Translator,
    g;
  d.datePartDelimiter = c.rtoken(/^([\s\-\.\,\/\x27]+)/);
  d.timePartDelimiter = c.stoken(":");
  d.whiteSpace = c.rtoken(/^\s*/);
  d.generalDelimiter = c.rtoken(/^(([\s\,]|at|@|on)+)/);
  var h = {};
  d.ctoken = function (a) {
    var b = h[a];
    if (!b) {
      for (var b = e.regexPatterns, d = a.split(/\s+/), f = [], g = 0; g < d.length; g++) f.push(c.replace(c.rtoken(b[d[g]]), d[g]));
      b = h[a] = c.any.apply(null, f)
    }
    return b
  };
  d.ctoken2 = function (a) {
    return c.rtoken(e.regexPatterns[a])
  };
  d.h = c.cache(c.process(c.rtoken(/^(0[0-9]|1[0-2]|[1-9])/), f.hour));
  d.hh = c.cache(c.process(c.rtoken(/^(0[0-9]|1[0-2])/), f.hour));
  d.H = c.cache(c.process(c.rtoken(/^([0-1][0-9]|2[0-3]|[0-9])/), f.hour));
  d.HH = c.cache(c.process(c.rtoken(/^([0-1][0-9]|2[0-3])/), f.hour));
  d.m = c.cache(c.process(c.rtoken(/^([0-5][0-9]|[0-9])/), f.minute));
  d.mm = c.cache(c.process(c.rtoken(/^[0-5][0-9]/), f.minute));
  d.s = c.cache(c.process(c.rtoken(/^([0-5][0-9]|[0-9])/), f.second));
  d.ss = c.cache(c.process(c.rtoken(/^[0-5][0-9]/), f.second));
  d.hms = c.cache(c.sequence([d.H, d.m, d.s], d.timePartDelimiter));
  d.t = c.cache(c.process(d.ctoken2("shortMeridian"), f.meridian));
  d.tt = c.cache(c.process(d.ctoken2("longMeridian"), f.meridian));
  d.z = c.cache(c.process(c.rtoken(/^((\+|\-)\s*\d\d\d\d)|((\+|\-)\d\d\:?\d\d)/), f.timezone));
  d.zz = c.cache(c.process(c.rtoken(/^((\+|\-)\s*\d\d\d\d)|((\+|\-)\d\d\:?\d\d)/), f.timezone));
  d.zzz = c.cache(c.process(d.ctoken2("timezone"), f.timezone));
  d.timeSuffix = c.each(c.ignore(d.whiteSpace), c.set([d.tt, d.zzz]));
  d.time = c.each(c.optional(c.ignore(c.stoken("T"))), d.hms, d.timeSuffix);
  d.d = c.cache(c.process(c.each(c.rtoken(/^([0-2]\d|3[0-1]|\d)/), c.optional(d.ctoken2("ordinalSuffix"))), f.day));
  d.dd = c.cache(c.process(c.each(c.rtoken(/^([0-2]\d|3[0-1])/), c.optional(d.ctoken2("ordinalSuffix"))), f.day));
  d.ddd = d.dddd = c.cache(c.process(d.ctoken("sun mon tue wed thu fri sat"), function (a) {
    return function () {
      this.weekday = a
    }
  }));
  d.M = c.cache(c.process(c.rtoken(/^(1[0-2]|0\d|\d)/), f.month));
  d.MM = c.cache(c.process(c.rtoken(/^(1[0-2]|0\d)/), f.month));
  d.MMM = d.MMMM = c.cache(c.process(d.ctoken("jan feb mar apr may jun jul aug sep oct nov dec"), f.month));
  d.y = c.cache(c.process(c.rtoken(/^(\d\d?)/), f.year));
  d.yy = c.cache(c.process(c.rtoken(/^(\d\d)/), f.year));
  d.yyy = c.cache(c.process(c.rtoken(/^(\d\d?\d?\d?)/), f.year));
  d.yyyy = c.cache(c.process(c.rtoken(/^(\d\d\d\d)/), f.year));
  g = function () {
    return c.each(c.any.apply(null, arguments), c.not(d.ctoken2("timeContext")))
  };
  d.day = g(d.d, d.dd);
  d.month = g(d.M, d.MMM);
  d.year = g(d.yyyy, d.yy);
  d.orientation = c.process(d.ctoken("past future"), function (a) {
    return function () {
      this.orient = a
    }
  });
  d.operator = c.process(d.ctoken("add subtract"), function (a) {
    return function () {
      this.operator = a
    }
  });
  d.rday = c.process(d.ctoken("yesterday tomorrow today now"), f.rday);
  d.unit = c.process(d.ctoken("second minute hour day week month year"), function (a) {
    return function () {
      this.unit = a
    }
  });
  d.value = c.process(c.rtoken(/^\d\d?(st|nd|rd|th)?/), function (a) {
    return function () {
      this.value = a.replace(/\D/g, "")
    }
  });
  d.expression = c.set([d.rday, d.operator, d.value, d.unit, d.orientation, d.ddd, d.MMM]);
  g = function () {
    return c.set(arguments, d.datePartDelimiter)
  };
  d.mdy = g(d.ddd, d.month, d.day, d.year);
  d.ymd = g(d.ddd, d.year, d.month, d.day);
  d.dmy = g(d.ddd, d.day, d.month, d.year);
  d.date = function (a) {
    return (d[e.dateElementOrder] || d.mdy).call(this, a)
  };
  d.format = c.process(c.many(c.any(c.process(c.rtoken(/^(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?)/), function (b) {
    if (d[b]) return d[b];
    else throw a.Parsing.Exception(b);
  }), c.process(c.rtoken(/^[^dMyhHmstz]+/), function (a) {
    return c.ignore(c.stoken(a))
  }))), function (a) {
    return c.process(c.each.apply(null, a), f.finishExact)
  });
  var j = {};
  d.formats = function (a) {
    if (a instanceof Array) {
      for (var b = [], e = 0; e < a.length; e++) b.push(j[a[e]] = j[a[e]] || d.format(a[e])[0]);
      return c.any.apply(null, b)
    } else return j[a] = j[a] || d.format(a)[0]
  };
  d._formats = d.formats('"yyyy-MM-ddTHH:mm:ssZ";yyyy-MM-ddTHH:mm:ssZ;yyyy-MM-ddTHH:mm:ssz;yyyy-MM-ddTHH:mm:ss;yyyy-MM-ddTHH:mmZ;yyyy-MM-ddTHH:mmz;yyyy-MM-ddTHH:mm;ddd, MMM dd, yyyy H:mm:ss tt;ddd MMM d yyyy HH:mm:ss zzz;MMddyyyy;ddMMyyyy;Mddyyyy;ddMyyyy;Mdyyyy;dMyyyy;yyyy;Mdyy;dMyy;d'.split(";"));
  d._start = c.process(c.set([d.date, d.time, d.expression], d.generalDelimiter, d.whiteSpace), f.finish);
  d.start = function (a) {
    try {
      var b = d._formats.call({}, a);
      if (b[1].length === 0) return b
    } catch (c) {}
    return d._start.call({}, a)
  };
  a._parse = a.parse;
  a.parse = function (b) {
    var c = null;
    if (!b) return null;
    if (b instanceof Date) return b;
    try {
      c = a.Grammar.start.call({}, b.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1"))
    } catch (d) {
      return null
    }
    return c[1].length === 0 ? c[0] : null
  };
  a.getParseFunction = function (b) {
    var c = a.Grammar.formats(b);
    return function (a) {
      var b = null;
      try {
        b = c.call({}, a)
      } catch (d) {
        return null
      }
      return b[1].length === 0 ? b[0] : null
    }
  };
  a.parseExact = function (b, c) {
    return a.getParseFunction(c)(b)
  }
})();
(function () {
  var a = Date._parse;
  Date.parse8601 = function (e) {
    var b = a(e),
      c = 0,
      d;
    if (isNaN(b) && (d = /^(\d{4}|[+\-]\d{6})-(\d{2})-(\d{2})(?:[T ](\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3,}))?)?(?:(Z)|([+\-])(\d{2})(?::?(\d{2}))?))?/.exec(e))) d[8] !== "Z" && (c = +d[10] * 60 + +d[11], d[9] === "+" && (c = 0 - c)), b = Date.UTC(+d[1], +d[2] - 1, +d[3], +d[4], +d[5] + c, +d[6]);
    return new Date(b)
  }
})();

function blank(a) {
  return a === null || "" + a == "" || a === void 0
}
function soft_to_timeofday(a) {
  var e = "h:iA,hA,hN,h:iN,H:i,H".split(",");
  if (blank(a)) return null;
  for (var b, a = a.replace(/ */g, "").toUpperCase(), c = 0; c < e.length; ++c) if (b = Date.parseDate(a, e[c])) return b.applyOffset(0);
  return null
}
function normalize_timeofday(a, e) {
  a = soft_to_timeofday(a);
  if (!a) return e;
  a = a.dateFormat(window.jquery_timeofday_format);
  return !a ? e : a
}
var TimeFormat = {
  seconds_to_hhmmss: function (a, e) {
    var b = ~~a,
      c = String.leftPad;
    b < 0 && (b += (new Date).getTime() / 1E3);
    var d = Math.floor(b / 3600),
      f = Math.floor(b % 3600 / 60),
      b = Math.floor(b % 60);
    if (e) return !d ? f + " min" : [d, "h ", c(f, 2, "0"), " min"].join("");
    if (!d) {
      if (!f) return b + " sec";
      b = c(b, 2, "0");
      f = c(f, 2, "0");
      return f + ":" + b + " min"
    }
    f = c(f, 2, "0");
    b = c(b, 2, "0");
    d = c(d, 2, "0");
    return d + ":" + f + ":" + b
  },
  seconds_to_hhmm: function (a) {
    return Math.floor(a / 3600) + ":" + String.leftPad("" + Math.floor(a % 3600 / 60), 2, "0") + " h"
  },
  milliseconds_to_hhmmss: function (a) {
    var e;
    e = parseInt(a, 10);
    if (isNaN(e)) return "0 sec";
    e < 0 && (e += (new Date).getTime());
    var a = Math.floor(e / 36E5),
      b = Math.floor(e % 36E5 / 6E4);
    e = Math.floor(e % 6E4 / 1E3);
    if (!a) {
      if (!b) return e + " sec";
      e = String.leftPad(e, 2, "0");
      b = String.leftPad(b, 2, "0");
      return b + ":" + e + " min"
    }
    b = String.leftPad(b, 2, "0");
    e = String.leftPad(e, 2, "0");
    a = String.leftPad(a, 2, "0");
    return a + ":" + b + ":" + e
  }
},
  Admin = {
    api_version: function () {
      return "v6"
    },
    milliseconds_to_hhmmss: function (a) {
      return TimeFormat.milliseconds_to_hhmmss(a)
    },
    change_profile: function (a, e) {
      $.ajax({
        url: "/api/v6/workspaces/" + e,
        type: "put",
        data: {
          "workspace[profile]": a
        },
        success: function () {
          window.location.reload()
        }
      })
    },
    toggle_administration_rights: function (a, e) {
      $.ajax({
        url: "/api/v7/workspace_users/" + a,
        type: "put",
        data: {
          "workspace_user[is_admin]": e
        }
      })
    },
    showTooltip: function (a, e, b) {
      $('<div id="tooltip" style="z-index:1">' + b + "</div>").css({
        top: e + 15,
        left: a - 80
      }).appendTo("body")
    },
    show_please_wait: function () {
      $("#small_loading").removeClass("hidden");
      $("#content").addClass("hidden")
    },
    hide_please_wait: function () {
      $("#small_loading").addClass("hidden");
      $("#content").removeClass("hidden")
    },
    init_client_inline: function (a) {
      $("[placeholder]").placeholder();
      Admin.init_autocompletes();
      var e = $("#ac_help"),
        b = $("#client_name"),
        c = e.text(),
        d = _(a).pluck("label"),
        f = function () {
          var a = b.val();
          !a || $.inArray(a, d) !== -1 ? e.addClass("hidden") : e.text(c.replace("%s", a)).removeClass("hidden")
        };
      b.bind({
        focusout: function () {
          $.inArray($("#client_name").val(), d) === -1 && $("#project_client_id").val("");
          f()
        },
        keyup: f,
        autocompleteclose: f
      })
    },
    change_workspace_select_arrow_class: function () {
      if ($("input#toggle-more-workspaces").hasClass("pressed")) {
        $("#workspace-picker-toggle").addClass("active");
        var a = $("#toggle-more-workspaces").width() - 60;
        $("#workspace-picker-toggle.active").css({
          marginLeft: a + 64
        })
      } else $("#workspace-picker-toggle").css({
        marginLeft: -12
      }), $("#workspace-picker-toggle").removeClass("active")
    },
    hide_more_actions: function (a) {
      $("." + a + "-container").each(function () {
        var e = $(this),
          b = e.find("input:button");
        b.length === 0 && (b = e.find("a.button,a.settings-button"));
        b.removeClass("pressed");
        e.find("." + a + "-popup").addClass("hidden")
      })
    },
    toggle_more_actions: function (a, e) {
      var b = $(e),
        c = b.parent().find("." + a + "-popup"),
        d = b.position(),
        f = Math.max(b.width(), c.width()),
        g = -2,
        h = b.hasClass("pressed");
      Admin.hide_more_actions(a);
      h || (c.trigger("beforepopup"), c.hasClass("popup-left") && (g = $.browser.msie && parseInt($.browser.version, 10) < 9 ? g - f - 5 : b.width() - f - 1), f = d.top + b.height() - 2, h = $("#content"), h.height() + h.offset().top < b.offset().top + b.height() + c.height() && (f = b.position().top - c.height() - 7), c.css({
        top: f,
        left: d.left + g
      }).toggleClass("hidden"), b.toggleClass("pressed"));
      return false
    },
    toggle_duration: function (a) {
      var a = $(a),
        e = $(".duration-popup"),
        b = a.position(),
        c = $("#toggle-duration-settings").position().left,
        d = a.hasClass("pressed");
      Admin.hide_more_actions("duration");
      d || (e.trigger("beforepopup"), b = b.top + a.height(), e.css({
        top: b + 1,
        left: c - e.width()
      }).toggleClass("hidden"), a.toggleClass("pressed"));
      return false
    },
    toggle_workspaces: function (a) {
      var a = $(a),
        e = $(".workspaces-popup"),
        b = a.position(),
        c = $("#toggle-more-workspaces").position().left,
        d = $("#toggle-more-workspaces").width(),
        f = $("#workspace-picker-toggle").width(),
        g = a.hasClass("pressed");
      Admin.hide_more_actions("workspaces");
      g || (e.trigger("beforepopup"), b = b.top + a.height() - 7, c = c + d + f + 7 - e.width(), c < 0 ? (d = 0, $("#toggle-more-workspaces").css({
        width: $("#toggle-more-workspaces").width() - c
      })) : d = c, e.css({
        top: b,
        left: d
      }).toggleClass("hidden"), a.toggleClass("pressed"));
      $("#workspace-link-wrapper").css({
        width: $("#toggle-more-workspaces").width()
      });
      return false
    },
    show_error_message: function (a) {
      var e = $("#error-notify"),
        b = $("#content");
      $(".popup-window:visible").length ? $(".popup-window .error-message").html(a).removeClass("hidden") : (e.find(".error-notify-content").html(a), a = b.offset().left + b.width() / 2 - e.width() / 2, e.css({
        left: a
      }).removeClass("hidden"));
      Admin.hide_please_wait()
    },
    show_error: function (a) {
      var e = Translation["Sorry, something went wrong."];
      a && a.status !== 0 && a.status == 500 && (e = Translation["Server error"]);
      Admin.show_error_message(e)
    },
    clear_errors: function () {
      $("#error-notify").addClass("hidden");
      $(".popup-window .error-message").addClass("hidden")
    },
    show_blog_post_message: function () {
      var a = $(".blog-post-info"),
        e = $("#content"),
        e = e.offset().left + e.width() / 2 - a.width() / 2;
      a.css({
        left: e
      }).removeClass("hidden");
      $("a[rel=hide-blog-message]").bind("click", function () {
        var b = $("a[rel=link-to-blog]").attr("href");
        $.post("api/v7/me", {
          user: {
            last_blog_entry: b
          },
          _method: "PUT"
        });
        a.addClass("hidden")
      })
    },
    change_plus_minus_title: function (a, e) {
      a.attr("title", e ? Translation.Collapse : Translation.Expand)
    },
    init_autocompletes: function () {
      $("input[data-autocomplete]").each(function () {
        var a = $(this),
          e = _($.parseJSON(a.attr("data-autocomplete"))).flatten(),
          b = $(a.attr("data-proxy"));
        a.autocomplete("widget").is(".ui-autocomplete") || (a.autocomplete({
          source: e,
          delay: 0,
          minLength: 0,
          select: function (a, d) {
            b.val(d.item.id)
          }
        }), a.bind("change", function () {
          $(this).val() === "" && b.val("")
        }), a.prev(".button").unbind(".ac").bind("click.ac", function () {
          a.autocomplete("widget").is(":visible") ? a.autocomplete("close") : (a.autocomplete("search", ""), a.focus())
        }))
      })
    },
    hide_report_date_range_popup: function () {
      $("#reports-outside, #reports-datepicker").addClass("hidden");
      $("#reports-datepicker-toggle").removeClass("shadow");
      $("body").unbind(".temp")
    },
    start_of_current_week: function () {
      var a = Date.today();
      a.moveToDayOfWeek(window.firstday);
      Date.compare(a, Date.today()) > 0 && a.addWeeks(-1);
      return a
    },
    detect_date_range: function (a, e) {
      var b = Date.today();
      if (e && a.same().day(e)) {
        if (b.same().day(a)) return Translation.today;
        if (b.add(-1).day().same().day(a)) return Translation.yesterday
      }
      var b = Admin.start_of_current_week(),
        c = new Date(b);
      c.add(6).days();
      if (a.same().day(b) && (!e || e.same().day(c))) return Translation["this week"];
      b.addWeeks(-1);
      c.addWeeks(-1);
      if (a.same().day(b) && (!e || e.same().day(c))) return Translation["last week"];
      if (e) {
        b = Date.today().moveToFirstDayOfMonth();
        c = Date.today().moveToLastDayOfMonth();
        if (b.same().day(a) && c.same().day(e)) return Translation["this month"];
        b.add(-1).month();
        c = (new Date(b)).moveToLastDayOfMonth();
        if (b.same().day(a) && c.same().day(e)) return Translation["last month"]
      }
      return null
    },
    parse_report_date_range: function () {
      var a = $(".datepicker[name=start_datetime]").datepicker("getDate"),
        e = $(".datepicker[name=end_datetime]"),
        b = null;
      e.length ? b = e.datepicker("getDate") : (b = new Date(a), b.add(6).days());
      var e = a.dateFormat(window.jquery_date_format),
        c = b.dateFormat(window.jquery_date_format),
        d = e + " &ndash; " + c,
        f = e + " - " + c,
        a = Admin.detect_date_range(a, b);
      $("a.reports-date-toggle").html(a || d).attr("title", f);
      $("input[name=start_datetime]").val(e);
      c && $("input[name=end_datetime]").val(c);
      $("#filters_form").submit()
    },
    initialize_csv_log: function () {
      $("a[rel=csv-latest-sync-log]").bind("click", function () {
        var a = $(".csv-sync-log");
        a.hasClass("hidden") ? (a.removeClass("hidden"), $(this).find(".icon-arrdown").addClass("collapse"), $.ajax({
          type: "GET",
          url: "/integration/csv/workspaces",
          error: function () {
            alert("Failed to load log file. Sorry!")
          },
          success: function (e) {
            a.html(e.join("<br/>"))
          }
        })) : (a.addClass("hidden"), $(this).find(".icon-arrdown").removeClass("collapse"))
      })
    },
    change_workspace: function (a) {
      $.post("/user/update", {
        switch_workspace: true,
        wsid: a
      }, function () {
        window.location.reload()
      })
    }
  };
$(function () {
  $(".workspaces-container > div#workspace-link-wrapper").live("click", function () {
    Admin.toggle_workspaces($(".workspaces-container").find("input"));
    Admin.change_workspace_select_arrow_class()
  });
  $(".actions-container > input:button").live("click", function () {
    Admin.toggle_more_actions("actions", $(this))
  });
  $(".actions-container > a").live("click", function (a) {
    a.preventDefault();
    Admin.toggle_more_actions("actions", $(this))
  });
  $(".actions-container img").bind("click", function () {
    $(this).parent().find("input:button")[0].click()
  });
  $(".duration-container > a").live("click", function (a) {
    a.preventDefault();
    Admin.toggle_duration($(this))
  });
  $(".divide-hours a").live("click", function (a) {
    a.preventDefault();
    $(this).addClass("pushed");
    $(this).attr("rel") === "decimal" ? ($("#filters_form #divide_hours").val("decimal"), $(this).parent().find('a[rel="minutes"]').removeClass("pushed"), $(".listing tbody tr .minutes, .reports-table tr .minutes").addClass("hidden"), $(".listing tbody tr .decimal, .reports-table tr .decimal").removeClass("hidden")) : ($("#filters_form #divide_hours").val("minutes"), $(this).parent().find('a[rel="decimal"]').removeClass("pushed"), $(".listing tbody tr .minutes, .reports-table tr .minutes").removeClass("hidden"), $(".listing tbody tr .decimal, .reports-table tr .decimal").addClass("hidden"))
  });
  var a = $.browser.mozilla ? "click keyup" : "change keyup";
  $(".select-box select").bind(a, function () {
    var a = $(this).find("option:selected").text();
    $(this).next(".select-box-text-area").find(".select-box-content").text(a)
  }).bind("focus", function () {
    $(this).parent().addClass("inputbox-hover")
  }).bind("focusout", function () {
    $(this).parent().removeClass("inputbox-hover")
  });
  $(".select-box select").each(function () {
    $(this).keyup()
  });
  $(document).bind("click", function (a) {
    a = $(a.target);
    !a.hasClass("no-hide") && !a.parent(".actions-container").length && !a.hasClass("nr-of-tags") && Admin.hide_more_actions("actions");
    !a.hasClass("no-hide") && !(a.parent(".duration-container").length > 0 || a.hasClass("duration-settings-img")) && Admin.hide_more_actions("duration");
    if (!a.hasClass("no-hide") && (!a.attr("id") || !$(".workspaces-container").find("#" + a.attr("id")).length)) Admin.hide_more_actions("workspaces"), Admin.change_workspace_select_arrow_class()
  });
  $(document).bind("keypress", function (a) {
    27 == a.keyCode && Admin.hide_more_actions("actions")
  });
  $("a[rel=leave-workspace-link]").bind("click", function () {
    confirm("Are you sure? You will permanently lose access to all projects and tasks in this workspace.") && $.ajax({
      url: $("#leave-workspace-url").val(),
      type: "POST",
      success: function () {
        window.reloader()
      },
      error: function (a) {
        alert(a.responseText)
      }
    });
    return false
  });
  $(".ie6").each(function () {
    $("body").prepend("<div>").load("/generic/ie6warn/")
  });
  $.browser.msie && ($(".overlay").addClass("ie-overlay-background"), $("#subscription_and_billing .plan").addClass("ie-plan").addClass("ie-shadow"), $("#subscription_and_billing #upgrade-form").addClass("ie-shadow"));
  $.datepicker.setDefaults($.datepicker.regional[window.language]);
  $.datepicker.setDefaults({
    dateFormat: window.jquery_datepicker_format,
    firstDay: window.firstday
  });
  $(".link-to-main").live("click", function (a) {
    if (a.metaKey || a.ctrlKey) return true;
    if ($(this).hasClass("confirm") && !confirm($(this).attr("rel"))) return false;
    a = "get";
    Admin.show_please_wait();
    window.ajaxload && window.ajaxload.abort();
    $(this).hasClass("method-delete") && (a = "delete");
    $(this).hasClass("method-put") && (a = "put");
    window.ajaxload = $.ajax({
      url: $(this).attr("href"),
      type: a,
      contentType: "text/html",
      data: "",
      dataType: "html",
      success: function (a) {
        Admin.hide_please_wait();
        $("#content").html(a)
      }
    });
    return false
  });
  $("#create-new-toggle, .create-new-toggle").live("click", function () {
    $("#create-new-box").toggleClass("hidden")
  });
  $("#language-select").bind("change", function () {
    var a = $(this).val();
    if (a.split("_")[0] !== window.language) Admin.show_please_wait(), document.location.href = "/home?language=" + a
  });
  $("#current-workspace-selection").bind("change", function () {
    Admin.change_workspace($("#current-workspace-selection option:selected").val())
  });
  $(".workspaces-popup a").bind("click", function (a) {
    $(this).hasClass("new-workspace") === false && (a.preventDefault(), Admin.change_workspace($(this).attr("data-id")))
  });
  $(".switch-workspace").bind("click", function (a) {
    a.preventDefault();
    Admin.change_workspace($(this).attr("data-id"))
  });
  $("a.rate-toggle").live("click", function () {
    var a = $("#rate_state");
    a.hasClass("custom-rate-visible") ? ($("#client_hourly_rate").val(""), $("#client_currency").val(""), a.removeClass("custom-rate-visible").addClass("default-rate-visible")) : a.addClass("custom-rate-visible").removeClass("default-rate-visible")
  });
  $("a", "#invitations").live("click", function () {
    $(this).closest("div").fadeOut(function () {
      $(this).remove();
      $("#invitations").children().length || $("#invitations").remove()
    })
  });
  $("body").ajaxStart(function () {
    $("#tooltip, .notice.temp").remove();
    $(".ui-autocomplete").each(function () {
      var a = $(this);
      a.autocomplete("widget").is(":visible") && a.autocomplete("close")
    });
    $("*").unbind(".temp")
  });
  $(".reports-date-toggle").live("click", function () {
    $("#reports-outside, #reports-datepicker").toggleClass("hidden");
    $("#reports-datepicker-toggle").toggleClass("shadow");
    $("body").bind("click.temp", function (a) {
      if (!$(a.target).closest("#reports-datepicker, #ui-datepicker-div, .ui-datepicker-header").length || $(a.target).hasClass("reports-date-toggle")) a.stopImmediatePropagation(), Admin.hide_report_date_range_popup()
    })
  });
  $("#datepicker-submit").bind("click", function (a) {
    a.preventDefault();
    Admin.parse_report_date_range()
  });
  $("a[rel=move-timespan-forward], a[rel=move-timespan-backward]").live("click", function () {
    var a = $(".datepicker[name=start_datetime]").datepicker("getDate"),
      b = $(".datepicker[name=end_datetime]"),
      b = b.length ? b.datepicker("getDate") : null,
      c = parseInt($(this).attr("data-direction"), 10),
      d = new Date(a),
      f = null;
    if (b) {
      f = new Date(a);
      f.moveToFirstDayOfMonth();
      var g = new Date(a);
      g.moveToLastDayOfMonth();
      a.same().day(f) && b.same().day(g) ? (d.add(c).month(), f = new Date(d), f.moveToLastDayOfMonth()) : (a = Math.abs(b.getTime() - a.getTime()), a = Math.round(a / 864E5), a++, d.add(c * a).day(), f = new Date(b), f.add(c * a).day())
    } else d.add(c * 7).day();
    $(".datepicker[name=start_datetime]").datepicker("setDate", d);
    f && $(".datepicker[name=end_datetime]").datepicker("setDate", f);
    Admin.parse_report_date_range()
  });
  $("a[rel=report-date-shortcut]").live("click", function () {
    var a = $(this);
    $("a.reports-date-toggle").text(a.attr("data-title")).attr("title", a.attr("data-hint")).attr("data-step", a.attr("data-step") || "");
    $("#page").val("1");
    var b = a.attr("data-startdate"),
      a = a.attr("data-enddate");
    b && a ? ($("#start_datetime").datepicker("setDate", b), $("#end_datetime").datepicker("setDate", a), $(".date-arrows > a").removeClass("hidden")) : $(".date-arrows > a").addClass("hidden");
    $("input[name=start_datetime]").val(b);
    $("input[name=end_datetime]").val(a);
    Admin.hide_report_date_range_popup();
    $("#filters_form").submit()
  });
  $(".ie.lte8 .reports-date-toggle").live("click", function () {
    $("#reports-outside").hasClass("hidden") ? Admin.create_report_charts() : $("#chart01, #chart02").empty()
  });
  $(".ie.lte8 #reports-datepicker *").live("click", function (a) {
    a.stopPropagation()
  });
  $(".plus-minus").live("click", function () {
    var a = $(this),
      b = $(this).closest("tr");
    if (a.parents("body").hasClass("print")) return false;
    b.toggleClass("expanded").nextAll().each(function (a, b) {
      if ($(b).hasClass("major-report-row")) return $(b).toggleClass("last"), false;
      $(b).toggleClass("hidden")
    });
    Admin.change_plus_minus_title(a, b.is(".expanded"))
  });
  $("#weekly-toggle").live("click", function () {
    var a = $(this).closest("tr").hasClass("expanded");
    $.each($(".major-report-row"), function (b, c) {
      $(c).hasClass("expanded") != a && $(c).find(".plus-minus").click()
    })
  });
  $(".dropdown > li.clickable").live("click", function () {
    $(this).siblings().toggleClass("hidden");
    $("body").bind("click.temp", function (a) {
      $(a.target).closest(".dropdown").length || ($(".dropdown > li.clickable").siblings().addClass("hidden"), $("body").unbind(".temp"))
    })
  });
  $("#project_ac").live("change", function () {
    $(this).val() === "" && $("#project_id, #client_id").val("")
  });
  $("#mass-checkbox").live("click", function () {
    this.checked ? $(".mass-checkbox").attr("checked", "checked") : $(".mass-checkbox").removeAttr("checked");
    $(".mass-checkbox").closest("tr").toggleClass("hilite", this.checked)
  });
  $(".mass-checkbox").live("click", function () {
    $(this).closest("tr").toggleClass("hilite", this.checked)
  });
  $("a[rel=projects-archive], a[rel=projects-unarchive]").live("click", function () {
    var a = "projects-unarchive" === $(this).attr("rel"),
      b = $.map($(".mass-checkbox:checked").closest("tr"), function (a) {
        return $(a).attr("id").split("_")[1]
      }),
      c = a ? "/projects/mass_open" : "/projects/mass_close";
    if (!b.length) return false;
    if (a || confirm("Do you want to archive the selected projects? You will not be able to track time in the selected projects until you reopen them.")) return Admin.show_please_wait(), $.post(c, {
      projects: b
    }, function () {
      window.location = window.location
    }), false
  });
  $("a[rel=projects-duplicate]").live("click", function () {
    var a = $.map($(".mass-checkbox:checked"), function (a) {
      return $(a).closest("tr").attr("id").split("_")[1]
    });
    if (!a.length) return false;
    Admin.show_please_wait();
    $("#content").load("/projects", {
      project_ids: a
    }, function () {
      window.reloader()
    });
    return false
  });
  $(".user-remove-link").live("click", function () {
    var a = $(this);
    confirm(a.attr("rel")) && $.ajax({
      url: a.attr("href"),
      type: "POST",
      success: function (b, c, d) {
        d && d.status && 203 === d.status ? window.reloader() : a.closest("tr").fadeOut()
      },
      error: function (a) {
        alert(a.responseText)
      }
    });
    return false
  });
  $("#new-tag").live("click", function () {
    $(this).next().toggleClass("hidden")
  });
  $("#change-password").live("click", function () {
    $(this).parent().next().removeClass("hidden").end().remove()
  });
  $(".plus-minus").attr("title", Translation.Expand);
  $("#cancel-new-client").bind("click", function () {
    $("#notice-error").addClass("hidden")
  });
  $(".toggle-admin-status").bind("click", function () {
    Admin.toggle_administration_rights($(this).attr("rel"), $(this).is(":checked"))
  });
  $("a[rel=close-error-notify]").bind("click", function () {
    $("#error-notify").addClass("hidden")
  });
  $(window).scroll(function () {
    var a = $(window).scrollTop();
    $("#update-notify, #error-notify").css("top", a > 30 ? a : 30)
  });
  $.fn.qtip.styles.shortcuts = {
    width: {
      max: 350
    },
    padding: "14px",
    border: {
      width: 9,
      radius: 3,
      color: "#505050"
    },
    name: "dark"
  };
  $(".shortcuts").qtip({
    content: {
      title: {
        text: "Keyboard shortcuts",
        button: "X"
      },
      text: "s - stop running time entry <br />n - start new time entry<br />m - create new manual time entry <br />c - continue last time entry<br />"
    },
    position: {
      target: $(".shortcuts"),
      corner: "center",
      adjust: {
        screen: true
      }
    },
    show: {
      when: "click",
      solo: true
    },
    hide: {
      when: {
        event: "unfocus"
      }
    },
    style: "shortcuts"
  });
  $(".hover-hilite-table-row").mouseenter(function () {
    $(this).find(".actions-column").removeClass("hidden")
  }).mouseleave(function () {
    $(this).find(".actions-column").addClass("hidden")
  });
  $(".round_sb").length > 0 && $(".round_sb").sb()
});
window.show_please_wait = Admin.show_please_wait;
window.hide_please_wait = Admin.hide_please_wait;
window.reloader = function () {
  window.location.reload()
};
$.fn.qtip.styles.toggl = {
  padding: 0,
  name: "light",
  border: {
    width: 0
  },
  classes: {
    tooltip: "shadow"
  }
};
$(function () {
  function a() {
    $("#vat-message").toggleClass("hidden", !$("#is-eu-resident").prop("checked") || $("#vat_number").val().length !== 0)
  }
  function e(a) {
    var b = $("#provider_adyen"),
      c = b.data("upgrade-profile");
    b.data("payment-months");
    $(".adyen-form, .adyen-payment-form").addClass("hidden");
    $("#paypal_only_logos").addClass("hidden");
    $("#upgrade-form form").addClass("hidden");
    b = $(".payment-action").val();
    b === "change-subscription" ? $("#adyen-form-" + c).removeClass("hidden") : b === "limited-offer" ? $("#adyen-payment-form-limited-offer").removeClass("hidden") : $("#adyen-payment-form-" + a).removeClass("hidden")
  }
  function b() {
    $(".instant-payment-spinner").addClass("hidden");
    $(".make-instant-payment").removeAttr("disabled")
  }
  function c(a) {
    $.ajax({
      url: "/workspace/instant_payment",
      type: "get",
      data: {
        months: a
      },
      success: function () {
        document.location.href = "/workspace/payment_complete"
      },
      error: function () {
        $(".instant-payment-error").text("Payment failed!");
        b()
      }
    })
  }
  function d(a, b, c) {
    var d = $("#upgrade-form #provider_adyen");
    d.data("upgrade-profile", a);
    d.data("payment-months", b);
    $("#paypal-plan").val(a);
    $("#paypal-months").val(b);
    $(".payment-action").val(c);
    $("#upgrade-form").removeClass("payment").removeClass("hidden").prev(".clearer").addClass("hidden");
    $("#provider_adyen").click()
  }
  function f() {
    var a = $("#upgrade-plan .upgrade");
    if (a.data("paypal") !== void 0) {
      var b = a.data("paypal"),
        c = a.data("upgrade-profile");
      $("#paypal-name").val(b.name);
      $("#paypal-price").val(b.price);
      $("#paypal-plan").val(b.id);
      $("#paypal-months").val(null);
      $(".payment-action").val("change-subscription");
      $(".package").removeClass("current");
      a.closest(".package").addClass("current");
      $("#upgrade-form").removeClass("payment").removeClass("hidden").prev(".clearer").addClass("hidden");
      $("#provider_adyen").data("upgrade-profile", c).click()
    }
  }
  function g() {
    $.ajax({
      url: "/workspace/take_limited_offer",
      type: "get",
      success: function () {
        document.location.href = "/workspace/payment_complete";
        b()
      },
      error: function () {
        b();
        $(".instant-payment-error").text("Payment failed")
      }
    })
  }
  $(".upgrade-cancel").live("click", function () {
    $(".package").removeClass("current");
    $(".uparrow").addClass("hidden");
    $("#is-eu-resident").prop("checked", false);
    $("#eu-vat").addClass("hidden");
    $("#upgrade-form").addClass("hidden").prev(".clearer").removeClass("hidden")
  });
  $("#vat_number").live("change keyup", function () {
    a()
  });
  $("#is-eu-resident").live("click", function () {
    $("#eu-vat").toggleClass("hidden", !$(this).is(":checked"));
    a()
  });
  $("#provider_adyen").live("click", function () {
    e($("#payment_period").val())
  });
  $("#provider_paypal").live("click", function () {
    $("#paypal_only_logos").removeClass("hidden");
    $("#upgrade-form form").addClass("hidden");
    $("#upgrade-form").hasClass("payment") ? $("#paypal-payment-form").removeClass("hidden") : $("#paypal-form").removeClass("hidden")
  });
  $("button.payment-button").attr("id", "pay-" + $("select#payment_period").val() + "-months");
  $("select#payment_period").live("change", function () {
    var a = "make-payment-" + $(this).val() + "-months";
    $("button.payment-button").attr("id", a)
  });
  $("#upgrade-plan .upgrade").live("click", function (a) {
    $(this).hasClass("from-free") ? f() : (a.preventDefault(), $(".upgrade-confirmation").removeClass("hidden"))
  });
  $(".payment-button").live("click", function () {
    var a = $("select#payment_period").val(),
      b = $("#price_per_month").val() * a;
    $(this).hasClass("instant") ? (a = $(".instant-payment"), a.find("span.sum").text(b), $(".payment-action").val("pre-payment"), a.removeClass("hidden")) : (b = $("#price_per_month").val() * a, $("#paypal-payment-name").val("Toggl pre-payment"), $("#paypal-payment-price").val(b), $("#upgrade-form").addClass("payment").removeClass("hidden").prev(".clearer").addClass("hidden"), e(a))
  });
  $(".reactivation-payment").live("click", function () {
    d($("#workspace_plan").val(), 1, "reactivate")
  });
  $(".make-instant-payment").live("click", function () {
    $(".instant-payment-spinner").removeClass("hidden");
    $(".make-instant-payment").attr("disabled", "disabled");
    $(".instant-payment-error").text("");
    $(".payment-action").val() == "limited-offer" ? g() : c($("select#payment_period").val())
  });
  $(".cancel-instant-payment, .cancel-upgrade").live("click", function () {
    $(this).parent().parent().addClass("hidden")
  });
  $(".make-upgrade").live("click", function () {
    $(this).parent().parent().addClass("hidden");
    $("#upgrade-plan .upgrade").data("paypal") ? f() : $("#upgrade-plan .upgrade-form").submit()
  });
  $(".limited-payment-button").live("click", function () {
    if ($(this).hasClass("use-recurring-contract")) {
      var a = $(".instant-payment"),
        b = $("#price_per_month").val() * 12 / 2;
      a.find("span.sum").text(b);
      $(".payment-action").val("limited-offer");
      a.removeClass("hidden")
    } else d(13, 12, "limited-offer")
  });
  $('#subscription_and_billing a[rel="change-settings"] ').live("click", function (a) {
    var b = $(this);
    $(".package").removeClass("current");
    b.closest(".package").addClass("current");
    var b = b.data("profile"),
      c = $("#upgrade-form #provider_adyen");
    c.data("upgrade-profile", b);
    c.data("payment-months", null);
    $("#paypal-plan").val(b);
    $("#paypal-months").val(null);
    $(".payment-action").val("change-subscription");
    $("#upgrade-form").removeClass("payment").removeClass("hidden").prev(".clearer").addClass("hidden");
    $("#provider_adyen").click();
    return a.preventDefault()
  });
  $("input[name=months]").bind("change", function () {
    var a = $(this).val();
    $(".payment-button").addClass("hidden");
    $("#make-payment-" + a + "-months").removeClass("hidden")
  });
  $("a.show-history").bind("click", function () {
    $(this).addClass("hidden");
    $(this).parents(".sidebar-list").find(".other-payments").removeClass("hidden")
  });
  $("#upgrade-form form").live("submit", function () {
    $.ajax({
      url: "/subscriptions",
      type: "post",
      async: false,
      data: {
        company_name: $("#company_name").val(),
        company_address: $("#company_address").val(),
        contact_person: $("#contact_person").val(),
        is_eu_resident: $("#is-eu-resident").is(":checked"),
        vat_number: $("#is-eu-resident").is(":checked") ? $("#vat_number").val() : ""
      }
    })
  })
});
Admin.init_project_edit = function (a, e) {
  $("#comment_form").submit(function () {
    show_please_wait();
    $.ajax({
      type: "POST",
      data: $(this).serialize(),
      url: this.action,
      success: function () {
        window.reloader()
      },
      error: function () {
        Admin.show_error_message($("#comment_form").attr("data-error"))
      }
    });
    $("#comment_content").val("");
    return false
  });
  $('a[rel="delete-project"]').bind("click", function () {
    if (confirm("Are you sure you want to delete this project? All the tasks under this project will also be deleted!")) {
      var a = $(this),
        c = a.attr("data-project-id"),
        d = a.attr("data-redirect-on-success");
      $.ajax({
        type: "POST",
        data: {
          _method: "delete"
        },
        url: "/api/" + Admin.api_version() + "/projects/" + c,
        success: function () {
          document.location.href = d
        },
        error: function () {
          Admin.show_error_message(Translation["Failed to delete project."])
        }
      })
    }
    return false
  });
  $("#planned-task-editcell").parent().bind("setdata", function (a, c) {
    a.stopPropagation();
    var d = $(this);
    d.find("#existing_task_estimated_seconds_hhmmss").val(Admin.milliseconds_to_hhmmss(c.estimated_seconds * 1E3));
    d.find("#existing_task_estimated_seconds").val(c.estimated_seconds);
    d.find("#existing_task_name").val(c.name);
    d.find("#existing_task_is_active").attr("checked", c.is_active);
    d.find("#existing_task_user_id").val(c.user ? c.user.id.toString() : "");
    d.find("input").each(function () {
      var a = $(this);
      a.toggleClass("placeholder", a.val() === "")
    })
  });
  $("form", "#comments-editcell").bind("submit", function () {
    var a = $(this);
    a.find("input").each(function () {
      $(this).trigger("clear-placeholders")
    });
    if (a.find("#planned_task_name").val() !== "") {
      var c = a.closest("form").find("#planned_task_estimated_seconds_hhmmss");
      c.length > 0 && Admin.update_hidden_duration_in_seconds(c, c.closest("form").find("#planned_task_estimated_seconds"));
      $.ajaxQueue.post(a.attr("action"), {
        data: a.serializeArray(),
        dataType: "html",
        success: function () {
          window.reloader()
        },
        error: function (a) {
          Admin.show_error(a)
        }
      })
    } else a.find("#planned_task_name").focus();
    return false
  });
  $("a[rel=edit]").click(function () {
    $(this).closest(".edit-state").find(".edit-show").removeClass("hidden").end().find(".edit-hide").addClass("hidden")
  });
  $("a[rel=cancel]").click(function () {
    $(this).closest("tr").addClass("hidden").prev().removeClass("hidden");
    $(this).closest(".edit-state").find(".edit-show").addClass("hidden").end().find(".edit-hide").removeClass("hidden")
  });
  e && ($("#project-estimate-toggle").qtip({
    content: $("#project-estimate"),
    show: {
      when: {
        event: "click"
      }
    },
    hide: {
      when: {
        event: "unfocus"
      }
    },
    style: "toggl",
    api: {
      onRender: function () {
        $("#project-estimate").remove()
      }
    }
  }), $("#project-billable-toggle").qtip({
    content: $("#project-billable"),
    show: {
      when: {
        event: "click"
      }
    },
    hide: {
      when: {
        event: "unfocus"
      }
    },
    style: "toggl",
    api: {
      onRender: function () {
        $("#project-billable").remove()
      }
    }
  }));
  $("#comments-table a[rel=delete]").live("click", function (a) {
    a.stopImmediatePropagation();
    if (confirm($(this).attr("alt"))) {
      var a = $(this).closest("tr"),
        c = a.attr("id").split("_")[1];
      a.fadeOut(function () {
        $(this).remove();
        $("#comments").children().length || $("#no_comments").removeClass("hidden")
      });
      $.ajax({
        url: "/comments/destroy/" + c,
        type: "post",
        contentType: "text/html",
        data: "",
        dataType: "html"
      })
    }
    return false
  });
  $("#comments-table a[rel=edit]").live("click", function () {
    $("#comments-table a[rel=cancel]").click();
    var a = $(this).closest("tr"),
      c = $("#comments-editcell").parent().removeClass("hidden");
    c.find("textarea").val($.trim(a.find("span:last").html()).replace(/<br>/ig, "\n"));
    c.find("form").attr("action", "/comments/update/" + a.attr("id").split("_")[1]);
    a.closest("tr").addClass("hidden").after(c)
  });
  $("#planned-tasks a[rel=edita]").live("click", function () {
    $("#planned-tasks a[rel=cancel]").click();
    var a = $(this).closest("tr"),
      c = $("#planned-task-editcell").parent().removeClass("hidden");
    c.trigger("setdata", a.data("pt"));
    a.closest("tr").addClass("hidden").after(c)
  });
  $(".planned-task button[rel=save]").live("click", function () {
    var a = $(this).parents("tr"),
      c = {
        id: a.attr("id").split("_")[2],
        "pt[name]": a.find("input[name=name]").val(),
        "pt[estimated_workhours]": isNaN(parseInt(a.find("input[name=estimated_workhours]").val(), 10)) ? 0 : parseInt(a.find("input[name=estimated_workhours]").val(), 10),
        "pt[user_id]": isNaN(parseInt(a.find("select[name*=user_id]").val(), 10)) ? "" : parseInt(a.find("select[name*=user_id]").val(), 10)
      };
    show_please_wait();
    $.post("/planned_tasks/update/" + c.id, $.param(c), function (c) {
      a.prev().before($(c));
      a.prev().andSelf().remove();
      hide_please_wait()
    })
  });
  $(".plus-minus").click(function () {
    $(this).parent().prev().find("span").toggleClass("hidden");
    $(this).closest("tr").toggleClass("expanded");
    Admin.change_plus_minus_title($(this), $(this).closest("tr").is(".expanded"))
  });
  $("a[rel=close], #save-estimated-time, #save-billable-data").bind("click", function () {
    $("#project-estimate-toggle, #project-billable-toggle, #team-list a[rel=edit]").qtip("hide")
  });
  $("a[rel=set-hourly-rate]").live("click", function () {
    var a = $(this),
      c = a.closest("span");
    c.find("input[type=text], .rate").removeClass("hidden");
    c.find(".rate").addClass("visible-rate");
    a.remove()
  });
  Admin.init_project_team()
};
Admin.init_progressbars = function () {
  $(".progressbar").each(function () {
    var a = $(this).attr("rel");
    $(this).progressbar({
      value: ~~a
    })
  })
};
Admin.bind_update_notification = function () {
  $("body").bind({
    ajaxSend: function (a, e, b) {
      b.type != "GET" && $("#update-notify").removeClass("hidden")
    },
    ajaxStop: function () {
      $("#update-notify").hasClass("hidden") === false && $("#update-notify").addClass("hidden")
    }
  })
};
Admin.init_planned_task_estimated_time = function () {
  $("#planned_task_estimated_seconds_hhmmss").live("focusout", function () {
    var a = $(this);
    Admin.update_hidden_duration_in_seconds(a, a.closest("form").find("#planned_task_estimated_seconds").first())
  })
};
Admin.add_ajax_links_to_project_users = function () {
  $("#team-list a[rel]").bind("click", function () {
    var a = $(this),
      e = $("#team-list").attr("rel").split("_")[1],
      b = a.closest(".stacked-list").attr("id").split("_")[1],
      c = {
        user_id: b,
        project_id: e,
        _method: "put"
      };
    switch (a.attr("rel")) {
    case "add":
      c = $.extend(c, {
        "user[manager]": false
      });
      break;
    case "edit":
      return $("#user_id").val(b), false;
    case "toggle-admin":
      c = $.extend(c, {
        "user[manager]": !a.find("img[alt^=Star_yellow]").length
      });
      break;
    case "delete":
      break;
    default:
      return false
    }
    $("#project_user_" + b).load("/projects/" + e + "/users/" + b, c, function () {
      Admin.init_project_team()
    })
  })
};
Admin.init_project_team = function () {
  $("#team-add-more").bind("click", function () {
    $("#team-list").removeClass("nonmembers-hidden");
    $("#team-list .nonmember a[rel!=add]").hide();
    $(this).remove()
  });
  $("#project_public").bind("change", function () {
    $(this).nextAll("input").click()
  });
  Admin.add_ajax_links_to_project_users()
};
Admin.update_hidden_duration_in_seconds = function (a, e) {
  var b = hhmmss_to_milliseconds(a.val()) || 0;
  e.val(b / 1E3);
  b = Admin.milliseconds_to_hhmmss(b);
  b === "0 sec" ? a.val("") : a.val(b)
};
Admin.init_new_project_dialog = function (a, e, b, c) {
  var d = new TogglProjectEntry;
  d.initialize({
    id: "#new-project",
    default_workspace_id: c,
    on_submit: function (a) {
      $.ajax({
        url: "/api/" + Admin.api_version() + "/projects",
        data: {
          project: a
        },
        type: "POST",
        success: function () {
          window.reloader()
        },
        error: function (a) {
          Admin.show_error_message($.parseJSON(a.responseText).join(", "))
        }
      })
    }
  });
  $("#new-project").data("clients", a);
  $("#new-project").data("templates", b);
  $("#new-project-workspace").html(_.map(e, function (a) {
    return '<option value="' + a.id + '">' + a.name + "</option>"
  }).join(""));
  $("a[rel=projects-create-project-project]").bind("click", function () {
    d.show_dialog()
  })
};
Admin.init_bookmarking_form = function () {
  $("#bookmark-link").bind("click", function () {
    $(".bookmarking").toggle(300)
  });
  $("form#bookmark-form").live("submit", function (a) {
    $.post($(this).attr("action"), $(this).serialize(), function (a) {
      $(".bookmark-form").toggle(300);
      $(".bookmark-form-url").toggle(300);
      $("#bookmark-form-url").val(a.url).click(function () {
        this.select()
      })
    }, "json");
    a.preventDefault()
  })
};
Admin.init_export_links = function () {
  $("#exports-more-actions a").bind("click", function (a) {
    a.preventDefault();
    a = $(this).attr("rel");
    $("form#filters_form input#export_format").val(a);
    a != "" && (a == "pdf" && ($("form#filters_form input#view").val("print"), $("form#filters_form").attr("action", "/report.pdf")), $("form#filters_form").find("#timestamp").val((new Date).getTime()), $("form#filters_form").submit(), $(".ui-multiselect-checkboxes input").removeAttr("disabled"), $("input#export_format, input#view").val(""), $("form#filters_form").attr("action", "/report"))
  });
  $(window).ready(function () {
    $("input#export_format, input#view").val("");
    $("form#filters_form").attr("action", "/report")
  })
};
Admin.subgrouping_links = function () {
  $(".subgrouping-link").bind("click", function (a) {
    a.preventDefault();
    var a = $(this).attr("subgrouping"),
      e = $(this).attr("calculate");
    a && $("form#filters_form input#subgrouping").val(a);
    e && $("form#filters_form input#calculate").val(e);
    $("form#filters_form").submit()
  })
};
Admin.init_grouping_select = function () {
  $("#grouping_select, #subgrouping_select").bind("change", function () {
    $("#filters_form input#groupfix").val(true);
    $("#filters_form input#grouping").val($("#grouping_select").val());
    $("#filters_form input#subgrouping").val($("#subgrouping_select").val());
    $("#filters_form").submit()
  });
  $("#grouping_select").bind("change", function () {
    $("#grouping_select").val()
  })
};
Admin.toggle_no_tags = function (a) {
  a.prop("checked") ? $("#tag_select").multiselect("widget").find(':checkbox[value!="no-tags"]').each(function () {
    $(this).prop("checked") && (this.click(), $(this).removeAttr("checked"));
    $(this).attr("disabled", "disabled")
  }) : $("#tag_select").multiselect("widget").find(":checkbox").removeAttr("disabled")
};
Admin.init_multiselect_report = function () {
  $("#cost_select").multiselect({
    header: false,
    selectedList: 1,
    multiple: false
  });
  $("#tag_select").multiselect({
    header: false,
    selectedList: 1,
    noneSelectedText: "Tags"
  });
  $("#project_multi_select").multiselect({
    selectedList: 1,
    noneSelectedText: "All projects",
    header: "",
    classes: "opt-groups"
  }).multiselectfilter({
    label: "",
    placeholder: ""
  });
  var a = $("#tag_select").multiselect("widget").find(':checkbox[value="no-tags"]');
  a.bind("click", function () {
    Admin.toggle_no_tags($(this))
  });
  $("#clear-filter").click(function () {
    $("select[multiple=multiple]").multiselect("uncheckAll");
    $("input#description").val("");
    $("#cost_select").multiselect("widget").find(":radio[name=multiselect_cost_select]").filter("[value=both]").attr("checked", "checked").click();
    $("#tag_select").multiselect("widget").find(":checkbox").removeAttr("disabled")
  });
  $("li.opt-group input").bind("click", function () {
    Admin.toggleGroupSelecting($(this))
  });
  $("li.opt-group input:checked").each(function () {
    Admin.toggleGroupSelecting($(this))
  });
  Admin.toggle_no_tags(a);
  $("#filters_form").submit(function () {
    $(".ui-multiselect-checkboxes input").attr("disabled", "disabled");
    $("#project_ids").val(Admin.collectSelectedOptions("#project_multi_select"));
    $("#client_ids").val(Admin.collectClientOptions());
    $("#user_ids").val(Admin.collectSelectedOptions("#user_select"));
    $("#tag_ids").val(Admin.collectSelectedOptions("#tag_select"))
  })
};
Admin.toggleGroupSelecting = function (a) {
  var e = a.parent().parent().nextUntil("li.opt-group");
  a.is(":checked") ? (e.find(":checkbox").removeAttr("checked"), e.find(":checkbox[aria-selected]").click().removeAttr("checked"), e.addClass("hidden")) : e.removeClass("hidden")
};
Admin.collectSelectedOptions = function (a) {
  var e = $(a + " option:selected:not(.opt-group)").map(function () {
    return $(this).val()
  });
  $(a).attr("disabled", "disabled");
  return e.get().join(",")
};
Admin.collectClientOptions = function () {
  return $("#project_multi_select option.opt-group:selected").map(function () {
    return $(this).val()
  }).get().join(",")
};
Admin.report_menu_links = function () {
  $(".report-menu-link").bind("click", function () {
    $("input#grouping").val($(this).attr("rel"));
    $("#filters_form").submit();
    return false
  })
};
Admin.format_time_for_pie_chart = function (a) {
  var e = Math.floor(a.datapoint[1][0][1] / 3600) + "h " + Math.floor(a.datapoint[1][0][1] % 3600 / 60) + "min";
  e += " - " + Math.round(a.datapoint[0]) + "%";
  return e
};
Admin.pie_formatter = function (a, e) {
  return '<div style="font-size:8pt; text-align:center; padding:0px; color:#545454;">' + a.split(" - ").join("<br>") + "<br/>" + Math.round(e.percent) + "%</div>"
};
Admin.pie_chart_settings = function () {
  return {
    series: {
      pie: {
        show: true,
        startAngle: -0.5,
        label: {
          threshold: 0.0010,
          formatter: Admin.pie_formatter
        },
        combine: {
          threshold: 0.01
        }
      }
    },
    legend: {
      show: true,
      container: $("#sidebar-chart-legend"),
      noColumns: 1,
      labelBoxBorderColor: "#eaedf2",
      backgroundColor: "green",
      backgroundOpacity: 0
    },
    grid: {
      hoverable: true
    },
    colors: "#376C99,#36AC62,#ED664A,#EDA84A,#C33D81,#A4DB45,#5042A4,#376C99,#36AC62,#ED664A,#EDA84A,#C33D81,#A4DB45,#5042A4,#376C99,#36AC62,#ED664A,#EDA84A,#C33D81,#A4DB45,#5042A4".split(",")
  }
};
Admin.create_report_charts = function (a) {
  var e = window.date_format.replace(/.?%Y.?/, "");
  Admin.charts_data1.daily || (e = window.date_format.replace(/Y/, "y").replace(/^%d.?/, "").replace(/\/%d/, "").replace(/.?%d$/, ""));
  var e = e.replace(/%/g, ""),
    b = {
      series: {
        bars: {
          show: true,
          lineWidth: 1,
          barWidth: 5E7,
          fill: true,
          fillColor: {
            colors: [{
              opacity: 0.8
            }, {
              opacity: 1
            }]
          },
          align: "center",
          horizontal: false
        }
      },
      xaxis: {
        mode: "time",
        minTickSize: [1, "day"],
        tickLength: 4,
        tickFormatter: function (a) {
          a = new Date(a);
          e == "" && (e = window.date_format.replace(/%/g, ""));
          return (new Date(a.getTime() + a.getTimezoneOffset() * 6E4)).dateFormat(e)
        }
      },
      yaxis: {
        min: 0,
        tickDecimals: 0
      },
      colors: ["#bebebe", "#eda800"],
      grid: {
        hoverable: true,
        borderWidth: 0
      }
    };
  setTimeout(function () {
    try {
      var c = _.map(Admin.charts_data1.rows, function (a) {
        var b = [a],
          c;
        if (c = Admin.charts_data1.daily) a = new Date(a[0]), a = new Date(a.getTime() + a.getTimezoneOffset() * 6E4), c = 6 === a.getDay() || 0 === a.getDay();
        return {
          data: b,
          color: c ? "#eda800" : "#bebebe"
        }
      });
      $.plot("#chart01", c, b);
      !a && Admin.charts_data2 && Admin.charts_data2.length && $.plot("#chart02", Admin.charts_data2, Admin.pie_chart_settings())
    } catch (d) {}
  }, 0)
};
Admin.init_pie_chart_hover = function (a) {
  var e = null;
  a.unbind("plothover").bind("plothover", function (a, c, d) {
    if (d) {
      if (e != d.datapoint) e = d.datapoint, $("#tooltip").remove(), Admin.showTooltip(c.pageX, c.pageY, d.series.label + " - " + Admin.format_time_for_pie_chart(d))
    } else $("#tooltip").remove(), e = null
  })
};
Admin.init_report_charts = function (a, e) {
  Admin.charts_data1 = a;
  Admin.charts_data2 = e;
  $(window).bind("resize.temp", function () {
    if ($.browser.msie && $.browser.version < 8) return false;
    $("#chart01").empty();
    Admin.create_report_charts(true)
  });
  $(document).bind("ready.temp", function () {
    Admin.create_report_charts()
  });
  $("#chart01").bind("ajaxComplete.temp", function () {
    Admin.create_report_charts()
  }).bind("ajaxStart.temp", function () {
    $(window).add(document).unbind(".temp")
  });
  if (!$("body").hasClass("print")) {
    var b = null;
    $("#chart01").bind("plothover", function (a, d, e) {
      if (e) {
        if (b != e.datapoint) {
          b = e.datapoint;
          $("#tooltip").remove();
          var a = new Date(e.datapoint[0]),
            g = Math.floor(e.datapoint[1]),
            e = Math.round((e.datapoint[1] - g) * 60);
          Admin.showTooltip(d.pageX, d.pageY, (new Date(a.getTime() + a.getTimezoneOffset() * 6E4)).dateFormat(window.jquery_date_format) + ", " + g + "h " + e + "min")
        }
      } else $("#tooltip").remove(), b = null
    });
    e !== null && Admin.init_pie_chart_hover($("#chart02"))
  }
};
Admin.init_report_qtip = function () {
  $(".reports-sprite").each(function () {
    var a = $(this).attr("rel");
    $(this).qtip({
      content: a,
      show: "mouseover",
      hide: "mouseout",
      style: {
        name: "cream"
      }
    })
  })
};
Admin.report_selected_task_ids = function () {
  return $.map($(".mass-checkbox:checked").closest("tr"), function (a) {
    return $(a).attr("id").split("_")[1]
  })
};
Admin.report_update_tag_selection = function () {
  var a = [];
  _.each($(".mass-checkbox:checked").closest("tr"), function (e) {
    e = $.map($(e).find(".report-tag"), function (a) {
      return $(a).text()
    });
    a.push(e)
  });
  _.each($("#apply-tags-list .actions-popup-item"), function (e) {
    var e = $(e),
      b = e.find(".tag-name").text(),
      e = e.find(".tag-state"),
      c = 0;
    _.each(a, function (a) {
      _.include(a, b) && (c += 1)
    });
    c && a.length === c ? e.removeClass("some-checked").addClass("all-checked") : c ? e.addClass("some-checked").removeClass("all-checked") : e.removeClass("some-checked").removeClass("all-checked")
  })
};
Admin.submit_new_tag = function (a) {
  var e = null;
  _.each($(".tag-name"), function (b) {
    $(b).text() === a && (e = $(b).parent())
  });
  e ? (e.find(".all-checked").length || e.click(), Tag.hide_new_tag_popup()) : $.ajaxQueue.post("/api/" + Admin.api_version() + "/tags/", {
    data: {
      name: a,
      workspace_id: Admin.current_workspace_id
    },
    success: function (a) {
      var c = Tag.template({
        id: "tag_" + a.data.id,
        name: Tag.$new_tag_name.val(),
        state: ""
      });
      $(".tags").append(c);
      $("#tag_" + a.data.id).bind("click", Admin.tag_click).click();
      $("#apply-tags-list").removeClass("no-tags");
      Tag.hide_new_tag_popup()
    },
    error: function (a) {
      Admin.show_error(a)
    }
  });
  return false
};
Admin.tag_click = function () {
  var a = $(this),
    e = a.find(".tag-state"),
    a = a.find(".tag-name"),
    a = {
      tasks: Admin.report_selected_task_ids(),
      tag_names: [a.text()]
    };
  a.tasks.length ? (e.hasClass("some-checked") ? (a.add_tags = true, e.removeClass("some-checked").addClass("all-checked")) : e.hasClass("all-checked") ? (a.remove_tags = true, e.removeClass("some-checked").removeClass("all-checked")) : (a.add_tags = true, e.addClass("all-checked")), $.post("/tasks/update", a, function (a) {
    _.each(a.tasks, function (c) {
      var c = $("#task_" + c),
        d = c.data("instance");
      _.each(a.tags, function (c) {
        if (a.add_tags && !_.include(d.data.tag_names, c)) d.data.tag_names.push(c);
        else if (a.remove_tags && _.include(d.data.tag_names, c)) d.data.tag_names = _.reject(d.data.tag_names, function (a) {
          return a === c
        })
      });
      c.data("instance", d);
      d.update_ui()
    })
  })) : Admin.hide_more_actions()
};
Admin.init_report_actions = function () {
  $("a[rel=add-new-tag]").bind("click", function () {
    Tag.show_new_tag_popup({
      top: $(".toggle-apply-tags").offset().top - 60,
      submit: Admin.submit_new_tag
    })
  });
  $("a[rel=cancel-new-tag]").bind("click", Tag.hide_new_tag_popup);
  $("#apply-tags-list .actions-popup-item").bind("click", Admin.tag_click);
  $("#apply-tags-list").bind("beforepopup", Admin.report_update_tag_selection);
  $("#tasks-delete").live("click", function () {
    var a = $.map($(".mass-checkbox:checked").closest("tr"), function (a) {
      return $(a).attr("id").split("_")[1]
    });
    if (a.length && (Admin.hide_more_actions(), confirm(1 === a.length ? "Delete 1 time entry? This action cannot be reversed." : "Delete " + a.length + " time entries? This action cannot be reversed."))) show_please_wait(), $.ajaxQueue.post("/api/" + Admin.api_version() + "/bulk_updates/", {
      data: {
        name: "delete",
        ids: a
      },
      success: function () {
        for (var e = 0; e < a.length; e++) {
          var b = $("#task_" + a[e]);
          b.next().next().remove();
          b.next().remove();
          b.remove()
        }
        hide_please_wait()
      },
      error: function () {
        hide_please_wait()
      }
    });
    return false
  });
  $("#tasks-mark-billable, #tasks-mark-not-billable").live("click", function () {
    var a = Admin.report_selected_task_ids();
    a.length && (show_please_wait(), $(this).attr("id"), $.ajaxQueue.post("/api/" + Admin.api_version() + "/bulk_updates/", {
      data: {
        name: "tasks-mark-billable" === $(this).attr("id") ? "mark_as_billable" : "mark_as_nonbillable",
        ids: a
      },
      success: function () {
        window.reloader()
      },
      error: function () {
        hide_please_wait()
      }
    }))
  })
};
Admin.request_params = function () {
  for (var a = {}, e = window.location.search.substring(1).split("&"), b = 0; b < e.length; b++) {
    var c = e[b].split("=");
    c[0] && (a[c[0]] = decodeURIComponent(c[1]))
  }
  return a
};
Admin.select_options_from_params = function (a) {
  var a = $(a),
    e = a.attr("name").replace("[]", ""),
    b = (Admin.request_params()[e] || "").split(",");
  $("option:selected", a).not(".opt-group").removeAttr("selected");
  $("option", a).not(".opt-group").filter(function () {
    return $.inArray($(this).val(), b) > -1
  }).each(function () {
    $(this).attr("selected", "selected")
  });
  a.multiselect && a.multiselect("refresh")
};
Admin.select_optgroups_from_params = function (a, e) {
  var b = $(a),
    c = (Admin.request_params()[e] || "").split(",");
  $("option.opt-group:selected", b).removeAttr("selected");
  $("option.opt-group", b).filter(function () {
    return $.inArray($(this).val(), c) > -1
  }).each(function () {
    $(this).attr("selected", "selected")
  });
  b.multiselect && b.multiselect("refresh")
};
Admin.set_value_from_params = function (a) {
  var a = $(a),
    e;
  a && (e = Admin.request_params()[a.attr("name").replace("[]", "")], e !== void 0 && a.val(e))
};
(function () {
  window.Integration = function (a, e, b) {
    var c;
    c = {};
    c.$el = a;
    c.integration_name = e;
    c.todolists_enabled = b;
    c.$sync_failed = a.find(".sync-failed");
    c.$sync_success = a.find(".sync-success");
    c.$account_name = a.find("#" + e + "-workspace-account-name");
    c.$token = a.find("#" + e + "-workspace-token");
    c.$export_time_entries = a.find("#" + e + "-workspace-export-time-entries");
    c.$sync_daily = a.find("#" + e + "-workspace-sync-daily");
    c.$sync_time = a.find("input.integration-sync-time");
    c.$import_todolists = a.find("#" + e + "-workspace-import-todolists");
    c.$edit_integration_workspace = a.find("a.settings-link");
    c.$integration_workspace_name = a.find(".integration-workspace-name");
    c.$error_description = a.find(".error-description");
    c.$todolists = a.find("p.todolists");
    c.$integration_workspace_id = a.find("input[name=integration-workspace-id]");
    c.$latest_sync_log = a.find("a[rel=latest-sync-log]");
    c.$integration_sync_status = a.find(".integration-sync-status");
    c.$start_sync = a.find("a.start-sync");
    c.$sync_at = a.find(".sync-at");
    c.$sync_spinner = a.find(".sync-spinner");
    c.$remove_settings_btn = a.find("a.deactivate");
    c.$sync_log = a.find(".sync-log");
    c.$automatic_sync = a.find(".automatic-sync");
    c.$manual_sync = a.find(".manual-sync");
    c.initialize = function () {
      c.todolists_enabled && c.$todolists.removeClass("hidden");
      c.$edit_integration_workspace.bind("click", function () {
        c.$sync_time.val() || c.$sync_time.val(c.$el.find("input[name=default-sync-time]").val());
        return c.switch_to_edit_mode()
      });
      c.$start_sync.live("click", function (a) {
        a.preventDefault();
        return $(this).hasClass("hidden") === false && $(this).hasClass("disabled") === false ? (c.display_sync_in_progress(), c.$sync_log.addClass("hidden"), c.$sync_log.html(Translation["Loading..."]), $.ajax({
          url: "/integration/" + c.integration_name + "/jobs",
          success: function () {
            return c.poll_job_status()
          }
        })) : false
      });
      c.$el.find("a[rel=cancel-form], .icon-close").bind("click", function () {
        var a;
        return c.$account_name.val().length > 0 ? (a = c.$el.data("integration-workspace") || {}, c.load(a), c.switch_to_view_mode()) : (c.$el.addClass("hidden"), $('.connect-integration a[rel="' + c.integration_name + '"]').parent().removeClass("hidden"))
      });
      c.$el.find("a[rel=latest-sync-log]").bind("click", function () {
        return c.$sync_log.hasClass("hidden") ? (c.$sync_log.removeClass("hidden"), $(this).find(".icon-arrdown").addClass("collapse"), $.ajax({
          type: "GET",
          url: "/integration/" + c.integration_name + "/workspaces",
          error: function () {
            return alert("Failed to load log file. Sorry!")
          },
          success: function (a) {
            return c.$sync_log.html(a.join("<br/>"))
          }
        })) : (c.$sync_log.addClass("hidden"), $(this).find(".icon-arrdown").removeClass("collapse"))
      });
      c.$remove_settings_btn.bind("click", function (a) {
        a.preventDefault();
        if (confirm(Translation["Are you sure you want clear all the settings of this integration?"])) return a = c.$integration_workspace_id.val(), a = "/integration/" + c.integration_name + "/workspaces/" + a, c.$sync_log.addClass("hidden"), $.ajax({
          url: a,
          data: {
            _method: "DELETE"
          },
          success: function () {
            c.load({}, true);
            return c.switch_to_view_mode()
          },
          error: function () {
            return alert("Removal of integration failed. Sorry!")
          }
        }), c.$el.addClass("hidden"), $('.connect-integration a[rel="' + c.integration_name + '"]').parent().removeClass("hidden"), false
      });
      return c.$el.find("form").bind("submit", function (a) {
        var b;
        b = c.$integration_workspace_id.val();
        a.preventDefault();
        a = "/integration/" + c.integration_name + "/workspaces/";
        b != null && b.length > 0 && (a += b);
        return $.ajax({
          url: a,
          data: {
            _method: b != null && b.length > 0 ? "PUT" : "POST",
            account_name: c.$account_name.val(),
            token: c.$token.val(),
            sync_time: c.$sync_time.val(),
            sync_daily: c.$sync_daily.is(":checked"),
            import_todolists: c.$import_todolists.is(":checked"),
            export_time_entries: c.$export_time_entries.is(":checked")
          },
          success: function (a) {
            c.load(a, true);
            c.switch_to_view_mode();
            c.poll_job_status();
            return c.$start_sync.removeClass("disabled")
          },
          error: function (a) {
            return a != null && a.status === 404 ? (c.load({}, true), c.switch_to_view_mode()) : a != null && a.status === 409 ? (alert("Integration has been set up meanwhile. Click OK to reload page."), document.location.reload()) : c.$el.find(".error-messages").removeClass("hidden")
          }
        })
      })
    };
    c.format_sync_details = function (a) {
      return a.sync_daily && a.sync_time ? Translation["Autosync - daily at"] + " " + a.sync_time : ""
    };
    c.formatPendingMessage = function (a) {
      return a.pending_time_entry_count === 1 ? Translation["1 time entry ready for export."] : a.pending_time_entry_count > 1 ? a.pending_time_entry_count + " " + Translation["time entries ready for export."] : ""
    };
    c.display_synchronized_at = function (a) {
      c.$sync_at.text(a.synchronized_at);
      a.synchronization_error ? (c.$sync_failed.removeClass("hidden"), c.$sync_success.addClass("hidden"), c.$error_description.text(a.synchronization_error)) : (c.$sync_failed.addClass("hidden"), c.$sync_success.removeClass("hidden"));
      return c.$latest_sync_log.toggleClass("hidden", a.log_size <= 0)
    };
    c.load = function (a, b) {
      var e;
      c.$integration_workspace_id.val(a.id);
      a.id && a.account_name && c.$integration_workspace_name.text(Translation["Integration is configured"] + ": " + a.account_name);
      !a.id && c.$el.data("integration-workspace") && (c.$integration_workspace_name.text(Translation["Integration settings have been cleared!"]), c.$start_sync.addClass("hidden"), c.$sync_failed.addClass("hidden"), c.$sync_success.addClass("hidden"), c.$latest_sync_log.addClass("hidden"));
      c.$el.find(".daily-sync-info").text(c.format_sync_details(a));
      e = c.$el.find(".pending-info");
      a.export_time_entries ? (a.pending_time_entry_count != null && e.text(c.formatPendingMessage(a)), e.removeClass("hidden")) : e.addClass("hidden");
      c.$account_name.val(a.account_name);
      c.$token.val(a.token);
      c.$export_time_entries.attr("checked", !! a.export_time_entries);
      c.$sync_daily.attr("checked", !! a.sync_daily);
      c.switch_sync_mode( !! a.sync_daily);
      c.$sync_time.val(a.sync_time);
      c.$import_todolists.attr("checked", !! a.import_todolists);
      b && c.$el.data("integration-workspace", a);
      if (a.synchronized_at) return c.display_synchronized_at(a)
    };
    c.display_sync_in_progress = function () {
      c.$latest_sync_log.addClass("hidden");
      c.$start_sync.addClass("hidden");
      c.$sync_spinner.removeClass("hidden");
      c.$edit_integration_workspace.addClass("hidden");
      c.$sync_failed.addClass("hidden");
      return c.$sync_success.addClass("hidden")
    };
    c.display_sync_finished = function () {
      c.$start_sync.removeClass("hidden");
      c.$sync_spinner.addClass("hidden");
      return c.$edit_integration_workspace.removeClass("hidden")
    };
    c.poll_job_status = function () {
      return $.ajax({
        url: "/integration/" + c.integration_name + "/jobs",
        type: "GET",
        complete: function (a) {
          return a.status === 202 ? (c.display_sync_in_progress(), setTimeout(c.integration_name + "_integration.poll_job_status()", 5E3)) : (c.display_sync_finished(), c.load($.parseJSON(a.responseText)))
        }
      })
    };
    c.switch_sync_mode = function (a) {
      return a ? (c.$manual_sync.addClass("hidden"), c.$automatic_sync.removeClass("hidden")) : (c.$automatic_sync.addClass("hidden"), c.$manual_sync.removeClass("hidden"))
    };
    c.switch_to_view_mode = function () {
      c.$el.find(".icon-close").hide();
      c.$el.find(".settings-link").show();
      c.$el.find(".integration-form").slideUp("normal");
      c.$el.find("footer").slideDown("fast");
      return false
    };
    c.switch_to_edit_mode = function () {
      c.$el.find(".integration-form").slideDown("normal");
      c.$el.find("footer").slideUp("fast");
      c.$edit_integration_workspace.hide();
      c.$el.find("header").find(".icon-close").show();
      c.$el.find(".error-messages").addClass("hidden");
      c.$remove_settings_btn.toggleClass("hidden", !c.$integration_workspace_id.val());
      return false
    };
    return c
  }
}).call(this);
(function () {
  if (window.trackgoogle) {
    window._gaq = window._gaq || [];
    _gaq.push(["_setAccount", "UA-3215787-2"]);
    _gaq.push(["_trackPageview"]);
    var a = document.createElement("script");
    a.type = "text/javascript";
    a.async = true;
    a.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
    var e = document.getElementsByTagName("script")[0];
    e.parentNode.insertBefore(a, e)
  }
})();
var tasktimer_report = {
  max_hours: function () {
    var a = 8;
    tasktimer_report.data_in_hours && tasktimer_report.data_in_hours.length && _.each(tasktimer_report.data_in_hours, function (e) {
      a = Math.max(a, Math.ceil(e[1]))
    });
    return a
  },
  hours_to_ticks: function (a) {
    for (var e = [], b = 0; b <= a; b++) 0 === b % 2 && e.push(b);
    return e
  },
  create_bar_chart: function () {
    var a = tasktimer_report.max_hours(),
      e = tasktimer_report.hours_to_ticks(a),
      b = {
        series: {
          bars: {
            show: true,
            lineWidth: 2,
            barWidth: 5E7,
            fill: true,
            fillColor: "#5da9f3",
            align: "center",
            horizontal: false
          }
        },
        xaxis: {
          mode: "time",
          minTickSize: [1, "day"],
          timeformat: "",
          tickLength: 0
        },
        yaxis: {
          min: 0,
          max: a,
          ticks: e,
          tickDecimals: 0
        },
        colors: ["#5da9f3"],
        grid: {
          hoverable: true,
          borderWidth: 0
        }
      };
    setTimeout(function () {
      try {
        $("#sidebar-chart-legend").addClass("hidden"), $("#reports-sidebar").removeClass("pie").addClass("bar"), $.plot("#sidebar-chart-placeholder", [tasktimer_report.data_in_hours], b), $("#hours-this-week").removeClass("hidden"), $("a.sidebar-chart.bar-chart").addClass("active"), $("a.sidebar-chart.pie-chart").removeClass("active")
      } catch (a) {}
    }, 0)
  },
  show_tooltip: function (a, e, b) {
    $('<div id="tooltip" style="z-index:100">' + b + "</div>").css({
      top: e + 15,
      left: a - 80
    }).appendTo("body")
  },
  sum: function () {
    var a = 0;
    _.each(tasktimer_report.data || [], function (e) {
      a += e[1]
    });
    return a
  },
  init_pie_chart: function (a) {
    setTimeout(function () {
      try {
        $("#reports-sidebar").removeClass("bar").addClass("pie");
        var e = 0;
        _.each(a, function (a) {
          e += a.data
        });
        $("#hours-this-week-value").text(TimeFormat.seconds_to_hhmm(e, true));
        var b = Admin.pie_chart_settings();
        b.legend = {
          show: true,
          container: $("#sidebar-chart-legend"),
          noColumns: 1,
          labelBoxBorderColor: "#eaedf2",
          backgroundColor: "green",
          backgroundOpacity: 0
        };
        if (!a.length) a = [{
          data: 1,
          label: Translation["No time tracked."]
        }], b.colors = ["#b6b9b5"];
        $.plot("#sidebar-chart-placeholder", a, b);
        $("#sidebar-chart-legend").removeClass("hidden");
        $("#hours-this-week").removeClass("hidden");
        $("a.sidebar-chart.bar-chart").removeClass("active");
        $("a.sidebar-chart.pie-chart").addClass("active")
      } catch (c) {}
    }, 0);
    Admin.init_pie_chart_hover($("#sidebar-chart-placeholder"))
  },
  init_bar_chart: function (a) {
    tasktimer_report.data = a;
    tasktimer_report.data_in_hours = _.map(tasktimer_report.data || [], function (a) {
      return [a[0], a[1] / 3600]
    });
    $("#hours-this-week-value").text(TimeFormat.seconds_to_hhmm(tasktimer_report.sum(), true));
    tasktimer_report.create_bar_chart();
    var e = null;
    $("#sidebar-chart-placeholder").unbind("plothover").bind("plothover", function (a, c, d) {
      if (d) {
        if (e != d.datapoint) {
          e = d.datapoint;
          $("#tooltip").remove();
          var a = new Date(d.datapoint[0]),
            f = Math.floor(d.datapoint[1]),
            d = Math.round((d.datapoint[1] - f) * 60),
            d = (new Date(a.getTime() + a.getTimezoneOffset() * 6E4)).dateFormat(window.jquery_date_format) + ", " + f + "h " + d + "min";
          tasktimer_report.show_tooltip(c.pageX, c.pageY, d)
        }
      } else $("#tooltip").remove(), e = null
    })
  },
  reload_chart: function () {
    $.ajaxQueue.get("/api/" + Admin.api_version() + "/reports", {
      global: false,
      success: function (a) {
        a && a.data && ("bar" == a.data.chart_type ? tasktimer_report.init_bar_chart(a.data.rows) : tasktimer_report.init_pie_chart(a.data.rows))
      }
    })
  }
};
$(function () {
  $(window).bind("update_tasktimer_reports", function () {
    tasktimer_report.reload_chart()
  });
  $("a[rel=toggle-sidebar-chart]").bind("click", function () {
    var a = $(this);
    $.post("/user/update", {
      _method: "PUT",
      sidebar_piechart: a.hasClass("pie-chart")
    }, function () {
      tasktimer_report.reload_chart()
    })
  })
});
var Aes = {};
Aes.Cipher = function (a, e) {
  for (var b = e.length / 4 - 1, c = [
    [],
    [],
    [],
    []
  ], d = 0; d < 16; d++) c[d % 4][Math.floor(d / 4)] = a[d];
  c = Aes.AddRoundKey(c, e, 0, 4);
  for (d = 1; d < b; d++) c = Aes.SubBytes(c, 4), c = Aes.ShiftRows(c, 4), c = Aes.MixColumns(c, 4), c = Aes.AddRoundKey(c, e, d, 4);
  c = Aes.SubBytes(c, 4);
  c = Aes.ShiftRows(c, 4);
  c = Aes.AddRoundKey(c, e, b, 4);
  b = Array(16);
  for (d = 0; d < 16; d++) b[d] = c[d % 4][Math.floor(d / 4)];
  return b
};
Aes.KeyExpansion = function (a) {
  for (var e = a.length / 4, b = e + 6, c = Array(4 * (b + 1)), d = Array(4), f = 0; f < e; f++) c[f] = [a[4 * f], a[4 * f + 1], a[4 * f + 2], a[4 * f + 3]];
  for (f = e; f < 4 * (b + 1); f++) {
    c[f] = Array(4);
    for (a = 0; a < 4; a++) d[a] = c[f - 1][a];
    if (f % e == 0) {
      d = Aes.SubWord(Aes.RotWord(d));
      for (a = 0; a < 4; a++) d[a] ^= Aes.Rcon[f / e][a]
    } else e > 6 && f % e == 4 && (d = Aes.SubWord(d));
    for (a = 0; a < 4; a++) c[f][a] = c[f - e][a] ^ d[a]
  }
  return c
};
Aes.SubBytes = function (a, e) {
  for (var b = 0; b < 4; b++) for (var c = 0; c < e; c++) a[b][c] = Aes.Sbox[a[b][c]];
  return a
};
Aes.ShiftRows = function (a, e) {
  for (var b = Array(4), c = 1; c < 4; c++) {
    for (var d = 0; d < 4; d++) b[d] = a[c][(d + c) % e];
    for (d = 0; d < 4; d++) a[c][d] = b[d]
  }
  return a
};
Aes.MixColumns = function (a) {
  for (var e = 0; e < 4; e++) {
    for (var b = Array(4), c = Array(4), d = 0; d < 4; d++) b[d] = a[d][e], c[d] = a[d][e] & 128 ? a[d][e] << 1 ^ 283 : a[d][e] << 1;
    a[0][e] = c[0] ^ b[1] ^ c[1] ^ b[2] ^ b[3];
    a[1][e] = b[0] ^ c[1] ^ b[2] ^ c[2] ^ b[3];
    a[2][e] = b[0] ^ b[1] ^ c[2] ^ b[3] ^ c[3];
    a[3][e] = b[0] ^ c[0] ^ b[1] ^ b[2] ^ c[3]
  }
  return a
};
Aes.AddRoundKey = function (a, e, b, c) {
  for (var d = 0; d < 4; d++) for (var f = 0; f < c; f++) a[d][f] ^= e[b * 4 + f][d];
  return a
};
Aes.SubWord = function (a) {
  for (var e = 0; e < 4; e++) a[e] = Aes.Sbox[a[e]];
  return a
};
Aes.RotWord = function (a) {
  for (var e = a[0], b = 0; b < 3; b++) a[b] = a[b + 1];
  a[3] = e;
  return a
};
Aes.Sbox = [99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118, 202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192, 183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21, 4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117, 9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132, 83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207, 208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168, 81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210, 205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115, 96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219, 224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121, 231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8, 186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138, 112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158, 225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223, 140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22];
Aes.Rcon = [
  [0, 0, 0, 0],
  [1, 0, 0, 0],
  [2, 0, 0, 0],
  [4, 0, 0, 0],
  [8, 0, 0, 0],
  [16, 0, 0, 0],
  [32, 0, 0, 0],
  [64, 0, 0, 0],
  [128, 0, 0, 0],
  [27, 0, 0, 0],
  [54, 0, 0, 0]
];
var AesCtr = {
  encrypt: function (a, e, b) {
    if (!(b == 128 || b == 192 || b == 256)) return "";
    for (var a = Utf8.encode(a), e = Utf8.encode(e), c = b / 8, d = Array(c), b = 0; b < c; b++) d[b] = isNaN(e.charCodeAt(b)) ? 0 : e.charCodeAt(b);
    for (var d = Aes.Cipher(d, Aes.KeyExpansion(d)), d = d.concat(d.slice(0, c - 16)), e = Array(16), b = (new Date).getTime(), c = Math.floor(b / 1E3), f = b % 1E3, b = 0; b < 4; b++) e[b] = c >>> b * 8 & 255;
    for (b = 0; b < 4; b++) e[b + 4] = f & 255;
    c = "";
    for (b = 0; b < 8; b++) c += String.fromCharCode(e[b]);
    for (var d = Aes.KeyExpansion(d), f = Math.ceil(a.length / 16), g = Array(f), h = 0; h < f; h++) {
      for (b = 0; b < 4; b++) e[15 - b] = h >>> b * 8 & 255;
      for (b = 0; b < 4; b++) e[15 - b - 4] = h / 4294967296 >>> b * 8;
      for (var j = Aes.Cipher(e, d), l = h < f - 1 ? 16 : (a.length - 1) % 16 + 1, n = Array(l), b = 0; b < l; b++) n[b] = j[b] ^ a.charCodeAt(h * 16 + b), n[b] = String.fromCharCode(n[b]);
      g[h] = n.join("")
    }
    a = c + g.join("");
    return a = Base64.encode(a)
  },
  decrypt: function (a, e, b) {
    if (!(b == 128 || b == 192 || b == 256)) return "";
    for (var a = Base64.decode(a), e = Utf8.encode(e), c = b / 8, d = Array(c), b = 0; b < c; b++) d[b] = isNaN(e.charCodeAt(b)) ? 0 : e.charCodeAt(b);
    d = Aes.Cipher(d, Aes.KeyExpansion(d));
    d = d.concat(d.slice(0, c - 16));
    e = Array(8);
    ctrTxt = a.slice(0, 8);
    for (b = 0; b < 8; b++) e[b] = ctrTxt.charCodeAt(b);
    for (var c = Aes.KeyExpansion(d), d = Math.ceil((a.length - 8) / 16), b = Array(d), f = 0; f < d; f++) b[f] = a.slice(8 + f * 16, f * 16 + 24);
    for (var a = b, g = Array(a.length), f = 0; f < d; f++) {
      for (b = 0; b < 4; b++) e[15 - b] = f >>> b * 8 & 255;
      for (b = 0; b < 4; b++) e[15 - b - 4] = (f + 1) / 4294967296 - 1 >>> b * 8 & 255;
      for (var h = Aes.Cipher(e, c), j = Array(a[f].length), b = 0; b < a[f].length; b++) j[b] = h[b] ^ a[f].charCodeAt(b), j[b] = String.fromCharCode(j[b]);
      g[f] = j.join("")
    }
    a = g.join("");
    return a = Utf8.decode(a)
  }
},
  Base64 = {
    code: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function (a, e) {
      var b, c, d, f, g = [],
        h = "",
        j, l, n = Base64.code;
      l = (typeof e == "undefined" ? 0 : e) ? a.encodeUTF8() : a;
      j = l.length % 3;
      if (j > 0) for (; j++ < 3;) h += "=", l += "\x00";
      for (j = 0; j < l.length; j += 3) b = l.charCodeAt(j), c = l.charCodeAt(j + 1), d = l.charCodeAt(j + 2), f = b << 16 | c << 8 | d, b = f >> 18 & 63, c = f >> 12 & 63, d = f >> 6 & 63, f &= 63, g[j / 3] = n.charAt(b) + n.charAt(c) + n.charAt(d) + n.charAt(f);
      g = g.join("");
      return g = g.slice(0, g.length - h.length) + h
    },
    decode: function (a, e) {
      var e = typeof e == "undefined" ? false : e,
        b, c, d, f, g, h = [],
        j, l = Base64.code;
      j = e ? a.decodeUTF8() : a;
      for (var n = 0; n < j.length; n += 4) b = l.indexOf(j.charAt(n)), c = l.indexOf(j.charAt(n + 1)), f = l.indexOf(j.charAt(n + 2)), g = l.indexOf(j.charAt(n + 3)), d = b << 18 | c << 12 | f << 6 | g, b = d >>> 16 & 255, c = d >>> 8 & 255, d &= 255, h[n / 4] = String.fromCharCode(b, c, d), g == 64 && (h[n / 4] = String.fromCharCode(b, c)), f == 64 && (h[n / 4] = String.fromCharCode(b));
      f = h.join("");
      return e ? f.decodeUTF8() : f
    }
  },
  Utf8 = {
    encode: function (a) {
      a = a.replace(/[\u0080-\u07ff]/g, function (a) {
        a = a.charCodeAt(0);
        return String.fromCharCode(192 | a >> 6, 128 | a & 63)
      });
      return a = a.replace(/[\u0800-\uffff]/g, function (a) {
        a = a.charCodeAt(0);
        return String.fromCharCode(224 | a >> 12, 128 | a >> 6 & 63, 128 | a & 63)
      })
    },
    decode: function (a) {
      a = a.replace(/[\u00c0-\u00df][\u0080-\u00bf]/g, function (a) {
        a = (a.charCodeAt(0) & 31) << 6 | a.charCodeAt(1) & 63;
        return String.fromCharCode(a)
      });
      return a = a.replace(/[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g, function (a) {
        a = (a.charCodeAt(0) & 15) << 12 | (a.charCodeAt(1) & 63) << 6 | a.charCodeAt(2) & 63;
        return String.fromCharCode(a)
      })
    }
  };

function Task() {
  function a() {
    var a = j.data.duration;
    return a < 0 ? (new Date).getTime() / 1E3 + a : a
  }
  function e() {
    return j.data.description ? j.data.description : Translation["(no description)"]
  }
  function b() {
    return j.data.project ? j.data.project.name + ": " + e() : e()
  }
  function c(a) {
    return "tasktimer" === l.ui.name ? l.ui.is_shown_task(a) : a === l.shown_task
  }
  function d(a) {
    if (a.id && j.data.id !== a.id) {
      l.store("tasks", $.grep(l.store("tasks") || [], function (a) {
        return a.id !== j.data.id
      }));
      var b = j.data.id;
      j.data.id = a.id;
      j.data.workspace = a.workspace;
      l.replace_task_id(b, j.data.id);
      n.attr("id", "task_" + a.id);
      j.blocksave && delete j.blocksave
    }
    if (a.project && j.data.project && j.data.project.id !== a.project.id) $.inArray(parseInt(j.data.project.id, 10), _.pluck(l.projects_data, "id")) === -1 && (l.projects_data.push($.extend(a.project, {
      workspace: a.workspace
    })), l.store("projects", l.projects_data), $(document.body).trigger("update-ac")), j.data.project = a.project, j.data.workspace = a.workspace, j.is_tracking() && window.parentSandboxBridge && window.parentSandboxBridge.change_current_project_id && window.parentSandboxBridge.change_current_project_id(j.data.project.id);
    l.store_current_data();
    if (c(j)) {
      $("#task-planned").toggleClass("hidden", !j.data.planned_task);
      var b = Date.parse8601(a.start),
        d = Date.parse8601(a.stop);
      l.ui.update_task_time(b, d, j);
      l.ui.show_tags(j.data.workspace, a.tag_names);
      l.update_task_amount(a.hourly_rate, a.currency, j)
    }
    l.refreshing || j.update_ui()
  }
  function f(a) {
    try {
      if (a && a.status === 404 && "Requested object not found" === a.responseText) {
        l.refresh_required = true;
        return
      }
    } catch (b) {
      l.offline = true;
      return
    }
    j.blocksave && delete j.blocksave;
    l.refreshing && ($.ajaxQueue.reset(), l.revert());
    if (a && a.status === 400) {
      var c = $.parseJSON(a.responseText),
        a = $("#error-notify"),
        d = $("#content");
      $(".popup-window:visible").length ? $(".popup-window .error-message").html(c).removeClass("hidden") : (a.find(".error-notify-content").html(c), c = d.offset().left + d.width() / 2 - a.width() / 2, a.css({
        left: c
      }).removeClass("hidden"))
    }
  }
  function g() {
    $("#error-notify").addClass("hidden");
    $(".popup-window .error-message").addClass("hidden");
    var a = j.data.updated,
      b = j.data.id;
    $.ajaxQueue[b > 0 ? "put" : "post"](b > 0 ? "/api/" + l.api_version + "/time_entries/" + b : "/api/" + l.api_version + "/time_entries/", {
      data: function () {
        var a = {
          time_entry: {
            duration: j.data.duration,
            description: j.data.description || "",
            project: j.data.project,
            billable: j.data.billable,
            start: j.data.start,
            stop: j.data.stop,
            tag_names: j.data.tag_names || [],
            task: j.data.planned_task,
            workspace: j.data.workspace || {
              id: l.logged_in_user.default_workspace_id
            },
            ignore_start_and_stop: j.data.ignore_start_and_stop
          }
        };
        if (l.last_known_data) a.related_data_updated_at = l.last_known_data;
        if (b < 0) a.time_entry.created_with = l.ui.name;
        return a
      },
      success: function (b) {
        b ? (d(b.data), j.data.updated === a && (delete j.data.updated, l.store_current_data()), l.ui.task_saved_success && l.ui.task_saved_success()) : l.offline = true
      },
      error: f
    })
  }
  function h() {
    l.refreshing || (l.tracked_task && j.data.id === l.tracked_task.data.id && (l.ui.stop_tracking(), delete l.tracked_task), n.remove(), l.ui.update_task_list())
  }
  if (arguments.length < 2) {
    if (!arguments[0]) throw "I need a timer to run in";
    throw "I need a timer and data to run in";
  }
  var j = this,
    l = arguments[0],
    n = $([]),
    m = $([]),
    p = $([]),
    o = $([]),
    u = $([]);
  this.destroy_without_request = function () {
    j.data.deleted = true;
    n.hide();
    l.tracked_task && j.data.id === l.tracked_task.data.id && (l.ui.stop_tracking(), delete l.tracked_task);
    n.remove()
  };
  this.destroy = function () {
    window.parentSandboxBridge && j.is_tracking() && (window.parentSandboxBridge.clear_tooltip && window.parentSandboxBridge.clear_tooltip(), window.parentSandboxBridge.set_current_project_id && window.parentSandboxBridge.set_current_project_id(null));
    j.data.deleted = true;
    l.get_online() ? (n.hide(), $.ajaxQueue.del("/api/" + l.api_version + "/time_entries/" + j.data.id, {
      dataType: "text",
      data: function () {
        return {
          related_data_updated_at: l.last_known_data
        }
      },
      error: function (a, b) {
        a && a.status === 404 && "Requested object not found" === a.responseText && (h(), l.store_current_data());
        if (b === "timeout") l.offline = true, l.store_current_data(), h()
      },
      success: function (a, b, c) {
        c.status !== 200 && c.status !== 203 ? (l.offline = true, l.store_current_data(), h()) : (h(), l.store_current_data());
        l.ui.task_saved_success && l.ui.task_saved_success()
      }
    })) : (l.store_current_data(), h())
  };
  this.save = function () {
    j.data.project && null === j.data.project.id && delete j.data.project.id;
    j.data.updated = new Date;
    l.store_current_data();
    if (!j.blocksave) if (l.get_online()) {
      clearTimeout(j.saving);
      if (j.data.id < 0 || !j.data.id) j.blocksave = true;
      g()
    } else c(this) && l.ui.show_tags(j.data.workspace, j.data.tag_names)
  };
  this.export_time = function () {
    var b = TimeFormat.seconds_to_hhmmss(a());
    u.text(b);
    return b
  };
  this.update_ui = function () {
    var a = e();
    !n.length && !j.blocksave ? (n = l.ui.create_task_fragment(j, a), m = n.find(".project-name"), p = n.find(".task-desc"), u = n.find("span.duration"), o = n.find("span.billable")) : l.ui.update_task_fragment ? l.ui.update_task_fragment(n, j, a) : (m.text(j.data.project ? j.data.project.client_project_name : ""), p.attr("title", a), p.text(a), o.text(j.data.billable ? "$" : ""));
    j === l.tracked_task && j.is_tracking() && (n.addClass("running-task"), window.parentSandboxBridge && window.parentSandboxBridge.set_tooltip && window.parentSandboxBridge.set_tooltip(b()));
    j.export_time();
    j.data.deleted || l.ui.add_task_to_list(j, n)
  };
  this.stop_time = function () {
    if (j.is_tracking()) {
      var a = new Date;
      j.data.duration += a.getTime() / 1E3;
      j.data.stop = a.dateFormat(Date.patterns.ISO8601LongPattern);
      l.ui.update_task_time(Date.parse8601(j.data.start), Date.parse8601(j.data.stop), j);
      j.save();
      l.ui.stop_tracking();
      n.removeClass("running-task");
      window.parentSandboxBridge && (window.parentSandboxBridge.clear_tooltip && window.parentSandboxBridge.clear_tooltip(), window.parentSandboxBridge.set_current_project_id && window.parentSandboxBridge.set_current_project_id(null))
    }
  };
  this.start_time = function (a) {
    var c = new Date;
    l.tracked_task && j !== l.tracked_task && l.tracked_task.stop_time();
    l.tracked_task = j;
    if (!j.data.duration) j.data.duration = 0;
    j.is_tracking() || (j.data.duration -= c.getTime() / 1E3);
    j.data.stop = c.dateFormat(Date.patterns.ISO8601LongPattern);
    l.ui.update_task_time(Date.parse8601(j.data.start), Date.parse8601(j.data.stop), j);
    true === a && j.save();
    l.ui.start_tracking();
    n.addClass("running-task");
    window.parentSandboxBridge && (window.parentSandboxBridge.set_tooltip && window.parentSandboxBridge.set_tooltip(b()), window.parentSandboxBridge.set_current_project_id && window.parentSandboxBridge.set_current_project_id((j.data.project || {
      id: null
    }).id))
  };
  this.import_data = function (a) {
    if (c(j)) j.data = $.extend(j.data, a), j.update_ui(), j.save()
  };
  this.started_today = function () {
    var a = new Date,
      b = Date.parse8601(j.data.start);
    return a.getDate() === b.getDate() && a.getMonth() === b.getMonth() && a.getFullYear() === b.getFullYear()
  };
  (function (a) {
    j.data = $.extend(true, {}, a.data);
    if (!j.data.stop) j.data.stop = j.data.start;
    n = $("#task_" + j.data.id);
    n.length && (m = n.find("div.project-name"), p = n.find("strong.title"), u = n.find("span.duration"));
    true === a.start_now && j.start_time(a.save_now)
  })(arguments[1]);
  return this
}
Task.prototype = {
  is_tracking: function () {
    return this.data.duration < 0
  },
  toggle_time: function () {
    this.is_tracking() ? this.stop_time() : this.start_time(true);
    return this.is_tracking()
  },
  go_idle: function (a) {
    if (this.is_tracking()) this.blocksave = true, this.idle_start = this.idle_start || a
  },
  set_duration: function (a) {
    a = hhmmss_to_milliseconds(a) / 1E3;
    this.data.duration = this.is_tracking() ? a - (new Date).getTime() / 1E3 : a;
    this.save()
  }
};
$.ajaxSetup({
  type: "post",
  cache: false,
  timeout: 15E3,
  dataType: "json"
});

function Timer(a, e, b) {
  function c() {
    var a = j.store("tasks") || [],
      c = j.store("projects") || [],
      d = 0;
    _(c).each(function (c) {
      var e = c.id;
      e < 0 && (d++, $.ajaxQueue.post("/api/" + b + "/projects/", {
        data: {
          project: c
        },
        success: function (b) {
          _(a).each(function (a) {
            if (a.project && a.project.id == e) a.project = b.data
          })
        }
      }))
    });
    $.ajaxQueue.add(function () {
      _(a).each(function (a) {
        if (a.updated || a.deleted) {
          d++;
          var b = new Task(j, {
            data: a,
            save_now: false,
            start_now: false
          });
          a.deleted ? b.destroy() : a.updated && b.save(true)
        }
      })
    });
    return d > 0
  }
  function d(a) {
    delete j.refreshing;
    delete j.refresh_required;
    $(document.body).trigger("dataready", [a]);
    j.ui.update_task_list();
    $("#middle-notify").addClass("hidden")
  }
  function f() {
    _("tasks,projects,planned_tasks,tags,workspaces,clients,templates".split(",")).each(function (a) {
      l[a](j.store(a))
    });
    d(j.store("tasks"))
  }
  function g(a) {
    var b = "Server response error.";
    window.localStorage && j.store("tasks") ? (_(a).each(function (a) {
      a.abort()
    }), f()) : (b += " No data available offline.", j.ui.show_refresh());
    j.ui.show_error(b);
    $(window).trigger("offline")
  }

  function h() {
    j.get_online() ? (j.shown_task && $("#tracking").trigger("reset").addClass("hidden"), $("#middle-notify").removeClass("hidden"), j.tags = {}, $(document).one("ajaxStop", function () {
      var a = [];
      j.tasks_data = [];
      j.projects_data = [];
      j.workspaces_data = [];
      $.ajax($.extend({
        type: "GET",
        data: "",
        global: false,
        error: function () {
          g(a)
        }
      }, {
        data: {
          with_related_data: true
        },
        url: "/api/" + b + "/me",
        success: function (a) {
          a && a.data && (_("tasks,projects,planned_tasks,tags,workspaces,clients,templates".split(",")).each(function (b) {
            var c = b;
            "tasks" === c ? c = "time_entries" : "planned_tasks" === c && (c = "tasks");
            var d = a.data[c] || [];
            "time_entries" === c && (d = _.map(d, function (a) {
              if (a.task) a.planned_task = $.extend(true, {}, a.task), delete a.task;
              return a
            }));
            $.toJSON(d) !== j.store_string(b) && j.store(b, d);
            l[b](d)
          }), j.store("current_ws", a.data.default_workspace_id));
          d(a.data.time_entries)
        }
      }))
    }), c() || $(document).trigger("ajaxStop")) : (j.tags = {}, f())
  }
  var j = this;
  this.version = e;
  this.api_version = b;
  this.ui = new a(j);
  this.tasks_data = [];
  this.projects_data = [];
  this.workspaces_data = [];
  var l = {
    tasks: function (a) {
      _.each(a, function (a) {
        $.inArray(a.description, _.pluck(j.tasks_data, "description")) === -1 && j.tasks_data.push({
          description: a.description || Translation["(no description)"],
          project: a.project,
          billable: a.billable,
          workspace: a.workspace
        })
      })
    },
    projects: function (a) {
      j.projects_data = _(a).sortBy(function (a) {
        return a.client_project_name
      })
    },
    planned_tasks: function (a) {
      j.tasks_data = $.merge(j.tasks_data, _.map(a, function (a) {
        return {
          description: a.name,
          project: a.project,
          planned_task: a,
          workspace: a.workspace
        }
      }))
    },
    tags: function (a) {
      _.each(a, function (a) {
        j.tags[a.workspace.id] = j.tags[a.workspace.id] || [];
        j.tags[a.workspace.id].push({
          id: a.id,
          name: a.name
        })
      })
    },
    clients: function (a) {
      $("#new-project").data("clients", a)
    },
    templates: function (a) {
      $("#new-project").data("templates", a)
    },
    workspaces: function (a) {
      j.workspaces_data = a;
      $("#new-project-workspace").html(_.map(a, function (a) {
        var b = a.id == j.store("current_ws") ? 'selected="selected"' : "";
        return '<option value="' + a.id + '" ' + b + ">" + a.name + "</option>"
      }).join(""))
    }
  };
  this.create_new_task = function (a, b) {
    var c = new Date;
    return new Task(j, {
      data: $.extend({
        duration: 0,
        id: -_.uniqueId() - 1,
        description: "",
        billable: true,
        start: c.dateFormat(Date.patterns.ISO8601LongPattern),
        stop: c.dateFormat(Date.patterns.ISO8601LongPattern)
      }, a),
      save_now: false,
      start_now: j.logged_in_user.new_time_entries_start_automatically || b
    })
  };
  this.seconds_to_hhmmss = function (a, b) {
    return TimeFormat.seconds_to_hhmmss(a, b)
  };
  this.switch_task = function (a, b) {
    j.shown_task = j.create_new_task(a);
    j.shown_task.update_ui();
    j.logged_in_user.new_time_entries_start_automatically || b ? j.shown_task.start_time(true) : j.shown_task.save();
    j.ui.edit_task(j.shown_task, true)
  };
  this.set_document_title = function () {
    var a = j.tracked_task,
      b = "";
    a && a.is_tracking() && !a.data.deleted && (a.data.project && (b = a.data.project.client_project_name ? a.data.project.client_project_name + ": " : Translation["No project"], b += ": "), b = b + (a.data.description || Translation["(no description)"]) + " - ");
    document.title = b + "Toggl"
  };
  this.store_task = function (a) {
    (new Task(j, {
      data: a,
      save_now: false,
      start_now: a.duration < 0
    })).update_ui()
  };
  this.replace_task_id = function (a, b) {
    $(".task").each(function () {
      var c = $(this).data("instance").data;
      if (c.id === a) c.id = b
    })
  };
  this.store_current_data = function () {
    if (j.refreshing) return setTimeout(j.store_current_data, 50);
    var a = $.grep(j.store("tasks") || [], function (a) {
      return a.deleted || a.updated
    }),
      b = _.pluck(a, "id");
    $(".task").each(function () {
      var c = $(this).data("instance").data,
        d = $.inArray(c.id, b);
      if (d === -1) a.push(c), b.push(c.id);
      else {
        if (c.id < 0 && c.deleted) return a.splice(d, 1), true;
        a[d] = c
      }
    });
    j.store("tasks", a || [])
  };
  this.planned_tasks = function () {
    var a = _.select(j.tasks_data, function (a) {
      return a.planned_task
    });
    return _.map(a, function (a) {
      return a.planned_task
    })
  };
  this.is_premium_workspace = function (a) {
    if (a && a.id) {
      var b = _.detect(j.workspaces_data || [], function (b) {
        return b.id === a.id
      });
      if (b && b.profile_name) return "Free" !== b.profile_name
    }
    return false
  };
  this.test_online = function (a) {
    $.ajax($.extend($.extend({
      success: function (a, b, c) {
        j.offline && c.status > 0 && $(window).trigger("online")
      },
      error: function () {
        $(window).trigger("offline")
      }
    }, a), {
      type: "get",
      url: "/api/" + b + "/status",
      global: false
    }))
  };
  this.test_logged_in = function (a) {
    $.ajax($.extend($.extend({}, a), {
      type: "get",
      url: "/api/" + b + "/me",
      global: false
    }))
  };
  this.get_online = function () {
    var a = true;
    return a = $.browser.webkit ? !j.offline : navigator.onLine === true && !j.offline
  };
  this.revert = f;
  this.store_user = function (a, b) {
    j.logged_in_user = a;
    if (b) {
      var c = AesCtr.encrypt($.toJSON(a), a.email + b, 192),
        d = j.store("users") || {};
      d[a.email] = c;
      j.store("users", d)
    }
  };
  $(document).bind("ajaxSuccess", function (a, b) {
    try {
      if (b && b.responseText) {
        if (j.last_known_data = $.parseJSON(b.responseText).related_data_updated_at, !j.refreshing && b.status === 203) j.refresh_required = true, (j.ui.name === "classic" || !j.shown_task) && $(document).trigger("refresh")
      } else j.offline = true
    } catch (c) {
      j.offline = true
    }
  });
  $(document).bind("refresh", function () {
    if (!j.refreshing) delete j.shown_task, delete j.tracked_task, j.refreshing = true, h()
  });
  this.do_offline_login = function (a) {
    j.offline_login(a.username, a.password) ? ($(document.body).trigger("logindone"), $(window).trigger("offline")) : $(document.body).trigger("loginerror")
  };
  this.update_task_amount = function () {
    return true
  };
  this.do_login = function (a) {
    j.get_online() ? $.ajax({
      url: "/api/" + b + "/sessions",
      type: "post",
      data: {
        email: a.username,
        password: a.password,
        remember_me: a.remember
      },
      success: function (b) {
        b && b.data ? (j.store_user(b.data, a.password), $(document.body).trigger("logindone")) : j.do_offline_login(a)
      },
      error: function () {
        j.do_offline_login(a)
      }
    }) : j.do_offline_login(a)
  };
  window.childSandboxBridge = {
    start_idle: function (a) {
      j.tracked_task && j.tracked_task.go_idle(a)
    },
    end_idle: function () {
      j.ui.end_idle(j.tracked_task)
    },
    is_tracking: function () {
      return !(!j.tracked_task || !j.tracked_task.is_tracking())
    },
    logout: function () {
      $(document.body).trigger("logout")
    },
    project_ids: function () {
      return _.pluck(j.store("projects"), "id")
    },
    current_project_id: function () {
      if (j.tracked_task && j.tracked_task.is_tracking() && j.tracked_task.data.project) return j.tracked_task.data.project.id
    },
    client: function (a) {
      return $.grep(j.store("clients"), function (b) {
        return b.id == a
      })[0]
    },
    project: function (a) {
      return $.grep(j.store("projects"), function (b) {
        return b.id == a
      })[0]
    },
    project_name: function (a) {
      return (this.project(a) || {
        client_project_name: null
      }).client_project_name
    },
    switch_to_project: function (a) {
      var b = this.project_name(a);
      if (!b) return false;
      var c = _($(".task")).chain().map(function (a) {
        return $(a).data("instance")
      }).select(function (b) {
        return b.data.project && b.data.project.id == a
      }).select(function (a) {
        return a.started_today()
      }).sortBy(function (a) {
        return a.data.start
      }).first().value();
      c || (c = j.create_new_task({
        project: {
          id: a,
          client_project_name: b
        }
      }));
      j.shown_task = c;
      j.shown_task.update_ui();
      j.shown_task.save();
      j.shown_task.start_time(true);
      j.ui.edit_task(j.shown_task)
    },
    stop_current_task: function () {
      j.tracked_task && j.tracked_task.stop_time()
    }
  }
}
Timer.prototype = {
  store_string: function (a, e) {
    var b = this.logged_in_user ? this.logged_in_user.id : 0,
      b = "u" + b + "/" + a;
    "users" === a && (b = a);
    return window.localStorage ? e ? (window.localStorage.removeItem(b), window.localStorage.setItem(b, e)) : window.localStorage.getItem(b) : null
  },
  store: function (a, e) {
    try {
      return e ? this.store_string(a, $.toJSON(e)) : $.evalJSON(this.store_string(a))
    } catch (b) {
      return null
    }
  },
  offline_login: function (a, e) {
    var b = (this.store("users") || {})[a];
    if (b) {
      b = AesCtr.decrypt(b, a + e, 192);
      try {
        return this.logged_in_user = $.parseJSON(b), true
      } catch (c) {}
    }
    return false
  }
};
$(function () {
  try {
    var a = window.applicationCache;
    a && document.addEventListener && a.addEventListener("updateready", function () {
      a.update();
      a.swapCache();
      try {
        document.location.reload()
      } catch (b) {
        try {
          window.location.reload(true)
        } catch (c) {}
      }
    }, false)
  } catch (e) {}
  setInterval(function () {
    try {
      window.applicationCache.update()
    } catch (a) {}
  }, 36E5)
});

function Accordion(a, e) {
  var b = this;
  this.create_containers = function (b) {
    var d = (new Date).getTime();
    for (b++; b; b--) {
      var e = '<h3></h3><div id="date_' + (new Date(d - (b - 1) * 864E5)).dateFormat("Ymd") + '"></div>';
      a.prepend(e)
    }
  };
  this.clean = function () {
    a.children("div").each(function () {
      $(this).children().length || $(this).prev().andSelf().remove()
    });
    $("#no-tasks").toggleClass("hidden", a.find(">div").length > 0)
  };
  this.reinit = function () {
    b.clean();
    b.create_headers();
    a.accordion({
      clearStyle: true,
      autoHeight: false
    })
  };
  this.add_item = function (c, d) {
    var e = $("#date_" + d);
    if (e.length) e.prepend(c);
    else {
      a.accordion("destroy");
      var g = $('<h3></h3><div id="date_' + d + '"></div>');
      $(g[1]).append(c);
      a.children().each(function () {
        var a = this.id.split("_")[1];
        if (d > a) return $(this).prev().before(g), false
      });
      g.closest("#accordion").length || a.append(g);
      b.reinit()
    }
  };
  this.create_headers = function () {
    var b = (new Date).getTime() / 1E3,
      d, e, g = function () {
        d += $(this).data("instance").data.duration
      };
    a.children("div").each(function () {
      d = 0;
      var a = this.id.split("_")[1],
        j = parseInt(a.slice(0, 4), 10),
        l = parseInt(a.slice(4, 6), 10),
        a = parseInt(a.slice(6, 8), 10);
      a: {
        j = new Date(j, l - 1, a);
        l = new Date;
        if (j.getMonth() === l.getMonth() && j.getFullYear() === l.getFullYear()) {
          if (j.getDate() === l.getDate()) {
            e = "<strong>" + Translation.Today + "</strong>";
            break a
          }
          if (j.getDate() === l.getDate() - 1) {
            e = "<strong>" + Translation.Yesterday + "</strong>";
            break a
          }
        }
        e = "<strong>" + j.dateFormat("D") + "</strong>" + j.dateFormat("d. M")
      }
      $(this).children().each(g);
      d < 0 && (d += b);
      j = '<span class="right">' + Math.floor(d / 3600) + ":" + String.leftPad("" + Math.floor(d % 3600 / 60), 2, "0") + "h</span>";
      $(this).prev().html(j + e)
    })
  };
  b.create_containers(e);
  return this
}

function Tasklist(a, e) {
  var b = this;
  this.create_containers = function (b) {
    var d = (new Date).getTime();
    for (b++; b; b--) {
      var e = '<h3></h3><div id="date_' + (new Date(d - (b - 1) * 864E5)).dateFormat("Ymd") + '" class="dateheader"></div>';
      a.prepend(e)
    }
  };
  this.reposition_task_form = function () {
    var b = a.find(".task-form:visible");
    if (b.length) {
      var d = b.data("task");
      d && $("#task_" + d.data.id).after(b)
    }
  };
  this.clean = function () {
    a.children("div").each(function () {
      var a = $(this);
      a.children("div").length || (b.reposition_task_form(), a.prev().andSelf().remove());
      a.prev().toggleClass("hidden", 0 === a.find(".task").length - a.find(".running-task").length)
    });
    b.toggle_header()
  };
  this.toggle_header = function () {
    var a = 0 === $("#accordion").find(".time_per_day:visible").length;
    $(".tasktimer-subtitle, #tasktimer-actions").toggleClass("hidden", a);
    a === true ? $("#tasktimer-footer").addClass("center-text") : $("#tasktimer-footer").removeClass("center-text")
  };
  this.reinit = function () {
    b.clean();
    b.create_headers();
    b.toggle_header()
  };
  this.add_item = function (c, d) {
    var e = $("#date_" + d);
    if (e.length) e.prepend(c);
    else {
      a.accordion("destroy");
      var g = $('<h3></h3><div id="date_' + d + '" class="dateheader"></div>');
      $(g[1]).append(c);
      a.children().each(function () {
        var a = this.id.split("_")[1];
        if (d > a) return $(this).prev().before(g), false
      });
      g.closest("#accordion").length || a.append(g);
      b.reinit()
    }
  };
  this.ensure_headers_visible = function () {
    a.children("div").each(function () {
      var a = $(this),
        b = 0 === a.find(".task:visible").length + a.find("#old-task-form.hidden").length;
      a.prev().toggleClass("hidden", b)
    });
    b.toggle_header()
  };
  this.create_headers = function () {
    var b = (new Date).getTime() / 1E3,
      d, e, g = function () {
        d += $(this).data("instance").data.duration
      };
    a.children("div").each(function () {
      var a = $(this);
      d = 0;
      var j = a.attr("id").split("_")[1],
        l = parseInt(j.slice(0, 4), 10),
        n = parseInt(j.slice(4, 6), 10),
        j = parseInt(j.slice(6, 8), 10);
      a: {
        l = new Date(l, n - 1, j);
        n = new Date;
        if (l.getMonth() === n.getMonth() && l.getFullYear() === n.getFullYear()) {
          if (l.getDate() === n.getDate()) {
            e = "<strong>" + Translation.Today + "</strong>";
            break a
          }
          if (l.getDate() === n.getDate() - 1) {
            e = "<strong>" + Translation.Yesterday + "</strong>";
            break a
          }
        }
        e = "<strong>" + l.dateFormat("D") + " " + l.dateFormat("d. M") + "</strong>"
      }
      a.children("div").each(g);
      d < 0 && (d += b);
      l = '<span class="time_per_day">&ndash; ' + TimeFormat.seconds_to_hhmm(d) + "</span>";
      a.prev().html(e + l)
    })
  };
  $("#tasktimer-footer").toggleClass("hidden");
  b.create_containers(e);
  return this
}
var Tag = {
  template: _.template('<div class="actions-popup-item clearer" id="{{ id }}"><div class="left tag-state no-hide {{ state }}"></div><div class="left tag-name">{{ name }}</div></div>'),
  $new_tag_name: $("#new-tag-name"),
  $new_tag_popup: $("#new-tag-popup"),
  show_new_tag_popup: function (a) {
    Admin.clear_errors();
    a && a.top && Tag.$new_tag_popup.css("top", a.top);
    Tag.$new_tag_popup.parent(".overlay").removeClass("hidden");
    Tag.$new_tag_popup.find("form").bind("submit.temp", function (e) {
      e.preventDefault();
      e = $.trim(Tag.$new_tag_name.val());
      a.submit(e)
    });
    Tag.$new_tag_name.val("").focus()
  },
  hide_new_tag_popup: function () {
    Tag.$new_tag_popup.parent(".overlay").addClass("hidden");
    Tag.$new_tag_popup.find("form").unbind(".temp")
  }
};

function TogglProjectEntry() {
  var a, e, b;
  this.initialize = function (c) {
    a = $(c.id);
    e = c.on_submit;
    b = c.default_workspace_id;
    $('a[rel="cancel-new-project"]').bind("click", this.hide_dialog);
    $("#new-project-workspace").bind("change", this.workspace_changed);
    $("#new-project-workspace").bind("change", this.workspace_templates_change);
    a.find("form").bind("submit", this.submit);
    $('a[rel="new-client"]').bind("click", function () {
      $('a[rel="new-client"]').addClass("hidden");
      $("#new-project-client").addClass("hidden");
      $("#new-client-name").removeClass("hidden").focus()
    })
  };
  this.show_dialog = function (c) {
    Admin.clear_errors();
    $("#new-project-client").removeClass("hidden");
    $("#new-client-name").addClass("hidden");
    $("#new-client-name, #new-project-name, #new-project-client").val("");
    a.toggleClass("single-workspace", !! c);
    b && $("#new-project-workspace").val(b);
    $("#new-project-workspace").change();
    $('a[rel="new-client"]').removeClass("hidden");
    c = a.parent(".overlay");
    c.length ? c.removeClass("hidden") : a.removeClass("hidden");
    $("#new-project-name").focus()
  };
  this.hide_dialog = function () {
    Admin.clear_errors();
    var b = a.parent(".overlay");
    b.length ? b.addClass("hidden") : a.addClass("hidden")
  };
  this.submit = function () {
    var a = $.trim($("#new-project-name").val()),
      b = $("#new-project-client :selected"),
      f = $("#new-project-template :selected"),
      g = $.trim($("#new-client-name").val()),
      h = $("#new-project-workspace :selected"),
      j = $("#new-project-public").is(":checked"),
      h = {
        name: a,
        is_public: j,
        workspace: {
          id: h.val()
        }
      };
    if (!a) return false;
    if (b.val()) h.client = {
      id: b.val()
    };
    if (f.val()) h.template_id = f.val();
    if (g) h.client = {
      name: g
    };
    e(h);
    return false
  };
  this.workspace_templates_change = function () {
    var b = $(this).val(),
      d = a.data("templates") || [];
    d.length < 1 && $(".new-project-template-container").hide();
    d = $.grep(d, function (a) {
      return b == (a.wid || a.workspace.id)
    });
    d.unshift({
      name: ""
    });
    $("#new-project-template").html(_.map(d, function (a) {
      return '<option value="' + (a.id || "") + '">' + a.name + "</option>"
    }).join(""))
  };
  this.workspace_changed = function () {
    var b = $(this).val(),
      d = a.data("clients"),
      d = $.grep(d, function (a) {
        return b == (a.wid || a.workspace.id)
      });
    d.unshift({
      name: ""
    });
    $("#new-project-client").html(_.map(d, function (a) {
      return '<option value="' + (a.id || "") + '">' + a.name + "</option>"
    }).join(""))
  }
}
var OptgroupSelection = {
  template: {
    client: _.template('<option value="client-id-{{ id }}">{{ name }} - All projects</option>'),
    project: _.template('<option value="{{ id }}" data-client-id="{{ client_id }}">{{ name }}</option>'),
    task: _.template('<option value="planned-task-id-{{ id }}" data-project-id="{{ project_id }}" class="planned_task">&nbsp;&nbsp;&nbsp;&nbsp;{{ name }}</option>')
  },
  options_for_project_selection: function (a, e, b, c, d, f, g) {
    var h = [],
      j = a.length > 1 && !b,
      l = {};
    d && h.push('<option value="no-project">' + Translation["(no project)"] + "</option>");
    _.each(a, function (a) {
      j && h.push('<optgroup label="' + a.name + '">');
      _.each(e, function (d) {
        if (d.workspace.id === a.id && (!b || d.workspace.id == b)) {
          var e = d.client ? d.client.id : "";
          c && e && d.is_active !== false && !l[e] && (h.push(OptgroupSelection.template.client({
            id: e,
            name: d.client.name
          })), l[e] = e);
          d.is_active !== false && h.push(OptgroupSelection.template.project({
            id: d.id,
            client_id: e,
            name: d.client_project_name
          }));
          g && _.each(g, function (a) {
            a.is_active !== false && a.project.id == d.id && h.push(OptgroupSelection.template.task({
              id: a.id,
              project_id: d.id,
              name: a.name
            }))
          })
        }
      });
      j && h.push("</optgroup>")
    });
    c && h.unshift('<option value="">' + Translation["(all projects)"] + "</option>");
    f && h.unshift('<option value=""></option>');
    return h.join("")
  },
  update_title: function (a, e, b) {
    var c = $.trim(a.find("option:selected").text());
    e && b && (c = b + " - " + c);
    a.next(".select-box-text-area").find(".select-box-content").text(c)
  },
  select_option: function (a) {
    var e = a.select,
      b = a.projects,
      c = a.planned_tasks,
      d = a.project_id,
      f = a.planned_task_id,
      g = a.client_project_name,
      a = a.do_not_raise_errors;
    if (d) {
      var h = _.detect(b, function (a) {
        return d == a.id
      });
      if (h) g = h.client_project_name
    }
    e.find("option:selected").removeAttr("selected");
    var h = f ? "planned-task-id-" + f : d,
      j = e.find("option[value=" + h + "]");
    if (!j.length) if (/planned-task-id-/.test(h)) {
      var l = h.replace("planned-task-id-", "");
      if (b = _.detect(c, function (a) {
        return a.id == l
      })) j = $(OptgroupSelection.template.task({
        id: b.id,
        project_id: b.project.id,
        name: b.name
      })), j.appendTo(e)
    } else if (b = _.detect(b, function (a) {
      return a.id == d && !a.is_active
    })) j = $(OptgroupSelection.template.project({
      id: b.id,
      client_id: b.client ? b.client.id : "",
      name: b.client_project_name
    })), j.appendTo(e);
    !j.length && !a && (g ? alert("You are not a member of project " + g + ". Please select another project!") : alert("You are not a member of the selected project. Please select another project!"));
    e.val(h);
    OptgroupSelection.update_title(e, f, g)
  },
  select_option_deprecated: function (a, e, b, c, d, f) {
    return OptgroupSelection.select_option({
      select: a,
      projects: e,
      planned_tasks: b,
      project_id: c,
      planned_task_id: d,
      client_project_name: f
    })
  },
  clear_selected_options: function (a) {
    a.val("");
    a.next(".select-box-text-area").find(".select-box-content").text("")
  }
};
(function () {
  window.TaskForm = function (a, e) {
    var b;
    b = {
      hide_old_task_form: null,
      start_tracking_interval: null,
      stop_tracking_interval: null
    };
    b.timer = e;
    b.$task_form = a;
    b.$task_description = b.$task_form.find(".task-description");
    b.$task_project_name = b.$task_form.find(".task-project-name");
    b.$task_duration_ms = b.$task_form.find(".task-duration-ms");
    b.$task_duration_hhmmss = b.$task_form.find(".task-duration-hhmmss");
    b.$task_start_date = b.$task_form.find(".task-date-start");
    b.$task_time_start = b.$task_form.find(".task-time-start");
    b.$task_time_stop = b.$task_form.find(".task-time-stop");
    b.$task_billable = b.$task_form.find(".billable-container > input");
    b.$task_billable_in_reports = b.$task_form.find('.billable-container > a[rel="billable"]');
    b.$task_not_billable_in_reports = b.$task_form.find('.billable-container > a[rel="not-billable"]');
    b.$task_billable_upsell = b.$task_form.find(".billable-upsell-container > input");
    b.$tags_container = b.$task_form.find(".tags-container");
    b.$task_tags = b.$tags_container.find("input");
    if (b.$task_tags.length === 0) b.$task_tags = b.$tags_container.find("a.button");
    b.$planned_task_name = b.$task_form.find(".planned-task-name");
    b.$planned_task_id = b.$task_form.find(".planned-task-id");
    b.$task_select_task_checkbox = b.$task_form.find(".select-task-checkbox");
    b.$task_extra_fields = b.$task_form.find(".task-extra-fields");
    b.$validation_errors = $("#validation-errors");
    b.current_workspace = $.parseJSON(b.$task_form.attr("data-current-workspace"));
    b.$upsell_popup = b.$task_form.find(".upsell-popup");
    b.$tags_popup = b.$task_form.find(".actions-popup");
    b.$tags_popup_list = b.$task_form.find(".actions-popup .tags");
    b.$title = b.$task_form.find(".tasktimer-title");
    b.$button = b.$task_form.find(".add-button");
    b.show_validation_error = function (a) {
      b.$validation_errors.text(a).fadeIn();
      return setTimeout(function () {
        return b.$validation_errors.fadeOut()
      }, 4E3)
    };
    b.show_view_project_link = function () {
      var a, d;
      d = b.$task_project_name.val();
      (a = b.$task_project_name.find("option:selected").attr("data-project-id")) && (d = a);
      return d ? (b.$task_form.find("a[rel=view-project]").removeClass("hidden").attr("href", "/projects/" + d + "/edit"), b.$task_form.find("a[rel=create-new-project]").addClass("hidden")) : (b.$task_form.find("a[rel=view-project]").addClass("hidden").attr("href", "#"), b.$task_form.find("a[rel=create-new-project]").removeClass("hidden"))
    };
    b.set_planned_task = function (a) {
      b.$planned_task_id.val(a && a.id ? a.id : "");
      return b.$planned_task_name.val(a && a.name ? a.name : "")
    };
    b.get_task_data = function () {
      var a, d, e, g, h, j;
      j = {};
      j.description = b.$task_description.val();
      if (j.description === Translation["(no description)"]) j.description = "";
      d = hhmmss_to_milliseconds(b.$task_duration_hhmmss.val());
      j.duration = b.$task_form.data("task").data.duration < 0 ? (d - (new Date).getTime()) / 1E3 : d / 1E3;
      g = Date.parseDate(b.$task_start_date.val(), b.timer.logged_in_user.jquery_date_format);
      d = soft_to_timeofday(b.$task_time_start.val());
      h = soft_to_timeofday(b.$task_time_stop.val());
      if (g && (a = (new Date).getTimezoneOffset() * -60, g.applyOffset(a), g.applyOffset(0), a = g.getDate(), e = g.getMonth(), g = g.getFullYear(), d && h)) d.setFullYear(g, e, a), j.start = d.dateFormat(Date.patterns.ISO8601LongPattern), h.setFullYear(g, e, a), j.stop = h.dateFormat(Date.patterns.ISO8601LongPattern);
      j.billable = b.is_billable();
      return j
    };
    b.get_start_or_end_time = function (a) {
      var d, e;
      d = Date.parseDate(b.$task_start_date.val(), b.timer.logged_in_user.jquery_date_format) || new Date;
      e = soft_to_timeofday(a.val()) || new Date;
      a = ((new Date).getTimezoneOffset() - d.getTimezoneOffset()) * -60;
      e.setUTCFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
      e.applyOffset(a);
      return e
    };
    b.calculate_task_start_time_from_duration = function () {
      var a, d;
      if (b.$task_time_start.val() !== "") return d = b.get_start_or_end_time(b.$task_time_stop), a = hhmmss_to_milliseconds(b.$task_duration_hhmmss.val()), a = new Date(d.getTime() - (a - a % 6E4)), b.$task_time_start.val(a.dateFormat(b.timer.logged_in_user.jquery_timeofday_format))
    };
    b.autocomplete_source_for_tasks = function () {
      var a, d, e, g;
      e = function (a) {
        var b;
        b = a.description || Translation["(no description)"];
        a.project && a.project.client_project_name && (b += " - " + a.project.client_project_name);
        a.planned_task && a.id && (b += " - " + a.planned_task.name);
        return b
      };
      a = _($(".task")).map(function (a) {
        return $(a).data("instance").data
      }).concat(b.timer.tasks_data);
      d = {};
      g = [];
      _.each(a, function (a) {
        var b;
        if (a.id || a.planned_task) if (b = e(a), !d[b]) return g.push(a), d[b] = true
      });
      return _(g).map(function (a) {
        return $.extend(a, {
          value: a.description,
          label: e(a)
        })
      })
    };
    b.init_autocompletes = function () {
      return $("input[data-autocomplete]").each(function () {
        var a;
        a = $(this);
        if (!a.autocomplete("widget").is(".ui-autocomplete")) return a.autocomplete({
          delay: 0,
          minLength: 0
        }), a.prev(".button").unbind(".ac").bind("click.ac", function () {
          if (a.autocomplete("widget").is(":visible")) a.autocomplete("close");
          else return a.autocomplete("search", ""), a.focus()
        })
      })
    };
    b.set_billable = function (a) {
      b.$task_billable.length > 0 && (b.$task_billable.toggleClass("pushed", !! a), b.$task_billable_upsell.toggleClass("pushed", !! a));
      if (b.$task_billable_in_reports.length > 0) return b.timer.ui.set_billable(a)
    };
    b.is_billable = function () {
      if (b.$task_billable.length > 0) return b.$task_billable.hasClass("pushed");
      if (b.$task_billable_in_reports.length > 0) return b.$task_billable_in_reports.hasClass("pushed")
    };
    b.create_tag_fragment = function (a, b, e) {
      var g;
      g = '<div class="left inputbox tag"><input type="checkbox" id="';
      g += e;
      g += '"';
      b && (g += " checked");
      g += '/><label for="';
      g += e;
      g += '">';
      g += a;
      g += "</label></div>";
      return g
    };
    b.redraw_tags = function (a, d) {
      var e, d = d || [];
      b.$task_tags.toggleClass("pushed", d.length > 0);
      if (a) return e = _.map(b.timer.tags[a.id] || [], function (a) {
        return Tag.template({
          id: "tag_" + a.id,
          name: a.name,
          state: _.include(d, a.name) ? "all-checked" : ""
        })
      }), b.$tags_popup_list.html(e.join("")), b.$tags_container.find(".actions-popup-item").bind("click", b.toggle_tag), b.$tags_popup.toggleClass("no-tags", !e.length)
    };
    b.task_delete = function () {
      var a;
      if (confirm(Translation["Delete this time entry?"]) && (a = b.$task_form.data("task"))) a.destroy(), b.timer.tracked_task === a ? (b.timer.tracked_task = null, b.$task_form.hide()) : b.$task_form.fadeOut(500), b.$task_form.data("task", null), b.task_deleted && b.task_deleted();
      return false
    };
    b.update_hidden_duration = function (a, b) {
      var e;
      e = hhmmss_to_milliseconds(a.val());
      b.val(e || 0);
      e = TimeFormat.seconds_to_hhmmss(e / 1E3, true);
      return e === "0 sec" ? a.val("") : a.val(e)
    };
    b.update_visible_duration = function (a, b) {
      var e;
      e = a.val();
      e = TimeFormat.seconds_to_hhmmss(e / 1E3, true);
      return b.val(e)
    };
    b.task_duration_change = function () {
      return b.update_visible_duration($(this), b.$task_duration_hhmmss)
    };
    b.is_single_workspace = function () {
      return b.timer.workspaces_data.length < 2
    };
    b.calculate_duration_from_start_and_stop_values = function (a, d) {
      var e, g, h;
      g = Date.parseDate(b.$task_start_date.val(), b.timer.logged_in_user.jquery_date_format) || new Date;
      h = d;
      if (a && h) return e = (new Date).getTimezoneOffset() * -60, a.applyOffset(e), h.applyOffset(e), a.setUTCFullYear(g.getUTCFullYear(), g.getUTCMonth(), g.getUTCDate()), h.setUTCFullYear(g.getUTCFullYear(), g.getUTCMonth(), g.getUTCDate()), a.applyOffset(0), h.applyOffset(0), h.getTime() < a.getTime() && (h = new Date(h.getTime() + 864E5)), b.$task_duration_hhmmss.removeClass("text-light"), e = 0, b.$task_form.data("task") && (e = (new Date(hhmmss_to_milliseconds(b.$task_duration_hhmmss.val()))).getSeconds() * 1E3), b.$task_duration_ms.val(h.getTime() - a.getTime() + e).change()
    };
    b.format_time_on_blur = function () {
      var a;
      a = $(this);
      return a.bind("blur.temp", function () {
        var d;
        (d = soft_to_timeofday(a.val())) && a.val(d.dateFormat(b.timer.logged_in_user.jquery_timeofday_format));
        return a.unbind(".temp")
      })
    };
    b.toggle_non_premium_workspace_form = function (a) {
      var d;
      d = !b.timer.is_premium_workspace(a);
      a = a && a.id ? "/workspace/upgrade_info?wsid=" + a.id : "/workspace/upgrade_info";
      b.$task_form.toggleClass("non-premium", d);
      return $("a[rel=pro-feature-link]").attr("href", a)
    };
    b.init_autocompletes();
    b.$task_form.find("a[rel=delete]").live("click", b.task_delete);
    b.$task_duration_ms.bind("change", b.task_duration_change);
    b.$task_duration_hhmmss.bind("keypress", function (a) {
      if (a.keyCode === 13) return $(this).blur()
    });
    b.$task_billable_upsell.qtip({
      content: b.$upsell_popup,
      show: {
        delay: 0,
        when: {
          event: "click"
        },
        effect: {
          length: 0
        }
      },
      hide: {
        when: {
          event: "unfocus"
        }
      },
      position: {
        corner: {
          target: "center",
          tooltip: "center"
        }
      },
      style: {
        name: "toggl"
      }
    });
    $("a[rel=cancel-new-tag]").bind("click", Tag.hide_new_tag_popup);
    b.$task_form.find("a[rel=add-new-tag]").bind("click", function () {
      Tag.show_new_tag_popup({
        top: b.$task_form.offset().top - 42,
        submit: b.submit_new_tag
      });
      return false
    });
    return b
  }
}).call(this);
(function () {
  window.TaskFormWithTask = function (a, e) {
    var b;
    b = TaskForm(a, e);
    b.init_task_autocomplete = function () {
      return b.$task_description.autocomplete({
        source: b.autocomplete_source_for_tasks(),
        focus: function () {
          return false
        },
        select: function (a, d) {
          var e, g, h, j;
          e = d.item;
          j = b.$task_form.data("task");
          if (e.project) {
            if (h = e.project.id, g = null, h && (g = _.detect(b.timer.projects_data, function (a) {
              return parseInt(h, 10) === parseInt(a.id, 10)
            }), b.set_billable(g && b.timer.is_premium_workspace(g.workspace) && g.billable), g)) j.data.project = g, j.data.billable = g.billable
          } else b.set_billable(e.billable);
          j.data.planned_task = e.planned_task;
          $(this).val(e.description);
          g && g.client_project_name ? OptgroupSelection.select_option_deprecated(b.$task_project_name, b.timer.projects_data, b.timer.planned_tasks(), e.project.id, e.planned_task ? e.planned_task.id : null) : OptgroupSelection.clear_selected_options(b.$task_project_name);
          $("label[for=task-description]").removeClass("hidden");
          j.gui_workspace && j.gui_workspace.id !== e.workspace.id ? b.workspace_changed(e.workspace) : b.redraw_tags(e.workspace, j.data.tag_names);
          j.gui_workspace = {
            id: e.workspace.id
          };
          b.import_data();
          b.show_view_project_link();
          return b.$task_description.autocomplete({
            source: b.autocomplete_source_for_tasks()
          })
        }
      })
    };
    b.description_input_detect_backspace = function (a) {
      if (8 === a.keyCode) return b.description_input()
    };
    b.description_input = function () {
      clearTimeout(b.timer.text_area_change);
      return b.timer.text_area_change = setTimeout(function () {
        b.import_data();
        return delete b.timer.text_area_change
      }, 750)
    };
    b.toggle_billable = function () {
      var a;
      a = b.is_billable();
      b.set_billable(!a);
      return b.$task_form.data("task").import_data({
        billable: !a
      })
    };
    b.workspace_changed = function (a) {
      var d;
      d = b.$task_form.data("task");
      d.data.tag_names = [];
      d.save();
      b.redraw_tags(a, d.data.tag_names);
      return b.$task_form.data("task").gui_workspace = a
    };
    b.task_project_changed = function () {
      var a, d, e, g, h, j, l;
      l = b.$task_form.data("task");
      d = l.gui_workspace || l.data.workspace;
      a = b.$task_project_name.find(":selected");
      h = j = a.val();
      e = {};
      g = null;
      if (/planned-task-id-/.test(j)) {
        g = j.replace("planned-task-id-", "");
        if ((j = _.detect(b.timer.tasks_data, function (a) {
          return a.planned_task && a.planned_task.id.toString() === g
        })) && j.planned_task) e = j.planned_task;
        h = a.attr("data-project-id")
      }
      j = l.data.project && l.data.project.id === h || !l.data.project && h === "";
      a = l.data.planned_task && l.data.planned_task.id === g || !l.data.planned_task && !g;
      if (j && a) return false;
      (a = _.detect(b.timer.projects_data || [], function (a) {
        return a.id.toString() === h
      })) ? (b.set_billable(b.timer.is_premium_workspace(a.workspace) && a.billable), l.data.project = {
        client_project_name: a.client_project_name,
        id: a.id
      }, l.data.workspace = {
        id: a.workspace.id,
        name: a.workspace.name
      }) : l.data.project = null;
      l.data.planned_task = e ? {
        id: e.id,
        name: e.name
      } : null;
      b.toggle_non_premium_workspace_form(a ? a.workspace : l.data.workspace);
      b.import_data();
      a && d.id !== a.workspace.id && b.workspace_changed(a.workspace);
      b.set_planned_task(e);
      OptgroupSelection.update_title(b.$task_project_name, e.id, a ? a.client_project_name : null);
      b.show_view_project_link();
      return b.$task_description.autocomplete({
        source: b.autocomplete_source_for_tasks()
      })
    };
    b.select_project = function (a) {
      var d;
      OptgroupSelection.select_option_deprecated(b.$task_project_name, b.timer.projects_data, b.timer.planned_tasks(), a.id);
      b.$task_form.data("task").data.project = {
        client_project_name: a.client_project_name,
        id: a.id
      };
      b.toggle_non_premium_workspace_form(a.workspace);
      d = _.detect(b.timer.projects_data, function (b) {
        return b.id === a.id
      });
      b.set_billable(d && b.timer.is_premium_workspace(d.workspace) && d.billable);
      b.import_data();
      b.set_planned_task({});
      return b.show_view_project_link()
    };
    b.calculate_duration_from_start_and_stop = function () {
      var a, d;
      a = soft_to_timeofday(b.$task_time_start.val());
      d = soft_to_timeofday(b.$task_time_stop.val());
      return b.calculate_duration_from_start_and_stop_values(a, d)
    };
    b.import_data = function () {
      return b.$task_form.data("task").import_data(b.get_task_data())
    };
    b.duration_change = function () {
      var a;
      a = $(this);
      b.$task_time_start.val();
      b.update_hidden_duration(a, b.$task_duration_ms);
      b.calculate_task_start_time_from_duration();
      b.import_data();
      a = b.$task_form.data("task");
      return $(this).val(a.export_time())
    };
    b.duration_blur = function () {
      return b.start_tracking_interval()
    };
    b.duration_input = function () {
      return b.stop_tracking_interval()
    };
    b.task_start_input = function () {
      var a, d, e;
      a = $(this);
      e = a.val();
      d = b.$task_duration_hhmmss.val();
      a.bind("blur.temp", function () {
        if (a.val() !== e || d !== b.$task_duration_hhmmss.val()) b.calculate_duration_from_start_and_stop(), b.import_data();
        return a.unbind(".temp")
      });
      return a.bind("keypress.temp", function (b) {
        if (b.keyCode === 13) return a.blur()
      })
    };
    b.task_stop_input = function () {
      var a, d, e;
      a = $(this);
      e = a.val();
      d = b.$task_duration_hhmmss.val();
      a.bind("blur.temp", function () {
        if (a.val() !== e || d !== b.$task_duration_hhmmss.val()) b.calculate_duration_from_start_and_stop(), b.import_data();
        return a.unbind(".temp")
      });
      return a.bind("keypress.temp", function (b) {
        if (b.keyCode === 13) return a.blur()
      })
    };
    b.submit_new_tag = function (c) {
      var d, e;
      c && (e = b.$task_form.data("task"), b.timer.tags[e.data.workspace.id] = b.timer.tags[e.data.workspace.id] || [], (d = _.detect(b.timer.tags[e.data.workspace.id], function (a) {
        return a.name === c
      })) ? (a = b.$task_form.find("#tag_" + d.id + " .tag-state"), a.is(".all-checked") || a.click()) : $.ajaxQueue.post("/api/" + b.timer.api_version + "/tags/", {
        data: {
          name: c,
          workspace_id: e.data.workspace.id
        },
        success: function (a) {
          e.data.tag_names = e.data.tag_names || [];
          e.data.tag_names.push(a.data.name);
          b.timer.tags[e.data.workspace.id].push({
            name: a.data.name,
            id: a.data.id
          });
          e.save();
          return $("body").trigger("new_tag_received", a.data)
        }
      }));
      Tag.hide_new_tag_popup();
      return false
    };
    b.new_tag_received = function () {
      var a;
      if (a = b.$task_form.data("task")) return b.redraw_tags(a.data.workspace, a.data.tag_names)
    };
    b.toggle_tag = function () {
      var a, d, e;
      a = $(this);
      d = a.find(".tag-state");
      e = a.find(".tag-name").text();
      a = b.$task_form.data("task");
      if (!a.data.tag_names) a.data.tag_names = [];
      d.hasClass("all-checked") ? (a.data.tag_names = _.reject(a.data.tag_names, function (a) {
        return a === e
      }), d.removeClass("all-checked")) : (_.include(a.data.tag_names, e) || a.data.tag_names.push(e), d.addClass("all-checked"));
      b.$task_tags.toggleClass("pushed", a.data.tag_names.length > 0);
      return a.save()
    };
    b.initialize = function () {
      $(".collapse-task-details").live("click", b.hide_old_task_form);
      b.$task_start_date.bind("change", b.import_data);
      $("#task-time button").bind("click", b.import_data);
      b.$task_time_start.bind("focus", b.task_start_input);
      b.$task_time_stop.bind("focus", b.task_stop_input);
      b.$task_description.bind("keypress paste cut", b.description_input);
      b.$task_description.bind("keyup", b.description_input_detect_backspace);
      b.$task_billable.length > 0 && b.$task_billable.bind("click", b.toggle_billable);
      b.$task_billable_in_reports.length > 0 && (b.$task_billable_in_reports.bind("click", function (a) {
        a.preventDefault();
        if (!$(this).hasClass("pushed")) return b.toggle_billable()
      }), b.$task_not_billable_in_reports.bind("click", function (a) {
        a.preventDefault();
        if (!$(this).hasClass("pushed")) return b.toggle_billable()
      }));
      b.$task_select_task_checkbox.live("click", function () {
        var a;
        a = b.$task_form.data("task");
        return $("#task_" + a.data.id).find("input:checkbox").attr("checked", $(this).is(":checked"))
      });
      b.$task_project_name.unbind("change keyup").bind("change keyup", b.task_project_changed);
      b.$task_duration_hhmmss.bind("focus", b.duration_input);
      b.$task_duration_hhmmss.bind("change", b.duration_change);
      b.$task_duration_hhmmss.bind("blur", b.duration_blur);
      b.$task_duration_hhmmss.bind("keypress", function (a) {
        if (a.keyCode === 13) return $(this).change()
      });
      return b.$task_description.bind("keypress", function (a) {
        if (a.keyCode === 13) return $(this).blur()
      })
    };
    return b
  }
}).call(this);
(function () {
  window.TaskFormWithoutTask = function (a, e) {
    var b;
    b = TaskForm(a, e);
    b.tag_names = [];
    b.submitted = null;
    b.init_task_autocomplete = function () {
      return b.$task_description.autocomplete({
        source: b.autocomplete_source_for_tasks(),
        focus: function () {
          return false
        },
        select: function (a, d) {
          var e, g, h;
          e = d.item;
          $(this).val(e.description);
          if (e.project) {
            if (g = e.project, h = g.id) g = _.detect(b.timer.projects_data, function (a) {
              return parseInt(h, 10) === parseInt(a.id, 10)
            }), b.set_billable(g && b.timer.is_premium_workspace(g.workspace) && g.billable), b.toggle_non_premium_workspace_form(g ? g.workspace : b.current_workspace)
          } else b.set_billable(e.billable);
          g = e.planned_task ? e.planned_task.id : null;
          e.project && e.project.client_project_name ? OptgroupSelection.select_option_deprecated(b.$task_project_name, b.timer.projects_data, b.timer.planned_tasks(), e.project.id, g, e.project.client_project_name) : OptgroupSelection.clear_selected_options(b.$task_project_name);
          b.set_planned_task(e.planned_task || {});
          return b.show_view_project_link()
        }
      })
    };
    b.new_task_form_reset = function () {
      var a;
      b.$task_description.val("");
      b.$task_duration_hhmmss.val("0 min").addClass("text-light");
      b.$task_duration_ms.val(0);
      b.$task_time_start.val("");
      b.$task_time_stop.val("");
      b.set_planned_task({});
      b.$task_time_start.parent().removeClass("invalid-field");
      b.tag_names = [];
      b.redraw_tags(b.current_workspace, b.tag_names);
      OptgroupSelection.clear_selected_options(b.$task_project_name);
      b.show_view_project_link();
      a = Date.parseDate(b.$task_start_date.val(), b.timer.logged_in_user.jquery_date_format);
      return b.$task_form.hasClass("manual-mode") ? b.reset_extra_fields(a) : b.$task_extra_fields.fadeOut(200, function () {
        return b.reset_extra_fields(a)
      })
    };
    b.reset_extra_fields = function (a) {
      var d;
      b.$task_description.each(function () {
        var a;
        a = $(this);
        if (a.autocomplete("widget").is(":visible")) return a.autocomplete("close")
      });
      a && (d = (new Date).getTimezoneOffset() * -60, a.applyOffset(d), a.setUTCFullYear(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate()), a.applyOffset(0), b.$task_start_date.val(a.dateFormat(b.timer.logged_in_user.jquery_date_format)));
      return b.$task_description.focus()
    };
    b.project_selected_for_new_task = function () {
      var a, d, e, g, h, j;
      a = b.$task_project_name.find("option:selected");
      h = j = a.attr("value");
      d = {};
      g = null;
      if (/planned-task-id-/.test(j)) {
        e = j.replace("planned-task-id-", "");
        if ((j = _.detect(b.timer.tasks_data, function (a) {
          return a.planned_task && a.planned_task.id.toString() === e
        })) && j.planned_task) d = j.planned_task;
        h = a.attr("data-project-id")
      }
      h && (g = _.detect(b.timer.projects_data, function (a) {
        return h === a.id.toString()
      }), b.set_billable(g && b.timer.is_premium_workspace(g.workspace) && g.billable), b.toggle_non_premium_workspace_form(g ? g.workspace : b.current_workspace));
      b.set_planned_task(d);
      OptgroupSelection.update_title(b.$task_project_name, d.id, g ? g.client_project_name : null);
      return b.show_view_project_link()
    };
    b.toggle_new_task_billable = function () {
      return b.set_billable(!b.is_billable())
    };
    b.new_task_form_submit = function () {
      var a, d, e, g, h, j, l, n;
      e = (new Date).getTimezoneOffset() * -60;
      h = new Date;
      l = new Date;
      a = false;
      if (d = Date.parseDate(b.$task_form.find(".todays-date").val(), b.timer.logged_in_user.jquery_date_format)) d.applyOffset(e), d.applyOffset(0), l.setFullYear(d.getFullYear(), d.getMonth(), d.getDate()), a = h.getUTCFullYear() === l.getUTCFullYear() && h.getUTCMonth() === l.getUTCMonth() && h.getUTCDate() === l.getUTCDate();
      if (d === null || !a) d = (new Date).dateFormat(b.timer.logged_in_user.jquery_date_format), b.$task_start_date.val(d), b.$task_form.find(".todays-date").val(d);
      b.$task_duration_hhmmss.focusout().change();
      if (b.$task_time_start.val() === "" && b.$task_time_stop.val() !== "") b.show_validation_error("Please insert start time"), b.$task_time_start.focus(), b.$task_time_start.parent().addClass("invalid-field");
      else {
        n = new Date;
        if (d = Date.parseDate(b.$task_start_date.val(), b.timer.logged_in_user.jquery_date_format)) d.applyOffset(e), d.applyOffset(0), n.setFullYear(d.getFullYear(), d.getMonth(), d.getDate());
        l = soft_to_timeofday(b.$task_time_start.val());
        a = soft_to_timeofday(b.$task_time_stop.val());
        d = hhmmss_to_milliseconds(b.$task_duration_hhmmss.val());
        b.$task_duration_ms.val(d || 0);
        b.$task_duration_hhmmss.val(b.timer.seconds_to_hhmmss(d / 1E3));
        a || (a = new Date, a.setUTCFullYear(n.getUTCFullYear(), n.getUTCMonth(), n.getUTCDate()), e = ((new Date).getTimezoneOffset() - n.getTimezoneOffset()) * -60, a.applyOffset(e));
        l || (l = new Date(a.getTime() - d));
        l.dateFormat("Ymd") !== n.dateFormat("Ymd") && (e = new Date(n), e.setHours(l.getHours()), e.setMinutes(l.getMinutes()), l = new Date(e));
        a.dateFormat("Ymd") !== n.dateFormat("Ymd") && (e = new Date(n), e.setHours(a.getHours()), e.setMinutes(a.getMinutes()), a = new Date(e));
        e = b.timer.create_new_task({}, true);
        e.data.description = b.$task_description.val();
        e.data.start = l.dateFormat(Date.patterns.ISO8601LongPattern);
        e.data.stop = a.dateFormat(Date.patterns.ISO8601LongPattern);
        e.data.billable = b.is_billable();
        e.data.tag_names = b.tag_names;
        e.data.ignore_start_and_stop = !b.timer.logged_in_user.store_start_and_stop_time;
        a = b.$task_project_name.find("option:selected");
        if (a.length && a.val()) {
          n = a.val();
          j = /planned-task-id-/.test(n) ? a.attr("data-project-id") : n;
          a = _.detect(b.timer.projects_data || [], function (a) {
            return a.id.toString() === j
          });
          if (!a) throw "Project " + j + " not found!";
          e.data.project = {
            id: a.id,
            client_project_name: a.client_project_name
          };
          e.data.workspace = {
            id: a.workspace.id,
            name: a.workspace.name
          }
        }
        a = b.$planned_task_id.val();
        if (a.length) e.data.planned_task = {
          id: a,
          name: b.$planned_task_name.val()
        };
        n = b.$task_time_start.val() !== "" && b.$task_time_stop.val() !== "";
        a = !b.$task_duration_ms.val() && b.$task_time_start.val() === "" && b.$task_time_stop.val() === "";
        g = h.getUTCFullYear() === l.getUTCFullYear() && h.getUTCMonth() === l.getUTCMonth() && h.getUTCDate() === l.getUTCDate();
        h = new Date;
        h = !g || l.getTime() > h.getTime();
        e.data.duration = b.$task_form.hasClass("manual-mode") || a || n || h || d ? b.$task_duration_ms.val() / 1E3 : b.$task_duration_ms.val() / 1E3 - (new Date).getTime() / 1E3;
        if (!e.data.workspace) e.data.workspace = {
          id: b.current_workspace.id,
          name: b.current_workspace.name
        };
        e.update_ui();
        e.save();
        b.submitted(e);
        b.new_task_form_reset();
        return b.$task_description.autocomplete({
          source: b.autocomplete_source_for_tasks()
        })
      }
    };
    b.calculate_duration_from_start_and_stop = function () {
      var a, d;
      a = soft_to_timeofday(b.$task_time_start.val());
      d = soft_to_timeofday(b.$task_time_stop.val()) || new Date;
      return b.calculate_duration_from_start_and_stop_values(a, d)
    };
    b.select_project = function (a) {
      b.$task_project_name.val(a.id);
      b.set_planned_task({});
      b.project_selected_for_new_task();
      return b.$task_project_name.change()
    };
    b.task_duration_focusin = function () {
      "0 min" === b.$task_duration_hhmmss.val() && b.$task_duration_hhmmss.val("");
      return b.$task_duration_hhmmss.removeClass("text-light")
    };
    b.duration_input = function () {
      var a;
      a = $(this);
      a.bind("blur.temp", function () {
        b.update_hidden_duration($(this), b.$task_duration_ms);
        b.calculate_task_start_time_from_duration();
        b.$task_duration_hhmmss.val() || b.$task_duration_hhmmss.addClass("text-light").val("0 min");
        return a.unbind(".temp")
      });
      return a.bind("keypress.temp", function (b) {
        if (13 === b.keyCode) return a.blur()
      })
    };
    b.task_start_input = function () {
      var a;
      a = $(this);
      a.val();
      a.bind("blur.temp", function () {
        b.calculate_duration_from_start_and_stop();
        return a.unbind(".temp")
      });
      return a.bind("keypress.temp", function (b) {
        if (13 === b.keyCode) return a.blur()
      })
    };
    b.task_stop_input = function () {
      var a;
      a = $(this);
      a.val();
      a.bind("blur.temp", function () {
        b.calculate_duration_from_start_and_stop();
        return a.unbind(".temp")
      });
      return a.bind("keypress.temp", function (b) {
        if (13 === b.keyCode) return a.blur()
      })
    };
    b.toggle_tag = function () {
      var a, d, e;
      a = $(this);
      d = a.find(".tag-state");
      e = a.find(".tag-name").text();
      d.hasClass("all-checked") ? (b.tag_names = _.reject(b.tag_names, function (a) {
        return a === e
      }), d.removeClass("all-checked")) : (_.include(b.tag_names, e) || b.tag_names.push(e), d.addClass("all-checked"));
      return b.$task_tags.toggleClass("pushed", b.tag_names.length > 0)
    };
    b.submit_new_tag = function (c) {
      var d;
      c && (b.timer.tags[b.current_workspace.id] = b.timer.tags[b.current_workspace.id] || [], (d = _.detect(b.timer.tags[b.current_workspace.id], function (a) {
        return a.name === c
      })) ? (a = b.$task_form.find("#tag_" + d.id + " .tag-state"), a.is(".all-checked") || a.click()) : $.ajaxQueue.post("/api/" + b.timer.api_version + "/tags/", {
        data: {
          name: c,
          workspace_id: b.current_workspace.id
        },
        success: function (a) {
          b.tag_names.push(a.data.name);
          b.timer.tags[b.current_workspace.id].push({
            name: a.data.name,
            id: a.data.id
          });
          return $("body").trigger("new_tag_received", a.data)
        },
        error: function (a) {
          return Admin.show_error(a)
        }
      }));
      Tag.hide_new_tag_popup();
      return false
    };
    b.new_tag_received = function () {
      return b.redraw_tags(b.current_workspace, b.tag_names)
    };
    b.update_form_title = function () {
      return b.$task_form.hasClass("manual-mode") ? (b.$button.val(Translation.Save), b.$title.text(Translation["What have you done?"])) : (b.$button.val(Translation.Start), b.$title.text(Translation["What are you working on?"]))
    };
    b.initialize = function () {
      b.update_form_title();
      b.$task_description.focus();
      b.$task_start_date.val((new Date).dateFormat(b.timer.logged_in_user.jquery_date_format));
      _.each([b.$task_description, b.$task_duration_hhmmss], function (a) {
        return $(a).bind("keyup click", function () {
          return b.$task_extra_fields.fadeIn(500)
        })
      });
      _.each([b.$task_time_start, b.$task_time_stop], function (a) {
        return $(a).bind("focus", b.format_time_on_blur)
      });
      b.$task_form.bind("submit", b.new_task_form_submit);
      b.$task_duration_hhmmss.bind("focusin", b.task_duration_focusin);
      b.$task_duration_hhmmss.bind("focus", b.duration_input);
      b.$task_time_start.bind("focus", b.task_start_input);
      b.$task_time_stop.bind("focus", b.task_stop_input);
      b.$task_project_name.unbind("change").bind("change", b.project_selected_for_new_task);
      b.$task_form.find("a[rel=timer-mode]").click(function () {
        b.$task_form.removeClass("manual-mode");
        b.update_form_title();
        return $.post("/user/update", {
          _method: "PUT",
          manual_mode: false
        })
      });
      b.$task_form.find("a[rel=manual-mode]").click(function () {
        b.$task_form.addClass("manual-mode");
        b.update_form_title();
        return $.post("/user/update", {
          _method: "PUT",
          manual_mode: true
        })
      });
      b.$task_billable.bind("click", b.toggle_new_task_billable);
      b.$task_description.bind("keypress", function (a) {
        if (a.keyCode === 13) return $(this).blur()
      });
      return $(document).click(function (a) {
        if ($(a.target).parents("#new-task-form, .ui-autocomplete, .ui-datepicker, .ui-datepicker-header").length === 0 && !$("#more-actions").is(":visible") && !b.$task_project_name.val() && !b.$task_time_start.val() && !b.$task_time_stop.val() && !b.$task_duration_hhmmss.val() && !b.$task_description.val()) return b.$task_extra_fields.fadeOut(500)
      })
    };
    return b
  }
}).call(this);
(function () {
  window.TasktimerUI = function (a) {
    var e, b, c, d, f, g, h, j, l, n, m, p, o, u, w, s, D;
    s = this;
    D = false;
    d = null;
    l = new TogglProjectEntry;
    h = new TaskFormWithoutTask($("#new-task-form"), a);
    n = new TaskFormWithTask($("#running-task-form"), a);
    j = new TaskFormWithTask($("#old-task-form"), a);
    u = [h, n, j];
    c = $("#update-notify");
    e = $("#accordion");
    b = $("#content");
    f = true;
    w = _.template('<div class="clearer task {{ task_class }}" id="task_{{ task_id }}"><input type="checkbox" class="left"/><div class="time-entry-description-holder clickable opens-time-entry"><div class="title-container left inline"><span class="billable {{ billable_class }}"><div title="This time entry is billable" class="billable_icon_small"></div></span><span class="title task-desc" title="{{ title }}">{{ tags }}{{ title }}</span></div></div><div class="right"><span class="project-name clickable opens-time-entry" title="{{ task_client_project_name }}">{{ task_client_project_name }}</span><span class="duration clickable opens-time-entry"></span><span class="start-stop-container clickable opens-time-entry">{{ start_stop }}</span><span class="continue-task" title="' + Translation["Continue with this time entry"] + '"><a href="javascript:void(0)" rel="continue">' + Translation.Continue + "</a></span></div></div>");
    m = function () {
      var b, c;
      return (b = a.tracked_task) && b.is_tracking() && b.data.description ? (c = b.data.description, b.data.project && b.data.project.client_project_name && (c += " - ", c += b.data.project.client_project_name.replace(" - ", ": ")), document.title = c) : document.title = "Toggl"
    };
    p = function () {
      m();
      return D !== false ? void 0 : D = setInterval(function () {
        var b;
        if ((b = n.$task_form.data("task")) && b.is_tracking()) return b = b.export_time(), n.$task_duration_hhmmss.val(b), n.$task_time_stop.val((new Date).dateFormat(a.logged_in_user.jquery_timeofday_format))
      }, 500)
    };
    o = function () {
      m();
      D && clearInterval(D);
      return D = false
    };
    g = {
      dataready: function (b) {
        g.populate_project_select();
        _.each(b, function (b) {
          return a.store_task(b)
        });
        a.tracked_task && s.edit_task(a.tracked_task);
        setInterval(function () {
          return d.create_headers()
        }, 1E4);
        _.each(u, function (a) {
          return a.init_task_autocomplete()
        });
        h.redraw_tags(h.current_workspace, []);
        l.initialize({
          id: "#new-project",
          on_submit: g.new_project_submit
        });
        h.$task_form.find("a[rel=create-new-project]").bind("click", function () {
          $("#new-project").data("form", h);
          return l.show_dialog(h.is_single_workspace())
        });
        n.$task_form.find("a[rel=create-new-project]").bind("click", function () {
          $("#new-project").data("form", n);
          return l.show_dialog(j.is_single_workspace())
        });
        j.$task_form.find("a[rel=create-new-project]").bind("click", function () {
          $("#new-project").data("form", j);
          return l.show_dialog(j.is_single_workspace())
        });
        window.first_task && a.switch_task(window.first_task);
        delete a.refreshing;
        delete a.refresh_required;
        return g.dataready = function (b) {
          _.each(u, function (a) {
            return a.init_task_autocomplete()
          });
          _.each(b || [], function (b) {
            return a.store_task(b)
          });
          g.populate_project_select();
          delete a.refreshing;
          return delete a.refresh_required
        }
      },
      docready: function () {
        $(document.body).bind("dataready", function (a, b) {
          return g.dataready(b)
        });
        a.test_logged_in({
          success: function (b) {
            if (b && (b = b.data) && b.id) return a.store_user(b), $(document.body).trigger("logindone")
          },
          complete: function () {
            return $("#login-temp").remove()
          }
        });
        $("#username").focus();
        $(document.body).bind("logindone", g.logindone);
        return $(document.body).bind("loginerror", g.loginerror)
      },
      pass_click_to_anchor: function (a) {
        return $(a.target).is("a") ? ($(this).parent().click(), false) : $(this).find("a").click()
      },
      continue_task: function () {
        var b;
        a.tracked_task && a.tracked_task.is_tracking() && a.tracked_task.stop_time();
        b = $(this).closest(".task").data("instance") || j.$task_form.data("task");
        a.logged_in_user.store_start_and_stop_time || !b.started_today() ? (b = $.extend({}, b.data), delete b.duration, delete b.start, delete b.stop, delete b.id) : b = b.data;
        b.ignore_start_and_stop = !a.logged_in_user.store_start_and_stop_time;
        window.parent.$("html, body").animate({
          scrollTop: "0px"
        }, 400);
        return a.switch_task(b, true)
      },
      loginerror: function () {
        return $("#loginform").trigger("reset").fadeTo(200, 1).find(".failed").html("<h2>Login Failed</h2>")
      },
      selected_tasks: function () {
        return $(".task > input:checkbox:checked").map(function () {
          return $(this).parent(".task").data("instance")
        })
      },
      hide_old_task_form: function () {
        var a, b;
        j.$task_form.is(":visible") && (b = j.$task_form.data("task"), a = $("#task_" + b.data.id), s.update_task_fragment(a, b, b.data.description));
        $(".task.hidden").removeClass("hidden");
        return j.$task_form.hide()
      },
      bulk_update: function (b, c, d) {
        return $.ajaxQueue.post("/api/" + a.api_version + "/bulk_updates/", {
          data: {
            name: b,
            ids: _.map(c, function (a) {
              return a.data.id
            })
          },
          success: function (b) {
            d && d(b);
            $("#accordion").find(".task input[type=checkbox]").attr("checked", false);
            s.update_task_list();
            a.store_current_data();
            return s.task_saved_success()
          }
        })
      },
      delete_selected_tasks: function () {
        var a, b;
        b = g.selected_tasks();
        if (b.length && (Admin.hide_more_actions(), a = b.length === 1 ? "Delete 1 time entry? This action cannot be reversed." : "Delete " + b.length + " time entries? This action cannot be reversed.", confirm(a))) return j.$task_form.is(":visible") && _.include(b, j.$task_form.data("task")) && (g.hide_old_task_form(), j.$task_select_task_checkbox.attr("checked", false)), _.each(b, function (a) {
          return a.destroy_without_request()
        }), g.bulk_update("delete", b)
      },
      mark_selected_tasks_as_billable: function (a) {
        var b, c;
        c = g.selected_tasks();
        if (c.length) return g.bulk_update(a ? "mark_as_billable" : "mark_as_nonbillable", c, function (b) {
          j.$task_form.is(":visible") && _.include(b, j.$task_form.data("task").data.id) && j.set_billable(a);
          return _.each(c, function (c) {
            if (_.include(b, c.data.id)) return c.data.billable = a, c.update_ui()
          })
        })
      },
      logindone: function () {
        $(".download-widget").bind("click", function () {
          return window.open($(this).find("a[rel=widgets]").attr("href"))
        });
        _.each([h.$task_form, $("#tasktimer-actions"), $("#tasktimer-report-panel")], function (a) {
          return $(a).removeClass("hidden")
        });
        d = new Tasklist(e, a.logged_in_user.time_entry_retention_days);
        $(document).trigger("refresh");
        window.parentSandboxBridge && window.parentSandboxBridge.user_logged_in && window.parentSandboxBridge.user_logged_in();
        $("#login").fadeOut(function () {
          return $(this).remove()
        });
        $("body").bind({
          ajaxSend: function () {
            var a;
            $("error-notify:visible").length || (a = b.offset().left + b.width() / 2 - c.width() / 2);
            return c.css({
              left: a
            }).removeClass("hidden")
          },
          ajaxStop: function () {
            return c.addClass("hidden")
          },
          ajaxError: function (a, b) {
            try {
              (!b || !(b.status === 404 && b.responseText === "Requested object not found")) && b && b.status === 402 && Admin.show_error_message(Translation["This workspace is suspended! Time entry is not saved!"])
            } catch (c) {
              return Admin.show_error_message(Translation["Time entry update failed. Please try again."])
            }
          }
        });
        $(document).bind("refresh", g.document_refresh);
        $("a[rel=refresh]").click(function () {
          return document.location.reload()
        });
        $(".opens-time-entry", e).live("click", g.task_list_click);
        $("a[rel=continue]").live("click", g.continue_task);
        $("#delete-selected-tasks").bind("click", g.delete_selected_tasks);
        $("#mark-selected-tasks-as-billable").bind("click", function () {
          return g.mark_selected_tasks_as_billable(true)
        });
        $("#mark-selected-tasks-as-nonbillable").bind("click", function () {
          return g.mark_selected_tasks_as_billable(false)
        });
        n.$task_form.find(".stop-running-task").live("click", g.toggle_time_tracked);
        _.each(u, function (b) {
          b.stop_tracking_interval = o;
          b.start_tracking_interval = p;
          b.hide_old_task_form = g.hide_old_task_form;
          b.initialize();
          $("body").bind("new_tag_received", b.new_tag_received);
          return b.$task_start_date.datepicker({
            firstDay: a.logged_in_user.beginning_of_week,
            dateFormat: a.logged_in_user.jquery_date_format,
            duration: ""
          })
        });
        h.submitted = function (a) {
          return a.is_tracking() ? s.edit_task(a) : $("#task_" + a.data.id).effect("highlight", {
            color: "#FFFF99"
          }, 2E3)
        };
        n.task_deleted = function () {
          h.$task_form.show();
          return h.$task_description.focus()
        };
        $("input[placeholder], textarea[placeholder]").placeholder();
        $(".inputbox > input[type=text]").bind("focus", function () {
          return $(this).parent(".inputbox").addClass("focused")
        }).bind("focusout", function () {
          return $(this).parent(".inputbox").removeClass("focused")
        });
        $.ajaxSetup({
          error: function (a) {
            a.status === 403 && $("#middle-notify").html("You need to re-authenticate yourself!<br><a href='javascript:window.location.reload()'>Click here to reload</a>").removeClass("hidden")
          }
        });
        $(this).unbind("logindone");
        return $(this).unbind("loginerror")
      },
      document_refresh: function () {
        return e.accordion("destroy")
      },
      new_project_submit: function (b) {
        return $.ajaxQueue.post("/api/" + a.api_version + "/projects", {
          data: {
            project: b
          },
          global: false,
          success: function (b) {
            b = b.data;
            a.projects_data.unshift($.extend(b, {
              value: b.client_project_name,
              label: b.client_project_name + " (" + b.workspace.name + ")"
            }));
            a.projects_data = _.sortBy(a.projects_data, function (a) {
              return a.value
            });
            g.populate_project_select();
            $("#new-project").data("form").select_project(b);
            $(document.body).trigger("dataready");
            return l.hide_dialog()
          },
          error: function (a) {
            return s.show_error($.parseJSON(a.responseText).join(", "))
          }
        })
      },
      toggle_time_tracked: function () {
        if (a.tracked_task && !n.$task_form.data("task").blocksave) {
          if (!a.tracked_task.toggle_time()) a.tracked_task = null;
          d.ensure_headers_visible();
          return false
        }
      },
      task_list_click: function () {
        var a;
        a = $(this).closest(".task").data("instance");
        return s.edit_task(a)
      },
      autocomplete_source_for_projects: function () {
        return _.map(a.projects_data, function (b) {
          return _(a.projects_data).chain().pluck("workspace").pluck("id").uniq().value().length === 1 ? $.extend(b, {
            value: b.client_project_name,
            label: b.client_project_name
          }) : $.extend(b, {
            value: b.client_project_name,
            label: b.client_project_name + " (" + b.workspace.name + ")"
          })
        })
      },
      populate_project_select: function () {
        var b, c;
        c = a.planned_tasks();
        b = OptgroupSelection.options_for_project_selection(a.workspaces_data, a.projects_data, null, false, false, true, c);
        return _.each(u, function (a) {
          var c;
          c = a.$task_project_name.val();
          a.$task_project_name.html(b);
          return a.$task_project_name.val(c || "")
        })
      }
    };
    this.task_saved_success = function () {
      m();
      return $(window).trigger("update_tasktimer_reports")
    };
    this.is_shown_task = function (a) {
      return j.$task_form.is(":visible") && j.$task_form.data("task") === a ? true : n.$task_form.is(":visible") && n.$task_form.data("task") === a ? true : false
    };
    this.edit_task = function (b) {
      var c, e, f, l, m;
      if (!b) throw "No task given";
      e = b.is_tracking() ? n : j;
      e.$title.text(b.is_tracking() ? "You are tracking time for" : "What are you working on?");
      e.$task_form.data("task", b);
      e.$task_form.toggleClass("ignore-start-and-stop", !! b.data.ignore_start_and_stop);
      b.update_ui();
      g.hide_old_task_form();
      c = $("#task_" + b.data.id);
      c.addClass("hidden");
      $(".task.hidden").removeClass("hidden");
      e.$task_select_task_checkbox.attr("checked", c.find("input:checkbox").is(":checked"));
      m = b.data;
      f = Date.parse8601(m.start);
      l = Date.parse8601(m.stop);
      e.$task_description.val(m.description || "");
      m.project && m.project.client_project_name ? OptgroupSelection.select_option_deprecated(e.$task_project_name, a.projects_data, a.planned_tasks(), m.project.id, m.planned_task ? m.planned_task.id : null, m.project.client_project_name) : OptgroupSelection.clear_selected_options(e.$task_project_name);
      e.show_view_project_link();
      e.set_planned_task(m.planned_task);
      e.set_billable(m.billable);
      e.$task_start_date.val(f.dateFormat(a.logged_in_user.jquery_date_format));
      e.$task_time_start.val(f.dateFormat(a.logged_in_user.jquery_timeofday_format));
      e.$task_time_stop.val(l.dateFormat(a.logged_in_user.jquery_timeofday_format));
      e.$task_duration_hhmmss.val(b.export_time());
      m.workspace && e.redraw_tags(m.workspace, m.tag_names);
      e.toggle_non_premium_workspace_form(m.workspace);
      c.addClass("hidden");
      b.is_tracking() ? h.$task_form.hide() : c.after(e.$task_form);
      d.ensure_headers_visible();
      e.$task_form.show();
      return e.$task_form.find(".task-description").focus()
    };
    this.update_task_time = function (b, c, d) {
      var e, f;
      if (!d) throw "Tasktimer needs a task to update_task_time!";
      e = null;
      (f = n.$task_form.data("task")) && f.data.id === d.data.id && (e = n);
      e || (f = j.$task_form.data("task")) && f.data.id === d.data.id && (e = j);
      if (e) return e.$task_time_start.val(b.dateFormat(a.logged_in_user.jquery_timeofday_format)), e.$task_time_stop.val(c.dateFormat(a.logged_in_user.jquery_timeofday_format))
    };
    this.show_tags = function () {};
    this.update_task_list = function () {
      f = false;
      return d.reinit()
    };
    this.add_task_to_list = function (a, b) {
      var c;
      c = Date.parse8601(a.data.start).dateFormat("Ymd");
      if (!$("#date_" + c).has(b[0]).length && (d.add_item(b, c), !f)) return d.clean()
    };
    this.handle_no_workspace = function () {
      var a;
      a = "<h1>" + Translation["You cannot use the Timer as you are not a member of any workspace."] + '</h1><a href="/workspace/new" target="_top">' + Translation["Create a new workspace"] + "</a>";
      a = $("#middle-notify").removeClass("hidden").html(a);
      return $("body").empty().prepend(a)
    };
    this.show_error = function (a) {
      return Admin.show_error_message(a)
    };
    this.show_refresh = function () {
      return $("#middle-notify").removeClass("hidden").html("No data available offline.<br><a href='javascript:window.location.reload()'>Click here to reload</a>")
    };
    this.end_idle = function (b) {
      var c, d;
      if (b && !isNaN(b.idle_start)) return d = (new Date).getTime(), c = (d - b.idle_start) / 1E3, $("#idle-away").text((new Date(b.idle_start)).dateFormat(a.logged_in_user.jquery_timeofday_format)), $("#idle-return").text((new Date(d)).dateFormat(a.logged_in_user.jquery_timeofday_format)), $("#idle-time").text(a.seconds_to_hhmmss(c)), $("#idleoverlay").children("a").unbind("click"), $("#idleoverlay").children("a").bind("click", function () {
        delete b.blocksave;
        delete b.idle_start;
        $(this).attr("rel") === "no" && (b.data.duration -= c, b.save());
        return $(this).parent().fadeOut()
      }), $("#idleoverlay").fadeIn()
    };
    this.format_task_start_and_stop = function (b) {
      return ("" + Date.parse8601(b.data.start).dateFormat(a.logged_in_user.jquery_timeofday_format) + "\u2013" + Date.parse8601(b.data.stop).dateFormat(a.logged_in_user.jquery_timeofday_format)).replace(/AM/g, "<sup class='timeofday'>AM</sup>").replace(/PM/g, "<sup class='timeofday'>PM</sup>")
    };
    this.format_client_project_name = function (a) {
      return a.data.project && a.data.project.client_project_name ? a.data.project.client_project_name : ""
    };
    this.create_task_tags_fragment = function (a) {
      return _(a.data.tag_names || []).map(function (a) {
        return '<span class="tag">' + a + "</span>"
      }).join("")
    };
    this.create_task_fragment = function (b, c) {
      var d, e, f, g;
      e = "";
      b.data.planned_task && (e = " - " + b.data.planned_task.name);
      g = this.create_task_tags_fragment(b);
      d = this.format_client_project_name(b);
      f = b.data.billable && a.is_premium_workspace(b.data.workspace);
      d = w({
        task_class: (b.is_tracking() ? "running-task" : "") + " " + (b.data.ignore_start_and_stop ? "ignore-start-and-stop" : ""),
        task_id: b.data.id,
        billable_class: f ? "" : "hidden",
        title: _.escapeHTML(c),
        tags: g,
        client_project_name: d,
        task_client_project_name: d + e,
        start_stop: this.format_task_start_and_stop(b)
      });
      return $(d).data("instance", b)
    };
    this.update_task_fragment = function (b, c, d) {
      var e, f, g, h, j, l;
      h = "";
      c.data.planned_task && c.data.planned_task.name && (h = " - " + c.data.planned_task.name);
      b.toggleClass("ignore-start-and-stop", !! c.data.ignore_start_and_stop);
      f = b.find(".project-name");
      g = b.find(".task-desc");
      e = b.find("span.billable");
      j = this.create_task_tags_fragment(c);
      l = (c.data.project ? c.data.project.client_project_name : "") + h;
      h = c.data.billable && a.is_premium_workspace(c.data.workspace);
      f.attr("title", l);
      f.text(l);
      g.attr("title", d);
      g.html(j + (_.escapeHTML(d) || "(no description)"));
      e.toggleClass("hidden", !h);
      return b.find("span.start-stop-container").html(this.format_task_start_and_stop(c))
    };
    this.start_tracking = function () {
      return p()
    };
    this.stop_tracking = function () {
      var b;
      if (b = $("#task_" + a.tracked_task.data.id)) return b.data("instance", a.tracked_task), b.removeClass("hidden"), b.removeClass("running-task"), b.effect("highlight", {
        color: "#FFFF99"
      }, 2E3), n.$task_form.hide(), h.$task_extra_fields.hide(), h.$task_form.show(), h.$task_description.focus(), o()
    };
    this.name = "tasktimer";
    $.browser.msie && $("select.wide").addClass("expand");
    $(g.docready);
    return this
  }
}).call(this);
(function () {
  togglWeb.ui.TimeEntryReportView = Backbone.View.extend({
    initialize: function (a) {
      var e;
      _.bindAll(this, "populate_project_select", "edit_task", "hide_old_task_form", "real_to_shown_duration");
      this.project_entry = new TogglProjectEntry;
      this.old_form = new TaskFormWithTask($("#old-task-form"), window.timer);
      this.$update_notify = $("#update-notify");
      this.$content = $("#content");
      this.name = "tasktimer";
      this.old_form.hide_old_task_form = this.hide_old_task_form;
      this.old_form.stop_tracking_interval = function () {};
      this.old_form.start_tracking_interval = function () {};
      this.old_form.initialize();
      $("body").bind("new_tag_received", this.old_form.new_tag_received);
      this.old_form.$task_start_date.datepicker({
        firstDay: timer.logged_in_user.beginning_of_week,
        dateFormat: timer.logged_in_user.jquery_date_format,
        duration: ""
      });
      $("body").bind({
        ajaxSend: function () {
          var a;
          if (!$("error-notify:visible").length) return a = $("#content").offset().left + $("#content").width() / 2 - $("#update-notify").width() / 2, $("#update-notify").css({
            left: a
          }).removeClass("hidden")
        },
        ajaxStop: function () {
          return $("#update-notify").addClass("hidden")
        }
      });
      $("input[placeholder], textarea[placeholder]").placeholder();
      $(".inputbox > input[type=text]").bind("focus", function () {
        return $(this).parent(".inputbox").addClass("focused")
      }).bind("focusout", function () {
        return $(this).parent(".inputbox").removeClass("focused")
      });
      e = this;
      $(".inline-edit-link").live("click", function () {
        var a;
        a = $(this).closest(".task").data("instance");
        return e.edit_task(a)
      });
      $.browser.msie && $("select.wide").addClass("expand");
      this.populate_project_select();
      _.each(window.timer.tasks_data, function (a) {
        var c;
        c = $("#task_" + a.id);
        if (c.length > 0) return c.data("instance", new Task(window.timer, {
          data: a
        }))
      });
      window.timer.tasks_data = $.merge(timer.tasks_data, _.map(a, function (a) {
        return {
          description: a.name,
          project: a.project,
          planned_task: a,
          workspace: a.workspace
        }
      }));
      this.old_form.init_task_autocomplete();
      return this.populate_project_select()
    },
    populate_project_select: function () {
      var a, e;
      a = timer.planned_tasks();
      a = OptgroupSelection.options_for_project_selection(timer.workspaces_data, timer.projects_data, timer.current_workspace_id, false, false, true, a);
      e = this.old_form.$task_project_name.val();
      this.old_form.$task_project_name.html(a);
      return this.old_form.$task_project_name.val(e || "")
    },
    update_task_fragment: function (a, e, b) {
      var c, d, f, g, h, j, l, n, m, p;
      n = "";
      e.data.planned_task && e.data.planned_task.name && (n = " - " + e.data.planned_task.name);
      a.toggleClass("ignore-start-and-stop", !! e.data.ignore_start_and_stop);
      h = a.find(".project-name");
      l = a.find(".task-desc");
      j = a.find(".task-tags-container");
      g = a.find(".task-duration.minutes");
      f = a.find(".task-duration.decimal");
      d = a.find(".task-date");
      c = a.find(".billable-amount");
      m = this.create_task_tags_fragment(e);
      if (e.data.start && e.data.stop && !e.data.ignore_start_and_stop) p = timer.logged_in_user.jquery_date_format, p = p.match(/^Y.*/) !== null ? p.substring(5, 2) : p.match(/.*%Y$/) !== 0 ? p.substring(0, 3) : p, d.html(Date.parse8601(e.data.start).dateFormat(p)), a.find(".task-start-stop").html(this.format_task_start_and_stop(e));
      e.data.billable === true ? (c.removeClass("hidden"), a = e.data.duration / 3600 * e.data.hourly_rate, c.html(Math.round(a).toFixed(2) + " " + e.data.currency)) : c.html("");
      c = (e.data.project ? e.data.project.client_project_name : "") + n;
      h.attr("title", c).text(c);
      l.attr("title", b).html(_.escapeHTML(b) || "(no description)");
      g.html(TimeFormat.seconds_to_hhmmss(this.real_to_shown_duration(e), true));
      f.html((e.data.duration / 3600).toFixed(2) + " h");
      return j.html(m)
    },
    hide_old_task_form: function () {
      var a, e;
      this.old_form.$task_form.is(":visible") && (e = this.old_form.$task_form.data("task"), a = $("#task_" + e.data.id), this.update_task_fragment(a, e, e.data.description));
      $(".task.hidden").removeClass("hidden");
      return this.old_form.$task_form.hide()
    },
    new_project_submit: function (a) {
      return $.ajaxQueue.post("/api/" + timer.api_version + "/projects", {
        data: {
          project: a
        },
        global: false,
        success: function (a) {
          a = a.data;
          timer.projects_data.unshift($.extend(a, {
            value: a.client_project_name,
            label: a.client_project_name + " (" + a.workspace.name + ")"
          }));
          timer.projects_data = _.sortBy(timer.projects_data, function (a) {
            return a.value
          });
          this.populate_project_select();
          $("#new-project").data("form").select_project(a);
          $(document.body).trigger("dataready");
          return project_entry.hide_dialog()
        },
        error: function (a) {
          return this.show_error($.parseJSON(a.responseText).join(", "))
        }
      })
    },
    autocomplete_source_for_projects: function () {
      return _.map(timer.projects_data, function (a) {
        return _(timer.projects_data).chain().pluck("workspace").pluck("id").uniq().value().length === 1 ? $.extend(a, {
          value: a.client_project_name,
          label: a.client_project_name
        }) : $.extend(a, {
          value: a.client_project_name,
          label: a.client_project_name + " (" + a.workspace.name + ")"
        })
      })
    },
    update_task_time: function (a, e) {
      this.old_form.$task_time_start.val(a.dateFormat(timer.logged_in_user.jquery_timeofday_format));
      return this.old_form.$task_time_stop.val(e.dateFormat(timer.logged_in_user.jquery_timeofday_format))
    },
    show_tags: function () {},
    is_shown_task: function (a) {
      return this.old_form.$task_form.is(":visible") && this.old_form.$task_form.data("task") === a ? true : false
    },
    add_task_to_list: function () {
      return true
    },
    edit_task: function (a) {
      var e, b, c, d, f, g, h;
      if (!a) throw "No task given";
      b = this.old_form;
      b.$task_form.data("task", a);
      b.$task_form.toggleClass("ignore-start-and-stop", !! a.data.ignore_start_and_stop);
      this.hide_old_task_form();
      e = $("#task_" + a.data.id);
      e.addClass("hidden");
      g = e.find(".user-name").html();
      $(".task.hidden").removeClass("hidden");
      b.$task_select_task_checkbox.attr("checked", e.find("input:checkbox").is(":checked"));
      f = a.data;
      c = Date.parse8601(f.start);
      d = Date.parse8601(f.stop);
      b.$task_description.val(f.description || "");
      f.project && f.project.client_project_name ? OptgroupSelection.select_option_deprecated(b.$task_project_name, timer.projects_data, timer.planned_tasks(), f.project.id, f.planned_task ? f.planned_task.id : null, f.project.client_project_name) : OptgroupSelection.clear_selected_options(b.$task_project_name);
      b.show_view_project_link();
      b.set_planned_task(f.planned_task);
      this.set_billable(f.billable);
      b.$task_start_date.val(c.dateFormat(timer.logged_in_user.jquery_date_format));
      b.$task_time_start.val(c.dateFormat(timer.logged_in_user.jquery_timeofday_format));
      b.$task_time_stop.val(d.dateFormat(timer.logged_in_user.jquery_timeofday_format));
      a = TimeFormat.seconds_to_hhmmss(this.real_to_shown_duration(a), true);
      b.$task_duration_hhmmss.val(a);
      a = timer.avatar_urls[f.user_id];
      f.workspace && b.redraw_tags(f.workspace, f.tag_names);
      b.toggle_non_premium_workspace_form(f.workspace);
      e.addClass("hidden");
      $(".edit-task-row").addClass("empty");
      b.$task_form.find(".profile-image").attr("src", a);
      f = $('<tr class="edit-task-row">').append($("<td colspan=5>").append(b.$task_form));
      e.after(f);
      $(".empty").remove();
      f.find(".task-user-name").html(g);
      this.set_tags_count();
      b.$task_form.show();
      b.$task_form.find(".task-description").focus();
      h = this;
      return b.$task_form.find(".tags-container .tags div").bind("click", function () {
        return h.set_tags_count()
      })
    },
    set_tags_count: function () {
      var a;
      a = this.old_form.$task_form.find(".tags").find(".all-checked").length;
      return this.old_form.$task_form.find(".tags-container").find(".nr-of-tags").html("(" + a + ")")
    },
    set_billable: function (a) {
      var e, b;
      b = this.old_form.$task_form.find(".billable-container");
      e = b.find("a[rel='billable']");
      b = b.find("a[rel='not-billable']");
      return a ? (e.addClass("pushed"), b.removeClass("pushed")) : (e.removeClass("pushed"), b.addClass("pushed"))
    },
    real_to_shown_duration: function (a) {
      var e, a = a.data.duration;
      return a < 0 ? (e = new Date, e.getTime() / 1E3 + a) : a
    },
    show_error: function (a) {
      return Admin.show_error_message(a)
    },
    show_refresh: function () {
      return $("#middle-notify").removeClass("hidden").html("No data available offline.<br><a href='javascript:window.location.reload()'>Click here to reload</a>")
    },
    format_task_start_and_stop: function (a) {
      return "" + Date.parse8601(a.data.start).dateFormat(timer.logged_in_user.jquery_timeofday_format) + "\u2013" + Date.parse8601(a.data.stop).dateFormat(timer.logged_in_user.jquery_timeofday_format)
    },
    format_client_project_name: function (a) {
      if (a.data.project) return a.data.project.client_project_name ? a.data.project.client_project_name : ""
    },
    create_task_tags_fragment: function (a) {
      return _(a.data.tag_names || []).map(function (a) {
        return '<span class="report-tag ui-corner-all">' + a + "</span>"
      }).join("")
    }
  })
}).call(this);
(function () {
  var a = {
    init: function () {
      this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
      this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "an unknown version";
      this.OS = this.searchString(this.dataOS) || "an unknown OS"
    },
    searchString: function (a) {
      for (var b = 0; b < a.length; b++) {
        var c = a[b].string,
          d = a[b].prop;
        this.versionSearchString = a[b].versionSearch || a[b].identity;
        if (c) {
          if (c.indexOf(a[b].subString) != -1) return a[b].identity
        } else if (d) return a[b].identity
      }
    },
    searchVersion: function (a) {
      var b = a.indexOf(this.versionSearchString);
      return b == -1 ? void 0 : parseFloat(a.substring(b + this.versionSearchString.length + 1))
    },
    dataBrowser: [{
      string: navigator.userAgent,
      subString: "Chrome",
      identity: "Chrome"
    }, {
      string: navigator.userAgent,
      subString: "OmniWeb",
      versionSearch: "OmniWeb/",
      identity: "OmniWeb"
    }, {
      string: navigator.vendor,
      subString: "Apple",
      identity: "Safari",
      versionSearch: "Version"
    }, {
      prop: window.opera,
      identity: "Opera"
    }, {
      string: navigator.vendor,
      subString: "iCab",
      identity: "iCab"
    }, {
      string: navigator.vendor,
      subString: "KDE",
      identity: "Konqueror"
    }, {
      string: navigator.userAgent,
      subString: "Firefox",
      identity: "Firefox"
    }, {
      string: navigator.vendor,
      subString: "Camino",
      identity: "Camino"
    }, {
      string: navigator.userAgent,
      subString: "Netscape",
      identity: "Netscape"
    }, {
      string: navigator.userAgent,
      subString: "MSIE",
      identity: "Explorer",
      versionSearch: "MSIE"
    }, {
      string: navigator.userAgent,
      subString: "Gecko",
      identity: "Mozilla",
      versionSearch: "rv"
    }, {
      string: navigator.userAgent,
      subString: "Mozilla",
      identity: "Netscape",
      versionSearch: "Mozilla"
    }],
    dataOS: [{
      string: navigator.platform,
      subString: "Win",
      identity: "Windows"
    }, {
      string: navigator.platform,
      subString: "Mac",
      identity: "Mac"
    }, {
      string: navigator.userAgent,
      subString: "iPhone",
      identity: "iPhone/iPod"
    }, {
      string: navigator.platform,
      subString: "Linux",
      identity: "Linux"
    }]
  };
  a.init();
  window.$.client = {
    os: a.OS,
    browser: a.browser,
    version: a.version
  }
})();

function badBrowser() {
  var a = $.client.browser,
    e = $.client.version;
  return "Explorer" == a && e < 8 ? true : "Opera" == a && e < 9.5 ? true : "Firefox" == a && e < 3.1 ? true : false
}

function getBadBrowser(a) {
  if (document.cookie.length > 0 && (c_start = document.cookie.indexOf(a + "="), c_start != -1)) {
    c_start = c_start + a.length + 1;
    c_end = document.cookie.indexOf(";", c_start);
    if (c_end == -1) c_end = document.cookie.length;
    return unescape(document.cookie.substring(c_start, c_end))
  }
  return ""
}
function setBadBrowser(a, e, b) {
  var c = new Date;
  c.setDate(c.getDate() + b);
  document.cookie = a + "=" + escape(e) + (b == null ? "" : ";expires=" + c.toGMTString())
}
badBrowser() && getBadBrowser("browserWarning") != "seen" && $(function () {
  $("<div id='browserWarning'>It seems that you are using an older browser that is not supported by Toggl. Please upgrade your browser or switch to a modern browser. Thanks!&nbsp;&nbsp;&nbsp;[<a href='#' id='warningClose'>Close</a>] </div> ").css({
    backgroundColor: "#fcfdde",
    width: "100%",
    "border-top": "solid 1px #000",
    "border-bottom": "solid 1px #000",
    "text-align": "center",
    padding: "5px 0px 5px 0px"
  }).prependTo("body");
  $("#warningClose").click(function () {
    setBadBrowser("browserWarning", "seen");
    $("#browserWarning").slideUp("slow");
    return false
  })
});
(function (a) {
  function e(a, b) {
    for (var c = []; b > 0; c[--b] = a);
    return c.join("")
  }
  var b = String.prototype.trim,
    c = /^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
    d = {
      isBlank: function (a) {
        return a == false
      },
      isEmail: function (a) {
        return !!a.match(c)
      },
      stripTags: function (a) {
        return a.replace(/<\/?[^>]+>/ig, "")
      },
      capitalize: function (a) {
        return a.charAt(0).toUpperCase() + a.substring(1).toLowerCase()
      },
      chop: function (a, b) {
        for (var b = b || a.length, c = [], d = 0; d < a.length;) c.push(a.slice(d, d + b)), d += b;
        return c
      },
      clean: function (a) {
        return d.strip(a.replace(/\s+/g, " "))
      },
      count: function (a, b) {
        for (var c = 0, d, e = 0; e < a.length;) d = a.indexOf(b, e), d >= 0 && c++, e = e + (d >= 0 ? d : 0) + b.length;
        return c
      },
      chars: function (a) {
        return a.split("")
      },
      escapeHTML: function (a) {
        return String(a || "").replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&apos;")
      },
      unescapeHTML: function (a) {
        return String(a || "").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&apos;/g, "'").replace(/&amp;/g, "&")
      },
      escapeRegExp: function (a) {
        return String(a || "").replace(/([-.*+?^${}()|[\]\/\\])/g, "\\$1")
      },
      insert: function (a, b, c) {
        a = a.split("");
        a.splice(b, 0, c);
        return a.join("")
      },
      includes: function (a, b) {
        return a.indexOf(b) !== -1
      },
      join: function () {
        var a = Array.prototype.slice.call(arguments);
        return a.join(a.shift())
      },
      lines: function (a) {
        return a.split("\n")
      },
      reverse: function (a) {
        return Array.prototype.reverse.apply(a.split("")).join("")
      },
      splice: function (a, b, c, d) {
        a = a.split("");
        a.splice(b, c, d);
        return a.join("")
      },
      startsWith: function (a, b) {
        return a.length >= b.length && a.substring(0, b.length) === b
      },
      endsWith: function (a, b) {
        return a.length >= b.length && a.substring(a.length - b.length) === b
      },
      succ: function (a) {
        var b = a.split("");
        b.splice(a.length - 1, 1, String.fromCharCode(a.charCodeAt(a.length - 1) + 1));
        return b.join("")
      },
      titleize: function (a) {
        for (var a = a.split(" "), b, c = 0; c < a.length; c++) b = a[c].split(""), typeof b[0] !== "undefined" && (b[0] = b[0].toUpperCase()), c + 1 === a.length ? a[c] = b.join("") : a[c] = b.join("") + " ";
        return a.join("")
      },
      camelize: function (a) {
        return d.trim(a).replace(/(\-|_|\s)+(.)?/g, function (a, b, c) {
          return c ? c.toUpperCase() : ""
        })
      },
      underscored: function (a) {
        return d.trim(a).replace(/([a-z\d])([A-Z]+)/g, "$1_$2").replace(/\-|\s+/g, "_").toLowerCase()
      },
      dasherize: function (a) {
        return d.trim(a).replace(/([a-z\d])([A-Z]+)/g, "$1-$2").replace(/^([A-Z]+)/, "-$1").replace(/\_|\s+/g, "-").toLowerCase()
      },
      trim: function (a, c) {
        if (!c && b) return b.call(a);
        c = c ? d.escapeRegExp(c) : "\\s";
        return a.replace(RegExp("^[" + c + "]+|[" + c + "]+$", "g"), "")
      },
      ltrim: function (a, b) {
        b = b ? d.escapeRegExp(b) : "\\s";
        return a.replace(RegExp("^[" + b + "]+", "g"), "")
      },
      rtrim: function (a, b) {
        b = b ? d.escapeRegExp(b) : "\\s";
        return a.replace(RegExp("[" + b + "]+$", "g"), "")
      },
      truncate: function (a, b, c) {
        return a.length > b ? a.slice(0, b) + (c || "...") : a
      },
      words: function (a, b) {
        return a.split(b || " ")
      },
      pad: function (a, b, c, d) {
        var l = "",
          l = 0;
        c ? c.length > 1 && (c = c[0]) : c = " ";
        switch (d) {
        case "right":
          l = b - a.length;
          l = e(c, l);
          a += l;
          break;
        case "both":
          l = b - a.length;
          l = {
            left: e(c, Math.ceil(l / 2)),
            right: e(c, Math.floor(l / 2))
          };
          a = l.left + a + l.right;
          break;
        default:
          l = b - a.length, l = e(c, l), a = l + a
        }
        return a
      },
      lpad: function (a, b, c) {
        return d.pad(a, b, c)
      },
      rpad: function (a, b, c) {
        return d.pad(a, b, c, "right")
      },
      lrpad: function (a, b, c) {
        return d.pad(a, b, c, "both")
      },
      sprintf: function () {
        for (var a = 0, b, c = arguments[a++], d = [], l, n, m; c;) {
          if (l = /^[^\x25]+/.exec(c)) d.push(l[0]);
          else if (l = /^\x25{2}/.exec(c)) d.push("%");
          else if (l = /^\x25(?:(\d+)\$)?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(c)) {
            if ((b = arguments[l[1] || a++]) == null || b == void 0) throw Error("Too few arguments.");
            if (/[^s]/.test(l[7]) && typeof b != "number") throw new TypeError("Expecting number but found " + typeof b);
            switch (l[7]) {
            case "b":
              b = b.toString(2);
              break;
            case "c":
              b = String.fromCharCode(b);
              break;
            case "d":
              b = parseInt(b);
              break;
            case "e":
              b = l[6] ? b.toExponential(l[6]) : b.toExponential();
              break;
            case "f":
              b = l[6] ? parseFloat(b).toFixed(l[6]) : parseFloat(b);
              break;
            case "o":
              b = b.toString(8);
              break;
            case "s":
              b = (b = String(b)) && l[6] ? b.substring(0, l[6]) : b;
              break;
            case "u":
              b = Math.abs(b);
              break;
            case "x":
              b = b.toString(16);
              break;
            case "X":
              b = b.toString(16).toUpperCase()
            }
            b = /[def]/.test(l[7]) && l[2] && b >= 0 ? "+" + b : b;
            n = l[3] ? l[3] == "0" ? "0" : l[3].charAt(1) : " ";
            m = l[5] - String(b).length - 0;
            n = l[5] ? e(n, m) : "";
            d.push("" + (l[4] ? b + n : n + b))
          } else throw Error("Universe error: 17");
          c = c.substring(l[0].length)
        }
        return d.join("")
      },
      toNumber: function (a, b) {
        return (a * 1 || 0).toFixed(b * 1 || 0) * 1 || 0
      },
      strRight: function (a, b) {
        var c = !b ? -1 : a.indexOf(b);
        return c != -1 ? a.slice(c + b.length, a.length) : a
      },
      strRightBack: function (a, b) {
        var c = !b ? -1 : a.lastIndexOf(b);
        return c != -1 ? a.slice(c + b.length, a.length) : a
      },
      strLeft: function (a, b) {
        var c = !b ? -1 : a.indexOf(b);
        return c != -1 ? a.slice(0, c) : a
      },
      strLeftBack: function (a, b) {
        var c = a.lastIndexOf(b);
        return c != -1 ? a.slice(0, c) : a
      }
    };
  d.strip = d.trim;
  d.lstrip = d.ltrim;
  d.rstrip = d.rtrim;
  d.center = d.lrpad;
  d.ljust = d.lpad;
  d.rjust = d.rpad;
  typeof window === "undefined" && typeof module !== "undefined" ? module.exports = d : typeof a._ !== "undefined" ? a._.mixin(d) : a._ = d
})(this || window);
(function (a) {
  var e = 0;
  a.widget("ech.multiselect", {
    options: {
      header: true,
      classes: "",
      height: true,
      checkAllText: "Check all",
      uncheckAllText: "Uncheck all",
      noneSelectedText: "Select options",
      selectedText: "# selected",
      selectedList: 0,
      show: "",
      hide: "",
      autoOpen: false,
      multiple: true,
      position: {}
    },
    _create: function () {
      var b = this.element.hide(),
        c = this.options;
      this.speed = a.fx.speeds._default;
      this._isOpen = false;
      b = (this.button = a('<button type="button"><span class="ui-icon ui-icon-triangle-2-n-s"></span></button>')).addClass("ui-multiselect ui-widget ui-state-default ui-corner-all").addClass(c.classes).attr({
        title: b.attr("title"),
        "aria-haspopup": true,
        tabIndex: b.attr("tabIndex")
      }).insertAfter(b);
      (this.buttonlabel = a("<span />")).html(c.noneSelectedText).appendTo(b);
      var b = (this.menu = a("<div />")).addClass("ui-multiselect-menu ui-widget ui-widget-content ui-corner-all").addClass(c.classes).insertAfter(b),
        d = (this.header = a("<div />")).addClass("ui-widget-header ui-corner-all ui-multiselect-header ui-helper-clearfix").appendTo(b);
      (this.headerLinkContainer = a("<ul />")).addClass("ui-helper-reset").html(function () {
        return c.header === true ? '<li><a class="ui-multiselect-all" href="#"><span class="ui-icon ui-icon-check"></span><span>' + c.checkAllText + '</span></a></li><li><a class="ui-multiselect-none" href="#"><span class="ui-icon ui-icon-closethick"></span><span>' + c.uncheckAllText + "</span></a></li>" : typeof c.header === "string" ? "<li>" + c.header + "</li>" : ""
      }).append('<li class="ui-multiselect-close"><a href="#" class="ui-multiselect-close"><span class="ui-icon ui-icon-circle-close"></span></a></li>').appendTo(d);
      (this.checkboxContainer = a("<ul />")).addClass("ui-multiselect-checkboxes ui-helper-reset").appendTo(b);
      this._bindEvents();
      this.refresh(true);
      c.multiple || b.addClass("ui-multiselect-single")
    },
    _init: function () {
      this.options.header === false && this.header.hide();
      this.options.multiple || this.headerLinkContainer.find(".ui-multiselect-all, .ui-multiselect-none").hide();
      this.options.autoOpen && this.open();
      this.element.is(":disabled") && this.disable()
    },
    refresh: function (b) {
      var c = this.options,
        d = this.menu,
        f = this.checkboxContainer,
        g = [],
        h = [],
        j = this.element.attr("id") || e++;
      this.element.find("option").each(function (b) {
        a(this);
        var d = this.parentNode,
          e = this.innerHTML,
          f = this.title,
          o = this.value,
          b = this.id || "ui-multiselect-" + j + "-option-" + b,
          u = this.disabled,
          w = this.getAttribute("class"),
          s = this.selected,
          D = ["ui-corner-all"];
        d.tagName.toLowerCase() === "optgroup" && (d = d.getAttribute("label"), a.inArray(d, g) === -1 && (h.push('<li class="ui-multiselect-optgroup-label"><a href="#">' + d + "</a></li>"), g.push(d)));
        u && D.push("ui-state-disabled");
        s && !c.multiple && D.push("ui-state-active");
        h.push('<li class="' + (u ? "ui-multiselect-disabled" : "") + (w ? " " + w : "") + '">');
        h.push('<label for="' + b + '" title="' + f + '" class="' + D.join(" ") + '">');
        h.push('<input id="' + b + '" name="multiselect_' + j + '" type="' + (c.multiple ? "checkbox" : "radio") + '" value="' + o + '" title="' + e + '"');
        s && (h.push(' checked="checked"'), h.push(' aria-selected="true"'));
        u && (h.push(' disabled="disabled"'), h.push(' aria-disabled="true"'));
        h.push(" /><span>" + e + "</span></label></li>")
      });
      f.html(h.join(""));
      this.labels = d.find("label");
      this._setButtonWidth();
      this._setMenuWidth();
      this.button[0].defaultValue = this.update();
      b || this._trigger("refresh")
    },
    update: function () {
      var b = this.options,
        c = this.labels.find("input"),
        d = c.filter(":checked"),
        e = d.length,
        b = e === 0 ? b.noneSelectedText : a.isFunction(b.selectedText) ? b.selectedText.call(this, e, c.length, d.get()) : /\d/.test(b.selectedList) && b.selectedList > 0 && e <= b.selectedList ? d.map(function () {
          return this.title
        }).get().join(", ") : b.selectedText.replace("#", e).replace("#", c.length);
      this.buttonlabel.html(b);
      return b
    },
    _bindEvents: function () {
      function b() {
        c[c._isOpen ? "close" : "open"]();
        return false
      }
      var c = this,
        d = this.button;
      d.find("span").bind("click.multiselect", b);
      d.bind({
        click: b,
        keypress: function (a) {
          switch (a.which) {
          case 27:
          case 38:
          case 37:
            c.close();
            break;
          case 39:
          case 40:
            c.open()
          }
        },
        mouseenter: function () {
          d.hasClass("ui-state-disabled") || a(this).addClass("ui-state-hover")
        },
        mouseleave: function () {
          a(this).removeClass("ui-state-hover")
        },
        focus: function () {
          d.hasClass("ui-state-disabled") || a(this).addClass("ui-state-focus")
        },
        blur: function () {
          a(this).removeClass("ui-state-focus")
        }
      });
      this.header.delegate("a", "click.multiselect", function (b) {
        if (a(this).hasClass("ui-multiselect-close")) c.close();
        else c[a(this).hasClass("ui-multiselect-all") ? "checkAll" : "uncheckAll"]();
        b.preventDefault()
      });
      this.menu.delegate("li.ui-multiselect-optgroup-label a", "click.multiselect", function (b) {
        b.preventDefault();
        var d = a(this),
          e = d.parent().nextUntil("li.ui-multiselect-optgroup-label").find("input:visible:not(:disabled)"),
          j = e.get(),
          d = d.parent().text();
        c._trigger("beforeoptgrouptoggle", b, {
          inputs: j,
          label: d
        }) !== false && (c._toggleChecked(e.filter(":checked").length !== e.length, e), c._trigger("optgrouptoggle", b, {
          inputs: j,
          label: d,
          checked: j[0].checked
        }))
      }).delegate("label", "mouseenter.multiselect", function () {
        a(this).hasClass("ui-state-disabled") || (c.labels.removeClass("ui-state-hover"), a(this).addClass("ui-state-hover").find("input").focus())
      }).delegate("label", "keydown.multiselect", function (b) {
        b.preventDefault();
        switch (b.which) {
        case 9:
        case 27:
          c.close();
          break;
        case 38:
        case 40:
        case 37:
        case 39:
          c._traverse(b.which, this);
          break;
        case 13:
          a(this).find("input")[0].click()
        }
      }).delegate('input[type="checkbox"], input[type="radio"]', "click.multiselect", function (b) {
        var d = a(this),
          e = this.value,
          j = this.checked,
          l = c.element.find("option");
        this.disabled || c._trigger("click", b, {
          value: e,
          text: this.title,
          checked: j
        }) === false ? b.preventDefault() : (d.attr("aria-selected", j), l.each(function () {
          if (this.value === e)(this.selected = j) ? this.setAttribute("selected", "selected") : this.removeAttribute("selected");
          else if (!c.options.multiple) this.selected = false
        }), c.options.multiple || (c.labels.removeClass("ui-state-active"), d.closest("label").toggleClass("ui-state-active", j), c.close()), c.element.trigger("change"), setTimeout(a.proxy(c.update, c), 10))
      });
      a(document).bind("mousedown.multiselect", function (b) {
        c._isOpen && !a.contains(c.menu[0], b.target) && !a.contains(c.button[0], b.target) && b.target !== c.button[0] && c.close()
      });
      a(this.element[0].form).bind("reset.multiselect", function () {
        setTimeout(function () {
          c.update()
        }, 10)
      })
    },
    _setButtonWidth: function () {
      var a = this.element.outerWidth(),
        c = this.options;
      if (/\d/.test(c.minWidth) && a < c.minWidth) a = c.minWidth;
      this.button.width(a)
    },
    _setMenuWidth: function () {
      var a = this.menu,
        c = this.button.outerWidth() - parseInt(a.css("padding-left"), 10) - parseInt(a.css("padding-right"), 10) - parseInt(a.css("border-right-width"), 10) - parseInt(a.css("border-left-width"), 10);
      a.width(c || this.button.outerWidth())
    },
    _traverse: function (b, c) {
      var d = a(c),
        e = b === 38 || b === 37,
        d = d.parent()[e ? "prevAll" : "nextAll"]("li:not(.ui-multiselect-disabled, .ui-multiselect-optgroup-label)")[e ? "last" : "first"]();
      d.length ? d.find("label").trigger("mouseover") : (d = this.menu.find("ul:last"), this.menu.find("label")[e ? "last" : "first"]().trigger("mouseover"), d.scrollTop(e ? d.height() : 0))
    },
    _toggleCheckbox: function (a, c) {
      return function () {
        !this.disabled && (this[a] = c);
        c ? this.setAttribute("aria-selected", true) : this.removeAttribute("aria-selected")
      }
    },
    _toggleChecked: function (b, c) {
      var d = c && c.length ? c : this.labels.find("input"),
        e = this;
      d.each(this._toggleCheckbox("checked", b));
      this.update();
      var g = d.map(function () {
        return this.value
      }).get();
      this.element.find("option").each(function () {
        !this.disabled && a.inArray(this.value, g) > -1 && e._toggleCheckbox("selected", b).call(this)
      });
      d.length && this.element.trigger("change")
    },
    _toggleDisabled: function (a) {
      this.button.attr({
        disabled: a,
        "aria-disabled": a
      })[a ? "addClass" : "removeClass"]("ui-state-disabled");
      this.menu.find("input").attr({
        disabled: a,
        "aria-disabled": a
      }).parent()[a ? "addClass" : "removeClass"]("ui-state-disabled");
      this.element.attr({
        disabled: a,
        "aria-disabled": a
      })
    },
    open: function () {
      var b = this.button,
        c = this.menu,
        d = this.speed,
        e = this.options;
      if (!(this._trigger("beforeopen") === false || b.hasClass("ui-state-disabled") || this._isOpen)) {
        var g = c.find("ul:last"),
          h = e.show,
          j = b.position();
        a.isArray(e.show) && (h = e.show[0], d = e.show[1] || this.speed);
        g.scrollTop(0).height(e.height);
        a.ui.position && !a.isEmptyObject(e.position) ? (e.position.of = e.position.of || b, c.show().position(e.position).hide().show(h, d)) : c.css({
          top: j.top + b.outerHeight(),
          left: j.left
        }).show(h, d);
        this.labels.eq(0).trigger("mouseover").trigger("mouseenter").find("input").trigger("focus");
        b.addClass("ui-state-active");
        this._isOpen = true;
        this._trigger("open")
      }
    },
    close: function () {
      if (this._trigger("beforeclose") !== false) {
        var b = this.options,
          c = b.hide,
          d = this.speed;
        a.isArray(b.hide) && (c = b.hide[0], d = b.hide[1] || this.speed);
        this.menu.hide(c, d);
        this.button.removeClass("ui-state-active").trigger("blur").trigger("mouseleave");
        this._isOpen = false;
        this._trigger("close")
      }
    },
    enable: function () {
      this._toggleDisabled(false)
    },
    disable: function () {
      this._toggleDisabled(true)
    },
    checkAll: function () {
      this._toggleChecked(true);
      this._trigger("checkAll")
    },
    uncheckAll: function () {
      this._toggleChecked(false);
      this._trigger("uncheckAll")
    },
    getChecked: function () {
      return this.menu.find("input").filter(":checked")
    },
    destroy: function () {
      a.Widget.prototype.destroy.call(this);
      this.button.remove();
      this.menu.remove();
      this.element.show();
      return this
    },
    isOpen: function () {
      return this._isOpen
    },
    widget: function () {
      return this.menu
    },
    _setOption: function (b, c) {
      var d = this.menu;
      switch (b) {
      case "header":
        d.find("div.ui-multiselect-header")[c ? "show" : "hide"]();
        break;
      case "checkAllText":
        d.find("a.ui-multiselect-all span").eq(-1).text(c);
        break;
      case "uncheckAllText":
        d.find("a.ui-multiselect-none span").eq(-1).text(c);
        break;
      case "height":
        d.find("ul:last").height(parseInt(c, 10));
        break;
      case "minWidth":
        this.options[b] = parseInt(c, 10);
        this._setButtonWidth();
        this._setMenuWidth();
        break;
      case "selectedText":
      case "selectedList":
      case "noneSelectedText":
        this.options[b] = c;
        this.update();
        break;
      case "classes":
        d.add(this.button).removeClass(this.options.classes).addClass(c)
      }
      a.Widget.prototype._setOption.apply(this, arguments)
    }
  })
})(jQuery);
(function (a) {
  var e = /[\-\[\]{}()*+?.,\\^$|#\s]/g;
  a.widget("ech.multiselectfilter", {
    options: {
      label: "Filter:",
      width: null,
      placeholder: "Enter keywords"
    },
    _create: function () {
      var f;
      var b = this,
        c = this.options,
        d = this.instance = a(this.element).data("multiselect");
      this.header = d.menu.find(".ui-multiselect-header").addClass("ui-multiselect-hasfilter");
      f = this.wrapper = a('<div class="ui-multiselect-filter">' + (c.label.length ? c.label : "") + '<input placeholder="' + c.placeholder + '" type="search"' + (/\d/.test(c.width) ? 'style="width:' + c.width + 'px"' : "") + " /></div>").prependTo(this.header), c = f;
      this.inputs = d.menu.find('input[type="checkbox"], input[type="radio"]');
      this.input = c.find("input").bind({
        keydown: function (a) {
          a.which === 13 && a.preventDefault()
        },
        keyup: a.proxy(b._handler, b),
        click: a.proxy(b._handler, b)
      });
      this.updateCache();
      d._toggleChecked = function (c, d) {
        var e = d && d.length ? d : this.labels.find("input"),
          j = this,
          e = e.not(b.instance._isOpen ? ":disabled, :hidden" : ":disabled").each(this._toggleCheckbox("checked", c));
        this.update();
        var l = e.map(function () {
          return this.value
        }).get();
        this.element.find("option").filter(function () {
          !this.disabled && a.inArray(this.value, l) > -1 && j._toggleCheckbox("selected", c).call(this)
        })
      };
      a(document).bind("multiselectrefresh", function () {
        b.updateCache();
        b._handler()
      })
    },
    _handler: function (b) {
      var c = a.trim(this.input[0].value.toLowerCase()),
        d = this.rows,
        f = this.inputs,
        g = this.cache;
      if (c) {
        d.hide();
        var h = RegExp(c.replace(e, "\\$&"), "gi");
        this._trigger("filter", b, a.map(g, function (a, b) {
          return a.search(h) !== -1 ? (d.eq(b).show(), f.get(b)) : null
        }))
      } else d.show();
      this.instance.menu.find(".ui-multiselect-optgroup-label").each(function () {
        var b = a(this);
        b[b.nextUntil(".ui-multiselect-optgroup-label").filter(":visible").length ? "show" : "hide"]()
      })
    },
    updateCache: function () {
      this.rows = this.instance.menu.find(".ui-multiselect-checkboxes li:not(.ui-multiselect-optgroup-label)");
      this.cache = this.element.children().map(function () {
        var b = a(this);
        this.tagName.toLowerCase() === "optgroup" && (b = b.children());
        return b.map(function () {
          return this.innerHTML.toLowerCase()
        }).get()
      }).get()
    },
    widget: function () {
      return this.wrapper
    },
    destroy: function () {
      a.Widget.prototype.destroy.call(this);
      this.input.val("").trigger("keyup");
      this.wrapper.remove()
    }
  })
})(jQuery);
(function () {
  var a;
  a = {
    closeActiveAndRenderTasks: function (a) {
      return $.post("/api/v7/me/close_active_time_entry", {
        _method: "PUT",
        user: {
          manual_mode: a
        }
      }, function () {
        return window.location = "/tasks"
      })
    },
    stopRunning: function () {
      if ($(".stop-running-task").length > 0) return $(".stop-running-task").click()
    }
  };
  key("s", function () {
    return window.location.pathname === "/tasks" ? (a.stopRunning(), false) : a.closeActiveAndRenderTasks(false)
  });
  key("n", function () {
    var e;
    return window.location.pathname === "/tasks" ? (a.stopRunning(), $("a[rel=timer-mode]").click(), $("form#new-task-form").submit(), false) : (e = $("#user_current_workspace").val(), $.post("/api/v7/me/start_new_time_entry", {
      _method: "PUT",
      workspace_id: e
    }, function () {
      return window.location = "/tasks"
    }))
  });
  key("m", function () {
    return window.location.pathname === "/tasks" ? (a.stopRunning(), $("a[rel=manual-mode]").click(), $("form#new-task-form .task-description").focus(), false) : a.closeActiveAndRenderTasks(true)
  });
  key("c", function () {
    return window.location.pathname === "/tasks" ? ($(".task:not(.running-task) .continue-task a[rel=continue]").first().click(), false) : $.post("/api/v7/me/continue_last_time_entry", {
      _method: "PUT"
    }, function () {
      return window.location = "/tasks"
    })
  });
  $(document).keypress(function (a) {
    if (a.which === 63 && document.activeElement.type === void 0) return $(".qtip-active").size() === 0 ? $(".shortcuts").click() : $(".shortcuts").qtip("hide")
  })
}).call(this);
(function () {
  togglWeb.model.Tag = Backbone.Model.extend({
    parse: function (a) {
      return {
        id: a.data.id,
        name: a.data.name
      }
    }
  });
  togglWeb.model.TagList = Backbone.Collection.extend({
    model: togglWeb.model.Tag,
    url: "/api/v6/tags"
  });
  togglWeb.Tags = new togglWeb.model.TagList
}).call(this);
(function () {
  togglWeb.ui.TagView = Backbone.View.extend({
    events: {
      "click a[rel=edit]": "showEdit",
      "click a[rel=cancel]": "hideEdit",
      "click a[rel=delete]": "clear",
      "keypress input": "updateOnEnter"
    },
    initialize: function () {
      _.bindAll(this, "render", "updateOnEnter", "close");
      this.template = _.template($("#tag-template").html());
      return this.model.bind("change", this.render, this)
    },
    render: function () {
      $(this.el).html(this.template(this.model.toJSON()));
      return this
    },
    updateOnEnter: function (a) {
      if (a.keyCode === 13) return this.close()
    },
    close: function () {
      return this.model.save({
        name: $("input:first", this.el).val()
      })
    },
    showEdit: function () {
      $("span", this.el).addClass("hidden");
      $("input, a[rel=cancel]", this.el).removeClass("hidden");
      return false
    },
    hideEdit: function () {
      $("span", this.el).removeClass("hidden");
      $("input, a[rel=cancel]", this.el).addClass("hidden");
      return false
    },
    clear: function () {
      confirm($("a[rel=delete]", this.el).attr("alt")) && ($(this.el).fadeOut(), this.model.destroy());
      return false
    }
  })
}).call(this);
(function () {
  togglWeb.ui.TagListView = Backbone.View.extend({
    el: $("#tags-container"),
    events: {
      "click #new_tag": "createNew"
    },
    initialize: function () {
      _.bindAll(this, "render", "appendItem");
      togglWeb.Tags.bind("reset", this.render);
      return togglWeb.Tags.bind("add", this.appendItem)
    },
    render: function () {
      togglWeb.Tags.each(this.appendItem);
      return this
    },
    appendItem: function (a) {
      a = new togglWeb.ui.TagView({
        model: a
      });
      return $(".tags", this.el).append(a.render().el)
    },
    createNew: function () {
      togglWeb.Tags.create({
        name: this.$("#tag_name").val(),
        workspace_id: this.$("#tag_workspace_id").val()
      }, {
        success: function () {
          return $("#tag_name").removeAttr("disabled").val("")
        },
        wait: true
      });
      this.$("#tag_name").attr("disabled", "disabled");
      return false
    }
  })
}).call(this);
(function () {
  togglWeb.model.Task = Backbone.Model.extend({
    fields: "active, ufirstname, uid, percent_done, estimated_sec, id, pid, name, actual_time, ufullname, created",
    parse: function (a) {
      return a.data
    },
    estimated_time: function () {
      return this.humanize(this.get("estimated_sec"))
    },
    actual_time_in_human: function () {
      return this.humanize(this.get("actual_time") || 0)
    },
    progress_description: function () {
      return this.estimation_present() ? this.get("actual_time") > 0 ? "" + this.actual_time_in_human() + " of " + this.estimated_time() : "" + this.estimated_time() + " to GO" : this.actual_time_in_human()
    },
    is_active: function () {
      return this.get("active") ? "Yes" : ""
    },
    estimation_present: function () {
      return !(this.get("estimated_sec") === 0 || this.get("estimated_sec") === null)
    },
    toJSON: function () {
      if (this.fields != null) this.attributes.fields = this.fields;
      return this.attributes
    },
    humanize: function (a) {
      var e, b;
      b = "";
      e = Math.floor(a / 3600);
      a = Math.floor(a / 60 % 60);
      e > 0 && (b += "" + e + "h ");
      a > 0 && (b += "" + a + "min ");
      return b
    }
  });
  togglWeb.model.TaskList = Backbone.Collection.extend({
    model: togglWeb.model.Task,
    url: "/api/v7/planned_tasks",
    comparator: function (a) {
      return -a.get("id")
    },
    done: function () {
      return this.without.apply(this, this.undone())
    },
    undone: function () {
      return this.filter(function (a) {
        return a.get("active")
      })
    }
  });
  togglWeb.Tasks = new togglWeb.model.TaskList
}).call(this);
(function () {
  togglWeb.ui.TaskView = Backbone.View.extend({
    tagName: "tr",
    className: "task-view-row",
    events: {
      "click a[rel=delete]": "clear",
      "click a[rel=edit]": "showEdit",
      "click a[rel=done]": "markDone",
      "click a[rel=activate]": "markActive",
      removeEditing: "hideEdit",
      "submit #planned_task_edit": "updateItem",
      mouseenter: "showLinks",
      mouseleave: "hideLinks",
      "focusout #existing_task_estimated_seconds_hhmmss": "calculateEstimated"
    },
    initialize: function () {
      _.bindAll(this, "render", "updateItem", "hideEdit", "calculateEstimated", "showLinks", "hideLinks", "removeItem", "markDone", "markActive", "clearOnEdit", "toggleDoneOnEdit");
      this.model.bind("change", this.render, this);
      this.model.bind("remove", this.removeItem);
      return this.template = _.template($("#task-template").html())
    },
    render: function () {
      var a;
      a = _.extend(this.model.toJSON(), {
        active: this.model.is_active(),
        user: this.model.get("ufullname"),
        actual_time_in_human: this.model.actual_time_in_human(),
        estimated_time: this.model.estimated_time(),
        progress_description: this.model.progress_description(),
        percent_done: this.model.get("percent_done") || 0,
        heavy: this.model.get("percent_done") > 100 ? "heavy" : "",
        show_overflow: this.model.get("percent_done") > 100 ? "overflow" : "",
        show_progress: this.model.estimation_present() ? "" : "invisible",
        show_active_buttons: this.model.is_active() ? "" : "hidden",
        show_undo_button: this.model.get("active") ? "hidden" : "",
        task_done: this.model.get("active") ? "" : "done-task",
        text_color: this.model.get("active") ? "" : "text-light"
      });
      $(this.el).html(this.template(a));
      a = $(this.el).find(".progressbar").attr("rel");
      $(this.el).find(".progressbar").progressbar({
        value: ~~a
      });
      return this
    },
    calculateEstimated: function () {
      return Admin.update_hidden_duration_in_seconds($("#existing_task_estimated_seconds_hhmmss"), $("#existing_task_estimated_seconds"))
    },
    showEdit: function () {
      var a;
      $("#planned-tasks .editing").trigger("removeEditing");
      this.populateEditRow();
      a = $("#planned-task-editcell").removeClass("hidden");
      $("a[rel=cancel]", a).bind("click", this.hideEdit);
      $("a[rel=delete]", a).unbind("click").bind("click", this.clearOnEdit);
      $("input.item-toggle-done", a).unbind("click").bind("click", this.toggleDoneOnEdit);
      $("form", a).unbind("submit").bind("submit", this.updateItem);
      $("#existing_task_estimated_seconds_hhmmss").bind("focusout", this.calculateEstimated);
      $(this.el).addClass("hidden editing").after(a);
      return false
    },
    hideEdit: function () {
      $(this.el).removeClass("hidden editing");
      return $("#planned-task-editcell").addClass("hidden")
    },
    populateEditRow: function () {
      $("#existing_task_estimated_seconds_hhmmss").val(Admin.milliseconds_to_hhmmss(this.model.get("estimated_sec") * 1E3));
      $("#existing_task_estimated_seconds").val(this.model.get("estimated_sec"));
      $("#existing_task_name").val(this.model.get("name"));
      $("#existing_task_is_active").attr("checked", this.model.get("active"));
      $("#existing_task_user_id").val(this.model.get("uid"));
      $("input.item-toggle-done").removeClass("hidden");
      return this.model.get("active") ? $("input.item-undo").addClass("hidden") : $("input.item-done").addClass("hidden")
    },
    updateItem: function () {
      this.calculateEstimated();
      $("#planned-tasks a[rel=cancel]").click();
      this.model.save({
        task: {
          name: $("#existing_task_name").val(),
          estimated_sec: $("#existing_task_estimated_seconds").val(),
          pid: $("#existing_task_project_id").val(),
          uid: $("#existing_task_user_id").val()
        }
      });
      return false
    },
    markDone: function () {
      return this.updateTaskActivity("false")
    },
    markActive: function () {
      return this.updateTaskActivity("true")
    },
    updateTaskActivity: function (a) {
      this.model.save({
        task: {
          active: a,
          pid: $("#existing_task_project_id").val(),
          name: this.model.get("name")
        }
      });
      return false
    },
    toggleDoneOnEdit: function () {
      $("#planned-tasks a[rel=cancel]").click();
      return this.updateTaskActivity(this.model.get("active") ? "false" : "true")
    },
    showLinks: function () {
      return this.$(".row-links").removeClass("hidden")
    },
    hideLinks: function () {
      return this.$(".row-links").addClass("hidden")
    },
    clear: function () {
      confirm($("a[rel=delete]", this.el).attr("alt")) && ($(this.el).fadeOut(), this.model.destroy());
      return false
    },
    clearOnEdit: function () {
      confirm($("a[rel=delete]", this.el).attr("alt")) && ($("#planned-tasks a[rel=cancel]").click(), this.model.destroy(), this.remove());
      return false
    },
    removeItem: function () {
      return this.remove()
    }
  })
}).call(this);
(function () {
  togglWeb.ui.TaskListView = Backbone.View.extend({
    el: $("#task_list"),
    page: 1,
    per_page: 30,
    events: {
      "submit #planned-task-create": "createItem",
      "click input.remove-items": "removeItems",
      "click input.mark-items-done": "markDone",
      "click input.activate-items": "markActive",
      "click a[rel=show-all]": "showAll",
      "click a[rel=show-active]": "showActive",
      "click .pagination a": "changePage",
      "change tbody input[type=checkbox]": "toggleActions",
      "change thead input[type=checkbox]": "toggleSelected"
    },
    initialize: function () {
      _.bindAll(this, "render", "appendItem", "setModelAttributes");
      this.disableMassActions();
      this.showAllTasks = false;
      togglWeb.Tasks.bind("reset", this.render);
      return togglWeb.Tasks.bind("add", this.appendItem)
    },
    render: function () {
      var a, e, b = this;
      a = 0;
      e = Math.ceil(this.tasks().length / this.per_page);
      if (this.page > e) this.page = 1;
      _.each(this.tasks(), function (c) {
        b.visibleTask(e, a) && b.appendItem(c);
        return a++
      });
      this.renderPagination(e);
      return this
    },
    unrender: function () {
      $("#planned-tasks tbody", this.el).html("");
      return this
    },
    renderPagination: function (a) {
      var e, b;
      if (a > 1) {
        b = this.page === 1 ? '<span class="disabled prev_page" rel="prev">\u00ab Previous</span> ' : '<a href="#prev" class="prev_page" rel="prev">\u00ab Previous</a> ';
        for (e = 1; 1 <= a ? e <= a : e >= a; 1 <= a ? e++ : e--) b += e === this.page ? '<span class="current">' + e + "</span> " : '<a href="#' + e + '">' + e + "</a> ";
        b += this.page === a ? '<span class="disabled next_page" rel="next">Next \u00bb</span>' : '<a href="#next" class="next_page" rel="next">Next \u00bb</a>';
        return $(".pagination").empty().append(b).removeClass("hidden")
      } else return $(".pagination").addClass("hidden")
    },
    visibleTask: function (a, e) {
      return this.page === a ? e >= (this.page - 1) * this.per_page : e >= (this.page - 1) * this.per_page && e < this.per_page * this.page
    },
    tasks: function () {
      return this.showAllTasks ? togglWeb.Tasks.models : togglWeb.Tasks.undone()
    },
    showAll: function () {
      this.toggleTasks(true);
      return false
    },
    showActive: function () {
      this.toggleTasks(false);
      return false
    },
    changePage: function (a) {
      a.stopPropagation();
      a = $(a.currentTarget);
      a.hasClass("next_page") ? (a = $(".pagination *").length - 2, this.page !== a && this.page++) : a.hasClass("prev_page") ? this.page > 1 && this.page-- : this.page = parseInt(a.text(), 10);
      this.unrender().render();
      return false
    },
    toggleTasks: function (a) {
      this.showAllTasks = a;
      this.unrender().render();
      return this.$("a[rel=show-all], a[rel=show-active]").toggleClass("active")
    },
    appendItem: function (a) {
      a = new togglWeb.ui.TaskView({
        model: a
      });
      return $("#planned-tasks", this.el).append(a.render().el)
    },
    calculateEstimated: function () {
      return Admin.update_hidden_duration_in_seconds($("#planned_task_estimated_seconds_hhmmss"), $("#planned_task_estimated_seconds"))
    },
    createItem: function (a) {
      a.stopPropagation();
      a = this.$("#planned_task_is_active").length > 0 ? this.$("#planned_task_is_active").is(":checked") : true;
      this.calculateEstimated();
      a = {
        task: {
          name: this.$("#planned_task_name").val(),
          active: a,
          estimated_sec: this.$("#planned_task_estimated_seconds").val(),
          uid: this.$("#planned_task_user_id").val(),
          pid: this.$("#planned_task_project_id").val()
        }
      };
      togglWeb.Tasks.create(a, {
        wait: true,
        error: function (a, b) {
          return Admin.show_error_message($.parseJSON(b.responseText)[0])
        }
      });
      this.$("#planned-task-create")[0].reset();
      this.$("#planned_task_name").focus();
      return false
    },
    selectedTasks: function () {
      return this.$("tbody :checkbox:checked").map(function () {
        return $(this).val()
      }).get().join(",")
    },
    disableMassActions: function () {
      return this.$(".button-list input[type=submit]").addClass("disabled").attr("disabled", "disabled")
    },
    removeSelected: function () {
      var a;
      a = this.$("tbody :checkbox:checked").map(function () {
        return $(this).val()
      });
      return _.each(a, function (a) {
        a = togglWeb.Tasks.get(a);
        return togglWeb.Tasks.remove(a)
      })
    },
    toggleSelected: function () {
      var a;
      a = this.$("thead input[type=checkbox]").is(":checked");
      this.$("tbody input[type=checkbox]").attr("checked", a);
      return this.toggleActions()
    },
    toggleActions: function () {
      return this.$("tbody :checkbox:checked").length > 0 ? this.$(".button-list input").removeClass("disabled").removeAttr("disabled") : this.$(".button-list input").addClass("disabled").attr("disabled", "disabled")
    },
    resetMassSelection: function () {
      return this.$("thead input[type=checkbox]").attr("checked", false)
    },
    removeItems: function () {
      var a;
      event.preventDefault();
      a = this.selectedTasks();
      this.disableMassActions();
      this.removeSelected();
      this.resetMassSelection();
      $.post("/api/v7/planned_tasks/" + a, {
        _method: "delete"
      });
      return false
    },
    setModelAttributes: function (a, e) {
      return togglWeb.Tasks.get(a).set(e)
    },
    markActive: function () {
      return this.updateTaskState(true)
    },
    updateTaskState: function (a) {
      var e, b;
      event.preventDefault();
      e = this;
      b = "/api/v7/planned_tasks/" + this.selectedTasks();
      $.ajax({
        type: "post",
        data: {
          _method: "PUT",
          task: {
            active: a ? "true" : "false"
          }
        },
        url: b,
        success: function (b) {
          console.log(b);
          $.isArray(b.data) ? (e.resetMassSelection(), _.each(b.data, function (b) {
            return e.setModelAttributes(b.id, {
              active: a
            })
          })) : e.setModelAttributes(b.data.id, {
            active: a
          });
          return e.unrender().render()
        },
        error: function (a) {
          return Admin.show_error_message($.parseJSON(a.responseText))
        }
      });
      return false
    },
    markDone: function () {
      return this.updateTaskState(false)
    }
  })
}).call(this);
(function () {
  togglWeb.model.ProjectUser = Backbone.Model.extend({
    fields: "pid, wid, created, fullname, uid, id, role, rate, manager",
    parse: function (a) {
      return a.data
    },
    toJSON: function () {
      if (this.fields != null) this.attributes.fields = this.fields;
      return this.attributes
    }
  });
  togglWeb.model.ProjectUserList = Backbone.Collection.extend({
    model: togglWeb.model.ProjectUser,
    url: "/api/v7/project_users",
    comparator: function (a) {
      return a.get("fullname")
    }
  });
  togglWeb.ProjectUsers = new togglWeb.model.ProjectUserList
}).call(this);
(function () {
  togglWeb.ui.ProjectUserView = Backbone.View.extend({
    tagName: "tr",
    events: function () {
      return {
        mouseenter: "showLinks",
        mouseleave: "hideLinks",
        "click a[rel=delete]": "clear",
        "click .set-role": "updateRole",
        "click td.rate-cell": "openEdit",
        "blur input.rate-input": "saveRate",
        "keypress input.rate-input": "updateOnEnter"
      }
    },
    initialize: function () {
      _.bindAll(this, "render", "openEdit", "saveRate", "updateRole", "removeItem");
      this.model.bind("change", this.render, this);
      this.model.bind("remove", this.removeItem);
      return this.template = _.template($("#project-user-template").html())
    },
    render: function () {
      var a;
      a = _.extend(this.model.toJSON(), {
        rate_desc: this.model.get("rate") || "click to set",
        role_class: this.model.get("manager") === true ? "Manager" : "User"
      });
      $(this.el).html(this.template(a));
      return this
    },
    clear: function () {
      $(this.el).remove();
      togglWeb.WorkspaceUsers.add(this.model);
      this.model.destroy();
      return false
    },
    openEdit: function () {
      this.$("span.hourly-rate").addClass("hidden");
      return this.$("input.rate-input").removeClass("hidden").focus()
    },
    updateOnEnter: function (a) {
      if (a.keyCode === 13) return this.$("input.rate-input").blur()
    },
    showLinks: function () {
      this.$(".row-links").removeClass("hidden");
      return false
    },
    hideLinks: function () {
      this.$(".row-links").addClass("hidden");
      return false
    },
    saveRate: function () {
      this.$("input.rate-input, span.hourly-rate").toggleClass("hidden");
      this.model.save({
        project_user: {
          hourly_rate: this.$("input.rate-input").val() || 0
        }
      });
      return false
    },
    updateRole: function () {
      this.model.save({
        project_user: {
          manager: this.model.get("manager") ? 0 : 1
        }
      });
      return false
    },
    removeItem: function () {
      return this.remove()
    }
  })
}).call(this);
(function () {
  togglWeb.ui.ProjectUserListView = Backbone.View.extend({
    el: $("#project_user_list"),
    events: {
      "click input.set-rate": "setRate",
      "keypress input#rate": "updateOnEnter",
      "click input.remove-items": "removeItems",
      "change tbody input[type=checkbox]": "toggleActions",
      "change thead input[type=checkbox]": "toggleSelected"
    },
    initialize: function () {
      _.bindAll(this, "render", "appendItem");
      this.toggleActions();
      togglWeb.ProjectUsers.bind("reset", this.render);
      return togglWeb.ProjectUsers.bind("add", this.appendItem)
    },
    render: function () {
      togglWeb.ProjectUsers.each(this.appendItem);
      return this
    },
    appendItem: function (a) {
      a = new togglWeb.ui.ProjectUserView({
        model: a
      });
      return this.$("tbody").prepend(a.render().el)
    },
    selectedUsers: function () {
      return this.$("tbody :checkbox:checked").map(function () {
        return $(this).val()
      }).get().join(",")
    },
    setRate: function () {
      var a, e;
      event.preventDefault();
      a = this.$("input#rate").val();
      this.$("input#rate").val("").blur();
      e = "/api/v7/project_users/" + this.selectedUsers();
      $.post(e, {
        _method: "PUT",
        project_user: {
          hourly_rate: a
        }
      }, function (a) {
        return _.each(a.data, function (a) {
          return togglWeb.ProjectUsers.get(a.id).set({
            rate: a.rate
          })
        })
      });
      this.resetMassSelection();
      return false
    },
    updateOnEnter: function (a) {
      if (a.keyCode === 13) return this.setRate(), false
    },
    removeSelected: function () {
      var a;
      a = this.$("tbody :checkbox:checked").map(function () {
        return $(this).val()
      });
      return _.each(a, function (a) {
        a = togglWeb.ProjectUsers.get(a);
        togglWeb.ProjectUsers.remove(a);
        return a.remove
      })
    },
    removeItems: function () {
      var a;
      a = "/api/v7/project_users/" + this.selectedUsers();
      $.post(a, {
        _method: "delete"
      });
      this.resetMassSelection();
      this.removeSelected();
      return false
    },
    resetMassSelection: function () {
      return this.$("thead input[type=checkbox]").attr("checked", false)
    },
    toggleSelected: function () {
      var a;
      a = this.$("thead input[type=checkbox]").is(":checked");
      this.$("tbody input[type=checkbox]").attr("checked", a);
      return this.toggleActions()
    },
    toggleActions: function () {
      return this.$("tbody :checkbox:checked").length > 0 ? this.$(".button-list input").removeClass("disabled").removeAttr("disabled") : this.$(".button-list input").addClass("disabled").attr("disabled", "disabled")
    }
  })
}).call(this);
(function () {
  togglWeb.model.WorkspaceUser = Backbone.Model.extend({
    fields: "pid, wid, fullname, uid, id, admin",
    parse: function (a) {
      return a.data
    },
    toJSON: function () {
      if (this.fields != null) this.attributes.fields = this.fields;
      return this.attributes
    }
  });
  togglWeb.model.WorkspaceUserList = Backbone.Collection.extend({
    model: togglWeb.model.WorkspaceUser,
    url: "/api/v7/workspace_users",
    comparator: function (a) {
      return a.get("fullname")
    }
  });
  togglWeb.WorkspaceUsers = new togglWeb.model.WorkspaceUserList
}).call(this);
(function () {
  togglWeb.ui.WorkspaceUserView = Backbone.View.extend({
    tagName: "li",
    events: function () {
      return {
        "click a[rel=delete]": "clear"
      }
    },
    initialize: function () {
      _.bindAll(this, "render");
      this.model.bind("change", this.render, this);
      return this.template = _.template($("#workspace-user-template").html())
    },
    render: function () {
      var a;
      a = this.model.toJSON();
      $(this.el).html(this.template(a));
      return this
    },
    clear: function () {
      confirm($("a[rel=delete]", this.el).attr("alt")) && ($(this.el).fadeOut(), this.model.destroy());
      return false
    }
  })
}).call(this);
(function () {
  togglWeb.ui.WorkspaceUserListView = Backbone.View.extend({
    el: $("#workspace_user_list"),
    events: {
      submit: "createItem",
      "click .hide-list": "hideList",
      "change input[type=checkbox]": "toggleMassActionButtons"
    },
    initialize: function () {
      _.bindAll(this, "render", "appendItem");
      this.disableMassActions();
      togglWeb.WorkspaceUsers.bind("reset", this.render);
      togglWeb.WorkspaceUsers.bind("add", this.appendItem);
      return togglWeb.ProjectUsers.bind("remove", this.appendItem)
    },
    render: function () {
      togglWeb.WorkspaceUsers.each(this.appendItem);
      return this
    },
    appendItem: function (a) {
      a = new togglWeb.ui.WorkspaceUserView({
        model: a
      });
      return this.$("ul").prepend(a.render().el)
    },
    selectedUsers: function () {
      return this.$(":checkbox:checked").map(function () {
        return $(this).val()
      }).get().join(",")
    },
    removeSelected: function () {
      return this.$(":checkbox:checked").parent().remove()
    },
    createItem: function (a) {
      a.preventDefault();
      this.disableMassActions();
      a = {
        project_id: $("#project_id").val(),
        user_id: this.selectedUsers()
      };
      this.removeSelected();
      $.post(togglWeb.ProjectUsers.url, a, function (a) {
        return togglWeb.ProjectUsers.add(a.data)
      });
      this.hideList();
      return false
    },
    hideList: function () {
      $("#project_user_list").show();
      $("#workspace_user_list").hide();
      return false
    },
    disableMassActions: function () {
      return this.$(".button-list input[type=submit]").addClass("disabled").attr("disabled", "disabled")
    },
    toggleMassActionButtons: function () {
      return this.$(":checkbox:checked").first().is(":checked") ? this.$(".button-list input[type=submit]").removeClass("disabled").removeAttr("disabled") : this.disableMassActions()
    }
  })
}).call(this);
(function () {
  togglWeb.ui.ProjectView = Backbone.View.extend({
    el: "#project-view",
    events: {
      "change #project_sharing": "updateSharing",
      "click .add-members": "showWorkspaceUsers",
      "click .enable-sharing": "enableSharing",
      "click .disable-sharing": "disableSharing",
      "click a[rel=archive]": "toggleArchive",
      "click .template-toggle": "toggleTemplate",
      "click a[rel=auto-estimate]": "disableEstimates",
      "click a[rel=total-estimate]": "enableEstimates"
    },
    showWorkspaceUsers: function () {
      $("#project_user_list").hide();
      $("#workspace_user_list").show();
      return false
    },
    disableEstimates: function () {
      $("#calculate_estimated_workhours").attr("checked", false);
      $(".estimation-enabled, .estimation-disabled").toggleClass("hidden");
      return false
    },
    enableEstimates: function () {
      $("#calculate_estimated_workhours").attr("checked", true);
      $(".estimation-enabled, .estimation-disabled").toggleClass("hidden");
      return false
    },
    enableSharing: function () {
      this.updateSharing(false);
      return false
    },
    disableSharing: function () {
      this.updateSharing(true);
      return false
    },
    toggleArchive: function () {
      togglWeb.Project.save({
        id: togglWeb.Project.get("project_id"),
        project: {
          is_active: !togglWeb.Project.get("is_active")
        }
      }, {
        success: function () {
          return document.location.href = "/projects"
        }
      });
      return false
    },
    toggleTemplate: function () {
      var a;
      a = this.$("#project_template").is(":checked");
      this.$("#project_template").attr("checked", !a);
      this.$(".template-toggle").toggleClass("pushed", !a);
      return togglWeb.Project.save({
        id: togglWeb.Project.get("project_id"),
        project: {
          template: !a
        }
      })
    },
    updateSharing: function (a) {
      var e;
      e = "/api/v7/projects/" + this.$("#project_id").val();
      $.post(e, {
        _method: "put",
        project: {
          is_private: a
        }
      }, function (a) {
        return a.data.is_private ? ($(".shared-project").addClass("hidden"), $(".team-members").removeClass("hidden")) : ($(".team-members").addClass("hidden"), $(".shared-project").removeClass("hidden"))
      });
      return false
    }
  })
}).call(this);
(function () {
  togglWeb.ui.MemberInvitationView = Backbone.View.extend({
    el: "#project-invite",
    events: {
      submit: "addMember"
    },
    addMember: function () {
      var a;
      event.preventDefault();
      a = {
        project_id: $("#project_id").val(),
        invitation_emails: this.$("#invitation_emails").val(),
        fields: "pid, wid, created, fullname, uid, id, role, rate, manager"
      };
      $.ajax({
        method: "POST",
        url: "/api/v7/project_users/",
        data: a,
        success: function (a) {
          togglWeb.ProjectUsers.add(a.data);
          return $("#invitation_emails").val("")
        },
        error: function (a) {
          return Admin.show_error_message(a.response)
        }
      });
      $("#project_user_list").show();
      $("#workspace_user_list").hide();
      return false
    }
  })
}).call(this);
(function () {
  togglWeb.model.Note = Backbone.Model.extend({
    urlRoot: "/api/v7/notes",
    parse: function (a) {
      return a.data
    }
  });
  togglWeb.model.NoteList = Backbone.Collection.extend({
    model: togglWeb.model.Note,
    url: "/api/v7/notes"
  });
  togglWeb.Notes = new togglWeb.model.NoteList
}).call(this);
(function () {
  togglWeb.ui.NoteView = Backbone.View.extend({
    tagName: "tr",
    className: "pad",
    events: {
      "click a[rel=edit]": "editItem",
      "click a[rel=cancel]": "closeItem",
      "click a[rel=delete]": "confirmRemove",
      "submit form": "updateItem"
    },
    initialize: function () {
      _.bindAll(this, "render", "removeItem");
      this.template = _.template($("#note-template").html());
      this.model.bind("change", this.render, this);
      return this.model.bind("remove", this.removeItem)
    },
    render: function () {
      $(this.el).html(this.template(this.model.toJSON()));
      return this
    },
    editItem: function () {
      $(this.el).addClass("editing");
      this.$("textarea").focus();
      return false
    },
    updateItem: function () {
      this.model.set({
        content: this.$("textarea").val()
      });
      this.closeItem();
      this.model.save({
        note: {
          content: this.model.get("content")
        }
      });
      return false
    },
    closeItem: function () {
      $(this.el).removeClass("editing");
      return false
    },
    removeItem: function () {
      $(this.el).remove();
      return this.model.destroy()
    },
    confirmRemove: function () {
      confirm($("a[rel=delete]", this.el).attr("alt")) && this.removeItem();
      return false
    }
  })
}).call(this);
(function () {
  togglWeb.ui.NoteListView = Backbone.View.extend({
    el: "div.notes",
    events: {
      "submit .first-item form": "createItem",
      "click input.remove-items": "removeItems",
      "change tbody input[type=checkbox]": "toggleActions",
      "change thead input[type=checkbox]": "toggleSelected"
    },
    initialize: function () {
      _.bindAll(this, "render", "appendItem");
      togglWeb.Notes.bind("reset", this.render);
      return togglWeb.Notes.bind("add", this.appendItem)
    },
    render: function () {
      togglWeb.Notes.each(this.appendItem);
      return this
    },
    appendItem: function (a) {
      a = new togglWeb.ui.NoteView({
        model: a
      });
      return $("#note_list tbody", this.el).prepend(a.render().el)
    },
    removeItems: function () {
      var a, e, b, c, d;
      c = this.selectedUsers();
      d = [];
      for (e = 0, b = c.length; e < b; e++) a = c[e], a = togglWeb.Notes.get(a), d.push(togglWeb.Notes.remove(a));
      return d
    },
    selectedUsers: function () {
      return this.$("tbody :checkbox:checked").map(function () {
        return $(this).val()
      })
    },
    clearForm: function () {
      this.$(".first-item form")[0].reset();
      return this.$(".first-item textarea").focus()
    },
    toggleSelected: function () {
      var a;
      a = this.$("thead input[type=checkbox]").is(":checked");
      this.$("tbody input[type=checkbox]").attr("checked", a);
      return this.toggleActions()
    },
    toggleActions: function () {
      return this.$("tbody :checkbox:checked").length > 0 ? this.$(".button-list input").removeClass("disabled").removeAttr("disabled") : this.$(".button-list input").addClass("disabled").attr("disabled", "disabled")
    },
    createItem: function () {
      var a;
      this.$(".first-item textarea").val() === "" ? this.$(".first-item textarea").focus() : (a = {
        note: {
          created: new Date,
          notification: this.$(":checkbox").is(":checked"),
          content: this.$(".first-item textarea").val(),
          pid: this.$(".first-item input.pid").val(),
          wid: this.$(".first-item input.wid").val(),
          uname: this.$(".first-item input.username").val()
        }
      }, togglWeb.Notes.create(a, {
        wait: true
      }), this.clearForm());
      return false
    }
  })
}).call(this);
(function () {
  togglWeb.model.Project = Backbone.Model.extend({
    urlRoot: "/api/v7/projects",
    parse: function (a) {
      return a.data
    }
  });
  togglWeb.Project = new togglWeb.model.Project
}).call(this);
(function () {
  togglWeb.model.Client = Backbone.Model.extend({
    fields: "notes, id, name, hrate, cur",
    parse: function (a) {
      return a.data
    },
    toJSON: function () {
      if (this.fields != null) this.attributes.fields = this.fields;
      return this.attributes
    }
  });
  togglWeb.model.ClientList = Backbone.Collection.extend({
    model: togglWeb.model.Client,
    url: "/api/v7/clients",
    comparator: function (a) {
      return -a.get("name")
    }
  });
  togglWeb.Clients = new togglWeb.model.ClientList
}).call(this);
(function () {
  togglWeb.ui.ClientView = Backbone.View.extend({
    tagName: "tr",
    className: "client-list-row",
    events: {
      "click a[rel=delete]": "clear",
      mouseenter: "showLinks",
      mouseleave: "hideLinks",
      "click a[rel=edit]": "showEdit",
      removeEditing: "hideEdit",
      "submit #client_edit": "updateItem"
    },
    initialize: function () {
      _.bindAll(this, "render", "showLinks", "hideLinks", "updateItem", "hideEdit");
      this.template = _.template($("#client-template").html());
      return this.model.bind("change", this.render, this)
    },
    render: function () {
      var a;
      a = _.extend(this.model.toJSON(), {
        defaults: !this.model.get("hrate") || this.model.get("hrate") < 0 ? "" : "hidden",
        own_rate: this.model.get("hrate") && this.model.get("hrate") > 0 ? "" : "hidden"
      });
      $(this.el).html(this.template(a));
      return this
    },
    showLinks: function () {
      return this.$(".row-links").removeClass("hidden")
    },
    hideLinks: function () {
      return this.$(".row-links").addClass("hidden")
    },
    showEdit: function () {
      var a;
      $("#clients .editing").trigger("removeEditing");
      this.populateEditRow();
      a = $("#client-editcell").removeClass("hidden");
      $("a[rel=cancel]", a).bind("click", this.hideEdit);
      $("a[rel=delete]", a).unbind("click").bind("click", this.clearOnEdit);
      $("form", a).unbind("submit").bind("submit", this.updateItem);
      $(this.el).addClass("hidden editing").after(a);
      return false
    },
    populateEditRow: function () {
      $("#existing_client_name").val(this.model.get("name"));
      $("#existing_client_notes").val(this.model.get("notes"));
      $("#existing_client_hourly_rate").val(this.model.get("hrate"));
      return $("#existing_client_currency").val(this.model.get("cur"))
    },
    hideEdit: function () {
      $(this.el).removeClass("hidden editing");
      return $("#client-editcell").addClass("hidden")
    },
    updateItem: function () {
      $("#clients a[rel=cancel]").click();
      this.model.save({
        client: {
          name: $("#existing_client_name").val(),
          notes: $("#existing_client_notes").val(),
          cur: $("#existing_client_currency").val(),
          hrate: $("#existing_client_hourly_rate").val()
        }
      });
      return false
    },
    clear: function () {
      confirm($("a[rel=delete]", this.el).attr("alt")) && ($(this.el).fadeOut(), this.model.destroy());
      return false
    }
  })
}).call(this);
(function () {
  togglWeb.ui.ClientListView = Backbone.View.extend({
    el: $(".clients-list"),
    events: {
      "submit #new_client": "createItem"
    },
    initialize: function () {
      _.bindAll(this, "render", "appendItem");
      togglWeb.Clients.bind("reset", this.render);
      return togglWeb.Clients.bind("add", this.appendItem)
    },
    render: function () {
      togglWeb.Clients.each(this.appendItem);
      return this
    },
    appendItem: function (a) {
      a = new togglWeb.ui.ClientView({
        model: a
      });
      return $("#clients tbody").prepend(a.render().el)
    },
    createItem: function (a) {
      a.stopPropagation();
      $(".empty").remove();
      togglWeb.Clients.create({
        client: {
          name: this.$("#client_name").val()
        }
      }, {
        wait: true
      });
      this.$("#client_name").val("");
      this.$("#cancel-new-client").click();
      return false
    }
  })
}).call(this);
(function (a, e, b) {
  a.fn.borderWidth = function () {
    return a(this).outerWidth() - a(this).innerWidth()
  };
  a.fn.paddingWidth = function () {
    return a(this).innerWidth() - a(this).width()
  };
  a.fn.extraWidth = function () {
    return a(this).outerWidth(true) - a(this).width()
  };
  a.fn.offsetFrom = function (b) {
    b = a(b);
    return {
      left: a(this).offset().left - b.offset().left,
      top: a(this).offset().top - b.offset().top
    }
  };
  a.fn.maxWidth = function () {
    var b = 0;
    a(this).each(function () {
      a(this).width() > b && (b = a(this).width())
    });
    return b
  };
  a.fn.triggerAll = function (b, c) {
    return a(this).each(function () {
      a(this).triggerHandler(b, c)
    })
  };
  var c = Array.prototype.slice,
    d = function () {
      return Math.floor(Math.random() * 999999999)
    };
  a.proto = function (b, d, e) {
    var f = d,
      n = {};
    opts = a.extend({
      elem: "elem",
      access: "access",
      init: "init",
      instantAccess: false
    }, e);
    d._super && (n[opts.init] = function () {}, f = d.extend(n));
    a.fn[b] = function () {
      var e, j = arguments;
      a(this).each(function () {
        var n = a(this),
          u = n.data(b),
          w = !u;
        if (w) {
          u = new f;
          if (d._super) u[opts.init] = d.prototype.init;
          u[opts.elem] = n[0];
          u[opts.init] && u[opts.init].apply(u, opts.instantAccess ? [] : c.call(j, 0));
          n.data(b, u)
        }
        if (!w || opts.instantAccess) u[opts.access] && u[opts.access].apply(u, c.call(j, 0)), j.length > 0 ? a.isFunction(u[j[0]]) ? e = u[j[0]].apply(u, c.call(j, 1)) : j.length === 1 ? e = a.getObject ? a.getObject(j[0], u) : u[j[0]] : a.setObject ? a.setObject(j[0], j[1], u) : u[j[0]] = j[1] : e === void 0 && (e = n.data(b))
      });
      return e === void 0 ? a(this) : e
    }
  };
  var f = function () {
      return false
    };
  a.proto("sb", function () {
    var c = this,
      h = {},
      j = null,
      l = null,
      n = null,
      m = null,
      p = null,
      o = null,
      u = "",
      w = null,
      s = null,
      D = null,
      C, z, F, q, K, O, V, cb, Ha, H, sa, gb, k, va, Z, fa, Ea, P, Y, M, A, S, La, kb, E, ea, W, X, I, ua, ya, Fa, za, la, aa, Ra, hb;
    C = function () {
      n = a("<div class='sb " + h.selectboxClass + " " + j.attr("class") + "' id='sb" + d() + "'></div>").attr("role", "listbox").attr("aria-has-popup", "true").attr("aria-labelledby", l.attr("id") ? l.attr("id") : "");
      a("body").append(n);
      var b = j.children().size() > 0 ? h.displayFormat.call(j.find("option:selected")[0], 0, 0) : "&nbsp;";
      m = a("<div class='display " + j.attr("class") + "' id='sbd" + d() + "'></div>").append(a("<div class='text'></div>").append(b)).append(h.arrowMarkup);
      n.append(m);
      p = a("<ul class='" + h.selectboxClass + " items " + j.attr("class") + "' role='menu' id='sbdd" + d() + "'></ul>").attr("aria-hidden", "true");
      n.append(p).attr("aria-owns", p.attr("id"));
      j.children().size() === 0 ? p.append(z().addClass("selected")) : j.children().each(function (b) {
        var c, d, e, f;
        a(this).is("optgroup") ? (d = a(this), e = a("<li class='optgroup'>" + h.optgroupFormat.call(d[0], b + 1) + "</li>").addClass(d.is(":disabled") ? "disabled" : "").attr("aria-disabled", d.is(":disabled") ? "true" : ""), f = a("<ul class='items'></ul>"), e.append(f), p.append(e), d.children("option").each(function () {
          c = z(a(this), b).addClass(d.is(":disabled") ? "disabled" : "").attr("aria-disabled", d.is(":disabled") ? "true" : "");
          f.append(c)
        })) : p.append(z(a(this), b))
      });
      o = p.find("li").not(".optgroup");
      n.attr("aria-active-descendant", o.filter(".selected").attr("id"));
      p.children(":first").addClass("first");
      p.children(":last").addClass("last");
      h.fixedWidth ? h.maxWidth && n.width() > h.maxWidth && n.width(h.maxWidth) : (b = p.find(".text, .optgroup").maxWidth() + m.extraWidth() + 1, n.width(h.maxWidth ? Math.min(h.maxWidth, b) : b));
      j.before(n).addClass("has_sb").hide().show();
      sa();
      Ra();
      p.hide();
      if (j.is(":disabled")) n.addClass("disabled").attr("aria-disabled"), m.click(function (a) {
        a.preventDefault()
      });
      else if (j.bind("blur.sb", q).bind("focus.sb", F), m.mouseup(S).mouseup(va).click(f).focus(P).blur(Y).hover(M, A), ea().click(Z).hover(M, A), p.find(".optgroup").hover(M, A).click(f), o.filter(".disabled").click(f), !a.browser.msie || a.browser.version >= 9) a(e).resize(a.throttle ? a.throttle(100, gb) : k);
      n.bind("close.sb", H).bind("destroy.sb", K);
      j.bind("reload.sb", O);
      a.fn.tie && h.useTie && j.bind("domupdate.sb", V)
    };
    k = function () {
      clearTimeout(D);
      D = setTimeout(gb, 50)
    };
    gb = function () {
      n.is(".open") && (sa(), cb(true))
    };
    z = function (b, c) {
      b || (b = a("<option value=''>&nbsp;</option>"), c = 0);
      var e = a("<li id='sbo" + d() + "'></li>").attr("role", "option").data("orig", b[0]).data("value", b ? b.attr("value") : "").addClass(b.is(":selected") ? "selected" : "").addClass(b.is(":disabled") ? "disabled" : "").attr("aria-disabled", b.is(":disabled") ? "true" : ""),
        f = a("<div class='item'></div>"),
        g = a("<div class='text'></div>").html(h.optionFormat.call(b[0], 0, c + 1));
      return e.append(f.append(g))
    };
    F = function () {
      la();
      m.triggerHandler("focus")
    };
    q = function () {
      n.is(".open") || m.triggerHandler("blur")
    };
    K = function (b) {
      n.remove();
      j.unbind(".sb").removeClass("has_sb");
      a(e).unbind("resize", k);
      b || j.removeData("sb")
    };
    O = function () {
      var a = n.is(".open"),
        b = m.is(".focused");
      H(true);
      K(true);
      c.init(h);
      a ? (j.focus(), cb(true)) : b && j.focus()
    };
    V = function () {
      clearTimeout(s);
      s = setTimeout(O, 30)
    };
    za = function () {
      n.removeClass("focused");
      H();
      hb()
    };
    hb = function () {
      a(document).unbind("click", za).unbind("keyup", fa).unbind("keypress", aa).unbind("keydown", aa).unbind("keydown", Ea)
    };
    la = function () {
      a(".sb.focused." + h.selectboxClass).not(n[0]).find(".display").blur()
    };
    Fa = function () {
      a(".sb.open." + h.selectboxClass).not(n[0]).triggerAll("close")
    };
    H = function (a) {
      n.is(".open") && (m.blur(), o.removeClass("hover"), hb(), p.attr("aria-hidden", "true"), a === true ? (p.hide(), n.removeClass("open"), n.append(p)) : p.fadeOut(h.animDuration, function () {
        n.removeClass("open");
        n.append(p)
      }))
    };
    kb = function () {
      var b = null;
      return b = h.ddCtx === "self" ? n : a.isFunction(h.ddCtx) ? a(h.ddCtx.call(j[0])) : a(h.ddCtx)
    };
    E = function () {
      return o.filter(".selected")
    };
    ea = function () {
      return o.not(".disabled")
    };
    Ha = function () {
      p.scrollTop(p.scrollTop() + E().offsetFrom(p).top - p.height() / 2 + E().outerHeight(true) / 2)
    };
    Ra = function () {
      a.browser.msie && a.browser.version < 8 && a("." + h.selectboxClass + " .display").hide().show()
    };
    cb = function (a) {
      var b;
      b = kb();
      la();
      n.addClass("open");
      b.append(p);
      b = sa();
      p.attr("aria-hidden", "false");
      a === true ? (p.show(), Ha()) : b === "down" ? p.slideDown(h.animDuration, Ha) : p.fadeIn(h.animDuration, Ha);
      j.focus()
    };
    sa = function () {
      var b = kb(),
        c = 0,
        d = m.offsetFrom(b).left,
        f = 0,
        g = "",
        j, k;
      p.removeClass("above");
      p.show().css({
        maxHeight: "none",
        position: "relative",
        visibility: "hidden"
      });
      h.fixedWidth || p.width(m.outerWidth() - p.extraWidth() + 1);
      c = a(e).scrollTop() + a(e).height() - m.offset().top - m.outerHeight();
      g = m.offset().top - a(e).scrollTop();
      f = m.offsetFrom(b).top + m.outerHeight();
      k = c - g + h.dropupThreshold;
      p.outerHeight() < c ? (c = h.maxHeight ? h.maxHeight : c, g = "down") : p.outerHeight() < g ? (c = h.maxHeight ? h.maxHeight : g, f = m.offsetFrom(b).top - Math.min(c, p.outerHeight()), g = "up") : k >= 0 ? (c = h.maxHeight ? h.maxHeight : c, g = "down") : k < 0 ? (c = h.maxHeight ? h.maxHeight : g, f = m.offsetFrom(b).top - Math.min(c, p.outerHeight()), g = "up") : (c = h.maxHeight ? h.maxHeight : "none", g = "down");
      j = ("" + a("body").css("margin-left")).match(/^\d+/) ? a("body").css("margin-left") : 0;
      k = ("" + a("body").css("margin-top")).match(/^\d+/) ? a("body").css("margin-top") : 0;
      j = a().jquery >= "1.4.2" ? parseInt(j) : a("body").offset().left;
      k = a().jquery >= "1.4.2" ? parseInt(k) : a("body").offset().top;
      p.hide().css({
        left: d + (b.is("body") ? j : 0),
        maxHeight: c,
        position: "absolute",
        top: f + (b.is("body") ? k : 0),
        visibility: "visible"
      });
      g === "up" && p.addClass("above");
      return g
    };
    va = function () {
      n.is(".open") ? H() : cb();
      return false
    };
    W = function () {
      var b = a(this),
        c = j.val(),
        d = b.data("value");
      j.find("option").each(function () {
        this.selected = false
      });
      a(b.data("orig")).each(function () {
        this.selected = true
      });
      o.removeClass("selected");
      b.addClass("selected");
      n.attr("aria-active-descendant", b.attr("id"));
      m.find(".text").attr("title", b.find(".text").html());
      m.find(".text").html(h.displayFormat.call(b.data("orig")));
      c !== d && j.change()
    };
    Z = function () {
      za();
      j.focus();
      W.call(this);
      return false
    };
    X = function () {
      u = ""
    };
    I = function (a) {
      var b, c, d = ea();
      for (b = 0; b < d.size(); b++) if (c = d.eq(b).find(".text"), c = c.children().size() == 0 ? c.text() : c.find("*").text(), a.length > 0 && c.toLowerCase().match("^" + a.toLowerCase())) return d.eq(b);
      return null
    };
    ua = function (a) {
      a = I(a);
      return a !== null ? (W.call(a[0]), true) : false
    };
    aa = function (a) {
      !a.ctrlKey && !a.altKey && (a.which === 38 || a.which === 40 || a.which === 8 || a.which === 32) && a.preventDefault()
    };
    ya = function (a) {
      var b, c;
      b = E();
      var d = ea();
      for (b = d.index(b) + 1; b < d.size(); b++) if (c = d.eq(b).find(".text").text(), c !== "" && c.substring(0, 1).toLowerCase() === a.toLowerCase()) return W.call(d.eq(b)[0]), true;
      return false
    };
    Ea = function (a) {
      if (a.altKey || a.ctrlKey) return false;
      var b = E(),
        c = ea();
      switch (a.which) {
      case 9:
        H();
        Y();
        break;
      case 35:
        b.size() > 0 && (a.preventDefault(), W.call(c.filter(":last")[0]), Ha());
        break;
      case 36:
        b.size() > 0 && (a.preventDefault(), W.call(c.filter(":first")[0]), Ha());
        break;
      case 38:
        b.size() > 0 && (c.filter(":first")[0] !== b[0] && (a.preventDefault(), W.call(c.eq(c.index(b) - 1)[0])), Ha());
        break;
      case 40:
        b.size() > 0 ? c.filter(":last")[0] !== b[0] && (a.preventDefault(), W.call(c.eq(c.index(b) + 1)[0]), Ha()) : o.size() > 1 && (a.preventDefault(), W.call(o.eq(0)[0]))
      }
    };
    fa = function (a) {
      if (a.altKey || a.ctrlKey) return false;
      a.which !== 38 && a.which !== 40 && (u += String.fromCharCode(a.keyCode), ua(u) ? (clearTimeout(w), w = setTimeout(X, h.acTimeout)) : ya(String.fromCharCode(a.keyCode)) ? (Ha(), clearTimeout(w), w = setTimeout(X, h.acTimeout)) : (X(), clearTimeout(w)))
    };
    P = function () {
      Fa();
      n.addClass("focused");
      a(document).click(za).keyup(fa).keypress(aa).keydown(aa).keydown(Ea)
    };
    Y = function () {
      n.removeClass("focused");
      m.removeClass("active");
      a(document).unbind("keyup", fa).unbind("keydown", aa).unbind("keypress", aa).unbind("keydown", Ea)
    };
    M = function () {
      a(this).addClass("hover")
    };
    A = function () {
      a(this).removeClass("hover")
    };
    S = function () {
      m.addClass("active");
      a(document).bind("mouseup", La)
    };
    La = function () {
      m.removeClass("active");
      a(document).unbind("mouseup", La)
    };
    this.init = function (c) {
      if (!(a.browser.msie && a.browser.version < 7)) {
        j = a(this.elem);
        j.attr("id") && (l = a("label[for='" + j.attr("id") + "']:first"));
        if (!l || l.size() === 0) l = j.closest("label");
        if (!j.hasClass("has_sb")) h = a.extend({
          acTimeout: 800,
          animDuration: 200,
          ddCtx: "body",
          dropupThreshold: 150,
          fixedWidth: false,
          maxHeight: false,
          maxWidth: false,
          selectboxClass: "selectbox",
          useTie: false,
          arrowMarkup: "<div class='arrow_btn'><span class='arrow'></span></div>",
          displayFormat: b,
          optionFormat: function () {
            if (a(this).size() > 0) {
              var b = a(this).attr("label");
              return b && b.length > 0 ? b : a(this).text()
            } else return ""
          },
          optgroupFormat: function () {
            return "<span class='label'>" + a(this).attr("label") + "</span>"
          }
        }, c), h.displayFormat = h.displayFormat || h.optionFormat, C()
      }
    };
    this.open = cb;
    this.close = H;
    this.refresh = O;
    this.destroy = K;
    this.options = function (b) {
      h = a.extend(h, b);
      O()
    }
    console.log(hhmmss_to_milliseconds('1d 2h 30m')); 
  })
})(jQuery, window);
(function () {
  var a;
  a = function () {
    var a;
    a = $(".integrations .integration-popup");
    if (a.attr("rel")) return a.find("p.integrations-logo").removeClass(a.attr("rel")), a.find("p.key-info").html(""), a.attr("rel", ""), a.addClass("hidden")
  };
  $(".connect-integration ul a").bind("click", function (e) {
    var b, c;
    e.preventDefault();
    a();
    e = $(this).attr("rel");
    b = $("#" + e + "_workspace .key-info").html();
    c = $(".integrations .integration-popup");
    c.attr("rel", e);
    c.find("p.integrations-logo").addClass(e);
    c.find("p.key-info").html(b);
    return c.removeClass("hidden")
  });
  $(".integration-popup a.cancel-connecting").bind("click", function (e) {
    e.preventDefault();
    return a()
  });
  $(".integration-popup input.connect").bind("click", function (e) {
    var b;
    e.preventDefault();
    b = $(".integrations .integration-popup").attr("rel");
    e = $("#" + b + "_workspace");
    e.removeClass("hidden");
    e.find("a.settings-link").click();
    $('a[rel="' + b + '"]').parent().addClass("hidden");
    return a()
  })
}).call(this);