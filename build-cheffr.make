api = 2
core = 7.x

; Include the definition of how to build Drupal core directly, including patches.
includes[] = "drupal-org-core.make"

; Download the Capla install profile and recursively build all its dependencies.
projects[cheffr][type] = "profile"
projects[cheffr][download][type] = "get"
projects[cheffr][download][url] = xx.tar.gz