<?php
/**
 * @file
 * foodblog_basic_perms.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function foodblog_basic_perms_user_default_permissions() {
  $permissions = array();

  // Exported permission: access administration menu.
  $permissions['access administration menu'] = array(
    'name' => 'access administration menu',
    'roles' => array(
      0 => 'tester',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: access blog toolbar.
  $permissions['access blog toolbar'] = array(
    'name' => 'access blog toolbar',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'foodblog_toolbar',
  );

  // Exported permission: access content.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      0 => 'anonymous user',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: blog post.
  $permissions['blog post'] = array(
    'name' => 'blog post',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'foodblog_blog',
  );

  // Exported permission: display drupal links.
  $permissions['display drupal links'] = array(
    'name' => 'display drupal links',
    'roles' => array(
      0 => 'tester',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: flush caches.
  $permissions['flush caches'] = array(
    'name' => 'flush caches',
    'roles' => array(
      0 => 'tester',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: switch users.
  $permissions['switch users'] = array(
    'name' => 'switch users',
    'roles' => array(
      0 => 'tester',
    ),
    'module' => 'devel',
  );

  return $permissions;
}
