<?php
/**
 * @file
 * foodblog_contexts_blogs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function foodblog_contexts_blogs_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
