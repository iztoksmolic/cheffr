<?php
/**
 * @file
 * foodblog_contexts_blogs.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function foodblog_contexts_blogs_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cheffr_user_blog_global';
  $context->description = 'Global context for user blogs';
  $context->tag = 'Blog';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'foodblog_blog-about' => array(
          'module' => 'foodblog_blog',
          'delta' => 'about',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-76c782bb5f8775009d03693af5d1a674' => array(
          'module' => 'views',
          'delta' => '76c782bb5f8775009d03693af5d1a674',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-blog_terms_list-recipe_courses' => array(
          'module' => 'views',
          'delta' => 'blog_terms_list-recipe_courses',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog');
  t('Global context for user blogs');
  $export['cheffr_user_blog_global'] = $context;

  return $export;
}
