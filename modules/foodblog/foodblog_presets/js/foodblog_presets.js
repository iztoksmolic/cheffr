(function($) {

/**
 * Core behavior for Foodblog Toolbar.
 */
Drupal.behaviors.foodblogPresets = {
  attach: function (context, settings) {
    //console.log(settings.foodblog_toolbar.destination);

    if (settings.admin_menu.suppress) {
      return;
    }

    var toolbar_html_path = "http://" + settings.foodblog_toolbar.destination;

    $.ajax({
      url: toolbar_html_path,
      success: function(data) {
        $('body').append(data).addClass('foodblog-toolbar');
      }
    });
  }
};

})(jQuery);
