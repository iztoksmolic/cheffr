/* $Id: admin_menu.js,v 1.32 2010/02/20 23:44:00 sun Exp $ */
(function($) {

/**
 * Core behavior for Foodblog Toolbar.
 */
Drupal.behaviors.foodblogToolbar = {
  attach: function (context, settings) {
    //console.log(settings.foodblog_toolbar.destination);

    if (settings.foodblog_toolbar.suppress) {
      return;
    }

    var toolbar_html_path = "http://" + settings.foodblog_toolbar.destination;

    $.ajax({
      url: toolbar_html_path,
      success: function(data) {
        $('body').append(data).addClass('foodblog-toolbar');
      }
    });
  }
};

})(jQuery);
