<?php
/**
 * @file
 * foodblog_perms.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function foodblog_perms_user_default_permissions() {
  $permissions = array();

  // Exported permission: access administration menu
  $permissions['access administration menu'] = array(
    'name' => 'access administration menu',
    'roles' => array(
      0 => 'tester',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: access all views
  $permissions['access all views'] = array(
    'name' => 'access all views',
    'roles' => array(),
  );

  // Exported permission: access blog toolbar
  $permissions['access blog toolbar'] = array(
    'name' => 'access blog toolbar',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'foodblog_toolbar',
  );

  // Exported permission: access content
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      0 => 'anonymous user',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: access content overview
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(),
  );

  // Exported permission: access contextual links
  $permissions['access contextual links'] = array(
    'name' => 'access contextual links',
    'roles' => array(),
  );

  // Exported permission: access devel information
  $permissions['access devel information'] = array(
    'name' => 'access devel information',
    'roles' => array(
      0 => 'anonymous user',
      1 => 'authenticated user',
      2 => 'tester',
    ),
    'module' => 'devel',
  );

  // Exported permission: access user profiles
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(),
  );

  // Exported permission: administer content types
  $permissions['administer content types'] = array(
    'name' => 'administer content types',
    'roles' => array(),
  );

  // Exported permission: administer nodes
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(),
  );

  // Exported permission: administer permissions
  $permissions['administer permissions'] = array(
    'name' => 'administer permissions',
    'roles' => array(),
  );

  // Exported permission: administer search
  $permissions['administer search'] = array(
    'name' => 'administer search',
    'roles' => array(
      0 => 'admin',
    ),
    'module' => 'search',
  );

  // Exported permission: administer users
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(),
  );

  // Exported permission: administer views
  $permissions['administer views'] = array(
    'name' => 'administer views',
    'roles' => array(),
  );

  // Exported permission: blog post
  $permissions['blog post'] = array(
    'name' => 'blog post',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'foodblog_blog',
  );

  // Exported permission: bypass node access
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(),
  );

  // Exported permission: cancel account
  $permissions['cancel account'] = array(
    'name' => 'cancel account',
    'roles' => array(),
  );

  // Exported permission: change own username
  $permissions['change own username'] = array(
    'name' => 'change own username',
    'roles' => array(),
  );

  // Exported permission: create cheffr_blog content
  $permissions['create cheffr_blog content'] = array(
    'name' => 'create cheffr_blog content',
    'roles' => array(
      0 => 'anonymous user',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create global_page content
  $permissions['create global_page content'] = array(
    'name' => 'create global_page content',
    'roles' => array(),
  );

  // Exported permission: create post_blog content
  $permissions['create post_blog content'] = array(
    'name' => 'create post_blog content',
    'roles' => array(),
  );

  // Exported permission: create post_recipe content
  $permissions['create post_recipe content'] = array(
    'name' => 'create post_recipe content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create post_review content
  $permissions['create post_review content'] = array(
    'name' => 'create post_review content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any cheffr_blog content
  $permissions['delete any cheffr_blog content'] = array(
    'name' => 'delete any cheffr_blog content',
    'roles' => array(),
  );

  // Exported permission: delete any global_page content
  $permissions['delete any global_page content'] = array(
    'name' => 'delete any global_page content',
    'roles' => array(),
  );

  // Exported permission: delete any post_blog content
  $permissions['delete any post_blog content'] = array(
    'name' => 'delete any post_blog content',
    'roles' => array(),
  );

  // Exported permission: delete any post_recipe content
  $permissions['delete any post_recipe content'] = array(
    'name' => 'delete any post_recipe content',
    'roles' => array(),
  );

  // Exported permission: delete any post_review content
  $permissions['delete any post_review content'] = array(
    'name' => 'delete any post_review content',
    'roles' => array(),
  );

  // Exported permission: delete own cheffr_blog content
  $permissions['delete own cheffr_blog content'] = array(
    'name' => 'delete own cheffr_blog content',
    'roles' => array(),
  );

  // Exported permission: delete own global_page content
  $permissions['delete own global_page content'] = array(
    'name' => 'delete own global_page content',
    'roles' => array(),
  );

  // Exported permission: delete own post_blog content
  $permissions['delete own post_blog content'] = array(
    'name' => 'delete own post_blog content',
    'roles' => array(),
  );

  // Exported permission: delete own post_recipe content
  $permissions['delete own post_recipe content'] = array(
    'name' => 'delete own post_recipe content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own post_review content
  $permissions['delete own post_review content'] = array(
    'name' => 'delete own post_review content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete revisions
  $permissions['delete revisions'] = array(
    'name' => 'delete revisions',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: display drupal links
  $permissions['display drupal links'] = array(
    'name' => 'display drupal links',
    'roles' => array(
      0 => 'tester',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: edit any cheffr_blog content
  $permissions['edit any cheffr_blog content'] = array(
    'name' => 'edit any cheffr_blog content',
    'roles' => array(),
  );

  // Exported permission: edit any global_page content
  $permissions['edit any global_page content'] = array(
    'name' => 'edit any global_page content',
    'roles' => array(),
  );

  // Exported permission: edit any post_blog content
  $permissions['edit any post_blog content'] = array(
    'name' => 'edit any post_blog content',
    'roles' => array(),
  );

  // Exported permission: edit any post_recipe content
  $permissions['edit any post_recipe content'] = array(
    'name' => 'edit any post_recipe content',
    'roles' => array(),
  );

  // Exported permission: edit any post_review content
  $permissions['edit any post_review content'] = array(
    'name' => 'edit any post_review content',
    'roles' => array(),
  );

  // Exported permission: edit own cheffr_blog content
  $permissions['edit own cheffr_blog content'] = array(
    'name' => 'edit own cheffr_blog content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own global_page content
  $permissions['edit own global_page content'] = array(
    'name' => 'edit own global_page content',
    'roles' => array(),
  );

  // Exported permission: edit own post_blog content
  $permissions['edit own post_blog content'] = array(
    'name' => 'edit own post_blog content',
    'roles' => array(),
  );

  // Exported permission: edit own post_recipe content
  $permissions['edit own post_recipe content'] = array(
    'name' => 'edit own post_recipe content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own post_review content
  $permissions['edit own post_review content'] = array(
    'name' => 'edit own post_review content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: execute php code
  $permissions['execute php code'] = array(
    'name' => 'execute php code',
    'roles' => array(
      0 => 'tester',
    ),
    'module' => 'devel',
  );

  // Exported permission: flush caches
  $permissions['flush caches'] = array(
    'name' => 'flush caches',
    'roles' => array(
      0 => 'tester',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: rate content
  $permissions['rate content'] = array(
    'name' => 'rate content',
    'roles' => array(
      0 => 'admin',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'fivestar',
  );

  // Exported permission: revert revisions
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: save draft
  $permissions['save draft'] = array(
    'name' => 'save draft',
    'roles' => array(
      0 => 'admin',
      1 => 'authenticated user',
    ),
    'module' => 'save_draft',
  );

  // Exported permission: search content
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(
      0 => 'admin',
    ),
    'module' => 'search',
  );

  // Exported permission: select account cancellation method
  $permissions['select account cancellation method'] = array(
    'name' => 'select account cancellation method',
    'roles' => array(),
  );

  // Exported permission: switch users
  $permissions['switch users'] = array(
    'name' => 'switch users',
    'roles' => array(
      0 => 'tester',
    ),
    'module' => 'devel',
  );

  // Exported permission: view own unpublished content
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: view revisions
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
