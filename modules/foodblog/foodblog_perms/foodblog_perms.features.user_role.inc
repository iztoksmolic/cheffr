<?php
/**
 * @file
 * foodblog_perms.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function foodblog_perms_user_default_roles() {
  $roles = array();

  // Exported role: admin
  $roles['admin'] = array(
    'name' => 'admin',
    'weight' => '4',
  );

  // Exported role: tester
  $roles['tester'] = array(
    'name' => 'tester',
    'weight' => '3',
  );

  return $roles;
}
