<?php

/**
 * Returns an array of post node type.
 */
function blog_post_node_types() {
  return array('post_recipe', 'post_blog');
}

/**
 * Implements hook_permission().
 */
function foodblog_blog_permission() {
  return array(
    'blog post' => array(
      'title' => t('Blog posting'), 
      'description' => t('Perform blog posting.'),
    ),
  );
}

/**
 * Implements hook_init().
 */
function foodblog_blog_init() {
  //dpm(arg(0));
  //dpm(arg(1));
  //dpm(arg(2));
  
  foodblog_blog_update_domains();
}

/**
 * Implements hook_menu_alter().
 */
function foodblog_blog_menu_alter(&$items) {
  // alter ther node/%node page view
  //$items['user/register']['access callback'] = false;
}

/**
 * Implements hook_menu().
 */
function foodblog_blog_menu() {
  $items['blog/%blog_node'] = array(
    'page callback' => 'foodblog_blog_page_view',
    'page arguments' => array(1),
    'access callback' => 'node_access',
    'access arguments' => array('view', 1),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['blog/%blog_node/post/%post_node'] = array(
    'title callback' => 'foodblog_blog_post_page_view_page_title',
    'title arguments' => array(1, 3),
    'page callback' => 'foodblog_blog_node_page_view',
    'page arguments' => array(3),
    'access callback' => 'node_access',
    'access arguments' => array('view', 3),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['dashboard'] = array(
    'title' => 'My posts',
    'page callback' => 'foodblog_dashboard_posts_published',
    'access callback' => true,
  );
  $items['dashboard/posts'] = array(
    'title' => 'My posts',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
    'access callback' => true,
  );
  $items['dashboard/posts/published'] = array(
    'title' => 'My published posts',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
    'access callback' => true,
  );
  $items['dashboard/posts/drafts'] = array(
    'title' => 'My drafts',
    'page callback' => 'foodblog_dashboard_posts_drafts',
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
    'access callback' => true,
  );
  $items['dashboard/posts/search'] = array(
    'title' => 'Search my posts',
    'page callback' => 'foodblog_dashboard_posts_search',
    'type' => MENU_LOCAL_TASK,
    'access callback' => true,
  );
  $items['dashboard/customize'] = array(
    'title' => 'Customize blog',
    'page callback' => 'foodblog_dashboard_customize',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
    'access callback' => true,
    'file' =>  'node.pages.inc',
    'file path' => drupal_get_path('module', 'node'),
  );
  $items['dashboard/customize/info'] = array(
    'title' => "Basic information",
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
    'access callback' => true,
  );
  $items['dashboard/customize/design'] = array(
    'title' => "Design",
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
    'access callback' => true,
  );
  $items['dashboard/account'] = array(
    'title' => 'Account',
    'page callback' => 'foodblog_dashboard_account',
    'file' =>  'user.pages.inc',
    'file path' => drupal_get_path('module', 'user'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
    'access callback' => true,
  );
  $items['new'] = array(
    'page callback' => 'node_add',
    'page arguments' => array('post_recipe'),
    'access callback' => 'node_access',
    'access arguments' => array('create', 'post_recipe'),
    'file' =>  'node.pages.inc',
    'file path' => drupal_get_path('module', 'node'),
    'title' => 'New recipe post',
  );
  $items['new/recipe'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
    'title' => 'Recipe',
  );
  $items['new/blog'] = array(
    'title' => 'Blog',
    'page callback' => 'node_add',
    'page arguments' => array('post_blog'),
    'access callback' => 'node_access',
    'access arguments' => array('create', 'post_blog'),
    'file' =>  'node.pages.inc',
    'file path' => drupal_get_path('module', 'node'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  $items['signup'] = array(
    'title' => 'Signup',
    'title' => 'Create new account',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_register_form'),
    'access callback' => 'user_register_access',
  );
  return $items;
}

/**
 * front page of any blog
 */
function foodblog_blog_page_view($blog_node) {
  
  if($blog_node) {
    $blog_id = $blog_node->nid;
  } else {
    return false;
  }
  
  $output = views_embed_view('blog_front_page', 'default', $blog_id);

  return $output;
}

/**
 * Menu callback; view a single node.
 */
function foodblog_blog_node_page_view($node) {
  $uri = entity_uri('node', $node);
  // Set the node path as the canonical URL to prevent duplicate content.
  drupal_add_html_head_link(array('rel' => 'canonical', 'href' => url($uri['path'], $uri['options'])), TRUE);
  // Set the non-aliased path as a default shortlink.
  drupal_add_html_head_link(array('rel' => 'shortlink', 'href' => url($uri['path'], array_merge($uri['options'], array('alias' => TRUE)))), TRUE);
  return node_show($node);
}

function foodblog_blog_post_page_view_page_title($blog_node, $post_node) {
  return $post_node->title;
}

function blog_node_load($nid = NULL, $vid = NULL, $reset = FALSE) {
  $nids = (isset($nid) ? array($nid) : array());
  $conditions = (isset($vid) ? array('vid' => $vid) : array());
  $node = node_load_multiple($nids, $conditions, $reset);
  if(reset($node)->type != 'cheffr_blog') {
    return FALSE;
  }
  return $node ? reset($node) : FALSE; 
}

function post_node_load($nid = NULL, $vid = NULL, $reset = FALSE) {
  $nids = (isset($nid) ? array($nid) : array());
  $conditions = (isset($vid) ? array('vid' => $vid) : array());
  $node = node_load_multiple($nids, $conditions, $reset);
  
  $single_node = reset($node);
  
  if($single_node && isset($single_node->field_blog_reference)) {
    return $single_node;
  } else {
    return FALSE;
  }
}

/**
 * dashboard pages
 */
function foodblog_dashboard_posts_published() {  
  global $user;
  if (!$user->uid) {
    menu_set_active_item('signup');
    return menu_execute_active_handler(NULL, FALSE);
  }
  else {
    return latest_blog_posts();
  }
}

/**
 * Generate a listing of blog nodes.
 */
function latest_blog_posts() {
  global $user;
  $uid = $user->uid;

  if($blogs_ids = get_user_blogs($uid)) {
    $blog_id = $blogs_ids[0];
    
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
                      ->fieldCondition('field_blog_reference', 'target_id', $blog_id)
                      ->propertyOrderBy('created', 'DESC')
                      ->pager()
                      ->execute();
  
    if(!empty($entities['node'])) {
      $nodes = node_load_multiple(array_keys($entities['node']));
      $build = node_view_multiple($nodes, 'teaser');  
      $build['pager'] = array(
        '#theme' => 'pager',
        '#weight' => 5,
      );
      return $build; 
    } else {
      return 'No posts';
    }
   
  } else {
    return false;
  }

}

function foodblog_dashboard_posts_drafts() {
  return 'view 2';
}

function foodblog_dashboard_posts_search() {
  return 'view 3';
}

function foodblog_dashboard_customize() {
  global $user;
  $uid = $user->uid;
  
  $blogs = get_user_blogs($uid);
  $node_id = $blogs[0];
  
  $node = node_load($node_id);
  
  $type_name = node_type_get_name($node);
  drupal_set_title(t('<em>Edit @type</em> @title', array('@type' => $type_name, '@title' => $node->title)), PASS_THROUGH);
  return drupal_get_form($node->type . '_node_form', $node);
}

function foodblog_dashboard_account() {
  global $user;
  return drupal_get_form('user_profile_form', $user);
}

/**
 * new post page
 */
function foodblog_new() {
  return 'new post';
}

/**
 * get the array of blogs from user
 */

function get_user_blogs($uid) {
  $select = db_select('node', 'n')
    ->fields('n', array('nid', 'created'))
    ->condition('uid', $uid)
    ->condition('type', 'cheffr_blog')
    ->orderBy('created', 'DESC');

  $nids = $select->execute()->fetchCol();
  
  if($nids) {
    return $nids;
  } else {
    return false;
  }
}

/**
 * Implements hook_block_info().
 */
function foodblog_blog_block_info() {
  $blocks['about'] = array(
    'info' => t('Cheffr blog: About'), 
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function foodblog_blog_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'about':
      $block['subject'] = NULL;
      $block['content'] = _foodblog_blog_block_about();
      break;
  }
  return $block;
}

function _foodblog_blog_block_about() {
  if($blog_node = menu_get_object('blog_node', 1)) {
    //dpm($blog_node);
    $html = l($blog_node->title, 'node/' . $blog_node->nid);
    $html .= $blog_node->field_blog_about[LANGUAGE_NONE][0]['safe_value'];
    return $html;
  } else {
    return false;
  }
}

/**
 * Preprocess variables for html.tpl.php
 */
function foodblog_blog_preprocess_html(&$variables) {
  if($blog_node = menu_get_object('blog_node', 1)) {
    // Construct page title.
    if (drupal_get_title()) {
      $head_title = array(
        'title' => strip_tags(drupal_get_title()),
        'name' => check_plain($blog_node->title),
      );
    }
    else {
      $head_title = array('name' => check_plain($blog_node->title));
      if (!empty($blog_node->field_blog_slogan[LANGUAGE_NONE][0]['safe_value'])) {
        $head_title['slogan'] = check_plain($blog_node->field_blog_slogan[LANGUAGE_NONE][0]['safe_value']);
      }
    }
    $variables['head_title_array'] = $head_title;
    $variables['head_title'] = implode(' | ', $head_title);
  }
}

/**
 * Preprocess variables for page.tpl.php
 */
function foodblog_blog_preprocess_page(&$variables) {
  // loading blog node, type must be blog_node since we have blog_node_load() function
  //dpm($blog_node);
  if($blog_node = menu_get_object('blog_node', 1)) {
    $variables['site_name'] = check_plain($blog_node->title);
    $variables['site_slogan']  = check_plain($blog_node->field_blog_slogan[LANGUAGE_NONE][0]['safe_value']);
    $variables['front_page'] = url('');
    $inline_css = foodblog_blog_generate_css($blog_node);
    drupal_add_css($inline_css, array('type' => 'inline'));
  }

  if($blog_node = menu_get_object('post_node', 3)) {
    $variables['is_post_node'] = true;
  }

  if(arg(0) == 'dashboard') {
    $variables['cheffr_site_name'] = t('dashboard');
  } elseif(arg(0) == 'new') {
    $variables['cheffr_site_name'] = t('new post');
  } else {
    $variables['cheffr_site_name'] = $variables['site_name'];
  }

  if(arg(0) == 'signup') {
    $variables['title'] = '';
  }
  
  if(arg(0) == 'dashboard') {
    //drupal_set_title(t('Dashboard'));
    $variables['section_title'] = t('Dashboard');
  }
  
  if(arg(0) == 'new') {
    $variables['section_title'] = t('New post');
  }
  
  if(arg(0) == 'node' && arg(2) == 'edit') {
    drupal_set_title(t('Edit post')); 
  }
  
  if(arg(0) == 'explore-recipes') {
    $variables['section_title'] = t('Explore recipes');
  }  
  
  
}

/**
 * Preprocess variables for node.tpl.php
 */
function foodblog_blog_preprocess_node(&$variables) {
  // loading blog node, type must be blog_node since we have blog_node_load() function
  //dpm($blog_node);
  if($post_node = menu_get_object('post_node', 3)) {
    $variables['page'] = TRUE;
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for the registration form.
 */
function foodblog_blog_form_user_register_form_alter(&$form, &$form_state) {
  $form['field_blog_domain_prefix'] = array(
    '#type' => 'textfield', 
    '#title' => t('Blog URL'), 
    '#required' => TRUE,
    '#field_suffix' => '.' . variable_get('main_domain_name'),
    //'#description' => "Please enter the blog URL.", 
    '#size' => 40, 
    '#maxlength' => 40,
  );
  $form['#validate'][] = 'user_register_validate_field_blog_domain_prefix';
  $form['#submit'][] = 'user_register_submit_field_blog_domain';
  unset($form['account']['pass']['#description']);
  unset($form['account']['mail']['#description']);
  $form['actions']['submit']['#value'] = t('Start posting!');
  $form['message']['#markup'] = '<p class="signup-message">' . t('Start food-blogging in seconds with cheffr!') . '</p>';
  $form['message']['#weight'] = -20;
}

/**
 * Implements hook_node_presave() function.
 */
function foodblog_blog_node_presave($node) {
  dpm($node);
}

/**
 * Implements hook_form_alter() function.
 */
function foodblog_blog_form_alter(&$form, &$form_state, $form_id) {
  // node edit form on blog posts
  if(!empty($form['#node_edit_form']) && $form['#node_edit_form'] == TRUE && isset($form['field_blog_reference'])) {
    global $user;
    $lang = $form['field_blog_reference']['#language'];
    $form['field_blog_reference'][$lang]['#element_validate'][] = 'foodblog_blog_reference_field_validate';
    
    if($blog_ids = get_user_blogs($user->uid)) {
      $form['field_blog_reference'][$lang]['#default_value'] = $blog_ids[0];
      foreach($form['field_blog_reference'][$lang]['#options'] as $index=>$option) {
        if(!in_array($index,$blog_ids) && !user_access('administer nodes')) {
          unset($form['field_blog_reference'][$lang]['#options'][$index]);
        }
      }
      if(count($form['field_blog_reference'][$lang]['#options']) <= 1 && !user_access('administer nodes')) {
        $form['field_blog_reference'][$lang]['#access'] = false;
      }
    } else {
      drupal_set_message(t('You will need to first create a blog to post on it:)'));
      drupal_goto('node/add/cheffr-blog');
    }
  }
  
  if($form_id == 'post_recipe_node_form') {
    //dpm($form);
    $form['#attached']['js'] = array(
      drupal_get_path('module', 'foodblog_blog') . '/js/foodblog_addfield.js',
    );
  }
}


function foodblog_blog_reference_field_validate($element, &$form_state, $form) {
  global $user;
  if($blog_ids = get_user_blogs($user->uid)) {
    $value = $element['#value'];
    if(!in_array($value,$blog_ids)) {
      form_error($element, t('You must choose a blog you own!'));
    }
  }
}

/**
 * submit blog url
 */
function user_register_submit_field_blog_domain($form, &$form_state) {
  //dpm(&$form_state);
  
  if(isset($form_state['user']->uid)) {
    $uid = $form_state['user']->uid;
    
    $domain_prefix = $form_state['values']['field_blog_domain_prefix'];
    $domain = $domain_prefix . '.' . variable_get('main_domain_name');
    $name = ucfirst(str_replace('-',' ',strtolower($domain_prefix)));
    
    // Create a node object, and add node properties.
    $newNode = (object) NULL;
    $newNode->type = 'cheffr_blog';
    $newNode->uid = $uid;
    $newNode->created = strtotime("now");
    $newNode->changed = strtotime("now");
    $newNode->status = 1;
    $newNode->title = $name . t(" - food blog");
    $newNode->field_blog_slogan[LANGUAGE_NONE][0]['value'] = t('A very special food blog');
    $newNode->field_blog_domain[LANGUAGE_NONE][0]['value'] = $domain;
    $newNode->language = LANGUAGE_NONE;

    node_save($newNode);
  }
}

/**
 * Implements hook_node_insert().
 */
function foodblog_blog_node_insert($node) {
  // update the file with maped domains with NIDs
  if($node->type == "cheffr_blog") {
    foodblog_blog_update_domains();
  }
  
}

/**
 * Implements hook_custom_theme().
 */
function foodblog_blog_custom_theme() {
  if (arg(0) == 'blog') {
    return 'cheffrblog';
  }
}

/**
 * validate blog url
 */

function user_register_validate_field_blog_domain_prefix($form, &$form_state) {
  if(isset($form_state['values']['field_blog_domain_prefix'])) {
    $domain_prefix = $form_state['values']['field_blog_domain_prefix'];
    $domain = $domain_prefix . '.' . variable_get('main_domain_name');
    
    if(!is_valid_domain_name_prefix($domain_prefix)) {
      form_set_error('blog_domain', t('Not valid domain name prefix.'));
    } else {
      $select = db_select('field_data_field_blog_domain', 'fbd')
        ->fields('fbd', array('field_blog_domain_value'))
        ->condition('fbd.field_blog_domain_value', $domain)
        ->execute();
        
      $num_of_results = $select->rowCount();
  
      if($num_of_results > 0) {
        form_set_error('blog_domain', t('This domain name is taken.'));
      }      
    }
  } else {
    form_set_error('blog_domain', t('Blog domain field is required.'));
  }
}

/**
 * check if valid domain name
 */
function is_valid_domain_name($domain_name) {
  $pieces = explode(".",$domain_name);
  foreach($pieces as $piece) {
    if (!preg_match('/^[a-z\d][a-z\d-]{0,62}$/i', $piece) || preg_match('/-$/', $piece) ) {
      return false;
    }
  }
  return true;
}

/**
 * check if valid domain prefix
 */
function is_valid_domain_name_prefix($domain_name_prefix) {  
  if (preg_match("/^[0-9a-z][0-9a-z-]{1,18}[0-9a-z]$/i", $domain_name_prefix) ) {
    return true;
  } else {
    return false;
  }
}

/**
 * generate CSS for specific blog
 */
function foodblog_blog_generate_css($blog_node) {
  $css = '';
  
  $field = $blog_node->field_blog_color_background;
  $color = !empty($field[LANGUAGE_NONE][0]['jquery_colorpicker']) ? $field[LANGUAGE_NONE][0]['jquery_colorpicker'] : default_colorpick($blog_node, 'field_blog_color_background');
  $css .= 'body { background-color:#' . $color . ' !important; }';

  if(!empty($blog_node->field_blog_image_background[LANGUAGE_NONE][0]['uri'])) {
    $image_path = $blog_node->field_blog_image_background[LANGUAGE_NONE][0]['uri'];
    $path = file_create_url($image_path);
    $css .= '#page-wrapper { background-image:url(' . $path . ') !important; }';
  }

  $field = $blog_node->field_blog_color_link;
  $color = !empty($field[LANGUAGE_NONE][0]['jquery_colorpicker']) ? $field[LANGUAGE_NONE][0]['jquery_colorpicker'] : default_colorpick($blog_node, 'field_blog_color_link');
  $css .= '#page a { color:#' . $color . ' !important; }';
  
  return $css;
}

function default_colorpick($blog_node, $field_name) {
  $result = field_get_default_value('node', $blog_node, field_info_field($field_name), field_info_instance('node', $field_name, 'cheffr_blog'));
  return $result[0]['jquery_colorpicker'];
}

/**
 * this function updates the txt file with the domain names
 *
 * help: http://httpd.apache.org/docs/trunk/rewrite/rewritemap.html
 */
function foodblog_blog_update_domains() {
  $query = db_select('field_data_field_blog_domain', 'fbd')
    ->fields('fbd', array('field_blog_domain_value', 'entity_id'))
    ->orderBy('entity_id', 'ASC');
  
  $result = $query->execute();
  //dpm($result->fetchAll());
  
  $str = '';
  
  foreach ($result as $record) {
    $str .= $record->field_blog_domain_value . " " . $record->entity_id . "\n";
  }

  //$hash = sdbm_hash($str);
  //$destination = '/Users/iztok/www/cookblogplatform/blogs.sdbm';
  //$file_error = file_put_contents($destination, $hash);  
  
  //$destination = '/Users/iztok/www/cookblogplatform/blogs.txt';
  $destination = variable_get('domain_list_path');
  $file_error = file_put_contents($destination, $str);
  
  watchdog('cheffr', 'generationg domain file: ' . $file_error);
}

/**
 * generate SDBM hash from string
 * http://rustylog.wordpress.com/2012/01/05/sdbm-hash-implementation-in-php/
 */
function sdbm_hash($str) {
	$hash = 0; $n=strlen($str);
	for ($i=0; $i<$n; $i++) {
		$h1 = $hash << 6;
		if ($h1<0) $h1+=0x100000000;
		$h2 = $hash << 16;
		if ($h2<0) $h2+=0x100000000;
		$h3=($hash>=0x80000000?0x100000000-$hash:-$hash);
		$hash = (int)((int)ord($str[$i]) + $h1 + $h2 + $h3);
		if($hash<0) $hash=$hash+0x100000000;
	}
	return $hash;
}