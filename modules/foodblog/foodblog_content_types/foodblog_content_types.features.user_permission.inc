<?php
/**
 * @file
 * foodblog_content_types.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function foodblog_content_types_user_default_permissions() {
  $permissions = array();

  // Exported permission: create cheffr_blog content.
  $permissions['create cheffr_blog content'] = array(
    'name' => 'create cheffr_blog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: create global_page content.
  $permissions['create global_page content'] = array(
    'name' => 'create global_page content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: create post_blog content.
  $permissions['create post_blog content'] = array(
    'name' => 'create post_blog content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create post_recipe content.
  $permissions['create post_recipe content'] = array(
    'name' => 'create post_recipe content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any cheffr_blog content.
  $permissions['delete any cheffr_blog content'] = array(
    'name' => 'delete any cheffr_blog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any global_page content.
  $permissions['delete any global_page content'] = array(
    'name' => 'delete any global_page content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any post_blog content.
  $permissions['delete any post_blog content'] = array(
    'name' => 'delete any post_blog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any post_recipe content.
  $permissions['delete any post_recipe content'] = array(
    'name' => 'delete any post_recipe content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own cheffr_blog content.
  $permissions['delete own cheffr_blog content'] = array(
    'name' => 'delete own cheffr_blog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own global_page content.
  $permissions['delete own global_page content'] = array(
    'name' => 'delete own global_page content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own post_blog content.
  $permissions['delete own post_blog content'] = array(
    'name' => 'delete own post_blog content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own post_recipe content.
  $permissions['delete own post_recipe content'] = array(
    'name' => 'delete own post_recipe content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any cheffr_blog content.
  $permissions['edit any cheffr_blog content'] = array(
    'name' => 'edit any cheffr_blog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any global_page content.
  $permissions['edit any global_page content'] = array(
    'name' => 'edit any global_page content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any post_blog content.
  $permissions['edit any post_blog content'] = array(
    'name' => 'edit any post_blog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any post_recipe content.
  $permissions['edit any post_recipe content'] = array(
    'name' => 'edit any post_recipe content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own cheffr_blog content.
  $permissions['edit own cheffr_blog content'] = array(
    'name' => 'edit own cheffr_blog content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own global_page content.
  $permissions['edit own global_page content'] = array(
    'name' => 'edit own global_page content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own post_blog content.
  $permissions['edit own post_blog content'] = array(
    'name' => 'edit own post_blog content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own post_recipe content.
  $permissions['edit own post_recipe content'] = array(
    'name' => 'edit own post_recipe content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
