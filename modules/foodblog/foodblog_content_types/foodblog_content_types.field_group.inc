<?php
/**
 * @file
 * foodblog_content_types.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function foodblog_content_types_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_custom_design|node|cheffr_blog|form';
  $field_group->group_name = 'group_blog_custom_design';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cheffr_blog';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_blog_design';
  $field_group->data = array(
    'label' => 'Custom design',
    'weight' => '7',
    'children' => array(
      0 => 'field_blog_color_link',
      1 => 'field_blog_color_background',
      2 => 'field_blog_image_background',
      3 => 'field_blog_image_background_tile',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Custom design',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_blog_custom_design|node|cheffr_blog|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_design|node|cheffr_blog|form';
  $field_group->group_name = 'group_blog_design';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cheffr_blog';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Design',
    'weight' => '1',
    'children' => array(
      0 => 'field_blog_preset_designs',
      1 => 'group_blog_custom_design',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_blog_design|node|cheffr_blog|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_info|node|cheffr_blog|form';
  $field_group->group_name = 'group_blog_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cheffr_blog';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Blog info',
    'weight' => '0',
    'children' => array(
      0 => 'field_blog_slogan',
      1 => 'field_blog_about',
      2 => 'field_blog_domain',
      3 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_blog_info|node|cheffr_blog|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_recipe_basicinfo|node|post_recipe|form';
  $field_group->group_name = 'group_post_recipe_basicinfo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post_recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Basic info',
    'weight' => '0',
    'children' => array(
      0 => 'field_post_recipe_summary',
      1 => 'field_post_recipe_photo',
      2 => 'field_post_recipe_tags',
      3 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Basic info',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_post_recipe_basicinfo|node|post_recipe|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_recipe_info|node|post_recipe|form';
  $field_group->group_name = 'group_post_recipe_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post_recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Recipe info',
    'weight' => '2',
    'children' => array(
      0 => 'group_post_recipe_ingredients',
      1 => 'group_post_recipe_instructions',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_post_recipe_info|node|post_recipe|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_recipe_ingredients|node|post_recipe|form';
  $field_group->group_name = 'group_post_recipe_ingredients';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post_recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_post_recipe_info';
  $field_group->data = array(
    'label' => 'Ingredients',
    'weight' => '10',
    'children' => array(
      0 => 'field_post_recipe_ingredients',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_post_recipe_ingredients|node|post_recipe|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_recipe_instructions|node|post_recipe|form';
  $field_group->group_name = 'group_post_recipe_instructions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post_recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_post_recipe_info';
  $field_group->data = array(
    'label' => 'Instructions',
    'weight' => '11',
    'children' => array(
      0 => 'field_post_recipe_instructions',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_post_recipe_instructions|node|post_recipe|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_recipe_metainfo|node|post_recipe|form';
  $field_group->group_name = 'group_post_recipe_metainfo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post_recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Meta info',
    'weight' => '1',
    'children' => array(
      0 => 'field_post_recipe_course',
      1 => 'field_blog_reference',
      2 => 'field_post_recipe_preptime',
      3 => 'field_post_recipe_cooktime',
      4 => 'group_post_recipe_timing',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Meta info',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_post_recipe_metainfo|node|post_recipe|form'] = $field_group;

  return $export;
}
