<?php
/**
 * @file
 * foodblog_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function foodblog_content_types_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function foodblog_content_types_node_info() {
  $items = array(
    'cheffr_blog' => array(
      'name' => t('Cheffr blog'),
      'base' => 'node_content',
      'description' => t('Blog as a cheffr site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'global_page' => array(
      'name' => t('Global page'),
      'base' => 'node_content',
      'description' => t('Global page on the main domain.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'post_blog' => array(
      'name' => t('Post - Blog'),
      'base' => 'node_content',
      'description' => t('Basic blog post without recipe or any kind of food-blogging features.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'post_recipe' => array(
      'name' => t('Post - Recipe'),
      'base' => 'node_content',
      'description' => t('Blog post recipe.'),
      'has_title' => '1',
      'title_label' => t('Recipe title'),
      'help' => '',
    ),
  );
  return $items;
}
